<html>
<head>
</head>
<body style="margin:0;">
<div id='all' style="width:600px;">
<div id='header' style="text-align:center;padding:20px 0;border-bottom: solid 2px #B5B5B5;margin-bottom: 30px;">
<a target='_blank' href='http://www.civimi.com'> 
<img src='http://www.civimi.com/beta/civimi/img/logo/logo-desktop.png' style='width:200px;' alt='civimi'>
</a>
</div>
<div id="content" style="padding: 0 15px;">
Hi ".$username.",<br><br>
Email kamu sudah terverifikasi, selangkah lagi untuk menunjukkan pengalaman dan portfolio ".$username.".<br><br>
Ayo segera lengkapi resume dan portfolio ".$username.", agar segera dapat menikmati layanan dari <a target='_blank' href='http://www.civimi.com/'>civimi.com</a> seperti :<br><br>
<b>- One step create CV.</b> Tinggal download atau print CV ketika diperlukan sesegera mungkin, hanya tinggal login dan print deh<br><br>
<b>- New Job.</b> Kalau CV ".$username." sudah lengkap, ".$username." bisa dengan mudah melamar pekerjaan baru.<br><br>
<b>- Access to Company.</b> Setelah CV dan portfolio ".$username." sudah terisi semua, ".$username." dapat melamar pekerjaan dan mengikuti informasi lowongan terbaru dari perusahaan yang ".$username." inginkan.<br><br>
<b>- Find Me.</b> Nah, ini manfaat paling penting kenapa CV ".$username." harus create in Civimi.com. Diluar sana banyak loh perusahaan yang sedang mencari kriteria <i>employee</i> atau bahkan <i>partner</i> seperti ".$username.".<br><br>
<b>- One step create CV.</b> Tinggal download atau print CV ketika diperlukan sesegera mungkin, hanya tinggal login dan print deh<br><br>
<b>- Ocean Connection.</b> Selain perusahaan favorite, ".$username." juga terkoneksi dengan berbagai orang dari berbagai kalangan di seluruh dunia.<br><br>
Info aja, pengguna Civimi.com bukan hanya dari Indonesia loh, tapi Civimi.com sudah eksis di berbagai negara seperti America, Germany, Singapore dan masih banyak lagi.<br><br>
<a target='_blank' href='http://www.civimi.com/beta/edit/'>Klik disini untuk melengkapi resume kamu.</a><br><br><br>
Regards,<br><br>
<u>Billy Gani</u><br><br>
Customer Success Manager<br>
Civimi.com
</div>
<div id='footer' style='background-color: #666;padding: 10px 0;margin-top:20px;'>
<div style='display: block;height: 50px;padding: 0 20px;position: relative;'>
<div style='font-size: 20px;font-style: italic;color: #FFFFFF;font-weight: bold;display: block;width: 50%;float: left;'>'Reinvent Yourself'</div>
<div style='display: block;float: left;width: 50%;position: relative;text-align: right;'>
<a target='_blank' href='https://www.facebook.com/civimi'><img src='http://www.civimi.com/beta/civimi/img/logo/facebook.png' alt='facebook' style='width: 20px;'></a>
<a target='_blank' href='https://plus.google.com/+Civimi'><img src='http://www.civimi.com/beta/civimi/img/logo/google.png' alt='google' style='width: 20px;margin-left:8px;'></a>
<a target='_blank' href='https://twitter.com/civimi'><img src='http://www.civimi.com/beta/civimi/img/logo/twitter.png' alt='twitter' style='width: 20px;margin-left:8px;'></a>
</div>
</div>
<div style='text-align:center;font-size: 13px;color: #FFFFFF;margin:20px 0 0 0;padding:0 20px;position:relative;'>
Copyright &copy; 2015 Civimi. All rights reserved.
<div style='position: absolute;right: 20px;top: 0;'><a target='_blank' href="" style="font-size:12px;color: #FFFFFF;outline:none !important;text-decoration:underline;">unsubscribe</a></div>
</div>
</div>
</div>
</body>
</html>
