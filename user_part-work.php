<div id="personal-work-section" class="job-sbox" data-order="3">
  <div id="work-header" class="user-section-header row border-bottom-eee">
    <div class="col-xs-6">
      <div class="profile-title"><i class="glyphicon glyphicon-briefcase"></i>&nbsp;&nbsp;<?=$lang['work-title'];?></div>
    </div>
    <div class="col-xs-6">
        <div class="edit-button text-right">
            <span class="sort-icon"><i class="glyphicon glyphicon-sort"></i></span>
            <a href="Javascript:;" id="add-work" onclick="hideEmptyWork();" data-toggle="collapse" data-target="#slide-work" aria-expanded="true" aria-controls="slide-work">
        <i class="glyphicon glyphicon-plus"></i> <span class="hidden-xs"><?=$lang['work-btn'];?></span>
      </a>
        </div>
    </div>
  </div>
  <div id="work-profile" class="">
    <!-- start add work -->
    <div id="slide-work" class="collapse panel-input">
      <div class="panel-input-body panel-input-work">
        <div class="row up15">
          <div class="col-xs-12">
            <div class="form-label bold"><?=$lang['work-job'];?> *</div>
          </div>
          <div class="col-sm-12 col-xs-12">
            <input id="work-title" type="text" name="" class="form-control no-radius" placeholder="<?=$lang['work-job_placeholder'];?>">
            <div id="work-vtitle" class="valid-message"></div>
          </div>
        </div>
        <div class="row up1">
          <div class="col-xs-12">
            <div class="form-label bold"><?=$lang['work-company'];?> *</div>
          </div>
          <div class="col-sm-10 col-xs-12">
            <div id="company-box" class="company-box">
              <input id="work-company" onfocus="$('#company-list').show();" oninput="get_company();$('#company-list').show();" type="text" name="" class="form-control no-radius" placeholder="<?=$lang['work-company_placeholder'];?>">
              <div id="company-list" class="company-list" onmouseover="$('#company-list').show();">
                <ul id="company-ul">

                </ul>
              </div>
            </div>
            <div id="work-vcompany" class="valid-message"></div>
          </div>
        </div>
        <div class="row up1">
          <div class="col-xs-6">
            <div class="form-label bold"><?=$lang['work-country'];?> *</div>
          </div>
          <div class="col-xs-6">
            <div class="form-label bold"><?=$lang['work-city'];?> *</div>
          </div>
        </div>
        <div class="row">
          <div class="col-xs-6">
            <input id="work-country" type="text" class="form-control no-radius" name="country" placeholder="<?=$lang['work-country_placeholder'];?>">
          </div>
          <div class="col-xs-6">
            <input id="work-city" type="text" class="form-control no-radius" name="city" placeholder="<?=$lang['work-city_placeholder'];?>">
          </div>
          <div class="col-xs-12">
            <div id="work-vcountry" class="valid-message"></div>
          </div>
        </div>
        <div class="row up1">
          <!-- <div class="col-xs-12">
            <div class="form-label">Employment Status</div>
          </div> -->
          <div class="col-sm-6 col-xs-12">
            <!-- 
            <select id="work-status" name="" class="form-control no-radius" onchange="checkStat();">
              <option>Choose Status</option>
              <option value="1">Previously Employed</option>
              <option value="2">Currently Employed</option>
            </select> -->
            <div class="checkbox">
              <label>
                <input id="work-status" type="checkbox" onclick="checkStat();" value="yes"> <?=$lang['work-attending'];?>
              </label>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-xs-6 up1">
            <div class="form-label bold"><?=$lang['work-from'];?> *</div>
            <div class="row">
              <div class="col-xs-6" style="padding: 0 5px 0 0;">
                <select id="work-fmonth" name="" class="form-control no-radius">
                  <option value="">Month</option>
                  <?php $month = '12';
                  for ($months = '1'; $months <= $month; $months++) { ?>
                  <option value="<?php echo $months; ?>"><?php echo date("F", strtotime("$months/12/10")); ?></option>
                  <?php } ?>
                </select>
              </div>
              <div class="col-xs-6" style="padding: 0 0 0 5px;">
                <select id="work-fyear" name="" class="form-control no-radius">
                  <option value="">--</option>
                  <?php $year = '1950';
                  for ($years = date("Y"); $years >= $year; $years--) { ?>
                  <option value="<?php echo $years; ?>"><?php echo $years; ?></option>
                  <?php } ?>
                </select>
              </div>
            </div>
          </div>
          <div id="to-stat" class="col-xs-6 up1">
            <div class="form-label bold"><?=$lang['work-to'];?> *</div>
            <div class="row">
              <div class="col-xs-6" style="padding: 0 5px 0 0;">
                <select id="work-tmonth" name="" class="form-control no-radius">
                  <option value="" class="work-tmonth">Month</option>
                  <?php $month = '12';
                  for ($months = '1'; $months <= $month; $months++) { ?>
                  <option value="<?php echo $months; ?>" class="work-tmonth"><?php echo date("F", strtotime("$months/12/10")); ?></option>
                  <?php } ?>
                </select>
              </div>
              <div class="col-xs-6" style="padding: 0 0 0 5px;">
                <select id="work-tyear" name="" class="form-control no-radius">
                  <option value="" class="work-tyear">--</option>
                  <?php $year = '1950';
                  for ($years = date("Y"); $years >= $year; $years--) { ?>
                  <option value="<?php echo $years; ?>" class="work-tyear"><?php echo $years; ?></option>
                  <?php } ?>
                </select>
              </div>
            </div>
          </div>
          <div class="col-xs-12">
            <div id="work-vdate" class="valid-message"></div>
          </div>
        </div>
        <div class="row up1">
          <div class="col-xs-12">
            <div class="form-label"><?=$lang['work-desc'];?></div>
          </div>
          <div class="col-sm-12 col-xs-12">
            <textarea id="work-desc" name="desc" class="form-control no-radius" rows="4" placeholder="<?=$lang['work-desc_placeholder'];?>"></textarea>
          </div>
        </div>
        <div class="row up2">
          <div class="col-xs-12">
            <a href="Javascript:;" class="btn btn-info" onclick="save_work();">Save</a><span class="pad05"></span>
            <a href="Javascript:;" onclick="hideEmptyWork();" type="reset" class="btn btn-default" data-toggle="collapse" data-target="#slide-work" aria-expanded="true" aria-controls="slide-work">Cancel</a>
          </div>
        </div>
      </div>
    </div><!-- end panel add work -->


    <!-- main work -->
    <div id="main-work" class="panel-group marg0" role="tablist" aria-multiselectable="true">
    </div> <!-- end main work -->
    <div class="see-more"><a href="javascript:;" id="see-work" class="hide">SEE <span id="seeWork-num"></span> MORE</a></div>
    <div id='load-work' class='text-center'></div>

    <!-- start edit work -->
    <div id="edit-work-place" class="hide">
      <div id="edit-work">
        <div class="panel-input-work">
          <div class="row up15">
            <div class="col-xs-12">
              <div class="form-label bold"><?=$lang['work-job'];?> *</div>
            </div>
            <div class="col-sm-12 col-xs-12">
              <input id="work-etitle" type="text" name="" class="form-control no-radius" placeholder="<?=$lang['work-job_placeholder'];?>">
              <div id="work-evtitle" class="valid-message"></div>
            </div>
          </div>
          <div class="row up1">
            <div class="col-xs-12">
              <div class="form-label bold"><?=$lang['work-company'];?> *</div>
            </div>
            <div class="col-sm-10 col-xs-12">
              <div id="company-boxEdit" class="company-box">
                <input id="work-ecompany" onfocus="$('#company-listEdit').show();" oninput="get_companyEdit();$('#company-listEdit').show();" type="text" name="" class="form-control no-radius" placeholder="<?=$lang['work-company_placeholder'];?>">
                <div id="company-listEdit" class="company-list" onmouseover="$('#company-listEdit').show();">
                  <ul id="company-eul">

                  </ul>
                </div>
              </div>
              <div id="work-evcompany" class="valid-message"></div>
            </div>
          </div>
          <div class="row up1">
            <div class="col-xs-6">
              <div class="form-label bold"><?=$lang['work-country'];?> *</div>
            </div>
            <div class="col-xs-6">
              <div class="form-label bold"><?=$lang['work-city'];?> *</div>
            </div>
          </div>
          <div class="row">
            <div class="col-xs-6">
              <input id="work-ecountry" type="text" class="form-control no-radius" name="country" placeholder="<?=$lang['work-country_placeholder'];?>">
            </div>
            <div class="col-xs-6">
              <input id="work-ecity" type="text" class="form-control no-radius" name="city" placeholder="<?=$lang['work-city_placeholderi'];?>">
            </div>
            <div class="col-xs-12">
              <div id="work-evcountry" class="valid-message"></div>
            </div>
          </div>
          <div class="row up1">
            <!-- <div class="col-xs-12">
              <div class="form-label">Employment Status</div>
            </div> -->
            <div class="col-sm-6 col-xs-12">
              <!-- <select id="work-estatus" name="" class="form-control no-radius" onchange="checkStat2();">
                <option>Choose Status</option>
                <option value="1" class="work-estatus">Previously Employed</option>
                <option value="2" class="work-estatus">Currently Employed</option>
              </select> -->
              <div class="checkbox">
                <label>
                  <input id="work-estatus" type="checkbox" onclick="checkStat2();" value="yes"> <?=$lang['work-attending'];?>
                </label>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-xs-6 up1">
              <div class="form-label bold"><?=$lang['work-from'];?> *</div>
              <div class="row">
                <div class="col-xs-6" style="padding: 0 5px 0 0;">
                  <select id="work-efmonth" name="" class="form-control no-radius">
                    <option value="" class="work-efmonth">Month</option>
                    <?php $month = '12';
                    for ($months = '1'; $months <= $month; $months++) { ?>
                    <option value="<?php echo $months; ?>" class="work-efmonth"><?php echo date("F", strtotime("$months/12/10")); ?></option>
                    <?php } ?>
                  </select>
                </div>
                <div class="col-xs-6" style="padding: 0 0 0 5px;">
                  <select id="work-efyear" name="" class="form-control no-radius">
                    <option value="" class="work-efyear">--</option>
                    <?php $year = '1950';
                    for ($years = date("Y"); $years >= $year; $years--) { ?>
                    <option value="<?php echo $years; ?>" class="work-efyear"><?php echo $years; ?></option>
                    <?php } ?>
                  </select>
                </div>
              </div>
            </div>
            <div id="to-estat" class="col-xs-6 up1">
              <div class="form-label bold"><?=$lang['work-to'];?> *</div>
              <div class="row">
                <div class="col-xs-6" style="padding: 0 5px 0 0;">
                  <select id="work-etmonth" name="" class="form-control no-radius">
                    <option value="" class="work-etmonth">Month</option>
                    <?php $month = '12';
                    for ($months = '1'; $months <= $month; $months++) { ?>
                    <option value="<?php echo $months; ?>" class="work-etmonth"><?php echo date("F", strtotime("$months/12/10")); ?></option>
                    <?php } ?>
                  </select>
                </div>
                <div class="col-xs-6" style="padding: 0 0 0 5px;">
                  <select id="work-etyear" name="" class="form-control no-radius">
                    <option value="" class="work-etyear">--</option>
                    <?php $year = '1950';
                    for ($years = date("Y"); $years >= $year; $years--) { ?>
                    <option value="<?php echo $years; ?>" class="work-etyear"><?php echo $years; ?></option>
                    <?php } ?>
                  </select>
                </div>
              </div>
            </div>
            <div class="col-xs-12">
              <div id="work-evdate" class="valid-message"></div>
            </div>
          </div>
          <div class="row up1">
            <div class="col-xs-12">
              <div class="form-label"><?=$lang['work-desc'];?></div>
            </div>
            <div class="col-sm-12 col-xs-12">
              <textarea id="work-edesc" name="desc" class="form-control no-radius" rows="3" placeholder="<?=$lang['work-desc_placeholder'];?>"></textarea>
            </div>
          </div>
          <div class="row up1">
            <div class="col-xs-12">
              <input id="edit_work_old_title" type="hidden">
              <input id="edit_work_ATid" type="hidden">
              <a class="btn btn-info" onclick="update_work();" >Save</a><span class="pad05"></span>
              <a id="reset_edit_work" type="reset" class="btn btn-default" onclick="close_edit_work()">Cancel</a>
              <a href="javascript:;" id="delete-work" onclick="delete_work();" class="delete-text">Delete</a>
            </div>
          </div>
        </div>
      </div>
    </div><!-- end edit work -->
  </div> <!-- end work profile -->

<script>

//get company
$('html').click(function() {
  $('#company-list').hide();
  $("#work-company").removeClass("load-input");
});
$('#company-box').click(function(event){
  event.stopPropagation();
  $("#work-company").removeClass("load-input");
});
$('html').click(function() {
  $('#company-listEdit').hide();
  $("#work-ecompany").removeClass("load-input");
});
$('#company-boxEdit').click(function(event){
  event.stopPropagation();
  $("#work-ecompany").removeClass("load-input");
});
function get_company(){
  $("#work-company").addClass("load-input");
  var q = $("#work-company").val();
  var url = "<?=$api['work-autocomplete'];?>"+q;                            
  $.ajax({url: url,success:function(result){
    $("#work-company").removeClass("load-input");
    $('#company-ul').html(replace_company(result.data,q));
  }}); 
}
function get_companyEdit(){
  $("#work-ecompany").addClass("load-input");
  var q = $("#work-ecompany").val();
  var url = "<?=$api['work-autocomplete'];?>"+q;                            
  $.ajax({url: url,success:function(result){
    $("#work-ecompany").removeClass("load-input");
    $('#company-eul').html(replace_companyEdit(result.data,q));
  }}); 
}
function replace_company(datas,text){
  var resultHTML='';

  if(datas != null){
    var obj = datas;    
    for(var i=0;i < obj.length;i++){
      if(obj[i].Company_id != '' && obj[i].Company_title != ''){
        if(obj[i].Country_title !== null ){
          var country = obj[i].Country_title;
        } else {
          var country = '';
        }
        if(obj[i].City_title !== null ){
          var city = obj[i].City_title;
          var coma = ", ";
        } else {
          var city = '';
          var coma = " ";
        }
        resultHTML += "<li class='company-li' id='comp-"+obj[i].Company_id+"' onclick='selectCompany("+obj[i].Company_id+")' data-cp_image='"+obj[i].Company_img+"' data-cp_title='"+obj[i].Company_title+"' data-cp_country='"+country +"' data-cp_city='"+city+"'>";
        if(obj[i].Company_img != ""){
          resultHTML += "<div class='company-image'><img src='<?=$global['img-url'];?>"+obj[i].Company_img+"' alt='image'></div>";
        }else {
          resultHTML += "<div class='company-image'><img src='<?=$placeholder['img-work'];?>' alt='image'></div>";
        }
        resultHTML += "<div class='company-text'>";
        resultHTML += "<div class='company-title'>"+obj[i].Company_title+"</div>";
        resultHTML += "<div class='company-place'>"+country+coma+city+"</div>";
        resultHTML += "</div>";
        resultHTML += "</li>";
      }
    }
  } else {
    resultHTML += "<li class='no-list'><div class='no-list-text'><?=$lang['entry-result'];?> \"<span>"+text+"</span>\" <?=$lang['entry-result_text'];?></div></li>";
  }
  return resultHTML;
}
function replace_companyEdit(datas,text){
  var resultHTML='';

  if(datas != null){
    var obj = datas;    
    for(var i=0;i < obj.length;i++){
      if(obj[i].Company_id != '' && obj[i].Company_title != ''){
        if(obj[i].Country_title !== null ){
          var country = obj[i].Country_title;
        } else {
          var country = '';
        }
        if(obj[i].City_title !== null ){
          var city = obj[i].City_title;
          var coma = ", ";
        } else {
          var city = '';
          var coma = " ";
        }
        resultHTML += "<li class='company-li' id='compe-"+obj[i].Company_id+"' onclick='selectCompanyEdit("+obj[i].Company_id+")' data-cp_eimage='"+obj[i].Company_img+"' data-cp_etitle='"+obj[i].Company_title+"' data-cp_ecountry='"+country +"' data-cp_ecity='"+city+"'>";
        if(obj[i].Company_img != ""){
          resultHTML += "<div class='company-image'><img src='<?=$global['img-url'];?>"+obj[i].Company_img+"' alt='image'></div>";
        }else {
          resultHTML += "<div class='company-image'><img src='<?=$placeholder['img-work'];?>' alt='image'></div>";
        }
        resultHTML += "<div class='company-text'>";
        resultHTML += "<div class='company-title'>"+obj[i].Company_title+"</div>";
        resultHTML += "<div class='company-place'>"+country+coma+city+"</div>";
        resultHTML += "</div>";
        resultHTML += "</li>";
      }
    }
  } else {
    resultHTML += "<li class='no-list'><div class='no-list-text'><?=$lang['entry-result'];?> \"<span>"+text+"</span>\" <?=$lang['entry-result_text'];?></div></li>";
  }
  return resultHTML;
}
function selectCompany(param){
  var title = $('#comp-'+param).data('cp_title');
  var country = $('#comp-'+param).data('cp_country');
  var city = $('#comp-'+param).data('cp_city');
  $("#work-company").removeClass("load-input");
  $('#work-company').val(title);
  $('#work-country').val(country);
  $('#work-city').val(city);
  $('#company-list').hide();
}
function selectCompanyEdit(param){
  var title = $('#compe-'+param).data('cp_etitle');
  var country = $('#compe-'+param).data('cp_ecountry');
  var city = $('#compe-'+param).data('cp_ecity');
  $("#work-ecompany").removeClass("load-input");
  $('#work-ecompany').val(title);
  $('#work-ecountry').val(country);
  $('#work-ecity').val(city);
  $('#company-listEdit').hide();
}
get_work();
function get_work(){
  $('#see-work').addClass('hide');
  $('#load-work').html("<img src='<?=$global['absolute-url'];?>img/load-blue.gif' style='margin:70px 0' />");
  $('#main-work').html('');
  var url = "<?=$api['work-data'];?>&user_id=<?php echo $_SESSION['userData']['id'];?>&auth_code=<?php echo $_SESSION['userData']['auth_code'];?>";                            
  $.ajax({url: url,success:function(result){
    setTimeout(function() {
      $('#load-work').html('');
      $('#main-work').html(replace_work(result.data));
      seeMoreWork();
    }, 1000)
  }}); 
}
function replace_work(datas){
  var resultHTML='';resultNext = "";
  var total = 0;previous = "";user_name = "";link="";
  var d = new Date();strip_string ="";
  var current = d.getTime();
  // console.log(datas);
  if(datas != null){
    var obj = datas;    
    for(var i=0;i < obj.length;i++){
      if(obj[i].ATwork_ID != '' && obj[i].ATwork_title != ''){
        resultHTML += "<div class='row row-work hide hm-work' role='tab'>";
        resultHTML += "<div class='col-xs-12'>";
        resultHTML += "<a href='#wcollapse-"+obj[i].ATwork_ID+"' aria-controls='wcollapse-"+obj[i].ATwork_ID+"' data-parent='#main-work' onclick='edit_box_work("+obj[i].ATwork_ID+");' data-toggle='collapse' aria-expanded='false' id='work-"+obj[i].ATwork_ID+"' class='work-box' data-work_atid='"+obj[i].ATwork_ID+"' data-work_country='"+obj[i].Country_title+"' data-work_city='"+obj[i].City_title+"' data-work_status='"+obj[i].ATwork_current+"' data-work_company='"+obj[i].Company_title+"' data-work_title='"+obj[i].ATwork_title+"' data-work_fmonth='"+obj[i].ATwork_from_month+"' data-work_fyear='"+obj[i].ATwork_from_year+"' data-work_tmonth='"+obj[i].ATwork_to_month+"' data-work_tyear='"+obj[i].ATwork_to_year+"' data-work_desc='"+obj[i].ATwork_desc+"'>";
        resultHTML += "<div class='row'>";
        resultHTML += "<div class='col-md-10 col-xs-9 pad0'>";
        //resultHTML += "<a href='#wcollapse-"+obj[i].ATwork_ID+"' aria-controls='wcollapse-"+obj[i].ATwork_ID+"' data-parent='#main-work' onclick='edit_box_work("+obj[i].ATwork_ID+");' data-toggle='collapse' aria-expanded='false' >";
        resultHTML += "<div class='work-title'>"+obj[i].ATwork_title+" <span class='work-etext edit-button'><i class='glyphicon glyphicon-edit'></i> <span class='hidden-xs'>Edit</span></span></div>";
        resultHTML += "<div class='work-company'>"+obj[i].Company_title+"</div>";
        resultHTML += "<div class='work-location'>"+obj[i].City_title+", "+obj[i].Country_title+"</div>";
        if(obj[i].ATwork_to_year == "" || obj[i].ATwork_to_year == 0){
          var toYear = "Present";
        } else {
          var toYear = obj[i].ATwork_to_year;
        }
        resultHTML += "<div class='work-year-mobile visible-xs'>"+obj[i].ATwork_from_year+" - "+toYear+"</div>";
        //resultHTML += "</a>";
        // if(obj[i].ATwork_desc != ''){
        // resultHTML += "<a id='work-link"+obj[i].ATwork_ID+"' onclick='wshowhide("+obj[i].ATwork_ID+");' data-toggle='collapse' href='#work-more"+obj[i].ATwork_ID+"' aria-expanded='true' aria-controls='work-more1' class='detail-work'>Show Desc</a>";
        // resultHTML += "<div id='work-more"+obj[i].ATwork_ID+"' class='panel-collapse collapse' role='tabpanel'>";
        // resultHTML += "<div class='work-desc'>"+encodesummary(obj[i].ATwork_desc)+"</div>";
        // resultHTML += "</div>";
        // }
        resultHTML += "</div>";
        resultHTML += "<div class='col-md-2 col-xs-3'>";
        //resultHTML += "<a href='#wcollapse-"+obj[i].ATwork_ID+"' aria-controls='wcollapse-"+obj[i].ATwork_ID+"' data-parent='#main-work' onclick='edit_box_work("+obj[i].ATwork_ID+");' data-toggle='collapse' aria-expanded='false' >";
        if(obj[i].Company_img != ''){
          resultHTML += "<div class='work-image'><img src='<?=$global['img-url'];?>"+obj[i].Company_img+"' alt='"+obj[i].Company_title+"'></div>";
        }else {
          resultHTML += "<div class='work-image'><img src='<?=$placeholder['img-work'];?>' alt='image'></div>";
        }
          
        resultHTML += "<div class='work-year hidden-xs'>"+obj[i].ATwork_from_year+" - "+toYear+"</div>";
        //resultHTML += "</a>";
        resultHTML += "</div>";
        resultHTML += "</div>";
        resultHTML += "</a>";
        resultHTML += "<div id='load-work-"+obj[i].ATwork_ID+"' class='text-center'></div>";
        resultHTML += "<a id='close-work-"+obj[i].ATwork_ID+"' href='#wcollapse-"+obj[i].ATwork_ID+"' aria-controls='wcollapse-"+obj[i].ATwork_ID+"' data-parent='#main-work' data-toggle='collapse' aria-expanded='false' class='hide'></a>";
        resultHTML += "</div>";
        resultHTML += "</div>";
        resultHTML += "<div id='wcollapse-"+obj[i].ATwork_ID+"' aria-labelledby='work-"+obj[i].ATwork_ID+"' class='panel-collapse collapse collapse-work pad0' role='tabpanel'></div>";
      }
    }
  } else {
    resultHTML += "<div id='empty-work' class='empty-file'><?=$lang['work-placeholder'];?></div>";
  }
  return resultHTML;
}
function seeMoreWork(){
    size = $(".row-work").size();
    x=2;

    $('.row-work:lt('+x+')').removeClass('hide');
    $('.row-work:lt('+x+')').removeClass('hm-work'); 
    size_left= $(".hm-work").size();
    if(size > 2){
      $('#see-work').removeClass('hide');
      $("#seeWork-num").text('('+size_left+')');
    } else {
      $('#see-work').addClass('hide');
    }

    $('#see-work').on('click', function(){
        $('.row-work').removeClass('hide');
        $('.row-work').removeClass('hm-work');
        $('#see-work').addClass('hide');
    });
};
function wshowhide(param){
  var teks = $('#work-link'+param).text();
  if(teks == 'Hide Desc'){
    $('#work-link'+param).text('Show Desc');
  } else{
    $('#work-link'+param).text('Hide Desc');
  }
}
//clear value add work
function emptyAddWork(){
  $("#work-title").val('');
  $("#work-company").val('');
  $("#work-country").val('');
  $("#work-city").val('');
  $("#work-desc").val('');
  $("#work-fmonth").val('');
  $("#work-fyear").val('');
  $("#work-tmonth").val('');
  $("#work-tyear").val('');
  document.getElementById("work-status").checked = false;
  $("#to-stat").removeClass("hide");
}
//open slide edit
function edit_box_work(param){
  if ($('#slide-work').hasClass('in')){
    $('#slide-work').collapse('hide');
  }
  if($('.work-box').hasClass('hide-work')){
    $('.work-box').removeClass('hide-work');
    if($('.collapse-work').hasClass('in')){
      $('.collapse-work').removeClass('in')
    }
    $('#work-'+param).addClass('hide-work');
  } else {
    $('#work-'+param).addClass('hide-work');
  }
  $('#edit-work').detach().appendTo('#wcollapse-'+param+'');
  var ATid = $('#work-'+param).data('work_atid');
  var title = $('#work-'+param).data('work_title');
  var company = $('#work-'+param).data('work_company');
  var status = $('#work-'+param).data('work_status');
  var country = $('#work-'+param).data('work_country');
  var city = $('#work-'+param).data('work_city');
  var desc = $('#work-'+param).data('work_desc');
  var fmonth = $('#work-'+param).data('work_fmonth');
  var fyear = $('#work-'+param).data('work_fyear');
  var tmonth = $('#work-'+param).data('work_tmonth');
  var tyear = $('#work-'+param).data('work_tyear');

  $('#reset_edit_work').data("target") === "#wcollapse";
  $('#work-etitle').val(title);
  $('#work-ecompany').val(company);
  $('#work-ecountry').val(country);
  $('#work-ecity').val(city);
  $('#work-edesc').val(desc);
  $('#edit_work_old_title').val(title);
  $('#edit_work_ATid').val(ATid);
  $('option[value="' + fmonth + '"].work-efmonth').attr("selected", true);
  $('option[value="' + fyear + '"].work-efyear').attr("selected", true);
  if(status == 'yes') {
    document.getElementById("work-estatus").checked = true;
  } else {
    document.getElementById("work-estatus").checked = false;
    $('option[value="' + tmonth + '"].work-etmonth').attr("selected", true);
    $('option[value="' + tyear + '"].work-etyear').attr("selected", true);
  }
  checkStat2();
}
//close edit work
function close_edit_work(){
  var id = $('#edit_work_ATid').val();
  $('#close-work-'+id+'').click();
  $('#work-'+id).removeClass('hide-work');
}
//hide empty word when add new
function hideEmptyWork(){
  var myElem = document.getElementById('empty-work');
  if(myElem != null){
    $('#empty-work').toggle();
  }
  var id = $('#edit_work_ATid').val();
  if (id != ''){
    if ($('#wcollapse-'+id).hasClass('in')){
      $('#close-work-'+id+'').click();
      $('#work-'+id).removeClass('hide-work');
    }
  }
  emptyAddWork();
}
//update function
function update_work(){
  if( validWorkEdit() ){
    $('#edit-work').detach().appendTo('#edit-work-place');
    var iwork = $('#edit_work_ATid').val();
    $('#close-work-'+iwork).click();
    $('html, body').animate({
      scrollTop: $('#work-header').offset().top-80
    }, 'fast');

    var url = "<?=$api['work-update'];?>";
    var work_id = $("#edit_work_ATid").val();
    var work_title = $("#work-etitle").val();
    var work_company = $("#work-ecompany").val();
    var work_country = $("#work-ecountry").val();
    var work_city = $("#work-ecity").val();
    var work_desc = $("#work-edesc").val();
    var work_fmonth = $("#work-efmonth").val();
    var work_fyear = $("#work-efyear").val();
    var work_tmonth = $("#work-etmonth").val();
    var work_tyear = $("#work-etyear").val();
    var work_status = document.getElementById('work-estatus');
    if (work_status.checked) {
      work_status = 'yes';
    } else {
      work_status = 'no';
    }
    var data = {
      user_id : "<?=$_SESSION['userData']['id'];?>",
      auth_code : "<?=$_SESSION['userData']['auth_code'];?>",
      id : work_id,
      title : work_title,
      city : work_city,
      country : work_country,
      company : work_company,
      current : work_status,
      from_month : work_fmonth,
      from_year : work_fyear,
      to_month : work_tmonth,
      to_year : work_tyear,
      desc : work_desc
    }
    $.ajax({url: url,data : data, success:function(result){
      get_work();
      console.log(result);
    }});
  }
}
//insert function
function save_work(){
  if( validWork() ){
    $('#edit-work').detach().appendTo('#edit-work-place');
    $('html, body').animate({
      scrollTop: $('#work-header').offset().top-80
    }, 'fast');

    var url = "<?=$api['work-update'];?>";
    var work_title = $("#work-title").val();
    var work_company = $("#work-company").val();
    var work_country = $("#work-country").val();
    var work_city = $("#work-city").val();
    var work_desc = $("#work-desc").val();
    var work_fmonth = $("#work-fmonth").val();
    var work_fyear = $("#work-fyear").val();
    var work_tmonth = $("#work-tmonth").val();
    var work_tyear = $("#work-tyear").val();
    var work_status = document.getElementById('work-status');
    if (work_status.checked) {
      work_status = 'yes';
    } else {
      work_status = 'no';
    }
    var data = {
      user_id : "<?=$_SESSION['userData']['id'];?>",
      auth_code : "<?=$_SESSION['userData']['auth_code'];?>",
      title : work_title,
      city : work_city,
      country : work_country,
      company : work_company,
      current : work_status,
      from_month : work_fmonth,
      from_year : work_fyear,
      to_month : work_tmonth,
      to_year : work_tyear,
      desc : work_desc
    }
    $.ajax({url: url,data : data, success:function(result){
      $('#slide-work').collapse('hide');
      get_work();
      emptyAddWork();
      console.log(result);
    }});
  }
}
//delete function
function delete_work(){
  if (confirm("Are you sure to delete this information ?")) {
    $('#edit-work').detach().appendTo('#edit-work-place');
    $('html, body').animate({
      scrollTop: $('#work-header').offset().top-80
    }, 'fast');
    
    var url = "<?=$api['work-delete'];?>";
    var work_id = $("#edit_work_ATid").val();
    var data = {
      user_id : "<?=$_SESSION['userData']['id'];?>",
      auth_code : "<?=$_SESSION['userData']['auth_code'];?>",
      id : work_id
    }
    $.ajax({url: url,data : data, success:function(result){
      get_work();
      console.log(result);
    }});
  }
}
function checkStat() {
var s = document.getElementById('work-status');
  if (s.checked) {
    if($("#to-stat").hasClass("hide")){
          //nothing happen
    } else {
      $("#to-stat").addClass("hide");
    $('option[value=""].work-tmonth').attr("selected", true);
    $('option[value=""].work-tyear').attr("selected", true);
    }
  } else {
    if($("#to-stat").hasClass("hide")){
      $("#to-stat").removeClass("hide");
    }
  }
}
function checkStat2() {
var s = document.getElementById('work-estatus');
  if (s.checked) {
    if($("#to-estat").hasClass("hide")){
          //nothing happen
    } else {
      $("#to-estat").addClass("hide");
    $('option[value=""].work-etmonth').attr("selected", true);
    $('option[value=""].work-etyear').attr("selected", true);
    }
  } else {
    if($("#to-estat").hasClass("hide")){
      $("#to-estat").removeClass("hide");
    }
  }
}
function validWork(){
  var work_title = $("#work-title").val();
  var work_company = $("#work-company").val();
  var work_country = $("#work-country").val();
  var work_city = $("#work-city").val();
  var work_desc = $("#work-desc").val();
  var work_fmonth = $("#work-fmonth").val();
  var work_fyear = $("#work-fyear").val();
  var work_tmonth = $("#work-tmonth").val();
  var work_tyear = $("#work-tyear").val();
  var work_status = document.getElementById('work-status');
  var message = "";

  if(work_title != ""){
    if(work_title.length >= 3){
      message = "";
      $("#work-title").removeClass('input-error');
      $("#work-vtitle").text(message);
    } else {
      message = "Job title has to be at least 3 characters!";
      $("#work-title").addClass('input-error');
      $("#work-title").focus();
      $("#work-vtitle").text(message);
      return false;
    }
  } else {
    message = "Please input your Job Title!";
    $("#work-title").addClass('input-error');
    $("#work-title").focus();
    $("#work-vtitle").text(message);
    return false;
  }
  if(work_company != ""){
    if(work_company.length >= 3){
      message = "";
      $("#work-company").removeClass('input-error');
      $("#work-vcompany").text(message);
    } else {
      message = "Company Name has to be at least 3 characters!";
      $("#work-company").addClass('input-error');
      $("#work-company").focus();
      $("#work-vcompany").text(message);
      return false;
    }
  } else {
    message = "Please input or select your Company name!";
    $("#work-company").addClass('input-error');
    $("#work-company").focus();
    $("#work-vcompany").text(message);
    return false;
  }
  if(work_country != ""){
    message = "";
    $("#work-country").removeClass('input-error');
    $("#work-vcountry").text(message);
  } else {
    message = "Please input Country of Company!";
    $("#work-country").addClass('input-error');
    $("#work-country").focus();
    $("#work-vcountry").text(message);
    return false;
  }
  if(work_city != ""){
    message = "";
    $("#work-city").removeClass('input-error');
    $("#work-vcountry").text(message);
  } else {
    message = "Please input City of Company!";
    $("#work-city").addClass('input-error');
    $("#work-city").focus();
    $("#work-vcountry").text(message);
    return false;
  }
  if (work_status.checked) {
    if(work_fmonth != ""){
      message = "";
      $("#work-fmonth").removeClass('input-error');
      $("#work-vdate").text(message);
    } else {
      message = "Start Month cannot be empty!";
      $("#work-fmonth").addClass('input-error');
      $("#work-fmonth").focus();
      $("#work-vdate").text(message);
      return false;
    }
    if(work_fyear != ""){
      message = "";
      $("#work-fyear").removeClass('input-error');
      $("#work-vdate").text(message);
    } else {
      message = "Start Year cannot be empty!";
      $("#work-fyear").addClass('input-error');
      $("#work-fyear").focus();
      $("#work-vdate").text(message);
      return false;
    }
  } else {
    if(work_fmonth != ""){
      message = "";
      $("#work-fmonth").removeClass('input-error');
      $("#work-vdate").text(message);
    } else {
      message = "Start Month cannot be empty!";
      $("#work-fmonth").addClass('input-error');
      $("#work-fmonth").focus();
      $("#work-vdate").text(message);
      return false;
    }
    if(work_fyear != ""){
      message = "";
      $("#work-fyear").removeClass('input-error');
      $("#work-vdate").text(message);
    } else {
      message = "Start Year cannot be empty!";
      $("#work-fyear").addClass('input-error');
      $("#work-fyear").focus();
      $("#work-vdate").text(message);
      return false;
    }
    if(work_tmonth != ""){
      if(work_tyear == work_fyear){
        if(eval(work_tmonth) >= eval(work_fmonth)){
          message = "";
          $("#work-tmonth").removeClass('input-error');
          $("#work-vdate").text(message);
        } else {
          message = "End Date has to be later than Start Date!";
          $("#work-tmonth").addClass('input-error');
          $("#work-tmonth").focus();
          $("#work-vdate").text(message);
          return false;
        }
      } else {
        message = "";
        $("#work-tmonth").removeClass('input-error');
        $("#work-vdate").text(message);
      }
    } else {
      message = "End Month cannot be empty!";
      $("#work-tmonth").addClass('input-error');
      $("#work-tmonth").focus();
      $("#work-vdate").text(message);
      return false;
    }
    if(work_tyear != ""){
      if(work_tyear >= work_fyear){
        message = "";
        $("#work-tyear").removeClass('input-error');
        $("#work-vdate").text(message);
      } else {
        message = "End Date has to be later than Start Date!";
        $("#work-tyear").addClass('input-error');
        $("#work-tyear").focus();
        $("#work-vdate").text(message);
        return false;
      } 
    } else {
      message = "End Year cannot be empty!";
      $("#work-tyear").addClass('input-error');
      $("#work-tyear").focus();
      $("#work-vdate").text(message);
      return false;
    }
  }
  return true;
}
function validWorkEdit(){
  var work_title = $("#work-etitle").val();
  var work_company = $("#work-ecompany").val();
  var work_country = $("#work-ecountry").val();
  var work_city = $("#work-ecity").val();
  var work_desc = $("#work-edesc").val();
  var work_fmonth = $("#work-efmonth").val();
  var work_fyear = $("#work-efyear").val();
  var work_tmonth = $("#work-etmonth").val();
  var work_tyear = $("#work-etyear").val();
  var work_status = document.getElementById('work-estatus');
  var message = "";

  if(work_title != ""){
    if(work_title.length >= 3){
      message = "";
      $("#work-etitle").removeClass('input-error');
      $("#work-evtitle").text(message);
    } else {
      message = "Job title has to be at least 3 characters!";
      $("#work-etitle").addClass('input-error');
      $("#work-etitle").focus();
      $("#work-evtitle").text(message);
      return false;
    }
  } else {
    message = "Please input your Job Title!";
    $("#work-etitle").addClass('input-error');
    $("#work-etitle").focus();
    $("#work-evtitle").text(message);
    return false;
  }
  if(work_company != ""){
    if(work_company.length >= 3){
      message = "";
      $("#work-ecompany").removeClass('input-error');
      $("#work-evcompany").text(message);
    } else {
      message = "Company Name has to be at least 3 characters!";
      $("#work-ecompany").addClass('input-error');
      $("#work-ecompany").focus();
      $("#work-evcompany").text(message);
      return false;
    }
  } else {
    message = "Please input or select your Company name!";
    $("#work-ecompany").addClass('input-error');
    $("#work-ecompany").focus();
    $("#work-evcompany").text(message);
    return false;
  }
  if(work_country != ""){
    message = "";
    $("#work-ecountry").removeClass('input-error');
    $("#work-evcountry").text(message);
  } else {
    message = "Please input Country of Company!";
    $("#work-ecountry").addClass('input-error');
    $("#work-ecountry").focus();
    $("#work-evcountry").text(message);
    return false;
  }
  if(work_city != ""){
    message = "";
    $("#work-ecity").removeClass('input-error');
    $("#work-evcountry").text(message);
  } else {
    message = "Please input City of Company!";
    $("#work-ecity").addClass('input-error');
    $("#work-ecity").focus();
    $("#work-evcountry").text(message);
    return false;
  }
  if (work_status.checked) {
    if(work_fmonth != ""){
      message = "";
      $("#work-efmonth").removeClass('input-error');
      $("#work-evdate").text(message);
    } else {
      message = "Start Month cannot be empty!";
      $("#work-efmonth").addClass('input-error');
      $("#work-efmonth").focus();
      $("#work-evdate").text(message);
      return false;
    }
    if(work_fyear != ""){
      message = "";
      $("#work-efyear").removeClass('input-error');
      $("#work-evdate").text(message);
    } else {
      message = "Start Year cannot be empty!";
      $("#work-efyear").addClass('input-error');
      $("#work-efyear").focus();
      $("#work-evdate").text(message);
      return false;
    }
  } else {
    if(work_fmonth != ""){
      message = "";
      $("#work-efmonth").removeClass('input-error');
      $("#work-evdate").text(message);
    } else {
      message = "Start Month cannot be empty!";
      $("#work-efmonth").addClass('input-error');
      $("#work-efmonth").focus();
      $("#work-evdate").text(message);
      return false;
    }
    if(work_fyear != ""){
      message = "";
      $("#work-efyear").removeClass('input-error');
      $("#work-evdate").text(message);
    } else {
      message = "Start Year cannot be empty!";
      $("#work-efyear").addClass('input-error');
      $("#work-efyear").focus();
      $("#work-evdate").text(message);
      return false;
    }
    if(work_tmonth != ""){
      if(work_tyear == work_fyear){
        if(eval(work_tmonth) >= eval(work_fmonth)){
          message = "";
          $("#work-etmonth").removeClass('input-error');
          $("#work-evdate").text(message);
        } else {
          message = "End Date has to be later than Start Date!";
          $("#work-etmonth").addClass('input-error');
          $("#work-etmonth").focus();
          $("#work-evdate").text(message);
          return false;
        }
      } else {
        message = "";
        $("#work-etmonth").removeClass('input-error');
        $("#work-evdate").text(message);
      }
    } else {
      message = "End Month cannot be empty!";
      $("#work-etmonth").addClass('input-error');
      $("#work-etmonth").focus();
      $("#work-evdate").text(message);
      return false;
    }
    if(work_tyear != ""){
      if(work_tyear >= work_fyear){
        message = "";
        $("#work-etyear").removeClass('input-error');
        $("#work-evdate").text(message);
      } else {
        message = "End Date has to be later than Start Date!";
        $("#work-etyear").addClass('input-error');
        $("#work-etyear").focus();
        $("#work-evdate").text(message);
        return false;
      } 
    } else {
      message = "End Year cannot be empty!";
      $("#work-etyear").addClass('input-error');
      $("#work-etyear").focus();
      $("#work-evdate").text(message);
      return false;
    }
  }
  return true;
}
</script>
</div> <!-- end personal work section -->