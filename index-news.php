<div id="index-news-content">
    <div class="container container-max">
        <div class="row">
            <div class="col-xs-12">
                <div class="popular-profile-header"><?=$lang['home-news'];?></div>
                <div class="popular-profile-header-child"><?=$lang['home-news_note'];?></div>
            </div>
        </div>
        <div class="row row-index-news">
            <?php foreach($data_news as $dataNews){ ?>
            <div class="col-sm-3 col-xs-6 col-index-news">
                <div class="inews-content">
                    <a href="<?php echo $path['blog'].$dataNews['News_ID']."_".encode($dataNews['News_title']).".html";?>" class="inews-img">
                        <img src="<?php echo $global['base'].'admin/'.$dataNews['Np_img_thmb'];?>" alt="<?php echo correctDisplay($dataNews['News_title']);?>">
                    </a>
                    <div class="inews-title">
                        <a href="<?php echo $path['blog'].$dataNews['News_ID']."_".encode($dataNews['News_title']).".html";?>" title="<?php echo correctDisplay($dataNews['News_title']);?>">
                            <?php echo charLength(correctDisplay($dataNews['News_title']),50);?>
                        </a>
                    </div>
                    <div class="inews-date"><?php echo relative_time($dataNews['News_create_date']);?></div>
                </div>
            </div>
            <?php } ?>
        </div>
        <div class="index-news-more"><a href="<?=$path['blog'];?>"><?=$lang['home-news_more'];?> <i class="glyphicon glyphicon-chevron-right"></i></a></div>
    </div>
</div>