<?php 
include("packages/require.php");
include("controller/controller_search.php");

$curpage='search';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title><?=$global['title-website'];?></title>
    <meta name="keywords" content="">
    <meta name="description" content="">
    <?php include("packages/head.php");?>
</head>
<body>
    <div id="all">
        <!-- start top nav -->
     
        <?php include("section-top-nav.php");?>
        <!-- end top nav -->

        <div id="search-header-section">
            <div class="up6 visible-xs"></div>
            <div class="search-header-layer">
                <form action="search1.php?action=search" method="GET">
                <br>
                <div class="row hide">
                    <div class="col-xs-12 col-hsearch">
                        <input id="input-qsearch" type="text" name="q" class="form-control input-nav-search" placeholder="keyword" value="<?php echo $O_keyword;?>">
                    </div>
                    <div class="col-xs-12 col-hsearch">
                        <button id="submit-search" type="submit" class="btn btn-info btn-nav-search">Search</button>
                    </div>
                </div>
                <div class="row hidden-xs">
                    <div class="col-xs-12 col-hsearch">
                        <div class="hsearch-title">Location</div>
                        <select multiple="multiple" type="text" class="js-example-basic-multiple form-control hsearch-input tags-location" name="location">
                           <?php foreach($data_citys as $data_city){ ?>
                            <option <?php if($O_location == $data_city['City_ID']){ echo "selected=selected"; }?> value="<?php echo $data_city['City_ID'];?>"><?php echo $data_city['City_title'];?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="col-xs-12 col-hsearch">
                        <div class="hsearch-title">Expertise</div>
                        <select multiple="multiple" type="text" class="js-example-basic-multiple form-control hsearch-input tags-expertise" name="expertise">
                            <?php foreach($data_levels as $data_level){ ?>
                            <option <?php if($O_expertise == $data_level['Level_ID']){ echo "selected=selected"; }?> value="<?php echo $data_level['Level_ID'];?>"><?php echo $data_level['Level_title'];?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="col-xs-12 col-hsearch">
                        <div class="hsearch-title">Job Function</div>
                        <select multiple="multiple" type="text" class="js-example-basic-multiple form-control hsearch-input tags-function" name="function">
                            <?php foreach($data_fields as $data_field){ ?>
                            <option <?php if($O_function == $data_field['Field_ID']){ echo "selected=selected"; }?> value="<?php echo $data_field['Field_ID'];?>"><?php echo $data_field['Field_parent']." - ".$data_field['Field_title'];?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="col-xs-12 col-hsearch">
                        <div class="hsearch-title">Industry</div>
                        <select multiple="multiple" type="text" class="js-example-basic-multiple form-control hsearch-input tags-industry" name="industry">
                            <?php foreach($data_industrys as $data_industry){ ?>
                            <option <?php if($O_industry == $data_industry['Industry_ID']){ echo "selected=selected"; }?> value="<?php echo $data_industry['Industry_ID'];?>"><?php echo $data_industry['Industry_title'];?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="col-xs-12 col-hsearch">
                        <div class="hsearch-title">Experience</div>
                        <select type="text" class="form-control hsearch-input" style="font-size:12px;" name="experience">
                            <option value="">all experience</option>
                            <option <?php if ($O_exp == "1"){echo "selected=selected";}?> value="1">1 years</option>
                            <option <?php if ($O_exp == "2"){echo "selected=selected";}?> value="2">2 years</option>
                            <option <?php if ($O_exp == "3"){echo "selected=selected";}?> value="3">3 years</option>
                            <option <?php if ($O_exp == "4"){echo "selected=selected";}?> value="4">4 years</option>
                            <option <?php if ($O_exp == "5"){echo "selected=selected";}?> value="5">5 years</option>
                        </select>
                    </div>
                </div>
                </form>
                <div class="row hidden-xs">
                    <div class="col-sm-9 col-xs-12">
                        <div class="recent-list">
                            <span class="hrecent-list">Recent Search</span>
                            <?php if(is_array($data_recents)){
                            foreach($data_recents AS $data_recent){?>
                                <a href="search1.php?q=<?php echo $data_recent['Qj_keyword'];?>"><?php echo $data_recent['Qj_keyword'];?></a>
                            <?php }};?>
                        </div>
                    </div>
                    <div class="col-sm-3 col-xs-12">
                        <div class="search-options">
                            <span class="drop-setting">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">More Options <span class="caret"></span></a>
                                <ul class="dropdown-menu dropdown-setting" role="menu">
                                    <li><a href="#">Setting</a></li>
                                </ul>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="row visible-xs">
                    <div class="col-xs-12">
                        <div class="mobile-btn-content down3">
                            <a href="#" class="btn-mobile-search" onclick="searchSlideTag();">
                                <div class="input-mobile-search">search for job title, skills, or keywords</div>
                                <div class="icon-search"><span class="glyphicon glyphicon-search"></span></div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div><!--  end header section -->
        <div id="search-section">
            <div class="row">
                <div class="col-xs-12">
                    <?php 
                    if(is_array($data_results)){
                    ?>
                        <div class="search-result">
                            Showing <strong><?php echo $total_result;?> jobs</strong> 
                            <?php if($O_company != ""){echo "for <strong>\"".$data_results[0]['Company_title']."\"</strong>";}?> 
                            <?php if($O_keyword != "" || $O_location != "" || $O_expertise != "" || $O_function != "" || $O_industry != "" || $O_exp != ""){echo "for";}?> 
                            <?php if($O_keyword != ""){echo "<strong>\"".$O_keyword."\"</strong>";}?>
                            <?php if($O_keyword != "" && $O_location != ""){echo "in";}?>
                            <?php if($O_location != ""){echo "<strong>\"".$data_results[0]['City_title']."\"</strong>";}?>
                            <?php if($O_location != "" && $O_expertise != "" || $O_function != "" || $O_industry != "" || $O_exp != ""){echo "with";}?> 
                            <?php if($O_expertise != ""){echo "<strong>\"".$data_results[0]['Level_title']."\"</strong> job expertise";}?> 
                            <?php if($O_function != "" || $O_industry != "" || $O_exp != ""){echo ",";}?>
                            <?php if($O_function != ""){echo "<strong>\"".$data_results[0]['Field_title']."\"</strong> job function";}?> 
                            <?php if($O_industry != "" || $O_exp != ""){echo ",";}?>
                            <?php if($O_industry != ""){echo "<strong>\"".$O_industry."\"</strong> industry";}?> 
                            <?php if($O_exp != ""){echo ",";}?>
                            <?php if($O_exp != ""){echo "<strong>\"".$O_exp."\" years </strong> experience";}?>
                        </div>
                    <?php 
                    }else{?>
                        <div class="search-result">
                            <strong><?php if($O_keyword != ""){echo "No search for a \"".$O_keyword ."\"";}else{echo "No search for a jobs";}?></strong>
                        </div>
                    <?php };?>
                    <div class="drop-recent">
                        <select name="sort" class="text" onChange="window.location.href=this.options[this.selectedIndex].value">
                            <option value="search1.php?q=<?php echo $O_keyword;?>&location=<?php echo $O_location;?>&expertise=<?php echo $O_expertise;?>&function=<?php echo $O_function;?>&industry=<?php echo $O_industry;?>&experience=<?php echo $O_exp;?>&sort=recent" <?php if($sort == 'recent'){echo "selected=selected";}?>>Recently Added</option>
                            <option value="search1.php?q=<?php echo $O_keyword;?>&location=<?php echo $O_location;?>&expertise=<?php echo $O_expertise;?>&function=<?php echo $O_function;?>&industry=<?php echo $O_industry;?>&experience=<?php echo $O_exp;?>&sort=weight" <?php if($sort == 'weight'){echo "selected=selected";}?>>Relevance</option>
                            <option value="search1.php?q=<?php echo $O_keyword;?>&location=<?php echo $O_location;?>&expertise=<?php echo $O_expertise;?>&function=<?php echo $O_function;?>&industry=<?php echo $O_industry;?>&experience=<?php echo $O_exp;?>&sort=mostview" <?php if($sort == 'mostview'){echo "selected=selected";}?>>Most Viewed Jobs</option>
                            <option value="#">Most Popular Jobs</option>
                            <option value="search1.php?q=<?php echo $O_keyword;?>&location=<?php echo $O_location;?>&expertise=<?php echo $O_expertise;?>&function=<?php echo $O_function;?>&industry=<?php echo $O_industry;?>&experience=<?php echo $O_exp;?>&sort=leastview" <?php if($sort == 'leastview'){echo "selected=selected";}?>>Least Viewed Jobs</option>
                            <option value="#">Least Popular Jobs</option>
                        </select>
                        <!--<a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">Recently Added <span class="caret"></span></a>
                        <ul class="dropdown-menu dropdown-recent" role="menu">
                            <li><a href="#">Setting</a></li>
                        </ul>!-->
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4 col-xs-12">
                    <div class="job-slist">
                        <div id="job-list">
                        </div>
                        <button id="load-latest" class="btn btn-load-more"><i class='glyphicon glyphicon-repeat'></i> Load More</button> 
                    </div>      
                </div>
                <div class="col-sm-8 col-xs-12 hidden-xs">
                    <div class="search-content">
                        <div class="job-stitle"><?php echo strtoupper($O_keyword) ?></div>
                        <div class="job-sdesc">
                            <?php if($J_content != ""){ echo "<div align='justify'>$J_content</div>";}?>
                        </div>
                        <a href="#" class="banner-search"><img src="<?=$global['absolute-url'];?>img/banner.png" alt="banner"></a>
                    </div>
                </div>
            </div>
        </div>
        <?php //}?>
        <?php //}?>
        <div class="up3"></div>
        <!-- start footer section -->
        <?php include("section-footer.php");?>
        <!-- end footer section -->
    </div><!--  end all div -->
    <!-- search modal -->
    <div id="search-modal-tag">
        <div class="ss-header">
            <div class="ss-htext">Search</div>
            <a href="javascript:;" class="ss-close" onclick="searchSlideTag();"><span class="glyphicon glyphicon-remove"></span></a>
        </div>
        <form action="search1.php?action=search" method="GET">
        <div class="ss-layer">
            <div class="row">
                <div class="col-xs-12 col-hsearch">
                    <div class="hsearch-title">Keyword</div>
                    <input type="text" name="q" class="js-example-basic-multiple form-control hsearch-input" value="<?php echo $O_keyword;?>" placeholder="keyword">
                </div>
                <div class="col-xs-12 col-hsearch">
                    <div class="hsearch-title">Location</div>
                    <select multiple="multiple" type="text" class="js-example-basic-multiple form-control hsearch-input tags-location" name="location">
                        <?php foreach($data_citys as $data_city){ ?>
                        <option <?php if($O_location == $data_city['City_ID']){ echo "selected=selected"; }?> value="<?php echo $data_city['City_ID'];?>"><?php echo $data_city['City_title'];?></option>
                        <?php } ?>
                    </select>
                </div>
                <div class="col-xs-12 col-hsearch">
                    <div class="hsearch-title">Expertise</div>
                    <select multiple="multiple" type="text" class="js-example-basic-multiple form-control hsearch-input tags-expertise" name="expertise">
                        <?php foreach($data_levels as $data_level){ ?>
                        <option <?php if($O_expertise == $data_level['Level_ID']){ echo "selected=selected"; }?> value="<?php echo $data_level['Level_ID'];?>"><?php echo $data_level['Level_title'];?></option>
                        <?php } ?>
                    </select>
                </div>
                <div class="col-xs-12 col-hsearch">
                    <div class="hsearch-title">Job Function</div>
                    <select multiple="multiple" type="text" class="js-example-basic-multiple form-control hsearch-input tags-function" name="function">
                        <?php foreach($data_fields as $data_field){ ?>
                        <option <?php if($O_function == $data_field['Field_ID']){ echo "selected=selected"; }?> value="<?php echo $data_field['Field_ID'];?>"><?php echo $data_field['Field_parent']." - ".$data_field['Field_title'];?></option>
                        <?php } ?>
                    </select>
                </div>
                <div class="col-xs-12 col-hsearch">
                    <div class="hsearch-title">Industry</div>
                    <select multiple="multiple" type="text" class="js-example-basic-multiple form-control hsearch-input tags-industry" name="industry">
                        <?php foreach($data_industrys as $data_industry){ ?>
                        <option <?php if($O_industry == $data_industry['Industry_ID']){ echo "selected=selected"; }?> value="<?php echo $data_industry['Industry_ID'];?>"><?php echo $data_industry['Industry_title'];?></option>
                        <?php } ?>
                    </select>
                </div>
                <div class="col-xs-12 col-hsearch">
                    <div class="hsearch-title">Experience</div>
                    <select type="text" class="form-control hsearch-input" style="font-size:12px;" name="experience">
                        <option value="">all experience</option>
                        <option <?php if ($O_exp == "1"){echo "selected=selected";}?> value="1">1 years</option>
                        <option <?php if ($O_exp == "2"){echo "selected=selected";}?> value="2">2 years</option>
                        <option <?php if ($O_exp == "3"){echo "selected=selected";}?> value="3">3 years</option>
                        <option <?php if ($O_exp == "4"){echo "selected=selected";}?> value="4">4 years</option>
                        <option <?php if ($O_exp == "5"){echo "selected=selected";}?> value="5">5 years</option>
                    </select>
                </div>
            </div>
            <div class="row up2">
                <div class="col-xs-12">
                    <button name="submit" type="submit" class="btn btn-block btn-info btn-search" style="width:100%;">Search</button>
                </div>
            </div>
        </div>
        </form>
    </div><!--  end search modal -->
    <script src="<?php echo $global['absolute-url'];?>js/function.js"></script>
    <script type="text/javascript">
    function keywordValue(){
        var key = $('#input-nav-qsearch').val();
        $('#input-qsearch').val(key);
    }
    $('#input-nav-qsearch').keypress(function(e) {
        if(e.which == 13) {
            jQuery(this).blur();
            jQuery('#submit-search').focus().click();
        }
    });
    $('.btn-nav-search').keypress(function(e) {
        if(e.which == 13) {
            jQuery(this).blur();
            jQuery('#submit-search').focus().click();
        }
    });
        function searchSlideTag() {
            $('#search-modal-tag').fadeToggle(300);
        };
        $(".tags-location").select2({
          placeholder: "all location"
      });
        $(".tags-expertise").select2({
          placeholder: "all expertise"
      });
        $(".tags-function").select2({
          placeholder: "all job function"
      });
        $(".tags-industry").select2({
          placeholder: "all industry"
      });

        var count = 1;
        get_latests(1);
        $('#load-latest').click(function(){
          $('#load-latest').html("");
          $('#load-latest').html("<img src='<?php echo $global['absolute-url'];?>img/loadmores.gif' alt='loading' style='width:20px;''> Loading....");
          setTimeout(function() { 
              get_latests(count); 
          }, 300)
          count++;
        });
        function get_latests(){
            setTimeout(function() {
                var url = "<?=$api['job-result'];?>"+count+"&q=<?=$O_keyword;?>&location=<?=$O_location;?>&expertise=<?=$O_expertise;?>&function=<?=$O_function;?>&industry=<?=$O_industry;?>&expertise=<?=$O_exp;?>&sort=<?=$sort;?>";                            
                $.ajax({url: url,success:function(result){
                 
                    $('#job-list').append(replace_data(result.data));
                    $('#load-latest').html("<i class='glyphicon glyphicon-repeat'></i> Load More");
                }});  
            }, 1000)
        }

        function replace_data(datas){
        var resultHTML='';resultNext = "";
        var total = 0;previous = "";user_name = "";link="";
        var d = new Date();strip_string ="";
        var current = d.getTime();
        // console.log(current);
        if(datas != null){
          var obj = datas;    

          for(var i=0;i < obj.length;i++){

            if(obj[i].Jobs_ID != '' && obj[i].Jobs_title != ''){
                previous = new Date(obj[i].Jobs_create_date).getTime();
              var jobsTitle = obj[i].Jobs_title;
              var regex = /\\/g;
              var replaceTitle = jobsTitle.replace(regex, "").replace(/&quot;/g, "");
              var lowercase = replaceTitle.toLowerCase();
              var encodeTitle = lowercase.replace(/\s+/g, '-');

                link = "<?=$path['job-detail'];?>"+encodeTitle+"_"+obj[i].Jobs_ID+".html";

                resultHTML += "<div class='job-sbox'>";
                resultHTML += "<a href='"+link+"' class='list-stitle'>"+jobsTitle+"</a>";
                resultHTML += "<div class='list-scompany'>"+obj[i].Company_title+"</div>";
                resultHTML += "<div class='list-saddress'><img src='<?php echo $global['absolute-url'];?>img/map.png' alt='map'>"+obj[i].City_title+"</div>";
                resultHTML += "<div class='list-stime'><img src='<?php echo $global['absolute-url'];?>img/clock.png' alt='map'>"+timeDifference(current,previous)+"</div>";
                //resultHTML += "<div class='list-stime'><img src='<?php echo $global['absolute-url'];?>img/clock.png' alt='map'>"+obj[i].Jobs_create_date+"</div>";
                resultHTML += "<a href='#' class='search-bookmark'><span class='glyphicon glyphicon-star'></span></a>";
                resultHTML += "<div align='right' class='list-stime'><span class='glyphicon glyphicon-eye-open'></span> "+obj[i].counter+" views</div>"
                resultHTML += "</div>";
              }
            }
          }
          else{
                $('#load-latest').addClass('hidden');
                /*resultHTML += "<div class='job-sbox'>";
                resultHTML += "<div class='list-scompany'>No jobs title</div>";
                resultHTML += "<div class='list-saddress'><img src='<?php echo $global['absolute-url'];?>img/map.png' alt='map'>No city</div>";
                resultHTML += "<div class='list-stime'><img src='<?php echo $global['absolute-url'];?>img/clock.png' alt='map'>No time</div>";
                resultHTML += "<a href='#' class='search-bookmark'><span class='glyphicon glyphicon-star'></span></a>";
                resultHTML += "<div align='right' class='list-stime'><span class='glyphicon glyphicon-eye-open'></span> No view</div>"
                resultHTML += "</div>";*/
              }
          return resultHTML;
        };
    </script>
</body>
</html>
