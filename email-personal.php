<html>
<head>
</head>
<body style="margin:0;">
<div id='all' style="width:600px;">
<div id='header' style="text-align:center;padding:20px 0;border-bottom: solid 2px #B5B5B5;margin-bottom: 30px;">
<a target='_blank' href='http://www.civimi.com'> 
<img src='http://www.civimi.com/beta/civimi/img/logo/logo-desktop.png' style='width:200px;' alt='civimi'>
</a>
</div>
<div id="content" style="padding: 0 15px;">
Hallo ".$username."!<br><br>
Terima kasih sudah bergabung dengan Civimi.com.<br><br>
Perkenalkan nama saya Billy dari <a target='_blank' href='http://www.civimi.com/'>civimi.com</a> ingin membantu ".$username." dan rekan lainnya untuk memperoleh kemudahan dalam membuat dan menyimpan biodata diri dalam bentuk CV.<br><br>
Civimi.com penyedia layanan CV Online Gratis menyediakan berbagai keuntungan seperti :
<li><b>Kemudahan menyimpan data seperti CV dan Portofolio,</b> dibandingkan dengan menyimpan pada flash disk dan berkemungkinan tinggi dapat hilang dan file corrupt.</li>
<li><b>Panduan mudah</b> untuk mengisi CV dan Portfolio karena format sudah tersedia, Anda tidak perlu mencari format CV lagi</li>
<li><b>PDF Ready.</b> CV dan Portfolio yang Anda buat dapat langsung disimpan bahkan di print langsung.</li>
<li><b>GRATIS</b> tanpa biaya, Anda hanya perlu <i>sign up</i> saja.</li>
<li><b>Mudah diakses dimana saja</b> (Komputer, Tablet, Smartphone) dibandingkan dengan Anda menyimpan di komputer pribadi atau mencari warnet untuk menemukan CV Anda.</li>
<li><b>Anda dapat ditemukan di Google dan Yahoo.</b></li>
<br><br>
Lengkapi data CV dan Portfolio ".$username.", dan nantikan sharing dan tips dari saya.
<br><br><br>
Klik untuk <a target='_blank' href='http://www.civimi.com/beta/edit/'>Lengkapi Profile Anda Sekarang juga!!</a><br><br>
Kritik dan saran sangatlah kami hargai. Oleh karena itu, jangan sungkan untuk menghubungi kami di email pribadi saya atau customer support kami di support@civimi.com<br><br>
Terima kasih telah menggunakan layanan <a target='_blank' href='http://www.civimi.com/'>civimi.com!</a><br><br>  
Regards,<br><br>
Billy<br>
Customer Success Manager<br>
Civimi.com
</div>
<div id='footer' style='background-color: #666;padding: 10px 0;margin-top:20px;'>
<div style='display: block;height: 50px;padding: 0 20px;position: relative;'>
<div style='font-size: 20px;font-style: italic;color: #FFFFFF;font-weight: bold;display: block;width: 50%;float: left;'>'Reinvent Yourself'</div>
<div style='display: block;float: left;width: 50%;position: relative;text-align: right;'>
<a target='_blank' href='https://www.facebook.com/civimi'><img src='http://www.civimi.com/beta/civimi/img/logo/facebook.png' alt='facebook' style='width: 20px;'></a>
<a target='_blank' href='https://plus.google.com/+Civimi'><img src='http://www.civimi.com/beta/civimi/img/logo/google.png' alt='google' style='width: 20px;margin-left:8px;'></a>
<a target='_blank' href='https://twitter.com/civimi'><img src='http://www.civimi.com/beta/civimi/img/logo/twitter.png' alt='twitter' style='width: 20px;margin-left:8px;'></a>
</div>
</div>
<div style='text-align:center;font-size: 13px;color: #FFFFFF;margin:20px 0 0 0;padding:0 20px;position:relative;'>
Copyright &copy; 2015 Civimi. All rights reserved.
<div style='position: absolute;right: 20px;top: 0;'><a target='_blank' href="" style="font-size:12px;color: #FFFFFF;outline:none !important;text-decoration:underline;">unsubscribe</a></div>
</div>
</div>
</div>
</body>
</html>
