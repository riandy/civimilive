<a href="#" class="back-top"><img src="<?php echo $global['absolute-url'];?>img/back-top.png" alt="back-top"></a>
    <script>            
    jQuery(document).ready(function() {
      var offset = 220;
      var duration = 500;
      jQuery(window).scroll(function() {
        if (jQuery(this).scrollTop() > offset) {
          jQuery('.back-top').fadeIn(duration);
        } else {
          jQuery('.back-top').fadeOut(duration);
        }
      });
   
      jQuery('.back-top').click(function(event) {
        event.preventDefault();
        jQuery('html, body').animate({scrollTop: 0}, duration);
        return false;
      })
    });
  </script>