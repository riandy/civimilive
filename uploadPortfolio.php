<?php
session_start();
header("Access-Control-Allow-Origin: *");
header('Content-type: application/json');
require_once('packages/SimpleImage.php');

require_once("model/Connection.php");
$obj_con = new Connection();

require_once("model/Photo.php"); 
$obj_photo = new Photo();

//$id = $_SESSION['userData']['id'];
//$auth_code = $_SESSION['userData']['auth_code'];
$album_id = $_GET['albumID'];

$ds = DIRECTORY_SEPARATOR;

$dir = 'image/photos';
$dirThmb = 'image/thmb-photos';

if (!empty($_FILES)) {
	$tempFile = $_FILES['file']['tmp_name'];

	$targetPath = $dir . $ds;
	$targetPathThmb = $dirThmb . $ds;

	$file = $_FILES['file']['name'];
	$ext = pathinfo($file, PATHINFO_EXTENSION);

	$fn = 'img-'.time().rand(0,10).'.jpg';

	$targetFile = $targetPath. $fn;
	if(move_uploaded_file($tempFile,$targetFile))
	{
	$image = new SimpleImage();
	$image->load($targetPath. $fn);
	$image->resize(240,240);
	$image->save($targetPathThmb.'thumb-'.$fn);
	}
	$array = array("img"=>$targetPath.$fn,"thumb"=>$targetPathThmb.'thumb-'.$fn);
	echo json_encode($array);

	$photo = $targetPath.$fn; //real size photo
	$photoThmb = $targetPathThmb.'thumb-'.$fn; //thumbnail size photo

	// save file here
	$obj_con->up();

	$result = $obj_photo->insert_data_photo($photo, $photoThmb, $album_id, "Publish");
	
	$obj_con->down();
	//end save file here
}
?>