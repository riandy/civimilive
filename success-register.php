<?php 
include("packages/civ_require.php");
include("controller/controller_civ_global.php");
include("controller/controller_civ_success_register.php");
$curpage='success_register';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title><?=$seo['title-success_register'];?></title>
    <meta name="keywords" content="<?=$seo['keyword-success_register'];?>">
    <meta name="description" content="<?=$seo['desc-success_register'];?>">
    <?php include("packages/civ_head.php");?>
</head>
<body>
    <!-- HEADER -->
    <?php include("parts/part-header.php");?>
    <!-- END HEADER -->
    
    <div class="sregister-part">
        <div class="container container-max">
            <?php if($userEmail != "" && $userUsername != "" && $userCode != ""){?>
            <div class="row">
                <div class="col-xs-12">
                    <div class="sregister-head">CONGRATULATIONS!</div> 
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <div class="sregister-content">
                        <div class="thumbs-image">
                            <img src="<?=$global['img-url'];?>img/thumbs-up.png" class="sregister-img" alt="success">
                        </div>
                        <div class="sregister-note">
                            You have successfully registered in CIVIMI. <br/>To Fully utilize our service, you will need to open your <?=$userEmail;?> 's Inbox (please check Spam folder as well!).<br/>
                            Then click "Activate Account" button<br/><br/>
                            <i>Didn't get your email? Click <a href="javascript:void(0)" onclick="return resendConfirmation()">Resend Confirmation E-mail</a> or refresh page</i>
                        </div>
                    </div>
                </div>
            </div>
            <?php } else { ?>
            <div class="row">
                <div class="col-xs-12">
                    <div class="sregister-notconnect">Not Connected!</div> 
                </div>
            </div>
            <?php } ?>
        </div>
    </div>

    <!-- START PART FOOTER -->
    <?php include("parts/part-footer.php");?>
    <!-- END PART FOOTER --> 
</body>
</html>