<?php 
include("packages/require.php");
include("controller/controller_global.php");
$curpage='notif';
?>
<html>
<head>
</head>
<body style="margin:0;">
<div id='all' style="width:600px;margin:0 auto;">
<div id="notif-content" style="border: solid 1px #B5B5B5;">
<div id='header' style="text-align: left;padding: 10px;border-bottom: solid 2px #B5B5B5;margin-bottom: 30px;">
<a target='_blank' href='http://www.civimi.com'> 
<img src='http://www.civimi.com/img/logo/logo-desktop.png' style='width:150px;' alt='civimi'>
</a>
</div>
<div id="content">
<div style="padding: 0 50px;">
<div style="display: inline-block;width: 70%;vertical-align: top;">
<div style="font-weight: bold;color: #5A5A5A;font-size: 16px;"><?=$lang['card-welcome'];?> Name</div>
<div style="font-weight: bold;color: #5A5A5A;font-size: 16px;margin: 5px 0 0 0;"><?=$lang['card-welcome_text'];?> Bali <?=$lang['card-welcome_text2'];?></div>
</div>
<div style="display: inline-block;width: 30%;margin-left: -4px;">
<div style="text-align: right;"><img src="http://www.civimi.com/img/notif-heart.png" style="width: 70px;" alt="civimi"></div>
</div>
</div>
<div id="notif-profil" style="margin: 30px 100px;border: solid 1px #B5B5B5;padding-bottom: 10px;">
<div style="padding: 0 10px;margin: 10px 0 0 0;">
<div style="display: table-cell;vertical-align: top;"><img src="http://www.civimi.com/img/dummy.jpg" style="width: 30px;" alt="civimi"></div>
<div style="display: table-cell;vertical-align: bottom;padding-left: 5px;font-weight: bold;font-size: 16px;color: #5A5A5F;">John Hannah</div>
</div>
<div style="font-weight: bold;color: #4E5F69;font-size: 14px;padding: 0 10px;margin: 10px 0 0 0;">Front End Web Developer</div>
<div style="padding: 0 10px;;color: #5A5A5F;margin: 5px 0 0 0;font-size: 14px;">Canada</div>
<div style="text-align: center;margin-top:10px;"><img src="http://www.civimi.com/img/header3.jpg" style="width:100%;" alt="civimi"></div>
<div style="margin: 5px 0 0 0;padding: 0 10px;font-size: 14px;color: #5F5A5A;font-weight: bold;">Final Design</div>
</div>
<div style="text-align: center;margin-bottom: 30px;"><a href="" style="display: inline-block;text-decoration: none !important;outline: none !important;color: #FFFFFF;font-weight: bold;font-size: 16px;padding: 10px 20px;background-color: #19A0D9;">View in Civimi</a></div>
</div>
</div>
<div style="text-align: center;font-size: 18px;font-weight: bold;color: #666;margin: 10px 0 0 0;"><?=$lang['card-closing'];?></div>
<div id='footer' style='background-color: #666;padding: 10px 0;margin-top:20px;'>
<div style='display: block;height: 50px;padding: 0 20px;position: relative;'>
<div style='font-size: 20px;font-style: italic;color: #FFFFFF;font-weight: bold;display: block;width: 50%;float: left;'>'Reinvent Yourself'</div>
<div style='display: block;float: left;width: 50%;position: relative;text-align: right;'>
<a target='_blank' href='https://www.facebook.com/civimi'><img src='http://www.civimi.com/img/logo/facebook.png' alt='facebook' style='width: 20px;'></a>
<a target='_blank' href='https://plus.google.com/+Civimi'><img src='http://www.civimi.com/img/logo/google.png' alt='google' style='width: 20px;margin-left:8px;'></a>
<a target='_blank' href='https://twitter.com/civimi'><img src='http://www.civimi.com/img/logo/twitter.png' alt='twitter' style='width: 20px;margin-left:8px;'></a>
</div>
</div>
<div style='text-align:left;padding: 0 20px;margin-top: 20px;'><a target='_blank' href="" style="font-size:12px;color: #FFFFFF;outline:none !important;text-decoration:underline;"><?=$lang['card-notification_link'];?></a></div>
<div style='text-align:center;font-size: 13px;color: #FFFFFF;margin:5px 0 0 0;padding:0 20px;position:relative;'>
Copyright &copy; 2015 Civimi. All rights reserved.
</div>
</div>
</div>
</body>
</html>
