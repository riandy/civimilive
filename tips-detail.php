<?php 
include("packages/civ_require.php");
include("controller/controller_civ_global.php");
include("controller/controller_civ_tips_detail.php");
$curpage='tips';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title><?php echo correctDisplay($datas[0]['News_title']);?> | <?=$global['title-tips'];?> |</title>
    <meta name="author" content="CIVIMI">
    <meta name="keywords" content="<?=$datas[0]['News_tag'];?>">
    <meta name="description" content="<?php echo charLength(correctDisplay($datas[0]['News_content']),200);?>">
    <meta name="robots" content="<?=$seo['robot_yes'];?>">
    <meta itemprop="dateCreated" content="<?php echo date('d M Y',strtotime($datas[0]['News_create_date']));?>">
    <meta itemprop="name" content="<?php echo correctDisplay($datas[0]['News_title']);?>">
    <meta itemprop="description" content="<?php echo charLength(correctDisplay($datas[0]['News_content']),200);?>">
    <meta itemprop="image" content="<?=$global['absolute-base-admin'].$data_photos[0]['Np_img'];?>">
    <!-- SOCIAL MEDIA META -->
    <meta property="og:description" content="<?php echo charLength(correctDisplay($datas[0]['News_content']),200);?>">
    <meta property="og:image" content="<?=$global['absolute-base-admin'].$data_photos[0]['Np_img'];?>">
    <meta property="og:site_name" content="civimi">
    <meta property="og:title" content="<?php echo correctDisplay($datas[0]['News_title']);?>">
    <meta property="og:type" content="website">
    <meta property="og:url" content="<?php echo $path['tips'].$datas[0]['News_ID']."_".encode($datas[0]['News_title']).".html";?>">

    <!-- twitter meta -->
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:description" content="<?php echo charLength(correctDisplay($datas[0]['News_content']),200);?>">
    <meta name="twitter:title" content="<?php echo correctDisplay($datas[0]['News_title']);?>">
    <meta name="twitter:site" content="@civimi">
    <meta name="twitter:creator" content="@civimi">
    <meta name="twitter:image" content="<?=$global['absolute-base-admin'].$data_photos[0]['Np_img'];?>">
    <meta name="twitter:image:src" content="<?=$global['absolute-base-admin'].$data_photos[0]['Np_img'];?>">
    <?php include("packages/civ_head.php");?>
    <link rel="stylesheet" href="<?=$global['absolute-url'];?>css/swiper.min.css"/>
    <script src="<?=$global['absolute-url'];?>js/swiper.min.js"></script>
    <script language="javascript">
  var popupWindow = null;
  function centeredPopup(url,winName,w,h,scroll){
    LeftPosition = (screen.width) ? (screen.width-w)/2 : 0;
    TopPosition = (screen.height) ? (screen.height-h)/2 : 0;
    settings ='height='+h+',width='+w+',top='+TopPosition+',left='+LeftPosition+',scrollbars='+scroll+',resizable'
    popupWindow = window.open(url,winName,settings)
  }
  </script>
</head>
<body>
    <!-- HEADER -->
    <?php include("parts/part-header.php");?>
    <!-- END HEADER -->
    
    <div class="tips-part">
        <div class="container container-max">
            <div class="row">
                <div class="col-md-9 col-sm-8 col-xs-12">
                    <div class="tips-warp marg0">
                        
                        <!-- tips image -->
                        <div class="row">
                            <div class="col-xs-12 pad0" style="position:relative">
                                <?php $total_img=count($data_photos); if ( $total_img > 1 ){ ?>
                                <a href="javascript:;" class="swiper-left hidden-xs hidden-sm"><img class="swiper-arrow-news" alt="prev" src="<?php echo $global['absolute-url'];?>img/base-left.png"></a>
                                <a href="javascript:;" class="swiper-right hidden-xs hidden-sm"><img class="swiper-arrow-news" alt="next" src="<?php echo $global['absolute-url'];?>img/base-right.png"></a>
                                <?php } ?>
                                <div id="swiper-news" class="swiper-container height-auto">
                                    <div class="swiper-wrapper height-auto">
                                        <?php for($i=0;$i<$total_img;$i++){?>
                                        <div class="swiper-slide height-auto <?php if ( $total_img <= 1 ){ ?>swiper-no-swiping <?php } ?>">
                                            <div class="content-slide">
                                                <div class="main-swiper">
                                                    <img src="<?php echo $global['absolute-base-admin'].$data_photos[$i]['Np_img'];?>" alt="blog civimi <?php echo $data_photos[$i]['Np_title'];?>" class="swiper-image-news">
                                                </div>
                                            </div>
                                        </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- end tips image -->
                        <div class="row up1">
                            <div class="col-xs-6 pad0">
                                <div class="tips-cat">
                                    <span><?=relative_time($datas[0]['News_create_date']);?> </span>
                                    <span class="cat-title"><?=correctDisplay($datas[0]['Nc_title']);?></span>
                                </div>
                            </div>
                            <div class="col-xs-6 pad0">
                                <div class="tips-share">
                                    <a onclick="centeredPopup('https://www.facebook.com/sharer/sharer.php?u=<?php echo $path['tips'].encode($datas[0]['News_title'])."_".$datas[0]['News_ID'].".html";?>','myWindow','500','300','yes');return false" href="#" class="share-icon s-facebook"></a>
                                    <a onclick="centeredPopup('https://twitter.com/intent/tweet?url=<?php echo $path['tips'].encode($datas[0]['News_title'])."_".$datas[0]['News_ID'].".html";?>&text=<?php echo correctDisplay($datas[0]['News_title']);?>&via=civimi');return false" href="#" class="share-icon s-twitter"></a>
                                    <a onclick="centeredPopup('https://plus.google.com/share?url=<?php echo $path['tips'].encode($datas[0]['News_title'])."_".$datas[0]['News_ID'].".html";?>','myWindow','500','300','yes');return false" href="#" class="share-icon s-google"></a>
                                </div>
                            </div>
                        </div>
                        <div class="tips-container">
                            <div class="tips-content">
                                <h1 class="tips-title">
                                    <?=$datas[0]['News_title'];?>
                                </h1>
                                <?php if($datas[0]['News_author'] != ""){ ?>
                                <div class="tips-author">by 
                                    <?php if($datas[0]['News_author_link'] != ""){ ?>
                                    <a href="<?=$datas[0]['News_author_link'];?>" target="_blank"><?=correctDisplay($datas[0]['News_author']);?></a>
                                    <?php } else { ?>
                                    <a href="#"><?=correctDisplay($datas[0]['News_author']);?></a>
                                    <?php } ?>
                                </div>
                                <?php } ?>
                                <div class="tips-desc">
                                    <?=correctDisplay($datas[0]['News_content']);?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-4 col-xs-12">
                    <?php if(is_array($data_relateds)){ ?>
                    <div class="related-tips">
                        <div class="rt-head">Related Tips & Advice</div>
                        <?php foreach ($data_relateds as $data_related) { ?>
                        <div class="rt-wrap">
                            <a href="<?=$path['tips'].encode($data_related['News_title']).'_'.$data_related['News_ID'].'.html';?>" class="rt-link">
                                <div class="rt-image" style="background-image: url(<?php echo $global['absolute-base-admin'].$data_related['Np_img'];?>);"></div>
                                <div class="rt-title"><?=correctDisplay($data_related['News_title']);?></div>
                            </a>
                        </div>
                        <?php } ?>
                    </div>
                    <?php } ?>
                </div>
            </div>

            <!-- START PART TESTIMONI -->
            <?php include("parts/part-testimoni.php");?>
            <!-- END PART TESTIMONI -->

        </div>
    </div>
    <!-- START PART FOOTER -->
    <?php include("parts/part-footer.php");?>
    <!-- END PART FOOTER --> 
    <script type="text/javascript">
        var contentSwiper = new Swiper('#swiper-news',{
        <?php if ( $total_img > 1 ){ ?>
        autoplay:3000,
        grabCursor: true,
        loop:true,
        <?php } else { ?>
        noSwiping:true,
        <?php } ?>
        speed : 1000,
        });
        $('.swiper-left').on('click', function(e){
            e.preventDefault()
            contentSwiper.swipePrev()
        });
        $('.swiper-right').on('click', function(e){
            e.preventDefault()
            contentSwiper.swipeNext()
        });
    </script>
</body>
</html>