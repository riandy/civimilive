<div id="personal-achievement-section" class="job-sbox" data-order="2">
  <div id="achievement-header" class="user-section-header row border-bottom-eee">
    <div class="col-xs-6">
      <div class="profile-title"><i class="glyphicon glyphicon-glass"></i>&nbsp;&nbsp;<?=$lang['achieve-title'];?></div>
    </div>
    <div class="col-xs-6">
        <div class="edit-button text-right">
          <span class="sort-icon"><i class="glyphicon glyphicon-sort"></i></span>
            <a href="Javascript:;" id="add-achievement" onclick="hideEmptyAchieve();" data-toggle="collapse" data-target="#slide-achievement" aria-expanded="true" aria-controls="slide-achievement">
            <i class="glyphicon glyphicon-plus"></i> <span class="hidden-xs"><?=$lang['achieve-btn'];?></span>
          </a>
        </div>
      
    </div>
  </div>
  <div id="achievement-profile">
    <!-- start add achieve -->
    <div id="slide-achievement" class="collapse panel-input">
      <div class="panel-input-body panel-input-achieve">
        <div class="row up15">
          <div class="col-xs-12 up1">
            <div class="form-label bold"><?=$lang['achieve-name'];?> *</div>
          </div>
          <div class="col-sm-10 col-xs-12">
            <input id="achieve-name" type="text" name="" class="form-control no-radius" placeholder="<?=$lang['achieve-name_placeholder'];?>">
            <div id="achieve-vname" class="valid-message"></div>
          </div>
        </div>
        <div class="row">
          <div class="col-xs-6 up1">
            <div class="form-label bold"><?=$lang['achieve-source'];?> *</div>
            <div class="row">
              <div class="col-xs-6 pad0">
                <select id="achieve-source" name="" class="form-control no-radius">
                  <option value="">-<?=$lang['achieve-source_placeholder'];?>-</option>
                  <option value="education">Education</option>
                  <option value="company">Company</option>
                  <option value="organization">Organization</option>
                  <option value="other">Other</option>
                </select>
              </div>
            </div>
          </div>
          <div class="col-xs-6"></div>
          <div class="col-xs-12">
            <div id="achieve-vsource" class="valid-message"></div>
          </div>
        </div>
        <div id="input-institute" class="row up1 hide">
          <div class="col-xs-12">
            <div class="form-label bold"><?=$lang['achieve-institute'];?> *</div>
          </div>
          <div class="col-sm-10 col-xs-12">
            <div id="achieve-box" class="institute-box">
              <input id="achieve-institute" onfocus="$('#institute-list').show();" oninput="get_institute();$('#institute-list').show();" type="text" name="" class="form-control no-radius" placeholder="<?=$lang['achieve-institute_placeholder'];?>">
              <div id="institute-list" class="institute-list" onmouseover="$('#institute-list').show();">
                <ul id="institute-ul">

                </ul>
              </div>
            </div>
            <div id="achieve-vinstitute" class="valid-message"></div>
          </div>
        </div>
        <div class="row">
          <div class="col-xs-6 up1">
            <div class="form-label bold"><?=$lang['achieve-year'];?> *</div>
            <div class="row">
              <div class="col-xs-6 pad0">
                <select id="achieve-year" name="" class="form-control no-radius">
                  <option value="">--</option>
                  <?php $year = '1950';
                  for ($years = date("Y"); $years >= $year; $years--) { ?>
                  <option value="<?php echo $years; ?>"><?php echo $years; ?></option>
                  <?php } ?>
                </select>
              </div>
            </div>
          </div>
          <div class="col-xs-6"></div>
          <div class="col-xs-12">
            <div id="achieve-vyear" class="valid-message"></div>
          </div>
        </div>
        <div class="row">
          <div class="col-xs-12 up1">
            <div class="form-label"><?=$lang['achieve-desc'];?></div>
          </div>
          <div class="col-sm-12 col-xs-12">
            <textarea id="achieve-desc" name="desc" class="form-control no-radius" rows="3" placeholder="<?=$lang['achieve-desc_placeholder'];?>"></textarea>
          </div>
        </div>
        <div class="row">
          <div class="col-xs-12 up2">
            <div id="achieve-message" class="valid-message"></div>
            <a href="Javascript:;" class="btn btn-info" onclick="save_achieve();">Save</a><span class="pad05"></span>
            <a href="Javascript:;" onclick="hideEmptyAchieve();" type="reset" class="btn btn-default" data-toggle="collapse" data-target="#slide-achievement" aria-expanded="true" aria-controls="slide-achievement">Cancel</a>
          </div>
        </div>
      </div>
    </div><!-- end panel add achieve -->

    <!-- main achieve -->
    <div class="panel-group marg0" id="main-achieve" role="tablist" aria-multiselectable="true">
    </div> <!-- end main achieve -->
    <div class="see-more"><a href="javascript:;" id="see-achieve" class="hide">SEE <span id="seeAchieve-num"></span> MORE</a></div>
    <div id='load-achieve' class='text-center'></div>

    <!-- start edit achieve -->
    <div id="edit-achieve-place" class="hide">
      <div id="edit-achieve">
        <div class="panel-input-achieve">
          <div class="row up15">
            <div class="col-xs-12 up1">
              <div class="form-label bold"><?=$lang['achieve-name'];?> *</div>
            </div>
            <div class="col-sm-10 col-xs-12">
              <input id="achieve-ename" type="text" name="" class="form-control no-radius" placeholder="<?=$lang['achieve-name_placeholder'];?>">
              <div id="achieve-evname" class="valid-message"></div>
            </div>
          </div>
          <div class="row">
            <div class="col-xs-6 up1">
              <div class="form-label bold"><?=$lang['achieve-source'];?> *</div>
              <div class="row">
                <div class="col-xs-6 pad0">
                  <select id="achieve-esource" name="" class="form-control no-radius">
                    <option value="">-<?=$lang['achieve-source_placeholder'];?>-</option>
                    <option value="education">Education</option>
                    <option value="company">Company</option>
                    <option value="organization">Organization</option>
                    <option value="other">Other</option>
                  </select>
                </div>
              </div>
            </div>
            <div class="col-xs-6"></div>
            <div class="col-xs-12">
              <div id="achieve-evsource" class="valid-message"></div>
            </div>
          </div>
          <div id="input-einstitute" class="row up1">
            <div class="col-xs-12">
              <div class="form-label bold"><?=$lang['achieve-institute'];?> *</div>
            </div>
            <div class="col-sm-10 col-xs-12">
              <div id="achieve-boxEdit" class="institute-box">
                <input id="achieve-einstitute" onfocus="$('#institute-listEdit').show();" oninput="get_institute2();$('#institute-listEdit').show();" type="text" name="" class="form-control no-radius" placeholder="<?=$lang['achieve-institute_placeholder'];?>">
                <div id="institute-listEdit" class="institute-list" onmouseover="$('#institute-listEdit').show();">
                  <ul id="institute-eul">

                  </ul>
                </div>
              </div>
              <div id="achieve-evinstitute" class="valid-message"></div>
            </div>
          </div>
          <div class="row">
            <div class="col-xs-6 up1">
              <div class="form-label bold"><?=$lang['achieve-year'];?> *</div>
              <div class="row">
                <div class="col-xs-6 pad0">
                  <select id="achieve-eyear" name="" class="form-control no-radius">
                    <option value="" class="achieve-eyear">--</option>
                    <?php $year = '1950';
                    for ($years = date("Y"); $years >= $year; $years--) { ?>
                    <option value="<?php echo $years; ?>" class="achieve-eyear"><?php echo $years; ?></option>
                    <?php } ?>
                  </select>
                </div>
              </div>
            </div>
            <div class="col-xs-6"></div>
            <div class="col-xs-12">
              <div id="achieve-evyear" class="valid-message"></div>
            </div>
          </div>
          <div class="row">
            <div class="col-xs-12 up1">
              <div class="form-label"><?=$lang['achieve-desc'];?></div>
            </div>
            <div class="col-sm-12 col-xs-12">
              <textarea id="achieve-edesc" name="desc" class="form-control no-radius" rows="3" placeholder="<?=$lang['achieve-desc_placeholder'];?>"></textarea>
            </div>
          </div>
          <div class="row up1">
            <div class="col-xs-12">
              <input id="edit_achieve_old_name" type="hidden">
              <input id="edit_achieve_id" type="hidden">
              <input id="edit_achieve_ATid" type="hidden">
              <a class="btn btn-info" onclick="update_achieve();" >Save</a><span class="pad05"></span>
              <a id="reset_edit_achieve" type="reset" class="btn btn-default" onclick="close_edit_achieve()">Cancel</a>
              <a href="javascript:;" id="delete-achieve" onclick="delete_achieve();" class="delete-text">Delete</a>
            </div>
          </div>
        </div>
      </div> 
    </div><!-- end edit achieve -->
  </div>

<script type="text/javascript">
document.getElementById("achieve-source").onchange = function(){
    var check = $("#achieve-source").val();
    if(check != ""){
      $("#input-institute").removeClass("hide");
    } else {
      $("#input-institute").addClass("hide");
    }
};
document.getElementById("achieve-esource").onchange = function(){
    var check = $("#achieve-esource").val();
    if(check != ""){
      $("#input-einstitute").removeClass("hide");
    } else {
      $("#input-einstitute").addClass("hide");
    }
};
// FUNCTION AUTOCOMPLETE SCHOOL 
function get_institute(){
  var s = $("#achieve-source").val();
  var q = $("#achieve-institute").val();
  $("#achieve-institute").addClass("load-input");
  if(s != '' || s != 'other'){
    var url = "<?=$api['achieve-autocomplete'];?>&source="+s+"&term="+q;                            
    $.ajax({url: url,success:function(result){
      $("#achieve-institute").removeClass("load-input");
      $('#institute-ul').html(replace_institute(result.data,q));
    }}); 
  }
}
function replace_institute(datas,text){
  var resultHTML='';
  if(datas != null){
    var obj = datas;    
    for(var i=0;i < obj.length;i++){
      if(obj[i].Achieve_ID != '' && obj[i].Achieve_name != ''){
        resultHTML += "<li class='institute-li' id='institute-"+obj[i].Achieve_ID+"' onclick='selectInstitute("+obj[i].Achieve_ID+")' data-ins_image='"+obj[i].Achieve_img+"' data-ins_title='"+obj[i].Achieve_name+"'>";
        if(obj[i].Achieve_img != ""){
          resultHTML += "<div class='institute-image'><img src='<?=$global['img-url'];?>"+obj[i].Achieve_img+"' alt=''></div>";
        }else {
          resultHTML += "<div class='institute-image'><img src='<?=$placeholder['img-achievement'];?>' alt='image'></div>";
        }
        resultHTML += "<div class='institute-text'>";
        resultHTML += "<div class='institute-title'>"+obj[i].Achieve_name+"</div>";
        if(obj[i].Achieve_city)
        resultHTML += "<div class='institute-place'>"+obj[i].Achieve_city+", "+obj[i].Achieve_country+"</div>";
        resultHTML += "</div>";
        resultHTML += "</li>";
      }
    }
  } else {
    resultHTML += "<li class='no-list'><div class='no-list-text'><?=$lang['entry-result'];?> \"<span>"+text+"</span>\" <?=$lang['entry-result_text'];?></div></li>";
  }
  return resultHTML;
}
function get_institute2(){
  var s = $("#achieve-esource").val();
  var q = $("#achieve-einstitute").val();
  $("#achieve-einstitute").addClass("load-input");
  if(s != '' || s != 'other'){
    var url = "<?=$api['achieve-autocomplete'];?>&source="+s+"&term="+q;                            
    $.ajax({url: url,success:function(result){
      $("#achieve-einstitute").removeClass("load-input");
      $('#institute-eul').html(replace_institute2(result.data,q));
    }});
  } 
}
function replace_institute2(datas,text){
  var resultHTML='';
  if(datas != null){
    var obj = datas;    
    for(var i=0;i < obj.length;i++){
      if(obj[i].Achieve_ID != '' && obj[i].Achieve_name != ''){
        resultHTML += "<li class='institute-li' id='institutee-"+obj[i].Achieve_ID+"' onclick='selectInstituteEdit("+obj[i].Achieve_ID+")' data-ins_eimage='"+obj[i].Achieve_img+"' data-ins_etitle='"+obj[i].Achieve_name+"'>";
        if(obj[i].Achieve_img != ""){
          resultHTML += "<div class='institute-image'><img src='<?=$global['img-url'];?>"+obj[i].Achieve_img+"' alt=''></div>";
        }else {
          resultHTML += "<div class='institute-image'><img src='<?=$placeholder['img-achievement'];?>' alt='image'></div>";
        }
        resultHTML += "<div class='institute-text'>";
        resultHTML += "<div class='institute-title'>"+obj[i].Achieve_name+"</div>";
        resultHTML += "<div class='institute-place'>"+obj[i].Achieve_city+", "+obj[i].Achieve_country+"</div>";
        resultHTML += "</div>";
        resultHTML += "</li>";
      }
    }
  } else {
    resultHTML += "<li class='no-list'><div class='no-list-text'><?=$lang['entry-result'];?> \"<span>"+text+"</span>\" <?=$lang['entry-result_text'];?></div></li>";
  }
  return resultHTML;
}
function selectInstitute(param){
  var title = $('#institute-'+param).data('ins_title');

  $("#achieve-institute").removeClass("load-input");
  $('#achieve-institute').val(title);
  $('#institute-list').hide();
}
function selectInstituteEdit(param){
  var title = $('#institutee-'+param).data('ins_etitle');

  $("#achieve-einstitute").removeClass("load-input");
  $('#achieve-einstitute').val(title);
  $('#institute-listEdit').hide();
}
$('html').click(function() {
  $('#institute-list').hide();
  $("#achieve-institute").removeClass("load-input");
});
$('#institute-box').click(function(event){
  event.stopPropagation();
  $("#achieve-institute").removeClass("load-input");
});
$('html').click(function() {
  $('#institute-listEdit').hide();
  $("#achieve-einstitute").removeClass("load-input");
});
$('#institute-boxEdit').click(function(event){
  event.stopPropagation();
  $("#achieve-einstitute").removeClass("load-input");
});
// END FUNCTION AUTOCOMPLETE SCHOOL 

// FUNCTION GET DATA EDUCATION
get_achieve();
function get_achieve(){
  $('#see-achieve').addClass('hide');
  $('#load-achieve').html("<img src='<?=$global['absolute-url'];?>img/load-blue.gif' style='margin:70px 0' />");
  $('#main-achieve').html('');
  var url = "<?=$api['achieve-data'];?>&user_id=<?php echo $_SESSION['userData']['id'];?>&auth_code=<?php echo $_SESSION['userData']['auth_code'];?>";                            
  $.ajax({url: url,success:function(result){
    setTimeout(function() {
      $('#load-achieve').html('');
      $('#main-achieve').html(replace_achieve(result.data));
      seeMoreAchieve();
    }, 1000)
  }}); 
}
function replace_achieve(datas){
  var resultHTML='';resultNext = "";
  var total = 0;previous = "";user_name = "";link="";
  var d = new Date();strip_string ="";
  var current = d.getTime();
  // console.log(datas);
  if(datas != null){
    var obj = datas;    
    for(var i=0;i < obj.length;i++){
      if(obj[i].ATachieve_ID != '' && obj[i].ATachieve_title != ''){
          resultHTML += "<div class='row row-achieve hide hm-achieve' role='tab'>";
          resultHTML += "<div class='col-xs-12'>";
          resultHTML += "<a href='#achcollapse-"+obj[i].ATachieve_ID+"' aria-controls='achcollapse-"+obj[i].ATachieve_ID+"' data-parent='#main-achieve' onclick='edit_box_achieve("+obj[i].ATachieve_ID+");' data-toggle='collapse' aria-expanded='false' id='achieve-"+obj[i].ATachieve_ID+"' class='achievement-box' data-achieve_atid='"+obj[i].ATachieve_ID+"' data-achieve_name='"+obj[i].ATachieve_title+"' data-achieve_source='"+obj[i].ATachieve_source+"' data-achieve_institute='"+obj[i].institute_name+"' data-achieve_year='"+obj[i].ATachieve_year+"' data-achieve_desc='"+obj[i].ATachieve_desc+"'>";
          resultHTML += "<div class='row'>";
          resultHTML += "<div class='col-md-10 col-xs-9 pad0'>";
          resultHTML += "<div class='achievement-title'>"+obj[i].ATachieve_title+" <span class='achieve-etext'><i class='glyphicon glyphicon-edit'></i> <span class='hidden-xs'>Edit</span></span></div>";
          resultHTML += "<div class='achievement-year'>"+obj[i].ATachieve_year+"</div>";
          resultHTML += "</div>";
          resultHTML += "<div class='col-md-2 col-xs-3'>";
          resultHTML += "</div>";
          resultHTML += "</div>";
          resultHTML += "</a>";
          resultHTML += "<div id='load-achieve-"+obj[i].ATachieve_ID+"' class='text-center'></div>";
          resultHTML += "<a id='close-achieve-"+obj[i].ATachieve_ID+"' href='#achcollapse-"+obj[i].ATachieve_ID+"' aria-controls='achcollapse-"+obj[i].ATachieve_ID+"' data-parent='#main-achieve' data-toggle='collapse' aria-expanded='false' class='hide'></a>";
          resultHTML += "</div>";
          resultHTML += "</div>";
          resultHTML += "<div id='achcollapse-"+obj[i].ATachieve_ID+"' aria-labelledby='achieve-"+obj[i].ATachieve_ID+"' class='panel-collapse collapse collapse-achievement pad0' role='tabpanel'></div>";
        }
    }
  } else {
    resultHTML += "<div id='empty-achieve' class='empty-file'><?=$lang['achieve-placeholder'];?></div>";
  }
  return resultHTML;
};
function seeMoreAchieve(){
    size = $(".row-achieve").size();
    x=2;

    $('.row-achieve:lt('+x+')').removeClass('hide');
    $('.row-achieve:lt('+x+')').removeClass('hm-achieve'); 
    size_left= $(".hm-achieve").size();
    if(size > 2){
      $('#see-achieve').removeClass('hide');
      $("#seeAchieve-num").text('('+size_left+')');
    } else {
      $('#see-achieve').addClass('hide');
    }

    $('#see-achieve').on('click', function(){
        $('.row-achieve').removeClass('hide');
        $('.row-achieve').removeClass('hm-achieve');
        $('#see-achieve').addClass('hide');
    });
  };
// END FUNCTION GET DATA EDUCATION

// FUNCTION VALIDATION SAVE AND UPDATE
function validAchieve(){
  var achieve_name = $("#achieve-name").val();
  var achieve_source = $("#achieve-source").val();
  var achieve_institute = $("#achieve-institute").val();
  var achieve_year = $("#achieve-year").val();
  var message = "";

  if(achieve_name != ""){
    message = "";
    $("#achieve-name").removeClass('input-error');
    $("#achieve-vname").text(message);
  } else {
    message = "Please input Achievement name!";
    $("#achieve-name").addClass('input-error');
    $("#achieve-name").focus();
    $("#achieve-vname").text(message);
    return false;
  }
  if(achieve_source != ""){
    message = "";
    $("#achieve-source").removeClass('input-error');
    $("#achieve-vsource").text(message);
  } else {
    message = "Please input Achievement Source!";
    $("#achieve-source").addClass('input-error');
    $("#achieve-source").focus();
    $("#achieve-vsource").text(message);
    return false;
  }
  if(achieve_institute != ""){
    message = "";
    $("#achieve-institute").removeClass('input-error');
    $("#achieve-vinstitute").text(message);
  } else {
    message = "Please input the institute!";
    $("#achieve-institute").addClass('input-error');
    $("#achieve-institute").focus();
    $("#achieve-vinstitute").text(message);
    return false;
  }
  if(achieve_year != ""){
    message = "";
    $("#achieve-year").removeClass('input-error');
    $("#achieve-vyear").text(message);
  } else {
    message = "Please input year of achievement!";
    $("#achieve-year").addClass('input-error');
    $("#achieve-year").focus();
    $("#achieve-vyear").text(message);
    return false;
  }
  return true;
}
function validAchieveEdit(){
  var achieve_name = $("#achieve-ename").val();
  var achieve_source = $("#achieve-esource").val();
  var achieve_institute = $("#achieve-einstitute").val();
  var achieve_year = $("#achieve-eyear").val();
  var message = "";

  if(achieve_name != ""){
    message = "";
    $("#achieve-ename").removeClass('input-error');
    $("#achieve-evname").text(message);
  } else {
    message = "Please input Achievement name!";
    $("#achieve-ename").addClass('input-error');
    $("#achieve-ename").focus();
    $("#achieve-evname").text(message);
    return false;
  }
  if(achieve_source != ""){
    message = "";
    $("#achieve-esource").removeClass('input-error');
    $("#achieve-evsource").text(message);
  } else {
    message = "Please input Achievement Source!";
    $("#achieve-esource").addClass('input-error');
    $("#achieve-esource").focus();
    $("#achieve-evsource").text(message);
    return false;
  }
  if(achieve_institute != ""){
    message = "";
    $("#achieve-einstitute").removeClass('input-error');
    $("#achieve-evinstitute").text(message);
  } else {
    message = "Please input the institute!";
    $("#achieve-einstitute").addClass('input-error');
    $("#achieve-einstitute").focus();
    $("#achieve-evinstitute").text(message);
    return false;
  }
  if(achieve_year != ""){
    message = "";
    $("#achieve-eyear").removeClass('input-error');
    $("#achieve-evyear").text(message);
  } else {
    message = "Please input year of achievement!";
    $("#achieve-eyear").addClass('input-error');
    $("#achieve-eyear").focus();
    $("#achieve-evyear").text(message);
    return false;
  }
  return true;
}
//END VALIDATION SAVE AND UPDATE

// FUNCTION SAVE, UPDATE, DELETE
function save_achieve(){
  if ( validAchieve() ){
    $('#edit-achieve').detach().appendTo('#edit-achieve-place');
    $('html, body').animate({
      scrollTop: $('#achievement-header').offset().top-80
    }, 'fast');
    //$('#load-achieve').html('<img src="img/load-blue.gif" style="margin:70px 0" />');

    var url = "<?=$api['achieve-update'];?>";
    var achieve_name = $("#achieve-name").val();
    var achieve_source = $("#achieve-source").val();
    var achieve_institute = $("#achieve-institute").val();
    var achieve_year = $("#achieve-year").val();
    var achieve_desc = $("#achieve-desc").val();
    var data = {
      user_id : "<?=$_SESSION['userData']['id'];?>",
      auth_code : "<?=$_SESSION['userData']['auth_code'];?>",
      title : achieve_name,
      source : achieve_source,
      institute : achieve_institute,
      year : achieve_year,
      desc : achieve_desc
    }
    $.ajax({url: url,data : data, success:function(result){
      $('#slide-achievement').collapse('hide');
      emptyAddAchieve();
      get_achieve();
      console.log(result);
    }});
  }
}
function update_achieve(){
  if( validAchieveEdit()){
    $('#edit-achieve').detach().appendTo('#edit-achieve-place');
    var idu = $('#edit_achieve_id').val();
    $('#close-achieve-'+idu).click();
    $('html, body').animate({
    scrollTop: $('#achievement-header').offset().top-80
    }, 'fast');
    //$('#load-achieve-'+idu).html('<img src="img/load-blue.gif" class="marg1" />');

    var url = "<?=$api['achieve-update'];?>";
    var achieve_id = $("#edit_achieve_ATid").val();
    var achieve_name = $("#achieve-ename").val();
    var achieve_source = $("#achieve-esource").val();
    var achieve_institute = $("#achieve-einstitute").val();
    var achieve_year = $("#achieve-eyear").val();
    var achieve_desc = $("#achieve-edesc").val();

    var data = {
      user_id : "<?=$_SESSION['userData']['id'];?>",
      auth_code : "<?=$_SESSION['userData']['auth_code'];?>",
      id : achieve_id,
      title : achieve_name,
      source : achieve_source,
      institute : achieve_institute,
      year : achieve_year,
      desc : achieve_desc,
    }
    $.ajax({url: url,data : data, success:function(result){
      get_achieve();
      console.log(result);
    }});
  }
}
function delete_achieve(){
  if (confirm("Are you sure to delete this information ?")) {
    $('#edit-achieve').detach().appendTo('#edit-achieve-place');
    $('html, body').animate({
      scrollTop: $('#achievement-header').offset().top-80
    }, 'fast');
    
    var url = "<?=$api['achieve-delete'];?>";
    var achieve_id = $("#edit_achieve_ATid").val();
    var data = {
      user_id : "<?=$_SESSION['userData']['id'];?>",
      auth_code : "<?=$_SESSION['userData']['auth_code'];?>",
      id : achieve_id
    }
    $.ajax({url: url,data : data, success:function(result){
      get_achieve();
      console.log(result);
    }});
  }
}
// END FUNCTION SAVE, UPDATE, DELETE

// OTHER FUNCTION
function close_edit_achieve(){
  var id = $('#edit_achieve_id').val();
  $('#close-achieve-'+id+'').click();
  $('#achieve-'+id).removeClass('hide-achieve');
}
function hideEmptyAchieve(){
  var myElem = document.getElementById('empty-achieve');
  if(myElem != null){
    $('#empty-achieve').toggle();
  }
  var id = $('#edit_achieve_id').val();
  if (id != ''){
    if ($('#achcollapse-'+id).hasClass('in')){
      $('#close-achieve-'+id+'').click();
      $('#achieve-'+id).removeClass('hide-achieve');
    }
  }
  emptyAddAchieve();
}
function emptyAddAchieve(){
  $("#achieve-name").val('');
  $("#achieve-source").val('');
  $("#achieve-institute").val('');
  $("#achieve-year").val('');
  $("#achieve-desc").val('');
}
function edit_box_achieve(param){
  if($('#slide-achievement').hasClass('in')){
    $('#slide-achievement').collapse('hide');
  }
  if($('.achievement-box').hasClass('hide-achieve')){
    $('.achievement-box').removeClass('hide-achieve');
    if($('.collapse-achievement').hasClass('in')){
      $('.collapse-achievement').removeClass('in')
    }
    $('#achieve-'+param).addClass('hide-achieve');
  } else {
    $('#achieve-'+param).addClass('hide-achieve');
  }
  $('#edit-achieve').detach().appendTo('#achcollapse-'+param+'');
  var id = $('#achieve-'+param).data('achieve_id');
  var ATid = $('#achieve-'+param).data('achieve_atid');
  var name = $('#achieve-'+param).data('achieve_name');
  var source = $('#achieve-'+param).data('achieve_source');
  var institute = $('#achieve-'+param).data('achieve_institute');
  var year = $('#achieve-'+param).data('achieve_year');
  var desc = $('#achieve-'+param).data('achieve_desc');

  $('#reset_edit_achieve').data("target") === "#achcollapse";
  $('#achieve-ename').val(name);
  $('#achieve-esource').val(source);
  $('#achieve-einstitute').val(institute);
  $('option[value="' + year + '"].achieve-eyear').attr("selected", true);
  $('#achieve-edesc').val(desc);
  $('#edit_achieve_id').val(ATid);
  $('#edit_achieve_ATid').val(ATid);
}
// END OTHER FUNCTION
</script>
</div> <!-- end personal achieve section -->