$(document).ready(function() {
    $(".resume-photo").fancybox({
        openEffect  : 'none',
        closeEffect : 'none'
    });
    // $(".work-info-img").fancybox({
    //     openEffect  : 'none',
    //     closeEffect : 'none'
    // });
    $('.video-frame').fancybox({
        openEffect  : 'none',
        closeEffect : 'none',
        autoSize: true,
        type: 'iframe',
        iframe: {
        preload: false // fixes issue with iframe and IE
        }
    });
    $(".doc-frame").fancybox({
        openEffect: 'elastic',
        closeEffect: 'elastic',
        autoSize: true,
        type: 'iframe',
        iframe: {
        preload: false // fixes issue with iframe and IE
        }
    });
    $(".web-frame").fancybox({
        openEffect: 'elastic',
        closeEffect: 'elastic',
        autoSize: true,
        type: 'iframe',
        iframe: {
        preload: false // fixes issue with iframe and IE
        }
    });
});
function swapObj(){
    if ($("#collapse-objective").hasClass('in')){
        $("#ic-object").show();
        $("#ic-object2").hide();
    } else {
        $("#ic-object2").show();
        $("#ic-object").hide();
        $("#ic-edu2").hide();
        $("#ic-exp2").hide();
        $("#ic-skill2").hide();
        $("#ic-edu").show();
        $("#ic-exp").show();
        $("#ic-skill").show();
    }
}
function swapEdu(){
    if ($("#collapse-education").hasClass('in')){
        $("#ic-edu").show();
        $("#ic-edu2").hide();
    } else {
        $("#ic-edu2").show();
        $("#ic-edu").hide();
        $("#ic-object2").hide();
        $("#ic-exp2").hide();
        $("#ic-skill2").hide();
        $("#ic-exp").show();
        $("#ic-object").show();
        $("#ic-skill").show();
    }
}
function swapExp(){
    if ($("#collapse-experience").hasClass('in')){
        $("#ic-exp").show();
        $("#ic-exp2").hide();
    } else {
        $("#ic-exp2").show();
        $("#ic-exp").hide();
        $("#ic-edu2").hide();
        $("#ic-object2").hide();
        $("#ic-skill2").hide();
        $("#ic-edu").show();
        $("#ic-object").show();
        $("#ic-skill").show();
    }
}
function swapSkill(){
    if ($("#collapse-skill").hasClass('in')){
        $("#ic-skill").show();
        $("#ic-skill2").hide();
    } else {
        $("#ic-skill2").show();
        $("#ic-skill").hide();
        $("#ic-edu2").hide();
        $("#ic-exp2").hide();
        $("#ic-object2").hide();
        $("#ic-edu").show();
        $("#ic-exp").show();
        $("#ic-object").show();
    }
}
function swapLang(){
    if ($("#collapse-lang").hasClass('in')){
        $("#ic-lang").show();
        $("#ic-lang2").hide();
    } else {
        $("#ic-lang2").show();
        $("#ic-lang").hide();
        $("#ic-edu2").hide();
        $("#ic-exp2").hide();
        $("#ic-object2").hide();
        $("#ic-edu").show();
        $("#ic-exp").show();
        $("#ic-object").show();
    }
}