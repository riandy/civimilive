<?php 
//Skill
if($O_skillNum>0)
{
$pdf->SetFont('Helvetica','B',12);
$pdf->Cell(46,5,'SKILLS   ',0,0,'R');
$pdf->Ln(5);
$pdf->SetFont('Helvetica','',10);
$pdf->Cell(46,5,'Abilities    ',0,0,'R');
$counterMargin=0;
for($i=0;$i<=$O_skillNum-1;$i++){
    if($counterMargin==2)
    {
    $pdf->Cell(48,5,'- '.charLength(html_entity_decode($O_skillName[$i]),24),0,1);
    $pdf->Cell(46,5,'',0,0,'R');
    $counterMargin=-1;
    }
    else
    {
    $pdf->Cell(48,5,'- '.charLength(html_entity_decode($O_skillName[$i]),24),0,0);
    }
    $counterMargin++;
}
$pdf->Ln(8);
}//END of skill part
?>
