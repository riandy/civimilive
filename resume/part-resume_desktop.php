<div id="resume-desktop" class="hidden-xs"> 
	<!-- start objective -->
	<?php if($data_user[0]['User_obj'] != ""){ ?>
	<div id="resume-objective" class="top-space">
		<div class="row">
			<div class="col-sm-3">
				<h2 class="entry-head">OBJECTIVE</h2>
			</div>
			<div class="col-sm-9">
				<div class="objective-desc">
					<?=correctText($data_user[0]['User_obj']);?>
				</div>
			</div>
		</div>
	</div>
	<?php } ?>
	<!-- end objective -->
	<!-- start education -->
	<?php if(is_array($data_edus)){ ?>
	<div id="resume-education" class="top-space">
		<div class="row">
			<div class="col-sm-3">
				<h2 class="entry-head">EDUCATION</h2>
			</div>
			<div class="col-sm-9"></div>
		</div>
		<?php foreach($data_edus as $data_edu){ ?>
		<div class="row up2">
			<div class="col-sm-3 col-xs-12">
				<h3 class="entry-child">
					<?=$data_edu['ATedu_fromMonth']."/".$data_edu['ATedu_fromYear'];?> - <?php if($data_edu['ATedu_toMonth'] != "0"){ echo $data_edu['ATedu_toMonth']."/".$data_edu['ATedu_toYear']; } else { echo "Present";}?>
				</h3>
			</div>
			<div class="col-sm-9 col-xs-12">
				<div class="entry-detail">
					<strong><?=$data_edu['Edu_schoolName'];?></strong>, <?=$data_edu['Edu_schoolCity']." ".$data_edu['Edu_schoolCountry'];?><br>
					<em><?=$data_edu["ATedu_degree"]."&nbsp;in ".$data_edu["ATedu_major"];?></em>
					<?php if($data_edu['ATedu_GPA'] != 0){ ?><em>GPA: <?=$data_edu['ATedu_GPA'];?></em><?php } ?>
				</div>
			</div>
		</div>
		<?php } ?>
	</div>
	<?php } ?>
	<!-- end education -->
	<!-- start experience -->
	<?php if(is_array($data_works)){ ?>
	<div id="resume-experience" class="top-space">
		<div class="row">
			<div class="col-sm-3">
				<h2 class="entry-head">EXPERIENCE</h2>
			</div>
			<div class="col-sm-9"></div>
		</div>
		<?php foreach($data_works as $data_work){ ?>
		<div class="row up2">
			<div class="col-sm-3 col-xs-12">
				<h3 class="entry-child">
					<?=$data_work['ATwork_from_month']."/".$data_work['ATwork_from_year'];?> - <?php if($data_work['ATwork_to_month'] != "0"){ echo $data_work['ATwork_to_month']."/".$data_work['ATwork_to_year']; } else { echo "Present";}?>
				</h3>
			</div>
			<div class="col-sm-9 col-xs-12">
				<div class="entry-detail">
					<strong><?=$data_work['Company_title'];?></strong>, <?=$data_work['City_title']." ".$data_work['Country_title'];?><br>
					<em><?=$data_work['ATwork_title'];?></em>
					<p class="experience-info">
						<?php echo correctText($data_work['ATwork_desc']);?>
					</p>
				</div>
			</div>
		</div>
		<?php } ?>
	</div>
	<?php } ?>
	<!-- end experience -->
	<!-- start organization -->
	<?php if(is_array($data_orgs)){ ?>
	<div id="resume-experience" class="top-space">
		<div class="row">
			<div class="col-sm-3">
				<h2 class="entry-head">ORGANIZATION <span class="badge badge-new">NEW</span></h2>
			</div>
			<div class="col-sm-9"></div>
		</div>
		<?php foreach($data_orgs as $data_org){ ?>
		<div class="row up2">
			<div class="col-sm-3 col-xs-12">
				<h3 class="entry-child">
					<?=$data_org['ATorg_from_month']."/".$data_org['ATorg_from_year'];?> - <?php if($data_org['ATorg_to_month'] != "0"){ echo $data_org['ATorg_to_month']."/".$data_org['ATorg_to_year']; } else { echo "Present";}?>
				</h3>
			</div>
			<div class="col-sm-9 col-xs-12">
				<div class="entry-detail">
					<strong><?=$data_org['Org_title'];?></strong>, <?=$data_org['City_title']." ".$data_org['Country_title'];?><br>
					<em><?=$data_org['ATorg_title'];?></em>
					<p class="experience-info">
						<?php echo correctText($data_org['ATorg_desc']);?>
					</p>
				</div>
			</div>
		</div>
		<?php } ?>
	</div>
	<?php } ?>
	<!-- end organization -->
	<!-- start skill -->
	<?php if(is_array($data_skills)){ ?>
	<div id="resume-skill" class="top-space">
		<div class="row">
			<div class="col-sm-3">
				<h2 class="entry-head">SKILL</h2>
			</div>
			<div class="col-sm-9"></div>
		</div>
		<div class="row up2">
			<div class="col-sm-3 col-xs-12">
				<h3 class="entry-child">Abilities</h3>
			</div>
			<div class="col-sm-9 col-xs-12">
				<ul class="skills-info">
					<?php foreach($data_skills as $data_skill){ ?>
					<li title="<?=correctDisplay($data_skill['ATskill_desc']);?>"> <?=$data_skill['Skill_name'];?></li>
					<?php } ?>
				</ul>
			</div>
		</div>
	</div>
	<?php } ?>
	<!-- end skill -->
	<!-- start language -->
	<?php if(is_array($data_langs)){ ?>
	<div id="resume-skill" class="top-space">
		<div class="row">
			<div class="col-sm-3">
				<h2 class="entry-head">LANGUAGE</h2>
			</div>
			<div class="col-sm-9"></div>
		</div>
		<div class="row up2">
			<div class="col-sm-3 col-xs-12">
				<h3 class="entry-child">Abilities</h3>
			</div>
			<div class="col-sm-9 col-xs-12">
				<ul class="skills-info">
					<?php foreach($data_langs as $data_lang){ ?>
					<li title="Listening <?=replace_rate($data_lang['ATlang_listen']);?>, Speaking <?=replace_rate($data_lang['ATlang_speak']);?>, Reading <?=replace_rate($data_lang['ATlang_read']);?>, Writing <?=replace_rate($data_lang['ATlang_write']);?>"> <?=$data_lang['Lang_name'];?></li>
					<?php } ?>
				</ul>
			</div>
		</div>
	</div>
	<?php } ?>
	<!-- end language -->
	<!-- start work -->
	<?php if(is_array($data_docs) || is_array($data_videos) || is_array($data_webs) || is_array($data_photos)){ ?>
	<div id="resume-work" class="top-space">
		<div class="row">
			<div class="col-sm-3">
				<h2 class="entry-head">WORKS</h2>
			</div>
			<div class="col-sm-9"></div>
		</div>
		<?php if(is_array($data_docs)){ ?>
		<div class="row up2">
			<div class="col-sm-3 col-xs-12">
				<h3 class="entry-child">Writing</h3>
			</div>
			<div class="col-sm-9 col-xs-12">
				<ul class="work-info">	
					<?php foreach($data_docs as $data){ ?>
					<li title="<?php echo $data['Doc_desc'];?>"><a href="<?php echo $global['img-url'].$data['Doc_fileLoc'];?>" class="various doc-frame"><?php echo $data['Doc_title'];?></a></li>
					<?php } ?>
				</ul>
			</div>
		</div>
		<?php } ?>
		<?php if(is_array($data_videos)){ ?>
		<div class="row up3">
			<div class="col-sm-3">
				<h3 class="entry-child">Video</h3>
			</div>
			<div class="col-sm-9">
				<ul class="work-info">
					<?php foreach($data_videos as $data){ ?>
					<li title="<?php echo $data['Video_desc'];?>"><a href="<?php echo correctDisplay($data['Video_link']);?>" class="various video-frame"><?php echo $data['Video_title'];?></a></li>
					<?php } ?>
				</ul>
			</div>
		</div>
		<?php } ?>
		<?php if(is_array($data_webs)){ ?>
		<div class="row up3">
			<div class="col-sm-3">
				<h3 class="entry-child">Web</h3>
			</div>
			<div class="col-sm-9">
				<ul class="work-info">
					<?php foreach($data_webs as $data){ ?>
					<li title="<?php echo $data['Web_desc'];?>"><a href="<?php echo correctDisplay($data['Web_link']);?>" class="various web-frame"><?php echo $data['Web_title'];?></a></li>
					<?php } ?>
				</ul>
			</div>
		</div>
		<?php } ?>
		<?php if(is_array($data_photos)){ ?>
		<div class="row up3">
			<div class="col-sm-3">
				<h3 class="entry-child">Photography</h3>
			</div>
			<div class="col-sm-9 pad0">
				<div class="row">
					<?php if(is_array($data_photos)){ foreach($data_photos as $data){ ?>
					<div class="col-xs-3"><a href="javascript:;" onclick="openCard('<?=$data['Photo_ID'];?>');" data-toggle="modal" data-target="#modal-card" class="work-info-img" title="<?php echo $data['Photo_title'];?>"><img src="<?php echo $global['img-url'].$data['Photo_ThmbImgLink'];?>" alt="<?php echo $data['Photo_title'];?>"></a></div>
					<?php }} ?>
				</div>
			</div>
		</div>
		<?php } ?>
	</div>
	<?php } ?>
	<!-- end work -->
	<!-- start footer -->
	<div class="footer-logo">
		<a href="<?php echo $global['base'];?>"><img src="<?=$global['logo-mobile'];?>" alt="logo" height="59" width="65"></a>
	</div><!-- end footer -->
</div> 