<?php 
include("packages/require.php");
include("controller/controller_resume.php");
include("controller/controller_portfolio_web.php");
$curpage='portfolio';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title><?=$data_user[0]['User_fname']." ".$data_user[0]['User_lname']."'s Portfolio";?></title>
    <meta name="keywords" content="">
    <meta name="description" content="">
    <?php include("packages/head.php");?>
    <!-- Add fancyBox main JS and CSS files -->
    <script type="text/javascript" src="<?php echo $global['resume-page'];?>js/fbox/source/jquery.fancybox.js?v=2.1.5"></script>
    <link rel="stylesheet" type="text/css" href="<?php echo $global['resume-page'];?>js/fbox/source/jquery.fancybox.css?v=2.1.5" media="screen" />
</head>
<body>
    <div id="all" class="background-cvm">
        <!-- start top nav resume -->
        <?php include("resume-top-nav.php");?>
        <!-- end top nav resume --> 
         
        <!-- start top nav resume -->
        <?php include("part-top_resume.php");?>
        <!-- end top nav resume -->
        <div class="container container-max container-resume">
            <div class="paper-cv paper-cv-portfolio">
                <div class="paper-pad paper-pad-portfolio">
                    <!-- start breadcrumb mobile -->
                    <?php include("part-mobile_breadcrumb.php");?>
                    <!-- end breadcrumb mobile -->
                    <div id="resume-profile">
                        <div class="row">
                            <div class="col-sm-2 col-xs-2 pad0-xs text-right">
                                <a href="<?=check_image_url($data_user[0]['User_proPhoto']);?>" class="resume-photo" title="<?=$data_user[0]['User_fname']." ".$data_user[0]['User_lname'];?>"><img src="<?php echo check_image_url($data_user[0]['User_proPhoto']);?>" alt="Resume picture of <?=$data_user[0]['User_fname']." ".$data_user[0]['User_lname'];?> in CIVIMI"></a>
                            </div>
                            <div class="col-sm-10 col-xs-10">
                                <div class="resume-name">
                                    <h1><?=$data_user[0]['User_fname']." ".$data_user[0]['User_lname'];?></h1>
                                    <span class="current-interest hidden-xs"><?=checkInterest($data_user[0]['User_interest']);?></span>
                                </div>
                                <div class="resume-current-job"><?=$data_user[0]['User_bioDesc'];?></div>
                                <ul class="resume-contact">
                                    <li class="address"><i class='glyphicon glyphicon-map-marker'></i>&nbsp;&nbsp;<?php if ($data_user[0]['User_city'] != ""){ ;?><?=$data_user[0]['User_city'].", ";?><?php } ?><?=$data_user[0]['User_country'];?></li>
                                    <!-- <li class="mail"><?=$data_user[0]['User_email'];?></li>
                                    <li class="phone"><?=$data_user[0]['User_phone'];?></li> -->
                                    <?php if ($data_user[0]['User_website'] != ""){ ;?>
                                    <li class="web"><i class='glyphicon glyphicon-globe'></i>&nbsp;&nbsp;<a target="_blank" title="Website of <?=$data_user[0]['User_fname']." ".$data_user[0]['User_lname'];?>" href="<?=checkLink($data_user[0]['User_website']);?>" rel="nofollow"><?=$data_user[0]['User_website'];?></a></li>
                                    <?php } ?>
                                </ul>
                                <div id="resume-share" class="visible-xs">
                                    <a onclick="centeredPopup('https://www.facebook.com/sharer/sharer.php?u=<?php echo $path["user-resume"];?><?php echo $O_username;?>','myWindow','500','300','yes');return false" href="#" class="share-resume"><img src="<?php echo $global['absolute-url'];?>img/share-facebook.png" alt="facebook"></a>
                                    <a onclick="centeredPopup('https://twitter.com/intent/tweet?url=<?php echo $path["user-resume"];?><?php echo $O_username;?>&via=civimi');return false" href="#" class="share-resume"><img src="<?php echo $global['absolute-url'];?>img/share-twitter.png" alt="twitter"></a>
                                    <a onclick="centeredPopup('https://plus.google.com/share?url=<?php echo $path["user-resume"];?><?php echo $O_username;?>','myWindow','500','300','yes');return false" href="#" class="share-resume"><img src="<?php echo $global['absolute-url'];?>img/share-google.png" alt="google"></a>
                                    <a onclick="centeredPopup('http://www.linkedin.com/shareArticle?mini=true&url=<?php echo $path["user-resume"];?><?php echo $O_username;?>&source=civimi','myWindow','500','300','yes');return false" href="#" class="share-resume"><img src="<?php echo $global['absolute-url'];?>img/share-linkedin.png" alt="linkedin"></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="portfolio-category">
                                <ul>
                                    <?php if(is_array($data_photos)){ ?>
                                    <li><a href="<?=$path['user-portfolio_image'].$O_username.".cvm";?>" class="portfolioCat-link"><i class="glyphicon glyphicon-picture"></i>Image</a></li>
                                    <?php } if(is_array($data_videos)){ ?>
                                    <li><a href="<?=$path['user-portfolio_video'].$O_username.".cvm";?>" class="portfolioCat-link"><i class="glyphicon glyphicon-facetime-video"></i>Video</a></li>
                                    <?php } if(is_array($data_docs)){ ?>
                                    <li><a href="<?=$path['user-portfolio_doc'].$O_username.".cvm";?>" class="portfolioCat-link"><i class="glyphicon glyphicon-book"></i>Document</a></li>
                                    <?php } if(is_array($data_webs)){ ?>
                                    <li><a href="<?=$path['user-portfolio_web'].$O_username.".cvm";?>" class="portfolioCat-link portfolioCat-active"><i class="glyphicon glyphicon-globe"></i>Website</a></li>
                                    <?php } ?>
                                </ul>
                            </div>
                            <?php if(is_array($data_web_cats)){ ?>
                            <div class="portfolio-folder">
                                <ul>
                                    <li><a href="<?php echo $path['user-portfolio_web'].$O_username.".cvm";?>" class="portfolioFolder-link <?php if($O_cat_id == null) { ?>portfolioFolder-active<?php } ?>">Recent</a></li>
                                    <?php foreach($data_web_cats as $data_web_cat){ ?>
                                    <li><a href="<?php echo $path['user-portfolio_web'].$data_web_cat["WebCat_ID"]."/".$O_username.".cvm";?>" class="portfolioFolder-link <?php if ($O_cat_id == $data_web_cat["WebCat_ID"]) { ?> portfolioFolder-active <?php } ?>"><?php echo $data_web_cat["WebCat_name"];?></a></li>
                                    <?php } ?>
                                </ul>
                            </div>
                            <?php } else { ?>
                            <div class="no-portfolio">There is no Web yet</div>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="hidden-xs">
                        <div class="portfolio-list">
                            <?php if(is_array($data_webs)){ $c = 0;$breaker = 3; foreach($data_webs as $data_web){ ?>
                            <?php if ($c == 0 || $c%$breaker == 0) { ?>
                            <div class="row">
                            <?php } ?>
                                <div class="col-sm-4 col-xs-6 col-portfolio-doc">
                                    <a href="<?php echo $data_web["Web_link"];?>" title="<?php echo $data_web["Web_title"];?>" class="portfolio-url-doc" onclick="storeHits(<?=$data_web["Web_ID"];?>);">
                                        <?php echo charLength(correctDisplay($data_web["Web_title"]),30);?>
                                    </a>
                                    <div class="document-desc">
                                        &nbsp;&nbsp;<?php echo charLength(correctDisplay($data_web["Web_desc"]),100);?>
                                    </div>
                                    <div class="document-read">
                                        <a href="<?php echo $data_web["Web_link"];?>" title="<?php echo $data_web["Web_title"];?>" class="read-doc" onclick="storeHits(<?=$data_web["Web_ID"];?>);">View website</a>
                                    </div>
                                </div>
                                <?php $c++; if ($c%$breaker == 0) { echo "</div>"; }?>
                                <?php  } } ?> 
                            </div>
                        </div>
                    </div>
                    <div class="visible-xs">
                        <div class="portfolio-list">
                            <?php if(is_array($data_webs)){ $d = 0;$part = 2; foreach($data_webs as $data_web){ ?>
                            <?php if ($d == 0 || $d%$part == 0) { ?>
                            <div class="row">
                            <?php } ?>
                                <div class="col-sm-4 col-xs-6 col-portfolio-doc">
                                    <a href="<?php echo $data_web["Web_link"];?>" title="<?php echo $data_web["Web_title"];?>" class="portfolio-url-doc" onclick="storeHits(<?=$data_web["Web_ID"];?>);">
                                        <?php echo charLength(correctDisplay($data_web["Web_title"]),30);?>
                                    </a>
                                    <div class="document-desc">
                                        &nbsp;&nbsp;<?php echo charLength(correctDisplay($data_web["Web_desc"]),100);?>
                                    </div>
                                    <div class="document-read">
                                        <a href="<?php echo $data_web["Web_link"];?>" title="<?php echo $data_web["Web_title"];?>" class="btn btn-info btn-xs read-doc" onclick="storeHits(<?=$data_web["Web_ID"];?>);">View</a>
                                    </div>
                                </div>
                                <?php $d++; if ($d%$part == 0) { echo "</div>"; }?>
                                <?php  } } ?> 
                            </div>
                        </div>
                    </div>
                    <!-- start footer -->
                    <div class="footer-logo">
                        <a href="<?php echo $global['base'];?>"><img src="<?=$global['logo-mobile'];?>" alt="logo" height="59" width="65"></a>
                    </div><!-- end footer -->
                </div><!--  end paper pad -->
            </div><!--  end paper cv -->
        </div><!-- end container max/resume --> 
    </div><!--  end all div -->
    <script src="<?php echo $global['resume-page'];?>js/home.js"></script>
    <script type="text/javascript">
    <?php if($message != ""){?>
        $(document).ready(function(){
            var message = "<?php echo $message;?>";
            popup(message);
        });
    <?php } ?>
    
    $(document).ready(function() {
        $(".portfolio-url-doc").fancybox({
            openEffect: 'elastic',
            closeEffect: 'elastic',
            autoSize: true,
            type: 'iframe',
            iframe: {
            preload: false // fixes issue with iframe and IE
            }
        });
    });
    $(document).ready(function() {
        $(".read-doc").fancybox({
            openEffect  : 'elastic',
            closeEffect : 'elastic',
            autoSize: true,
            type: 'iframe',
            iframe: {
            preload: false // fixes issue with iframe and IE
            }
        });
    });
    function storeHits(itemID){
        var data = { 
            user_id : "<?=$O_id;?>", 
            username : "<?=$O_username;?>",
            page : "<?=$N_page;?>",
            type : "<?=$N_type;?>",
            type_id : itemID,
            view_user : "<?=$N_view_userID;?>",
            ip : "<?=$N_ip;?>",
            ip_city : "<?=$N_city;?>",
            ip_country : "<?=$N_country;?>",
        }

        var url = "<?=$api['user-hits'];?>";
        $.ajax({type:'POST',url: url,data : data, success:function(result){
            console.log(result.message);
        }
      });
    }
    </script>
    <!-- script analytic -->
    <?php include("../packages/analytic.php");?>
</body>
</html>
