<div id="top-nav">
	<!-- start desktop nav -->
	<div id="desktop-nav" class="hidden-xs">
		<div class="container container-max">
			<div class="row">
				<div class="col-xs-12">
					<ul class="top-nav_link">
						<li><a href="<?=$path['user-resume'].$O_username.".cvm";?>" <?php if($curpage == 'resume'){?>class="active"<?php } ?>>resume / CV</a></li>
						<li id="resume-portfolio"><a href="<?=$path['user-portfolio_image'].$O_username.".cvm";?>" <?php if($curpage == 'portfolio'){?>class="active"<?php } ?>>portfolio</a></li>
						<li id="resume-pdf" style="position:relative;"><span class="badge badge-new">NEW</span><a href="<?=$path['user-resume-pdf'].$O_username.".pdf";?>">Print / Save to PDF</a></li>
						<?php if(isset($curpage)){ ?>
							<?php if(isset($_SESSION['userData']['id'])) {?>
								<?php if($_SESSION['userData']['id'] != $O_id){?>
								<li id="resume-contact" style="position:relative;">
		      						<span class="badge badge-new">NEW</span>
									<a href="#" data-toggle="modal" data-target="#modal-contact">Contact Me</a>
								</li>
								<?php } ?>
							<?php } else { $status = "For contact user, you must login first!"; $_SESSION['statusLogin'] = $status;?>
							<li id="resume-contact" style="position:relative;">
	      						<span class="badge badge-new">NEW</span>
								<a href="#" onclick="return loginFirst()">Contact Me</a>
							</li>
							<?php } ?>
						<?php } ?>
					</ul>
				</div>
				<div class="col-xs-6 hide"></div>
			</div>
		</div>
	</div><!-- end desktop nav -->
</div>
<!-- Modal -->
<div class="modal fade" id="modal-contact" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-sm" role="document" style="width:400px;max-width:100%;margin: 5% auto !important;">
    <div class="modal-content">
      <div class="modal-header mc-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">CONTACT ME</h4>
      </div>
      <div class="modal-body">
      	<div class="row">
      		<div class="col-xs-12 mc-col">
		        <div class="mc-head">
		        	<div class="mc-image"><img src="<?=check_image_url($data_user[0]['User_proPhoto']);?>" alt=""></div>
		        	<div class="mc-title">
		        		<div class="mc-name"><?=$data_user[0]['User_fname']." ".$data_user[0]['User_lname'];?></div>
		        		<div class="mc-job"><?=$data_works[0]['ATwork_title'];?> at <span><?=$data_works[0]['Company_title'];?></span></div>
		        	</div>
		        </div>
        	</div>
        </div>
        <form name="contact" action="http://www.civimi.com/resume/index.php?username=<?=$O_username;?>&action=contact_me" method="post" enctype="multipart/form-data" onsubmit="return validateContact();">
	        <div class="row">
	        	<div class="col-xs-12 pad0"><div id="validate-cf"></div></div>
	        </div>
	        <div class="row">
	        	<div class="col-sm-6 col-xs-12 mc-col up1">
	        		<input id="cf-name" name="contact_from_name" type="text" class="form-control" placeholder="name" value="<?php if(isset($_SESSION['contact_name'])){ echo $_SESSION['contact_name']; }?>" >
	        	</div>
	        	<div class="col-sm-6 col-xs-12 mc-col up1">
	        		<input id="cf-email" name="contact_from_email" type="email" class="form-control" placeholder="email" value="<?=$_SESSION['userData']['email'];?>" >
	        		<input name="contact_to_email" type="hidden" class="form-control" placeholder="email" value="<?=$O_email;?>" required>
	        	</div>
	        </div>
	        <div class="row up1">
	        	<div class="col-xs-12 mc-col">
	        		<select id="cf-purpose" class="form-control" name="contact_purpose" >
	        			<option value="">Purpose</option>
	        			<?php if(isset($_SESSION['contact_purpose'])){ ?>
	        			<option <?php if($_SESSION['contact_purpose'] == "Freelance"){echo "selected=selected";} ?> value="Freelance">Freelance</option>
	        			<option <?php if($_SESSION['contact_purpose'] == "Network"){echo "selected=selected";} ?> value="Network">Network</option>
	        			<option <?php if($_SESSION['contact_purpose'] == "Recruitment"){echo "selected=selected";} ?> value="Recruitment">Recruitment</option>
	        			<option <?php if($_SESSION['contact_purpose'] == "Partnership"){echo "selected=selected";} ?> value="Partnership">Partnership</option>
	        			<?php } else { ?>
	        			<option value="Freelance">Freelance</option>
	        			<option value="Network">Network</option>
	        			<option value="Recruitment">Recruitment</option>
	        			<option value="Partnership">Partnership</option>
	        			<?php } ?>
	        		</select>
	        	</div>
	        </div>
	        <div class="row up1">
	        	<div class="col-xs-12 mc-col">
	        		<textarea id="cf-message" class="form-control" name="contact_message" rows="3" placeholder="message" ><?php if(isset($_SESSION['contact_message'])){ echo $_SESSION['contact_message']; }?></textarea>
	        	</div>
	        </div>
	        <div class="row up1">
	        	<div class="col-xs-12 mc-col">
	        		<img src="<?php echo $global['absolute-url-captcha'];?>" alt="captcha" >
	        		<input id="cf-captcha" type="text" class="form-control up05" name="contact_captcha" placeholder="input captcha" >
	        	</div>
	        </div>
	        <div class="row up2">
	        	<div class="col-xs-12 mc-col">
	        		<input name="contact_url" type="hidden" value="<?php echo 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];?>">
	        		<input name="contact_to_user" type="hidden" value="<?=$O_id;?>">
	        		<input name="contact_username" type="hidden" value="<?=$O_username;?>">
	        		<input name="contact_from_user" type="hidden" value="<?=$_SESSION['userData']['id'];?>">
	        		<button class="btn btn-info btn-block" type="submit">Send Message</button>
	        	</div>
	        </div>
        </form>
      </div>
    </div>
  </div>
</div>
<!-- Modal Login-->
      <div class="modal fade" id="modal-login" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog mlogin-size">
          <div class="modal-content mlogin-content">
            <div class="modal-body mlogin-body">
              <div class="row">
                  <div class="col-xs-6"><div class="mlogin-header">CIVIMI Login</div></div>
                  <!--<div class="col-xs-6"><div class="mlogin-rheader">Employer Login <span style="color:#D54E55;">&gt;&gt;</span></div></div>-->
                  <div class="col-xs-12"><hr/></div>
              </div>
              <div class="row">
                  <div class="col-xs-12 col-modal-left">
                      <form name="register" action="<?=$path['user-login'];?>" method="post" enctype="multipart/form-data" >
                          <input type="email" class="form-control up15" name="email" placeholder="Email (name@example.com)" required>
                          <input type="password" class="form-control up15" name="password" placeholder="Password" required>
                          <div class="forget-pass">
                              <a href="<?=$path['forgot-password'];?>">Forget password?</a>
                          </div>
                          <button class="btn btn-info btn-block up15" type="submit">Login</button>
                      </form>
                  </div>
                  <div class="col-xs-1 col-modal-center hidden-xs"><img src="<?php echo $global['absolute-url'];?>img/or.png" alt="border"></div>
                  <div class="col-xs-12 col-modal-right">
                      <a href="http://login.facebook" onclick="this.href='<?=$path['login-with-facebook'];?>'" type="button" class="btn btn-info btn-facebook btn-block"><img src="<?php echo $global['absolute-url'];?>img/btn-facebook.png" alt="facebook">Login with Facebook</a>
                      <a href="http://login.google" onclick="this.href='<?=$path['login-with-google'];?>'" type="button" class="btn btn-info btn-google btn-block"><img src="<?php echo $global['absolute-url'];?>img/btn-google.png" alt="google">Login with Google+</a>
                      <!--<a href="http://login.twitter" onclick="this.href='<?=$path['login-with-twitter'];?>'" type="button" class="btn btn-info btn-twitter btn-block"><img src="<?php echo $global['absolute-url'];?>img/btn-twitter.png" alt="twitter">Login with Twitter</a>-->
                      <br />
                      <div class="mlogin-register">Don't have an account? <a href="<?=$path['register'];?>">Join now</a></div>
                  </div>
              </div>
            </div>
          </div>
        </div>
      </div>
<!-- popup alert -->
<div id="alert-popup" class="popup">
  <div id="popup-text" class="popup-text"></div>
  <a id="popup-close" href="javascript:;" class="popup-close"><i class="glyphicon glyphicon-remove"></i></a>
</div>
<script type="text/javascript">
function popup(message){
    $("#alert-popup").fadeIn();
    $("#popup-text").text(message);
    setTimeout(function(){
        $("#alert-popup").fadeOut();
    }, 5000);
}
$("#popup-close").click(function(){
    $("#alert-popup").fadeOut();
})
function validateContact(){
	var name = $("#cf-name").val();
	var email = $("#cf-email").val();
	var purpose = $("#cf-purpose").val();
	var message = $("#cf-message").val();
	var captcha = $("#cf-captcha").val();
	var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

	if(name != ""){
		$("#validate-cf").text("");
		$("#validate-cf").hide();
	} else {
		$("#validate-cf").text("insert your name!");
		$("#validate-cf").show();
		return false;
	}
	if(email != ""){
		if(email.match(mailformat)) {
			$("#validate-cf").text("");
			$("#validate-cf").hide();
		} else {
			$("#validate-cf").text("your email is not correct!");
			$("#validate-cf").show();
			return false;
		}
	} else {
		$("#validate-cf").text("insert your email!");
		$("#validate-cf").show();
		return false;
	}
	if(purpose != ""){
		$("#validate-cf").text("");
		$("#validate-cf").hide();
	} else {
		$("#validate-cf").text("choose your purpose!");
		$("#validate-cf").show();
		return false;
	}
	if(message != ""){
		$("#validate-cf").text("");
		$("#validate-cf").hide();
	} else {
		$("#validate-cf").text("insert your message!");
		$("#validate-cf").show();
		return false;
	}
	if(captcha != ""){
		$("#validate-cf").text("");
		$("#validate-cf").hide();
	} else {
		$("#validate-cf").text("insert captcha!");
		$("#validate-cf").show();
		return false;
	}
	return true;
}
</script>
<?php require_once("../packages/analytic.php");?>