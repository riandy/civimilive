<?php 
session_start();

if(!isset($_GET['action'])){
	require_once("../model/Connection.php");
	$obj_con = new Connection();

	require_once("../model/User.php");
	$obj_user = new User();

	require_once("../model/User_Setting.php");
	$obj_set = new User_Setting();

	require_once("../model/Album.php");
	$obj_photo_album = new Album();

	require_once("../model/Photo.php");
	$obj_photo = new Photo();

	require_once("../model/Hits_User.php");
	$obj_hu = new Hits_User();

	if($_GET['username'] != ''){
		error_reporting(E_ALL^E_NOTICE); //remove notice
		$obj_con->up();

		$O_username = mysql_real_escape_string(check_input($_GET['username']));
		$data_setting = $obj_set->get_data_by_username($O_username);
		if(is_array($data_setting)){
			$O_portolio = $data_setting[0]['Portfolio_set'];
		}else{
			$O_portolio = "Yes";
		}

		if($O_portolio == "Yes"){
			$data_user = $obj_user->get_data_by_username($O_username); //for data user
			if(is_array($data_user)){
				$O_id = $data_user[0]['User_ID'];
			}

			$data_albums = $obj_photo_album->get_album($O_id, "name");//get list of album names by a user
			$O_album_id = null;

			if(isset($_GET['id'])){
				$O_album_id = mysql_real_escape_string(check_input($_GET['id']));
			}

			if(isset($O_album_id)){
				$data_photos = $obj_photo->get_data_by_album($O_album_id); //for 
			}else{
				$data_photos = $obj_photo->get_data_recent($O_id); //for recent photo
			}

			//save in hits user
			$N_ip = getIP(); //get ip
			$userGeoData = getGeoIP($N_ip); //get data by ip address
			$N_view_userID = $_SESSION['userData']['id'];
			$N_page = "portfolio";
			$N_type = "image";
			$N_city = $userGeoData->city; //ip city
			$N_country = $userGeoData->country_name; //ip country
		}else{
			header("Location: {$global['base']}");
		}
		
		$obj_con->down();
	}
}
?>