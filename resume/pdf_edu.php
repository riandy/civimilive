<?php
//Education
if($O_eduNum>0)
{
$pdf->SetFont('Helvetica','B',12);
$pdf->Cell(46,5,'EDUCATION   ',0,0,'R');
$pdf->Ln(2);
for($i=0;$i<=$O_eduNum-1;$i++){

$pdf->SetFont('Helvetica','',10);
$pdf->MultiCell(0,5,'',0,1);

$pdf->Cell(46,5,$O_EfromMonth[$i].'/'.$O_EfromYear[$i].' - '.presentCal($O_EtoMonth[$i],$O_EtoYear[$i]).'    ',0,0,'R');
$pdf->MultiCell(0,5,$O_schoolName[$i].', '.ucwords($O_schoolCity[$i]).' '.ucwords($O_schoolCountry[$i]),0,1);

$pdf->SetFont('Helvetica','BI',10);
$pdf->Cell(46,5,'',0,0);
$pdf->MultiCell(0,5,$O_degree[$i].' in '.$O_major[$i],0,1);

$pdf->SetFont('Helvetica','I',10);

if($O_GPA[$i]!=0.00 && $O_GPA[$i] !=null && $O_GPA[$i]!='')
    {
$pdf->Cell(46,5,'',0,0);
$pdf->MultiCell(0,5,'GPA: '.$O_GPA[$i],0,1);
    }

$honor=htmlspecialchars_decode(stripslashes($O_honor[$i]),ENT_QUOTES);
    if($honor!='Rewards/ Honors' && $honor!=null && $honor!='')
    {
    $pdf->Cell(46,5,'',0,0);
    $pdf->MultiCell(0,5,"Honors: $honor",0,1);
    }
}
$pdf->Ln(4);
}//END of education part
?>
