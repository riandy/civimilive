<?php 
//Experience
if($O_workNum>0)
{
$pdf->SetFont('Helvetica','B',12);
$pdf->Cell(46,5,'EXPERIENCE   ',0,0,'R');
$pdf->Ln(2);
for($i=0;$i<=$O_workNum-1;$i++){

$pdf->SetFont('Helvetica','',10);
$pdf->MultiCell(0,5,'',0,1);

$pdf->Cell(46,5,$O_WfromMonth[$i].'/'.$O_WfromYear[$i].' - '.presentCal($O_WtoMonth[$i],$O_WtoYear[$i],$O_status[$i]).'    ',0,0,'R');
$pdf->MultiCell(0,5,$O_companyName[$i].', '.ucwords($O_companyCity[$i]).' '.ucwords($O_companyCountry[$i]),0,1);

$pdf->Cell(46,5,'',0,0);
$pdf->SetFont('Helvetica','BI',10);
$pdf->MultiCell(0,5, html_entity_decode($O_title[$i]),0,1);

$pdf->Cell(46,5,'',0,0);
$pdf->SetFont('Helvetica','',10);
$pdf->MultiCell(0,5,htmlspecialchars_decode(stripslashes($O_workDesc[$i]),ENT_QUOTES),0,1);
}
$pdf->Ln(4);
}//END of experience part
?>
