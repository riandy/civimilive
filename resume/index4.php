<?php 
include("packages/require.php");
include("controller/controller_resume.php");
$curpage='resume';
?> 
<!DOCTYPE html>
<html lang="en">
<head>
    <title><?=$data_user[0]['User_fname']." ".$data_user[0]['User_lname']."'s CV and Portfolio - ".$data_user[0]['User_bioDesc']." in ".$data_user[0]['User_city'].", ".$data_user[0]['User_country'];?></title>
    <meta name="keywords" content="<?=$data_user[0]['User_bioDesc'].' | '.$data_user[0]['User_fname'].' '.$data_user[0]['User_lname'];?>, resume, curricullum vitae, portfolio, experience, education, certificate, skills">
    <meta name="description" content="<?=$data_user[0]['User_obj'];?>">
    <meta itemprop="name" content="<?=$data_user[0]['User_fname']." ".$data_user[0]['User_lname'];?>">
    <meta itemprop="description" content="<?=$data_user[0]['User_obj'];?>">
    <meta itemprop="image" content="<?php echo check_image_url($data_user[0]['User_proPhoto']);?>">
    
    <!-- facebook and linkedin -->
    <meta prefix="og: http://ogp.me/ns#" property="og:description" content="<?=$data_user[0]['User_obj'];?>">
    <meta prefix="og: http://ogp.me/ns#" property="og:image" content="<?php echo check_image_url($data_user[0]['User_proPhoto']);?>">
    <meta prefix="og: http://ogp.me/ns#" property="og:site_name" content="CIVIMI">
    <meta prefix="og: http://ogp.me/ns#" property="og:title" content="<?=$data_user[0]['User_fname']." ".$data_user[0]['User_lname']."'s CV and Portfolio - ".$data_user[0]['User_bioDesc']." in ".$data_user[0]['User_city'].", ".$data_user[0]['User_country'];?>">
    <meta prefix="og: http://ogp.me/ns#" property="og:type" content="website">
    <meta prefix="og: http://ogp.me/ns#" property="og:url" content="<?=$path['user-resume'].$data_user[0]['User_username'].".cvm";?>">

    <!-- twitter meta -->
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:description" content="<?=$data_user[0]['User_obj'];?>">
    <meta name="twitter:title" content="<?=$data_user[0]['User_fname']." ".$data_user[0]['User_lname']."'s CV and Portfolio - ".$data_user[0]['User_bioDesc']." in ".$data_user[0]['User_city'].", ".$data_user[0]['User_country'];?>">
    <meta name="twitter:site" content="@CIVIMI">
    <meta name="twitter:creator" content="@CIVIMI">
    <meta name="twitter:image" content="<?php echo check_image_url($data_user[0]['User_proPhoto']);?>">
    <meta name="twitter:image:src" content="<?php echo check_image_url($data_user[0]['User_proPhoto']);?>">

    <?php include("packages/head.php");?>

    <link rel="stylesheet" href="<?php echo $global['resume-page'];?>stylesheets/template.css"/>

    <!-- Add fancyBox main JS and CSS files -->
    <script type="text/javascript" src="<?php echo $global['resume-page'];?>js/fbox/source/jquery.fancybox.js?v=2.1.5"></script>
    <link rel="stylesheet" type="text/css" href="<?php echo $global['resume-page'];?>js/fbox/source/jquery.fancybox.css?v=2.1.5" media="screen" />
</head>
<body>
    <div id="all" class="background-cvm">

        <!-- start top nav resume -->
        <?php include("resume-top-nav.php");?>
        <!-- end top nav resume --> 
        
        <!-- start top nav resume -->
        <?php include("part-top_resume.php");?>
        <!-- end top nav resume -->
        

        <div class="container container-max container-resume">

            <!-- start template -->
            <?php include("template/template.php");?>
            <!-- end template -->

        </div>
        <!-- end container max/resume --> 

    </div><!--  end all div -->

    <!-- START PORTFOLIO CARD-->
    <?php include("../part-portfolio_card.php");?>
    <!-- END PORTFOLIO CARD-->

    <script src="<?php echo $global['resume-page'];?>js/home.js"></script>

    <script type="text/javascript">
        $("#message1").fadeOut(8400);
        function loginFirst(){
            window.location.href = "http://www.civimi.com/beta/ERROR:_login-failed/"; 
        }
        <?php if($message != ""){?>
        $(document).ready(function(){
            var message = "<?php echo $message;?>";
            popup(message);
        });
        <?php } ?>

        function storeHits(itemID){
            var data = { 
                user_id : "<?=$O_id;?>", 
                username : "<?=$O_username;?>",
                page : "portfolio",
                type : "image",
                type_id : itemID,
                view_user : "<?=$N_view_userID;?>",
                ip : "<?=$N_ip;?>",
                ip_city : "<?=$N_city;?>",
                ip_country : "<?=$N_country;?>",
            }

            var url = "<?=$api['user-hits'];?>";
            $.ajax({type:'POST',url: url,data : data, success:function(result){
                console.log(result.message);
            }
          });
        }
    </script>
</body>
</html>
