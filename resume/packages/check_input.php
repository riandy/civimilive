<?php
//Billy - 14/6 -> pagination 1-15
function replacestr($param){
    if($param != null){
    $new_result = str_replace('/\r\n/g','<br/>',$param);
    $new_result = str_replace('/\r/g','<br/>',$param);
    $new_result = str_replace('/\n/g','<br/>',$param);
    }
    return $new_result;
}
function checkInterest($val){
    $result = "";
    if($val == "1"){
        $result = "Open for any new opportunities";
    } else if ($val == "2"){
        $result = "Available for freelance work";
    } else if ($val == "3"){
        $result = "Looking for a full-time job";
    } else if ($val == "4"){
        $result = "Looking for an internship";
    } else if ($val == "5"){
        $result = "Starting-up a company";
    } else if ($val == "6"){
        $result = "Getting higher education";
    } else if ($val == "7"){
        $result = "Finishing college";
    } else if ($val == "8"){
        $result = "Not so sure yet";
    } else {
        $result = "";
    }
    return $result;
}
function replace_rate($param){
    $rate = "";
    if($param == 1){
      $rate = "Beginner";
    } else if ($param == 2){
      $rate = "Intermediate";
    } else if ($param == 3){
      $rate = "Advanced";
    } else if ($param == 4){
      $rate = "Expert";
    }
    return $rate;
  }
function relative_time($ts)
{
    if(!ctype_digit($ts))
        $ts = strtotime($ts);

    $diff = time() - $ts;
    if($diff == 0)
        return 'now';
    elseif($diff > 0)
    {
        $day_diff = floor($diff / 86400);
        if($day_diff == 0)
        {
            if($diff < 60) return 'just now';
            if($diff < 120) return '1 minute ago';
            if($diff < 3600) return floor($diff / 60) . ' minutes ago';
            if($diff < 7200) return '1 hour ago';
            if($diff < 86400) return floor($diff / 3600) . ' hours ago';
        }
        if($day_diff == 1) return 'Yesterday';
        if($day_diff < 7) return $day_diff . ' days ago';
        if($day_diff < 31) return ceil($day_diff / 7) . ' weeks ago';
        if($day_diff < 60) return 'last month';
        return date('F Y', $ts);
    }
    else
    {
        $diff = abs($diff);
        $day_diff = floor($diff / 86400);
        if($day_diff == 0)
        {
            if($diff < 120) return 'in a minute';
            if($diff < 3600) return 'in ' . floor($diff / 60) . ' minutes';
            if($diff < 7200) return 'in an hour';
            if($diff < 86400) return 'in ' . floor($diff / 3600) . ' hours';
        }
        if($day_diff == 1) return 'Tomorrow';
        if($day_diff < 4) return date('l', $ts);
        if($day_diff < 7 + (7 - date('w'))) return 'next week';
        if(ceil($day_diff / 7) < 4) return 'in ' . ceil($day_diff / 7) . ' weeks';
        if(date('n', $ts) == date('n') + 1) return 'next month';
        return date('F Y', $ts);
    }
}

function check_select($real_value,$value){
    $result = "";
    if($real_value != null && $value != null){
	if($real_value == $value){
	    $result = " selected";
	}
    }
    return $result;
}

function check_image($image){
    $image_default = "http://apappun.com/DOKDIG/img/img-default.png";
    if(file_exists($image) || $image != null){
	$image_default = $image;
    }
    return $image_default;
}

function getBatch($page){
            $test = 5;
            $batch =1;
            $flag = false;
            while(!$flag){
		if($page > $test){
		    $test = $test + 5;
		    $batch++;
		}else{  
		    $flag = true;
		}
            }
            return $batch;
        }

function clean($string) {
		$string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
		$string = preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
		$string = strtolower($string); // Convert to lowercase
 
		return $string;
	}

function compare_search_type($array, $value){
    $len = count($array);
    $result = null;
    for($m = 0;$m < $len; $m++){
	//echo $array[$m]."-".$value.'<br/>';
	if(strcmp($array[$m],$value) == 0){
	    $result = $array[$m];
	    break;
	}
    }
    return $result;
}

function encode($param){
    $new_result = $param;
    if($param != null){
	
	$result = clean($param);
	
	$new_result = rawurlencode($result);
    }
    return $new_result;
}

function decode($param){
    $new_result = $param;
    if($param != null){
	$new_result = str_replace('-',' ',$param);
    }
    return $new_result;
}

function disease_bracket($alias){
    $text= '';
    if($alias != null && $alias != ''){
	$text = "(".$alias.")";
    }
    return $text;
}

function isSelected($real_value, $data){
    $text = "";
    if($real_value == $data){
	$text = " selected";
    }
    return $text;
}

//FUNCTION TO CHECK WHETHER TO RETURN '?' OR '&' for the next parameter in query string

function checkUrl($N_url,$param,$value){
    if(strpos($N_url,'?') || strpos($N_url,'&')){
            $url = $N_url."&".$param."=".$value;
        }else{
            $url = $N_url."?".$param."=".$value;
        }
        return $url;
}

function dropLang($newLang){
    if(strpos(curPageName(),'?lang')){
        $urlTest = substr(curPageName(),0,-2);
        $url = $urlTest."$newLang";
                                // echo "1";
    }
    else if(strpos(curPageName(),'&lang')){
        $urlTest = substr(curPageName(),0,-2);
        $url = $urlTest."$newLang";
                                // echo "2";
    }
    else if(strpos(curPageName(),'&') || strpos(curPageName(),'?')){
        $url = curPageName()."&lang=".$newLang;
                                // echo "3";
    }
    else{
        $url = curPageName()."?lang=".$newLang;
                                // echo "4";
    }
    return $url;
}

function checkSession($newLang=null){
    $timeout = 1800;
    if($newLang == null){
        $newLang = 'EN';
    }

    if($_GET['lang'] != null){
        if($_GET['lang'] != null){
            $newLang = $_GET['lang'];               
        }
        if(isset($_SESSION['lang'])){
            // session_destroy();
            unset($_SESSION['lang']);
        }
        session_start();
        $_SESSION['lang'] = $newLang;
        $_SESSION['time'] = time();
        $lang = $_SESSION['lang'];
    }
    else if($_SESSION['lang'] == null){
        if(isset($_SESSION['lang'])){
            unset($_SESSION['lang']);
        }
        session_start();
        $_SESSION['lang'] = $newLang;
        $_SESSION['time'] = time();
        $lang = $_SESSION['lang'];
    }
    else if($_GET['lang'] == null && $_SESSION['lang'] != null){
        $duration = time() - (int)$_SESSION['time'];
// echo time()."<br/>";
// echo (int)$_SESSION['time'];
        if($duration > $timeout){
            unset($_SESSION['lang']);
            $lang = checkSession($newLang);
        }
        $lang = $_SESSION['lang'];
    }

    return $lang;
}


function curPageName() {
/*     return substr($_SERVER["REQUEST_URI"],strrpos($_SERVER["SCRIPT_NAME"],"/")+1); */
    $url ="http://{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}";
    return $url;
}

//BILLY - MPJ - function to display default image on categories.php
function defaultPic($catID,$defaultIMG,$desiredIMG){
    if($catID == null || $catID == ''){
        return $defaultIMG;
    }else{
        return $desiredIMG;
    }
}


function tab($n) {
    $tabs = null;
    while ($n > 0) {
        $tabs .= "\t";
        --$n;
    }
    return $tabs;
}


function checkImage($photoThmb) {
    if ($photoThmb == null) {
        $temp = "img/img-empty.png";
    } else {
        $temp = "$photoThmb";
    }
    return $temp;
}

function checkImage1($photoThmb,$brandThmb) {
    if ($photoThmb == NULL) {
        $temp = "$brandThmb";
    } else {
        $temp = "$photoThmb";
    }
    return $temp;
}

function nowDate() {
    $date = date_create("", timezone_open('Asia/Jakarta'));
    $date = date_format($date, 'Y-m-d');
    return $date;
}

function nowDateComplete() {
    $date = date_create("", timezone_open('Asia/Jakarta'));
    $date = date_format($date, 'jS F Y - g:i A');
    return $date;
}

//Function To check and secure the login info from hacker attack
function check_input($data) {
    $data = trim($data);
    $data = stripslashes($data);
    $data = addslashes($data);
    $data = htmlspecialchars($data, ENT_QUOTES);
    return $data;
}

//Function get the extension of the document source
function getDocType($data) {
    $data = substr($data, -5);
    $pos = strpos($data, ".");

    if ($pos !== false) {
        $data = substr($data, $pos + 1);
        if ($data == "pdf" || $data == "doc" || $data == "docx") {
            return $data;
        } else {
            return "link";
        }
    } else {
        return "link";
    }
}

//Function to check "http://"
function checkLink($data) {
    $preFix = substr($data, 0, 4);
    if ($preFix != 'http') {
        $data = "http://" . $data;
    }
    return $data;
}

//function to put 3dots after lengthy string
function charLength($data, $length) {
    $strLength = strlen($data);
    if ($strLength > $length) {
        $data = substr(strip_tags($data), 0, $length) . "...";
    }
    return $data;
}

function correctDisplay($data) {
    $data = htmlspecialchars_decode(stripslashes($data), ENT_QUOTES);
    return $data;
}

//function
function checkVideoSource($data) {
    $findme = "youtube.com/watch?v=";
    $findme2 = "youtu.be/";
    $pos = strpos($data, $findme);
    $pos2 = strpos($data, $findme2);
    $dataLength = strlen($data);

    if ($pos !== false) {
        $findAmp = stripos($data, '&');

        if ($findAmp === false) {
            $data = str_replace("youtube.com/watch?v=", "youtube.com/embed/", $data);
            return $data;
        } else {
            $lengthTrimmed = $dataLength - ($findAmp);
            $data = substr($data, 0, -$lengthTrimmed);
            $data = str_replace("youtube.com/watch?v=", "youtube.com/embed/", $data);
            return $data;
        }
    } else if ($pos2 !== false) {
        $data = str_replace("youtu.be/", "youtube.com/embed/", $data);
        return $data;
    } else {
        return $data;
    }
}

function encodeURLslash($data) {
    $data = str_replace("/", "&slash", $data);
    return $data;
}

function decodeURLslash($data) {
    $data = str_replace("&slash", "/", $data);
    return $data;
}

function encodeURLspace($data) {
    $data = str_replace(" ", "%20", $data);
    return $data;
}

function curPageURL() {
    $pageURL = 'http';
    if ($_SERVER["HTTPS"] == "on") {
        $pageURL .= "s";
    }
    $pageURL .= "://";
    if ($_SERVER["SERVER_PORT"] != "80") {
        $pageURL .= $_SERVER["SERVER_NAME"] . ":" . $_SERVER["SERVER_PORT"] . $_SERVER["REQUEST_URI"];
    } else {
        $pageURL .= $_SERVER["SERVER_NAME"] . $_SERVER["REQUEST_URI"];
    }
    return $pageURL;
}

function getCurrentRating($shopID) {
    require("Shop-dbConn.php");
    $shopIDIn = mysql_real_escape_string(check_input($shopID));
    $sql = "SELECT AVG(Review_rating) as avgRating FROM T_Review WHERE Review_shopID= '$shopIDIn'";
    $result = @mysql_query($sql);
    $rs = @mysql_fetch_array($result);
    return @round($rs[avgRating], 1);
    mysql_close($dbh);
}

function countRating($shopID) {
    require("Shop-dbConn.php");
    $shopIDIn = mysql_real_escape_string(check_input($shopID));
    $sql = "SELECT COUNT(Review_rating) as countRating FROM T_Review WHERE Review_shopID= '$shopIDIn'";
    $result = @mysql_query($sql);
    $rs = @mysql_fetch_array($result);
    return $rs[countRating];
    mysql_close($dbh);
}

function getCurrentProduct($shopID) {
    require("Shop-dbConn.php");
    $shopIDIn = mysql_real_escape_string(check_input($shopID));
    $sql = "SELECT Product_ID, Product_name FROM T_Product WHERE Product_userID= '$shopIDIn'  ORDER BY Product_name ASC";
    $result = @mysql_query($sql);
    $productCounter = 0;
    while ($row = mysql_fetch_array($result, MYSQL_ASSOC)) {
        $O_productID[$productCounter] = correctDisplay($row['Product_ID']);
        $O_productName[$productCounter] = correctDisplay($row['Product_name']);
        echo "<option value='$O_productID[$productCounter]'>$O_productName[$productCounter]</option>";
        $productCounter++;
    }
//return $productCounter;
    mysql_close($dbh);
}

function getRatingColor($rating) {
    if ($rating == null || $rating == '' || $rating == 0) {
        $rating = '?';
        $color = "#3366cc";
    } else if ($rating < 3) {
        $color = "#CA0606";
    } else if ($rating >= 4) {
        $color = "#64AA2B";
    } else {
        $color = "#999";
    }
    $result = "<font color='$color'>$rating</font>";
    return $result;
}

function getRatingTitle($rating) {
    if ($rating == '?' || $rating == null || $rating == '' || $rating == 0) {
        $title = "Be the first person to review...";
    } else if ($rating < 3) {
        $title = "Need Improvements";
    } else if ($rating >= 4) {
        $title = "Recommended Seller";
    } else {
        $title = "So Far So Good";
    }
    return $title;
}

function checkPlural($data) {
    if ($data > 1) {
        $plural = 's';
    }
    return $plural;
}

function doHash($secData) {
//creates a random 5 character sequence
    $salt = substr(md5(time()), 0, 5);
    $secData = hash('sha256', $salt . $secData);
    return $secData;
}

?>