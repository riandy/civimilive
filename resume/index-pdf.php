<?php 
require("fpdf.php");

$root = substr($_SERVER['DOCUMENT_ROOT'], 0, strrpos($_SERVER['DOCUMENT_ROOT'],'/'));
$rootSite= "http://www.civimi.com";

require_once("../packages/check_input.php");//checking input
//require_once("$root/hash_civimi.php");

require_once("../model/Connection.php");
$obj_con = new Connection();

$action = $_GET['action'];
$type = $_GET['type'];
if($type==null||$type==''){$type='resume';}//this will be change to user preferences
//if($action==null||$action==''){$action='openPhoto';}//this will be change to user preferences

//if username is requested, try fetching the data from DB
if(($_GET['username'] != null || $_GET['username'] != '')&&($type == 'resume'))
{
    $obj_con->up();
    $N_username = mysql_real_escape_string(check_input($_GET['username']));
    
    //Bio/ profile
    $queryBio = mysql_query("SELECT User_ID, User_fname, User_lname, User_DOB, User_bioDesc, User_interest, User_obj, User_address, User_country, User_phone, User_emailShow, User_proPhoto, User_proPhotoThmb, User_website, User_deactivate FROM T_User WHERE User_username = '$N_username'");
    if (mysql_num_rows($queryBio) == 1) 
	{
        $rowOpen=mysql_fetch_array($queryBio);
        $O_ID= $rowOpen['User_ID'];
        $O_fName= ucwords($rowOpen['User_fname']);
        $O_lName= ucwords($rowOpen['User_lname']);
        $O_interest= $rowOpen['User_interest'];
        $O_desc= htmlspecialchars_decode(correctDisplay($rowOpen['User_bioDesc']), ENT_QUOTES);
        $O_obj= htmlspecialchars_decode(correctDisplay($rowOpen['User_obj']), ENT_QUOTES);

        $O_address= correctDisplay($rowOpen['User_address']);
        $O_address = iconv('UTF-8', 'windows-1252', $O_address);
        $O_country= $rowOpen['User_country'];
        $O_phone= $rowOpen['User_phone'];
        $O_emailShow= $rowOpen['User_emailShow'];
        $O_website= $rowOpen['User_website'];
        $O_proPhoto= $rowOpen['User_proPhoto'];
        $O_proPhotoThmb= $rowOpen['User_proPhotoThmb'];
        $O_accountDelete= $rowOpen['User_deactivate'];
        
        if($O_fName == null && $O_lName == null)
        {
           header("Location: $rootSite/index.php");
           die();
        }  
        
        if($O_accountDelete==1)
        {
           header("Location: $rootSite/index.php");
           die();
        }
        
        //Setting
        $querySetting = mysql_query("SELECT Setting_resume, Setting_resumeCode, Setting_portfolio, Setting_objSort, Setting_eduSort, Setting_expSort, Setting_skillSort, Setting_workSort FROM T_Setting WHERE User_ID = '$O_ID'");
        while($row = mysql_fetch_array($querySetting,MYSQL_ASSOC))
	    {
            $O_resumeSetting = correctDisplay($row['Setting_resume']);
            $O_resumeCode = $row['Setting_resumeCode'];
            $O_portfolioSetting= correctDisplay($row['Setting_portfolio']);
            $O_objSort = $row['Setting_objSort'];
            $O_eduSort = $row['Setting_eduSort'];
            $O_expSort = $row['Setting_expSort'];
            $O_skiSort = $row['Setting_skillSort'];
            $O_worSort = $row['Setting_workSort'];
            $k++;
            }
        
         if($O_resumeSetting==1)
         {
             header("Location: $rootSite/index.php");
             die();
         }
         else if($O_resumeCode!=null && $O_resumeCode!='')
         {
             $resumeCodeEx0 = $_REQUEST['code'];
             $resumeCodeEx1 = doHash($resumeCodeEx0,$salt);
             $resumeCodeEx2 = doHash($O_resumeCode,$salt);
             
             if($resumeCodeEx0==null || $resumeCodeEx0=='')
             {
             header("Location: $rootSite/resumeAccessCode.php?username=$N_username");
             die();
             }
             else if($resumeCodeEx1!=$resumeCodeEx2)
             {
             header("Location: $rootSite/unauthorized.php");
             die(); 
             }
         }
        
        //Social
        $querySocial = mysql_query("SELECT * FROM T_Social WHERE User_ID = '$O_ID' ORDER BY Social_type ASC ");
        $k = 0;
        if (mysql_num_rows($querySocial) >= 1) 
	{
        while($rowOpen = mysql_fetch_array($querySocial,MYSQL_ASSOC))
	    {
            $O_socialID[$k]= $rowOpen['Social_ID'];
            $O_socialName[$k]= correctDisplay($rowOpen['Social_name']);
            $O_socialLink[$k]= $rowOpen['Social_link'];
            $O_socialType[$k]= correctDisplay($rowOpen['Social_type']);
            $k++;
            }
            $O_socialNum = $k;
        }
        
        //education
        $queryEdu = mysql_query("SELECT * FROM AT_Edu, T_Edu WHERE AT_Edu.Edu_ID = T_Edu.Edu_ID AND AT_Edu.User_ID='$O_ID' ORDER BY AT_Edu.ATedu_fromYear DESC, AT_Edu.ATedu_fromMonth DESC  ");
        $k = 0;
        if (mysql_num_rows($queryEdu) >= 1) 
            {
            while($rowOpen = mysql_fetch_array($queryEdu,MYSQL_ASSOC))
                {
        $O_schoolName[$k]= iconv('UTF-8', 'windows-1252',correctDisplay($rowOpen['Edu_schoolName']));
        $O_schoolCity[$k]= iconv('UTF-8', 'windows-1252',correctDisplay($rowOpen['Edu_schoolCity']));
        $O_schoolCountry[$k]= correctDisplay($rowOpen['Edu_schoolCountry']);
        
        $O_ATID[$k]= $rowOpen['ATedu_ID'];
        $O_degree[$k]= iconv('UTF-8', 'windows-1252',correctDisplay($rowOpen['ATedu_degree']));
        $O_major[$k]= iconv('UTF-8', 'windows-1252',correctDisplay($rowOpen['ATedu_major']));
        $O_EfromMonth[$k]= $rowOpen['ATedu_fromMonth'];
        $O_EfromYear[$k]= $rowOpen['ATedu_fromYear'];
        $O_EtoMonth[$k]= $rowOpen['ATedu_toMonth'];
        $O_EtoYear[$k]= $rowOpen['ATedu_toYear'];
        $O_GPA[$k]= $rowOpen['ATedu_GPA'];
        $O_honor[$k]= iconv('UTF-8', 'windows-1252',correctDisplay($rowOpen['ATedu_honors']));  
        $k++;
                }
            $O_eduNum = $k;
            }
            
        //work
        $queryWork = mysql_query("SELECT * FROM T_Work WHERE User_ID = '$O_ID' ORDER BY Work_status DESC, Work_fromYear DESC ");
        $k = 0;
        if (mysql_num_rows($queryWork) >= 1) 
	{
        while($rowOpen = mysql_fetch_array($queryWork,MYSQL_ASSOC))
	    {
        $O_companyName[$k]= iconv('UTF-8', 'windows-1252',correctDisplay($rowOpen['Work_company']));
        $O_companyCity[$k]= iconv('UTF-8', 'windows-1252',correctDisplay($rowOpen['Work_city']));
        $O_companyCountry[$k]= correctDisplay($rowOpen['Work_country']);
        
        $O_workID[$k]= $rowOpen['Work_ID'];
        $O_title[$k]= iconv('UTF-8', 'windows-1252',correctDisplay($rowOpen['Work_title']));
        $O_WfromMonth[$k]= $rowOpen['Work_fromMonth'];
        $O_WfromYear[$k]= $rowOpen['Work_fromYear'];
        $O_WtoMonth[$k]= $rowOpen['Work_toMonth'];
        $O_WtoYear[$k]= $rowOpen['Work_toYear'];
        $O_status[$k]= $rowOpen['Work_status'];
        $O_workDesc[$k]= iconv('UTF-8', 'windows-1252',correctDisplay($rowOpen['Work_desc']));
        $k++;
        }
        $O_workNum = $k;
        }
        
        //skills
        $querySkill = mysql_query("SELECT * FROM AT_Skill, T_Skill WHERE AT_Skill.Skill_ID = T_Skill.Skill_ID AND User_ID = '$O_ID' ORDER BY T_Skill.Skill_name ASC ");
        $k = 0;
        if (mysql_num_rows($querySkill) >= 1) 
	{
        while($rowOpen = mysql_fetch_array($querySkill,MYSQL_ASSOC))
	    {
        $O_skillName[$k]= correctDisplay($rowOpen['Skill_name']);
        $O_skillID[$k]= $rowOpen['Skill_ID'];
        $O_skillDesc[$k]= correctDisplay($rowOpen['ATskill_desc']);
        $k++;
        }
        $O_skillNum = $k;
        }
        
    $obj_con->down();
        }
}
else
{
    header("Location: $rootSite/index.php");
    die();
}

function presentCal($dataM,$dataY,$dataStatus='1')
{
    if(($dataM==0&&$dataY==0)||$dataStatus==2)
        {$result="Present";}
        else if($dataY==0){$result="Present";}
        else
        {
            if($dataM==0){$dataM=1;}
            $result="$dataM/$dataY";}
        return $result;
}

class PDF extends FPDF
{
// Page footer
function Footer()
{
    // Position at 1.5 cm from bottom
    $this->SetY(-15);
    // Logo
    $this->Image("http://www.civimi.com/img/icons/civimi-icon-med.png",100,278,10);
    // Arial italic 8
    $this->SetFont('Helvetica','B',8);
    //$this->Cell(0,10,'powered by civimi',0,0,'C');
    $totPageNum='{nb}';

    $this->Ln(4);
    // Page number
    $this->Cell(0,10,' '.$this->PageNo(),0,0,'C');
  
}
}

// Instanciation of inherited class
$pdf = new PDF();
$pdf->AliasNbPages();
$pdf->AddPage();

$pdf->Ln(6);

if($O_proPhotoThmb!= null && $O_proPhoto!=null)
{
// Logo
$profilePhoto=encodeURLspace($O_proPhotoThmb);
$pdf->Image("http://www.civimi.com/$profilePhoto",24,16,28);
//Bio info
$pdf->SetFont('Helvetica','B',16);
$pdf->Cell(46,5,'',0,0,'R');
$pdf->Cell(0,6,$O_fName.' '.$O_lName,0,1);

$pdf->SetFont('Helvetica','BI',12);
$pdf->Cell(46,5,'',0,0,'R');
$pdf->Cell(0,6,$O_desc,0,1);

$pdf->SetFont('Helvetica','',10);
$pdf->Cell(46,5,'',0,0,'R');
$pdf->Cell(0,5,$O_address.' '.$O_country,0,1);
$pdf->Cell(46,5,'',0,0,'R');
$pdf->Cell(0,5,$O_emailShow,0,1);
$pdf->Cell(46,5,'',0,0,'R');
$pdf->Cell(0,5,$O_phone,0,1);
$pdf->Cell(46,5,'',0,0,'R');
$pdf->Cell(0,5,$O_website,0,1);
}
else
{
 //Bio info
$pdf->SetFont('Helvetica','B',16);
$pdf->Cell(20,5,'',0,0,'R');
$pdf->Cell(0,6,$O_fName.' '.$O_lName,0,1);

$pdf->SetFont('Helvetica','BI',12);
$pdf->Cell(20,5,'',0,0,'R');
$pdf->Cell(0,6,$O_desc,0,1);

$pdf->SetFont('Helvetica','',10);
$pdf->Cell(20,5,'',0,0,'R');
$pdf->Cell(0,5,$O_address.' '.$O_country,0,1);
$pdf->Cell(20,5,'',0,0,'R');
$pdf->Cell(0,5,$O_emailShow,0,1);
$pdf->Cell(20,5,'',0,0,'R');
$pdf->Cell(0,5,$O_phone,0,1);
$pdf->Cell(20,5,'',0,0,'R');
$pdf->Cell(0,5,$O_website,0,1);   
}

$pdf->Ln(8);

require_once("sortOrderPDF.php");

$pdf->Output("$O_fName-$O_lName-civimi.pdf", "I");
?>