<?php 
include("packages/require.php");
include("controller/controller_resume.php");
$curpage='resume';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title><?=$data_user[0]['User_fname']." ".$data_user[0]['User_lname']."'s CV and Portfolio - ".$data_user[0]['User_bioDesc']." in ".$data_user[0]['User_city'].", ".$data_user[0]['User_country'];?></title>
    <meta name="keywords" content="<?=$data_user[0]['User_bioDesc'].' | '.$data_user[0]['User_fname'].' '.$data_user[0]['User_lname'];?>, resume, curricullum vitae, portfolio, experience, education, certificate, skills">
    <meta name="description" content="<?=$data_user[0]['User_obj'];?>">
    <meta itemprop="name" content="<?=$data_user[0]['User_fname']." ".$data_user[0]['User_lname'];?>">
    <meta itemprop="description" content="<?=$data_user[0]['User_obj'];?>">
    <meta itemprop="image" content="<?php echo check_image_url($data_user[0]['User_proPhoto']);?>">
    
    <!-- facebook and linkedin -->
    <meta prefix="og: http://ogp.me/ns#" property="og:description" content="<?=$data_user[0]['User_obj'];?>">
    <meta prefix="og: http://ogp.me/ns#" property="og:image" content="<?php echo check_image_url($data_user[0]['User_proPhoto']);?>">
    <meta prefix="og: http://ogp.me/ns#" property="og:site_name" content="CIVIMI">
    <meta prefix="og: http://ogp.me/ns#" property="og:title" content="<?=$data_user[0]['User_fname']." ".$data_user[0]['User_lname']."'s CV and Portfolio - ".$data_user[0]['User_bioDesc']." in ".$data_user[0]['User_city'].", ".$data_user[0]['User_country'];?>">
    <meta prefix="og: http://ogp.me/ns#" property="og:type" content="website">
    <meta prefix="og: http://ogp.me/ns#" property="og:url" content="<?=$path['user-resume'].$data_user[0]['User_username'].".cvm";?>">

    <!-- twitter meta -->
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:description" content="<?=$data_user[0]['User_obj'];?>">
    <meta name="twitter:title" content="<?=$data_user[0]['User_fname']." ".$data_user[0]['User_lname']."'s CV and Portfolio - ".$data_user[0]['User_bioDesc']." in ".$data_user[0]['User_city'].", ".$data_user[0]['User_country'];?>">
    <meta name="twitter:site" content="@CIVIMI">
    <meta name="twitter:creator" content="@CIVIMI">
    <meta name="twitter:image" content="<?php echo check_image_url($data_user[0]['User_proPhoto']);?>">
    <meta name="twitter:image:src" content="<?php echo check_image_url($data_user[0]['User_proPhoto']);?>">
    <?php include("packages/head.php");?>
    <!-- Add fancyBox main JS and CSS files -->
    <script type="text/javascript" src="<?php echo $global['resume-page'];?>js/fbox/source/jquery.fancybox.js?v=2.1.5"></script>
    <link rel="stylesheet" type="text/css" href="<?php echo $global['resume-page'];?>js/fbox/source/jquery.fancybox.css?v=2.1.5" media="screen" />
    <link href="<?php echo $global['absolute-url'];?>css/bootstrap-tour.min.css" rel="stylesheet">
    <script src="<?php echo $global['absolute-url'];?>js/bootstrap-tour.min.js"></script>
    <style type="text/css">
        #resume-share > span {
            font-size: 14px;
            font-weight: bold;
            position: relative;
            top: 1px;
            margin-right: 5px;
            color: #3E3D3D; 
        }
        .share-resume > img {
            width: 18px;
            margin-right: 5px;
        }
    </style>
    <script language="javascript">
    var popupWindow = null;
    function centeredPopup(url,winName,w,h,scroll){
        LeftPosition = (screen.width) ? (screen.width-w)/2 : 0;
        TopPosition = (screen.height) ? (screen.height-h)/2 : 0;
        settings ='height='+h+',width='+w+',top='+TopPosition+',left='+LeftPosition+',scrollbars='+scroll+',resizable'
        popupWindow = window.open(url,winName,settings)
    }
    </script>
</head>
<body>
    <div id="all" class="background-cvm"> 
        <!-- start top nav resume -->
        <?php include("resume-top-nav.php");?>
        <!-- end top nav resume --> 
        
        <!-- start top nav resume -->
        <?php include("part-top_resume.php");?>
        <!-- end top nav resume -->
        <div class="container container-max container-resume">
            <div id="resume-paper" class="paper-cv paper-cv-portfolio">
                <!--<div class="row">
                    <div class="col-xs-12 text-right">
                        <a href="#" class="pdf-link hidden-xs">print as pdf</a>
                    </div>
                </div>-->
                <div class="paper-pad">
                    <!-- start breadcrumb mobile -->
                    <?php include("part-mobile_breadcrumb.php");?>
                    <!-- end breadcrumb mobile -->
                    <div class="up2 visible-xs"></div>
                    <div id="resume-profile">
                        <div class="row">
                            <div class="col-sm-3 col-xs-4 pad0-xs text-right">
                                <a href="<?=check_image_url($data_user[0]['User_proPhoto']);?>" class="resume-photo" title="<?=$data_user[0]['User_fname']." ".$data_user[0]['User_lname'];?>"><img src="<?php echo check_image_url($data_user[0]['User_proPhoto']);?>" alt="Resume picture of <?=$data_user[0]['User_fname']." ".$data_user[0]['User_lname'];?> in CIVIMI"></a>
                            </div>
                            <div class="col-sm-9 col-xs-8">
                                <div class="resume-name">
                                    <h1><?=$data_user[0]['User_fname']." ".$data_user[0]['User_lname'];?></h1>
                                    <span class="current-interest hidden-xs"><?=checkInterest($data_user[0]['User_interest']);?></span>
                                </div>
                                <div class="resume-current-job"><?=$data_user[0]['User_bioDesc'];?></div>
                                <ul class="resume-contact">
                                    <li class="address"><i class='glyphicon glyphicon-map-marker'></i>&nbsp;&nbsp;<?php if ($data_user[0]['User_city'] != ""){ ;?><?=$data_user[0]['User_city'].", ";?><?php } ?><?=$data_user[0]['User_country'];?></li>
                                    <!-- <li class="mail"><?=$data_user[0]['User_email'];?></li>
                                    <li class="phone"><?=$data_user[0]['User_phone'];?></li> -->
                                    <?php if ($data_user[0]['User_website'] != ""){ ;?>
                                    <li class="web">
                                        <i class='glyphicon glyphicon-globe'></i>&nbsp;&nbsp;
                                        <a target="_blank" title="Website of <?=$data_user[0]['User_fname']." ".$data_user[0]['User_lname'];?>" href="<?=checkLink($data_user[0]['User_website']);?>" rel="nofollow"><?=$data_user[0]['User_website'];?></a>
                                    </li>
                                    <?php } ?>
                                </ul>
                                <div id="resume-share" class="visible-xs">
                                    <a onclick="centeredPopup('https://www.facebook.com/sharer/sharer.php?u=<?php echo $path["user-resume"];?><?php echo $O_username;?>','myWindow','500','300','yes');return false" href="#" class="share-resume"><img src="<?php echo $global['absolute-url'];?>img/share-facebook.png" alt="facebook"></a>
                                    <a onclick="centeredPopup('https://twitter.com/intent/tweet?url=<?php echo $path["user-resume"];?><?php echo $O_username;?>&via=civimi');return false" href="#" class="share-resume"><img src="<?php echo $global['absolute-url'];?>img/share-twitter.png" alt="twitter"></a>
                                    <a onclick="centeredPopup('https://plus.google.com/share?url=<?php echo $path["user-resume"];?><?php echo $O_username;?>','myWindow','500','300','yes');return false" href="#" class="share-resume"><img src="<?php echo $global['absolute-url'];?>img/share-google.png" alt="google"></a>
                                    <a onclick="centeredPopup('http://www.linkedin.com/shareArticle?mini=true&url=<?php echo $path["user-resume"];?><?php echo $O_username;?>&source=civimi','myWindow','500','300','yes');return false" href="#" class="share-resume"><img src="<?php echo $global['absolute-url'];?>img/share-linkedin.png" alt="linkedin"></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- start desktop view -->
                    <?php include("part-resume_desktop.php");?>
                    <!-- end desktop view -->
                </div><!--  end paper pad -->
            </div><!--  end paper cv -->
            <!-- mobile view -->
            <?php include("part-resume_mobile.php");?>
            <!-- end mobile view -->
        </div><!-- end container max/resume --> 
    </div><!--  end all div -->
    <!-- start back to top -->
    <?php include("../part-back_top.php");?>
    <!-- end back to top -->

    <!-- START PORTFOLIO CARD-->
    <?php include("../part-portfolio_card.php");?>
    <!-- END PORTFOLIO CARD-->
    
    <script src="<?php echo $global['resume-page'];?>js/home.js"></script>
    <script type="text/javascript">
        $("#message1").fadeOut(8400);
        function loginFirst(){
            window.location.href = "http://www.civimi.com/ERROR:_login-failed/"; 
        }
        <?php if($message != ""){?>
        $(document).ready(function(){
            var message = "<?php echo $message;?>";
            popup(message);
        });
        <?php } ?>

        function storeHits(itemID){
            var data = { 
                user_id : "<?=$O_id;?>", 
                username : "<?=$O_username;?>",
                page : "portfolio",
                type : "image",
                type_id : itemID,
                view_user : "<?=$N_view_userID;?>",
                ip : "<?=$N_ip;?>",
                ip_city : "<?=$N_city;?>",
                ip_country : "<?=$N_country;?>",
            }

            var url = "<?=$api['user-hits'];?>";
            $.ajax({type:'POST',url: url,data : data, success:function(result){
                console.log(result.message);
            }
          });
        }
    </script>
</body>
</html>
