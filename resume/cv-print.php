<?php 
include("packages/require.php");
include("controller/controller_resume.php");
$curpage='resume';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style type="text/css">
        body{
            font-family: 'Proxima Nova','Helvetica Neue','Segoe UI','trebuchet ms',arial;
        }
        h1, h2, h3, h4, h5, h6 {
            margin: 0;
            padding: 0;
        }
        .paper-pad{
            padding: 20px;
            margin: 0 auto;
            display: table;
        }        
        .row{
            width: 100%;
        }
        .left-side{
            width: 200px;
            vertical-align: top;
            padding: 0 15px;
        }
        .right-side{
            width: 400px;
            padding: 0 15px;
            vertical-align: top;
            padding-right: 15px;
            max-width: 100%;
        }
        .toppad2{
            padding-top: 20px;
        }
        .toppad05{
            padding-top: 5px;
        }
        .resume-photo {
            width: 120px;
            height: 120px;
            border: 1px solid #ddd;
        }
        .resume-name > h1 { 
            color: #666;    
            font-size: 30px;
            display: inline-block;
            font-weight: lighter;
            margin-bottom: 5px;
        }
        .resume-current-job {
            font-size: 22px;          
            margin: 5px 0 0 0;
            color: #4E5F69;
            font-weight: bold;
        }
        .resume-contact{
            margin: 10px 0 0 0;
            padding: 0;
        }
        .resume-contact > li {
            background-position: 0 .1em;
            background-repeat: no-repeat;
            display: block;
            height: 25px;
            margin-top: -2px;
        }
        .resume-contact > .mail{
            background-image: url(img/icon/icn-mail.gif);
        }
        .resume-contact > .phone{
            background-image: url(img/icon/icn-tel.gif);
        }
        .entry-head {
            color: #4E5F69;
            font-size: 25px;
            text-align: right;
        }
        .objective-desc {
            font-size: 14px;
            line-height: 120%;
        }
        .entry-child {
            color: #7d6a6c;
            font-size: 13px;
            text-align: right;
        }
        .entry-detail{
            font-size: 14px;
            margin: 0;
        }
        em {
            color: #666;
            display: block;
            font-family: 'Open Sans','Proxima Nova','Helvetica Neue','Segoe UI','trebuchet ms',sans-serif;
            font-size: 12px;
        }
        .experience-info {
            font-size: 12px;
            text-decoration: none;
            display: block;
        }
        .skills-info{
            list-style-type: none;
            margin: 0;
            padding: 0;
        }
        .skills-info > li{
            display: inline-block;
            margin: 0 5px 5px 0;
            width: 30%;
            font-size: 14px;
            vertical-align: top;
        }
        .work-info {
            list-style-type: none;
            margin: 0;
            padding: 0;
        }
        .work-info > li {
            background-image: url(../img/icon/bullet.gif);
            background-position: 0 .5em;
            background-repeat: no-repeat;
            display: inline-block;
            margin: 0 0 6px;
            padding: 0 10px 0 10px;
            width: 50%;
        }
        .work-info > li > a{
            color: #36c;
            font-style: italic;
        }
        .work-info-img {
            list-style-type: none;
        }
        .work-info-img > li {
            margin: 0;
            padding: 0;
        }
        .work-info-img > li > a > img {
            background-color: #fff;
            border: 1px solid #4E5F69;
            height: 95px;
            margin-bottom: 16px;
            margin-right: 20px;
            padding: 4px;
            width: 95px;
        }
        .work-info-img {
            display: block;
            background-color: #fff;
            border: 1px solid #ddd;
            margin-bottom: 15px;
            border-radius: 2px;
        }
        .work-info-img > img {
            width: 100%;
        }
        .footer-pos{
            position: fixed;
            left: 0;
            right: 0;
            bottom: 15px;
            text-align: center;
            width: 100%;
        }
        .footer-logo {
            width: 55px;
            height: 50px;
        }
        .col-photo{
            display: inline-block;
            width: 25%;
            margin-left: -4px;
            padding: 0 15px;
        }
        @page { margin: 30px 20px 50px 20px; }
    </style>
</head>
<body>
    <table class="paper-pad">
        <tr>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr class="row">
            <td class="left-side" style="text-align: right;">
                <img class="resume-photo" src="<?php echo check_image_url($data_user[0]['User_proPhotoThmb']);?>" alt="image">
            </td>
            <td class="right-side" colspan="2">
                <div class="resume-name">
                    <h1><?=$data_user[0]['User_fname']." ".$data_user[0]['User_lname'];?></h1>
                </div>
                <div class="resume-current-job"><?=$data_user[0]['User_bioDesc'];?></div>
                <ul class="resume-contact">
                    <li class="address"><?php if ($data_user[0]['User_city'] != ""){ ;?><?=$data_user[0]['User_city'].", ";?><?php } ?><?=$data_user[0]['User_country'];?></li>
                    <?php if ($data_user[0]['User_website'] != ""){ ;?>
                    <li class="web">
                        <?=$data_user[0]['User_website'];?>
                    </li>
                    <?php } ?>
                </ul>
            </td>
        </tr>

        <!-- start objective -->
        <?php if($data_user[0]['User_obj'] != ""){ ?>
        <tr class="row" >
            <td class="left-side toppad2">
                <h2 class="entry-head">OBJECTIVE</h2>
            </td>
            <td class="right-side toppad2" colspan="2">
                <div class="objective-desc">
                    <?=correctText($data_user[0]['User_obj']);?>
                </div>
            </td>
        </tr>
        <?php } ?>
        <!-- end objective -->

        <!-- start objective -->
        <?php if(is_array($data_edus)){ ?>
        <tr class="row">
            <td class="left-side toppad2">
                <h2 class="entry-head">EDUCATION</h2>
            </td>
            <td class="right-side toppad2" colspan="2"></td>
        </tr>
        <?php foreach($data_edus as $data_edu){ ?>
        <tr class="row">
            <td class="left-side toppad05">
                <h3 class="entry-child">
                    <?=$data_edu['ATedu_fromMonth']."/".$data_edu['ATedu_fromYear'];?> - <?php if($data_edu['ATedu_toMonth'] != "0"){ echo $data_edu['ATedu_toMonth']."/".$data_edu['ATedu_toYear']; } else { echo "Present";}?>
                </h3>
            </td>
            <td class="right-side toppad05" colspan="2">
                <div class="entry-detail">
                    <strong><?=$data_edu['Edu_schoolName'];?></strong>, <?=$data_edu['Edu_schoolCity']." ".$data_edu['Edu_schoolCountry'];?><br>
                    <em><?=$data_edu["ATedu_degree"]."&nbsp;in ".$data_edu["ATedu_major"];?></em>
                    <?php if($data_edu['ATedu_GPA'] != 0){ ?><em>GPA: <?=$data_edu['ATedu_GPA'];?></em><?php } ?>
                </div>
            </td>
        </tr>
        <?php } ?>
        <?php } ?>
        <!-- end objective -->

        <!-- start experience -->
        <?php if(is_array($data_works)){ ?>
        <tr class="row">
            <td class="left-side toppad2">
                <h2 class="entry-head">EXPERIENCE</h2>
            </td>
            <td class="right-side toppad2" colspan="2"></td>
        </tr>
        <?php foreach($data_works as $data_work){ ?>
        <tr class="row">
            <td class="left-side toppad05">
                <h3 class="entry-child">
                    <?=$data_work['ATwork_from_month']."/".$data_work['ATwork_from_year'];?> - <?php if($data_work['ATwork_to_month'] != "0"){ echo $data_work['ATwork_to_month']."/".$data_work['ATwork_to_year']; } else { echo "Present";}?>
                </h3>
            </td>
            <td class="right-side toppad05" colspan="2">
                <div class="entry-detail">
                    <strong><?=$data_work['Company_title'];?></strong>, <?=$data_work['City_title']." ".$data_work['Country_title'];?><br>
                    <em><?=$data_work['ATwork_title'];?></em>
                    <p class="experience-info">
                        <?php echo correctText($data_work['ATwork_desc']);?>
                    </p>
                </div>
            </td>
        </tr>
        <?php } ?>
        <?php } ?>
        <!-- end experience -->

        <!-- start organization -->
        <?php if(is_array($data_orgs)){ ?>
        <tr class="row">
            <td class="left-side toppad2">
                <h2 class="entry-head">ORGANIZATION</h2>
            </td>
            <td class="right-side toppad2" colspan="2"></td>
        </tr>
        <?php foreach($data_orgs as $data_org){ ?>
        <tr class="row">
            <td class="left-side toppad05">
                <h3 class="entry-child">
                    <?=$data_org['ATorg_from_month']."/".$data_org['ATorg_from_year'];?> - <?php if($data_org['ATorg_to_month'] != "0"){ echo $data_org['ATorg_to_month']."/".$data_org['ATorg_to_year']; } else { echo "Present";}?>
                </h3>
            </td>
            <td class="right-side toppad05" colspan="2">
                <div class="entry-detail">
                    <strong><?=$data_org['Org_title'];?></strong>, <?=$data_org['City_title']." ".$data_org['Country_title'];?><br>
                    <em><?=$data_org['ATorg_title'];?></em>
                    <p class="experience-info">
                        <?php echo correctText($data_org['ATorg_desc']);?>
                    </p>
                </div>
            </td>
        </tr>
        <?php } ?>
        <?php } ?>
        <!-- end organization -->

        <!-- start skill -->
        <?php if(is_array($data_skills)){ ?>
        <tr class="row">
            <td class="left-side toppad2">
                <h2 class="entry-head">SKILL</h2>
            </td>
            <td class="right-side toppad2" colspan="2"></td>
        </tr>
        <tr class="row">
            <td class="left-side toppad05">
                <h3 class="entry-child">Abilities</h3>
            </td>
            <td class="right-side toppad05" colspan="2">
                <ul class="skills-info">
                    <?php $num=1; foreach($data_skills as $data_skill){ ?>
                    <li>- <?=$data_skill['Skill_name'];?></li>
                    <?php if ($num % 3 == 0) { echo "</ul><ul class='skills-info'>";} $num++; } ?>
                </ul>
            </td>
        </tr>
        <?php } ?>
        <!-- end skill -->

        <!-- start language -->
        <?php if(is_array($data_langs)){ ?>
        <tr class="row">
            <td class="left-side toppad2">
                <h2 class="entry-head">LANGUAGE</h2>
            </td>
            <td class="right-side toppad2" colspan="2"></td>
        </tr>
        <tr class="row">
            <td class="left-side toppad05">
                <h3 class="entry-child">Abilities</h3>
            </td>
            <td class="right-side toppad05" colspan="2">
                <ul class="skills-info">
                    <?php $num=1; foreach($data_langs as $data_lang){ ?>
                    <li>- <?=$data_lang['Lang_name'];?></li>
                    <?php if ($num % 3 == 0) { echo "</ul><ul class='skills-info'>";} $num++; } ?>
                </ul>
            </td>
        </tr>
        <?php } ?>
        <!-- end language -->

        <!-- start work -->
        <!-- <?php if(is_array($data_docs) || is_array($data_videos) || is_array($data_webs) || is_array($data_photos)){ ?>
        <div class="row" style="margin: 30px 0 0 0;">
            <div class="left-side">
                <h2 class="entry-head">WORKS</h2>
            </div>
            <div class="right-side"></div>
        </div>

        <?php if(is_array($data_docs)){ ?>
        <div class="row" style="margin: 15px 0 0 0;">
            <div class="left-side">
                <h3 class="entry-child">Writing</h3>
            </div>
            <div class="right-side">
                <ul class="work-info">  
                    <?php foreach($data_docs as $data){ ?>
                    <li><a href="<?php echo $global['img-url'].$data['Doc_fileLoc'];?>" class="various doc-frame"><?php echo $data['Doc_title'];?></a></li>
                    <?php } ?>
                </ul>
            </div>
        </div>
        <?php } ?>

        <?php if(is_array($data_videos)){ ?>
        <div class="row" style="margin: 15px 0 0 0;">
            <div class="left-side">
                <h3 class="entry-child">Video</h3>
            </div>
            <div class="right-side">
                <ul class="work-info">  
                    <?php foreach($data_videos as $data){ ?>
                    <li title="<?php echo $data['Video_desc'];?>"><a href="<?php echo correctDisplay($data['Video_link']);?>" class="various video-frame"><?php echo $data['Video_title'];?></a></li>
                    <?php } ?>
                </ul>
            </div>
        </div>
        <?php } ?>

        <?php if(is_array($data_webs)){ ?>
        <div class="row" style="margin: 15px 0 0 0;">
            <div class="left-side">
                <h3 class="entry-child">Web</h3>
            </div>
            <div class="right-side">
                <ul class="work-info">  
                    <?php foreach($data_webs as $data){ ?>
                    <li><a href="<?php echo correctDisplay($data['Web_link']);?>" class="various web-frame"><?php echo $data['Web_title'];?></a></li>
                    <?php } ?>
                </ul>
            </div>
        </div>
        <?php } ?>

        <?php if(is_array($data_photos)){ ?>
        <div class="row" style="margin: 15px 0 0 0;">
            <div class="left-side">
                <h3 class="entry-child">Photography</h3>
            </div>
            <div class="right-side">
                <div class="row">
                    <?php foreach($data_photos as $data){ ?>
                    <div class="col-photo">
                        <div class="work-info-img">
                            <img src="<?php echo $global['img-url'].$data['Photo_ThmbImgLink'];?>" alt="<?php echo $data['Photo_title'];?>">
                        </div>
                    </div>
                    <?php } ?>
                </div>
            </div>
        </div>
        <?php } ?>

        <?php } ?> -->
        <!-- end work -->

        <!-- start footer -->
        <div class="footer-pos">
            <img class="footer-logo" src="<?=$global['logo-mobile'];?>" alt="logo">
        <div>
        <!-- end footer -->

    </table>
</body>
</html>
