<?php

if(isset($_GET['username'])){

    require_once("../model/Connection.php");
    $obj_con = new Connection();

    require_once("../model/User.php");
    $obj_user = new User();

    if($_GET['username'] != ''){
        
        $O_username = $_GET['username'];

        $obj_con->up();

        $data_user = $obj_user->get_data_by_username($O_username);   
        $obj_con->down(); 

        $fullname = $data_user[0]["User_fname"]."-".substr($data_user[0]["User_lname"],0,1)."'s CV by civimi";
        $html = '';
        ob_start();
        include("cv-print.php");
        $html .= ob_get_clean();
        // $filename = $O_username.".pdf";
        $filename = $fullname.".pdf";

        function pdf_create($html, $filename='', $stream=TRUE) {
            
            require_once('../packages/dompdf/dompdf_config.inc.php');
            $savein = '';
            $dompdf = new DOMPDF();


            $dompdf->load_html($html);
            $dompdf->render();
            $dompdf->stream($filename);

            unset($html);
            unset($dompdf); 
        }

        pdf_create($html,$filename);
    }
}
?>