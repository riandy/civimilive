<div id="resume-mobile" class="visible-xs">
	<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
		<?php if($data_user[0]['User_obj'] != ""){ ?>
		<div class="panel panel-default marg0 radius0">
			<div class="panel-heading" role="tab" id="heading1">
				<h4 class="panel-title">
					<a class="collapsed mcollapse" data-toggle="collapse" data-parent="#accordion" href="#collapse-objective" aria-expanded="true" aria-controls="collapse-objective">
						OBJECTIVE
						<!--<span id="ic-object" class="glyphicon glyphicon-triangle-bottom drop-panel" style="display:none;"></span>
						<span id="ic-object2" class="glyphicon glyphicon-triangle-top drop-panel"></span>-->
					</a>
				</h4>
			</div>
			<div id="collapse-objective" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading1">
				<div class="panel-body">
					<div class="objective-desc">
						<?=correctText($data_user[0]['User_obj']);?>
					</div>
				</div>
			</div>
		</div>
		<?php } ?>
		<?php if(is_array($data_edus)){ ?>
		<div class="panel panel-default marg0 radius0">
			<div class="panel-heading" role="tab" id="heading2">
				<h4 class="panel-title">
					<a class="collapsed mcollapse" data-toggle="collapse" data-parent="#accordion" href="#collapse-education" aria-expanded="true" aria-controls="collapse-education">
						EDUCATION
						<!--<span id="ic-edu" class="glyphicon glyphicon-triangle-bottom drop-panel" style="display:none;"></span>
						<span id="ic-edu2" class="glyphicon glyphicon-triangle-top drop-panel"></span>-->
					</a>
				</h4>
			</div>
			<div id="collapse-education" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading2">
				<div class="panel-body pad0">
					<?php $num_edu=1; foreach($data_edus as $data_edu){ ?>
					<div class="panel-inside">
						<a class="panel-link" data-toggle="collapse" href="#edu<?=$num_edu;?>" aria-expanded="true">
							<span class="entry-mplace"><?=$data_edu['Edu_schoolName'];?></span><br/>
							<span class="entry-mdate"><?=$data_edu['ATedu_fromMonth']."/".$data_edu['ATedu_fromYear'];?> - <?php if($data_edu['ATedu_toMonth'] != "0"){ echo $data_edu['ATedu_toMonth']."/".$data_edu['ATedu_toYear']; } else { echo "Present";}?></span>
						</a>
						<div id="edu<?=$num_edu;?>" class="panel-collapse collapse">
							<div class="entry-mdetail">
								<?=$data_edu['Edu_schoolCity']." ".$data_edu['Edu_schoolCountry'];?><br>
								<em><?=$data_edu["ATedu_degree"]."&nbsp;in ".$data_edu["ATedu_major"];?></em>
								<?php if($data_edu['ATedu_GPA'] != 0){ ?><em>GPA: <?=$data_edu['ATedu_GPA'];?></em><?php } ?>
							</div>
						</div>
					</div>
					<?php $num_edu++; } ?>
				</div>
			</div>
		</div>
		<?php } ?>
		<?php if(is_array($data_works)){ ?>
		<div class="panel panel-default marg0 radius0">
			<div class="panel-heading" role="tab" id="heading3">
				<h4 class="panel-title">
					<a class="collapsed mcollapse" data-toggle="collapse" data-parent="#accordion" href="#collapse-experience" aria-expanded="true" aria-controls="collapse-experience">
						EXPERIENCE
						<!--<span id="ic-exp" class="glyphicon glyphicon-triangle-bottom drop-panel" style="display:none;"></span>
						<span id="ic-exp2" class="glyphicon glyphicon-triangle-top drop-panel"></span>-->
					</a>
				</h4>
			</div>
			<div id="collapse-experience" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading3">
				<div class="panel-body pad0">
					<?php $num_work=1; foreach($data_works as $data_work){ ?>
					<div class="panel-inside">
						<a class="panel-link" data-toggle="collapse" href="#exp<?=$num_work;?>" aria-expanded="true">
							<span class="entry-mjobs"><?=$data_work['ATwork_title'];?></span><br/>
							<span class="entry-mplace"><?=$data_work['Company_title'];?></span><br/>
							<span class="entry-mdate"><?=$data_work['ATwork_from_month']."/".$data_work['ATwork_from_year'];?> - <?php if($data_work['ATwork_to_month'] != "0"){ echo $data_work['ATwork_to_month']."/".$data_work['ATwork_to_year']; } else { echo "Present";}?></span>
						</a>
						<div id="exp<?=$num_work;?>" class="panel-collapse collapse">
							<div class="entry-mdetail">
								<?=$data_work['City_title']." ".$data_work['Country_title'];?><br>
								<div class="experience-info">
									<?=correctText($data_work['ATwork_desc']);?>
								</div>
							</div>
						</div>
					</div>
					<?php $num_work++; } ?>
				</div>
			</div>
		</div>
		<?php } ?>
		<?php if(is_array($data_orgs)){ ?>
		<div class="panel panel-default marg0 radius0">
			<div class="panel-heading" role="tab" id="heading3">
				<h4 class="panel-title">
					<a class="collapsed mcollapse" data-toggle="collapse" data-parent="#accordion" href="#collapse-organization" aria-expanded="true" aria-controls="collapse-organization">
						ORGANIZATION <span class="badge" style="background-color:red;color:#fff;font-style:italic;">NEW</span>
					</a>
				</h4>
			</div>
			<div id="collapse-organization" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading3">
				<div class="panel-body pad0">
					<?php $num_work=1; foreach($data_orgs as $data_org){ ?>
					<div class="panel-inside">
						<a class="panel-link" data-toggle="collapse" href="#exp<?=$num_work;?>" aria-expanded="true">
							<span class="entry-mjobs"><?=$data_org['ATorg_title'];?></span><br/>
							<span class="entry-mplace"><?=$data_org['Org_title'];?></span><br/>
							<span class="entry-mdate"><?=$data_org['ATorg_from_month']."/".$data_org['ATorg_from_year'];?> - <?php if($data_org['ATorg_to_month'] != "0"){ echo $data_org['ATorg_to_month']."/".$data_org['ATorg_to_year']; } else { echo "Present";}?></span>
						</a>
						<div id="exp<?=$num_work;?>" class="panel-collapse collapse">
							<div class="entry-mdetail">
								<?=$data_org['City_title']." ".$data_org['Country_title'];?><br>
								<div class="experience-info">
									<?=correctText($data_org['ATorg_desc']);?>
								</div>
							</div>
						</div>
					</div>
					<?php $num_work++; } ?>
				</div>
			</div>
		</div>
		<?php } ?>
		<?php if(is_array($data_skills)){ ?>
		<div class="panel panel-default marg0 radius0">
			<div class="panel-heading" role="tab" id="heading4">
				<h4 class="panel-title">
					<a class="collapsed mcollapse" data-toggle="collapse" data-parent="#accordion" href="#collapse-skill" aria-expanded="true" aria-controls="collapse-skill">
						SKILL
						<!--<span id="ic-skill" class="glyphicon glyphicon-triangle-bottom drop-panel" style="display:none;"></span>
						<span id="ic-skill2" class="glyphicon glyphicon-triangle-top drop-panel"></span>-->
					</a>
				</h4>
			</div>
			<div id="collapse-skill" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading4">
				<div class="panel-body pad0">
					<?php foreach($data_skills as $data_skill){ ?>
					<div class="skill-panel">
						<span class="skill-name"><?=$data_skill['Skill_name'];?></span>
						<span class="label label-primary skill-score"><?=replace_rate($data_skill['ATskill_rate']);?></span>
					</div>
					<?php } ?>
					<!--<a href="#" class="see-skill">see more <span style="font-size: 14px;" class="glyphicon glyphicon-menu-down"></span></a>-->
				</div>
			</div>
		</div>
		<?php } ?>
		<?php if(is_array($data_langs)){?>
		<div class="panel panel-default marg0 radius0">
			<div class="panel-heading" role="tab" id="heading2">
				<h4 class="panel-title">
					<a class="collapsed mcollapse" data-toggle="collapse" data-parent="#accordion" href="#collapse-lang" aria-expanded="true" aria-controls="collapse-lang">
						LANGUAGE
						<!--<span id="ic-lang" class="glyphicon glyphicon-triangle-bottom drop-panel" style="display:none;"></span>
						<span id="ic-lang2" class="glyphicon glyphicon-triangle-top drop-panel"></span>-->
					</a>
				</h4>
			</div>
			<div id="collapse-lang" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading2">
				<div class="panel-body pad0">
					<?php $num_lang=1; foreach($data_langs as $data_lang){ ?>
					<div class="skill-panel">
						<a class="panel-link" data-toggle="collapse" href="#lang<?=$num_lang;?>" aria-expanded="true">
							<span class="entry-mplace"><?=$data_lang['Lang_name'];?></span><br/>
						</a>
						<div id="lang<?=$num_lang;?>" class="panel-collapse collapse">
							<div class="entry-mdetail">
								Listening : <?=replace_rate($data_lang['ATlang_listen']);?><br>
								Speaking  : <?=replace_rate($data_lang['ATlang_speak']);?><br>
								Reading   : <?=replace_rate($data_lang['ATlang_read']);?><br>
								Writing   : <?=replace_rate($data_lang['ATlang_write']);?>
							</div>
						</div>
					</div>
					<?php $num_lang++; } ?>
				</div>
			</div>
		</div>
		<?php } ?>
	</div>
	<!-- start footer -->
	<div class="footer-logo">
		<a href="<?php echo $global['base'];?>"><img src="<?=$global['logo-mobile'];?>" alt="logo" height="59" width="65"></a>
	</div><!-- end footer -->
</div>