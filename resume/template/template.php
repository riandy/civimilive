<div class="template-paper">
	<div class="row">
		<div class="tpaper-side">
			<div class="tpaper-sidewrap">
				<div class="tpaper-side-top">
					<div class="tpaper-image">
						<img src="<?php echo check_image_url($data_user[0]['User_proPhotoThmb']);?>" alt="image">
					</div>
					<div class="tpaper-name"><?=$data_user[0]['User_fname']." ".$data_user[0]['User_lname'];?></div>
					<div class="tpaper-jobs"><?=$data_user[0]['User_bioDesc'];?></div>
				</div>
				<div class="tpaper-side-bottom">
					<div class="tabout-head">
						<img src="<?php echo $global['resume-page'];?>img/icon-about.png" alt="icon">
						ABOUT ME
					</div>
					<div class="tabout-desc">
						<?=correctText($data_user[0]['User_obj']);?>
					</div>
					<div class="tabout-head">
						<img src="<?php echo $global['resume-page'];?>img/icon-contact.png" alt="icon">
						CONTACT ME
					</div>
					<div class="tcontact-desc">
						<ul>
							<li><?=$data_user[0]['User_city'].", ".$data_user[0]['User_state'].", ".$data_user[0]['User_country'];?></li>
							<li><span class="email-text"><?=strrev($data_user[0]['User_email']);?></span></li>
							<li><span class="email-text"><?=strrev($data_user[0]['User_phone']);?></span></li>
						</ul>
					</div>
					<?php if(is_array($data_skills)){ ?>
					<div class="tabout-head">
						<img src="<?php echo $global['resume-page'];?>img/icon-skill.png" alt="icon">
						MY SKILLS
					</div>
					<div class="tcontact-desc">
						<ul>
							<?php foreach($data_skills as $data_skill){ ?>
							<li>
								<?=$data_skill['Skill_name'];?>
								<div class="progress progress-skill">
									<div class="progress-bar progress-barskill" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: <?=skillProgress($data_skill['ATskill_rate']);?>;">
										<?=skillProgress($data_skill['ATskill_rate']);?>
									</div>
								</div>
							</li>
							<?php } ?>
						</ul>
					</div>
					<?php } ?>
				</div>
			</div>
		</div>
		<div class="tpaper-content">

			<?php if(is_array($data_works)){ ?>
			<div id="work-section" class="content-wrap">
				<div class="tcontent-head">
					<img src="<?php echo $global['resume-page'];?>img/icon-work.png" alt="icon">
					WORK EXPERIENCE
				</div>
				<div class="row">
					<div class="col-xs-12">

						<?php if(count($data_works) >= 3){ for($k=0;$k<=2;$k++){ ?>
						<div class="twork-title"><?=$data_works[$k]['ATwork_title'];?></div>
						<div class="twork-content">
							<span class="twork-company"><i class="glyphicon glyphicon-briefcase"></i><?=$data_works[$k]['Company_title'];?></span>
							<span class="twork-year"><i class="glyphicon glyphicon-time"></i><?=$data_works[$k]['ATwork_from_month']."/".$data_works[$k]['ATwork_from_year'];?> - <?php if($data_works[$k]['ATwork_to_month'] != "0"){ echo $data_works[$k]['ATwork_to_month']."/".$data_works[$k]['ATwork_to_year']; } else { echo "Present";}?></span>
						</div>
						<div class="twork-desc">
							<?php echo correctText($data_works[$k]['ATwork_desc']);?>
						</div>
						<?php } } else { foreach($data_works as $data_work){ ?>
						<div class="twork-title"><?=$data_work['ATwork_title'];?></div>
						<div class="twork-content">
							<span class="twork-company"><i class="glyphicon glyphicon-briefcase"></i><?=$data_work['Company_title'];?></span>
							<span class="twork-year"><i class="glyphicon glyphicon-time"></i><?=$data_work['ATwork_from_month']."/".$data_work['ATwork_from_year'];?> - <?php if($data_work['ATwork_to_month'] != "0"){ echo $data_work['ATwork_to_month']."/".$data_work['ATwork_to_year']; } else { echo "Present";}?></span>
						</div>
						<div class="twork-desc">
							<?php echo correctText($data_work['ATwork_desc']);?>
						</div>
						<?php } } ?>

					</div>
				</div>
			</div>
			<?php } ?>

			<?php if(is_array($data_edus)){ ?>
			<div id="education-section" class="content-wrap">
				<div class="tcontent-head">
					<img src="<?php echo $global['resume-page'];?>img/icon-education.png" alt="icon">
					EDUCATION
				</div>
				<div class="row">
					<div class="col-xs-12">
						<?php if(count($data_edus) >= 3){ for($e=0;$e<=2;$e++){ ?>
						<div class="twork-title"><?=$data_edus[$e]['Edu_schoolName'];?></div>
						<div class="twork-content">

							<?php if( $data_edus[$e]["ATedu_degree"] != "-" && $data_edus[$e]["ATedu_major"] != "-" ){ ?>
							<span class="twork-company"><i class="glyphicon glyphicon-book"></i><?=$data_edus[$e]["ATedu_degree"]."&nbsp;in ".$data_edus[$e]["ATedu_major"];?></span>
							<?php } else if( $data_edus[$e]["ATedu_degree"] != "-" && $data_edus[$e]["ATedu_major"] == "-" ){ ?>
							<span class="twork-company"><i class="glyphicon glyphicon-book"></i><?=$data_edus[$e]["ATedu_degree"];?></span>
							<?php } else if( $data_edus[$e]["ATedu_degree"] == "-" && $data_edus[$e]["ATedu_major"] != "-" ){ ?>
							<span class="twork-company"><i class="glyphicon glyphicon-book"></i><?=$data_edus[$e]["ATedu_major"];?></span>
							<?php } ?>

							<?php if($data_edus[$e]['ATedu_GPA'] != 0){ ?>
							<span class="twork-company"><i class="glyphicon glyphicon-tags"></i>GPA: <?=$data_edus[$e]['ATedu_GPA'];?></span>
							<?php } ?>
							<span class="twork-year"><i class="glyphicon glyphicon-time"></i><?=$data_edus[$e]['ATedu_fromMonth']."/".$data_edus[$e]['ATedu_fromYear'];?> - <?php if($data_edus[$e]['ATedu_toMonth'] != "0"){ echo $data_edus[$e]['ATedu_toMonth']."/".$data_edus[$e]['ATedu_toYear']; } else { echo "Present";}?></span>
						</div>
						<div class="twork-desc">
							<?php echo correctText($data_edus[$e]['ATedu_desc']);?>
						</div>
						<?php } } else { foreach($data_edus as $data_edu){ ?>
						<div class="twork-title"><?=$data_edu['Edu_schoolName'];?></div>
						<div class="twork-content">

							<?php if( $data_edu["ATedu_degree"] != "-" && $data_edu["ATedu_major"] != "-" ){ ?>
							<span class="twork-company"><i class="glyphicon glyphicon-book"></i><?=$data_edu["ATedu_degree"]."&nbsp;in ".$data_edu["ATedu_major"];?></span>
							<?php } else if( $data_edu["ATedu_degree"] != "-" && $data_edu["ATedu_major"] == "-" ){ ?>
							<span class="twork-company"><i class="glyphicon glyphicon-book"></i><?=$data_edu["ATedu_degree"];?></span>
							<?php } else if( $data_edu["ATedu_degree"] == "-" && $data_edu["ATedu_major"] != "-" ){ ?>
							<span class="twork-company"><i class="glyphicon glyphicon-book"></i><?=$data_edu["ATedu_major"];?></span>
							<?php } ?>

							<?php if($data_edu['ATedu_GPA'] != 0){ ?>
							<span class="twork-company"><i class="glyphicon glyphicon-tags"></i>GPA: <?=$data_edu['ATedu_GPA'];?></span>
							<?php } ?>
							<span class="twork-year"><i class="glyphicon glyphicon-time"></i><?=$data_edu['ATedu_fromMonth']."/".$data_edu['ATedu_fromYear'];?> - <?php if($data_edu['ATedu_toMonth'] != "0"){ echo $data_edu['ATedu_toMonth']."/".$data_edu['ATedu_toYear']; } else { echo "Present";}?></span>
						</div>
						<div class="twork-desc">
							<?php echo correctText($data_work['ATedu_desc']);?>
						</div>
						<?php } } ?>
					</div>
				</div>
			</div>
			<?php } ?>

			<?php if(is_array($data_orgs)){ ?>
			<div id="organization-section" class="content-wrap">
				<div class="tcontent-head">
					<img src="<?php echo $global['resume-page'];?>img/icon-organization.png" alt="icon">
					ORGANIZATION
				</div>
				<div class="row">
					<div class="col-xs-12">
						<?php if(count($data_orgs) >= 3){ for($o=0;$o<=2;$o++){ ?>
						<div class="twork-title"><?=$data_orgs[$o]['ATorg_title'];?></div>
						<div class="twork-content">
							<span class="twork-company"><i class="glyphicon glyphicon-briefcase"></i><?=$data_orgs[$o]['Org_title'];?></span>
							<span class="twork-year"><i class="glyphicon glyphicon-time"></i><?=$data_orgs[$o]['ATorg_from_month']."/".$data_orgs[$o]['ATorg_from_year'];?> - <?php if($data_orgs[$o]['ATorg_to_month'] != "0"){ echo $data_orgs[$o]['ATorg_to_month']."/".$data_orgs[$o]['ATorg_to_year']; } else { echo "Present";}?></span>
						</div>
						<div class="twork-desc">
							<?php echo correctText($data_orgs[$o]['ATorg_desc']);?>
						</div>
						<?php } } else { foreach($data_orgs as $data_org){ ?>
						<div class="twork-title"><?=$data_org['ATorg_title'];?></div>
						<div class="twork-content">
							<span class="twork-company"><i class="glyphicon glyphicon-briefcase"></i><?=$data_org['Org_title'];?></span>
							<span class="twork-year"><i class="glyphicon glyphicon-time"></i><?=$data_org['ATorg_from_month']."/".$data_org['ATorg_from_year'];?> - <?php if($data_org['ATorg_to_month'] != "0"){ echo $data_org['ATorg_to_month']."/".$data_org['ATorg_to_year']; } else { echo "Present";}?></span>
						</div>
						<div class="twork-desc">
							<?php echo correctText($data_org['ATorg_desc']);?>
						</div>
						<?php } } ?>
					</div>
				</div>
			</div>
			<?php } ?>

			<?php if(is_array($data_langs)){ ?>
			<div id="language-section" class="content-wrap">
				<div class="tcontent-head">
					<img src="<?php echo $global['resume-page'];?>img/icon-language.png" alt="icon">
					LANGUAGE
				</div>
				<div class="row">
					<?php foreach($data_langs as $data_lang){ ?>
					<div class="col-sm-4 col-xs-6">
						<div class="tlang-wrap">
							<div class="tlang-flag">
								<img src="<?php echo $global['resume-page'];?>img/jobs.jpg" alt="flag">
							</div>
							<div class="tlang-title"><?=$data_lang['Lang_name'];?></div>
							<div class="tlang-star">
								<i class="glyphicon glyphicon-star"></i>
								<i class="glyphicon glyphicon-star"></i>
								<i class="glyphicon glyphicon-star"></i>
								<i class="glyphicon glyphicon-star"></i>
								<i class="glyphicon glyphicon-star"></i>
							</div>
						</div>
					</div>
					<?php } ?>
				</div>
			</div>
			<?php } ?>

			<?php if(is_array($data_photos)){ ?>
			<div id="portfolio-section" class="content-wrap">
				<div class="tcontent-head">
					<img src="<?php echo $global['resume-page'];?>img/icon-portfolio.png" alt="icon">
					PORTFOLIO
				</div>
				<?php $num=1;foreach($data_photos as $data){ ?>
				<?php if($num == 1){ ?>
				<div class="col-sm-12 col-xs-12">
					<div class="tportfolio-wrap">
						<a href="javascript:;" onclick="openCard('<?=$data['Photo_ID'];?>');" data-toggle="modal" data-target="#modal-card" class="tportfolio-img" style="background-image: url(<?php echo $global['img-url'].$data['Photo_imgLink'];?>);"></a>
						<div class="tportfolio-title"><?php echo charLength(correctDisplay($data['Photo_title']),30);?></div>
					</div>
				</div>
				<?php } else { if($num <=3){ ?>
				<div class="col-sm-6 col-xs-6">
					<div class="tportfolio-wrap">
						<a href="javascript:;" onclick="openCard('<?=$data['Photo_ID'];?>');" data-toggle="modal" data-target="#modal-card" class="tportfolio-img2" style="background-image: url(<?php echo $global['img-url'].$data['Photo_imgLink'];?>);"></a>
						<div class="tportfolio-title"><?php echo charLength(correctDisplay($data['Photo_title']),30);?></div>
					</div>
				</div>
				<?php } }  ?>
				<?php $num++;} ?>
			</div>
			<?php } ?>

		</div>
	</div>
</div>