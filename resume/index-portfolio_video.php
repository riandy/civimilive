<?php 
include("packages/require.php");
include("controller/controller_resume.php");
include("controller/controller_portfolio_video.php");
$curpage='portfolio';
?>
<?php
    function getYoutubeImage($url){
        $urltokens = explode("/", $url);
        $vid_id = $urltokens[4];
        $result = "http://img.youtube.com/vi/" . $vid_id . "/0.jpg";
        return $result;
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title><?=$data_user[0]['User_fname']." ".$data_user[0]['User_lname']."'s Portfolio";?></title>
    <meta name="keywords" content="">
    <meta name="description" content="">
    <?php include("packages/head.php");?>
    <!-- Add fancyBox main JS and CSS files -->
    <script type="text/javascript" src="<?php echo $global['resume-page'];?>js/fbox/source/jquery.fancybox.js?v=2.1.5"></script>
    <link rel="stylesheet" type="text/css" href="<?php echo $global['resume-page'];?>js/fbox/source/jquery.fancybox.css?v=2.1.5" media="screen" />
    <script type="text/javascript">
        function parseVideoID(url){
            var regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]*).*/;
            var match = url.match(regExp);
            if (match && match[2].length == 11) {
                return match[2];
            } else {
                return "error URL";
            }
        }
      //generate thumbnail
        function videoGenerate(id,url){
            var thumbnail = "";
            var iframe_src       = url;
            var youtube_video_id = parseVideoID(iframe_src);
            var thumbnail = "<?=$global['absolute-url'];?>img/placeholder-video.png";
            if (youtube_video_id.length == 11) {
                var thumbnail = "//img.youtube.com/vi/"+youtube_video_id+"/0.jpg";
                var embedURL = "https://www.youtube.com/embed/"+youtube_video_id;
            }
            $("#portfolio-"+id).attr('src', thumbnail);
            $("#portfolioEmbed-"+id).attr('href', embedURL);
        }
        function videoGenerateMobile(id,url){
            var thumbnail = "";
            var iframe_src       = url;
            var youtube_video_id = parseVideoID(iframe_src);
            var thumbnail = "<?=$global['absolute-url'];?>img/placeholder-video.png";
            if (youtube_video_id.length == 11) {
                var thumbnail = "//img.youtube.com/vi/"+youtube_video_id+"/0.jpg";
                var embedURL = "https://www.youtube.com/embed/"+youtube_video_id;
            }
            $("#portfolioMobile-"+id).attr('src', thumbnail);
            $("#portfolioEmbedMobile-"+id).attr('href', embedURL);
        }
    </script>
</head>
<body>
    <div id="all" class="background-cvm">
        <!-- start top nav resume -->
        <?php include("resume-top-nav.php");?>
        <!-- end top nav resume --> 
         
        <!-- start top nav resume -->
        <?php include("part-top_resume.php");?>
        <!-- end top nav resume -->
        <div class="container container-max container-resume">
            <div class="paper-cv paper-cv-portfolio">
                <div class="paper-pad paper-pad-portfolio">
                    <!-- start breadcrumb mobile -->
                    <?php include("part-mobile_breadcrumb.php");?>
                    <!-- end breadcrumb mobile -->
                    <div id="resume-profile">
                        <div class="row">
                            <div class="col-sm-2 col-xs-2 pad0-xs text-right">
                                <a href="<?=check_image_url($data_user[0]['User_proPhoto']);?>" class="resume-photo" title="<?=$data_user[0]['User_fname']." ".$data_user[0]['User_lname'];?>"><img src="<?php echo check_image_url($data_user[0]['User_proPhoto']);?>" alt="Resume picture of <?=$data_user[0]['User_fname']." ".$data_user[0]['User_lname'];?> in CIVIMI"></a>
                            </div>
                            <div class="col-sm-10 col-xs-10">
                                <div class="resume-name">
                                    <h1><?=$data_user[0]['User_fname']." ".$data_user[0]['User_lname'];?></h1>
                                    <span class="current-interest hidden-xs"><?=checkInterest($data_user[0]['User_interest']);?></span>
                                </div>
                                <div class="resume-current-job"><?=$data_user[0]['User_bioDesc'];?></div>
                                <ul class="resume-contact">
                                    <li class="address"><i class='glyphicon glyphicon-map-marker'></i>&nbsp;&nbsp;<?php if ($data_user[0]['User_city'] != ""){ ;?><?=$data_user[0]['User_city'].", ";?><?php } ?><?=$data_user[0]['User_country'];?></li>
                                    <!-- <li class="mail"><?=$data_user[0]['User_email'];?></li>
                                    <li class="phone"><?=$data_user[0]['User_phone'];?></li> -->
                                    <?php if ($data_user[0]['User_website'] != ""){ ;?>
                                    <li class="web"><i class='glyphicon glyphicon-globe'></i>&nbsp;&nbsp;<a target="_blank" title="Website of <?=$data_user[0]['User_fname']." ".$data_user[0]['User_lname'];?>" href="<?=checkLink($data_user[0]['User_website']);?>" rel="nofollow"><?=$data_user[0]['User_website'];?></a></li>
                                    <?php } ?>
                                </ul>
                                <div id="resume-share" class="visible-xs">
                                    <a onclick="centeredPopup('https://www.facebook.com/sharer/sharer.php?u=<?php echo $path["user-resume"];?><?php echo $O_username;?>','myWindow','500','300','yes');return false" href="#" class="share-resume"><img src="<?php echo $global['absolute-url'];?>img/share-facebook.png" alt="facebook"></a>
                                    <a onclick="centeredPopup('https://twitter.com/intent/tweet?url=<?php echo $path["user-resume"];?><?php echo $O_username;?>&via=civimi');return false" href="#" class="share-resume"><img src="<?php echo $global['absolute-url'];?>img/share-twitter.png" alt="twitter"></a>
                                    <a onclick="centeredPopup('https://plus.google.com/share?url=<?php echo $path["user-resume"];?><?php echo $O_username;?>','myWindow','500','300','yes');return false" href="#" class="share-resume"><img src="<?php echo $global['absolute-url'];?>img/share-google.png" alt="google"></a>
                                    <a onclick="centeredPopup('http://www.linkedin.com/shareArticle?mini=true&url=<?php echo $path["user-resume"];?><?php echo $O_username;?>&source=civimi','myWindow','500','300','yes');return false" href="#" class="share-resume"><img src="<?php echo $global['absolute-url'];?>img/share-linkedin.png" alt="linkedin"></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="portfolio-category">
                                <ul>
                                    <?php if(is_array($data_photos)){ ?>
                                    <li><a href="<?=$path['user-portfolio_image'].$O_username.".cvm";?>" class="portfolioCat-link"><i class="glyphicon glyphicon-picture"></i>Image</a></li>
                                    <?php } if(is_array($data_videos)){ ?>
                                    <li><a href="<?=$path['user-portfolio_video'].$O_username.".cvm";?>" class="portfolioCat-link portfolioCat-active"><i class="glyphicon glyphicon-facetime-video"></i>Video</a></li>
                                    <?php } if(is_array($data_docs)){ ?>
                                    <li><a href="<?=$path['user-portfolio_doc'].$O_username.".cvm";?>" class="portfolioCat-link"><i class="glyphicon glyphicon-book"></i>Document</a></li>
                                    <?php } if(is_array($data_webs)){ ?>
                                    <li><a href="<?=$path['user-portfolio_web'].$O_username.".cvm";?>" class="portfolioCat-link"><i class="glyphicon glyphicon-globe"></i>Website</a></li>
                                    <?php } ?>
                                </ul>
                            </div>
                            <?php if(is_array($data_cats)){ ?>
                            <div class="portfolio-folder">
                                <ul>
                                    <li><a href="<?php echo $path['user-portfolio_video'].$O_username.".cvm";?>" class="portfolioFolder-link <?php if($O_cat_id == null) { ?>portfolioFolder-active<?php } ?>">Recent</a></li>
                                    <?php foreach($data_cats as $data_cat){ ?>
                                    <li><a href="<?php echo $path['user-portfolio_video'].$data_cat["VideoCat_ID"]."/".$O_username.".cvm";?>" class="portfolioFolder-link <?php if ($O_cat_id == $data_cat["VideoCat_ID"]) { ?> portfolioFolder-active <?php } ?>"><?php echo $data_cat["VideoCat_name"];?></a></li>
                                    <?php } ?>
                                </ul>
                            </div>
                            <?php } else { ?>
                            <div class="no-portfolio">There is no Video yet</div>
                            <?php } ?>
                        </div>
                    </div>

                    <div class="visible-lg">
                        <div class="portfolio-list">
                            <?php if(is_array($data_videos)){ $d = 0;$part = 4; foreach($data_videos as $data_video){ ?>
                            <script type="text/javascript">
                            $(document).ready(function() {
                                videoGenerateMobile(<?=$data_video["Video_ID"];?>,"<?=$data_video['Video_link'];?>");
                            })
                            </script>
                            <?php if ($d == 0 || $d%$part == 0) { ?>
                            <div class="row">
                            <?php } ?>
                                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-4 col-portfolio">
                                    <a id="portfolioEmbedMobile-<?=$data_video["Video_ID"];?>" href="" title="<?php echo $data_video["Video_title"];?>" class="portfolio-url" onclick="storeHits(<?=$data_video["Video_ID"];?>);">
                                        <div class="portfolio-image">
                                            <img id="portfolioMobile-<?=$data_video["Video_ID"];?>" src="<?=$data_video['Video_link'];?>" alt="<?php echo $data_video["Video_title"];?>">
                                        </div>
                                        <div class="portfolio-title hidden-xs"><?php echo charLength(correctDisplay($data_video["Video_title"]),15);?></div>
                                    </a>
                                    <div class="portfolio-view hidden-sm hidden-xs">
                                        <span class="portfolio-viewer"><i class="glyphicon glyphicon-eye-open"></i>1200 views</span>
                                        <span class="portfolio-date"><?php echo relative_time($data_video["Video_create_date"]);?></span>
                                    </div>
                                </div> 
                                <?php $d++; if ($d%$part == 0) { echo "</div>"; }?>
                                <?php  } } ?> 
                            </div>
                        </div>
                    </div>
                    <div class="hidden-lg">
                        <div class="portfolio-list">
                            <?php if(is_array($data_videos)){ $dd = 0;$part2 = 3; foreach($data_videos as $data_video){ ?>
                            <script type="text/javascript">
                            $(document).ready(function() {
                                videoGenerate(<?=$data_video["Video_ID"];?>,"<?=$data_video['Video_link'];?>");
                            })
                            </script>
                            <?php if ($dd == 0 || $dd%$part2 == 0) { ?>
                            <div class="row">
                            <?php } ?>
                                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-4 col-portfolio">
                                    <a id="portfolioEmbed-<?=$data_video["Video_ID"];?>" href="" title="<?php echo $data_video["Video_title"];?>" class="portfolio-url" onclick="storeHits(<?=$data_video["Video_ID"];?>);">
                                        <div class="portfolio-image">
                                            <img id="portfolio-<?=$data_video["Video_ID"];?>" src="<?=$data_video['Video_link'];?>" alt="<?php echo $data_video["Video_title"];?>">
                                        </div>
                                        <div class="portfolio-title hidden-xs"><?php echo charLength(correctDisplay($data_video["Video_title"]),15);?></div>
                                    </a>
                                    <div class="portfolio-view hidden-sm hidden-xs">
                                        <span class="portfolio-viewer"><i class="glyphicon glyphicon-eye-open"></i>1200 views</span>
                                        <span class="portfolio-date"><?php echo relative_time($data_video["Video_create_date"]);?></span>
                                    </div>
                                </div> 
                                <?php $dd++; if ($dd%$part2 == 0) { echo "</div>"; }?>
                                <?php  } } ?> 
                            </div>
                        </div>
                    </div>
                    <!-- start footer -->
                    <div class="footer-logo">
                        <a href="<?php echo $global['base'];?>"><img src="<?=$global['logo-mobile'];?>" alt="logo" height="59" width="65"></a>
                    </div><!-- end footer -->
                </div><!--  end paper pad -->
            </div><!--  end paper cv -->
        </div><!-- end container max/resume --> 
    </div><!--  end all div -->
    <script src="<?php echo $global['resume-page'];?>js/home.js"></script>
    <script type="text/javascript">
    <?php if($message != ""){?>
        $(document).ready(function(){
            var message = "<?php echo $message;?>";
            popup(message);
        });
    <?php } ?>
    
    $(document).ready(function() {
        $(".portfolio-url").fancybox({
            openEffect  : 'none',
            closeEffect : 'none',
            autoSize: true,
            type: 'iframe',
            iframe: {
            preload: false // fixes issue with iframe and IE
            }
        });
    });
    function storeHits(itemID){
        var data = { 
            user_id : "<?=$O_id;?>", 
            username : "<?=$O_username;?>",
            page : "<?=$N_page;?>",
            type : "<?=$N_type;?>",
            type_id : itemID,
            view_user : "<?=$N_view_userID;?>",
            ip : "<?=$N_ip;?>",
            ip_city : "<?=$N_city;?>",
            ip_country : "<?=$N_country;?>",
        }

        var url = "<?=$api['user-hits'];?>";
        $.ajax({type:'POST',url: url,data : data, success:function(result){
            console.log(result.message);
        }
      });
    }
    </script>
    <!-- script analytic -->
    <?php include("../packages/analytic.php");?>
</body>
</html>
