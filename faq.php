<?php 
include("packages/require.php");
include("controller/controller_user_profile.php");
include("controller/controller_user_setting_privacy.php");

$curpage='user_setting';
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title><?=$global['title-faq'];?></title>
  <meta name="keywords" content="">
  <meta name="description" content="">
  <?php include("packages/head.php");?>
  <link rel="stylesheet" href="<?php echo $global['absolute-url'];?>css/dropzone.css"/>
  <script src="<?php echo $global['absolute-url'];?>js/dropzone.js"></script>
</head>
<body>
  <div id="all">
    <!-- start top nav -->
    <?php include("section-top-nav.php");?>
    <!-- end top nav -->

    <div id="user-section">
    
    <div class="container">
      <div id="profile-content">
        <div class="up5 visible-xs"></div>
        <div class="row hidden-xs">
          <div class="col-xs-12">
            <div class="profile-header">Help & FAQs</div>
          </div>
        </div>
        <div class="row">
          <div class="up5 visible-xs"></div>
          <!-- faq sidebar -->
          <div class="col-sm-3 col-xs-12">

            <!-- start sidebar -->
            <div id="faq-sidebar" class="faq-sidebar hidden-xs">
              <a data-toggle="collapse" onclick="faqNav();" data-parent="#accordion" href="#faq-bar" aria-expanded="true" aria-controls="faq-bar" class="profile-side">
                Help & FAQs <span id="faq-arrow" class="arrow"></span>
              </a>
              <div id="faq-bar" class="panel-collapse collapse in" role="tabpanel">
                <div class="profile-nav">
                  <ul>
                    <li class="active"><a href="#faq-general" class="profile-link" aria-controls="faq-general" role="tab" data-toggle="tab">General FAQs</a></li>
                    <li><a href="#faq-privacy" class="profile-link" aria-controls="faq-privacy" role="tab" data-toggle="tab">Privacy</a></li>
                    <li><a href="#faq-account" class="profile-link" aria-controls="faq-account" role="tab" data-toggle="tab">Account</a></li>
                    <li><a href="#faq-email" class="profile-link" aria-controls="faq-email" role="tab" data-toggle="tab">Email Support Team</a></li>
                  </ul>
                </div>
              </div>
            </div>
            <!-- mobile sidebar -->
            <div id="faq-sidebar" class="faq-sidebar visible-xs">
              <a data-toggle="collapse" onclick="faqNav2();" data-parent="#accordion" href="#faq-bar2" aria-expanded="true" aria-controls="faq-bar2" class="profile-side">
                Help & FAQs <span id="faq-arrow2" class="arrow nav-close"></span>
              </a>
              <div id="faq-bar2" class="panel-collapse collapse" role="tabpanel">
                <div class="profile-nav">
                  <ul>
                    <li class="active"><a href="#faq-general" class="profile-link" aria-controls="faq-general" role="tab" data-toggle="tab">General FAQs</a></li>
                    <li><a href="#faq-privacy" class="profile-link" aria-controls="faq-privacy" role="tab" data-toggle="tab">Privacy</a></li>
                    <li><a href="#faq-account" class="profile-link" aria-controls="faq-account" role="tab" data-toggle="tab">Account</a></li>
                    <li><a href="#faq-email" class="profile-link" aria-controls="faq-email" role="tab" data-toggle="tab">Email Support Team</a></li>
                  </ul>
                </div>
              </div>
            </div>
            <!-- end sidebar -->

          </div><!-- faq sidebar -->
          <div class="col-sm-9 col-xs-12 pad0-xs">
              <div id="personal-portfolio-section" class="job-sbox down1">
                <div class="tab-content pad2">
                  <div role="tabpanel" class="tab-pane fade in active" id="faq-general">
                    General FAQs<br/>
                    How do I use the tool button?<br/>
                    Firefox<br/>
                    Notable’s tool button for Firefox captures a full page screen shot by default. To capture and upload a screen to Notable, just press the tool button.
                    <br/><br/>
                    With Firefox, you can take advantage of Notable’s capture queue to capture multiple pages before you begin to leave feedback. Captures will stay in your queue until you restart Firefox and will be uploaded to your Notable account as soon as you click the “Add Notes” button.
                    <br/><br/>
                    Chrome<br/>
                    The Chrome Notable tool works a little differently as it offers to different ways of capturing a website. When you click on the button, a drop down menu will appear with two options:
                    <br/><br/>
                    Capture Visible Screen - This allows you to capture only the content currently seen in the browser<br/>
                    Capture Entire Page - Will capture the entire webpage, even if it is currently out of view. Both options will automatically open the screenshot in a new browser tab as well as create the post within your Notable account.<br/>
                    Both capture methods will open a new tab of your post as well as add it to your Notable account.
                    <br/><br/>
                    Internet Explorer & Safari<br/>
                    The capture tool for these browsers uses a hotkey system to capture either a full web pages. To capture a full page, use the keyboard command Control+Shift+3.
                    <br/><br/>
                    Clicking the Notable tool button will only capture the portion of the screen currently visible within the browser.
                    <br/><br/>
                    My tool button isn’t working; what should I do?<br/>
                    If you find your Notable tool doesn’t seem to be working properly, there may be many possible reasons as to why. In order to address possible reasons and solutions, as well as address your specific case, please email us at support@notableapp.com with the following information:
                    <br/><br/>
                    Type of browser you’re working from and the version you’re running<br/>
                    The operating system of your computer<br/>
                    Any additional plugins installed on the browser<br/>
                    The website you are attempting to capture<br/>
                  </div>
                  <div role="tabpanel" class="tab-pane fade" id="faq-privacy">
                    Privacy<br/><br/>
                    Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, 
                    vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim 
                    qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. <br/><br/>
                    Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum. 
                    Typi non habent claritatem insitam; est usus legentis in iis qui facit eorum claritatem. 
                    Investigationes demonstraverunt lectores legere me lius quod ii legunt saepius.
                  </div>
                  <div role="tabpanel" class="tab-pane fade" id="faq-account">
                    Account<br/><br/>
                    Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, 
                    vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim 
                    qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. <br/><br/>
                    Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum. 
                    Typi non habent claritatem insitam; est usus legentis in iis qui facit eorum claritatem. 
                    Investigationes demonstraverunt lectores legere me lius quod ii legunt saepius.
                  </div>
                  <div role="tabpanel" class="tab-pane fade" id="faq-email">
                    Email Support Team<br/><br/>
                    Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, 
                    vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim 
                    qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. <br/><br/>
                    Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum. 
                    Typi non habent claritatem insitam; est usus legentis in iis qui facit eorum claritatem. 
                    Investigationes demonstraverunt lectores legere me lius quod ii legunt saepius.
                  </div>
                </div>
              </div>
          </div>
      </div>
    </div>
            
    </div>
    <!-- end center content -->
  </div>
    <!-- start footer section -->
    <?php include("section-footer.php");?>
    <!-- end footer section -->
  </div><!--  end all div -->
  <script src="<?php echo $global['absolute-url'];?>js/function.js"></script>
  <script type="text/javascript">
  function faqNav(){
    $('#faq-arrow').toggleClass('nav-close');
  }
  function faqNav2(){
    $('#faq-arrow2').toggleClass('nav-close');
  }
  </script>
</body>
</html>
