<?php
session_start();

include("Facebook/Facebook_key.php"); // include api facebook key
require_once("Facebook/autoload.php"); // added in v4.0.0

require_once("../packages/front_config.php");

require_once("../model/Connection.php");
$obj_con = new Connection();

require_once("../model/Third_party.php");
$obj_fb = new Third_party();

require_once("../model/Setting.php");
$obj_set = new Setting();

require_once("../model/AT_Progress.php");
$obj_atpro = new AT_Progress();

// added facebook sdk v4
require_once('Facebook/FacebookSession.php');
require_once('Facebook/FacebookRedirectLoginHelper.php');
require_once('Facebook/FacebookRequest.php');
require_once('Facebook/FacebookResponse.php');
require_once('Facebook/FacebookSDKException.php');
require_once('Facebook/FacebookRequestException.php');
require_once('Facebook/FacebookAuthorizationException.php');
require_once('Facebook/GraphObject.php');
require_once('Facebook/Entities/AccessToken.php');
require_once('Facebook/HttpClients/FacebookHttpable.php');
require_once('Facebook/HttpClients/FacebookCurlHttpClient.php');
require_once('Facebook/HttpClients/FacebookCurl.php');

// use namespace
use Facebook\FacebookSession;
use Facebook\FacebookRedirectLoginHelper;
use Facebook\FacebookRequest;
use Facebook\FacebookResponse;
use Facebook\FacebookSDKException;
use Facebook\FacebookRequestException;
use Facebook\FacebookAuthorizationException;
use Facebook\GraphObject;
use Facebook\Entities\AccessToken;
use Facebook\HttpClients\FacebookCurlHttpClient;
use Facebook\HttpClients\FacebookHttpable;

// init app with app id and secret
FacebookSession::setDefaultApplication($app_id, $app_secret);
// login helper with redirect_uri
$helper = new FacebookRedirectLoginHelper($redirect_uri);
try{
  $session = $helper->getSessionFromRedirect();
}catch(FacebookRequestException $ex){
  // When Facebook returns an error
}catch(Exception $ex){
  // When validation fails or other local issues
}

// see if we have a session
if(isset($session)){
  $dbh_con = $obj_con->up();

  // graph api request for user data
  $request = new FacebookRequest($session, 'GET', '/me');
  $response = $request->execute();
  // get response
  $graphObject = $response->getGraphObject();

  // check reference field in https://developers.facebook.com/docs/graph-api/reference/user
  $fb_id              = $graphObject->getProperty('id');         // To Get Facebook ID
  $fb_firstname       = $graphObject->getProperty('first_name'); // To Get Facebook first name
  $fb_lastname        = $graphObject->getProperty('last_name');  // To Get Facebook last name
 	$fb_fullname        = $graphObject->getProperty('name');       // To Get Facebook full name
	$fb_email           = $graphObject->getProperty('email');      // To Get Facebook email ID
  $fb_birthday        = $graphObject->getProperty('birthday');   // To Get Facebook birthday
  $fb_profile_picture = "https://graph.facebook.com/".$fb_id."/picture?type=large"; // To Get Profile Picture
  $fb_provider        = "Facebook"; // Provider Name

  if($fb_email == NULL){
    $req = new FacebookRequest($session, 'DELETE', '/'.$fb_id.'/permissions'); //revoke permissions access app
    $res = $req->execute();
    //redirect facebook login url
    $location = $helper->getLoginUrl(array('scope'=>$scope));
  }else{
    $check = $obj_fb->check_primary_email($fb_email); // check e-mail 
    if(is_array($check)){
      $result = $obj_fb->user_login($check[0]['User_email']);
      if(is_array($result)){
        $obj_fb->update_action_login($result[0]['User_ID']); //update for last login and num login
        //if auth_code null, update
        if($result[0]['User_auth_code'] == "" || $result[0]['User_auth_code'] == NULL){
          $auth_code = $obj_fb->update_auth_code($result[0]['User_ID']);
          //create cookie for login
          if(isset($_SESSION['userData'])){unset($_SESSION['userData']);}
          create_cookie($result[0]['User_ID'], $result[0]['User_username'], $auth_code, $result[0]['User_email']);
        }else{
          //create cookie for login
          if(isset($_SESSION['userData'])){unset($_SESSION['userData']);}
          create_cookie($result[0]['User_ID'], $result[0]['User_username'], $result[0]['User_auth_code'], $result[0]['User_email']);
        }
        $obj_set->insert_data($result[0]['User_ID'], 1, 2, 3, 4, 5, 6, 7); //insert setting sort
        $obj_atpro->insert_data($result[0]['User_ID'], 0, 0, 0, 0, 0, 0, 0, 0); //insert progress
        $location = $path['user-edit']; //redirect url
      }else{
        $location = $global['base']; //redirect url
      }
    }else{
    	/* --- Set Variables --- */
      $N_fname    = $fb_firstname;
      $N_lname    = $fb_lastname;   
      $N_email    = $fb_email;
      $N_picture  = $fb_profile_picture;
      $N_provider = $fb_provider;  

      $result_create = $obj_fb->insert_data($N_fname, $N_lname, $N_email, $N_provider, $N_picture, $N_picture);
      //var_dump($result_create);
      if($result_create){
        unset($_SESSION['userData']);
        if(!isset($_SESSION['userData']['id'])){
          $_SESSION['userData']['id'] = $result_create; //session id  
        }
        if(!isset($_SESSION['userData']['email'])){
          $_SESSION['userData']['email'] = $N_email; //session email  
        }
        $obj_set->insert_data($result_create, 1, 2, 3, 4, 5, 6, 7); //insert setting sort
        $obj_atpro->insert_data($result_create, 0, 0, 0, 0, 0, 0, 0, 0); //insert progress
        $location = $path['form-goal'];
      }else if(!$result_create){
        $location = $global['base'];
      }
    }
  }
  header("Location:".$location);
  $obj_con->down($dbh_con);
} 
else{
  $loginUrl = $helper->getLoginUrl(array('scope'=>$scope));
  header("Location: ".$loginUrl);
}
?>