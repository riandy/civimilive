<?php
session_start();
include_once("LinkedIn/config.php");
include_once("LinkedIn/OAuth/http.php");
include_once("LinkedIn/OAuth/oauth_client.php");

require_once("../packages/front_config.php");

require_once("../model/Connection.php");
$obj_con = new Connection();

require_once("../model/Third_party.php");
$obj_li = new Third_party();

require_once("../model/Setting.php");
$obj_set = new Setting();

require_once("../model/AT_Progress.php");
$obj_atpro = new AT_Progress();

if (isset($_GET["oauth_problem"]) && $_GET["oauth_problem"] <> "") {
  // in case if user cancel the login. redirect back to home page.
  echo $_GET["oauth_problem"];
  exit;
}

$client = new oauth_client_class;

$client->debug = false;
$client->debug_http = true;
$client->redirect_uri = $callbackURL;

$client->client_id = $linkedinApiKey;
$application_line = __LINE__;
$client->client_secret = $linkedinApiSecret;

if(strlen($client->client_id) == 0 || strlen($client->client_secret) == 0)
  die('Please go to LinkedIn Apps page https://www.linkedin.com/secure/developer?newapp= , '.
			'create an application, and in the line '.$application_line.
			' set the client_id to Consumer key and client_secret with Consumer secret. '.
			'The Callback URL must be '.$client->redirect_uri).' Make sure you enable the '.
			'necessary permissions to execute the API calls your application needs.';

/* API permissions */
$client->scope = $linkedinScope;
if(($success = $client->Initialize())){
  if(($success = $client->Process())){
    if(strlen($client->authorization_error)){
      $client->error = $client->authorization_error;
      $success = false;
    }elseif (strlen($client->access_token)){
      $success = $client->CallAPI(
					//'http://api.linkedin.com/v1/people/~:(id,email-address,first-name,last-name,location,picture-urls::(original),phone-numbers,summary,public-profile-url,formatted-name,positions,skills:(id,skill:(name)),languages:(id,language:(name),proficiency:(name)),certifications:(id,name,authority:(name),number,start-date,end-date),educations:(id,school-name,field-of-study,start-date,end-date,degree,activities,notes))?format=json',
          'http://api.linkedin.com/v1/people/~:(id,email-address,first-name,last-name,location,picture-urls::(original),summary,public-profile-url,formatted-name)',  
          'GET', array(
						'format'=>'json'
					), array('FailOnAccessError'=>true), $user
      );
    }
  }
  $success = $client->Finalize($success);
}

if($client->exit) exit;

if($success){
  $dbh_con = $obj_con->up();

  $id                 = $user->id;
  $firstname          = $user->firstName;
  $lastname           = $user->lastName;
  $fullname           = $user->formattedName;
  $email              = $user->emailAddress;
  $profile_image_url  = $user->pictureUrls->values[0];
  $provider           = "LinkedIn"; // Provider Name
  
  $check = $obj_li->check_primary_email($email); // check e-mail 
  if(is_array($check)){
    $result = $obj_li->user_login($check[0]['User_email']);
    if(is_array($result)){
      $obj_li->update_action_login($result[0]['User_ID']); //update for last login and num login
      //if auth_code null, update
      if($result[0]['User_auth_code'] == "" || $result[0]['User_auth_code'] == NULL){
        $auth_code = $obj_li->update_auth_code($result[0]['User_ID']);
        //create cookie for login
        if(isset($_SESSION['userData'])){unset($_SESSION['userData']);}
        create_cookie($result[0]['User_ID'], $result[0]['User_username'], $auth_code, $result[0]['User_email']);
      }else{
        //create cookie for login
        if(isset($_SESSION['userData'])){unset($_SESSION['userData']);}
        create_cookie($result[0]['User_ID'], $result[0]['User_username'], $result[0]['User_auth_code'], $result[0]['User_email']);
      }
      $obj_set->insert_data($result[0]['User_ID'], 1, 2, 3, 4, 5, 6, 7); //insert setting sort
      $obj_atpro->insert_data($result[0]['User_ID'], 0, 0, 0, 0, 0, 0, 0, 0); //insert progress
      $location = $path['user-edit']; //redirect url
    }else{
      $location = $global['base']; //redirect url
    }
  }else{
    /* --- Set Variables --- */
    $N_fname    = $firstname;
    $N_lname    = $lastname;   
    $N_email    = $email;
    $N_picture  = $profile_image_url;
    $N_provider = $provider;  
     
    $result_create = $obj_li->insert_data($N_fname, $N_lname, $N_email, $N_provider, $N_picture, $N_picture);
    //var_dump($result_create);
    if($result_create){
      unset($_SESSION['userData']);
      if(!isset($_SESSION['userData']['id'])){
        $_SESSION['userData']['id'] = $result_create; //session id
      }
      if(!isset($_SESSION['userData']['email'])){
        $_SESSION['userData']['email'] = $N_email; //session email  
      }
      $obj_set->insert_data($result_create, 1, 2, 3, 4, 5, 6, 7); //insert setting sort
      $obj_atpro->insert_data($result_create, 0, 0, 0, 0, 0, 0, 0, 0); //insert progress
      $location = $path['form-goal'];
    }else if(!$result_create){
      $location = $global['base'];
    }
  }

  header("Location:".$location);
  $obj_con->down($dbh_con);  
}else{
  if(isset($_SESSION['OAUTH_ACCESS_TOKEN'])){
    unset($_SESSION['OAUTH_ACCESS_TOKEN']);
    header("Location:{$path['login-with-linkedin']}");  
  }else{
    echo $client->error;
  }
}
exit;
?>