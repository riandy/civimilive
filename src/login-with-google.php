<?php
session_start(); //start session

//include google api files
include("Google/Google_key.php");
require_once("Google/Google_Client.php");
require_once("Google/contrib/Google_Oauth2Service.php");

require_once("../packages/front_config.php");

require_once("../model/Connection.php");
$obj_con = new Connection();

require_once("../model/Third_party.php");
$obj_gp = new Third_party();

require_once("../model/Setting.php");
$obj_set = new Setting();

require_once("../model/AT_Progress.php");
$obj_atpro = new AT_Progress();

$gClient = new Google_Client();
$gClient->setApplicationName('Login to civimi.com');
$gClient->setClientId($google_client_id);
$gClient->setClientSecret($google_client_secret);
$gClient->setRedirectUri($google_redirect_url);
$gClient->setDeveloperKey($google_developer_key);

$google_oauthV2 = new Google_Oauth2Service($gClient);

//If user wish to log out, we just unset Session variable
if (isset($_REQUEST['reset'])) 
{
  unset($_SESSION['token']);
  $gClient->revokeToken();
  header('Location: ' . filter_var($google_redirect_url, FILTER_SANITIZE_URL)); //redirect user back to page
}

//If code is empty, redirect user to google authentication page for code.
//Code is required to aquire Access Token from google
//Once we have access token, assign token to session variable
//and we can redirect user back to page and login.
if (isset($_GET['code'])) 
{ 
	$gClient->authenticate($_GET['code']);
	$_SESSION['token'] = $gClient->getAccessToken();
	header('Location: ' . filter_var($google_redirect_url, FILTER_SANITIZE_URL));
	return;
}

if (isset($_SESSION['token'])) 
{ 
	$gClient->setAccessToken($_SESSION['token']);
}

if ($gClient->getAccessToken()) 
{
	$dbh_con = $obj_con->up();

	//For logged in user, get details from google using access token
	$user 				= $google_oauthV2->userinfo->get();
	$id 				= $user['id'];
	$birthday 			= $user['birthday'];
	$firstname 			= filter_var($user['given_name'], FILTER_SANITIZE_SPECIAL_CHARS);
	$lastname 			= filter_var($user['family_name'], FILTER_SANITIZE_SPECIAL_CHARS);
	$fullname 			= filter_var($user['name'], FILTER_SANITIZE_SPECIAL_CHARS);
	$email 				= filter_var($user['email'], FILTER_SANITIZE_EMAIL);
	$profile_image_url 	= filter_var($user['picture'], FILTER_VALIDATE_URL);
	$provider           = "Google"; // Provider Name
	
	$check = $obj_gp->check_primary_email($email); // check e-mail 
    if(is_array($check)){
	    $result = $obj_gp->user_login($check[0]['User_email']);
	    if(is_array($result)){
	    	$obj_gp->update_action_login($result[0]['User_ID']); //update for last login and num login
	      	//if auth_code null, update
	        if($result[0]['User_auth_code'] == "" || $result[0]['User_auth_code'] == NULL){
	          $auth_code = $obj_gp->update_auth_code($result[0]['User_ID']);
	          //create cookie for login
	          if(isset($_SESSION['userData'])){unset($_SESSION['userData']);}
	          create_cookie($result[0]['User_ID'], $result[0]['User_username'], $auth_code, $result[0]['User_email']);
	        }else{
	          //create cookie for login
	          if(isset($_SESSION['userData'])){unset($_SESSION['userData']);}
	          create_cookie($result[0]['User_ID'], $result[0]['User_username'], $result[0]['User_auth_code'], $result[0]['User_email']);
	        }
	        $obj_set->insert_data($result[0]['User_ID'], 1, 2, 3, 4, 5, 6, 7); //insert setting sort
	        $obj_atpro->insert_data($result[0]['User_ID'], 0, 0, 0, 0, 0, 0, 0, 0); //insert progress
	        $location = $path['user-edit']; //redirect url
	    }else{
       		$location = $global['base']; //redirect url
      	}
    }else{
	  	/* --- Set Variables --- */
	    $N_fname    = $firstname;
      	$N_lname    = $lastname;   
      	$N_email    = $email;
      	$N_picture  = $profile_image_url;
      	$N_provider = $provider;  
	    
		$result_create = $obj_gp->insert_data($N_fname, $N_lname, $N_email, $N_provider, $N_picture, $N_picture);
	    //var_dump($result_create);
	    if($result_create){
	    	unset($_SESSION['userData']);
	    	if(!isset($_SESSION['userData']['id'])){
	          $_SESSION['userData']['id'] = $result_create; //session id
	        }
	        if(!isset($_SESSION['userData']['email'])){
	          $_SESSION['userData']['email'] = $N_email; //session email  
	        }
	        $obj_set->insert_data($result_create, 1, 2, 3, 4, 5, 6, 7); //insert setting sort
	        $obj_atpro->insert_data($result_create, 0, 0, 0, 0, 0, 0, 0, 0); //insert progress
	        $location = $path['form-goal'];
	   	}else if(!$result_create){
	        $location = $global['base'];
	    }
    }

    $_SESSION['token'] 	= $gClient->getAccessToken();
	unset($_SESSION['token']);
    header("Location:".$location);
    $obj_con->down($dbh_con);
}
else 
{
	//For Guest user, get google login url
	$authUrl = $gClient->createAuthUrl();
	header("Location:".$authUrl);
}

?>