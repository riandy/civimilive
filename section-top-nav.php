<div id="index-top-nav" <?php if($curpage == 'main'){ ?>style='position:relative;'<?php } ?> class="hidden-xs <?php if($curpage == 'search' || $curpage == 'job_detail'|| $curpage == 'user_profile'){ echo 'border-bottom-ccc'; }?>">
    <a href="<?=$global['base'];?>" class="nav-logo"><img src="<?=$global['logo-desktop'];?>" alt="logo"></a>
    <?php if($curpage == 'main'){?>
    <a href="#" class="welcome-btn" data-toggle="modal" data-target="#modal-welcome">"What's New"</a>
    <?php } ?>
    <?php if($curpage == 'search' || $curpage == 'job_detail'  || $curpage == 'user_profile'){?>
    <?php if($curpage == 'job_detail' || $curpage == 'user_profile'){?>
    <form action="<?=$path['job-search'];?>?action=search" method="GET" style="display: inline;">
    <?php } ?>
    <!-- <div class="nav-search">
        <div id="search-autocomplete" style="display:inline-block;position: relative;">
        <input autocomplete="off" onfocus="$('#recent-key').show();" id="input-nav-qsearch" <?php if($curpage == 'search'){?> oninput="recentSugest();" <?php } ?> name="q" type="text" class="form-control input-nav-search" placeholder="keyword" value="<?php if($curpage == 'search'){ echo $O_keyword; }?>" onchange="keywordValue();">
        <div id="recent-key" onmouseover="$('#recent-key').show();">
            <?php if($curpage == 'search'){?>
            <ul class="search-sugestion hide">
                <li class="head-sugestion">search sugestion</li>
                <li class="sugestion-link" onclick="openSugest(0);"><span class="sugest-key"></span> in <span class="sugest-type">All</span></li>
                <li id="sugest-1" class="sugestion-link" onclick="openSugest(1);"><span class="sugest-key"></span> in <span id="slocation" class="sugest-type" data-value="">all location</span></li>
                <li id="sugest-2" class="sugestion-link" onclick="openSugest(2);"><span class="sugest-key"></span> in <span id="sexpertise" class="sugest-type" data-value="">all expertise</span></li>
                <li id="sugest-3" class="sugestion-link" onclick="openSugest(3);"><span class="sugest-key"></span> in <span id="sfunction" class="sugest-type" data-value="">all function</span></li>
                <li id="sugest-4" class="sugestion-link" onclick="openSugest(4);"><span class="sugest-key"></span> in <span id="sindustry" class="sugest-type" data-value="">all industry</span></li>
                <li id="sugest-5" class="sugestion-link" onclick="openSugest(5);"><span class="sugest-key"></span> in <span id="sexperience" class="sugest-type" data-value="">all experience</span></li>
            </ul>
            <?php } ?>
            <ul>
                <?php if(is_array($data_recents)){ $numkey = 1 ; foreach($data_recents AS $data_recent){ ?>
                <li id="rkey-<?=$numkey;?>" onclick="selectRecent(<?=$numkey;?>)" data-rc_key="<?php echo $data_recent['Qj_keyword'];?>" class="recent-link"><?php echo $data_recent['Qj_keyword'];?></li>
                <?php $numkey++; } } ?>
            </ul>
        </div>
        </div>
        <button class="btn btn-info btn-nav-search" type="submit" onclick="$('#submit-search').click();">Search</button>
    </div> -->
    <?php if($curpage == 'job_detail' || $curpage == 'user_profile'){?>
    </form>
    <?php } ?>
    <?php } ?>
    <div class="desktop-nav">
        <ul>
            <li class="nav-link dropdown">
                <a href="#" data-target="dropdown-user" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
                <?php if($_SESSION['sub'] == "en"){ echo "English"; } else { echo "Indonesian"; }?>
                <span class="glyphicon glyphicon-chevron-down" aria-hidden="true" style="position:relative;top: 2px;font-size: 12px;"></span>
                </a>
                <ul id="dropdown-lang" class="dropdown-menu dropdown-usernav" role="menu" >
                    
                    <?php if(isset($curpage) && $curpage == 'main') { ?>
                    <li><a href="<?php echo checkLang('en'); ?>"><img src="<?=$global['absolute-url'];?>img/flag-english.png" alt="english" class="flag-img">English</a></li>
                    <li><a href="<?php echo checkLang('id'); ?>"><img src="<?=$global['absolute-url'];?>img/flag-indonesia.png" alt="indonesia" class="flag-img">Indonesian</a></li>
                    <?php } else { ?>
                    <li><a href="javascript:;" onclick="changeSub('en')"><img src="<?=$global['absolute-url'];?>img/flag-english.png" alt="english" class="flag-img">English</a></li>
                    <li><a href="javascript:;" onclick="changeSub('id')"><img src="<?=$global['absolute-url'];?>img/flag-indonesia.png" alt="indonesia" class="flag-img">Indonesian</a></li>
                    <?php } ?>
                </ul>
                <span class="badge badge-red">NEW</span>
            </li>
            <li class="nav-link <?php if(isset($_SESSION['userData']['id'])){?>dropdown<?php }?>">
            <!--<img src="<?php echo $global['absolute-url'];?>img/user.png" alt="user">-->
            <?php if(isset($_SESSION['userData']['id']) && isset($_SESSION['userData']['username']) && isset($_SESSION['userData']['email']) && isset($_SESSION['userData']['auth_code'])){?>
                <a href="#" data-target="dropdown-user" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
                <span class="glyphicon glyphicon-user" aria-hidden="true"></span>
                <?php echo $lang['dropdown-hello'].", ".$_SESSION['userData']['username'];?>
                <span class="glyphicon glyphicon-chevron-down" aria-hidden="true" style="position:relative;top: 2px;font-size: 12px;"></span>
                </a>
                <ul id="dropdown-user" class="dropdown-menu dropdown-usernav" role="menu">
                    <span class="caret-user"></span>
                    <li class="dropdown-head"><span><?=$lang['dropdown-edit'];?></span></li>
                    <li id="user-nav-1"><a href="<?=$path['user-edit'];?>"><?=$lang['dropdown-profile'];?></a></li>
                    <li><a href="<?=$path['user-portfolio'];?>"><?=$lang['dropdown-portfolio'];?></a></li>
                    <!-- <li><a href="<?=$path['user-setting-privacy'];?>"><?=$lang['dropdown-setting'];?></a></li> -->
                    <li class="dropdown-head"><span><?=$lang['dropdown-view'];?></span></li>
                    <li><a href="<?=$path['user-page_profile'].$_SESSION['userData']['username'].".cvm";?>" target="_blank"><?=$lang['dropdown-my-profile'];?></a></li>
                    <li><a href="<?=$path['user-resume'].$_SESSION['userData']['username'].".cvm";?>" target="_blank"><?=$lang['dropdown-my-resume'];?></a></li>
                    <li><a href="<?=$path['user-portfolio_image'].$_SESSION['userData']['username'].".cvm";?>" target="_blank"><?=$lang['dropdown-my-portfolio'];?></a></li>
                    <li class="dropdown-bottom"><a href="<?=$path['user-resume-pdf'].$_SESSION['userData']['username'].".pdf";?>" target="_blank"><?=$lang['btn-pdf'];?></a></li>
                    <li class="dropdown-bottom"><a href="<?=$path['user-logout'];?>"><?=$lang['dropdown-logout'];?></a></li>
                </ul>
                <?}else{?>
                <a href="#" data-toggle="modal" data-target="#modal-login">Login / Sign up</a>
                <?php }?>
            </li>
            
            <!--<?php if($curpage == 'main'){?>
            <li class="nav-link-job"><a id="look-job" href="#"  data-toggle="tooltip" data-placement="bottom" title="Coming Soon!">Look for a Job</a></li>
            <?php } ?>-->
        </ul>
    </div>
</div>
<div id="mobile-top-nav" class="visible-xs">
    <div class="row top-nav-mobile">
        <div class="col-xs-4 pad0">
        <button id="btn-nav-mobile" type="button" class="navbar-toggle collapsed toggle-mobile">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
            <!-- <div class="btn-nav-mobile"><a id="btn-nav-mobile" href="#" onclick="" class="btn"><i class="glyphicon glyphicon-align-justify up05"></i></a></div> -->
        </div>
        <div class="col-xs-4 text-center pad0">
            <a id="nav-logo-mobile" href="<?php echo $path['home'];?>" >
                <img src="<?=$global['logo-mobile'];?>" alt="logo">
            </a>
        </div>
        <div class="col-xs-4 pad0">
            <!-- <div class="text-right btn-nav-mobile"><a  id="btn-nav-mobile" href="#" <?php if($curpage == 'search'){?>onclick="searchSlideTag();"<?php } else { ?> onclick="searchSlide();"<?php } ?> class="btn"><i class="glyphicon glyphicon-search up05"></i></a></div> -->
        </div>
    </div>
</div>
<div class="fixed-nav-mobile">
    <ul>
        <?php if(!isset($_SESSION['userData']['id'])){?>
        <li><a href="#" class="nav-mob" data-toggle="modal" data-target="#modal-login">Login</a></li>
        <li><a href="<?=$path['register'];?>" class="nav-mob">Sign Up</a></li>
        <?php } else { ?>
        <li class="dropdown">
            <a href="#" class="nav-mob" onclick="sideDropUser();"><img src="<?=$global['absolute-url'];?>img/dummy.jpg" class="sideUser-pic"><span class="sideUser-name"><?=$_SESSION['userData']['username'];?></span> <span id="caret-drop-user" class="caret-sidebar"></span></a>
            <ul id="sidedrop-user" class="dropdown-sidebar">
                <li class="dropdown-head"><span><?=$lang['dropdown-edit'];?></span></li>
                <li><a href="<?=$path['user-edit'];?>"><?=$lang['dropdown-profile'];?></a></li>
                <li><a href="<?=$path['user-portfolio'];?>"><?=$lang['dropdown-portfolio'];?></a></li>
                <li class="hide"><a href="<?=$path['user-jobs'];?>">Saved Jobs</a></li>
                <!-- <li><a href="<?=$path['user-setting'];?>"><?=$lang['dropdown-setting'];?></a></li> -->
                <li class="dropdown-head"><span><?=$lang['dropdown-view'];?></span></li>
                <li><a href="<?=$path['user-page_profile'].$_SESSION['userData']['username'].".cvm";?>" target="_blank"><?=$lang['dropdown-my-profile'];?></a></li>
                <li><a href="<?=$path['user-resume'].$_SESSION['userData']['username'].".cvm";?>" target="_blank"><?=$lang['dropdown-my-resume'];?></a></li>
                <li><a href="<?=$path['user-portfolio_image'].$_SESSION['userData']['username'].".cvm";?>" target="_blank"><?=$lang['dropdown-my-portfolio'];?></a></li>
                <li><a href="<?=$path['user-resume-pdf'].$_SESSION['userData']['username'].".pdf";?>" target="_blank"><b><?=$lang['btn-pdf'];?></b></a></li>
                <li><a href="<?=$path['user-logout'];?>"><?=$lang['dropdown-logout'];?></a></li>
            </ul>
        </li>
        <?php } ?>
        <li><a href="<?=$global['base'];?>" class="nav-mob">Discover</a></li>
        <?php if(is_array($data_tops)){ ?>
        <li class="dropdown">
            <a href="#" class="nav-mob" onclick="sideDropProfile();">Top 10 Profiles <span id="caret-top-profile" class="caret-sidebar"></span></a>
            <ul id="top-10-profile" class="dropdown-sidebar">
                <?php foreach($data_tops as $data_top){ ?>
                <li><a href="<?=$path['user-resume'].$data_top['User_username'].".cvm";?>" class="nav-mob"><img src="<?=check_image_url($data_top['User_proPhotoThmb']);?>" class="sideUser-pic"><span class="sideUser-name"><?=$data_top['User_fname'];?></span></a></li>
                <?php } ?>
            </ul>
        </li>
        <?php } ?>
        <!-- blog in sidebar -->
        <li><a href="<?=$path['blog'];?>" class="nav-mob">Blog</a></li>
        <li class="hide">
            <a href="#" class="nav-mob" onclick="sideDropJobs();">Hottest Job<span id="caret-hotest-job" class="caret-sidebar"></span></a>
            <ul id="hotest-job" class="dropdown-sidebar">
                <li><a href="#">Action</a></li>
                <li><a href="#">Another action</a></li>
                <li><a href="#">Something else here</a></li>
                <li><a href="#">Separated link</a></li>
            </ul>
        </li>
    </ul>
</div>
<div class="mobile-layer"></div>
<!-- search modal -->
<div id="search-modal">
    <div class="ss-header">
        <div class="ss-htext">Search</div>
        <a href="javascript:;" class="ss-close" onclick="searchSlide();"><span class="glyphicon glyphicon-remove"></span></a>
    </div>
    <div class="ss-layer">
        <form action="<?=$path['job-search'];?>" method="GET">
        <!-- <div class="ss-level-radio">
            <div class="row">
                <div id="mlevel-list" class="col-xs-12">
                    <label class="radio-inline ss-inline">
                        <input type="radio" name="job_level" value="1"> All levels
                    </label>
                </div>
            </div>
        </div> -->
        <!-- <div class="ss-level-select">
            <div class="row">
                <div class="col-xs-12">
                    <select id="mlevel-select" class="form-control ss-select-level" name="expertise">
                        <option value="">All levels</option>
                    </select>
                </div>
            </div>
        </div> -->
        <div class="row up2">
            <div class="col-xs-12 col-mjob">
                <select id="mjob-function" class="form-control select-search" style="width:100%;" name="function">
                    <option value="">All Job Function</option>
                </select>
            </div>
        </div>
        <div class="row up2">
            <div class="col-xs-12">
                <input type="text" class="form-control input-search" name="q" placeholder="search for job title, skills, or keywords">
                <div class="search-location">
                    <img src="<?php echo $global['absolute-url'];?>img/location.png" alt="location" class="location-icon">
                    <span class="dropdown">
                        <a id="drop-mcity" href="#" class="dropdown-toggle location-select" data-toggle="dropdown" aria-expanded="true">Location <span class="caret"></span></a>
                        <ul id="mcity-list" class="dropdown-menu drop-mcity" role="menu">
                        </ul>
                    </span>
                </div>
            </div>
        </div>
        <div class="row up2">
            <div class="col-xs-12">
                <button class="btn btn-block btn-info btn-search" type="submit" style="width:100%;">Search</button>
            </div>
        </div>
        </form>
    </div>
</div><!--  end search modal -->
<!-- Modal Login-->
<div class="modal fade" id="modal-login" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog mlogin-size">
    <div class="modal-content mlogin-content">
      <div class="modal-body mlogin-body">
        <div class="row">
            <div class="col-xs-6"><div class="mlogin-header">CIVIMI Login</div></div>
            <!--<div class="col-xs-6"><div class="mlogin-rheader">Employer Login <span style="color:#D54E55;">&gt;&gt;</span></div></div>-->
            <div class="col-xs-12"><hr/></div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-modal-left">
                <form name="register" action="<?=$path['user-login'];?>" method="post" enctype="multipart/form-data" >
                    <?php if(isset($_SESSION['statusLogin'])){ ?>
                        <div class="row">
                            <div class="col-xs-12">
                                <div style="color: #a94442;font-weight: bold;font-size: 13px;margin: 3px 0;"><?php echo $_SESSION['statusLogin'];?></div>
                            </div>
                        </div>
                    <?php } ?>
                    <input type="email" class="form-control up15" name="email" placeholder="Email (name@example.com)" required>
                    <input type="password" class="form-control up15" name="password" placeholder="Password" required>
                    <div class="forget-pass">
                        <a href="<?=$path['forgot-password'];?>">Forget password?</a>
                    </div>
                    <button class="btn btn-info btn-block up15" type="submit">Login</button>
                </form>
            </div>
            <div class="col-xs-1 col-modal-center hidden-xs"><img src="<?php echo $global['absolute-url'];?>img/or.png" alt="border"></div>
            <div class="col-xs-12 col-modal-right">
                <a href="http://login.facebook" onclick="this.href='<?=$path['login-with-facebook'];?>'" type="button" class="btn btn-info btn-facebook btn-block"><img src="<?php echo $global['absolute-url'];?>img/btn-facebook.png" alt="facebook">Login with Facebook</a>
                <a href="http://login.google" onclick="this.href='<?=$path['login-with-google'];?>'" type="button" class="btn btn-info btn-google btn-block"><img src="<?php echo $global['absolute-url'];?>img/btn-google.png" alt="google">Login with Google+</a>
                <a href="http://login.linkedin" onclick="this.href='<?=$path['login-with-linkedin'];?>'" type="button" class="btn btn-info btn-linkedin btn-block"><img src="<?php echo $global['absolute-url'];?>img/btn-linkedin.png" alt="linkedin">Login with LinkedIn</a>
                <!--<a href="http://login.twitter" onclick="this.href='<?=$path['login-with-twitter'];?>'" type="button" class="btn btn-info btn-twitter btn-block"><img src="<?php echo $global['absolute-url'];?>img/btn-twitter.png" alt="twitter">Login with Twitter</a>-->
                <br />
                <div class="mlogin-register">Don't have an account? <a href="<?=$path['register'];?>">Join now</a></div>
            </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script>

    function changeSub(sub){
        var link = "<?php echo 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];?>";
        jQuery.ajax({
        url: "<?php echo $global['absolute-url'];?>packages/setlanguage.php",
        type: "POST",
        data: {
            lang: sub,
        },
        dataType : 'json',
        success: function(data, textStatus, xhr) {
            console.log(data); // do with data e.g success message
            window.location.href = link;
        }
        });
    }
    $("#message1").fadeOut(8400);  

    <?php if(isset($_GET['msg'])){
        if($_GET['msg'] == 'ERROR:_login-failed'){ ?>
        $('#modal-login').modal('show'); 
    <?php }} ?>
    
    function sideDropUser(){
        $("#sidedrop-user").slideToggle();
        $("#caret-drop-user").toggleClass('caret-sidebar-up');
    }
    function sideDropProfile(){
        $("#top-10-profile").slideToggle();
        $("#caret-top-profile").toggleClass('caret-sidebar-up');
    }
    function sideDropJobs(){
        $("#hotest-job").slideToggle();
        $("#caret-hotest-job").toggleClass('caret-sidebar-up');
    }
    $("#user-nav-1").on("mouseover", function(){
        $(".caret-user").addClass('caret-hover');
    });
    $("#user-nav-1").on("mouseout", function(){
        $(".caret-user").removeClass('caret-hover');
    });
    <?php if($curpage == 'search'){?>
    function openSugest(param){
        var q = $('#input-nav-qsearch').val();
        if (param == 0 ){
            window.location.href = "<?php echo $global['absolute-url'];?>search3.php?q="+q;
        } else if (param == 1 ){
            var val = $('#slocation').data('value');
            window.location.href = "<?php echo $global['absolute-url'];?>search3.php?q="+q+"&location="+val;
        } else if (param == 2){
            var val = $('#sexpertise').data('value');
            window.location.href = "<?php echo $global['absolute-url'];?>search3.php?q="+q+"&expertise="+val;
        } else if (param == 3){
            var val = $('#sfunction').data('value');
            window.location.href = "<?php echo $global['absolute-url'];?>search3.php?q="+q+"&function="+val;
        } else if (param == 4){
            var val = $('#sindustry').data('value');
            window.location.href = "<?php echo $global['absolute-url'];?>search3.php?q="+q+"&industry="+val;
        } else if (param == 5){
            var val = $('#sexperience').data('value');
            window.location.href = "<?php echo $global['absolute-url'];?>search3.php?q="+q+"&experience="+val;
        }
    }
    $(document).ready(function(){
        recentSugest();
    })
    function recentSugest(){
        var q = $('#input-nav-qsearch').val();
        if(q.length > 2){
            $('.search-sugestion').removeClass('hide');
            $('.sugest-key').text(q);
        } else {
            $('.search-sugestion').addClass('hide');
        }
    }
    <?php } ?>
    <?php if($curpage == 'search' || $curpage == 'job_detail' || $curpage == 'user_profile'){ ?>
    function selectRecent(param){
        var keyword = $('#rkey-'+param).data('rc_key');
        $('#input-qsearch').val(keyword);
        $('#input-nav-qsearch').val(keyword);
        $('#recent-key').hide();
    }
    $('html').click(function() {
        $('#recent-key').hide();
    });
    $('#search-autocomplete').click(function(event){
        event.stopPropagation();
    });
    <?php } ?>
    $('#btn-nav-mobile').click(function() {
        $('.mobile-layer').toggle();
        $('.fixed-nav-mobile').toggle('slide', {
            direction : 'left'
        }, 200); 
        $('body').toggleClass('hide-scroll');
        return false;
    });
    $('.mobile-layer').click(function(){
        $('.mobile-layer').toggle();
        $('.fixed-nav-mobile').toggle('slide', {
            direction : 'left'
        }, 100); 
        $('body').toggleClass('hide-scroll');
        return false;
    })
    function searchSlide() {
        $('body').toggleClass('hide-scroll');
        $('#search-modal').fadeToggle(300);
    };
    // get_mlevel();
    function get_mlevel(){
      var url = "<?=$api['level-index'];?>";
          $.ajax({url: url,success:function(result){
             
              // $('#mlevel-list').append(replace_mlevel(result));
              $('#mlevel-select').append(replace_mlevel_select(result.data));
        }});
    }
    // function replace_mlevel(datas){
    // var resultHTML='';resultNext = "";
    // var total = 0;previous = "";user_name = "";link="";
    // var d = new Date();strip_string ="";
    // var current = d.getTime();
    //      // console.log(datas);
    //     if(datas != null){
    //       var obj = datas;    

    //       for(var i=0;i < obj.length;i++){

    //         if(obj[i].Level_ID != '' && obj[i].Level_title != ''){

    //             resultHTML += "<label class='radio-inline ss-inline'><input type='radio' name='expertise' value='"+obj[i].Level_ID+"' > "+obj[i].Level_title+"</label>";
    //           }
    //         }
    //       }
    //       return resultHTML;
    // };
    function replace_mlevel_select(datas){
    var resultHTML='';resultNext = "";
    var total = 0;previous = "";user_name = "";link="";
    var d = new Date();strip_string ="";
    var current = d.getTime();
         // console.log(datas);
        if(datas != null){
          var obj = datas;    

          for(var i=0;i < obj.length;i++){

            if(obj[i].Level_ID != '' && obj[i].Level_title != ''){

                resultHTML += "<option value='"+obj[i].Level_ID+"'>"+obj[i].Level_title+"</option>";
              }
            }
          }
          return resultHTML;
    };
    get_mjfunction();
    function get_mjfunction(){
      var url = "<?=$api['field-index'];?>";
          $.ajax({url: url,success:function(result){
             
              $('#mjob-function').append(replace_mjfunction(result.data));
              $("#mjob-function").select2({
                placeholder: "all job function"
              });
        }});
    }
    function replace_mjfunction(datas){
    var resultHTML='';resultNext = "";
    var total = 0;previous = "";user_name = "";link="";
    var d = new Date();strip_string ="";
    var current = d.getTime();
         // console.log(datas);
        if(datas != null){
          var obj = datas;    

          for(var i=0;i < obj.length;i++){

            if(obj[i].Field_ID != '' && obj[i].Field_title != ''){

                resultHTML += "<option value='"+obj[i].Field_ID+"'>"+obj[i].Field_title+"</option>";
              }
            }
          }
          return resultHTML;
    };
    get_mcity();
    function get_mcity(){
      var url = "<?=$api['city-index'];?>";
          $.ajax({url: url,success:function(result){
             
              $('#mcity-list').append(replace_mcity(result.data));
        }});
    }
    function replace_mcity(datas){
    var resultHTML='';resultNext = "";
    var total = 0;previous = "";user_name = "";link="";
    var d = new Date();strip_string ="";
    var current = d.getTime();
          // console.log(datas);
        if(datas != null){
          var obj = datas;    

          for(var i=0;i < obj.length;i++){

            if(obj[i].City_ID != '' && obj[i].City_title != ''){

                resultHTML += "<li><div class='radio city-radio'><label><input type='radio' data-city_title='"+obj[i].City_title+"' name='location' id='rcity-"+obj[i].City_ID+"' value='"+obj[i].City_ID+"' onclick='placeCity("+obj[i].City_ID+");'>"+obj[i].City_title+"</label></div></li>";
              }
            }
          }
          return resultHTML;
    };
    function placeCity(param){
      var title = $('#rcity-'+param).data('city_title');
      $('#drop-city').html('');
      $('#drop-city').html(title+' <span class="caret"></span>');
      $('#drop-mcity').html('');
      $('#drop-mcity').html(title+' <span class="caret"></span>');
    }
</script>