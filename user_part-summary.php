<div id="personal-summary-section" class="job-sbox" data-order="1" style="border-radius: 0;">
  <div id="summary-header" class="user-section-header row border-bottom-eee">
    <div class="col-xs-6">
      <div class="profile-title"><i class="glyphicon glyphicon-pencil"></i>&nbsp;&nbsp;<?=$lang['sum-title'];?></div>
    </div>
    <div class="col-xs-6">
        <div class="edit-button text-right">
          <span class="sort-icon"><i class="glyphicon glyphicon-sort"></i></span>
        </div>
    </div>
  </div>
  <div id="summary-profile" class="">
    <div id="summary-edit-box" class="summary-edit-box hide">
      <div class="row">
        <div class="col-xs-12 pad0">

          <div class="row">
            <div class="col-xs-12 pad0">
              <textarea id="summary-text" class="form-control no-radius" rows="5" placeholder="<?=$lang['sum-desc_placeholder'];?>"><?=$data_user[0]['User_obj'];?></textarea>
            </div>
            <div class="col-xs-12 pad0 up1">
              <a id="btn-update-summary" class="btn btn-primary" onclick="update_summary();">Save</a>  
              <a id="btn-cancel-summary" class="btn btn-default" onclick="edit_toggle('.summary-box', '.summary-edit-box');">Cancel</a>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="row row-summary">
      <div class="col-xs-12">
        <a href="javascript:;" onclick="edit_toggle('.summary-box', '.summary-edit-box');" class="summary-box">
          <span class="summary-desc">
          
          </span>
          <span class="summary-etext edit-button"><i class="glyphicon glyphicon-edit"></i> <span class="hidden-xs">Edit</span></span>
        </a>
        <div class="summary-loader text-center hide" style="padding:10px;">
          <img src="<?=$global['absolute-url'];?>img/load-blue.gif" />
        </div>
      </div>
    </div>
  </div>

<script>
var text = $("#summary-text").val();
var s = encodesummary(text);
<?php if ($data_user[0]['User_obj'] != ""){ ?>
$(".summary-desc").html(s);
<?php } else { ?>
$(".summary-desc").html("<span id='empty-summary' class='empty-file'><?=$lang['sum-empty'];?></span>");  
<?php } ?>
function edit_toggle(noneditable,editable){
  if($(editable).hasClass("hide")){
    $(noneditable).addClass("hide");
    $(editable).removeClass("hide");
  }else{
    $(editable).addClass("hide");
    $(noneditable).removeClass("hide");
  }
}

function encodesummary(str) {
     if (typeof(str) == "string") {
        str = str.replace(/&gt;/ig, ">");
        str = str.replace(/&lt;/ig, "<");
        str = str.replace(/&#039;/g, "'");
        str = str.replace(/&quot;/ig, '"');
        str = str.replace(/\r\n/g, "<br/>");
        str = str.replace(/\r/g, "<br/>");
        str = str.replace(/\n/g, "<br/>");
        str = str.replace(/&amp;/ig, '&'); /* must do &amp; last */
    }
    return str;
}
function update_summary(){
  $(".summary-edit-box").addClass("hide"); 
  $(".summary-loader").removeClass("hide");
  $('html, body').animate({
        scrollTop: $('#summary-header').offset().top
      }, 'fast');
  var url = "<?=$api['user-update_summary'];?>";
  var sum = $("#summary-text").val();
  var data = {
    id : "<?=$_SESSION['userData']['id'];?>",
    auth_code : "<?=$_SESSION['userData']['auth_code'];?>",
    summary : sum
  }
  $.ajax({url: url,data : data, success:function(result){
    console.log(result);

    $(".summary-loader").addClass("hide"); 
    var sumTxt = $("#summary-text").val();
    if(sumTxt != ''){
      var decodetxt = encodesummary(sumTxt);
      $(".summary-desc").html(decodetxt);
    } else {
      $(".summary-desc").html("<span id='empty-summary' class='empty-file'><?=$lang['sum-empty'];?></span>");
    }
    $(".summary-box").removeClass("hide");
  }
});
}
</script>
</div> <!-- end personal summary section -->