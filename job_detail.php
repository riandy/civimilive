<?php 
include("packages/require.php");
include("controller/controller_global.php");
include("controller/controller_job_detail.php");
$curpage='job_detail';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title><?=$global['title-job-detail']." | ".$data_jobss[0]['Jobs_title'];?></title>
    <meta name="keywords" content="<?=$seo['keyword-job-detail'];?>">
    <meta name="description" content="<?=$seo['desc-job-detail'];?>">
    <?php include("packages/head.php");?>
</head>
<body>
    <div id="all">
        <!--START HEADER -->
        <?php include("section-top-nav.php");?>
        <!--END HEADER -->
        <div id="job-detail-section">
        <?php if(is_array($data_jobss)){
            foreach($data_jobss as $data_jobs){ ?>
		<div class="job-header-section">
            <div class="up7 visible-xs"></div>
			<div class="container">
                            <div class="col-xs-12">
                            <!--<a href="#back">&lt; back to search results</a>!-->
                            </div>
			</div>
		</div>
        <div class="job-detail-content hide">
            <div class="container">
                <div class="row">
                    <div class="col-xs-7 col-sm-8 col-md-9 col-job-content">
                        <div class="up1 visible-xs"></div>
                        <div class="job-company-name"><?php echo correctDisplay($data_jobs['Company_title']);?></div>
                        <div class="job-company-motto up05"><?php echo correctDisplay($data_jobs['Company_motto']);?></div>
                        <div class="job-company-bottom up15">
                            <span class="job-company-website"><?php echo $data_jobs['Company_email'];?></span>
                            <button type="button" class="btn btn-info btn-sm btn-overview" data-toggle="modal" data-target="#modal-overview">Company Overview <span class="glyphicon glyphicon-info-sign"></span></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
            <div class="container">
            <div class="row">
                <div class="col-sm-9 col-xs-12">
                    <div class="row search-content">
                        <div class='row s-content-title'>
                            <div class='col-xs-12 col-sm-10'>
                                <div class="job-stitle"><?php echo $data_jobs['Jobs_title'];?></div>
                                <div class="list-saddress"><img src="http://www.civimi.com/img/map.png" alt="map"><?php echo $data_jobs['City_title'];?>, <?php echo $data_jobs['Country_title'];?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="glyphicon glyphicon-eye-open"></span>&nbsp;&nbsp;<?=$data_hits;?> views</div>
                            </div>
                        <div class='col-sm-2 hidden-xs'>
                            <a href="#company-url-here" data-toggle="modal" data-target="#modal-overview"><img src="<?php if($data_jobs['Company_img'] != ""){ echo $global['img_url'].$data_jobs['Company_img'];}else{ echo $global['img-url']."img/work.png";}?>" alt="<?php echo $data_jobs['Company_title'];?> logo" class="scontent-img" /></a>
                        </div>
                        </div>
                            
                             <div class="col-xs-12">
                            <div class="job-posted-date">posted by <a href="#company-url-here" data-toggle="modal" data-target="#modal-overview"><?php echo correctDisplay($data_jobs['Company_title']);?></a> on <?php echo date("d M Y",strtotime($data_jobs['Jobs_create_date']));?></div>
                            <div class="job-description-header"><?php if($data_jobs['Jobs_content'] != ""){echo "Job Description";}?></div>
                            <div class="job-description">
                                <?php echo correctDisplay($data_jobs['Jobs_content']);?>
                            </div>
                            <div class="job-qualification-header"><?php if($data_jobs['Jobs_qualification_info'] != ""){echo "Qualification";}?></div>
                                <?php echo correctDisplay($data_jobs['Jobs_qualification_info']);?>
                            <?php if($data_jobs['Jobs_apply_info'] != ""){?>
                            <div class="job-apply-header">How to Apply</div>
                            <div class="job-apply-desc">
                                <?php echo correctDisplay($data_jobs['Jobs_apply_info']);?>
                            </div>
                            
                            <div class="apply-btn-place">
                                <!--<button type="button" class="btn btn-success xs-100">Apply Now</span></button>
                                <span class="or-text">OR</span>
                                <button type="button" class="btn btn-info xs-100">Apply Now</button>!-->
                                <?php if(isset($_SESSION['userData']['id'])){$target="#career-apply";} //if user login
                                    else{$target = "#modal-login";}?>
                                <button class="btn btn-info btn-apply xs-100" data-toggle="modal" data-target="<?php echo $target;?>">Apply Now</button>
                            </div>
                            <?php };?>
                        </div>
                    </div>
                    
                </div>
                <div class="col-sm-3 col-xs-12">
                    <div id="related-job-col" class="job-sbox line13 row hide">
                        <div class="col-xs-12 no-padding">  
                            <div class="job-related-header">
                                Related Jobs
                            </div>
                            <div id="related-job">
                            </div>
                        </div>
                    </div>
                    <div id="related-company-col" class="row job-sbox line13 hide">
                                    <div class="col-xs-12 no-padding">  
                                        <div class="job-related-header">
                                            Other Jobs by <?php echo correctDisplay($data_jobs['Company_title']);?>
                                        </div>
                                        <div id="related-company">
                                        </div>
                                    </div>
                                </div>
                    <?php if(is_array($data_related_news)){?>
                    <div class="row job-sbox line13">
                        <div class="col-xs-12 no-padding">  
                            <div class="job-related-header">
                                Related Articles
                            </div>
                            <?php foreach($data_related_news as $data){?>
                            <a href="<?php echo $path['blog'].$data['News_ID']."_".encode($data['News_title']).".html";?>" class="job-related-box">
                                <div class="job-related-title"><?php echo $data['News_title'];?></div>
                                <div class="job-related-location-time">
                                    <span class="job-related-location"><?php echo $data['Nc_title'];?></span>
                                    <span class="job-related-time"><?php echo relative_time($data['News_create_date']);?></span>
                                </div>
                            </a>
                            <?php }?>
                        </div>
                    </div>
                    <?php }?>
                </div>
            </div>
                </div>
           
        <?php }} ?>
        <div class="up3"></div>
        </div>
        <!--START Footer -->
        <?php include("section-footer.php");?>
        <!--END Footer -->
    </div><!--  end all div -->
    
    <?php if(is_array($data_jobss)){
    foreach($data_jobss as $data_jobs){ ?>
    <!-- Modal -->
    <div class="modal fade" id="modal-overview" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <div class="modal-logo"><img src="<?=$global['logo-mobile'];?>" alt="logo"></div>
          </div>
          <div class="modal-body">
            <div class="modal-company-logo"><img src="<?php if($data_jobs['Company_img'] != ""){echo $global['img_url'].$data_jobs['Company_img'];}else{ echo $global['img-url']."img/work.png";}?>" alt="logo"></div>
            <div class="modal-company-name"><?php echo $data_jobs['Company_title'];?></div>
            <?php if($data_jobs['Company_content'] != ''){?>
            <div class="modal-company-warp">
                <div class="modal-company-header">Company Overview</div>
                <div class="modal-company-desc">
                    <?php echo correctDisplay($data_jobs['Company_content']);?>
                </div>
            </div>
            <?php }?>
          </div>
          <div class="modal-footer">
          </div>
        </div>
      </div>
    </div>

    <!-- modal apply career -->
    <div class="modal fade" id="career-apply" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header modal-header-footer">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h1 class="modal-title-footer">Apply Job in Civimi</h1>
                </div>
                <div class="modal-body modal-body-footer">
                    <div class="row up1">
                        <div class="col-xs-12 pad0">
                            <form name="applyCareer" action="<?=$global['client-page'];?>job_detail.php?action=apply" method="post" enctype="multipart/form-data">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-3">
                                        <div class="career-input-header">Job *</div>
                                    </div>
                                    <div class="col-xs-12 col-sm-9">
                                        <input id="job-title-modal" type="text" name="job" class="form-control" placeholder="choosen job" value="<?php echo $data_jobs['Jobs_title'];?>" disabled readonly>
                                    </div>
                                </div>
                                <div class="row up1">
                                    <div class="col-xs-12 col-sm-3">
                                        <div class="career-input-header">Email *</div>
                                    </div>
                                    <div class="col-xs-12 col-sm-9">
                                        <input type="email" name="email" class="form-control" placeholder="email address" required>
                                    </div>
                                </div>
                                <div class="row up1">
                                    <div class="col-xs-12 col-sm-3">
                                        <div class="career-input-header">Full Name *</div>
                                    </div>
                                    <div class="col-xs-12 col-sm-9">
                                        <input type="text" name="name" class="form-control" placeholder="full name" required>
                                    </div>
                                </div>
                                <div class="row up1">
                                    <div class="col-xs-12 col-sm-3">
                                        <div class="career-input-header">Phone Number *</div>
                                    </div>
                                    <div class="col-xs-12 col-sm-9">
                                        <input type="text" name="phone" class="form-control" placeholder="phone number" required>
                                    </div>
                                </div>
                                <div class="row up1">
                                    <div class="col-xs-12 col-sm-3">
                                        <div class="career-input-header">CV File *</div>
                                    </div>
                                    <div class="col-xs-12 col-sm-9">
                                        <input type="file" name="cv" class="form-control" required>
                                    </div>
                                </div>
                                <div class="row up1">
                                    <div class="col-xs-12 col-sm-3">
                                        <div class="career-input-header">Message</div>
                                    </div>
                                    <div class="col-xs-12 col-sm-9">
                                        <textarea class="form-control" rows="6" name="message"></textarea>
                                    </div>
                                </div>
                                <div class="row up1">
                                    <div class="col-xs-12 col-sm-3"></div>
                                    <div class="col-xs-12 col-sm-9">
                                        <img src="<?php echo $global['absolute-url-captcha'];?>" alt="captcha" >
                                        <input type="text" name="captcha_text" class="form-control up05" placeholder="input captcha" required>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12">
                                        <input type="hidden" name="job_id" class="form-control" value="<?php echo $data_jobs['Jobs_ID'];?>">
                                        <input type="hidden" name="job_title" class="form-control" value="<?php echo $data_jobs['Jobs_title'];?>">
                                        <input type="hidden" name="company_title" class="form-control" value="<?php echo $data_jobs['Company_title'];?>">
                                        <input type="hidden" name="user_id" class="form-control" value="<?php echo $_SESSION['userData']['id'];?>">
                                        <button class="btn btn-info btn-apply up2" type="submit" style="width:100%;">Apply Now</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php }} ?>
</body>
<script>
    get_related_jobs();
    function get_related_jobs(){
        var url = "<?=$api['job-related'];?>&jobs_id=<?=$O_id;?>";
        $.ajax({url: url,success:function(result){

            $('#related-job').append(replace_related(result.data));
        }});  
    }
    function replace_related(datas){
    var resultHTML='';resultNext = "";
    var total = 0;previous = "";user_name = "";link="";
    var d = new Date();strip_string ="";
    var current = d.getTime();
        // console.log(current);
        if(datas != null){
          $('#related-job-col').removeClass('hide');
          var obj = datas;    

          for(var i=0;i < obj.length;i++){

            if(obj[i].Jobs_ID != '' && obj[i].Jobs_title != ''){

              var jobTitle = obj[i].Jobs_title;
              var regex = /\\/g;
              var replaceTitle = jobTitle.replace(regex, "").replace(/&quot;/g, "");
              var lowercase = replaceTitle.toLowerCase();
              var encodeTitle = lowercase.replace(/\s+/g, '-');

                link = "<?=$path['job-detail'];?>"+encodeTitle+"_"+obj[i].Jobs_ID+".html";

                resultHTML += "<a href='"+link+"' class='job-related-box'>";
                resultHTML += "<div class='job-related-title'>"+obj[i].Jobs_title+"</div>";
                resultHTML += "<div class='job-related-location-time'>";
                resultHTML += "<span class='job-related-location'>"+obj[i].Company_title+", "+obj[i].City_title+"</span>";
                // resultHTML += "<span class='job-related-time'>"+obj[i].Jobs_ID+"</span>";
                resultHTML += "</div>";
                resultHTML += "</a>";
              }
            }
          }
          return resultHTML;
    };
    get_related_company();
    function get_related_company(){
        var url = "<?=$api['job-company-related'];?>&jobs_id=<?=$O_id;?>&company_id=<?=$O_companyID;?>";
        $.ajax({url: url,success:function(result){

            $('#related-company').append(replace_related_company(result.data));
        }});  
    }
    function replace_related_company(datas){
    var resultHTML='';resultNext = "";
    var total = 0;previous = "";user_name = "";link="";
    var d = new Date();strip_string ="";
    var current = d.getTime();
        // console.log(current);
        if(datas != null){
          $('#related-company-col').removeClass('hide');
          var obj = datas;    

          for(var i=0;i < obj.length;i++){

            if(obj[i].Jobs_ID != '' && obj[i].Jobs_title != ''){


              if(obj[i].Jobs_intern == 1){ var worktime = "Intern"; }                                           
              if(obj[i].Jobs_fulltime == 1){ var worktime = "Fulltime";}                                      
              if(obj[i].Jobs_parttime == 1){ var worktime = "Partime";}                                         
              if(obj[i].Jobs_freelance == 1){ var worktime = "Freelance";}  

              var jobTitle = obj[i].Jobs_title;
              var regex = /\\/g;
              var replaceTitle = jobTitle.replace(regex, "").replace(/&quot;/g, "");
              var lowercase = replaceTitle.toLowerCase();
              var encodeTitle = lowercase.replace(/\s+/g, '-');

                link = "<?=$path['job-detail'];?>"+encodeTitle+"_"+obj[i].Jobs_ID+".html";

                resultHTML += "<div class='col-xs-12 no-padding'>";
                resultHTML += "<a href='"+link+"' class='job-related-box'>";
                resultHTML += "<div class='other-job-title job-related-title'>"+obj[i].Jobs_title+"</div>";
                resultHTML += "<div class='other-job-location-time'>";
                resultHTML += "<span class='other-job-location'>"+obj[i].City_title+", "+obj[i].Country_title+"</span>";
                //resultHTML += "<span class='other-job-time'>"+worktime+"</span>";
                resultHTML += "</div>";
                resultHTML += "</a>";
                resultHTML += "</div>";
              }
            }
          }
          return resultHTML;
    };
</script>
</html>
