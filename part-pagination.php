<div id="pagination-content" class="row">
    <div class="col-xs-12">
        <div class="hidden-xs">
            <div class="part-pagination text-center">
                <ul class="pagination">
                <?php 
                $batch = getBatch($O_page);
                if($batch < 1){$batch = 1;}
                $prevLimit = 1 +(5*($batch-1));
                $nextLimit = 5 * $batch;
                if($nextLimit > $total_page){$nextLimit = $total_page;}
                ?>
                <?php if($O_page > 1){?>
                  <li><a href="<?php echo $page_name.($O_page-1);?>/">&laquo;</a></li>
                <?php } ?>
                <?php for($pagina = $prevLimit;$pagina <= $nextLimit;$pagina++){?>
                  <li class="<?php if($pagina == $O_page){echo "active";}?>"><a href="<?php echo $page_name.($pagina);?>/"><?php echo $pagina;?></a></li>
                <?php } ?>
                <?php if($total_page > 1 && ($O_page < $total_page)){?>
                  <li><a href="<?php echo $page_name.($O_page+1);?>/">&raquo;</a></li>
                <?php } ?>
              </ul>
          </div>
      </div>
      <div class="visible-xs">
        <div class="part-pagination text-center">
            <ul class="pager pagination">
            <?php if ($total_page >= 1 && $O_page > 1) {?>
              <li class="previous"><a href="<?php echo $page_name.($O_page-1);?>/" style="border-radius: 50px;margin: 0 10px;">&larr; Prev</a></li>
            <?php } else { ?>
            <li class="previous disabled"><a style="border-radius: 50px;margin: 0 10px;">&larr; Prev</a></li>
            <?php } ?>
              <li class="active"><a href="<?php echo $page_name.($O_page);?>/"><?php echo $O_page;?></a></li>
            <?php if($total_page > 1 && ($O_page < $total_page)){?>
              <li class="next"><a href="<?php echo $page_name.($O_page+1);?>/" style="border-radius: 50px;margin: 0 10px;">Next &rarr;</a></li>
            <?php } else { ?>
              <li class="next disabled"><a style="border-radius: 50px;margin: 0 10px;">Next &rarr;</a></li>
            <?php } ?>
            </ul>
        </div>
      </div>
    </div>
</div>