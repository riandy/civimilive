<?php require_once("packages/analytic.php");?>
<div id="footer-content">
    <div class="container">
        <div class="desktop-footer">
            <div class="row">
                <div class="col-sm-3 col-md-2 hidden-xs">
                    <a href="#" class="give-feedback" data-toggle="modal" data-target="#modalFeedback"><?=$lang['feedback-link'];?></a>
                    <div class="nav-footer">
                        <ul>
                            <!-- <li><a href="">About us</a></li>
                            <li><a href="">Jobs</a></li>
                            <li><a href="">Design</a></li> -->
                        </ul>
                    </div>
                </div>
                <div class="col-sm-3 col-md-2 hidden-xs">
                    <div class="nav-footer">
                        <ul>
                            <!-- <li><a href="">Blog</a></li>
                            <li><a href="">Sketchbook</a></li> -->
                        </ul>
                    </div>
                    <!-- <div class="footer-social">
                        <a href="" class="social-logo"><img src="<?=$global['absolute-url'];?>img/logo/facebook.png" alt=""></a>
                        <a href="" class="social-logo"><img src="<?=$global['absolute-url'];?>img/logo/google.png" alt=""></a>
                        <a href="" class="social-logo"><img src="<?=$global['absolute-url'];?>img/logo/twitter.png" alt=""></a>
                    </div> -->
                </div>
                <div class="col-sm-6 col-md-8 col-xs-12">
                    <div class="footer-slogan"><?=$lang['footer-note']?></div>
                </div>
            </div>
            <div class="row visible-xs">
                <div class="col-xs-12 text-center">
                    <button class="btn btn-info btn-morelink" data-toggle="modal" data-target="#modalFooter">More Links</button>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <div class="footer-copyright"><?=$global['copyright'];?></div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- modal footer -->
<div class="modal fade" id="modalFeedback" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-feedback">
        <div class="modal-content" style="border-radius:0;">
            <div class="modal-body feedback-wrapper">
                <form name="contact" action="<?=$global['absolute-url'];?>index.php?action=feedback" onsubmit="return validateFeed();" method="post" enctype="multipart/form-data" class="feedback-inner">
                    <button type="button" class="close feedback-close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h1 class="feedback-head"><?=$lang['feedback-head'];?></h1>
                    <div class="row up05">
                        <div class="col-sm-6 col-xs-12 up1">
                            <input id="feedback-name" name="feedback_name" type="text" class="form-control" placeholder="<?=$lang['feedback-name'];?>" value="<?php if(isset($_SESSION['feedback_name'])){ echo $_SESSION['feedback_name']; }?>">
                        </div>
                        <div class="col-sm-6 col-xs-12 up1">
                            <input id="feedback-email" name="feedback_email" type="email" class="form-control" placeholder="<?=$lang['feedback-email'];?>" value="<?php if(isset($_SESSION['feedback_email'])){ echo $_SESSION['feedback_email']; }?>">
                        </div>
                    </div>
                    <div class="row up1">
                        <div class="col-xs-12 mc-col">
                            <textarea id="feedback-desc" class="form-control" name="feedback_message" rows="3" placeholder="<?=$lang['feedback-message'];?>"><?php if(isset($_SESSION['feedback_message'])){ echo $_SESSION['feedback_message']; }?></textarea>
                        </div>
                    </div>
                    <div class="row up1">
                        <div class="col-xs-12">
                            <img src="<?php echo $global['absolute-url-captcha'];?>" alt="captcha" >
                            <input id="feedback-captcha" type="text" class="form-control up05" name="feedback_captcha" placeholder="<?=$lang['feedback-captcha'];?>">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div id="feedback-alert" class="valid-message text-center"></div>
                        </div>
                    </div>
                    <div class="row up2">
                        <div class="col-xs-12 text-center">
                            <input name="feedback_url" type="hidden" value="<?php echo 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];?>">
                            <button class="btn btn-info bold" type="submit"><?=$lang['feedback-send'];?></button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- modal footer -->
<div class="modal fade" id="modalFooter" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content" style="border-radius:0;">
            <div class="modal-header modal-header-footer">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h1 class="modal-title-footer">More Links</h1>
            </div>
            <div class="modal-body modal-body-footer">
                <div class="row up1">
                    <div class="col-xs-12 text-center">
                        <div class="modal-nav-footer">
                            <ul>
                                <li><a href="<?=$global['absolute-url'];?>">About us</a></li>
                                <li><a href="<?=$global['absolute-url'];?>">Jobs</a></li>
                                <li><a href="http://design.civimi.com/">Design</a></li>
                                <li><a href="<?=$global['absolute-url'];?>">Blog</a></li>
                                <li><a href="<?=$global['absolute-url'];?>">Sketchbook</a></li>
                                <li><a href="#" data-dismiss="modal" data-toggle="modal" data-target="#modalFeedback"><?=$lang['feedback-link'];?></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer" style="background-color: #03A1E4;">
                <div class="modal-footer-social">
                    <a href="" class="social-logo"><img src="<?=$global['absolute-url'];?>img/logo/facebook.png" alt=""></a>
                    <a href="" class="social-logo"><img src="<?=$global['absolute-url'];?>img/logo/google.png" alt=""></a>
                    <a href="" class="social-logo"><img src="<?=$global['absolute-url'];?>img/logo/twitter.png" alt=""></a>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- popup alert -->
<div id="alert-popups" class="popup">
  <div id="popups-text" class="popup-text"></div>
  <a id="popups-close" href="javascript:;" class="popup-close"><i class="glyphicon glyphicon-remove"></i></a>
</div>
<!--Start of Zopim Live Chat Script-->
<script type="text/javascript">
window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute('charset','utf-8');
$.src='//v2.zopim.com/?2A3PPDcs86vxL2CiM0VSqddRgEbcZIBX';z.t=+new Date;$.
type='text/javascript';e.parentNode.insertBefore($,e)})(document,'script');
function popups(message){
    $("#alert-popups").fadeIn();
    $("#popups-text").text(message);
    setTimeout(function(){
        $("#alert-popups").fadeOut();
    }, 5000);
}
$("#popups-close").click(function(){
    $("#alert-popups").fadeOut();
})
<?php if(isset($messageFeedback)){ if($messageFeedback != null){?>
$(document).ready(function(){
    var message = "<?php echo $messageFeedback;?>";
    popups(message);
});
<?php } } ?>
function validateFeed(){
    var feed_name = $("#feedback-name").val();
    var feed_email = $("#feedback-email").val();
    var feed_desc = $("#feedback-desc").val();
    var feed_captcha = $("#feedback-captcha").val();
    var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

    if(feed_name != ""){
        $("#feedback-name").removeClass('input-error');
        $("#feedback-alert").text("");
    } else {
        $("#feedback-name").addClass('input-error');
        $("#feedback-alert").text("please input your name!");
        return false;
    }
    if(feed_email != ""){
        if(feed_email.match(mailformat)) {
            $("#feedback-email").removeClass('input-error');
            $("#feedback-alert").text("");
        } else {
            $("#feedback-email").addClass('input-error');
            $("#feedback-alert").text("your email is not correct!");
            return false;
        }
    } else {
        $("#feedback-email").addClass('input-error');
        $("#feedback-alert").text("please input your email!");
        return false;
    }
    if(feed_desc != ""){
        $("#feedback-desc").removeClass('input-error');
        $("#feedback-alert").text("");
    } else {
        $("#feedback-desc").addClass('input-error');
        $("#feedback-alert").text("please input your feedback message!");
        return false;
    }
    if(feed_captcha != ""){
        $("#feedback-captcha").removeClass('input-error');
        $("#feedback-alert").text("");
    } else {
        $("#feedback-captcha").addClass('input-error');
        $("#feedback-alert").text("please input captcha!");
        return false;
    }
    return true;
}
</script>
<!--End of Zopim Live Chat Script-->