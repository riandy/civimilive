<?php
session_start();
header("Access-Control-Allow-Origin: *");
header('Content-type: application/json');

require_once('packages/SimpleImage.php'); // class simple image

require_once("model/Connection.php");
$obj_con = new Connection();

require_once("model/User.php");
$obj_user = new User();

require_once("model/AT_Progress.php");
$obj_atpro = new AT_Progress();

$id = $_SESSION['userData']['id'];
$auth_code = $_SESSION['userData']['auth_code'];

$ds = DIRECTORY_SEPARATOR;
//for save in database
$dir = 'image/proPhotos';
$dirThmb = 'image/thmb-proPhotos';

if (!empty($_FILES)) {
	$tempFile = $_FILES['file']['tmp_name'];

	$targetPath = $dir . $ds;
	$targetPathThmb = $dirThmb . $ds;

	$file = $_FILES['file']['name'];
	$ext = pathinfo($file, PATHINFO_EXTENSION);

	$fn = 'img-'.time().rand(0,10).'.jpg';

	$targetFile = $targetPath. $fn;
	if(move_uploaded_file($tempFile,$targetFile))
	{
	$image = new SimpleImage();
	$image->load($targetPath. $fn);
	$image->resize(200,200);
	$image->save($targetPathThmb.'thumb-'.$fn);
	}
	$array = array("img"=>"http://www.civimi.com/".$targetPath.$fn,"thumb"=>"http://www.civimi.com/".$targetPathThmb.'thumb-'.$fn);
	
	$photo = $targetPath.$fn; //real size photo
	$photoThmb = $targetPathThmb.'thumb-'.$fn; //thumbnail size photo

	//save file here
	$obj_con->up();
	$result = $obj_user->update_personal_image($id, $auth_code, $photo, $photoThmb);
	if($result){
		//save progress
		$users = $obj_user->get_data_detail($id);
		if(is_array($users)){
			$N_array = array(
		        array('module' => 'image', 'field' => $users[0]['User_proPhotoThmb']),
			    array('module' => 'profession', 'field' => $users[0]['User_interest']),
			    array('module' => 'city', 'field' => $users[0]['User_city']),
			    array('module' => 'state', 'field' => $users[0]['User_state']),
			    array('module' => 'country', 'field' => $users[0]['User_country']),
			    array('module' => 'phone', 'field' => $users[0]['User_phone']),
			    array('module' => 'website', 'field' => $users[0]['User_website'])
			);

			$progress_personal = check_progress("personal", $N_array);
			$obj_atpro->save_progress($id, "personal", $progress_personal);
			$array['personal'] = $progress_personal;
		}//end save progress
	}

	echo json_encode($array);
	$obj_con->down();
	//end save file here
}
?>