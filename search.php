<?php 
include("packages/require.php");
$curpage='search';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title><?=$global['title-website'];?></title>
    <meta name="keywords" content="">
    <meta name="description" content="">
    <?php include("packages/head.php");?>
    <link rel="stylesheet" type="text/css" href="<?=$global['absolute-url'];?>css/select2.css" />
    <script type="text/javascript" src="<?=$global['absolute-url'];?>js/select2.full.js"></script>
    <script type="text/javascript" src="<?=$global['absolute-url'];?>js/select2.js"></script>
</head>
<body>
    <div id="all">
        <!-- start top nav -->
        <?php include("section-top-nav.php");?>
        <!-- end top nav -->

        <div id="search-header-section">
            <div class="up6 visible-xs"></div>
            <div class="search-header-layer">
                <div class="row hidden-xs">
                    <div class="col-xs-12 col-hsearch">
                        <div class="hsearch-title">Location</div>
                        <select multiple="multiple" type="text" class="js-example-basic-multiple form-control hsearch-input tags-location" name="location">
                            <option value="1">jakarta</option>
                            <option value="2">bandung</option>
                            <option value="3">bogor</option>
                            <option value="4">bekasi</option>
                        </select>
                    </div>
                    <div class="col-xs-12 col-hsearch">
                        <div class="hsearch-title">Expertise</div>
                        <select multiple="multiple" type="text" class="js-example-basic-multiple form-control hsearch-input tags-expertise" name="expertise">
                            <option value="1">expertise</option>
                            <option value="2">expertise 2</option>
                            <option value="3">expertise 3</option>
                        </select>
                    </div>
                    <div class="col-xs-12 col-hsearch">
                        <div class="hsearch-title">Job Function</div>
                        <select multiple="multiple" type="text" class="js-example-basic-multiple form-control hsearch-input tags-function" name="function">
                            <option value="1">function</option>
                            <option value="2">function 2</option>
                            <option value="3">function 3</option>
                        </select>
                    </div>
                    <div class="col-xs-12 col-hsearch">
                        <div class="hsearch-title">Industry</div>
                        <select multiple="multiple" type="text" class="js-example-basic-multiple form-control hsearch-input tags-industry" name="industry">
                            <option value="1">industry</option>
                            <option value="2">industry 2</option>
                            <option value="3">industry 3</option>
                        </select>
                    </div>
                    <div class="col-xs-12 col-hsearch">
                        <div class="hsearch-title">Experience</div>
                        <select type="text" class="form-control hsearch-input" style="font-size:12px;" name="experience">
                            <option value="">all experience</option>
                        </select>
                    </div>
                </div>
                <div class="row hidden-xs">
                    <div class="col-sm-9 col-xs-12">
                        <div class="recent-list">
                            <span class="hrecent-list">Recent Search</span>
                            <a href="#">business analyst</a>
                            <a href="#">project management</a>
                            <a href="#">product owner of mobile</a>
                            <a href="#">chief technology officer</a>
                            <a href="#">senior auditor</a>
                        </div>
                    </div>
                    <div class="col-sm-3 col-xs-12">
                        <div class="search-options">
                            <span class="drop-setting">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">More Options <span class="caret"></span></a>
                                <ul class="dropdown-menu dropdown-setting" role="menu">
                                    <li><a href="#">Setting</a></li>
                                </ul>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="row visible-xs">
                    <div class="col-xs-12">
                        <div class="mobile-btn-content down3">
                            <a href="#" class="btn-mobile-search" onclick="searchSlideTag();">
                                <div class="input-mobile-search">search for job title, skills, or keywords</div>
                                <div class="icon-search"><span class="glyphicon glyphicon-search"></span></div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div><!--  end header section -->
        <div id="search-section">
            <div class="row">
                <div class="col-xs-12">
                    <div class="search-result">
                        Showing <strong>88 jobs</strong> for <strong>"product manager"</strong> in <strong>Jakarta, Bandung</strong> with <strong>"IT-Software"</strong> job function, <strong>"E-commerce"</strong> industry, <strong>"4 years"</strong> experience
                    </div>
                    <div class="drop-recent">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">Recently Added <span class="caret"></span></a>
                        <ul class="dropdown-menu dropdown-recent" role="menu">
                            <li><a href="#">Setting</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4 col-xs-12">
                    <div class="job-slist">
                        <div id="job-list">
                            <div class="job-sbox">
                                <a href="#" class="list-stitle">Product Manager,Mobile Apps</a>
                                <div class="list-scompany">Amazon,LLC</div>
                                <div class="list-saddress"><img src="<?=$global['absolute-url'];?>img/map.png" alt="map">Jakarta</div>
                                <div class="list-stime"><img src="<?=$global['absolute-url'];?>img/clock.png" alt="map">Today</div>
                                <a href="#" class="search-bookmark bookmark-active"><span class="glyphicon glyphicon-star"></span></a>
                            </div>
                            <div class="job-sbox">
                                <a href="#" class="list-stitle">Technology Product Manager</a>
                                <div class="list-scompany">PT.Global Tiket Network</div>
                                <div class="list-saddress"><img src="<?=$global['absolute-url'];?>img/map.png" alt="map">Jakarta,Bandung,Surabaya,Medan</div>
                                <div class="list-stime"><img src="<?=$global['absolute-url'];?>img/clock.png" alt="map">Today</div>
                                <a href="#" class="search-bookmark"><span class="glyphicon glyphicon-star"></span></a>
                            </div>
                            <div class="job-sbox">
                                <a href="#" class="list-stitle"><?php echo charLength('Senior Product Manager of Google Solution',30);?></a>
                                <div class="list-scompany">Google Inc</div>
                                <div class="list-saddress"><img src="<?=$global['absolute-url'];?>img/map.png" alt="map">Jakarta,Singapore</div>
                                <div class="list-stime"><img src="<?=$global['absolute-url'];?>img/clock.png" alt="map">Yesterday</div>
                                <a href="#" class="search-bookmark"><span class="glyphicon glyphicon-star"></span></a>
                            </div>
                            <div class="job-sbox">
                                <a href="#" class="list-stitle"><?php echo charLength('Marketing Product Manager, Website',30);?></a>
                                <div class="list-scompany">Amazon,LLC</div>
                                <div class="list-saddress"><img src="<?=$global['absolute-url'];?>img/map.png" alt="map">Jakarta</div>
                                <div class="list-stime"><img src="<?=$global['absolute-url'];?>img/clock.png" alt="map">Yesterday</div>
                                <a href="#" class="search-bookmark"><span class="glyphicon glyphicon-star"></span></a>
                            </div>
                            <?php for($m=1;$m<=5;$m++){?>
                            <div class="job-sbox">
                                <a href="#" class="list-stitle"><?php echo charLength('Jr. Product Manager of IOPS',30);?></a>
                                <div class="list-scompany">PT.Traveloka</div>
                                <div class="list-saddress"><img src="<?=$global['absolute-url'];?>img/map.png" alt="map">Jakarta</div>
                                <div class="list-stime"><img src="<?=$global['absolute-url'];?>img/clock.png" alt="map">3 Days ago</div>
                                <a href="#" class="search-bookmark"><span class="glyphicon glyphicon-star"></span></a>
                            </div>
                            <?php } ?>
                        </div>
                        <button id="load-latest" class="btn btn-load-more"><i class='glyphicon glyphicon-repeat'></i> Load More</button> 
                    </div>
                </div>
                <div class="col-sm-8 col-xs-12 hidden-xs">
                    <div class="search-content">
                        <div class="job-stitle">Product Manager</div>
                        <div class="job-sdesc">
                            A product manager investigates, selects, and drives the development of products for an organization, performing the activities of product management.
                            <br/><br/>
                            A product manager considers numerous factors such as intended demographic, the products offered by the competition, and how well the product fits with the company's business model. Generally, a product manager manages one or more tangible products. However, the term may be used to describe a person who manages intangible products, such as music, information, and services.
                            <br/><br/>
                            A product manager's role in tangible goods industries is similar to a program director's role in service industries.
                            <br/><br/>
                            Diverse interpretations regarding the role of the product manager are the norm. The product manager title is often used in many ways to describe drastically different duties and responsibilities. Even within the high-tech industry where product management is better defined, the product manager's job description varies widely among companies. This is due to tradition and intuitive interpretations by different individuals.
                            <br/><br/>
                            In the financial services industry (banking, insurance etc.), product managers manage products (for example, credit card portfolios), their profit and loss, and also determine the business development strategy.
                            <br/><br/>
                            In some companies, the product manager also acts as a:
                            <br/><br/>
                            Product marketing manager — may perform all outbound marketing activities in the older sense of the term<br/>
                            Project manager — may perform all activities related to schedule and resource management<br/>
                            Program manager — may perform activities related to schedule, resource, and cross-functional execution<br/>
                        </div>
                        <a href="#" class="banner-search"><img src="<?=$global['absolute-url'];?>img/banner.png" alt="banner"></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="up3"></div>
        <!-- start footer section -->
        <?php include("section-footer.php");?>
        <!-- end footer section -->
    </div><!--  end all div -->
    <!-- search modal -->
    <div id="search-modal-tag">
        <div class="ss-header">
            <div class="ss-htext">Search</div>
            <a href="javascript:;" class="ss-close" onclick="searchSlideTag();"><span class="glyphicon glyphicon-remove"></span></a>
        </div>
        <div class="ss-layer">
            <div class="row">
                <div class="col-xs-12 col-hsearch">
                    <div class="hsearch-title">Location</div>
                    <select multiple="multiple" type="text" class="js-example-basic-multiple form-control hsearch-input tags-location" name="location">
                        <option value="1">jakarta</option>
                        <option value="2">bandung</option>
                        <option value="3">bogor</option>
                        <option value="4">bekasi</option>
                    </select>
                </div>
                <div class="col-xs-12 col-hsearch">
                    <div class="hsearch-title">Expertise</div>
                    <select multiple="multiple" type="text" class="js-example-basic-multiple form-control hsearch-input tags-expertise" name="expertise">
                        <option value="1">expertise</option>
                        <option value="2">expertise 2</option>
                        <option value="3">expertise 3</option>
                    </select>
                </div>
                <div class="col-xs-12 col-hsearch">
                    <div class="hsearch-title">Job Function</div>
                    <select multiple="multiple" type="text" class="js-example-basic-multiple form-control hsearch-input tags-function" name="function">
                        <option value="1">function</option>
                        <option value="2">function 2</option>
                        <option value="3">function 3</option>
                    </select>
                </div>
                <div class="col-xs-12 col-hsearch">
                    <div class="hsearch-title">Industry</div>
                    <select multiple="multiple" type="text" class="js-example-basic-multiple form-control hsearch-input tags-industry" name="industry">
                        <option value="1">industry</option>
                        <option value="2">industry 2</option>
                        <option value="3">industry 3</option>
                    </select>
                </div>
                <div class="col-xs-12 col-hsearch">
                    <div class="hsearch-title">Experience</div>
                    <select type="text" class="form-control hsearch-input" style="font-size:12px;" name="experience">
                        <option value="">all experience</option>
                    </select>
                </div>
            </div>
            <div class="row up2">
                <div class="col-xs-12">
                    <button class="btn btn-block btn-info btn-search" style="width:100%;">Search</button>
                </div>
            </div>
        </div>
    </div><!--  end search modal -->
    <script type="text/javascript">
        function searchSlideTag() {
            $('#search-modal-tag').fadeToggle(300);
        };
        $(".tags-location").select2({
          placeholder: "all location"
      });
        $(".tags-expertise").select2({
          placeholder: "all expertise"
      });
        $(".tags-function").select2({
          placeholder: "all job function"
      });
        $(".tags-industry").select2({
          placeholder: "all industry"
      });
        $('#load-latest').click(function(){
          $('#load-latest').html("");
          $('#load-latest').html("<img src='<?php echo $global['absolute-url'];?>img/loadmores.gif' alt='loading' style='width:20px;''> Loading....");
          setTimeout(function() { 
              get_latests(); 
          }, 300)
          count++;
        });
        function get_latests(){
            $('#load-latest').html("<i class='glyphicon glyphicon-repeat'></i> Load More");              
            $('#job-list').append(replace_data(5));
        }
        function replace_data(num){
            var resultHTML='';

            for(var i=1;i <= num;i++){
                resultHTML += "<div class='job-sbox'><a href='#' class='list-stitle'>Senior Website Development</a><div class='list-scompany'>Amazon,LLC</div><div class='list-saddress'><img src='<?php echo $global['absolute-url'];?>img/map.png' alt='map'>Jakarta</div><div class='list-stime'><img src='<?php echo $global['absolute-url'];?>img/clock.png' alt='map'>Yesterday</div><a href='#' class='search-bookmark'><span class='glyphicon glyphicon-star'></span></a></div>";
            }
            return resultHTML;
        };
    </script>
</body>
</html>
