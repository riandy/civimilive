<?php 
include("packages/require.php");
include("controller/controller_global.php");
include("controller/controller_user_profile.php");
include("controller/controller_user_portfolio.php");

$curpage='user_portfolio';
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title><?=$_SESSION['userData']['username']."'s ".$data_album[0]['Album_name']." - ".$global['title-portfolio_image'];?></title>
  <meta name="keywords" content="">
  <meta name="description" content="">
  <meta name="robots" content="<?=$seo['robot_no'];?>">
  <?php include("packages/head.php");?>
  <link rel="stylesheet" href="<?php echo $global['absolute-url'];?>css/dropzone.css"/>
  <script src="<?php echo $global['absolute-url'];?>js/dropzone.js"></script>
</head>
<body>
  <div id="all">
    <!-- start top nav -->
    <?php include("section-top-nav.php");?>
    <!-- end top nav -->
    <div id="user-section" class="container-section">
     
    <div class="container">
      <div id="profile-content">
        <div class="up5 visible-xs"></div>
        <!-- start fixed mobile nav -->
        <?php $header_content = "Portfolio";  include("user_part-mobile-nav.php");?>
        <!-- end fixed mobile nav-->
        <div class="row">
            <div class="up5 visible-xs"></div>
          <!-- profile sidebar -->
          <div class="col-sm-3 col-xs-12">
            <!-- start sidebar -->
            <?php include("user_part-sidebar.php");?>
            <!-- end sidebar -->
          </div><!-- profile sidebar -->
          <div class="col-sm-9 col-xs-12 xs-pad0">
              <div id="personal-portfolio-section" class="job-sbox">
                <div id="education-header" class="user-section-header row border-bottom-eee">
                  <div class="col-xs-10">
                    <div class="profile-title hidden-xs" style="font-size:14px;"><i class="glyphicon glyphicon-picture"></i>&nbsp;&nbsp;
                      <a href="<?=$path['user-portfolio'];?>" class="module-breadcrumb">Album</a>&nbsp;&gt;&nbsp;
                      <span id="album-nav" class="module-breadcrumb"><?=$data_album[0]['Album_name'];?></span>
                    </div>
                    <div class="profile-title visible-xs" style="font-size:14px;">
                      <a href="<?=$path['user-portfolio'];?>" class="module-breadcrumb"><strong>&lt;&nbsp;&nbsp;</strong>
                      <i class="glyphicon glyphicon-picture"></i>&nbsp;
                      <span id="album-nav" class="module-breadcrumb"><?=charLength($data_album[0]['Album_name'],12);?></span>
                      </a>
                    </div>
                  </div>
                  <div class="col-xs-2">
                      <div class="edit-button text-right">
                        <a href="javascript:;" class="edit-portfolio" title="show edit album" data-toggle='modal' data-target='#modal-portfolio'><i class="glyphicon glyphicon-pencil"></i></a>
                      </div>
                  </div>
                </div>
                <!-- <div class="add-photo-bar">
                  <a href="#modal-photo" class="add-photo-portfolio" data-toggle='modal' data-target='#modal-photo'><i class="glyphicon glyphicon-camera"></i> Add Photo</a>
                </div> -->
                <div id="portfolio-profile">
                  <div id="photo-list" class="row">
                  </div>
                  <div class="row">
                    <div class="col-xs-12"><div id="load-album" class="text-center"></div></div>
                  </div>
                </div>
              </div>
          </div>
      </div>
    </div>
            
    </div>
    <!-- end center content -->
  </div>
    <!-- start footer section -->
    <?php include("section-footer.php");?>
    <!-- end footer section -->
  </div><!--  end all div -->
  <!-- Modal Edit Portfolio -->
  <div class="modal fade" id="modal-portfolio" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content" style="border-radius:0;">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="font-size: 35px;position: absolute;right: 10px;top: 3px;"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel" style="text-align: center;font-size: 20px;font-weight: bold;color: #717171;">Edit Album</h4>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-sm-4 col-xs-12">
              <div class="portfolio-label bold">Title *</div>
            </div>
            <div class="col-sm-8 col-xs-12">
              <input id="album-title" type="text" class="form-control no-radius" placeholder="name of the album" value="<?=$data_album[0]['Album_name'];?>">
            </div>
          </div>
          <div class="row up1">
            <div class="col-sm-4 col-xs-12">
              <div class="portfolio-label">Description</div>
            </div>
            <div class="col-sm-8 col-xs-12">
              <textarea  id="album-desc" class="form-control" rows="3" placeholder="tell story about this album"><?=$data_album[0]['Album_desc'];?></textarea>
            </div>
          </div>
        </div>
        <div class="modal-footer" style="text-align:left;">
          <input id="album-id" type="hidden" value="<?=$data_album[0]['Album_ID'];?>">
          <div class="btn-group">
            <button id="save-album" type="button" class="btn btn-primary btn-sphoto">Save</button>
            <button id="cancel-album" type="button" class="btn btn-default btn-sphoto" data-dismiss="modal">Cancel</button>
          </div>
          <div id="mload-portfolio" class="edit-state hide">
            <img src="<?=$global['absolute-url'];?>img/loadmores.gif" alt="loading">
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- Modal Add Photo -->
  <div class="modal fade" id="modal-photo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static">
    <div class="modal-dialog" role="document">
      <div class="modal-content" style="border-radius:0;">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="font-size: 35px;position: absolute;right: 10px;top: 3px;"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel" style="text-align: center;font-size: 20px;font-weight: bold;color: #717171;">Add New Photo</h4>
        </div>
        <div class="modal-body pad0">
          <form action="<?=$global['absolute-url'];?>uploadPortfolio.php?albumID=<?=$data_album[0]['Album_ID'];?>" method="GET" class="uploadform dropzone-portfolio dropzone no-margin dz-clickable">
            
          </form>
        </div>
      </div>
    </div>
  </div>
  <!-- Modal Edit Photo -->
  <div class="modal fade pad0" id="modal-photoEdit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-dialog-photo" role="document">
      <div class="modal-content" style="border-radius:0;">
        <div class="modal-body pad0">
          <div class="row">
            <div class="col-xs-12 pad0">
              <div class="text-center" style="position:relative;">
                <img id="photo-editor" src="" alt="" class="photo-editor">
                <span id="label-primary" class="label label-info label-primary-photo">Primary Photo</span>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-xs-12 pad0">
              <input id="photo-title" type="text" class="form-control no-radius" placeholder="describe your image">
            </div>
          </div>
              <button id="photo-primary" onclick="update_primary();" type="button" class="btn btn-primary btn-xs" style="margin: 5px 15px;">Set as primary</button>
              <div id="load-epditing" class="edit-state hide">
                <img src="<?=$global['absolute-url'];?>img/loadmores.gif" alt="loading">
              </div>
        </div>
        <div class="modal-footer" style="text-align:left;">
          <input id="photo-id" type="hidden">
          <div class="btn-group">
            <button id="save-photo" type="button" class="btn btn-success btn-ephoto"><i class="visible-xs glyphicon glyphicon-ok"></i><span class="hidden-xs">Save</span></button>
            <button id="cancel-photo" type="button" class="btn btn-default btn-ephoto" data-dismiss="modal"><i class="visible-xs glyphicon glyphicon-remove"></i><span class="hidden-xs">Cancel</span></button>
            <button id="delete-photo" type="button" class="btn btn-danger btn-ephoto"><i class="visible-xs glyphicon glyphicon-trash"></i><span class="hidden-xs">Delete</span></button>
          </div>
          <div id="load-editing" class="edit-state hide">
            <img src="<?=$global['absolute-url'];?>img/loadmores.gif" alt="loading">
          </div>
        </div>
      </div>
    </div>
  </div>
  <script src="<?php echo $global['absolute-url'];?>js/function.js"></script>
  <script type="text/javascript">
  $(document).ready(function(){
  Dropzone.autoDiscover = false; // keep this line if you have multiple dropzones in the same page
  $(".uploadform").dropzone({
  acceptedFiles: "image/jpeg,image/png,image/gif",
  // url: "uploadPortfolio.php",
  maxFiles: 6, // Number of files at a time
  maxFilesize: 10, //in MB
  error: function(file){
  alert('Your uploaded file is not supported!');
  },
  maxfilesexceeded: function(file)
  {
  alert('You have uploaded more than 6 Image. Only the six file will be uploaded!');
  },
  // processing: function () {
  // $('#load-profile').removeClass('hide');//on process show loading progress
  // },
  success: function (response) {
  var x = JSON.parse(response.xhr.responseText);
  console.log('Image -> '+x.img+', Thumb -> '+x.thumb); // Just to return the JSON to the console.
  },
  queuecomplete: function(response) {
  $('#modal-photo').modal('hide');
  this.removeAllFiles(); // This removes all files after upload to reset dropzone for next upload
  $('#photo-list').html('');
  $('#load-album').html("<img src='<?=$global['absolute-url'];?>img/loader.gif' style='width: 200px;' />");
  get_image();
  },
  addRemoveLinks: true,
  removedfile: function(file) {
  var _ref; // Remove file on clicking the 'Remove file' button
  return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;
  }
  });
  });
  get_image();
  function get_image(){
    var url = "<?=$api['photo-data'];?>&user_id=<?php echo $_SESSION['userData']['id'];?>&auth_code=<?php echo $_SESSION['userData']['auth_code'];?>&album_id=<?=$data_album[0]['Album_ID'];?>";                            
    $.ajax({url: url,success:function(result){
    $('#photo-list').html(replace_image(result.data));
    $('#load-album').html('');
    }});
  }
  function replace_image(datas){
    var resultHTML = '';
resultHTML += "<div class='col-sm-3 col-xs-4 col-photo-potfolio'><div class='portfolio-content'><a href='#modal-photo' class='add-photo-portfolio' data-toggle='modal' data-target='#modal-photo'><img class='portfolio-img' src='<?=$global['absolute-url'];?>img/add-photo.png' title='add image' alt='add image' /></a></div></div>";
    
    if(datas != null){
    var obj = datas;    
    for(var i=0;i < obj.length;i++){
    resultHTML += "<div id='col-pthmb-"+obj[i].Photo_ID+"' class='col-sm-3 col-xs-4 col-photo-potfolio'>";
    resultHTML += "<div class='portfolio-content'>";
    resultHTML += "<a href='#modal-photoEdit' onclick='openPhoto("+obj[i].Photo_ID+");' data-toggle='modal' data-target='#modal-photoEdit' class='portfolio-link' title='"+obj[i].Photo_title+"'>";
    if(obj[i].Photo_primary == 'yes'){
    resultHTML += "<span id='label-primary' class='label label-info label-primary-photo' style='display: block;z-index:5;'><span class='hidden-xs'>Primary</span><span class='visible-xs'>P</span></span>";
    }
    resultHTML += "<div id='pthmb-"+obj[i].Photo_ID+"' data-pp_title='"+obj[i].Photo_title+"' data-pp_primary='"+obj[i].Photo_primary+"' data-pp_img='<?=$global['img-url'];?>"+obj[i].Photo_imgLink+"' class='portfolio-thmb'>";
    resultHTML += "<img src='<?=$global['img-url'];?>"+obj[i].Photo_ThmbImgLink+"' alt='"+obj[i].Photo_title+"' class='portfolio-img'></div>";
    resultHTML += "</a>";
    resultHTML += "</div>";
    resultHTML += "</div>";
 
    } 
  } else {
    //resultHTML += "<div id='empty-photo' class='empty-file col-xs-12' style='padding:0 20px 20px 20px;'>insert your photo</div>";
  }
    return resultHTML;
  }
  $('#save-album').on('click', function(){
    update_album();
  })
  $('#save-photo').on('click', function(){
    update_photo();
  })
  $('#delete-photo').on('click', function(){
    delete_photo();
  })
  function openPhoto(param){
    var image = $('#pthmb-'+param).data('pp_img');
    var title = $('#pthmb-'+param).data('pp_title');
    var primary = $('#pthmb-'+param).data('pp_primary');

    if(primary == "yes") {
      $("#photo-primary").addClass("hide");
      $("#label-primary").show();
    } else {
      $("#label-primary").hide();
      $("#photo-primary").removeClass("hide");
    }
    $("#photo-title").val(title);
    $("#photo-editor").attr('src',image);
    $("#photo-editor").attr('alt',title);
    $("#photo-id").val(param);
  }
  // update function photo
  function update_photo(){

    var url = "<?=$api['photo-update'];?>";
    var photo_title = $("#photo-title").val();
    var photo_id = $("#photo-id").val();

    $(".btn-ephoto").addClass("disabled");
    $("#load-editing").removeClass("hide");
    var data = {
      user_id : "<?=$_SESSION['userData']['id'];?>",
      auth_code : "<?=$_SESSION['userData']['auth_code'];?>",
      id : photo_id,
      title : photo_title
    }
    $.ajax({url: url,data : data, success:function(result){
      console.log(result);
      setTimeout(function() {
          get_image();
          $('#modal-photoEdit').modal('hide');
          $(".btn-ephoto").removeClass("disabled");
          $("#load-editing").addClass("hide");
      }, 1500)
    }});
  }
  // update function primary
  function update_primary(){

    var url = "<?=$api['photo-primary'];?>";
    var photo_id = $("#photo-id").val();

    $("#photo-primary").addClass("disabled");
    $("#load-epditing").removeClass("hide");
    var data = {
      user_id : "<?=$_SESSION['userData']['id'];?>",
      auth_code : "<?=$_SESSION['userData']['auth_code'];?>",
      album_id : "<?=$data_album[0]['Album_ID'];?>",
      primary : "yes",
      id : photo_id
    }
    $.ajax({url: url,data : data, success:function(result){
      console.log(result);
      setTimeout(function() {
      get_image();
      $('#modal-photoEdit').modal('hide');
      $("#photo-primary").removeClass("disabled");
      $("#load-epditing").addClass("hide");
      }, 1500)
    }});
  }
  //delete function photo
  function delete_photo(){
    if (confirm("Are you sure to delete this photo ?")) {
      var url = "<?=$api['photo-delete'];?>";
      var photo_id = $("#photo-id").val();
      $(".btn-ephoto").addClass("disabled");
      $("#load-editing").removeClass("hide");
      var data = {
        user_id : "<?=$_SESSION['userData']['id'];?>",
        auth_code : "<?=$_SESSION['userData']['auth_code'];?>",
        id : photo_id
      }
      $.ajax({url: url,data : data, success:function(result){
        console.log(result);
        setTimeout(function() {
          $("#col-pthmb-"+photo_id).remove();
          $('#modal-photoEdit').modal('hide');
          $(".btn-ephoto").removeClass("disabled");
          $("#load-editing").addClass("hide");
        }, 1500)
      }});
    }
  }
  // update function album
  function update_album(){

    var url = "<?=$api['album-update'];?>";
    var album_title = $("#album-title").val();
    var album_desc = $("#album-desc").val();
    var album_id = $("#album-id").val();
    $(".btn-sphoto").addClass("disabled");
    $("#mload-portfolio").removeClass("hide");

    var data = {
      user_id : "<?=$_SESSION['userData']['id'];?>",
      auth_code : "<?=$_SESSION['userData']['auth_code'];?>",
      id : album_id,
      name : album_title,
      desc : album_desc
    }
    $.ajax({url: url,data : data, success:function(result){
      console.log(result);
      setTimeout(function() {
        $('#modal-portfolio').modal('hide');
        $(".btn-sphoto").removeClass("disabled");
        $("#mload-portfolio").addClass("hide");
      }, 1500)
      $("#album-title").val(album_title);
      $("#album-desc").val(album_desc);
      $("#album-nav").text(album_title);
    }});
  }
  function profileNav(){
    $('#profile-arrow').toggleClass('nav-close');
  }
  </script>
</body>
</html>
