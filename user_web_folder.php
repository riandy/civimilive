<?php 
include("packages/require.php");
include("controller/controller_global.php");
include("controller/controller_user_web.php");

$curpage='user_web';
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title><?=$_SESSION['userData']['username']."'s ".$data_web[0]['WebCat_name']." - ".$global['title-portfolio_web'];?></title>
  <meta name="keywords" content="">
  <meta name="description" content="">
  <meta name="robots" content="<?=$seo['robot_no'];?>">
  <?php include("packages/head.php");?>
  <link rel="stylesheet" href="<?php echo $global['absolute-url'];?>css/dropzone.css"/>
  <script src="<?php echo $global['absolute-url'];?>js/dropzone.js"></script>
</head>
<body>
  <div id="all">
    <!-- start top nav -->
    <?php include("section-top-nav.php");?>
    <!-- end top nav -->
    <div id="user-section" class="container-section">
    
    <div class="container">
      <div id="profile-content">
        <div class="up5 visible-xs"></div>
        <!-- start fixed mobile nav -->
        <?php $header_content = "Web";  include("user_part-mobile-nav.php");?>
        <!-- end fixed mobile nav-->
        <div class="row">
        <div class="up5 visible-xs"></div>
          <!-- profile sidebar -->
          <div class="col-sm-3 col-xs-12">
            <!-- start sidebar -->
            <?php include("user_part-sidebar.php");?>
            <!-- end sidebar -->
          </div><!-- profile sidebar -->
          <div class="col-sm-9 col-xs-12 xs-pad0">
              <div id="personal-portfolio-section" class="job-sbox">
                <div id="education-header" class="user-section-header row border-bottom-eee">
                  <div class="col-xs-8">
                    <div class="profile-title hidden-xs" style="font-size:14px;"><i class="glyphicon glyphicon-globe"></i>&nbsp;&nbsp;
                      <a href="<?=$path['user-web'];?>" class="module-breadcrumb">Web</a> >
                      <span id="folder-nav" class="module-breadcrumb"> <?=$data_web[0]['WebCat_name'];?></span>
                    </div>
                    <div class="profile-title visible-xs" style="font-size:14px;"><i class="glyphicon glyphicon-globe"></i>&nbsp;&nbsp;
                      <a id="folder-back" href="<?=$path['user-web'];?>" class="module-breadcrumb">< Back</a>
                    </div>
                  </div>
                  <div class="col-xs-4">
                      <div class="edit-button text-right">
                        <a href="javascript:;" class="edit-portfolio" title="show edit folder" data-toggle='modal' data-target='#modal-folder'><i class="glyphicon glyphicon-pencil"></i></a>
                      </div>
                  </div>
                </div>
                <!-- <div class="add-photo-bar">
                  <a href="#modal-web" class="add-photo-portfolio" data-toggle='modal' data-target='#modal-web'><i class="glyphicon glyphicon-globe"></i> Add Web</a>
                </div> -->
                <div id="portfolio-profile">
                  <div id="web-list" class="row">
                  </div>
                </div>
              </div>
          </div>
      </div>
    </div>
            
    </div>
    <!-- end center content -->
  </div>
    <!-- start footer section -->
    <?php include("section-footer.php");?>
    <!-- end footer section -->
  </div><!--  end all div -->
  <!-- Modal Edit Folder -->
  <div class="modal fade" id="modal-folder" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content" style="border-radius:0;">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="font-size: 35px;position: absolute;right: 10px;top: 3px;"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel" style="text-align: center;font-size: 20px;font-weight: bold;color: #717171;">Edit Album</h4>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-sm-4 col-xs-12">
              <div class="portfolio-label bold">Title *</div>
            </div>
            <div class="col-sm-8 col-xs-12">
              <input id="folder-title" type="text" class="form-control no-radius" placeholder="name of the web" value="<?=$data_web[0]['WebCat_name'];?>">
            </div>
          </div>
          <div class="row up1">
            <div class="col-sm-4 col-xs-12">
              <div class="portfolio-label">Description</div>
            </div>
            <div class="col-sm-8 col-xs-12">
              <textarea  id="folder-desc" class="form-control" rows="3" placeholder="tell story about this web"><?=$data_web[0]['WebCat_desc'];?></textarea>
            </div>
          </div>
        </div>
        <div class="modal-footer" style="text-align:left;">
          <input id="folder-id" type="hidden" value="<?=$data_web[0]['WebCat_ID'];?>">
          <div class="btn-group">
            <button id="save-folder" type="button" class="btn btn-primary btn-sfolder">Save</button>
            <button id="cancel-web" type="button" class="btn btn-default btn-sfolder" data-dismiss="modal">Cancel</button>
          </div>
          <div id="mload-folder" class="edit-state hide">
            <img src="<?=$global['absolute-url'];?>img/loadmores.gif" alt="loading">
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- Modal Add Web -->
  <div class="modal fade" id="modal-web" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content" style="border-radius:0;">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="font-size: 35px;position: absolute;right: 10px;top: 3px;"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel" style="text-align: center;font-size: 20px;font-weight: bold;color: #717171;">Add New Web</h4>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-sm-4 col-xs-12">
              <div class="portfolio-label bold">Web Folder *</div>
            </div>
            <div class="col-sm-8 col-xs-12">
              <select id="web-folder" class="form-control no-radius">
              </select>
            </div>
          </div>
          <div class="row up1">
            <div class="col-sm-4 col-xs-12">
              <div class="portfolio-label bold">Web Name *</div>
            </div>
            <div class="col-sm-8 col-xs-12">
              <input id="web-title" type="text" class="form-control no-radius" placeholder="name of the web">
            </div>
          </div>
          <div class="row up1">
            <div class="col-sm-4 col-xs-12">
              <div class="portfolio-label">Description</div>
            </div>
            <div class="col-sm-8 col-xs-12">
              <textarea  id="web-desc" class="form-control no-radius" rows="3" placeholder="tell story about this web"></textarea>
            </div>
          </div>
          <div class="row up1">
            <div class="col-sm-4 col-xs-12">
              <div class="portfolio-label bold">Web Link *</div>
            </div>
            <div class="col-sm-8 col-xs-12">
              <input id="web-link" type="text" class="form-control no-radius" placeholder="i.e. http://www.civimi.com">
              <div class="video-notice">*Paste your online web links here, i.e. http://www.civimi.com</div>
            </div>
          </div>
        </div>
        <div class="modal-footer" style="text-align:left;">
          <div class="btn-group">
            <button id="save-web" type="button" class="btn btn-success btn-web">Save</button>
            <button id="cancel-web" type="button" class="btn btn-default btn-web" data-dismiss="modal">Cancel</button>
          </div>
          <div id="load-web" class="edit-state hide">
            <img src="<?=$global['absolute-url'];?>img/loadmores.gif" alt="loading">
          </div>
      </div>
    </div>
  </div>
  <script src="<?php echo $global['absolute-url'];?>js/function.js"></script>
  <script type="text/javascript">
  function get_data_category(){
    var cat = $('#web-folder option:selected').text();
    var desc = $('#web-folder option:selected').data('desc');
    $('#folder-nav').text(cat);
    $('#folder-title').val(cat);
    $('#folder-desc').val(desc);
  }
  get_folder();
  function get_folder(){
    var url = "<?=$api['webcat-data'];?>&user_id=<?php echo $_SESSION['userData']['id'];?>&auth_code=<?php echo $_SESSION['userData']['auth_code'];?>";                            
    $.ajax({url: url,success:function(result){
    $('#web-folder').html(replace_folder(result.data));
    }});
  }
  function replace_folder(datas){
    var folder_id = $("#folder-id").val();
    var resultHTML='';
        resultHTML += "<option value=''>Choose folder</option>";
      if(datas != null){
        var obj = datas;    
        for(var i=0;i < obj.length;i++){
          if(folder_id == obj[i].WebCat_ID){
            var selected = "selected=selected";
          } else {
            var selected = "";
          }
          resultHTML += "<option value='"+obj[i].WebCat_ID+"' "+selected+" data-desc='"+obj[i].WebCat_desc+"'>"+obj[i].WebCat_name+"</option>";
        }
      }
    return resultHTML;
  }
  get_web();
  function get_web(){
    var folder_id = $("#folder-id").val();
    var url = "<?=$api['web-data'];?>&user_id=<?php echo $_SESSION['userData']['id'];?>&auth_code=<?php echo $_SESSION['userData']['auth_code'];?>&webcat_id="+folder_id;                            
    $.ajax({url: url,success:function(result){
    $('#web-list').html(replace_web(result.data));
    }});
  }
  function replace_web(datas){
    var resultHTML = '';
    resultHTML += "<div class='col-sm-3 col-xs-6 col-potfolio'>";
    resultHTML += "<div class='portfolio-content down2'>";
    resultHTML += "<a href='#modal-web' class='add-photo-portfolio' data-toggle='modal' data-target='#modal-web'>";
    resultHTML += "<img class='portfolio-img' src='<?=$global['absolute-url'];?>img/add-web.png' title='add web' alt='add web' />";
    resultHTML += "<div class='portfolio-name'>Add Web</div>";
    resultHTML += "</a>";
    resultHTML += "</div>";
    resultHTML += "</div>";
    if(datas != null){

    var obj = datas;    
    for(var i=0;i < obj.length;i++){
    var name = obj[i].Web_title;
    var regex = /\\/g;
    var replaceName = name.replace(regex, "").replace(/&quot;/g, "");
    var lowercase = replaceName.toLowerCase();
    var encodeName = lowercase.replace(/\s+/g, '-');
    resultHTML += "<div id='col-vthmb-"+obj[i].Web_ID+"' class='col-sm-3 col-xs-6 col-potfolio'>";
    resultHTML += "<div class='portfolio-content'>";
    resultHTML += "<a href='<?=$path['user-web-detail'];?>"+obj[i].Web_ID+"_"+encodeName+".html' class='portfolio-link' title='"+obj[i].Web_title+"'>";
    resultHTML += "<div class='portfolio-thmb'>";
    resultHTML += "<img src='<?=$global['absolute-url'];?>img/placeholder-web.png' alt='"+obj[i].Web_title+"' class='portfolio-img'></div>";
    resultHTML += "<div class='portfolio-name hidden-xs hidden-sm'>"+charString(obj[i].Web_title,12)+"</div>";
    resultHTML += "<div class='portfolio-name visible-xs visible-sm'>"+charString(obj[i].Web_title,5)+"</div>";
    resultHTML += "</a>";
    resultHTML += "</div>";
    resultHTML += "</div>";
 
    } 
  }

    return resultHTML;
  }
  $('#save-folder').on('click', function(){
    update_folder();
  })
  $('#save-web').on('click', function(){
    save_web();
  })
  // update function web
  function save_web(){

    var url = "<?=$api['web-insert'];?>";
    var web_title = $("#web-title").val();
    var web_desc = $("#web-desc").val();
    var web_link = $("#web-link").val();
    var web_catid = $("#web-folder").val();
    var cat = $('#web-folder option:selected').text();
    var name = cat;
    var regex = /\\/g;
    var replaceName = name.replace(regex, "").replace(/&quot;/g, "");
    var lowercase = replaceName.toLowerCase();
    var encodeName = lowercase.replace(/\s+/g, '-');
    
    $(".btn-web").addClass("disabled");
    $("#load-web").removeClass("hide");
    var data = {
      user_id : "<?=$_SESSION['userData']['id'];?>",
      auth_code : "<?=$_SESSION['userData']['auth_code'];?>",
      webcat_id : web_catid,
      title : web_title,
      desc : web_desc,
      link : web_link
    }
    $.ajax({url: url,data : data, success:function(result){
      console.log(result);
      setTimeout(function() {
          $("#folder-id").val(web_catid);
          $('#modal-web').modal('hide');
          $(".btn-web").removeClass("disabled");
          $("#load-web").addClass("hide");
          $("#web-title").val("");
          $("#web-desc").val("");
          $("#web-link").val("");
          get_web();
          get_data_category();
          get_folder();
          var newlinks = "<?=$path['user-web-folder'];?>"+web_catid+"_"+encodeName+".html";
          window.history.pushState('civimi', 'civimi', newlinks);
      }, 1500)
    }});
  }
  // update function folder
  function update_folder(){

    var url = "<?=$api['webcat-update'];?>";
    var folder_title = $("#folder-title").val();
    var folder_desc = $("#folder-desc").val();
    var folder_id = $("#folder-id").val();
    $(".btn-sfolder").addClass("disabled");
    $("#mload-folder").removeClass("hide");

    var data = {
      user_id : "<?=$_SESSION['userData']['id'];?>",
      auth_code : "<?=$_SESSION['userData']['auth_code'];?>",
      id : folder_id,
      name : folder_title,
      desc : folder_desc
    }
    $.ajax({url: url,data : data, success:function(result){
      console.log(result);
      setTimeout(function() {
        $('#modal-folder').modal('hide');
        $(".btn-sfolder").removeClass("disabled");
        $("#mload-folder").addClass("hide");
      }, 1500)
      $("#folder-title").val(folder_title);
      $("#folder-desc").val(folder_desc);
      $("#folder-nav").text(folder_title);
    }});
  }
  function profileNav(){
    $('#profile-arrow').toggleClass('nav-close');
  }
  </script>
</body>
</html>
