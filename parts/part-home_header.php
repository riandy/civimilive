<div class="section-header">
    <div class="bg-hand"></div>
    <div class="container container-max">
        <div class="index-top-nav">
            <a href="<?=$global['absolute-url'];?>" class="logo-desktop">
                <img src="<?=$global['logo-desktop'];?>" alt="logo">
                <span>your anywhere CV & portfolio</span>
            </a>
            <?php if(isset($_SESSION['userData']['id']) && isset($_SESSION['userData']['username']) && isset($_SESSION['userData']['email']) && isset($_SESSION['userData']['auth_code'])){ ?>
            <div class="login-nav dropdown">
                <a href="#" class="nav-user hidden-xs" data-target="dropdown-user" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
                    <i class="glyphicon glyphicon-user"></i> <?=$lang['hello-teks'].$_SESSION['userData']['username'];?> <span class="glyphicon glyphicon-chevron-down"></span>
                </a>
                <ul id="dropdown-user" class="dropdown-menu dropdown-usernav" role="menu">
                    <span class="caret-user"></span>
                    <li class="dropdown-head"><span><?=$lang['dropdown-edit'];?></span></li>
                    <li id="user-nav-1"><a href="<?=$path['user-edit'];?>"><?=$lang['dropdown-profile'];?></a></li>
                    <li><a href="<?=$path['user-portfolio'];?>"><?=$lang['dropdown-portfolio'];?></a></li>
                    <li class="dropdown-head"><span><?=$lang['dropdown-view'];?></span></li>
                    <li><a href="<?=$path['user-page_profile'].$_SESSION['userData']['username'].".cvm";?>" target="_blank"><?=$lang['dropdown-my-profile'];?></a></li>
                    <li><a href="<?=$path['user-resume'].$_SESSION['userData']['username'].".cvm";?>" target="_blank"><?=$lang['dropdown-my-resume'];?></a></li>
                    <li><a href="<?=$path['user-portfolio_image'].$_SESSION['userData']['username'].".cvm";?>" target="_blank"><?=$lang['dropdown-my-portfolio'];?></a></li>
                    <li class="dropdown-bottom"><a href="<?=$path['user-resume-pdf'].$_SESSION['userData']['username'].".pdf";?>" target="_blank"><?=$lang['btn-pdf'];?></a></li>
                    <li class="dropdown-bottom"><a href="<?=$path['user-logout'];?>"><?=$lang['dropdown-logout'];?></a></li>
                </ul>
            </div>
            <?php } else { ?>
            <div class="login-nav">
                <a href="<?=$path['login'];?>" class="hidden-xs">Login</a>
                <span class="hidden-xs"> / </span>
                <a href="<?=$path['register'];?>" class="hidden-xs">Sign up</a>
                <a href="<?=$path['login'];?>" class="visible-xs"><i class="glyphicon glyphicon-user"></i></a>
            </div>
            <?php } ?>
            <div class="select-sub hidden-xs">
                <select id="select-lang" onchange="changeLang();" name="lang" class="form-control input-lang">
                    <option value="en" <?php if($_sub == 'EN'){ ?>selected<?php } ?>>English</option>
                    <option value="id" <?php if($_sub == 'ID'){ ?>selected<?php } ?>>Indonesia</option>
                </select>
            </div>
            <div class="select-sub visible-xs up1">
                <select id="mob-lang" onchange="changeMobLang();" name="lang" class="form-control input-lang">
                    <option value="en" <?php if($_sub == 'EN'){ ?>selected<?php } ?>>En</option>
                    <option value="id" <?php if($_sub == 'ID'){ ?>selected<?php } ?>>Id</option>
                </select>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-7 col-xs-12 pad0">
                <div class="header-content">
                    <div class="header-text"><?=$lang['home-head'];?></div>
                    <div class="header-text-note"><?=$lang['home-note'];?></div>
                    <?php if(isset($_SESSION['userData']['id']) && isset($_SESSION['userData']['username']) && isset($_SESSION['userData']['email']) && isset($_SESSION['userData']['auth_code'])){ ?>
                    <div class="header-started">
                        <a href="<?=$path['user-page_profile'].$_SESSION['userData']['username'].".cvm";?>" class="hidden-xs"><?=$lang['link-profile'];?></a>
                        <span><?=$lang['free-text'];?></span>
                    </div>
                    <?php } else { ?>
                    <div class="header-started">
                        <a href="<?=$path['login'];?>" class="hidden-xs"><?=$lang['btn-started'];?></a>
                        <span><?=$lang['free-text'];?></span>
                    </div>
                    <?php } ?>
                    <?php if(is_array($data_tips)){ ?>
                    <div class="header-tips hidden-xs"><?=$lang['home-tips'];?></div>
                    <div class="header-tips-list hidden-xs">
                        <?php foreach ($data_tips as $data_tip) { ?>
                        <a href="<?=$path['tips'].encode($data_tip['News_title']).'_'.$data_tip['News_ID'].'.html';?>"><?=charLength(correctDisplay($data_tip['News_title']),50);?></a>
                        <?php } ?>
                        <a href="<?=$path['tips'];?>"><?=$lang['more-text'];?></a>
                    </div>
                    <?php } ?>
                </div>
            </div>
            <div class="col-sm-5 col-xs-12 pad0 col-right-header">
                <!-- <div class="bg-header">
                    <div class="screen-pos">
                        <div id="slide-screen" class="swiper-container">
                            <div class="swiper-wrapper">
                                <?php for($s=1;$s<=4;$s++){?>
                                <div class="swiper-slide">
                                    <div class="screen-img">
                                        <img src="<?=$global['img-url'];?>img/screen-<?=$s;?>.png" alt="image">
                                    </div>
                                </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div> -->
            </div>
        </div>
    </div>
    <div class="header-menu hidden-xs">
        <div class="container container-max">
            <ul class="hmenu-list">
                <li>
                    <a href="<?=$path['home'];?>" class="hmenu-link hmenu-active">
                        <?php if(isset($curpage)){ if($curpage == "home"){ ?>
                        <img src="<?=$global['absolute-url'];?>img/arrow.png" alt="image">
                        <?php } } ?>
                        Overview & Features
                    </a>
                </li>
                <li>
                    <a href="<?=$path['testimoni'];?>" class="hmenu-link">
                        <?php if(isset($curpage)){ if($curpage == "testimoni"){ ?>
                        <img src="<?=$global['absolute-url'];?>img/arrow.png" alt="image">
                        <?php } } ?>
                        Testimonial
                    </a>
                </li>
                <li>
                    <a href="<?=$path['tips'];?>" class="hmenu-link">
                        <?php if(isset($curpage)){ if($curpage == "tips"){ ?>
                        <img src="<?=$global['absolute-url'];?>img/arrow.png" alt="image">
                        <?php } } ?>
                        Career Tips & Blogs
                    </a>
                </li>
            </ul>
        </div>
    </div>
    <div class="header-menu-xs visible-xs">
        <ul id="bar-menu" class="hmenu-list-xs">
            <li><a href="<?=$path['home'];?>" class="hmenu-link-xs">Overview & Features</a></li>
            <li><a href="<?=$path['testimoni'];?>" class="hmenu-link-xs">Testimonial</a></li>
            <li><a href="<?=$path['tips'];?>" class="hmenu-link-xs">Career Tips & Blogs</a></li>
        </ul>
        
        <div class="hmenu-bar-xs">
            <a href="javascript:void(0)" id="bar-toggle" class="hmenu-toggle in"><img id="bar-img" src="<?=$global['img-url'];?>img/bar-icon.png" alt="icon"></a>
            <?php if(isset($_SESSION['userData']['id']) && isset($_SESSION['userData']['username']) && isset($_SESSION['userData']['email']) && isset($_SESSION['userData']['auth_code'])){ ?>
            <a href="<?=$path['user-edit'];?>" class="user-link-xs"><?=$_SESSION['userData']['username'];?> <i class="glyphicon glyphicon-user"></i></a>
            <?php } else { ?>
            <a href="<?=$path['login'];?>" class="hmenu-started"><?=$lang['btn-started'];?></a>
            <?php } ?>
        </div>
    </div>
</div>