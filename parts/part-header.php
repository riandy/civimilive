<div class="header-civ">
    <div class="container container-max">
        <div class="index-top-nav hidden-xs">
            <a href="<?=$global['absolute-url'];?>" class="logo-desktop">
                <img src="<?=$global['logo-desktop'];?>" alt="logo">
                <span>your anywhere CV & portfolio</span>
            </a>
            <?php if(isset($_SESSION['userData']['id']) && isset($_SESSION['userData']['username']) && isset($_SESSION['userData']['email']) && isset($_SESSION['userData']['auth_code'])){ ?>
            <div class="login-nav dropdown hidden-xs">
                <a href="#" class="nav-user" data-target="dropdown-user" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
                    <i class="glyphicon glyphicon-user"></i> <?=$lang['hello-teks'].$_SESSION['userData']['username'];?> <span class="glyphicon glyphicon-chevron-down"></span>
                </a>
                <ul id="dropdown-user" class="dropdown-menu dropdown-usernav" role="menu">
                    <span class="caret-user"></span>
                    <li class="dropdown-head"><span><?=$lang['dropdown-edit'];?></span></li>
                    <li id="user-nav-1"><a href="<?=$path['user-edit'];?>"><?=$lang['dropdown-profile'];?></a></li>
                    <li><a href="<?=$path['user-portfolio'];?>"><?=$lang['dropdown-portfolio'];?></a></li>
                    <li class="dropdown-head"><span><?=$lang['dropdown-view'];?></span></li>
                    <li><a href="<?=$path['user-page_profile'].$_SESSION['userData']['username'].".cvm";?>" target="_blank"><?=$lang['dropdown-my-profile'];?></a></li>
                    <li><a href="<?=$path['user-resume'].$_SESSION['userData']['username'].".cvm";?>" target="_blank"><?=$lang['dropdown-my-resume'];?></a></li>
                    <li><a href="<?=$path['user-portfolio_image'].$_SESSION['userData']['username'].".cvm";?>" target="_blank"><?=$lang['dropdown-my-portfolio'];?></a></li>
                    <li class="dropdown-bottom"><a href="<?=$path['user-resume-pdf'].$_SESSION['userData']['username'].".pdf";?>" target="_blank"><?=$lang['btn-pdf'];?></a></li>
                    <li class="dropdown-bottom"><a href="<?=$path['user-logout'];?>"><?=$lang['dropdown-logout'];?></a></li>
                </ul>
            </div>
            <?php } else { ?>
            <div class="login-nav hidden-xs">
                <a href="<?=$path['login'];?>">Login</a>
                <span> / </span>
                <a href="<?=$path['register'];?>">Sign up</a>
            </div>
            <?php } ?>
            <div class="select-sub">
                <select id="select-lang" onchange="changeLang();" name="lang" class="form-control input-lang">
                    <option value="en" <?php if($_sub == 'EN'){ ?>selected<?php } ?>>English</option>
                    <option value="id" <?php if($_sub == 'ID'){ ?>selected<?php } ?>>Indonesia</option>
                </select>
            </div>
        </div>
        <div class="mobile-top-nav visible-xs">
            <a href="<?=$global['absolute-url'];?>" class="logo-mobile">
                <img src="<?=$global['logo-desktop'];?>" alt="logo">
                <span>your anywhere CV & portfolio</span>
            </a>
            <div class="mobile-bar">
                <a href="javascript:void(0)" id="bar-toggle" class="mobile-toggle in">
                    <img id="bar-img" src="<?=$global['img-url'];?>img/bar-icon.png" alt="icon">
                </a>
                <ul id="bar-menu" class="mobile-list">
                    <li><a href="<?=$path['home'];?>" class="mobile-link">Overview & Features</a></li>
                    <li><a href="<?=$path['testimoni'];?>" class="mobile-link">Testimonial</a></li>
                    <li><a href="<?=$path['tips'];?>" class="mobile-link">Career Tips & Blogs</a></li>
                </ul>
                <div class="login-nav">
                    <?php if(isset($_SESSION['userData']['id']) && isset($_SESSION['userData']['username']) && isset($_SESSION['userData']['email']) && isset($_SESSION['userData']['auth_code'])){ ?>
                    <a href="<?=$path['user-edit'];?>" title="<?=$_SESSION['userData']['username'];?> Profile" class="mob-nav-user"><i class="glyphicon glyphicon-user"></i></a>
                    <?php } else { ?>
                    <a href="<?=$path['login'];?>" class="hmenu-started"><?=$lang['btn-started'];?></a>
                    <?php } ?>
                </div>
                <div class="select-sub">
                    <select id="mob-lang" onchange="changeMobLang();" name="lang" class="form-control input-lang">
                        <option value="en" <?php if($_sub == 'EN'){ ?>selected<?php } ?>>En</option>
                        <option value="id" <?php if($_sub == 'ID'){ ?>selected<?php } ?>>Id</option>
                    </select>
                </div>
            </div>
        </div>
    </div>
    <div class="tab-menu hidden-xs">
        <div class="container container-max">
            <ul class="hmenu-list">
                <li>
                    <a href="<?=$path['home'];?>" class="hmenu-link <?php if(isset($curpage)){ if($curpage == "home"){ ?>hmenu-active<?php } } ?>">
                        <?php if(isset($curpage)){ if($curpage == "home"){ ?>
                        <img src="<?=$global['absolute-url'];?>img/arrow.png" alt="image">
                        <?php } } ?>
                        Overview & Features
                    </a>
                </li>
                <li>
                    <a href="<?=$path['testimoni'];?>" class="hmenu-link <?php if(isset($curpage)){ if($curpage == "testimoni"){ ?>hmenu-active<?php } } ?>">
                        <?php if(isset($curpage)){ if($curpage == "testimoni"){ ?>
                        <img src="<?=$global['absolute-url'];?>img/arrow.png" alt="image">
                        <?php } } ?>
                        Testimonial
                    </a>
                </li>
                <li>
                    <a href="<?=$path['tips'];?>" class="hmenu-link <?php if(isset($curpage)){ if($curpage == "tips"){ ?>hmenu-active<?php } } ?>">
                        <?php if(isset($curpage)){ if($curpage == "tips"){ ?>
                        <img src="<?=$global['absolute-url'];?>img/arrow.png" alt="image">
                        <?php } } ?>
                        Career Tips & Blogs
                    </a>
                </li>
            </ul>
        </div>
    </div>
</div>
<script type="text/javascript">
    function changeLang(){
        var sub = $("#select-lang").val();
        var link = "<?php echo 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];?>";
        jQuery.ajax({
            url: "<?php echo $global['head-url'];?>packages/setlanguage.php",
            type: "POST",
            data: {
                lang: sub,
            },
            dataType : 'json',
            success: function(data, textStatus, xhr) {
                console.log(data); // do with data e.g success message
                window.location.href = link;
            }
        });
    }
    function changeMobLang(){
        var sub = $("#mob-lang").val();
        var link = "<?php echo 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];?>";
        jQuery.ajax({
            url: "<?php echo $global['head-url'];?>packages/setlanguage.php",
            type: "POST",
            data: {
                lang: sub,
            },
            dataType : 'json',
            success: function(data, textStatus, xhr) {
                console.log(data); // do with data e.g success message
                window.location.href = link;
            }
        });
    }
    $("#bar-toggle").click(function(){
        $("#bar-menu").slideToggle();
        if($("#bar-toggle").hasClass("in")){
            $("#bar-toggle").removeClass("in");
            $("#bar-img").attr("src","<?=$global['img-url'];?>img/cancel-icon.png");
        } else {
            $("#bar-toggle").addClass("in");
            $("#bar-img").attr("src","<?=$global['img-url'];?>img/bar-icon.png");
        }
    });
</script>