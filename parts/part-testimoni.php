<?php 
if(isset($_SESSION['testimonistatus'])){
    $testimoni_status = $_SESSION['testimonistatus'];
    unset($_SESSION['testimonistatus']);
} else {
    $testimoni_status = "";
}
?>
<div class="row">
    <div class="col-xs-12">
        <div class="testi-notice">
            <?=$lang['testimoni-footer'];?> <a href="#" data-toggle="modal" data-target="#modalTestimoni"><?=$lang['testimoni-link'];?></a>
        </div>
        <div class="testi-line"></div>
    </div>
</div>
<!-- Modal Testimoni-->
<div class="modal fade" id="modalTestimoni" tabindex="-1" role="dialog" aria-labelledby="modalTestimoni" data-backdrop="false">
    <div class="modal-dialog mtesti-dialog" role="document">
        <div class="modal-content mtesti-content">
            <div class="modal-body mtesti-body">
                <button type="button" class="mtesti-colse close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <div class="mtesti-desc">
                    <?=$lang['testimoni-modal_note'];?>
                </div>
                <div class="mtesti-title">
                    <?=$lang['testimoni-modal_head'];?>
                </div>
                <?php if(isset($_SESSION['userData']['id']) && isset($_SESSION['userData']['username']) && isset($_SESSION['userData']['email']) && isset($_SESSION['userData']['auth_code'])){ ?>
                <form name="formTestimoni" action="<?=$path['home'];?>?action=testimoni" method="POST" enctype="multipart/form-data">
                    <textarea class="form-control mtesti-textrea" name="testimoni" rows="5" required></textarea>
                    <input type="hidden" name="username" value="<?=$_SESSION['userData']['username'];?>" />
                    <input type="hidden" name="email" value="<?=$_SESSION['userData']['email'];?>" />
                    <input type="hidden" name="url" value="<?="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];?>" />
                    <div class="text-right">
                        <button type="submit" class="btn btn-mtesti"><?=$lang['btn-send'];?></button>
                    </div>
                </form>
                <?php } else { ?>
                <textarea class="form-control mtesti-textrea" name="testimonial" rows="5" readonly disabled required></textarea>
                <div class="text-right">
                    <a href="<?=$path['login'];?>" class="btn btn-mtesti"><?=$lang['notlog-testimoni'];?></a>
                </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>
<div id="alert-testimoni" class="testimoni-alert" <?php if($testimoni_status != ""){ ?> style="display: block;"<?php } ?>>
    <div class="testimoni-card">
        Thanks For Send Us Testimony.<br/>
        We Happy To Know What Your Thinking About Us.
    </div>
</div>
<?php if($testimoni_status != null || $testimoni_status != ""){ ?>
<script type="text/javascript">
  $(document).ready(function() {
      $("#alert-testimoni").delay(5000).fadeOut();
  });
</script>
<?php } ?>