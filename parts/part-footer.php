<?php if(!isset($_SESSION['userData']['id']) && !isset($_SESSION['userData']['username']) && !isset($_SESSION['userData']['email']) && !isset($_SESSION['userData']['auth_code'])){ ?>
<div class="section-footer">
    <div class="footer-text"><?=$lang['footer-head'];?></div>
    <div class="footer-started">
        <a href="<?=$path['login'];?>"><?=$lang['btn-started'];?></a>
    </div>
    <div class="footer-free"><?=$lang['footer-note'];?></div>
</div>
<?php } ?>
<div class="part-footer">
    <div class="container container-max">
        <div class="footer-nav-list hidden-xs" style="display: none;">
            <ul>
                <li><a href="#" class="footer-nav">About Us</a></li>
                <li><a href="#" class="footer-nav">Tips & Advice</a></li>
                <li><a href="#" class="footer-nav">Discover</a></li>
            </ul>
        </div>
        <div class="footer-menu-list">
            <ul>
                <li><a href="#" class="footer-menu hidden-xs">Privacy Policy</a></li>
                <li><a href="#" class="footer-menu hidden-xs">Legal</a></li>
                <li><span><?=$global['copyright'];?></span></li>
            </ul>
        </div>
    </div>
</div>