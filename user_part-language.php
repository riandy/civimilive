<div  id="personal-lang-section" class="job-sbox" data-order="6">
  <div id="language-header" class="user-section-header row border-bottom-eee">
    <div class="col-xs-6">
      <div class="profile-title"><i class="glyphicon glyphicon-text-background"></i>&nbsp;&nbsp;<?=$lang['lang-title'];?></div>
    </div>
    <div class="col-xs-6">
        <div class="edit-button text-right">
            <span class="sort-icon"><i class="glyphicon glyphicon-sort"></i></span>
            <a href="Javascript:;" id="add-language" onclick="hideEmptyLanguage();" data-toggle="collapse" data-target="#slide-language" aria-expanded="true" aria-controls="slide-language">
              <i class="glyphicon glyphicon-plus"></i> <span class="hidden-xs"><?=$lang['lang-btn'];?></span>
            </a>
        </div>
    </div>
  </div>

  <div id="language-profile" class="profile-well">
    <!-- start add language -->
    <div id="slide-language" class="collapse panel-input">
      <div class="panel-input-body panel-input-language">
        <div class="row up15">
          <div class="col-xs-12">
            <div class="form-label bold"><?=$lang['lang-name'];?> *</div>
          </div>
          <div class="col-sm-10 col-xs-12">
            <div id="lname-box" class="lname-box">
              <input id="language-name" onfocus="$('#lname-list').show();" oninput="get_lname();$('#lname-list').show();" type="text" name="" class="form-control no-radius" placeholder="<?=$lang['lang-name_placeholder'];?>">
              <div id="lname-list" class="lname-list" onmouseover="$('#lname-list').show();">
                <ul id="lname-ul">

                </ul>
              </div>
            </div>
            <div id="language-vname" class="valid-message"></div>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-10 col-xs-12 up1">
            <div class="form-label bold">
              <?=$lang['lang-listening'];?> *
              <a id="info_listening" href="javascript:;" class="language-tips" type="button" data-container="body" data-toggle="popover" data-placement="top" data-content="<?=$lang['lang-listening_placeholder'];?>"><i class="glyphicon glyphicon-info-sign"></i></a>
            </div>
          </div>
          <div class="col-sm-10 col-xs-12" style="">
            <select id="language-listening" name="" class="form-control no-radius">
              <option value="" class="language-listening"><?=$lang['lang-listening_select'];?></option>
              <option value="1" class="language-listening">Beginner - < 1 year</option>
              <option value="2" class="language-listening">Intermediate - 1 - 3 year</option>
              <option value="3" class="language-listening">Advanced - 3 - 5 year</option>
              <option value="4" class="language-listening">Expert - > 5 year</option>
              <option value="5" class="language-listening">Native</option>
            </select>
          </div>
          <div class="col-xs-12">
            <div id="language-vlistening" class="valid-message"></div>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-10 col-xs-12 up1">
            <div class="form-label bold">
              <?=$lang['lang-speaking'];?> *
              <a id="info_speaking" href="javascript:;" class="language-tips" type="button" data-container="body" data-toggle="popover" data-placement="top" data-content="<?=$lang['lang-speaking_placeholder'];?>"><i class="glyphicon glyphicon-info-sign"></i></a>
            </div>
          </div>
          <div class="col-sm-10 col-xs-12" style="">
            <select id="language-speaking" name="" class="form-control no-radius">
              <option value="" class="language-speaking"><?=$lang['lang-speaking_select'];?></option>
              <option value="1" class="language-speaking">Beginner - < 1 year</option>
              <option value="2" class="language-speaking">Intermediate - 1 - 3 year</option>
              <option value="3" class="language-speaking">Advanced - 3 - 5 year</option>
              <option value="4" class="language-speaking">Expert - > 5 year</option>
              <option value="5" class="language-speaking">Native</option>
            </select>
          </div>
          <div class="col-xs-12">
            <div id="language-vspeaking" class="valid-message"></div>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-10 col-xs-12 up1">
            <div class="form-label bold">
              <?=$lang['lang-reading'];?> *
              <a id="info_reading" href="javascript:;" class="language-tips" type="button" data-container="body" data-toggle="popover" data-placement="top" data-content="<?=$lang['lang-reading_placeholder'];?>"><i class="glyphicon glyphicon-info-sign"></i></a>
            </div>
          </div>
          <div class="col-sm-10 col-xs-12" style="">
            <select id="language-reading" name="" class="form-control no-radius">
              <option value="" class="language-reading"><?=$lang['lang-reading_select'];?></option>
              <option value="1" class="language-reading">Beginner - < 1 year</option>
              <option value="2" class="language-reading">Intermediate - 1 - 3 year</option>
              <option value="3" class="language-reading">Advanced - 3 - 5 year</option>
              <option value="4" class="language-reading">Expert - > 5 year</option>
              <option value="5" class="language-reading">Native</option>
            </select>
          </div>
          <div class="col-xs-12">
            <div id="language-vreading" class="valid-message"></div>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-10 col-xs-12 up1">
            <div class="form-label bold">
              <?=$lang['lang-writing'];?> *
              <a id="info_writing" href="javascript:;" class="language-tips" type="button" data-container="body" data-toggle="popover" data-placement="top" data-content="<?=$lang['lang-writing_placeholder'];?>"><i class="glyphicon glyphicon-info-sign"></i></a>
            </div>
          </div>
          <div class="col-sm-10 col-xs-12" style="">
            <select id="language-writing" name="" class="form-control no-radius">
              <option value="" class="language-writing"><?=$lang['lang-writing_select'];?></option>
              <option value="1" class="language-writing">Beginner - < 1 year</option>
              <option value="2" class="language-writing">Intermediate - 1 - 3 year</option>
              <option value="3" class="language-writing">Advanced - 3 - 5 year</option>
              <option value="4" class="language-writing">Expert - > 5 year</option>
              <option value="5" class="language-writing">Native</option>
            </select>
          </div>
          <div class="col-xs-12">
            <div id="language-vwriting" class="valid-message"></div>
          </div>
        </div>
        <div class="row up1">
          <div class="col-sm-10 col-xs-12">
            <div class="form-label">
              <?=$lang['lang-desc'];?>
              <a id="info_wdesc" href="javascript:;" class="language-tips" type="button" data-container="body" data-toggle="popover" data-placement="top" data-content="<?=$lang['lang-desc_placeholder'];?>"><i class="glyphicon glyphicon-info-sign"></i></a>
            </div>
          </div>
          <div class="col-sm-10 col-xs-12">
            <textarea id="language-desc" name="desc" class="form-control no-radius" rows="4" placeholder="<?=$lang['lang-desc_placeholder'];?>"></textarea>
          </div>
        </div>
        <div class="row up2">
          <div class="col-xs-12">
            <a href="Javascript:;" class="btn btn-info" onclick="save_language();">Save</a><span class="pad05"></span>
            <a href="Javascript:;" onclick="hideEmptyLanguage();" type="reset" class="btn btn-default" data-toggle="collapse" data-target="#slide-language" aria-expanded="true" aria-controls="slide-language">Cancel</a>
          </div>
        </div>
      </div>
    </div><!-- end panel add language -->


    <!-- main language -->
    <div id="main-language" class="panel-group marg0" role="tablist" aria-multiselectable="true">
    </div> <!-- end main language -->
    <div class="see-more"><a href="javascript:;" id="see-language" class="hide">SEE <span id="seeLang-num"></span> MORE</a></div>
    <div id='load-language' class='text-center'></div>
    <input id="total-language" type="hidden">

    <!-- start edit language -->
    <div id="edit-language-place" class="hide">
      <div id="edit-language">
        <div class="panel-input-language">
          <div class="row up15">
            <div class="col-xs-12">
              <div class="form-label bold"><?=$lang['lang-name'];?> *</div>
            </div>
            <div class="col-sm-10 col-xs-12">
              <div id="lname-boxEdit" class="lname-box">
                <input id="language-ename" onfocus="$('#lname-listEdit').show();" oninput="get_lnameEdit();$('#lname-listEdit').show();" type="text" name="" class="form-control no-radius" placeholder="<?=$lang['lang-name_placeholder'];?>">
                <div id="lname-listEdit" class="lname-list" onmouseover="$('#lname-listEdit').show();">
                  <ul id="lname-eul">

                  </ul>
                </div>
              </div>
              <div id="language-evname" class="valid-message"></div>
            </div>
          </div>
          <div class="row">
            <div class="col-sm-10 col-xs-12 up1">
              <div class="form-label bold">
                <?=$lang['lang-listening'];?> *
                <a id="info_elistening" href="javascript:;" class="language-tips" type="button" data-container="body" data-toggle="popover" data-placement="top" data-content="<?=$lang['lang-listening_placeholder'];?>"><i class="glyphicon glyphicon-info-sign"></i></a>
              </div>
            </div>
            <div class="col-sm-10 col-xs-12" style="">
              <select id="language-elistening" name="" class="form-control no-radius">
                <option value="" class="language-elistening"><?=$lang['lang-listening_select'];?></option>
                <option value="1" class="language-elistening">Beginner - < 1 year</option>
                <option value="2" class="language-elistening">Intermediate - 1 - 3 year</option>
                <option value="3" class="language-elistening">Advanced - 3 - 5 year</option>
                <option value="4" class="language-elistening">Expert - > 5 year</option>
                <option value="5" class="language-elistening">Native</option>
              </select>
            </div>
            <div class="col-xs-12">
              <div id="language-vlistening" class="valid-message"></div>
            </div>
          </div>
          <div class="row">
            <div class="col-sm-10 col-xs-12 up1">
              <div class="form-label bold">
                <?=$lang['lang-speaking'];?> *
                <a id="info_espeaking" href="javascript:;" class="language-tips" type="button" data-container="body" data-toggle="popover" data-placement="top" data-content="<?=$lang['lang-speaking_placeholder'];?>"><i class="glyphicon glyphicon-info-sign"></i></a>
              </div>
            </div>
            <div class="col-sm-10 col-xs-12" style="">
              <select id="language-espeaking" name="" class="form-control no-radius">
                <option value="" class="language-espeaking"><?=$lang['lang-speaking_select'];?></option>
                <option value="1" class="language-espeaking">Beginner - < 1 year</option>
                <option value="2" class="language-espeaking">Intermediate - 1 - 3 year</option>
                <option value="3" class="language-espeaking">Advanced - 3 - 5 year</option>
                <option value="4" class="language-espeaking">Expert - > 5 year</option>
                <option value="5" class="language-espeaking">Native</option>
              </select>
            </div>
            <div class="col-xs-12">
              <div id="language-vspeaking" class="valid-message"></div>
            </div>
          </div>
          <div class="row">
            <div class="col-sm-10 col-xs-12 up1">
              <div class="form-label bold">
                <?=$lang['lang-reading'];?> *
                <a id="info_ereading" href="javascript:;" class="language-tips" type="button" data-container="body" data-toggle="popover" data-placement="top" data-content="<?=$lang['lang-reading_placeholder'];?>"><i class="glyphicon glyphicon-info-sign"></i></a>
              </div>
            </div>
            <div class="col-sm-10 col-xs-12" style="">
              <select id="language-ereading" name="" class="form-control no-radius">
                <option value="" class="language-ereading"><?=$lang['lang-reading_select'];?></option>
                <option value="1" class="language-ereading">Beginner - < 1 year</option>
                <option value="2" class="language-ereading">Intermediate - 1 - 3 year</option>
                <option value="3" class="language-ereading">Advanced - 3 - 5 year</option>
                <option value="4" class="language-ereading">Expert - > 5 year</option>
                <option value="5" class="language-ereading">Native</option>
              </select>
            </div>
            <div class="col-xs-12">
              <div id="language-vreading" class="valid-message"></div>
            </div>
          </div>
          <div class="row">
            <div class="col-sm-10 col-xs-12 up1">
              <div class="form-label bold">
                <?=$lang['lang-writing'];?> *
                <a id="info_ewriting" href="javascript:;" class="language-tips" type="button" data-container="body" data-toggle="popover" data-placement="top" data-content="<?=$lang['lang-writing_placeholder'];?>"><i class="glyphicon glyphicon-info-sign"></i></a>
              </div>
            </div>
            <div class="col-sm-10 col-xs-12" style="">
              <select id="language-ewriting" name="" class="form-control no-radius">
                <option value="" class="language-ewriting"><?=$lang['lang-writing_select'];?></option>
                <option value="1" class="language-ewriting">Beginner - < 1 year</option>
                <option value="2" class="language-ewriting">Intermediate - 1 - 3 year</option>
                <option value="3" class="language-ewriting">Advanced - 3 - 5 year</option>
                <option value="4" class="language-ewriting">Expert - > 5 year</option>
                <option value="5" class="language-ewriting">Native</option>
              </select>
            </div>
            <div class="col-xs-12">
              <div id="language-vwriting" class="valid-message"></div>
            </div>
          </div>
          <div class="row up1">
            <div class="col-sm-10 col-xs-12">
              <div class="form-label">
                <?=$lang['lang-desc'];?>
                <a id="info_ewdesc" href="javascript:;" class="language-tips" type="button" data-container="body" data-toggle="popover" data-placement="top" data-content="<?=$lang['lang-desc_placeholder'];?>"><i class="glyphicon glyphicon-info-sign"></i></a>
              </div>
            </div>
            <div class="col-sm-10 col-xs-12">
              <textarea id="language-edesc" name="desc" class="form-control no-radius" rows="3" placeholder="<?=$lang['lang-desc_placeholder'];?>"></textarea>
            </div>
          </div>
          <div class="row up1">
            <div class="col-xs-12">
              <input id="edit_language_old_title" type="hidden">
              <input id="edit_language_id" type="hidden">
              <a class="btn btn-info" onclick="update_language();" >Save</a><span class="pad05"></span>
              <a id="reset_edit_language" type="reset" class="btn btn-default" onclick="close_edit_language()">Cancel</a>
              <a href="javascript:;" id="delete-language" onclick="delete_language();" class="delete-text">Delete</a>
            </div>
          </div>
        </div>
      </div>
    </div><!-- end edit language -->
  </div> <!-- end language profile -->

<script>
  $('#info_listening').popover({
    placement : 'top',
    trigger : 'click',
    delay: { 
       show: "100", 
       hide: "100"
    },
  });
  $('#info_listening').on('shown.bs.popover', function() {
    setTimeout(function() {
        $('#info_listening').popover('hide');
    }, 4000);
  });
  $('#info_speaking').popover({
    placement : 'top',
    trigger : 'click',
    delay: { 
       show: "100", 
       hide: "100"
    },
  });
  $('#info_speaking').on('shown.bs.popover', function() {
    setTimeout(function() {
        $('#info_speaking').popover('hide');
    }, 4000);
  });
  $('#info_reading').popover({
    placement : 'top',
    trigger : 'click',
    delay: { 
       show: "100", 
       hide: "100"
    },
  });
  $('#info_reading').on('shown.bs.popover', function() {
    setTimeout(function() {
        $('#info_reading').popover('hide');
    }, 4000);
  });
  $('#info_writing').popover({
    placement : 'top',
    trigger : 'click',
    delay: { 
       show: "100", 
       hide: "100"
    },
  });
  $('#info_writing').on('shown.bs.popover', function() {
    setTimeout(function() {
        $('#info_writing').popover('hide');
    }, 4000);
  });
  $('#info_elistening').popover({
    placement : 'top',
    trigger : 'click',
    delay: { 
       show: "100", 
       hide: "100"
    },
  });
  $('#info_elistening').on('shown.bs.popover', function() {
    setTimeout(function() {
        $('#info_elistening').popover('hide');
    }, 4000);
  });
  $('#info_espeaking').popover({
    placement : 'top',
    trigger : 'click',
    delay: { 
       show: "100", 
       hide: "100"
    },
  });
  $('#info_espeaking').on('shown.bs.popover', function() {
    setTimeout(function() {
        $('#info_espeaking').popover('hide');
    }, 4000);
  });
  $('#info_ereading').popover({
    placement : 'top',
    trigger : 'click',
    delay: { 
       show: "100", 
       hide: "100"
    },
  });
  $('#info_ereading').on('shown.bs.popover', function() {
    setTimeout(function() {
        $('#info_ereading').popover('hide');
    }, 4000);
  });
  $('#info_ewriting').popover({
    placement : 'top',
    trigger : 'click',
    delay: { 
       show: "100", 
       hide: "100"
    },
  });
  $('#info_ewriting').on('shown.bs.popover', function() {
    setTimeout(function() {
        $('#info_ewriting').popover('hide');
    }, 4000);
  });
  $('#info_wdesc').popover({
    placement : 'top',
    trigger : 'click',
    delay: { 
       show: "100", 
       hide: "100"
    },
  });
  $('#info_wdesc').on('shown.bs.popover', function() {
    setTimeout(function() {
        $('#info_wdesc').popover('hide');
    }, 4000);
  });
  $('#info_erate').popover({
    placement : 'top',
    trigger : 'click',
    delay: { 
       show: "100", 
       hide: "100"
    },
  });
  $('#info_erate').on('shown.bs.popover', function() {
    setTimeout(function() {
        $('#info_erate').popover('hide');
    }, 4000);
  });
  $('#info_ewdesc').popover({
    placement : 'top',
    trigger : 'click',
    delay: { 
       show: "100", 
       hide: "100"
    },
  });
  $('#info_ewdesc').on('shown.bs.popover', function() {
    setTimeout(function() {
        $('#info_ewdesc').popover('hide');
    }, 4000);
  });
  //get language name
  $('html').click(function() {
    $('#lname-list').hide();
    $("#language-name").removeClass("load-input");
  });
  $('#lname-box').click(function(event){
    event.stopPropagation();
    $("#language-name").removeClass("load-input");
  });
  $('html').click(function() {
    $('#lname-listEdit').hide();
    $("#language-ename").removeClass("load-input");
  });
  $('#lname-boxEdit').click(function(event){
    event.stopPropagation();
    $("#language-ename").removeClass("load-input");
  });
  function get_lname(){
    $("#language-name").addClass("load-input");
    var q = $("#language-name").val();
    var url = "<?=$api['lang-autocomplete'];?>"+q;                            
    $.ajax({url: url,success:function(result){
      $("#language-name").removeClass("load-input");
      $('#lname-ul').html(replace_lname(result.data,q));
    }}); 
  }
  function get_lnameEdit(){
    $("#language-ename").addClass("load-input");
    var q = $("#language-ename").val();
    var url = "<?=$api['lang-autocomplete'];?>"+q;                            
    $.ajax({url: url,success:function(result){
      $("#language-ename").removeClass("load-input");
      $('#lname-eul').html(replace_lnameEdit(result.data,q));
    }}); 
  }
  function replace_lname(datas,text){
    var resultHTML='';

    if(datas != null){
      var obj = datas;    
      for(var i=0;i < obj.length;i++){
        if(obj[i].Lang_ID != '' && obj[i].Lang_name != ''){
          resultHTML += "<li class='lname-li' id='lname-"+obj[i].Lang_ID+"' onclick='selectLname("+obj[i].Lang_ID+")' data-sk_title='"+obj[i].Lang_name+"'>";
          if(obj[i].Lang_img != ""){
            resultHTML += "<div class='lname-image'><img src='<?=$global['img-url'];?>"+obj[i].Lang_img+"' alt=''></div>";
          }else {
            resultHTML += "<div class='lname-image'><img src='<?=$placeholder['img-language'];?>' alt=''></div>";
          }
          resultHTML += "<div class='lname-text'>";
          resultHTML += "<div class='lname-title'>"+obj[i].Lang_name+"</div>";
          resultHTML += "</div>";
          resultHTML += "</li>";
        }
      }
    } else {
      resultHTML += "<li class='no-list'><div class='no-list-text'><?=$lang['entry-result'];?> \"<span>"+text+"</span>\" <?=$lang['entry-result_text'];?></div></li>";
    }
    return resultHTML;
  }
  function replace_lnameEdit(datas,text){
    var resultHTML='';

    if(datas != null){
      var obj = datas;    
      for(var i=0;i < obj.length;i++){
        if(obj[i].Lang_ID != '' && obj[i].Lang_name != ''){
          resultHTML += "<li class='lname-li' id='elname-"+obj[i].Lang_ID+"' onclick='selectLnameEdit("+obj[i].Lang_ID+")' data-sk_etitle='"+obj[i].Lang_name+"'>";
          if(obj[i].Lang_img != ""){
            resultHTML += "<div class='lname-image'><img src='<?=$global['img-url'];?>"+obj[i].Lang_img+"' alt=''></div>";
          }else {
            resultHTML += "<div class='lname-image'><img src='<?=$placeholder['img-language'];?>' alt=''></div>";
          }
          resultHTML += "<div class='lname-text'>";
          resultHTML += "<div class='lname-title'>"+obj[i].Lang_name+"</div>";
          resultHTML += "</div>";
          resultHTML += "</li>";
        }
      }
    } else {
      resultHTML += "<li class='no-list'><div class='no-list-text'><?=$lang['entry-result'];?> \"<span>"+text+"</span>\" <?=$lang['entry-result_text'];?></div></li>";
    }
    return resultHTML;
  }
  function selectLname(param){
    var title = $('#lname-'+param).data('sk_title');
    $("#language-name").removeClass("load-input");
    $('#language-name').val(title);
    $('#lname-list').hide();
  }
  function selectLnameEdit(param){
    var title = $('#elname-'+param).data('sk_etitle');
    $("#language-ename").removeClass("load-input");
    $('#language-ename').val(title);
    $('#lname-listEdit').hide();
  }
  //clear value add language
  function emptyAddLanguage(){
    $("#language-name").val('');
    $('option[value=""].language-listening').attr("selected", true);
    $('option[value=""].language-speaking').attr("selected", true);
    $('option[value=""].language-reading').attr("selected", true);
    $('option[value=""].language-writing').attr("selected", true);
    $("#language-desc").val('');
  }
  //open slide edit
  function edit_box_language(param){
    if ($('#slide-language').hasClass('in')){
      $('#slide-language').collapse('hide');
    }
    if($('.language-box').hasClass('hide-language')){
      $('.language-box').removeClass('hide-language');
      if($('.collapse-language').hasClass('in')){
        $('.collapse-language').removeClass('in')
      }
      $('#language-'+param).addClass('hide-language');
    } else {
      $('#language-'+param).addClass('hide-language');
    }
    $('#edit-language').detach().appendTo('#lcollapse-'+param+'');
    var id = $('#language-'+param).data('language_id');
    var title = $('#language-'+param).data('language_name');
    var listen = $('#language-'+param).data('language_listening');
    var speak = $('#language-'+param).data('language_speaking');
    var read = $('#language-'+param).data('language_reading');
    var write = $('#language-'+param).data('language_writing');
    var desc = $('#language-'+param).data('language_desc');

    $('#reset_edit_language').data("target") === "#lcollapse";
    $('#language-ename').val(title);
    $('#language-edesc').val(desc);
    $('#edit_language_old_title').val(title);
    $('#edit_language_id').val(id);
    $('option[value="' + listen + '"].language-elistening').attr("selected", true);
    $('option[value="' + speak + '"].language-espeaking').attr("selected", true);
    $('option[value="' + read + '"].language-ereading').attr("selected", true);
    $('option[value="' + write + '"].language-ewriting').attr("selected", true);
  }
  //close edit language
  function close_edit_language(){
    var id = $('#edit_language_id').val();
    $('#close-language-'+id+'').click();
    $('#language-'+id).removeClass('hide-language');
  }
  //hide empty word when add new
  function hideEmptyLanguage(){
    var myElem = document.getElementById('empty-language');
    if(myElem != null){
      $('#empty-language').toggle();
    }
    var id = $('#edit_language_id').val();
    if (id != ''){
      if ($('#lcollapse-'+id).hasClass('in')){
        $('#close-language-'+id+'').click();
        $('#language-'+id).removeClass('hide-language');
      }
    }
    emptyAddLanguage();
  }
  //update function
  function update_language(){
    if(validLanguageEdit()){
    $('#edit-language').detach().appendTo('#edit-language-place');
    var ilanguage = $('#edit_language_id').val();
    $('#close-language-'+ilanguage).click();
    $('html, body').animate({
      scrollTop: $('#language-header').offset().top-80
    }, 'fast');
    //$('#load-language-'+ilanguage).html('<img src="img/load-blue.gif" class="marg1" />');

      var url = "<?=$api['lang-update'];?>";
      var language_id = $("#edit_language_id").val();
      var language_name = $("#language-ename").val();
      var language_listening = $("#language-elistening").val();
      var language_speaking = $("#language-espeaking").val();
      var language_reading = $("#language-ereading").val();
      var language_writing = $("#language-ewriting").val();
      var language_desc = $("#language-edesc").val();
      var data = {
        user_id : "<?=$_SESSION['userData']['id'];?>",
        auth_code : "<?=$_SESSION['userData']['auth_code'];?>",
        id : language_id,
        name : language_name,
        desc : language_desc,
        listen : language_listening,
        speak : language_speaking,
        read : language_reading,
        write : language_writing
      }
      $.ajax({url: url,data : data, success:function(result){
        get_language();
        console.log(result);
      }});
    }
  }
  //insert function
  function save_language(){
    if(validLanguage()){
      $('#edit-language').detach().appendTo('#edit-language-place');
      $('html, body').animate({
        scrollTop: $('#language-header').offset().top-80
      }, 'fast');
      //$('#load-language').html('<img src="img/load-blue.gif" style="margin:70px 0" />');

      var url = "<?=$api['lang-update'];?>";
      var language_title = $("#language-name").val();
      var language_listening = $("#language-listening").val();
      var language_speaking = $("#language-speaking").val();
      var language_reading = $("#language-reading").val();
      var language_writing = $("#language-writing").val();
      var language_desc = $("#language-desc").val();
      var data = {
        user_id : "<?=$_SESSION['userData']['id'];?>",
        auth_code : "<?=$_SESSION['userData']['auth_code'];?>",
        name : language_title,
        desc : language_desc,
        listen : language_listening,
        speak : language_speaking,
        read : language_reading,
        write : language_writing
      }
      $.ajax({url: url,data : data, success:function(result){
        $('#slide-language').collapse('hide');
        get_language();
        emptyAddLanguage();
        console.log(result);
      }});
    }
  } 
  //delete function
  function delete_language(){
    if (confirm("Are you sure to delete this information ?")) {
      $('#edit-language').detach().appendTo('#edit-language-place');
      $('html, body').animate({
        scrollTop: $('#language-header').offset().top-80
      }, 'fast');
      
      var url = "<?=$api['lang-delete'];?>";
      var language_id = $("#edit_language_id").val();
      var data = {
        user_id : "<?=$_SESSION['userData']['id'];?>",
        auth_code : "<?=$_SESSION['userData']['auth_code'];?>",
        id : language_id
      }
      $.ajax({url: url,data : data, success:function(result){
        get_language();
        console.log(result);
      }});
    }
  }
  get_language();
  function get_language(){
    $('#see-language').addClass('hide');
    $('#load-language').html("<img src='<?=$global['absolute-url'];?>img/load-blue.gif' style='margin:70px 0' />");
    $('#main-language').html('');
    var url = "<?=$api['lang-data'];?>&user_id=<?php echo $_SESSION['userData']['id'];?>&auth_code=<?php echo $_SESSION['userData']['auth_code'];?>";                            
    $.ajax({url: url,success:function(result){
      setTimeout(function() {
      $('#load-language').html('');
      $('#main-language').html(replace_language(result.data));
      seeMoreLang();
      }, 1000)
    }}); 
  }
  function replace_profiency(param){
    var rate = "";
    if(param == 1){
      rate = "Beginner";
    } else if (param == 2){
      rate = "Intermediate";
    } else if (param == 3){
      rate = "Advanced";
    } else if (param == 4){
      rate = "Expert";
    } else if (param == 5){
      rate = "Native";
    }
    return rate;
  }
  function replace_language(datas){
    var resultHTML='';resultNext = "";
    var total = 0;previous = "";user_name = "";link="";
    var d = new Date();strip_string ="";
    var current = d.getTime();
    // console.log(datas);
    if(datas != null){
      var obj = datas;    
      for(var i=0;i < obj.length;i++){
        if(obj[i].Lang_ID != '' && obj[i].Lang_name != ''){
          resultHTML += "<div class='row row-language hide hm-language' role='tab'>";
          resultHTML += "<div class='col-xs-12'>";
          resultHTML += "<a href='#lcollapse-"+obj[i].Lang_ID+"' aria-controls='lcollapse-"+obj[i].Language_ID+"' data-parent='#main-language' onclick='edit_box_language("+obj[i].Lang_ID+");' data-toggle='collapse' aria-expanded='false' id='language-"+obj[i].Lang_ID+"' data-language_id='"+obj[i].Lang_ID+"' data-language_name='"+obj[i].Lang_name+"' data-language_listening='"+obj[i].ATlang_listen+"' data-language_speaking='"+obj[i].ATlang_speak+"' data-language_reading='"+obj[i].ATlang_read+"' data-language_writing='"+obj[i].ATlang_write+"' data-language_desc='"+obj[i].ATlang_desc+"' class='language-box'>";
          resultHTML += "<div class='row'>";
          resultHTML += "<div class='col-md-10 col-xs-9 pad0'>";
          resultHTML += "<div class='language-name'>"+obj[i].Lang_name+" <span class='language-etext edit-button'><i class='glyphicon glyphicon-edit'></i> <span class='hidden-xs'>Edit</span></span></div>";
          resultHTML += "<div class='row'>";
          resultHTML += "<div class='col-xs-6 col-sm-3 language-rate'><span class='language-type'>Listening : </span>"+replace_profiency(obj[i].ATlang_listen)+"</div>";
          resultHTML += "<div class='col-xs-6 col-sm-3 language-rate'><span class='language-type'>Speaking : </span>"+replace_profiency(obj[i].ATlang_speak)+"</div>";
          resultHTML += "<div class='col-xs-6 col-sm-3 language-rate'><span class='language-type'>Reading : </span>"+replace_profiency(obj[i].ATlang_read)+"</div>";
          resultHTML += "<div class='col-xs-6 col-sm-3 language-rate'><span class='language-type'>Writing : </span>"+replace_profiency(obj[i].ATlang_write)+"</div>";
          resultHTML += "</div>";
          resultHTML += "</div>";
          resultHTML += "<div class='col-md-2 col-xs-3'></div>";
          resultHTML += "</div>";
          resultHTML += "</a>";
          resultHTML += "<div id='load-language-"+obj[i].Lang_ID+"' class='text-center'></div>";
          resultHTML += "<a id='close-language-"+obj[i].Lang_ID+"' href='#lcollapse-"+obj[i].Lang_ID+"' aria-controls='lcollapse-"+obj[i].Lang_ID+"' data-parent='#main-language' data-toggle='collapse' aria-expanded='false' class='hide'></a>";
          resultHTML += "</div>";
          resultHTML += "</div>";
          resultHTML += "<div id='lcollapse-"+obj[i].Lang_ID+"' aria-labelledby='language-"+obj[i].Lang_ID+"' class='panel-collapse collapse collapse-language pad0' role='tabpanel'></div>";

          //$("#total-language").val(obj.length);
        }
      }
    } else {
      resultHTML += "<div id='empty-language' class='empty-file'><?=$lang['lang-placeholder'];?></div>";
    }
    return resultHTML;
  }
  function seeMoreLang(){
    size = $(".row-language").size();
    x=2;

    $('.row-language:lt('+x+')').removeClass('hide');
    $('.row-language:lt('+x+')').removeClass('hm-language'); 
    size_left= $(".hm-language").size();
    if(size > 2){
      $('#see-language').removeClass('hide');
      $("#seeLang-num").text('('+size_left+')');
    } else {
      $('#see-language').addClass('hide');
    }

    $('#see-language').on('click', function(){
        $('.row-language').removeClass('hide');
        $('.row-language').removeClass('hm-language');
        $('#see-language').addClass('hide');
    });
};
  function validLanguage(){
    var language_name = $("#language-name").val();
    var language_listen = $("#language-listening").val();
    var language_speak = $("#language-speaking").val();
    var language_read = $("#language-reading").val();
    var language_write = $("#language-writing").val();

    if(language_name != ""){
      if(language_name.length >= 3){
        message = "";
        $("#language-name").removeClass('input-error');
        $("#language-vname").text(message);
      } else {
        message = "Language Name has to be at least 3 characters!";
        $("#language-name").addClass('input-error');
        $("#language-name").focus();
        $("#language-vname").text(message);
        return false;
      }
    } else {
      message = "Please input or select your Language Name!";
      $("#language-name").addClass('input-error');
      $("#language-name").focus();
      $("#language-vname").text(message);
      return false;
    }
    if(language_listen != ""){
      message = "";
      $("#language-listening").removeClass('input-error');
      $("#language-vlistening").text(message);
    } else {
      message = "Please select your listening rate!";
      $("#language-listening").addClass('input-error');
      $("#language-listening").focus();
      $("#language-vlistening").text(message);
      return false;
    }
    if(language_speak != ""){
      message = "";
      $("#language-speaking").removeClass('input-error');
      $("#language-vspeaking").text(message);
    } else {
      message = "Please select your speaking rate!";
      $("#language-speaking").addClass('input-error');
      $("#language-speaking").focus();
      $("#language-vspeaking").text(message);
      return false;
    }
    if(language_read != ""){
      message = "";
      $("#language-reading").removeClass('input-error');
      $("#language-vreading").text(message);
    } else {
      message = "Please select your reading rate!";
      $("#language-reading").addClass('input-error');
      $("#language-reading").focus();
      $("#language-vreading").text(message);
      return false;
    }
    if(language_write != ""){
      message = "";
      $("#language-writing").removeClass('input-error');
      $("#language-vwriting").text(message);
    } else {
      message = "Please select your writing rate!";
      $("#language-writing").addClass('input-error');
      $("#language-writing").focus();
      $("#language-vwriting").text(message);
      return false;
    }
    return true;
  }
  function validLanguageEdit(){
    var language_name = $("#language-ename").val();
    var language_listen = $("#language-elistening").val();
    var language_speak = $("#language-espeaking").val();
    var language_read = $("#language-ereading").val();
    var language_write = $("#language-ewriting").val();

    if(language_name != ""){
      if(language_name.length >= 3){
        message = "";
        $("#language-ename").removeClass('input-error');
        $("#language-evname").text(message);
      } else {
        message = "Language Name has to be at least 3 characters!";
        $("#language-ename").addClass('input-error');
        $("#language-ename").focus();
        $("#language-evname").text(message);
        return false;
      }
    } else {
      message = "Please input or select your Language Name!";
      $("#language-ename").addClass('input-error');
      $("#language-ename").focus();
      $("#language-evname").text(message);
      return false;
    }
    if(language_listen != ""){
      message = "";
      $("#language-elistening").removeClass('input-error');
      $("#language-evlistening").text(message);
    } else {
      message = "Please select your listening rate!";
      $("#language-elistening").addClass('input-error');
      $("#language-elistening").focus();
      $("#language-evlistening").text(message);
      return false;
    }
    if(language_speak != ""){
      message = "";
      $("#language-espeaking").removeClass('input-error');
      $("#language-evspeaking").text(message);
    } else {
      message = "Please select your speaking rate!";
      $("#language-espeaking").addClass('input-error');
      $("#language-espeaking").focus();
      $("#language-evspeaking").text(message);
      return false;
    }
    if(language_read != ""){
      message = "";
      $("#language-ereading").removeClass('input-error');
      $("#language-evreading").text(message);
    } else {
      message = "Please select your reading rate!";
      $("#language-ereading").addClass('input-error');
      $("#language-ereading").focus();
      $("#language-evreading").text(message);
      return false;
    }
    if(language_write != ""){
      message = "";
      $("#language-ewriting").removeClass('input-error');
      $("#language-evwriting").text(message);
    } else {
      message = "Please select your writing rate!";
      $("#language-ewriting").addClass('input-error');
      $("#language-ewriting").focus();
      $("#language-evwriting").text(message);
      return false;
    }
    return true;
  }

  function calltips(){
  var total =  $("#total-language").val();
  for(var j=0;j < total;j++){
    $('#info_rate-'+j).popover({ placement : 'top', trigger : 'click', delay: { show: '100', hide: '100'}, });
    $('#info_rate-'+j).toggleClass('active');
  }
  }
  function timetips(param){
    $('#info_rate-'+param).on('shown.bs.popover', function() { setTimeout(function() { $('#info_rate-'+param).popover('hide'); }, 3000); });
  }
</script>
</div> <!-- end personal lang section -->