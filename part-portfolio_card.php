<div class="modal fade" id="modal-card" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content" style="border-radius: 0;">
      <div class="modal-body pad0" style="position:relative;min-height:450px;">
        <div class="mp-content">
          <a href="#" class="mp-close" data-dismiss="modal" aria-label="Close"><img src="<?=$global['absolute-url'];?>img/close-x.png" alt="close"></a>
          <div class="mp-name">
            <img id="card-uimage" src="" alt="photo"> <span id="card-uname"></span>
          </div>
          <div id="card-ujob" class="mp-job"></div>
          <div id="card-uplace" class="mp-place"></div>
        </div>
        <div class="mp-image"><img id="mp-image" src="" alt="image"></div>
        <div class="card-detail" style="padding-bottom:20px;">
          <div id="mp-title" class="mp-title"></div>
          <div class="mp-tag">
            <ul id="tag-photo" class="row"></ul>
          </div>
          <div class="photo-func">
            <ul>
              <?php if(isset($_SESSION['userData']['id'])){?>
              <li><a href="javascript:;" class="photo-like" onclick="likeCard();"><img id="like-icon" src="<?=$global['absolute-url'];?>img/heart.png" alt="like"><span id="cyou-like"></span><span id="ctotal-like"></span> <span id="clike-text"></span></a></li>
              <?php } else { ?>
              <li><a href="#" class="photo-like disabled"><img id="like-icon" src="<?=$global['absolute-url'];?>img/heart.png" alt="like"><span id="cyou-like"></span><span id="ctotal-like"></span> <span id="clike-text"></span></a></li>
              <?php } ?>
              <li><span><img src="<?=$global['absolute-url'];?>img/eye.png" alt="views"><span id="ctotal-view"></span> Views</span></li>
              <li class="mp-share">
                <a href="#" onclick="shareCardFacebook();"><img src="http://www.civimi.com/img/share-facebook.png" alt="share"></a>
                <a href="#" onclick="shareCardTwitter();"><img src="http://www.civimi.com/img/share-twitter.png" alt="share"></a>
                <a href="#" onclick="shareCardGoogle();"><img src="http://www.civimi.com/img/share-google.png" alt="share"></a>
              </li>
            </ul>
          </div>
          <?php if(!isset($_SESSION['userData']['id'])){?>
          <div class="card-not-login">
            <a href="#" data-dismiss="modal" data-toggle="modal" data-target="#modal-login">Log in</a> to like
          </div>
          <?php } ?>
        </div>
        <input type="hidden" id="card-id">
        <input type="hidden" id="card-userid">
        <input type="hidden" id="card-username">
        <input type="hidden" id="card-like">
        <input type="hidden" id="card-tag">
        <div id="card-load" class="card-load">
          <div class="card-load-bg">
            <div class="card-load-img">
              <img src="<?=$global['absolute-url'];?>img/load-input.gif" alt="load">
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php $N_ip = getIP(); $userGeoData = getGeoIP($N_ip);?>
<script>
  function shareCardFacebook(){
    var portfolio_id = $("#card-id").val();
    var url = "https://www.facebook.com/sharer/sharer.php?u=<?=$path['portfolio-card'];?>"+portfolio_id+".html";
    var winName = "myWindow";
    var w = "500";
    var h = "600";
    var scroll = "yes";

    centeredPopup(url,winName,w,h,scroll);
  }
  function shareCardTwitter(){
    var portfolio_id = $("#card-id").val();
    var url = "https://twitter.com/intent/tweet?url=<?=$path['portfolio-card'];?>"+portfolio_id+".html";
    var winName = "myWindow";
    var w = "500";
    var h = "600";
    var scroll = "yes";

    centeredPopup(url,winName,w,h,scroll);
  }
  function shareCardGoogle(){
    var portfolio_id = $("#card-id").val();
    var url = "https://plus.google.com/share?url=<?=$path['portfolio-card'];?>"+portfolio_id+".html";
    var winName = "myWindow";
    var w = "500";
    var h = "600";
    var scroll = "yes";

    centeredPopup(url,winName,w,h,scroll);
  }
  function checkCardImage(image){
    var image_url = "";
    if( image.indexOf('http://') >= 0){
      image_url = image;
    } else if (image.indexOf('https://') >= 0){
      image_url = image;
    } else {
      image_url = "<?=$global['img-url'];?>"+image;
    }
    return image_url;
  }
  function openCard(cardID){
    $("#card-load").show();
    $("#card-id").val(cardID);
    $("#mp-image").attr("src","");
    var url = "<?=$api['photo-detail'];?>";
    var data = {
      id : cardID,
    }
    $.ajax({url: url,data : data, success:function(result){
      var image_card = "<?=$global['img-url'];?>"+result.data[0].Photo_imgLink;
      if(result.data[0].Photo_title !== null){
        var title_card = result.data[0].Photo_title;
      } else {
        var title_card = "";
      }
      if(result.data[0].User_proPhotoThmb != null || result.data[0].User_proPhotoThmb != ""){
        var image = checkCardImage(result.data[0].User_proPhotoThmb);
      } else {
        var image = "<?=$global['img-url'];?>img/dummy.jpg";
      }
      var full_name = result.data[0].User_fname+" "+result.data[0].User_lname;
      if(result.data[0].User_bioDesc !== null){
        var job = result.data[0].User_bioDesc;
      } else {
        var job = "";
      }
      var address = result.data[0].User_city+", "+result.data[0].User_country;
      var user_id = result.data[0].id_user;
      var username = result.data[0].User_username;
      $("#mp-image").attr("src",image_card);
      $("#mp-title").text(title_card);
      $("#card-userid").val(user_id);
      $("#card-username").val(username);
      $("#ctotal-view").text(result.data[0].hits_image);
      $("#card-uimage").attr("src",image);
      $("#card-uname").text(full_name);
      $("#card-ujob").text(job);
      $("#card-uplace").text(address);
      if(title_card != ""){
        $("#mp-title").show();
      } else {
        $("#mp-title").hide();
      }
      hitsCard();
      $("#card-load").fadeOut("slow");
    }});
    totalLikeCard();
    tagCard();
  }
  function tagCard(){
    var portfolio_id = $("#card-id").val();
    var porfolio_name = "photo";
    var url = "<?=$api['tag-data'];?>";
    var data = {
      table_id : portfolio_id,
      table_name : porfolio_name
    }
    $.ajax({url: url,data : data, success:function(result){
      $("#tag-photo").html(replace_tag_card(result.data));
    }}); 
  }
  function replace_tag_card(datas){
    var resultHTML="";
    var resultTag = "";
    if(datas != null){
      var obj = datas;    
      for(var i=0;i < obj.length;i++){
        if(i == eval(obj.length-1)){
          var coma = "";
        } else {
          var coma = ", ";
        }
        resultHTML += "<li>"+obj[i].Tag_title+"</li>";
        resultTag += obj[i].Tag_title+coma;
      }
    }
    $("#card-tag").val(resultTag);
    return resultHTML;
  }
  function totalLikeCard(){
    var portfolio_id = $("#card-id").val();
    var porfolio_type = "image";
    var url = "<?=$api['like-data'];?>";
    var data = {
      type_id : portfolio_id,
      type : porfolio_type
    }
    $.ajax({url: url,data : data, success:function(result){
      var total = result.data[0].total_like;
      $("#card-like").val(total);
      checkLikeCard(total)
    }});
  }
  function checkLikeCard(total_like){
    var portfolio_id = $("#card-id").val();
    var porfolio_type = "image";
    var url = "<?=$api['like-check'];?>";
    var data = {
      type_id : portfolio_id,
      type : porfolio_type,
      view_user : "<?php if(isset($_SESSION['userData']['id'])){ echo $_SESSION['userData']['id']; } ?>",
      auth_code : "<?php if(isset($_SESSION['userData']['id'])){ echo $_SESSION['userData']['auth_code']; } ?>"
    }
    $.ajax({url: url,data : data, success:function(result){
      var likes = result.message;
      var heart = "<?=$global['absolute-url'];?>img/heart.png";
      var heart_reds = "<?=$global['absolute-url'];?>img/heart-reds.png";
      if(likes == "Liked"){
        $('.photo-like').addClass('disabled');//disable button if already liked

        if(total_like == "1"){
          $("#cyou-like").text("You like this");
          $("#ctotal-like").hide();
          $("#clike-text").text("");
        } else if (total_like >= "1"){
          $("#cyou-like").text("You and ");
          $("#ctotal-like").show();
          $("#ctotal-like").text(eval(total_like-1))
          $("#clike-text").text("other like this")
        }

        $("#like-icon").attr("src",heart_reds);
        $("#like-icon").addClass("liked");

      } else {
        $('.photo-like').removeClass('disabled');

        $("#cyou-like").text("");
        $("#ctotal-like").text(total_like);
        $("#ctotal-like").show();
        $("#clike-text").text("Likes");

        $("#like-icon").attr("src",heart);
        $("#like-icon").removeClass("liked");

      }
    }}); 
  }
  function hitsCard(){
    var portfolio_id = $("#card-id").val();
    var porfolio_page = "portfolio";
    var porfolio_type = "image";
    var porfolio_user_id = $("#card-userid").val();
    var porfolio_user_name = $("#card-username").val();
    var portfolio_ip = "<?=getIP();?>";
    var portfolio_ip_city = "<?=$userGeoData->city;?>";
    var portfolio_ip_country = "<?=$userGeoData->country_name;?>";
    var url = "<?=$api['user-hits'];?>";
    var data = {
      type_id : portfolio_id,
      user_id : porfolio_user_id,
      username : porfolio_user_name,
      page : porfolio_page,
      type : porfolio_type,
      view_user : "<?php if(isset($_SESSION['userData']['id'])){ echo $_SESSION['userData']['id']; } ?>",
      ip : portfolio_ip,
      ip_city : portfolio_ip_city,
      ip_country : portfolio_ip_country
    }
    $.ajax({url: url,data : data, success:function(result){
      // console.log(result);
    }});
  }
  function likeCard(){
    var heart_reds = "<?=$global['absolute-url'];?>img/heart-reds.png";
    var total_like = $("#card-like").val();
    var portfolio_tag = $("#card-tag").val();
    $("#like-icon").attr("src",heart_reds);
    $("#like-icon").addClass("liked");
    $('.photo-like').addClass('disabled');

    if(total_like == "0"){
      $("#cyou-like").text("You like this");
      $("#ctotal-like").hide();
      $("#clike-text").text("");
    } else if (total_like >= "1"){
      $("#cyou-like").text("You and ");
      $("#ctotal-like").show();
      $("#ctotal-like").text(eval(total_like))
      $("#clike-text").text("other like this")
    }

    var portfolio_id = $("#card-id").val();
    var porfolio_page = "portfolio";
    var porfolio_type = "image";
    var porfolio_user_id = $("#card-userid").val();
    var porfolio_user_name = $("#card-username").val();
    var portfolio_ip = "<?=getIP();?>";
    var portfolio_ip_city = "<?=$userGeoData->city;?>";
    var portfolio_ip_country = "<?=$userGeoData->country_name;?>";
    var url = "<?=$api['like-update'];?>";
    var data = {
      type_id : portfolio_id,
      user_id : porfolio_user_id,
      username : porfolio_user_name,
      page : porfolio_page,
      type : porfolio_type,
      tag : portfolio_tag,
      view_user : "<?php if(isset($_SESSION['userData']['id'])){ echo $_SESSION['userData']['id']; } ?>",
      auth_code : "<?php if(isset($_SESSION['userData']['id'])){ echo $_SESSION['userData']['auth_code']; } ?>",
      ip : portfolio_ip,
      ip_city : portfolio_ip_city,
      ip_country : portfolio_ip_country
    }
    $.ajax({url: url,data : data, success:function(result){
      //nothing
    }});
  }
</script>