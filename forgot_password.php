<?php 
include("packages/require.php");
include("controller/controller_forgot_password.php");

$curpage='home';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title><?=$global['title-forgot_password'];?></title>
    <meta name="keywords" content="">
    <meta name="description" content="">
    <?php include("packages/head.php");?>
    <link rel="stylesheet" href="<?php echo $global['absolute-url'];?>css/swiper.min.css"/>
    <script src="<?php echo $global['absolute-url'];?>js/swiper.min.js"></script>
</head>
<body>
    <div id="all">
        <!-- start top nav -->
        <?php include("section-top-nav.php");?>
        <!-- end top nav -->
        <div class="register-section">
            <div class="register-wrapper">
                <div class="container container-max">
                    <div class="pad3 visible-xs"></div>
                    <div class="row up8">
                        <div class="col-xs-12">
                            <div class="register-header">Forgot Password ?</div>
                            <div class="register-header-child">*Please input the e-mail that is registered in our system.</div>
                        </div>
                    </div>
                    <div class="row up3">
                        <div class="col-sm-7 col-sx-12">
                            <div class="register-content">
                                <div class="rcontent-head">Change your password in three easy steps. This helps to keep your new password secure.</div>
                                <ul class="rcontent-desc">
                                    <li>Fill in your e-mail address.</li>
                                    <li>We will send a link to the e-mail contains.</li>
                                    <li>Use the code to change your password on our secure website.</li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-sm-5 col-xs-12">
                            <div class="up3 visible-xs"></div>
                            <form name="register" action="<?=$path['forgot-password-action'];?>?action=forgot" method="post" enctype="multipart/form-data" onsubmit="return validation()">
                            <div class="register-form">
                                <?php
                                if($message!=null)
                                {
                                    echo "<div id='message1' class='alert alert-info'>" . $message . "</div>";
                                }
                                ?>
                                <div class="row">
                                    <div class="col-xs-12">
                                        <div class="rform-head">Reset password with E-mail</div>
                                    </div>
                                </div>
                                <div class="row up1">
                                    <div class="col-xs-12">
                                        <input class="form-control" type="email" name="email" placeholder="e-mail that is registered in civimi" required>
                                    </div>
                                </div>
                                <div class="row up1">
                                    <div class="col-xs-12">
                                        <div class="rform-desc">By clicking forget password, you agree to Civimi <a href="#" class="terms-link">Terms</a> and <a href="#" class="terms-link">Privacy Policy</a> </div>
                                    </div>
                                </div>
                                <div class="row up3">
                                    <div class="col-xs-12">
                                        <button type="submit" class="btn btn-info btn-register btn-block">Submit</button>
                                    </div>
                                </div>
                            </div>
                            </form>
                        </div>
                    </div>
                    <div class="up3"></div>
                </div>
            </div>
        </div>
        <!-- start footer section -->
        <?php include("section-footer.php");?>
        <!-- end footer section -->
    </div><!--  end all div -->
    <script type="text/javascript">
        $("#message1").fadeOut(8400);
        function add_zindex(){
            $('.ui-autocomplete').css("z-index", 1055);
        }

        function validation(){
            var usernameValid = /^[a-zA-Z]+(([\'\,\.\- ][a-zA-Z ])?[a-zA-Z]*)*$/;
            var emailValid    = /^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/;
            var username      = register.username.value;
            var email         = register.email.value;
            // var re_email      = register.re_email.value;
            var password      = register.password.value;
            var re_password   = register.re_password.value;
            var message = '';
             
            if (username != '' && !username.match(usernameValid)) {
                message += '-Invalid username\n';
            }

            if (email !=''  && !email.match(emailValid)) {
                message += '-Invalid e-mail\n';
            }

            // if (email != re_email) {
            //     message += '-Re-Enter email does not match\n';
            // }

            if (password != re_password) {
                message += '-Re-Enter password does not match\n';
            }
             
            if (message != '') {
                alert('Sorry, there is a mistake : \n'+message);
                return false;
            }
            return true
        }

        $("#message1").fadeOut(8400);  
    </script>
    <script src="<?=$global['absolute-url-admin'];?>js/globalJS.js" type="text/javascript"></script>
</body>
</html>
