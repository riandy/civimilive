<?php 
include("packages/require.php");
include("controller/controller_global.php");
include("controller/controller_user_profile.php");
include("controller/controller_user_portfolio_detail.php");

$curpage='user_portfolio';
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title><?=$_SESSION['userData']['username']."'s ".$global['title-portfolio_image'];?></title>
  <meta name="keywords" content="">
  <meta name="description" content="">
  <meta name="robots" content="<?=$seo['robot_no'];?>">
  <?php include("packages/head.php");?>
  <link rel="stylesheet" href="<?php echo $global['absolute-url'];?>css/dropzone.css"/>
  <script src="<?php echo $global['absolute-url'];?>js/dropzone.js"></script>
  <!-- tags input !-->
  <link rel="stylesheet" type="text/css" href="<?php echo $global['absolute-url-admin'];?>packages/jQuery-Tags/jquery-ui.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo $global['absolute-url-admin'];?>packages/jQuery-Tags/jquery.tagsinput.css" />
  <script type="text/javascript" src="<?php echo $global['absolute-url-admin'];?>packages/jQuery-Tags/jquery-ui.js"></script>
  <script type="text/javascript" src="<?php echo $global['absolute-url-admin'];?>packages/jQuery-Tags/jquery.tagsinput.js"></script>
  
  <!--<style>
    @import url('http://timschlechter.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.css');
    .input-group .bootstrap-tagsinput {
        border-top-right-radius: 0;
        border-bottom-right-radius: 0;
        width: 100%;
        margin-bottom: 1px;
    }
  </style>-->
</head>
<body>
  <div id="all">
    <!-- start top nav -->
    <?php include("section-top-nav.php");?>
    <!-- end top nav -->
    <div id="user-section" class="container-section">
    
    <div class="container">
      <div id="profile-content">
        <div class="up5 visible-xs"></div>
        <!-- start fixed mobile nav --> 
        <?php $header_content = "Portfolio";  include("user_part-mobile-nav.php");?>
        <!-- end fixed mobile nav-->
        <div class="row">
            <div class="up5 visible-xs"></div>
          <!-- profile sidebar -->
          <div class="col-sm-3 col-xs-12">
            <!-- start sidebar -->
            <?php include("user_part-sidebar.php");?>
            <!-- end sidebar -->
          </div><!-- profile sidebar -->
          <div class="col-sm-9 col-xs-12 xs-pad0">
              <div id="personal-portfolio-section" class="job-sbox">
                <div id="education-header" class="user-section-header row border-bottom-eee">
                  <div class="col-xs-10">
                    <div class="profile-title hidden-xs" style="font-size:14px;"><i class="glyphicon glyphicon-picture"></i>&nbsp;&nbsp;&gt; 
                      <a href="<?=$path['user-portfolio'];?>" class="module-breadcrumb">Album</a>&nbsp;&gt;&nbsp;
                      <span id="album-nav" class="module-breadcrumb"><?=correctDisplay($data_photo[0]['Album_name']);?></span>
                    </div>
                    <div class="profile-title visible-xs" style="font-size:14px;">
                      <a href="<?=$path['user-portfolio'];?>" class="module-breadcrumb"><strong>&lt;&nbsp;&nbsp;</strong>
                      <i class="glyphicon glyphicon-picture"></i>&nbsp;
                      <span id="album-nav" class="module-breadcrumb"><?=correctDisplay($data_photo[0]['Album_name']);?></span>
                      </a>
                    </div>
                  </div>
                </div>
                <div id="portfolio-profile">
                  <div class="row">
                    <div class="col-xs-12">
                      <img src="<?=$global['img-url'].$data_photo[0]['Photo_imgLink'];?>" alt="" class="img-pdetail">
                    </div>
                  </div>
                  <div class="row up1">
                    <div class="col-sm-4 col-xs-12">
                      <div class="portfolio-label bold">Describe Photo  *</div>
                    </div>
                    <div class="col-sm-8 col-xs-12">
                      <input id="photo-title" type="text" class="form-control no-radius" placeholder="Describe your photo" value="<?=$data_photo[0]['Photo_title'];?>">
                    </div>
                  </div>
                  <div class="row up1">
                    <div class="col-sm-4 col-xs-12">
                      <div class="portfolio-label">Tag</div>
                    </div>
                    <div class="col-sm-8 col-xs-12">
                      <!--<textarea  id="photo-desc" class="form-control no-radius" rows="3" placeholder="tag here"><?=$data_tags;?></textarea>!-->
                      <!--<input id="photo-tags" type="text" class="form-control no-radius" placeholder="tag here" data-role="tagsinput" value="<?=$data_tags;?>">!-->
                      <input id="photo-tags" type="text" class="form-control no-radius" placeholder="tag here" value="<?=$data_tags;?>">
                    </div>
                  </div>
                  <?php if ($data_photo[0]['Photo_primary'] != "yes") { ?>
                  <div id="row-primary" class="row up2">
                    <div class="col-sm-4 col-xs-12"></div>
                    <div class="col-sm-8 col-xs-12">
                      <button id="primary-photo" type="button" class="btn btn-info btn-photo" onclick="update_primary();">Set as Primary</button>
                      <div id="load-primary-photo" class="edit-state hide">
                        <img src="<?=$global['absolute-url'];?>img/loadmores.gif" alt="loading">
                      </div>
                    </div>
                  </div>
                  <?php } ?>
                  <div class="up2"></div>
                  <div class="row">
                    <div class="col-xs-12">
                      <div id="alert-photo" class="alert alert-success" role="alert" style="display:none;"></div>
                    </div>
                  </div>
                  <hr/>
                  <div class="row up2">
                    <div class="col-xs-6">
                      <input id="photo-id" type="hidden" value="<?=$data_photo[0]['Photo_ID'];?>">
                      <input id="photocat-id" type="hidden" value="<?=$data_photo[0]['Album_ID'];?>">
                      <input id="photocat-title" type="hidden" value="<?=$data_photo[0]['Album_name'];?>">
                      <div class="btn-group">
                        <button id="save-photo" type="button" class="btn btn-success btn-photo">Save</button>
                        <a href="<?=$path['user-portfolio'];?>" id="cancel-photo" type="button" class="btn btn-default btn-photo" data-dismiss="modal">Cancel</a>
                      </div>
                      <div id="load-photo" class="edit-state hide">
                        <img src="<?=$global['absolute-url'];?>img/loadmores.gif" alt="loading">
                      </div>
                    </div>
                    <div class="col-xs-6 text-right">
                      <button id="delete-photo" type="button" class="btn btn-danger btn-photo">Delete</button>
                      <div id="load-delete-photo" class="edit-state hide">
                        <img src="<?=$global['absolute-url'];?>img/loadmores.gif" alt="loading">
                      </div>
                    </div>
                  </div>
                  <div class="pad1"></div>
                </div>
              </div>
          </div>
      </div>
    </div>
            
    </div>
    <!-- end center content -->
  </div>
    <!-- start footer section -->
    <?php include("section-footer.php");?>
    <!-- end footer section -->
  </div><!--  end all div -->
  <script src="<?php echo $global['absolute-url'];?>js/function.js"></script>
  <script type="text/javascript">
  //$.getScript('http://timschlechter.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.js',function(){
  //});

  $('#save-photo').on('click', function(){
    update_photo();
    update_tags();
  })
  $('#delete-photo').on('click', function(){
    delete_photo();
  })
  function update_primary(){

    var url = "<?=$api['photo-primary'];?>";
    var photo_id = $("#photo-id").val();
    var photocat_id = $("#photocat-id").val();

    $(".btn-photo").addClass("disabled");
    $("#load-primary-photo").removeClass("hide");
    var data = {
      user_id : "<?=$_SESSION['userData']['id'];?>",
      auth_code : "<?=$_SESSION['userData']['auth_code'];?>",
      album_id : photocat_id,
      primary : "yes",
      id : photo_id
    }
    $.ajax({url: url,data : data, success:function(result){
      console.log(result);
      setTimeout(function() {
      $("#row-primary").hide();
      $(".btn-photo").removeClass("disabled");
      $("#load-primary-photo").addClass("hide");
      $("#alert-photo").show();
      $("#alert-photo").text("Success set primary photo");
      $("#alert-photo").fadeOut(5400);
      }, 1500)
    }});
  }
  // update function video
  function update_photo(){

    var url = "<?=$api['photo-update'];?>";
    var photo_title = $("#photo-title").val();
    var photo_id = $("#photo-id").val();


    $(".btn-photo").addClass("disabled");
    $("#load-photo").removeClass("hide");
    var data = {
      user_id : "<?=$_SESSION['userData']['id'];?>",
      auth_code : "<?=$_SESSION['userData']['auth_code'];?>",
      id : photo_id,
      title : photo_title
    }
    $.ajax({url: url,data : data, success:function(result){
      console.log(result);
      setTimeout(function() {
          $(".btn-photo").removeClass("disabled");
          $("#load-photo").addClass("hide");
          $("#photo-title").val(photo_title);
          $("#alert-photo").show();
          $("#alert-photo").text("Success update photo");
          $("#alert-photo").fadeOut(5400);
      }, 1500)
    }});
  }//delete function video
  function update_tags(){
      var photo_tags = $("#photo-tags").val();

      var data = { 
        table_id : "<?=$O_id;?>", 
        table_name : "<?=$table_tags;?>",
        data : photo_tags
      }

      var url = "<?=$api['tag-update'];?>";
        $.ajax({type:'POST',url: url,data : data, success:function(result){
          console.log(result.message);
      }
    });
  }
  function delete_photo(){
    if (confirm("Are you sure to delete this photo ?")) {
      var url = "<?=$api['photo-delete'];?>";
      var photo_id = $("#photo-id").val();
      var photocat_id = $("#photocat-id").val();
      var photocat_title = $("#photocat-title").val();

      var cat = photocat_title;
      var name = cat;
      var regex = /\\/g;
      var replaceName = name.replace(regex, "").replace(/&quot;/g, "");
      var lowercase = replaceName.toLowerCase();
      var encodeName = lowercase.replace(/\s+/g, '-');

      $(".btn-photo").addClass("disabled");
      $("#load-delete-photo").removeClass("hide");
      var data = {
        user_id : "<?=$_SESSION['userData']['id'];?>",
        auth_code : "<?=$_SESSION['userData']['auth_code'];?>",
        id : photo_id
      }
      $.ajax({url: url,data : data, success:function(result){
        console.log(result);
        setTimeout(function() {
          window.location = "<?=$path['user-portfolio'];?>"+photocat_id+"_"+encodeName+".html";
        }, 1500)
      }});
    }
  }
  function profileNav(){
    $('#profile-arrow').toggleClass('nav-close');
  }
  //autocomplete tagsinput
  $('#photo-tags').tagsInput({
        'width':'auto',
        'autocomplete_url' : '<?=$api["tag-autocomplete"];?>',
        'autocomplete':{selectFirst:true,width:'80px',autoFill:true}
  });
  </script>
</body>
</html>
