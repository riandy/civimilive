<html>
<head>
</head>
<body style="margin:0;">
<div id='all' style="width:600px;">
<div id='header' style="text-align:center;padding:20px 0;border-bottom: solid 2px #B5B5B5;margin-bottom: 30px;">
<a target='_blank' href='http://www.civimi.com'> 
<img src='http://www.civimi.com/beta/civimi/img/logo/logo-desktop.png' style='width:200px;' alt='civimi'>
</a>
</div>
<div id="content" style="padding: 0 15px;">
Hi ".$username."!<br><br>
Apakah ".$username." tau, mengapa data pendidikan merupakan bagian terpenting dalam membuat sebuah resume?<br><br>
Saya akan sharing sedikit mengenai apa yang dilihat perusahaan terhadap resume seorang pelamar kerja, terutama pada bagian pendidikan.<br><br>
Pendidikan di jaman modern ini berperan penting untuk menunjukan derajat atau status sosial seseorang. Perusahaan beranggapan bahwa lembaga pendidikan telah mempersiapkan calon pekerja yang sudah dibekali dengan pengetahuan mengenai dunia kerja. 
Lembaga pendidikan yang sudah ".$username." lewati berperan pentinga dalam menentukan ke arah mana perbekalan ".$username." setelah keluar dari lembaga tersebut. Hal inilah yang dilihat perusahaan, untuk menentukan latar belakang ".$username." dalam mencari kerja.<br><br>
Saya berikan contoh ceritanya:<br>
Misalkan ".$username." adalah seorang lulusan di bidang komunikasi, pastinya ".$username." tertarik untuk menekuni bidang ini dan lebih banyak belajar prakteknya lagi saat bekerja, 
mencoba menerapkan dalam dunia kerja sesungguhnya. Namun pekerjaan yang ".$username." lamar adalah perusahaan IT. Perusahaan IT ini akan melihat latar belakang ".$username." dan akan 
menempatkan ".$username." pada posisi yang tepat agar selain dapat mengembangkan perusahaan tapi juga dapat membuat ".$username." berkembang pada bidang ".$username." sendiri.<br><br>
Masih banyak lagi contoh-contoh pentingnya data pendidikan dengan peluang bekerja atau bahkan bekerja sama di luar sana. ".$username." dapat mengunjungi <a target='_blank' href='http://www.civimi.com/beta/blog/'>www.civimi.com/beta/blog</a> untuk mendapatkan tips-tips lainnya.<br><br>
Lengkapi Resume ".$username." : <a target='_blank' href='http://www.civimi.com/beta/edit/'>Klik disini</a><br><br><br>
Salam,<br><br>
<u>Billy Gani</u><br>
Customer Success Manager<br>
Civimi.com
</div>
<div id='footer' style='background-color: #666;padding: 10px 0;margin-top:20px;'>
<div style='display: block;height: 50px;padding: 0 20px;position: relative;'>
<div style='font-size: 20px;font-style: italic;color: #FFFFFF;font-weight: bold;display: block;width: 50%;float: left;'>'Reinvent Yourself'</div>
<div style='display: block;float: left;width: 50%;position: relative;text-align: right;'>
<a target='_blank' href='https://www.facebook.com/civimi'><img src='http://www.civimi.com/beta/civimi/img/logo/facebook.png' alt='facebook' style='width: 20px;'></a>
<a target='_blank' href='https://plus.google.com/+Civimi'><img src='http://www.civimi.com/beta/civimi/img/logo/google.png' alt='google' style='width: 20px;margin-left:8px;'></a>
<a target='_blank' href='https://twitter.com/civimi'><img src='http://www.civimi.com/beta/civimi/img/logo/twitter.png' alt='twitter' style='width: 20px;margin-left:8px;'></a>
</div>
</div>
<div style='text-align:center;font-size: 13px;color: #FFFFFF;margin:20px 0 0 0;padding:0 20px;position:relative;'>
Copyright &copy; 2015 Civimi. All rights reserved.
<div style='position: absolute;right: 20px;top: 0;'><a target='_blank' href="" style="font-size:12px;color: #FFFFFF;outline:none !important;text-decoration:underline;">unsubscribe</a></div>
</div>
</div>
</div>
</body>
</html>
