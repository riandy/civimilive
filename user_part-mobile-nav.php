<div class="row">
  <div class="col-xs-12 pad0-xs mobile-nav-section">
    <div class="profile-header hidden-xs"><?=$header_content;?></div>
    <div class="row visible-xs mobile-nav-pad">
    <div class="col-xs-4 mobile-nav-link text-center border-right-ccc"><a href="<?=$path['user-edit'];?>"><span class="glyphicon glyphicon-user"></span>&nbsp;<br class="visible-xs" />&nbsp;PROFILE</a></div>
      <div class="col-xs-4 mobile-nav-link text-center border-right-ccc"><a class="active" href="<?=$path['user-portfolio'];?>"><span class="glyphicon glyphicon-folder-open"></span>&nbsp;<br class="visible-xs" />&nbsp;PORTFOLIO</a></div>
      <div class="col-xs-4 mobile-nav-link text-center"><a  href="<?=$path['user-setting'];?>"><span class="glyphicon glyphicon-cog"></span>&nbsp;<br class="visible-xs" />&nbsp;SETTINGS</a></div>
    </div>
  </div>
</div>
<div class="row mobile-portfolio-nav">
  <div class="col-xs-12 pad0-xs">
    <div class="row visible-xs">
      <div class="col-xs-3 mobile-nav-link text-center border-right-ccc"><a <?php if($curpage == 'user_portfolio') {?>class="active"<?php } ?> href="<?=$path['user-portfolio'];?>"><span class="glyphicon glyphicon-picture"></span>&nbsp;<br class="visible-xs" />Image</a></div>
      <div class="col-xs-3 mobile-nav-link text-center border-right-ccc"><a <?php if($curpage == 'user_video') {?> class="active"<?php } ?>  href="<?=$path['user-video'];?>"><span class="glyphicon glyphicon-expand"></span>&nbsp;<br class="visible-xs" />Video</a></div>
      <div class="col-xs-3 mobile-nav-link text-center border-right-ccc"><a <?php if($curpage == 'user_document') {?> class="active"<?php } ?>  href="<?=$path['user-doc'];?>"><span class="glyphicon glyphicon-duplicate"></span>&nbsp;<br class="visible-xs" />Document</a></div>
      <div class="col-xs-3 mobile-nav-link text-center"><a <?php if($curpage == 'user_web') {?> class="active"<?php } ?> href="<?=$path['user-web'];?>"><span class="glyphicon glyphicon-globe"></span>&nbsp;<br class="visible-xs" />Apps</a></div>
    </div>
  </div>
</div>