<?php 
include("packages/require.php");
include("controller/controller_global.php");
$curpage = '404';
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title><?=$global['title-404'];?></title>
  <meta name="keywords" content="">
  <meta name="description" content="">
  <meta name="robots" content="<?=$seo['robot_no'];?>">
  <?php include("packages/head.php");?>
  </head>
  <body>
    <div id="all">
      <!-- start top nav -->
      <div style="border-bottom:solid 1px #ECECEC;">
      <?php include("section-top-nav.php");?>
      </div>
      <!-- end top nav -->
      <div id="section-404">
        
        <div class="container container-max">
          <div id="content-404">
            <div class="ops-404">OOPS</div>
            <div class="text-404">404</div>
            <div class="note-404">Halaman yang anda cari tidak ditemukan. Silahkan kembali ke <a href="<?=$global['base'];?>">awal</a></div>
            <div class="note-404">Page that you are looking for does not exist. Please go back to <a href="<?=$global['base'];?>">home</a></div>
          </div>
        </div>
        
      </div>
      <!-- end center content -->
      <!-- start footer section -->
      <?php include("section-footer.php");?>
      <!-- end footer section -->
    </div><!--  end all div -->
    <script src="<?php echo $global['absolute-url'];?>js/function.js"></script>
  </body>
  </html>
