<?php 
include("packages/require.php");
include("controller/controller_global.php");
include("controller/controller_user_video.php");

$curpage='user_video';
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title><?=$_SESSION['userData']['username']."'s ".$data_video[0]['VideoCat_name']." - ".$global['title-portfolio_video'];?></title>
  <meta name="keywords" content="">
  <meta name="description" content="">
  <meta name="robots" content="<?=$seo['robot_no'];?>">
  <?php include("packages/head.php");?>
  <link rel="stylesheet" href="<?php echo $global['absolute-url'];?>css/dropzone.css"/>
  <script src="<?php echo $global['absolute-url'];?>js/dropzone.js"></script>
  <script src="<?php echo $global['absolute-url'];?>js/function.js"></script>
</head>
<body>
  <div id="all">
    <!-- start top nav -->
    <?php include("section-top-nav.php");?>
    <!-- end top nav -->
    <div id="user-section" class="container-section">
    
    <div class="container">
      <div id="profile-content">
        <div class="up5 visible-xs"></div>
        <!-- start fixed mobile nav -->
        <?php $header_content = "Video";  include("user_part-mobile-nav.php");?>
        <!-- end fixed mobile nav-->
        <div class="row">
                <div class="up5 visible-xs"></div>
          <!-- profile sidebar -->
          <div class="col-sm-3 col-xs-12">
            <!-- start sidebar -->
            <?php include("user_part-sidebar.php");?>
            <!-- end sidebar -->
          </div><!-- profile sidebar -->
          <div class="col-sm-9 col-xs-12 pad0-xs">
              <div id="personal-portfolio-section" class="job-sbox">
                <div id="education-header" class="user-section-header row border-bottom-eee">
                  <div class="col-xs-10">
                    <div class="profile-title hidden-xs" style="font-size:14px;"><i class="glyphicon glyphicon-facetime-video"></i>&nbsp;&nbsp;
                      <a href="<?=$path['user-video'];?>" class="module-breadcrumb">Video</a> &gt;
                      <span id="folder-nav" class="module-breadcrumb"> <?=$data_video[0]['VideoCat_name'];?></span>
                    </div>
                    <div class="profile-title visible-xs" style="font-size:14px;">
                      <a id="folder-back" href="<?=$path['user-video'];?>" class="module-breadcrumb">&lt;
                      &nbsp;<i class="glyphicon glyphicon-facetime-video"></i>&nbsp;
                      <span id="folder-nav" class="module-breadcrumb"><?=charLength($data_video[0]['VideoCat_name'],12);?></span>
                      </a>
                    </div>
                  </div>
                  <div class="col-xs-2">
                      <div class="edit-button text-right">
                        <a href="javascript:;" class="edit-portfolio" title="show edit folder" data-toggle='modal' data-target='#modal-folder'><i class="glyphicon glyphicon-pencil"></i></a>
                      </div>
                  </div>
                </div>
                <!-- <div class="add-photo-bar">
                  <a href="#modal-video" class="add-photo-portfolio" data-toggle='modal' data-target='#modal-video'><i class="glyphicon glyphicon-facetime-video"></i> Add Video</a>
                </div> -->
                <div id="portfolio-profile">
                  <div id="video-list" class="row">
                  </div>
                </div>
              </div>
          </div>
      </div>
    </div>
            
    </div>
    <!-- end center content -->
  </div>
    <!-- start footer section -->
    <?php include("section-footer.php");?>
    <!-- end footer section -->
  </div><!--  end all div -->
  <!-- Modal Edit Folder -->
  <div class="modal fade" id="modal-folder" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content" style="border-radius:0;">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="font-size: 35px;position: absolute;right: 10px;top: 3px;"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel" style="text-align: center;font-size: 20px;font-weight: bold;color: #717171;">Edit Album</h4>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-sm-4 col-xs-12">
              <div class="portfolio-label bold">Title *</div>
            </div>
            <div class="col-sm-8 col-xs-12">
              <input id="folder-title" type="text" class="form-control no-radius" placeholder="name of the video" value="<?=$data_video[0]['VideoCat_name'];?>">
            </div>
          </div>
          <div class="row up1">
            <div class="col-sm-4 col-xs-12">
              <div class="portfolio-label">Description</div>
            </div>
            <div class="col-sm-8 col-xs-12">
              <textarea  id="folder-desc" class="form-control" rows="3" placeholder="tell story about this video"><?=$data_video[0]['VideoCat_desc'];?></textarea>
            </div>
          </div>
        </div>
        <div class="modal-footer" style="text-align:left;">
          <input id="folder-id" type="hidden" value="<?=$data_video[0]['VideoCat_ID'];?>">
          <div class="btn-group">
            <button id="save-folder" type="button" class="btn btn-primary btn-sfolder">Save</button>
            <button id="cancel-video" type="button" class="btn btn-default btn-sfolder" data-dismiss="modal">Cancel</button>
          </div>
          <div id="mload-folder" class="edit-state hide">
            <img src="<?=$global['absolute-url'];?>img/loadmores.gif" alt="loading">
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- Modal Add Video -->
  <div class="modal fade" id="modal-video" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content" style="border-radius:0;">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="font-size: 35px;position: absolute;right: 10px;top: 3px;"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel" style="text-align: center;font-size: 20px;font-weight: bold;color: #717171;">Add New Video</h4>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-sm-4 col-xs-12">
              <div class="portfolio-label bold">Video Folder *</div>
            </div>
            <div class="col-sm-8 col-xs-12">
              <select id="video-folder" class="form-control no-radius">
              </select>
            </div>
          </div>
          <div class="row up1">
            <div class="col-sm-4 col-xs-12">
              <div class="portfolio-label bold">Video Name *</div>
            </div>
            <div class="col-sm-8 col-xs-12">
              <input id="video-title" type="text" class="form-control no-radius" placeholder="name of the video">
            </div>
          </div>
          <div class="row up1">
            <div class="col-sm-4 col-xs-12">
              <div class="portfolio-label">Description</div>
            </div>
            <div class="col-sm-8 col-xs-12">
              <textarea  id="video-desc" class="form-control no-radius" rows="3" placeholder="tell story about this video"></textarea>
            </div>
          </div>
          <div class="row">
            <div class="col-xs-12">
              <div id="videoInstruction">
              <h3>Video embedding instructions</h3>
              &lt;iframe width="640" height="360" src="<span id="videoInstructionText">http://www.youtube.com/embed/ tyEpaPEbjzI?rel=0</span>" frameborder="0" allowfullscreen&gt;&lt;/iframe&gt;&nbsp;&nbsp;<br><h5>(copy the highlighted link from your embedded iframe into the below box)</h5>
              </div>
            </div>
          </div>
          <div class="row up15">
            <div class="col-sm-4 col-xs-12">
              <div class="portfolio-label bold">Video Link *</div>
            </div>
            <div class="col-sm-8 col-xs-12">
              <input id="video-link" type="text" class="form-control no-radius" placeholder="i.e. http://youtu.be/c1v1m1">
              <div class="video-notice">*Paste your online video links here, i.e. http://youtu.be/c1v1m1</div>
            </div>
          </div>
        </div>
        <div class="modal-footer" style="text-align:left;">
          <div class="btn-group">
            <button id="save-video" type="button" class="btn btn-success btn-video">Save</button>
            <button id="cancel-video" type="button" class="btn btn-default btn-video" data-dismiss="modal">Cancel</button>
          </div>
          <div id="load-video" class="edit-state hide">
            <img src="<?=$global['absolute-url'];?>img/loadmores.gif" alt="loading">
          </div>
      </div>
    </div>
  </div>
  <script src="<?php echo $global['absolute-url'];?>js/function.js"></script>
  
  <script type="text/javascript">
  function get_data_category(){
    var cat = $('#video-folder option:selected').text();
    var desc = $('#video-folder option:selected').data('desc');
    $('#folder-nav').text(cat);
    $('#folder-title').val(cat);
    $('#folder-desc').val(desc);
  }
  get_folder();
  function get_folder(){
    var url = "<?=$api['videocat-data'];?>&user_id=<?php echo $_SESSION['userData']['id'];?>&auth_code=<?php echo $_SESSION['userData']['auth_code'];?>";                            
    $.ajax({url: url,success:function(result){
    $('#video-folder').html(replace_folder(result.data));
    }});
  }
  function replace_folder(datas){
    var folder_id = $("#folder-id").val();
    var resultHTML='';
        resultHTML += "<option value=''>Choose folder</option>";
      if(datas != null){
        var obj = datas;    
        for(var i=0;i < obj.length;i++){
          if(folder_id == obj[i].VideoCat_ID){
            var selected = "selected=selected";
          } else {
            var selected = "";
          }
          resultHTML += "<option value='"+obj[i].VideoCat_ID+"' "+selected+" data-desc='"+obj[i].VideoCat_desc+"'>"+obj[i].VideoCat_name+"</option>";
        }
      }
    return resultHTML;
  }
  get_video();
  function get_video(){
    var folder_id = $("#folder-id").val();
    var url = "<?=$api['video-data'];?>&user_id=<?php echo $_SESSION['userData']['id'];?>&auth_code=<?php echo $_SESSION['userData']['auth_code'];?>&videocat_id="+folder_id;                            
    $.ajax({url: url,success:function(result){
    $('#video-list').html(replace_video(result.data));
    }});
  }
  function replace_video(datas){
    var resultHTML = '';
    resultHTML += "<div class='col-sm-3 col-xs-6 col-photo-potfolio'>";
    resultHTML += "<div class='portfolio-content down2'>";
    resultHTML += "<a href='#modal-video' class='add-photo-portfolio' data-toggle='modal' data-target='#modal-video'>";
    resultHTML += "<img class='video-img' src='<?=$global['absolute-url'];?>img/add-video.png' title='add video' alt='add video' />";
    resultHTML += "<div class='portfolio-name'>Add Video</div>";
    resultHTML += "</a>";
    resultHTML += "</div>";
    resultHTML += "</div>";
    if(datas != null){
    
    var obj = datas;    
    for(var i=0;i < obj.length;i++){
    var name = obj[i].Video_title;
    var regex = /\\/g;
    var replaceName = name.replace(regex, "").replace(/&quot;/g, "");
    var lowercase = replaceName.toLowerCase();
    var encodeName = lowercase.replace(/\s+/g, '-');

    //generta thumbnail
    var iframe_src       = obj[i].Video_link;
    var youtube_video_id = parseVideoID(iframe_src);
    var thumbnail = "<?=$global['absolute-url'];?>img/placeholder-video.png";
    if (youtube_video_id.length == 11) {
        var thumbnail = "//img.youtube.com/vi/"+youtube_video_id+"/0.jpg";
    }

    resultHTML += "<div id='col-vthmb-"+obj[i].Video_ID+"' class='col-sm-3 col-xs-6 col-photo-potfolio'>";
    resultHTML += "<div class='portfolio-content'>";
    resultHTML += "<a href='<?=$path['user-video-detail'];?>"+obj[i].Video_ID+"_"+encodeName+".html' class='portfolio-link' title='"+obj[i].Video_title+"'>";
    resultHTML += "<div class='portfolio-thmb'>";
    resultHTML += "<img src='"+thumbnail+"' alt='"+obj[i].Video_title+"' class='video-img'></div>";
    resultHTML += "<div class='portfolio-name hidden-xs hidden-sm'>"+charString(obj[i].Video_title,12)+"</div>";
    resultHTML += "<div class='portfolio-name visible-xs visible-sm'>"+charString(obj[i].Video_title,5)+"</div>";
    resultHTML += "</a>";
    resultHTML += "</div>";
    resultHTML += "</div>";
 
    } 
  }

    return resultHTML;
  }
  $('#save-folder').on('click', function(){
    update_folder();
  })
  $('#save-video').on('click', function(){
    save_video();
  })
  // update function video
  function save_video(){

    var url = "<?=$api['video-insert'];?>";
    var video_title = $("#video-title").val();
    var video_desc = $("#video-desc").val();
    var video_link = $("#video-link").val();
    var video_catid = $("#video-folder").val();
    var cat = $('#video-folder option:selected').text();
    var name = cat;
    var regex = /\\/g;
    var replaceName = name.replace(regex, "").replace(/&quot;/g, "");
    var lowercase = replaceName.toLowerCase();
    var encodeName = lowercase.replace(/\s+/g, '-');

    $(".btn-video").addClass("disabled");
    $("#load-video").removeClass("hide");
    var data = {
      user_id : "<?=$_SESSION['userData']['id'];?>",
      auth_code : "<?=$_SESSION['userData']['auth_code'];?>",
      videocat_id : video_catid,
      title : video_title,
      desc : video_desc,
      link : video_link
    }
    $.ajax({url: url,data : data, success:function(result){
      console.log(result);
      setTimeout(function() {
          $("#folder-id").val(video_catid);
          $('#modal-video').modal('hide');
          $(".btn-video").removeClass("disabled");
          $("#load-video").addClass("hide");
          $("#video-title").val("");
          $("#video-desc").val("");
          $("#video-link").val("");
          get_video();
          get_data_category();
          get_folder();
          var newlinks = "<?=$path['user-video-folder'];?>"+video_catid+"_"+encodeName+".html";
          window.history.pushState('civimi', 'civimi', newlinks);
      }, 1500)
    }});
  }
  // update function folder
  function update_folder(){

    var url = "<?=$api['videocat-update'];?>";
    var folder_title = $("#folder-title").val();
    var folder_desc = $("#folder-desc").val();
    var folder_id = $("#folder-id").val();
    $(".btn-sfolder").addClass("disabled");
    $("#mload-folder").removeClass("hide");

    var data = {
      user_id : "<?=$_SESSION['userData']['id'];?>",
      auth_code : "<?=$_SESSION['userData']['auth_code'];?>",
      id : folder_id,
      name : folder_title,
      desc : folder_desc
    }
    $.ajax({url: url,data : data, success:function(result){
      console.log(result);
      setTimeout(function() {
        $('#modal-folder').modal('hide');
        $(".btn-sfolder").removeClass("disabled");
        $("#mload-folder").addClass("hide");
      }, 1500)
      $("#folder-title").val(folder_title);
      $("#folder-desc").val(folder_desc);
      $("#folder-nav").text(folder_title);
    }});
  }
  function profileNav(){
    $('#profile-arrow').toggleClass('nav-close');
  }
  </script>
</body>
</html>
