<?php 
include("packages/require.php");
include("controller/controller_global.php");
include("controller/controller_user_document_detail.php");

$docStat = null;
if(isset($_SESSION['docstat'])){
  $docStat = $_SESSION['docstat'];
  unset($_SESSION['docstat']);
}

$curpage='user_doc';
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title><?=$_SESSION['userData']['username']."'s ".$data_doc[0]['Doc_title']." - ".$global['title-portfolio_doc'];?></title>
  <meta name="keywords" content="">
  <meta name="description" content="">
  <meta name="robots" content="<?=$seo['robot_no'];?>">
  <?php include("packages/head.php");?>
  <link rel="stylesheet" href="<?php echo $global['absolute-url'];?>css/dropzone.css"/>
  <script src="<?php echo $global['absolute-url'];?>js/dropzone.js"></script>
  <!-- Add fancyBox main JS and CSS files -->
  <link rel="stylesheet" type="text/css" href="<?php echo $global['absolute-url'];?>js/fbox/source/jquery.fancybox.css?v=2.1.5" media="screen" />
  <style>

input[type="file"] {
    display: none;
}
  </style>
</head>
<body>
  <div id="all">
    <!-- start top nav -->
    <?php include("section-top-nav.php");?>
    <!-- end top nav -->
    <div id="user-section" class="container-section">
    
    <div class="container"> 
      <div id="profile-content">
        <div class="up5 visible-xs"></div>
        <!-- start fixed mobile nav -->
        <?php $header_content = "Document"; include("user_part-mobile-nav.php");?>
        <!-- end fixed mobile nav-->
        <div class="row">
        <div class="up5 visible-xs"></div>
          <!-- profile sidebar -->
          <div class="col-sm-3 col-xs-12">
            <!-- start sidebar -->
            <?php include("user_part-sidebar.php");?>
            <!-- end sidebar -->
          </div><!-- profile sidebar -->
          <div class="col-sm-9 col-xs-12 xs-pad0">
              <div id="personal-portfolio-section" class="job-sbox">
                <div id="education-header" class="user-section-header row border-bottom-eee">
                  <div class="col-xs-12">
                    <div class="profile-title hidden-xs" style="font-size:14px;"><i class="glyphicon glyphicon-file"></i>&nbsp;&nbsp;
                      <a href="<?=$path['user-doc'];?>" class="module-breadcrumb">Document</a> >
                      <a id="folder-name" href="<?=$path['user-doc-folder'];?><?=$data_doc[0]['DocCat_ID'];?>_<?=encode($data_doc[0]['DocCat_name']);?>.html" class="module-breadcrumb"><?=$data_doc[0]['DocCat_name'];?></a> > 
                      <span id="doc-name" class="module-breadcrumb"><?=$data_doc[0]['Doc_title'];?></span>
                    </div>
                    <div class="profile-title visible-xs" style="font-size:14px;"><i class="glyphicon glyphicon-facetime-doc"></i>&nbsp;&nbsp;
                      <a id="folder-back" href="<?=$path['user-doc-folder'];?><?=$data_doc[0]['DocCat_ID'];?>_<?=encode($data_doc[0]['DocCat_name']);?>.html" class="module-breadcrumb">< Back</a>
                    </div>
                  </div>
                </div>
                <div id="portfolio-profile">
                  <form method="post" id="myForm" action="#" enctype="multipart/form-data" onsubmit="return update_doc()" class="hidden-xs">
                  <div class="row">
                    <div class="col-md-9 col-sm-8 col-xs-12">
                      <div class="row up2">
                        <div class="col-sm-4 col-xs-12">
                          <div class="portfolio-label bold">Folder *</div>
                        </div>
                        <div class="col-sm-8 col-xs-12">
                          <select id="doc-folder" class="form-control no-radius">
                          </select>
                        </div>
                      </div>
                      <div class="row up1">
                        <div class="col-sm-4 col-xs-12">
                          <div class="portfolio-label bold">Title *</div>
                        </div>
                        <div class="col-sm-8 col-xs-12">
                          <input id="doc-title" name="name" type="text" class="form-control no-radius" placeholder="name of the document" value="<?=$data_doc[0]['Doc_title'];?>">
                        </div>
                      </div>
                      <div class="row up1">
                        <div class="col-sm-4 col-xs-12">
                          <div class="portfolio-label">Description</div>
                        </div>
                        <div class="col-sm-8 col-xs-12">
                          <textarea id="doc-desc" name="desc" class="form-control no-radius" rows="3" placeholder="tell about this document"><?=$data_doc[0]['Doc_desc'];?></textarea>
                        </div>
                      </div>
                      <div class="row up1">
                        <div class="col-sm-4 col-xs-12">
                          <div class="portfolio-label">Availability</div>
                        </div>
                        <div class="col-sm-8 col-xs-12">
                          <div class="doc-date"><?=date("d M Y", strtotime($data_doc[0]['Doc_create_date']));?></div>
                        </div>
                      </div>
                      <div class="row up1">
                        <div class="col-sm-4 col-xs-5">
                          <div class="portfolio-label">Public</div>
                        </div>
                        <div class="col-sm-8 col-xs-7">
                          <div class="onoffswitch" style="float: initial;">
                              <input type="checkbox" name="publish" class="onoffswitch-checkbox" id="doc-public" <?php if($data_doc[0]['Doc_publish'] == "Publish"){echo "checked";}?> value="Publish">
                              <label class="onoffswitch-label" for="doc-public">
                                  <span class="onoffswitch-inner"></span>
                                  <span class="onoffswitch-switch"></span>
                              </label>
                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-sm-4 col-xs-12"></div>
                        <div class="col-sm-8 col-xs-12">
                          <div class="portfolio-label bold">*Document is <?php if($data_doc[0]['Doc_publish'] != "Publish"){echo "not";}?> open to public</div>
                        </div>
                      </div>
                      <div class="up2">
                        <?=$helper['doc-upload'];?>
                      </div>
                      <div class="row">
                        <div class="col-xs-12">
                          <?php if (isset($docStat)) {?>
                            <div id="alert-doc" class="alert alert-success" role="alert"><?php echo $docStat;?></div>
                          <?php } ?>
                        </div>
                      </div>
                      </div>
                      <div class="col-md-3 col-sm-4 col-xs-12">
                        <div class="row up2">
                          <div class="col-xs-12 text-center">
                            <div class="document-img">
                              <img src="<?php echo $global['absolute-url'];?>img/file.png" alt="file">
                              <div class="text-center">
                              <a id="read-link" href="<?php echo $global['img-url'];?><?php echo $data_doc[0]['Doc_fileLoc'];?>" target="_blank" class="read-file btn btn-info"><i class="glyphicon glyphicon-search"></i>Read</a>
                              </div>
                            </div>
                            <label for="doc-file" class="custom-file-upload up3">
                                <i class="glyphicon glyphicon-cloud-upload"></i> Re-Upload
                            </label>
                            <input id="doc-file" type="file" name="file" class="reUpload" />
                          </div>
                        </div>
                      </div>
                    </div>
                    <hr/>
                    <div class="row up2">
                      <div class="col-xs-12">
                        <input id="doc-id" name="id" type="hidden" value="<?=$data_doc[0]['Doc_ID'];?>">
                        <input id="doccat-id" name="doccat_id" type="hidden" value="<?=$data_doc[0]['DocCat_ID'];?>">
                        <input id="doc-file-loc" name="doc_file" type="hidden" value="<?=$data_doc[0]['Doc_fileLoc'];?>">
                        <div class="btn-group">
                          <button id="save-doc" type="button" class="btn btn-success btn-doc">Save</button>
                          <a href="<?=$path['user-doc-folder'];?><?=$data_doc[0]['DocCat_ID'];?>_<?=encode($data_doc[0]['DocCat_name']);?>.html" id="cancel-doc" type="button" class="btn btn-default btn-doc" data-dismiss="modal">Cancel</a>
                          <button id="delete-doc" type="button" class="btn btn-danger btn-doc">Delete</button>
                        </div>
                        <div id="load-doc" class="edit-state hide">
                          <img src="<?=$global['absolute-url'];?>img/loadmores.gif" alt="loading">
                        </div>
                      </div>
                    </div>
                    <div class="pad1"></div>
                    </form>

                    <form method="post" id="myForm2" action="#" enctype="multipart/form-data" onsubmit="return update_doc2()" class="visible-xs">
                    <div class="row">
                        <div class="col-md-3 col-sm-4 col-xs-12">
                          <div class="row up2">
                            <div class="col-xs-12 text-center">
                              <div class="document-img">
                                <img src="<?php echo $global['absolute-url'];?>img/file.png" alt="file">
                                <div class="text-center">
                                <a id="read-link2" href="<?php echo $global['img-url'];?><?php echo $data_doc[0]['Doc_fileLoc'];?>" target="_blank" class="read-file btn btn-info"><i class="glyphicon glyphicon-search"></i>Read</a>
                                </div>
                              </div>
                              <label for="doc-file2" class="custom-file-upload up3">
                                  <i class="glyphicon glyphicon-cloud-upload"></i> Re-Upload
                              </label>
                              <input id="doc-file2" type="file" name="file" class="reUpload" />
                              <br/><br/><?=$helper['doc-upload'];?>
                            </div>
                          </div>
                        </div>
                      <div class="col-md-9 col-sm-8 col-xs-12">
                        <div class="row up2">
                          <div class="col-sm-4 col-xs-12">
                            <div class="portfolio-label bold">Folder *</div>
                          </div>
                          <div class="col-sm-8 col-xs-12">
                            <select id="doc-folder2" class="form-control no-radius">
                            </select>
                          </div>
                        </div>
                        <div class="row up2">
                          <div class="col-sm-4 col-xs-12">
                            <div class="portfolio-label bold">Title *</div>
                          </div>
                          <div class="col-sm-8 col-xs-12">
                            <input id="doc-title2" name="name" type="text" class="form-control no-radius" placeholder="name of the document" value="<?=$data_doc[0]['Doc_title'];?>">
                          </div>
                        </div>
                        <div class="row up1">
                          <div class="col-sm-4 col-xs-12">
                            <div class="portfolio-label">Description</div>
                          </div>
                          <div class="col-sm-8 col-xs-12">
                            <textarea id="doc-desc2" name="desc" class="form-control no-radius" rows="3" placeholder="tell about this document"><?=$data_doc[0]['Doc_desc'];?></textarea>
                          </div>
                        </div>
                        <div class="row up1">
                          <div class="col-sm-4 col-xs-5">
                            <div class="portfolio-label bold">Available Since</div>
                          </div>
                          <div class="col-sm-8 col-xs-7">
                            <div class="doc-date"><?=date("d M Y", strtotime($data_doc[0]['Doc_create_date']));?></div>
                          </div>
                        </div>
                        <div class="row up1">
                          <div class="col-sm-4 col-xs-5">
                            <div class="portfolio-label bold">Public</div>
                          </div>
                          <div class="col-sm-8 col-xs-7">
                            <div class="onoffswitch" style="float: initial;">
                                <input type="checkbox" name="publish" class="onoffswitch-checkbox" id="doc-public2" <?php if($data_doc[0]['Doc_publish'] == "Publish"){echo "checked";}?> value="Publish">
                                <label class="onoffswitch-label" for="doc-public2">
                                    <span class="onoffswitch-inner"></span>
                                    <span class="onoffswitch-switch"></span>
                                </label>
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-sm-4 col-xs-12"></div>
                          <div class="col-sm-8 col-xs-12">
                            <div class="portfolio-label"><small class="text-primary">*Document is <?php if($data_doc[0]['Doc_publish'] != "Publish"){echo "not";}?> open to public</small></div>
                          </div>
                        </div>
                        
                        <div class="up2"></div>
                        <div class="row">
                          <div class="col-xs-12">
                            <?php if (isset($docStat)) {?>
                            <div id="alert-doc2" class="alert alert-success" role="alert"><?php echo $docStat;?></div>
                            <?php } ?>
                          </div>
                        </div>
                        </div>
                      </div>
                      <hr/>
                      <div class="row up2">
                        <div class="col-xs-12">
                          <input id="doc-id2" name="id" type="hidden" value="<?=$data_doc[0]['Doc_ID'];?>">
                          <input id="doccat-id2" name="doccat_id" type="hidden" value="<?=$data_doc[0]['DocCat_ID'];?>">
                          <input id="doc-file-loc2" name="doc_file" type="hidden" value="<?=$data_doc[0]['Doc_fileLoc'];?>">
                          <div class="btn-group">
                            <button id="save-doc2" type="button" class="btn btn-success btn-doc">Save</button>
                            <a href="<?=$path['user-doc-folder'];?><?=$data_doc[0]['DocCat_ID'];?>_<?=encode($data_doc[0]['DocCat_name']);?>.html" id="cancel-doc" type="button" class="btn btn-default btn-doc" data-dismiss="modal">Cancel</a>
                            <button id="delete-doc2" type="button" class="btn btn-danger btn-doc">Delete</button>
                          </div>
                          <div id="load-doc2" class="edit-state hide">
                            <img src="<?=$global['absolute-url'];?>img/loadmores.gif" alt="loading">
                          </div>
                        </div>
                      </div>
                      <div class="pad1"></div>
                      </form>

                </div>
              </div>
          </div>
      </div>
    </div>
            
    </div>
    <!-- end center content -->
  </div>
    <!-- start footer section -->
    <?php include("section-footer.php");?>
    <!-- end footer section -->
  </div><!--  end all div -->
  <script src="<?php echo $global['absolute-url'];?>js/function.js"></script>
  <script type="text/javascript" src="<?php echo $global['absolute-url'];?>js/fbox/source/jquery.fancybox.js?v=2.1.5"></script>
  <script type="text/javascript">

  $(document).ready(function() {
    <?php if ($docStat != null) {?>
    $("#alert-doc").fadeOut(8400);
    $("#alert-doc2").fadeOut(8400);
    <?php } ?>
    $(".read-file").fancybox({
    openEffect: 'elastic',
    closeEffect: 'elastic',
    autoSize: true,
    type: 'iframe',
    iframe: {
    preload: false // fixes issue with iframe and IE
    }
    });
  });
  $('#save-doc').on('click', function(){
    update_doc();
  })
  $('#delete-doc').on('click', function(){
    delete_doc();
  })
  $('#save-doc2').on('click', function(){
    update_doc2();
  })
  $('#delete-doc2').on('click', function(){
    delete_doc2();
  })
  get_folder();
  function get_folder(){
    var url = "<?=$api['doccat-data'];?>&user_id=<?php echo $_SESSION['userData']['id'];?>&auth_code=<?php echo $_SESSION['userData']['auth_code'];?>";                            
    $.ajax({url: url,success:function(result){
    $('#doc-folder').html(replace_folder(result.data));
    $('#doc-folder2').html(replace_folder(result.data));
    }});
  }
  function replace_folder(datas){
    var doccat_id = $("#doccat-id").val();
    var resultHTML='';
        resultHTML += "<option value=''>Choose folder</option>";
      if(datas != null){
        var obj = datas;    
        for(var i=0;i < obj.length;i++){
          if(doccat_id == obj[i].DocCat_ID){
            var selected = "selected=selected";
          } else {
            var selected = "";
          }
          resultHTML += "<option value='"+obj[i].DocCat_ID+"' "+selected+">"+obj[i].DocCat_name+"</option>";
        }
      }
    return resultHTML;
  }
  function update_doc(){ 
    var doc_title = $("#doc-title").val();
    var doc_desc = $("#doc-desc").val();
    var doc_id = $("#doc-id").val();

    var name = doc_title;
    var regex = /\\/g;
    var replaceName = name.replace(regex, "").replace(/&quot;/g, "");
    var lowercase = replaceName.toLowerCase();
    var encodeName = lowercase.replace(/\s+/g, '-');

    var formdata = new FormData();      
    var file = $('#doc-file')[0].files[0];
    formdata.append('FILE', file);
    $.each($('#myForm').serializeArray(), function(a, b){
      formdata.append(b.name, b.value);
    });
    $.ajax({url: '<?php echo $global['absolute-url'];?>uploadDocument.php?action=update_doc',
      data: formdata,
      processData: false,
      contentType: false,
      type: 'POST',
      beforeSend: function(){
        // add event or loading animation
        $(".btn-doc").addClass("disabled");
        $("#load-doc").removeClass("hide");
      },
      success: function(ret) {
        console.log(ret); // get return (if success) here
        setTimeout(function() {
          window.location = "<?=$path['user-doc-detail'];?>"+doc_id+"_"+encodeName+".html";
          $(".btn-doc").removeClass("disabled");
          $("#load-doc").addClass("hide");
        }, 1500)
      }
    });
  }
  //delete function doc
  function delete_doc(){
    if (confirm("Are you sure to delete this document ?")) {
      var doc_id = $("#doc-id").val();
      var doccat_id = $("#doccat-id").val();

      var cat = $('#doc-folder option:selected').text();
      var name = cat;
      var regex = /\\/g;
      var replaceName = name.replace(regex, "").replace(/&quot;/g, "");
      var lowercase = replaceName.toLowerCase();
      var encodeName = lowercase.replace(/\s+/g, '-');

      var formdata = new FormData();      
      var file = $('#doc-file')[0].files[0];
      formdata.append('FILE', file);
      $.each($('#myForm').serializeArray(), function(a, b){
        formdata.append(b.name, b.value);
      });
      $.ajax({url: '<?php echo $global['absolute-url'];?>uploadDocument.php?action=delete_doc',
        data: formdata,
        processData: false,
        contentType: false,
        type: 'POST',
        beforeSend: function(){
          // add event or loading animation
          $(".btn-doc").addClass("disabled");
          $("#load-doc").removeClass("hide");
        },
        success: function(ret) {
          console.log(ret); // get return (if success) here
          setTimeout(function() {
            window.location = "<?=$path['user-doc-folder'];?>"+doccat_id+"_"+encodeName+".html";
            $(".btn-doc").removeClass("disabled");
            $("#load-doc").addClass("hide");
          }, 1500)
        }
      });  
    }
  }
  function update_doc2(){ 
    var doc_title = $("#doc-title2").val();
    var doc_desc = $("#doc-desc2").val();
    var doc_id = $("#doc-id2").val();

    var name = doc_title;
    var regex = /\\/g;
    var replaceName = name.replace(regex, "").replace(/&quot;/g, "");
    var lowercase = replaceName.toLowerCase();
    var encodeName = lowercase.replace(/\s+/g, '-');

    var formdata = new FormData();      
    var file = $('#doc-file2')[0].files[0];
    formdata.append('FILE', file);
    $.each($('#myForm2').serializeArray(), function(a, b){
      formdata.append(b.name, b.value);
    });
    $.ajax({url: '<?php echo $global['absolute-url'];?>uploadDocument.php?action=update_doc',
      data: formdata,
      processData: false,
      contentType: false,
      type: 'POST',
      beforeSend: function(){
        // add event or loading animation
        $(".btn-doc").addClass("disabled");
        $("#load-doc2").removeClass("hide");
      },
      success: function(ret) {
        console.log(ret); // get return (if success) here
        setTimeout(function() {
          window.location = "<?=$path['user-doc-detail'];?>"+doc_id+"_"+encodeName+".html";
          $(".btn-doc").removeClass("disabled");
          $("#load-doc2").addClass("hide");
        }, 1500)
      }
    });
  }
  //delete function doc
  function delete_doc2(){
    if (confirm("Are you sure to delete this document ?")) {

      var doc_id = $("#doc-id2").val();
      var doccat_id = $("#doccat-id2").val();

      var cat = $('#doc-folder2 option:selected').text();
      var name = cat;
      var regex = /\\/g;
      var replaceName = name.replace(regex, "").replace(/&quot;/g, "");
      var lowercase = replaceName.toLowerCase();
      var encodeName = lowercase.replace(/\s+/g, '-');

      var formdata = new FormData();      
      var file = $('#doc-file2')[0].files[0];
      formdata.append('FILE', file);
      $.each($('#myForm2').serializeArray(), function(a, b){
        formdata.append(b.name, b.value);
      });
      $.ajax({url: '<?php echo $global['absolute-url'];?>uploadDocument.php?action=delete_doc',
        data: formdata,
        processData: false,
        contentType: false,
        type: 'POST',
        beforeSend: function(){
          // add event or loading animation
          $(".btn-doc").addClass("disabled");
          $("#load-doc2").removeClass("hide");
        },
        success: function(ret) {
          console.log(ret); // get return (if success) here
          setTimeout(function() {
            window.location = "<?=$path['user-doc-folder'];?>"+doccat_id+"_"+encodeName+".html";
            $(".btn-doc").removeClass("disabled");
            $("#load-doc2").addClass("hide");
          }, 1500)
        }
      });  
    }
  }
  </script>
</body>
</html>
