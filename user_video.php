<?php 
include("packages/require.php");
include("controller/controller_global.php");
if(!isset($_SESSION['userData']['id'])){
  $message = "You must login first!";
  $_SESSION['statusLogin'] = $message;
  header("Location:{$path['user-login-failed']}");
}
$curpage='user_video';
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title><?=$_SESSION['userData']['username']."'s ".$global['title-portfolio_video'];?></title>
  <meta name="keywords" content="">
  <meta name="description" content="">
  <meta name="robots" content="<?=$seo['robot_no'];?>">
  <?php include("packages/head.php");?>
  <link rel="stylesheet" href="<?php echo $global['absolute-url'];?>css/dropzone.css"/>
  <script src="<?php echo $global['absolute-url'];?>js/dropzone.js"></script>
</head>
<body>
  <div id="all">
    <!-- start top nav -->
    <?php include("section-top-nav.php");?>
    <!-- end top nav -->
    <div id="user-section">
    
    <div class="container">
      <div id="profile-content">
        <div class="up5 visible-xs"></div>
        <!-- start fixed mobile nav -->
        <?php $header_content = "Video";  include("user_part-mobile-nav.php");?>
        <!-- end fixed mobile nav-->
        <div class="row">
        <div class="up5 visible-xs"></div>
          <!-- profile sidebar -->
          <div class="col-sm-3 col-xs-12">
            <!-- start sidebar -->
            <?php include("user_part-sidebar.php");?>
            <!-- end sidebar -->
          </div><!-- profile sidebar -->
          <div class="col-sm-9 col-xs-12 pad0-xs">
              <div id="personal-portfolio-section" class="job-sbox">
                <div id="education-header" class="user-section-header row border-bottom-eee">
                  <div class="col-xs-8">
                    <div class="profile-title"><i class="glyphicon glyphicon-facetime-video"></i>&nbsp;&nbsp;Video</div>
                  </div>
                  <div class="col-xs-4">
                      <div class="edit-button text-right">
                        <a href="javascript:;" class="trash-portfolio" title="show remove album"><i class="glyphicon glyphicon-trash"></i></a>
                        <a href="javascript:;" class="done-portfolio hide" title="done remove album"><i class="glyphicon glyphicon-ok"></i> Done</a>
                      </div>
                  </div>
                </div>
                <div id="portfolio-profile">
                  <div id="album-list" class="row"></div>
                </div>
              </div>
          </div>
      </div>
    </div>
            
    </div>
    <!-- end center content -->
  </div>
    <!-- start footer section -->
    <?php include("section-footer.php");?>
    <!-- end footer section -->
  </div><!--  end all div -->
  <!-- Modal -->
  <div class="modal fade" id="modal-portfolio" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content" style="border-radius:0;">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="font-size: 35px;position: absolute;right: 10px;top: 3px;"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel" style="text-align: center;font-size: 20px;font-weight: bold;color: #717171;">Create New Folder</h4>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-sm-4 col-xs-12">
              <div class="portfolio-label bold">Title *</div>
            </div>
            <div class="col-sm-8 col-xs-12">
              <input id="album-title" type="text" class="form-control no-radius" placeholder="name of the folder">
            </div>
          </div>
          <div class="row up1">
            <div class="col-sm-4 col-xs-12">
              <div class="portfolio-label">Description</div>
            </div>
            <div class="col-sm-8 col-xs-12">
              <textarea  id="album-desc" class="form-control" rows="3" placeholder="tell story about this folder"></textarea>
            </div>
          </div>
        </div>
        <div class="modal-footer" style="text-align:left;">
          <div class="btn-group">
            <button id="save-album" type="button" class="btn btn-success btn-album">Save</button>
            <button id="cancel-album" type="button" class="btn btn-default btn-album" data-dismiss="modal">Cancel</button>
          </div>
          <div id="load-album" class="edit-state hide">
            <img src="<?=$global['absolute-url'];?>img/loadmores.gif" alt="loading">
          </div>
        </div>
      </div>
    </div>
  </div>
  <script src="<?php echo $global['absolute-url'];?>js/function.js"></script>
  <script type="text/javascript">
  $('.trash-portfolio').on('click', function(){
    $('.remove-album').removeClass('hide');
    $('.trash-portfolio').addClass('hide');
    $('.done-portfolio').removeClass('hide');
  });
  $('.done-portfolio').on('click', function(){
    $('.remove-album').addClass('hide');
    $('.done-portfolio').addClass('hide');
    $('.trash-portfolio').removeClass('hide');
  });
  function resetDelete(){
    $('.remove-album').addClass('hide');
    $('.done-portfolio').addClass('hide');
    $('.trash-portfolio').removeClass('hide');
  }
  function imageHover(){
    $('#create-img').attr('src',"<?=$global['absolute-url'];?>img/add-video2.png");
  }
  function imageBack(){
    $('#create-img').attr('src',"<?=$global['absolute-url'];?>img/add-video.png");
  }
  $('#save-album').on('click', function(){
    save_album();
  })
  get_album();
  function get_album(){
    var url = "<?=$api['videocat-data'];?>&user_id=<?php echo $_SESSION['userData']['id'];?>&auth_code=<?php echo $_SESSION['userData']['auth_code'];?>";                            
    $.ajax({url: url,success:function(result){
    resetDelete();
    $('#album-list').html(replace_album(result.data));
    }});
  }
  function replace_album(datas){
    var resultHTML='';
        resultHTML += "<div class='col-sm-3 col-xs-6 col-portfolio'>";
        resultHTML += "<a href='#modal-portfolio' class='add-portfolio' onmouseover='imageHover();' onmouseout='imageBack();' data-toggle='modal' data-target='#modal-portfolio'>";
        resultHTML += "<div class='portfolio-thmb text-center'><img id='create-img' src='<?=$global['absolute-url'];?>img/add-video.png' alt='create album' class='portfolio-img'></div>";
        resultHTML += "<div class='portfolio-name'>Create Folder</div>";
        resultHTML += "</a>";
        resultHTML += "</div>";
      if(datas != null){
        var obj = datas;    
        for(var i=0;i < obj.length;i++){
        var name = obj[i].VideoCat_name;
        var regex = /\\/g;
        var replaceName = name.replace(regex, "").replace(/&quot;/g, "");
        var lowercase = replaceName.toLowerCase();
        var encodeName = lowercase.replace(/\s+/g, '-');

        resultHTML += "<div id='album-"+obj[i].VideoCat_ID+"' data-album_name='"+obj[i].VideoCat_name+"' class='col-sm-3 col-xs-6 col-portfolio'>";
        resultHTML += "<div class='portfolio-content'>";
        resultHTML += "<a href='javascript:;' class='remove-album hide' onclick='removeAlbum("+obj[i].VideoCat_ID+");' title='delete folder'><i class='glyphicon glyphicon-remove'></i></a>";
        resultHTML += "<a href='<?=$path['user-video-folder'];?>"+obj[i].VideoCat_ID+"_"+encodeName+".html' class='portfolio-link' title='"+obj[i].VideoCat_name+"'>";
        resultHTML += "<div id='pthmb-"+obj[i].VideoCat_ID+"' class='portfolio-thmb'><img src='<?=$global['absolute-url'];?>img/placeholder-video.png' alt='primary of "+obj[i].VideoCat_name+"' class='portfolio-img'></div>";
        resultHTML += "<div class='portfolio-name hidden-xs hidden-sm'>"+charString(obj[i].VideoCat_name,12)+"</div>";
        resultHTML += "<div class='portfolio-name visible-xs visible-sm'>"+charString(obj[i].VideoCat_name,5)+"</div>";
        resultHTML += "</a>";
        resultHTML += "</div>";
        resultHTML += "</div>";
        }
      }
    return resultHTML;
  }
  function profileNav(){
    $('#profile-arrow').toggleClass('nav-close');
  }
  function emptyAddAlbum(){
    $("#album-title").val("");
    $("#album-desc").val("");
  }
  //delete function
  function removeAlbum(album_id){
    var name = $('#album-'+album_id).data('album_name');
    if (confirm("Are you sure to delete \'"+name+"\' ?")) { 

      $('#pthmb-'+album_id).append("<img src='<?=$global['absolute-url'];?>img/loader.gif' class='remove-loader' />");
      
      var url = "<?=$api['videocat-delete'];?>";
      var data = {
        user_id : "<?=$_SESSION['userData']['id'];?>",
        auth_code : "<?=$_SESSION['userData']['auth_code'];?>",
        id : album_id
      }
      $.ajax({url: url,data : data, success:function(result){
        console.log(result);
        setTimeout(function() {
          $('#album-'+album_id).remove();
        }, 1500)
      }});

    }
  }
  // save function
  function save_album(){
    $(".btn-album").addClass("disabled");
    $("#load-album").removeClass("hide");

    var url = "<?=$api['videocat-insert'];?>";
    var album_title = $("#album-title").val();
    var album_desc = $("#album-desc").val();

    var data = {
      user_id : "<?=$_SESSION['userData']['id'];?>",
      auth_code : "<?=$_SESSION['userData']['auth_code'];?>",
      name : album_title,
      desc : album_desc
    }
    $.ajax({url: url,data : data, success:function(result){
      console.log(result);
      setTimeout(function() {
        get_album();
        emptyAddAlbum();
        $('#modal-portfolio').modal('hide');
        $(".btn-album").removeClass("disabled");
        $("#load-album").addClass("hide");
      }, 1500)
    }});
  }
  </script>
</body>
</html>
