<?php 
include("packages/require.php");
include("controller/controller_global.php");
include("controller/controller_user_profile.php");

$curpage='user_profile';
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title><?=$_SESSION['userData']['username']."'s ".$global['title-profile'];?></title>
  <meta name="keywords" content="">
  <meta name="description" content="">
  <meta name="robots" content="<?=$seo['robot_no'];?>">
  <?php include("packages/head.php");?>
  <link rel="stylesheet" href="<?php echo $global['absolute-url'];?>css/dropzone.css"/>
  <script src="<?php echo $global['absolute-url'];?>js/dropzone.js"></script>
  <link href="<?php echo $global['absolute-url'];?>css/bootstrap-tour.min.css" rel="stylesheet">
  <script src="<?php echo $global['absolute-url'];?>js/bootstrap-tour.min.js"></script>
  <script>
  $(function() { 
    $( "#profile-wrapper" ).sortable({
      placeholder: "ui-state-highlight",
      forcePlaceholderSize: true,
      update: function( event, ui ) {
        saveSort();
      }
  })});

  function saveSort(){
    var element = $('.job-sbox');
    for(var ab = 0; ab < element.length; ab++){
      element.eq(ab).attr("data-order", (ab+1));
      console.log(ab+": "+(ab+1));
    }
        // console.log(element);

        var order = new Array(
          $('#personal-summary-section').attr('data-order'), 
          $('#personal-education-section').attr('data-order'),
          $('#personal-work-section').attr('data-order'),
          $('#personal-skill-section').attr('data-order'),
          $('#personal-cert-section').attr('data-order'),
          $('#personal-lang-section').attr('data-order')
          );
        // console.log(order);
        var data = {
          data : order
        }

        var url = "<?=$api['user-sort'];?>&user_id=<?=$_SESSION['userData']['id'];?>&auth_code=<?=$_SESSION['userData']['auth_code'];?>";
        $.ajax({type:'POST',url: url,data : data, success:function(result){
          // console.log(result);
        }
      });
        // save_sort();
      }
    </script>
  </head>
  <body>
    <div id="all">
      <!-- start top nav -->
      <?php include("section-top-nav.php");?>
      <!-- end top nav -->
      <div id="user-section">
        
        <div class="container">
          <div id="profile-content">
            <div class="up5 visible-xs"></div>
            <div class="row">
              <div class="col-xs-12 pad0-xs mobile-nav-section">
                <div class="profile-header hidden-xs"><?=$lang['nav-profil'];?></div>
                <div class="row visible-xs mobile-nav-pad">
                  <div class="col-xs-4 mobile-nav-link text-center border-right-ccc"><a class="active" href="<?=$path['user-edit'];?>"><span class="glyphicon glyphicon-user"></span>&nbsp;<br class="visible-xs" />&nbsp;<?=strtoupper($lang['nav-profil']);?></a></div>
                  <div class="col-xs-4 mobile-nav-link text-center border-right-ccc"><a href="<?=$path['user-portfolio'];?>"><span class="glyphicon glyphicon-folder-open"></span>&nbsp;<br class="visible-xs" />&nbsp;<?=strtoupper($lang['nav-portfolio']);?></a></div>
                  <div class="col-xs-4 mobile-nav-link text-center"><a href="<?=$path['user-setting'];?>"><span class="glyphicon glyphicon-cog"></span>&nbsp;<br class="visible-xs" />&nbsp;<?=strtoupper($lang['nav-setting']);?></a></div>
                </div>
              </div>
            </div>
            <div class="row">
              <!-- profile sidebar -->
              <div class="col-sm-3 col-xs-12">
                <!-- start sidebar -->
                <?php include("user_part-sidebar.php");?>
                <!-- end sidebar -->
              </div><!-- profile sidebar -->
              <div id="profile-container" class="pad0 col-sm-9 col-xs-12">
                <!-- start personal -->
                <?php include("user_part-personal.php");?>
                <!-- end personal -->

                <div id="profile-wrapper" class="up1">

                  <!-- start sort order -->
                  <?php foreach ($data_sort as $data) {
                    include ("{$data['page_name']}");
                  } ?>
                  <!-- end sort order -->

                </div>

              </div>
            </div>
          </div>
        </div>
        
      </div>
      <!-- end center content -->
      <!-- start footer section -->
      <?php include("section-footer.php");?>
      <!-- end footer section -->
      <button class="btn btn-warning" onclick="restartButton()">Restart Personal</button>
    </div><!--  end all div -->
    <script src="<?php echo $global['absolute-url'];?>js/function.js"></script>
    <script type="text/javascript">
    $(window).on("load", function () {
      var urlHash = window.location.href.split("#")[1];
      $('html,body').animate({
        scrollTop: $('#' + urlHash).offset().top-42
      }, 1000);
    });

    // TOUR
    var tour = new Tour({
      onEnd: function (tour) {
        $("#edit-personal").removeClass("disabled");
        $("#btn-update-personal").removeClass("disabled");
        $("#btn-cancel-personal").removeClass("disabled");
        $(".summary-box").removeClass("disabled");
        $("#btn-update-summary").removeClass("disabled");
        $("#btn-cancel-summary").removeClass("disabled");
        $(".education-box").removeClass("disabled");
        $("#add-education").removeClass("disabled");
        $(".work-box").removeClass("disabled");
        $("#add-work").removeClass("disabled");
        $(".skill-box").removeClass("disabled");
        $("#add-skill").removeClass("disabled");
        $(".language-box").removeClass("disabled");
        $("#add-language").removeClass("disabled");
        $(".cert-box").removeClass("disabled");
        $("#add-cert").removeClass("disabled");
      },
      steps: [
      {
        element: "#edit-personal",
        title: "<div class='tour-title'><?=$tour['personal-1-title'];?></div>",
        content: "<?=$tour['personal-1-content'];?>",
        placement: "left",
        backdrop: true,
        onShow: function (tour) {
          $("#edit-personal").addClass("disabled");
        },
        onNext: function (tour) {
          if($("#personal-edit-box").hasClass("hide")){
            $("#personal-profile").addClass("hide");
            $("#personal-edit-box").removeClass("hide");
          }
        },
      },
      {
        element: "#personal-form-edit",
        title: "<div class='tour-title'><?=$tour['personal-2-title'];?></div>",
        content: "<?=$tour['personal-2-content'];?>",
        placement: "bottom",
        backdrop: true,
        onShow: function (tour) {
          $("#btn-update-personal").addClass("disabled");
          $("#btn-cancel-personal").addClass("disabled");
        },
        onPrev: function (tour) {
          $("#personal-edit-box").addClass("hide");
          $("#personal-profile").removeClass("hide");
        },
      },
      {
        element: "#drop-personal",
        title: "<div class='tour-title'><?=$tour['personal-3-title'];?></div>",
        content: "<?=$tour['personal-3-content'];?>",
        placement: "bottom",
        backdrop: true,
        onNext: function (tour) {
          $("#personal-edit-box").addClass("hide");
          $("#personal-profile").removeClass("hide");
        },
      },
      {
        element: "#personal-summary-section",
        title: "<div class='tour-title'><?=$tour['summary-1-title'];?></div>",
        content: "<?=$tour['summary-1-content'];?>",
        placement: "top",
        backdrop: true,
        onShow: function (tour) {
          $(".summary-box").addClass("disabled");
        },
        onPrev: function (tour) {
          if($("#personal-edit-box").hasClass("hide")){
            $("#personal-profile").addClass("hide");
            $("#personal-edit-box").removeClass("hide");
          }
        },
        onNext: function (tour) {
          if($(".summary-edit-box").hasClass("hide")){
            $(".summary-box").addClass("hide");
            $(".summary-edit-box").removeClass("hide");
          }
        },
      },
      {
        element: "#summary-edit-box",
        title: "<div class='tour-title'><?=$tour['summary-2-title'];?></div>",
        content: "<?=$tour['summary-2-content'];?>",
        placement: "top",
        backdrop: true,
        onShow: function (tour) {
          $("#btn-update-summary").addClass("disabled");
          $("#btn-cancel-summary").addClass("disabled");
        },
        onPrev: function (tour) {
          $(".summary-edit-box").addClass("hide");
          $(".summary-box").removeClass("hide");
        },
        onNext: function (tour) {
          $(".summary-edit-box").addClass("hide");
          $(".summary-box").removeClass("hide");
        },
      },
      {
        element: "#personal-education-section",
        title: "<div class='tour-title'><?=$tour['edu-1-title'];?></div>",
        content: "<?=$tour['edu-1-content'];?>",
        placement: "top",
        backdrop: true,
        onPrev: function (tour) {
          if($(".summary-edit-box").hasClass("hide")){
            $(".summary-box").addClass("hide");
            $(".summary-edit-box").removeClass("hide");
          }
        },
        onShow: function (tour) {
          $(".education-box").addClass("disabled");
          $("#add-education").addClass("disabled");
        },
      },
      {
        element: "#add-education",
        title: "<div class='tour-title'><?=$tour['edu-2-title'];?></div>",
        content: "<?=$tour['edu-2-content'];?>",
        placement: "left",
        backdrop: true,
      },
      {
        element: "#personal-work-section",
        title: "<div class='tour-title'><?=$tour['work-1-title'];?></div>",
        content: "<?=$tour['work-1-content'];?>",
        placement: "top",
        backdrop: true,
        onShow: function (tour) {
          $(".work-box").addClass("disabled");
          $("#add-work").addClass("disabled");
        },
      },
      {
        element: "#add-work",
        title: "<div class='tour-title'><?=$tour['work-2-title'];?></div>",
        content: "<?=$tour['work-2-content'];?>",
        placement: "left",
        backdrop: true,
      },
      {
        element: "#personal-skill-section",
        title: "<div class='tour-title'><?=$tour['skill-1-title'];?></div>",
        content: "<?=$tour['skill-1-content'];?>",
        placement: "top",
        backdrop: true,
        onShow: function (tour) {
          $(".skill-box").addClass("disabled");
          $("#add-skill").addClass("disabled");
        },
      },
      {
        element: "#add-skill",
        title: "<div class='tour-title'><?=$tour['skill-2-title'];?></div>",
        content: "<?=$tour['skill-2-content'];?>",
        placement: "left",
        backdrop: true,
      },
      {
        element: "#personal-lang-section",
        title: "<div class='tour-title'><?=$tour['lang-1-title'];?></div>",
        content: "<?=$tour['lang-1-content'];?>",
        placement: "top",
        backdrop: true,
        onShow: function (tour) {
          $(".language-box").addClass("disabled");
          $("#add-language").addClass("disabled");
        },
      },
      {
        element: "#add-language",
        title: "<div class='tour-title'><?=$tour['lang-2-title'];?></div>",
        content: "<?=$tour['lang-2-content'];?>",
        placement: "left",
        backdrop: true,
      },
      {
        element: "#personal-cert-section",
        title: "<div class='tour-title'><?=$tour['cert-1-title'];?></div>",
        content: "<?=$tour['cert-1-content'];?>",
        placement: "top",
        backdrop: true,
        onShow: function (tour) {
          $(".cert-box").addClass("disabled");
          $("#add-cert").addClass("disabled");
        },
      },
      {
        element: "#add-cert",
        title: "<div class='tour-title'><?=$tour['cert-2-title'];?></div>",
        content: "<?=$tour['cert-2-content'];?>",
        placement: "left",
        backdrop: true,
      }
    ]});
    tour.init();
    // tourPersonal.restart();
    tour.start();
    //END TOUR

    function restartButton(){
      tour.restart();
    }
    </script>
  </body>
  </html>
