<?php 
include("packages/require.php");
include("controller/controller_form_goal.php");
$curpage='form_personal';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title><?=$global['title-form_goal'];?></title>
    <meta name="keywords" content="">
    <meta name="description" content="">
    <?php include("packages/head.php");?>
  <link rel="stylesheet" href="<?php echo $global['absolute-url'];?>css/dropzone.css"/>
  <script src="<?php echo $global['absolute-url'];?>js/dropzone.js"></script>
</head>
<body>
    <div id="all">
        <!-- start top nav -->
        <?php include("section-top-nav.php");?>
        <!-- end top nav -->
        <?php if(isset($_SESSION['userData']['id']) && ($datas[0]['User_step_goal'] == "No" || $datas[0]['User_step_goal'] == "")){ ?>
        <div class="register-section" style="background: url('<?=$global['base'];?>admin/img/pattern/pattern.png');">
            <div class="register-wrapper" style="background-color: transparent;">
                <div class="container container-max">
                    <div class="up7 visible-xs"></div>
                    <div class="row">
                        <div class="col-xs-12">
                            <!-- <div class="fgoal-welcome">Hello, <?php if($datas[0]['User_fname'] != ""){echo $datas[0]['User_fname']." ".$datas[0]['User_lname'];}else{echo $datas[0]['User_username'];}?>,</div> -->
                            <div class="fgoal-info"><?=$lang['fgoal-welcome'];?></div>
                            <div class="fgoal-info2"><?=$lang['fgoal-info'];?></div>
                        </div>
                    </div>
                    <form name="formPersonal" action="<?=$path['form-goal-action'];?>" method="post" enctype="multipart/form-data" onsubmit="return goalValid()" style="max-width: 500px;margin: 0 auto;">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="job-level">
                                    <ul id="level-list">
                                        <label class="radio-inline radio-inline-goal">
                                          <input <?php if($datas[0]['User_interest'] == "1"){echo "checked";}?> class="radio-goal" type="radio" name="goal" value="1">
                                          <div class="checked-goal"><?=$lang['fgoal-opportunity'];?></div>
                                        </label>
                                        <label class="radio-inline radio-inline-goal">
                                          <input <?php if($datas[0]['User_interest'] == "2"){echo "checked";}?> class="radio-goal" type="radio" name="goal" value="2"> 
                                          <div class="checked-goal"><?=$lang['fgoal-freelance'];?></div>
                                        </label>
                                        <label class="radio-inline radio-inline-goal">
                                          <input <?php if($datas[0]['User_interest'] == "3"){echo "checked";}?> class="radio-goal" type="radio" name="goal" value="3"> 
                                          <div class="checked-goal"><?=$lang['fgoal-fulltime'];?></div>
                                        </label>
                                        <label class="radio-inline radio-inline-goal">
                                          <input <?php if($datas[0]['User_interest'] == "4"){echo "checked";}?> class="radio-goal" type="radio" name="goal" value="4"> 
                                          <div class="checked-goal"><?=$lang['fgoal-internship'];?></div>
                                        </label>
                                        <label class="radio-inline radio-inline-goal">
                                          <input <?php if($datas[0]['User_interest'] == "5"){echo "checked";}?> class="radio-goal" type="radio" name="goal" value="5"> 
                                          <div class="checked-goal"><?=$lang['fgoal-company'];?></div>
                                        </label> 
                                        <label class="radio-inline radio-inline-goal">
                                          <input <?php if($datas[0]['User_interest'] == "6"){echo "checked";}?> class="radio-goal" type="radio" name="goal" value="6"> 
                                          <div class="checked-goal"><?=$lang['fgoal-education'];?></div>
                                        </label>         
                                        <label class="radio-inline radio-inline-goal">
                                          <input <?php if($datas[0]['User_interest'] == "7"){echo "checked";}?> class="radio-goal" type="radio" name="goal" value="7"> 
                                          <div class="checked-goal"><?=$lang['fgoal-college'];?></div>
                                        </label>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12"><div id="alert-goal" class="control-alert text-center up1"></div></div>
                        </div>
                        <div class="row up2">
                            <div class="col-xs-12 text-right">
                                <input type="hidden" name="id" value="<?php echo $_SESSION['userData']['id'];?>">
                                <input type="hidden" id="profile-img">
                                <button type="submit" class="btn btn-info btn-fpersonal"><?=$lang['fgoal-next'];?> <i class="glyphicon glyphicon-chevron-right"></i></button>
                            </div>
                        </div>
                    </form>
                    <div class="up3"></div>
                </div>
            </div>
        </div>
        <?php }else{ ?>
        <div class="register-section" style="background: url('<?=$global['base'];?>admin/img/pattern/pattern.png');">
            <div class="register-wrapper" style="background-color: transparent;">
                <div class="container container-max">
                    <div class="up7 visible-xs"></div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="fgoal-welcome">Not Connected</div>
                        </div>
                    </div>
                    <div class="up3"></div>
                </div>
            </div>
        </div>
        <?php } ?>
        <!-- start footer section -->
        <?php include("section-footer.php");?>
        <!-- end footer section -->
    </div><!--  end all div -->
    <script type="text/javascript">
        function goalValid(){
            var r = document.getElementsByName("goal")
            var c = -1
            for(var i=0; i < r.length; i++){
               if(r[i].checked) {
                  c = i; 
               }
            }
            if (c == -1){ 
                $("#alert-goal").text("you must choose one of your goal!");
                $("#alert-goal").show();
                return false;
            } else {
                $("#alert-goal").text("");
                $("#alert-goal").hide();
            }
            return true;
        }
    </script>
  <script src="<?php echo $global['absolute-url'];?>js/function.js"></script>
<script src="<?=$global['absolute-url-admin'];?>js/globalJS.js" type="text/javascript"></script>
</body>
</html>
