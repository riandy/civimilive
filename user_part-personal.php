<?php if($data_user[0]['User_bg_img'] != "") { ?>
<div id="personal-profile" class="bg-fixed" style="background-image:url(<?=$data_user[0]['User_bg_img'];?>);">
<?php } else { ?>
<div id="personal-profile" class="bg-fixed" style="background-image:url(<?=$global['absolute-url'];?>img/bg-2.jpg);">
<?php } ?>
    <div class="overlay">
    </div>
  <div class="edit-section-personal edit-button text-right pad1">
    <a id="edit-background" href="javascript:;" style="float:left;outline:none;" data-toggle="modal" data-target="#modal-bg">
      <i class="glyphicon glyphicon glyphicon-picture"></i><span class="hidden-xs"> Change</span> Background
    </a>
    <a id="edit-personal" href="javascript:;" onclick="personal_toggle();">
      <i class="glyphicon glyphicon-edit"></i> Edit
    </a>
  </div>
  <div class="profile-job"><?php if($data_user[0]['User_bioDesc'] != ''){echo $data_user[0]['User_bioDesc'];}else{ echo $lang['personal-empty'];}?></div>
  <div class="row" style="padding: 0 0 30px 0;">
    <div class="col-sm-2 col-xs-4 up3">
      <div class="profile-photo">
        <img id="img-profile" src="<?php echo check_image_url($data_user[0]['User_proPhotoThmb']);?>" alt="Resume picture of <?=$data_user[0]['User_fname']." ".$data_user[0]['User_lname'];?> in CIVIMI">  
      </div>
    </div>
    <div class="col-sm-10 col-xs-8 up3 pad0-xs">
      <div class="profile-name"><?=$data_user[0]['User_fname']." ".$data_user[0]['User_lname'].", ".calculate_age($data_user[0]['User_DOBday']."-".$data_user[0]['User_DOBmonth']."-".$data_user[0]['User_DOB']);?></div>
      <div class="profile-address"><span class="glyphicon glyphicon-home"></span>&nbsp;&nbsp;<?=$data_user[0]['User_city'].", ".$data_user[0]['User_state'].", ".$data_user[0]['User_country'];?></div>
      <div class="profile-email"><span class="glyphicon glyphicon-envelope"></span>&nbsp;&nbsp;<?=$data_user[0]['User_email'];?></div>
      <div class="profile-phone"><span class="glyphicon glyphicon-earphone"></span>&nbsp;&nbsp;<?=$data_user[0]['User_phone'];?></div>
    </div>
  </div>
</div>

<div id="personal-loader" class="text-center hide" style="padding:20px;border:1px solid #ccc;background-color:#fff;">
  <img src="<?=$global['absolute-url'];?>img/load-blue.gif" />
</div>


<div id="personal-edit-box" class="hide" style="background:#eee;">
  <div id="personal-header" class="user-section-header row">
    <div class="col-sm-9 col-xs-10">
      <div class="profile-title"><?=$lang['personal-title'];?></div>
    </div>
    <div class="col-sm-3 col-xs-2">
      <div class="profile-title text-right"><a href="javascript:;" onclick="personal_toggle();"><span class="glyphicon glyphicon-remove"></span> <span class="hidden-xs">Close</span></a></div>
    </div>
  </div>
  <div id="personal-edit" class="pad1">
    <div class="row">
      <div id="personal-form-edit" class="col-md-8 col-sm-8 col-xs-12">
        <div class="row">
          <div class="col-xs-12 pad0">
            <span class="label-title"><?=$lang['personal-fname'];?></span>
          </div>
          <div class="col-md-8 col-sm-9 col-xs-12 pad0">
            <input id="fname" autocomplete="off" type="text" class="form-control no-radius" placeholder="<?=$lang['personal-fname_placeholder'];?>" value="<?=$data_user[0]['User_fname'];?>"/>
            <div id="valid-fname" class="valid-message"></div>
          </div>
        </div>

        <div class="row up1">
          <div class="col-xs-12 pad0">
            <span class="label-title"><?=$lang['personal-lname'];?></span>
          </div>
          <div class="col-md-8 col-sm-9 col-xs-12 pad0">
            <input id="lname" autocomplete="off" type="text" class="form-control no-radius" placeholder="<?=$lang['personal-lname_placeholder'];?>" value="<?=$data_user[0]['User_lname'];?>"/>
            <div id="valid-lname" class="valid-message"></div>
          </div>
        </div>

        <div class="row up1">
          <div class="col-xs-12 pad0">
            <span class="label-title"><?=$lang['personal-date'];?></span>
          </div>
          <div class="col-md-2 col-sm-3 col-xs-4" style="padding-left:0;">
            <select id="day" class="form-control no-radius" style="padding:6px;">
              <option value="">day</option>
              <?php for($dd=1;$dd <= 31;$dd++){?>
              <option value="<?=$dd;?>" <?php if($data_user[0]['User_DOBday'] == $dd){echo "selected";};?> ><?=$dd;?></option>
              <?php } ?>
            </select>
          </div>
          <div class="col-md-2 col-sm-3 col-xs-4 pad0">
            <select id="month" class="form-control no-radius" style="padding:6px;">
              <option value="">month</option>
              <?php $arr_month = array("Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec");for($mm=1;$mm <= 12;$mm++){?>
              <option value="<?=$mm;?>" <?php if($data_user[0]['User_DOBmonth'] == $mm){echo "selected";};?>><?=$arr_month[$mm-1];?></option>
              <?php } ?>
            </select>
          </div>
          <div class="col-md-2 col-sm-3 col-xs-4" style="padding-right:0;">
            <select id="year" class="form-control no-radius" style="padding:6px;">
              <option value="">year</option>
              <?php $curr_year = date("Y");$begin_year = $curr_year -70;for($yy=$begin_year;$yy <= $curr_year-10;$yy++){?>
              <option value="<?=$yy;?>" <?php if($data_user[0]['User_DOB'] == $yy){echo "selected";};?> ><?=$yy;?></option>
              <?php } ?>
            </select>
          </div>
          <div class="col-xs-12 pad0"><div id="valid-date" class="valid-message"></div></div>
        </div>
        <div class="row">
          <div class="col-xs-12 pad0">
            <div class="checkbox marg0">
              <label>
                <input id="dob-show" type="checkbox" value="yes" <?php if($data_user[0]['User_DOB_show'] == 'yes'){ echo "checked=checked";}?>> <?=$lang['personal-dob_show'];?>
              </label>
            </div>
          </div>
        </div>
        <div class="row up1">
          <div class="col-xs-12 pad0">
            <span class="label-title"><?=$lang['personal-interest'];?></span>
          </div>
          <div class="col-md-6 col-sm-9 col-xs-12 pad0">
            <select id="interest" class="form-control no-radius">
              <option value=""><?=$lang['personal-interest_select'];?></option>
              <?php $arr_interest = array("Open for any new opportunities", "Available for freelance work", "Looking for a full-time job", "Looking for an internship", "Starting-up a company","Getting higher education","Finishing college","Not so sure yet");
              for($int=1;$int <= count($arr_interest);$int++){?>
              <option value="<?=$int;?>" <?php if($data_user[0]['User_interest'] == $int){echo "selected";};?>><?=$arr_interest[$int-1];?></option>
              <?php } ?>
            </select>
            <div id="valid-interest" class="valid-message"></div>
          </div>
        </div>

        <div class="row up1">
          <div class="col-xs-12 pad0">
            <span class="label-title"><?=$lang['personal-job'];?></span>
          </div>
          <div class="col-md-6 col-sm-9 col-xs-12 pad0">
            <input id="title" autocomplete="off" type="text" class="form-control no-radius" placeholder="<?=$lang['personal-job_placeholder'];?>" value="<?=$data_user[0]['User_bioDesc'];?>"/>
            <div id="valid-title" class="valid-message"></div>
          </div>
        </div>

        <div class="row up1">
          <div class="col-xs-12 pad0">
            <span class="label-title"><?=$lang['personal-city'];?></span>
          </div>
          <div class="col-md-8 col-sm-9 col-xs-12 pad0">
            <input id="city" autocomplete="off" type="text" class="form-control no-radius" placeholder="<?=$lang['personal-city_placeholder'];?>" value="<?=$data_user[0]['User_city'];?>"/>
            <div id="valid-city" class="valid-message"></div>
          </div>
        </div>

        <div class="row up1">
          <div class="col-xs-12 pad0">
            <span class="label-title"><?=$lang['personal-state'];?></span>
          </div>
          <div class="col-md-8 col-sm-9 col-xs-12 pad0">
            <input id="state" autocomplete="off" type="text" class="form-control no-radius" placeholder="<?=$lang['personal-state_placeholder'];?>" value="<?=$data_user[0]['User_state'];?>"/>
          </div>
        </div>

        <div class="row up1">
          <div class="col-xs-12 pad0">
            <span class="label-title"><?=$lang['personal-country'];?></span>
          </div>
          <div class="col-md-8 col-sm-9 col-xs-12 pad0">
            <select id="country" class="no-radius js-example-templating select-country">
              <option value=""><?=$lang['personal-country_select'];?></option>
              <?php foreach($data_countrys as $data_country){ ?>
              <option <?php if($data_country['Country_title'] == $data_user[0]['User_country']){echo "selected=selected";} ?> value="<?=$data_country['Country_title'];?>" title="<?=$global['img-url'];?><?=$data_country['Country_flag'];?>"><?=$data_country['Country_title'];?></option>
              <?php } ?>
            </select>
            <div id="valid-country" class="valid-message"></div>
          </div>
        </div>

        <div class="row up1">
          <div class="col-xs-12 pad0">
            <span class="label-title"><?=$lang['personal-email'];?></span>
          </div>
          <div class="col-md-8 col-sm-9 col-xs-12 pad0">
            <input id="email" autocomplete="off" type="text" class="form-control no-radius" placeholder="<?=$lang['personal-email_placeholder'];?>" value="<?=$data_user[0]['User_email'];?>" disabled/>
            <div id="valid-email" class="valid-message"></div>
          </div>
        </div>

        <div class="row up1">
          <div class="col-xs-12 pad0">
            <span class="label-title"><?=$lang['personal-phone'];?></span>
          </div>
          <div class="col-md-8 col-sm-9 col-xs-12 pad0">
            <input id="phone" autocomplete="off" type="text" class="form-control no-radius" placeholder="<?=$lang['personal-phone_placeholder'];?>" value="<?=$data_user[0]['User_phone'];?>"/>
            <div id="valid-phone" class="valid-message"></div>
          </div>
        </div>
        <div class="row">
          <div class="col-xs-12 pad0">
            <div class="checkbox marg0">
              <label>
                <input id="phone-show" type="checkbox" value="yes" <?php if($data_user[0]['User_contact_show'] == 'yes'){ echo "checked=checked";}?> > <?=$lang['personal-phone_show'];?>
              </label>
            </div>
          </div>
        </div>

        <div class="row up1">
          <div class="col-xs-12 pad0">
            <span class="label-title"><?=$lang['personal-website'];?></span>
          </div>
          <div class="col-md-8 col-sm-9 col-xs-12 pad0">
            <input id="website" autocomplete="off" type="text" class="form-control no-radius" placeholder="<?=$lang['personal-website_placeholder'];?>" value="<?php if($data_user[0]['User_website'] != ""){ echo $data_user[0]['User_website'];}else{ echo "profile.civimi.com/".$data_user[0]['User_username'].".cvm";}?>"/>
            <div id="valid-website" class="valid-message"></div>
          </div>
        </div>

        <div class="row up15">
          <div class="col-xs-12 pad0">
            <a id="btn-update-personal" class="btn btn-info" onclick="update_personal();">Save</a>
            <a id="btn-cancel-personal" class="btn btn-default" onclick="personal_toggle();">Cancel</a>
          </div>
        </div>
      </div>
      <div id="drop-personal" class="col-md-3 col-sm-4 col-xs-12">
        <div class="up2 visible-xs"></div>
        <div class="row">
          <div class="col-xs-12">
            <span class="label-profile-img">Profile Picture</span>
          </div>
        </div>
        <form action="" class="uploadform dropzone-profile dropzone no-margin dz-clickable" style="max-width: 200px;margin: 0 auto;">
          <div class="profile-img-box">
          <img id="img-eprofile" class="img-profile" src="<?php echo check_image_url($data_user[0]['User_proPhotoThmb']);?>" alt="Resume picture of <?=$data_user[0]['User_fname']." ".$data_user[0]['User_lname'];?> in CIVIMI"/>
          <div id="load-profile" class="drop-load hide"><div class="drop-load-img"><img src="<?=$global['absolute-url'];?>img/loader.gif" alt="load image"></div></div>
          </div>
          <div class="drop-file-text">Drop File To Changes</div>
        </form>

      </div>
    </div>
  </div>
</div>
<!-- Modal Background -->
<div class="modal fade" id="modal-bg" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Pick a Background</h4>
      </div>
      <div class="modal-body">
        <ul class="bg-list">
          <?php for($g=1;$g<=9;$g++){ ?>
          <li class="col-cbg">
            <a href="javascript:;" onclick="changeBg(<?=$g;?>);" class="link-cbg">
              <div id="pbg-<?=$g;?>" data-bg="<?=$global['absolute-url'];?>img/background/cvm-bg<?=$g;?>.jpg" class="img-cbg" style="background-image:url('<?=$global['absolute-url'];?>img/background/cvm-bg<?=$g;?>.jpg');"></div>
              <div id="activeBG-<?=$g;?>" class="bg-default <?php if($data_user[0]['User_bg_img'] == $global['absolute-url']."img/background/cvm-bg".$g.".jpg") { ?>active-bg<?php } ?>"><i class="glyphicon glyphicon-ok"></i></div>
              <div id="loadBG-<?=$g;?>" class="load-cbg"><img src="<?=$global['absolute-url'];?>img/loader.gif" alt="loading"></div>
            </a>
          </li>
          <?php } ?>
        </ul>
      </div>
    </div>
  </div>
</div>
<script>
function changeBg(number){
  $("#loadBG-"+number).show();
  var img = $('#pbg-'+number).data('bg');
  var urlString = 'url(' + img + ')';
  var url = "<?=$api['user-update_background'];?>";

  $(".bg-default").removeClass('active-bg');
  $(".link-cbg").addClass('disable-state');
  var data = {
    id : "<?=$_SESSION['userData']['id'];?>",
    auth_code : "<?=$_SESSION['userData']['auth_code'];?>",
    background : img
  }
  $.ajax({url: url,data : data, success:function(result){
    console.log(result);
    $("#loadBG-"+number).hide();
    document.getElementById("personal-profile").style.backgroundImage =  urlString;
    $("#activeBG-"+number).addClass('active-bg');
    $(".link-cbg").removeClass('disable-state');
    $('#modal-bg').modal('hide');
  }
});
}
function validPersonal(){
  var ip_fname = $("#fname").val();
  var ip_lname = $("#lname").val();
  var ip_day = $("#day").val();
  var ip_month = $("#month").val();
  var ip_year = $("#year").val();
  var ip_interest = $("#interest").val();
  var ip_title = $("#title").val();
  var ip_city = $("#city").val();
  var ip_state = $("#state").val();
  var ip_country = $("#country").val();
  var ip_phone = $("#phone").val();
  var number_format = /^(\d+(?:[\.\,]\d{1,2})?)$/;

  if(ip_fname != ""){
    message = "";
    $("#fname").removeClass('input-error');
    $("#valid-fname").text(message);
  } else {
    message = "Please input your First Name!";
    $("#fname").addClass('input-error');
    $("#fname").focus();
    $("#valid-fname").text(message);
    return false;
  }
  if(ip_lname != ""){
    message = "";
    $("#lname").removeClass('input-error');
    $("#valid-lname").text(message);
  } else {
    message = "Please input your Last Name!";
    $("#lname").addClass('input-error');
    $("#lname").focus();
    $("#valid-lname").text(message);
    return false;
  }
  if(ip_day != ""){
    message = "";
    $("#day").removeClass('input-error');
    $("#valid-date").text(message);
  } else {
    message = "Please select your date of birth!";
    $("#day").addClass('input-error');
    $("#day").focus();
    $("#valid-date").text(message);
    return false;
  }
  if(ip_month != ""){
    message = "";
    $("#month").removeClass('input-error');
    $("#valid-date").text(message);
  } else {
    message = "Please select your month of birth!";
    $("#month").addClass('input-error');
    $("#month").focus();
    $("#valid-date").text(message);
    return false;
  }
  if(ip_year != ""){
    message = "";
    $("#year").removeClass('input-error');
    $("#valid-date").text(message);
  } else {
    message = "Please select your year of birth!";
    $("#year").addClass('input-error');
    $("#year").focus();
    $("#valid-date").text(message);
    return false;
  }
  if(ip_interest != ""){
    message = "";
    $("#interest").removeClass('input-error');
    $("#valid-interest").text(message);
  } else {
    message = "Please select your Interest!";
    $("#interest").addClass('input-error');
    $("#interest").focus();
    $("#valid-interest").text(message);
    return false;
  }
  if(ip_title != ""){
    message = "";
    $("#title").removeClass('input-error');
    $("#valid-title").text(message);
  } else {
    message = "Please input your current Profession or Job!";
    $("#title").addClass('input-error');
    $("#title").focus();
    $("#valid-title").text(message);
    return false;
  }
  if(ip_city != ""){
    message = "";
    $("#city").removeClass('input-error');
    $("#valid-city").text(message);
  } else {
    message = "Please input your city!";
    $("#city").addClass('input-error');
    $("#city").focus();
    $("#valid-city").text(message);
    return false;
  }
  if(ip_state != ""){
    message = "";
    $("#state").removeClass('input-error');
    $("#valid-state").text(message);
  } else {
    message = "Please input your state!";
    $("#state").addClass('input-error');
    $("#state").focus();
    $("#valid-state").text(message);
    return false;
  }
  if(ip_country != ""){
    message = "";
    $("#country").removeClass('input-error');
    $("#valid-country").text(message);
  } else {
    message = "Please input or select your country!";
    $("#country").addClass('input-error');
    $("#country").focus();
    $("#valid-country").text(message);
    return false;
  }
  // if(ip_phone != ""){
  //   if(ip_phone.match(number_format)) {
  //     message = "";
  //     $("#phone").removeClass('input-error');
  //     $("#valid-phone").text(message);
  //   } else {
  //     message = "Phone has to be in number!";
  //     $("#phone").addClass('input-error');
  //     $("#phone").focus();
  //     $("#valid-phone").text(message);
  //     return false;
  //   }
  // } else {
  //   message = "Please input your phone number!";
  //   $("#phone").addClass('input-error');
  //   $("#phone").focus();
  //   $("#valid-phone").text(message);
  //   return false;
  // }
  return true;
}
function formatState (state) {
  if (!state.id) { return state.text; }
  var $state = $(
    '<span><img src="' + state.element.title + '" class="img-flag" /> ' + state.text + '</span>'
  );
  return $state;
};
 
$(".select-country").select2({
  templateResult: formatState
});

$(document).ready(function(){
Dropzone.autoDiscover = false; // keep this line if you have multiple dropzones in the same page
$(".uploadform").dropzone({
acceptedFiles: "image/jpeg,image/png",
url: "<?=$global['absolute-url'];?>upload.php",
maxFiles: 1, // Number of files at a time
maxFilesize: 1, //in MB
error: function(file){
alert('Your uploaded file is not supported!');
},
maxfilesexceeded: function(file)
{
alert('You have uploaded more than 1 Image. Only the first file will be uploaded!');
},
processing: function () {
$('#load-profile').removeClass('hide');//on process show loading progress
},
success: function (response) {
var x = JSON.parse(response.xhr.responseText);
//$('.img').attr('src',x.img); // Set src for the image
$('#load-profile').addClass('hide');// hide loading progress
$('#img-profile').attr('src',x.thumb); // Set src for the thumbnail
$('#img-eprofile').attr('src',x.thumb); // Set src for the thumbnail
$("#personal-progress").text(x.personal+"%"); // Set personal progress
this.removeAllFiles(); // This removes all files after upload to reset dropzone for next upload
console.log('Image -> '+x.img+', Thumb -> '+x.thumb); // Just to return the JSON to the console.
},
addRemoveLinks: true,
removedfile: function(file) {
var _ref; // Remove file on clicking the 'Remove file' button
return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;
}
});
});
// get_load();
function personal_toggle(){
  if($("#personal-edit-box").hasClass("hide")){
    $("#personal-profile").addClass("hide");
    $("#personal-edit-box").removeClass("hide");
  }else{
    $("#personal-edit-box").addClass("hide");
    $("#personal-profile").removeClass("hide");
  }
}

function get_load(){
  $("#personal-loader").removeClass("hide");
  $("#personal-profile").addClass("hide");
  var url = "<?=$api['user-get_personal'];?>";

  var data = {
    id : "<?=$_SESSION['userData']['id'];?>",
    auth_code : "<?=$_SESSION['userData']['auth_code'];?>"
  }
  $.ajax({url: url,data : data, success:function(result){
    console.log(result);

    $(".profile-job").html("<h1>"+result.data[0].User_bioDesc+"</h1>");
    $(".profile-name").html(result.data[0].User_fname+" "+result.data[0].User_lname+", "+result.data[0].age);
    $(".profile-address").html(result.data[0].User_city+", "+result.data[0].User_state+", "+result.data[0].User_country);
    $(".profile-email").html(result.data[0].User_email);
    $(".profile-phone").html(result.data[0].User_phone);

    $("#personal-progress").text(result.data[0].ATpro_personal+"%");

    $("#personal-loader").addClass("hide");
    $("#personal-profile").removeClass("hide");

    // $(".summary-loader").addClass("hide"); 
    // $(".summary-desc").html(decodetxt);
    // $(".summary-desc").removeClass("hide");
  }
});
}

function update_personal(){
if ( validPersonal() ){
  var url = "<?=$api['user-update_personal'];?>";
  var ip_fname = $("#fname").val();
  var ip_lname = $("#lname").val();
  var ip_day = $("#day").val();
  var ip_month = $("#month").val();
  var ip_year = $("#year").val();
  var ip_interest = $("#interest").val();
  var ip_title = $("#title").val();
  var ip_city = $("#city").val();
  var ip_state = $("#state").val();
  var ip_country = $("#country").val();
  var ip_email = $("#email").val();
  var ip_phone = $("#phone").val();
  var ip_website = $("#website").val();
  var ip_dob_show = document.getElementById('dob-show');
  var ip_phone_show = document.getElementById('phone-show');
  if (ip_dob_show.checked) {
    ip_dob_show = 'yes';
  } else {
    ip_dob_show = 'no';
  }
  if (ip_phone_show.checked) {
    ip_phone_show = 'yes';
  } else {
    ip_phone_show = 'no';
  }

  var data = {
    id : "<?=$_SESSION['userData']['id'];?>",
    auth_code : "<?=$_SESSION['userData']['auth_code'];?>",
    fname :ip_fname,
    lname :ip_lname,
    day : ip_day,
    month : ip_month,
    year: ip_year,
    interest : ip_interest,
    title : ip_title,
    city : ip_city,
    state: ip_state,
    country: ip_country,
    email: ip_email,
    phone: ip_phone,
    dob_show: ip_dob_show,
    phone_show: ip_phone_show,
    website : ip_website
  }
  $('html, body').animate({
        scrollTop: $('#personal-loader').offset().top-80
  }, 'slow');
  
  $.ajax({url: url,data : data,type:"POST", success:function(result){
    console.log(result);
    $("#personal-loader").removeClass("hide");
    $("#personal-edit-box").addClass("hide");
    setTimeout(function() {
    get_load();
    }, 1500)
  }
});
}
}
</script>