<div id="popular-job-content">
    <div class="container container-job">
        <div class="row">
            <div class="col-xs-12">
                <div class="popular-job-header">Most Popular Jobs</div>
                <div class="popular-job-header-child">Here are the current most popular jobs.</div>
            </div>
        </div>
        <div class="row row-popular-job hidden-xs">
            <?php for($j=1;$j<=5;$j++){?>
            <div class="col-xs-12 col-popular-job">
                <div class="popular-job-box" style="background-image:url('img/jobs.jpg')">
                    <div class="popular-job-text">
                        <div class="popular-job-title">IT Infrastructure Developer</div>
                        <a href="" class="popular-job-link">see openings</a>
                    </div>
                </div>
            </div>
            <?php } ?>
        </div>
        <div id="swiper-job" class="swiper-container height-auto visible-xs">
            <div class="swiper-wrapper height-auto">
                <?php for($p=1;$p<=6;$p++) { ?>
                <div class="swiper-slide swiper-slide-desktop height-auto">
                    <div class="col-xs-12 col-popular-job">
                        <div class="popular-job-box" style="background-image:url('img/jobs.jpg')">
                            <div class="popular-job-text">
                                <div class="popular-job-title">IT Infrastructure Developer</div>
                                <a href="" class="popular-job-link">see openings</a>
                            </div>
                        </div>
                    </div>
                </div>
                <?php } ?>
            </div>
        </div>
        <div class="row up3">
            <div class="col-xs-12">
                <a href="" class="popular-job-all-link">see all job openings</a>
            </div>
        </div>
    </div>
</div>