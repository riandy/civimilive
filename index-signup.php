<div id="index-signup-content">
    <div class="container container-max">
        <form name="register" action="<?=$path['register'];?>" method="post" enctype="multipart/form-data" onsubmit="return reinventValid()">
            <div class="row">
                <div class="col-xs-12">
                    <div class="index-signup-header"><h2><strong><?=$lang['home-reinvent'];?></strong></h2></div>
                    <div class="index-signup-header"><?=$lang['home-reinvent_note'];?></div>
                </div>
            </div>
            <div class="row up1">
                <div class="col-xs-10 col-xs-offset-1 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4">
                    <input id="reinvent" type="email" class="form-control form-textbox" name="email" placeholder="your email address">
                </div>
            </div>
            <div class="row up1">
                <div class="col-xs-12 text-center">
                    <button class="btn btn-signup1" type="submit"><?=$lang['home-reinvent_btn'];?></button>
                </div>
            </div>
        </form>
    </div>
</div>
<script type="text/javascript">
    function reinventValid(){
        var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        var email = $("#reinvent").val();
        // validation email
        if(email != ""){
            if(email.match(mailformat)) {
                $("#reinvent").removeClass('input-error');
            } else {  
                $("#reinvent").addClass('input-error');
                $("#reinvent").focus();
                var message = "Your format email is incorect!";
                popup(message);
                return false;  
            }
        } else {
            $("#reinvent").addClass('input-error');
            $("#reinvent").focus();
            var message = "Insert your email to process!";
            popup(message);
            return false;
        } //end validation email
        return true;
    }
</script>