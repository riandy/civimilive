<?php 
include("packages/require.php");
include("controller/controller_global.php");
include("controller/controller_home.php");
$curpage='home';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title><?=$global['title-job'];?></title>
    <meta name="keywords" content="<?=$seo['keyword-job'];?>">
    <meta name="description" content="<?=$seo['desc-job'];?>">
    <?php include("packages/head.php");?>
    <link rel="stylesheet" href="<?php echo $global['absolute-url'];?>css/swiper.min.css"/>
    <script src="<?php echo $global['absolute-url'];?>js/swiper.min.js"></script>
</head>
<body>
    <div id="all">
        
        <!-- start header -->
        <div class="top-content">
            <div class="top-wrapper">
            <!-- start top nav -->
        <?php include("section-top-nav.php");?>
        <!-- end top nav -->
            
                <div class="container container-header-cvm">
                    <div class="index-top-header">FIND YOUR CAREER HERE</div>
                    <div class="index-top-header-child">There are more than 8,051 jobs in civimi and counting...</div>
                    <!--<form action="search1.php" name="" method="GET" class="hidden-xs">!-->
                    <form action="<?=$path['job-search'];?>" method="GET">
                    <div class="row hidden-xs">
                        <div class="job-level">
                            <!-- <ul id="level-list">
                                <label class="radio-inline ss-inline ss-level">
                                  <input class="radio-level" type="radio" name="expertise" value="" checked> All levels
                                  <div class="checked-border"></div>
                                </label>          
                            </ul> -->
                        </div>
                        <div class="col-md-3 col-sm-3 col-search">
                            <select id="jb-function" class="js-example-basic-single form-control select-search" name="function">
                                <option value="">All Job Functions</option>
                            </select>
                        </div>
                        <div class="col-md-6 col-sm-6 col-search-center">
                            <input type="text" name="q" class="form-control input-search" placeholder="search for job title, skills, or keywords">
                            <div class="search-location">
                                <img src="<?php echo $global['absolute-url'];?>img/location.png" alt="location" class="location-icon">
                                <span class="dropdown">
                                    <a id="drop-city" href="#" class="dropdown-toggle location-select" data-toggle="dropdown" aria-expanded="true">Location <span class="caret"></span></a>
                                    <ul id="city-list" class="dropdown-menu drop-city" role="menu">
                                      <!-- <li><input type="radio" name="location" value="<?php echo $data_city['City_ID'];?>"> <?php echo $data_city['City_title'];?></li> -->
                                            <!--<a href="search1.php?location=<?php echo $data_city['City_ID'];?>"><?php echo $data_city['City_title'];?></a></li>!-->
                                                    
                                    </ul>
                                </span>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-search">
                            <button type="submit" class="btn btn-block btn-info btn-search">Search</button>
                        </div>
                    </div>
                    </form>
                    <div class="mobile-btn-content visible-xs">
                      <div class="row">
                        <div class="col-xs-12">
                          <a href="#" class="btn-mobile-search" onclick="searchSlide();">
                              <div class="input-mobile-search">search for job title, skills, <span class="hidden-xs">or keywords</span></div>
                              <div class="icon-search"><span class="glyphicon glyphicon-search"></span></div>
                          </a>
                        </div>
                      </div>
                    </div>
                    <div class="up5"></div>
                </div>
            </div><!--  end wrapper -->
        </div><!--  end top content -->
        <!-- end header -->
        <div id="center-content">
            <!-- start popular job -->
            <div id="popular-job-content">
                <div class="container container-job">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="popular-job-header">Jobs in-demand</div>
                            <div class="popular-job-header-child">Here are the current most popular jobs.</div>
                        </div>
                    </div>
                    <!-- <div class="row">
                      <div class="col-xs-6 col-md-4 col-popular-job">
                        <a href="#" class="index-pjob-box">
                          <div class="pjob-img"><img src="img/terminal.png" alt=""></div>
                          <div class="pjob-title">Progammer</div>
                        </a>
                      </div> 
                    </div> -->
                    <div class="row row-popular-job up3">
                        <div id="popular-field">
                           <!-- start loop -->
                        <!-- <div class="col-xs-6 col-md-4 col-popular-job">
                            <div class="popular-job-box" style="background-image:url('img/jobs.jpg')">
                                <div class="popular-job-text">
                                    <div class="popular-job-title">IT Infrastructure Developer</div>
                                    <a href="" class="popular-job-link">see openings</a>
                                </div>
                            </div>
                        </div> -->
                        <!-- //end loop -->
                        </div>
                        <div id="loading-popular-job" class="row hidden">
                            <div class="col-xs-12">
                                <div class="loading-img">
                                <img src="<?php echo $global['absolute-url'];?>img/loader.gif" alt="loading">
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- <div class="row visible-xs">
                        <?php for($p=1;$p<=6;$p++) { ?>
                        <div class="col-xs-4 col-popular-mjob">
                            <div class="popular-job-box" style="background-image:url('img/jobs.jpg')">
                                <div class="popular-job-text">
                                    <div class="popular-job-title">IT Infrastructure Developer</div>
                                    <a href="" class="popular-job-link">see openings</a>
                                </div>
                            </div>
                        </div>
                        <?php } ?>
                    </div> -->
                    <div class="row up3">
                        <div class="col-xs-12 text-center">
                            <a id="load-function" class="btn btn-default btn-job-all">see more job functions</a>
                        </div>
                    </div>
                </div>
            </div><!-- end popular job -->
            <div class="container container-header-cvm">
                <hr/>
            </div>
            <div id="popular-company-content">
                <div class="container container-popular-company">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="popular-company-header">Today's Popular Companies</div>
                            <div class="popular-company-header-child">Here are the current most popular company.</div>
                        </div>
                    </div>
                    <div class="popular-company-desktop hidden-xs">
                        <div id="popular-company" class="row"></div>
                        <div id="loading-company" class="row hidden">
                            <div class="col-xs-12">
                                <div class="loading-img">
                                <img src="<?php echo $global['absolute-url'];?>img/loader.gif" alt="loading">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="swiper-company" class="swiper-container height-auto visible-xs">
                        <a class="swiper-left-arrow swiper-arrow" href="#"><img alt="left" src="<?php echo $global['absolute-url'];?>img/arrow-previous.png"></a>
                        <a class="swiper-right-arrow swiper-arrow" href="#"><img alt="right" src="<?php echo $global['absolute-url'];?>img/arrow-next.png"></a>
                        <div id="popular-company-mob" class="swiper-wrapper height-auto"></div>
                    </div>
                </div>
            </div>
        </div><!-- end center content -->
        <!-- start footer section -->
        <?php include("section-footer.php");?>
        <!-- end footer section -->
    </div><!--  end all div -->
<script src="<?php echo $global['absolute-url'];?>js/function.js"></script>
<script type="text/javascript">
    // get_level();
    function get_level(){
      var url = "<?=$api['level-index'];?>";
      $.ajax({url: url,success:function(result){
         
          // $('#loader-company').remove();
          $('#level-list').append(replace_level(result.data));
     }});  
    }
    function replace_level(datas){
    var resultHTML='';resultNext = "";
    var total = 0;previous = "";user_name = "";link="";
    var d = new Date();strip_string ="";
    var current = d.getTime();
         // console.log(datas);
        if(datas != null){
          var obj = datas;    

          for(var i=0;i < obj.length;i++){

            if(obj[i].Level_ID != '' && obj[i].Level_title != ''){

                resultHTML += "<label class='radio-inline ss-inline ss-level'><input class='radio-level' type='radio' name='expertise' value='"+obj[i].Level_ID+"' > "+obj[i].Level_title+"<div class='checked-border'></div></label>";
              }
            }
          }
          return resultHTML;
    };
    get_jfunction();
    function get_jfunction(){
      var url = "<?=$api['field-index'];?>";
          $.ajax({url: url,success:function(result){
             
              $('#jb-function').append(replace_jfunction(result.data));       
              $("#jb-function").select2({
                placeholder: "all job function"
              });
        }});
    }
    function replace_jfunction(datas){
    var resultHTML='';resultNext = "";
    var total = 0;previous = "";user_name = "";link="";
    var d = new Date();strip_string ="";
    var current = d.getTime();
         // console.log(datas);
        if(datas != null){
          var obj = datas;    

          for(var i=0;i < obj.length;i++){

            if(obj[i].Field_ID != '' && obj[i].Field_title != ''){

                resultHTML += "<option value='"+obj[i].Field_ID+"'>"+obj[i].Field_title+"</option>";
              }
            }
          }
          return resultHTML;
    };
    get_city();
    function get_city(){
      var url = "<?=$api['city-index'];?>";
          $.ajax({url: url,success:function(result){
             
              $('#city-list').append(replace_city(result.data));
        }});
    }
    function replace_city(datas){
    var resultHTML='';resultNext = "";
    var total = 0;previous = "";user_name = "";link="";
    var d = new Date();strip_string ="";
    var current = d.getTime();
          // console.log(datas);
        if(datas != null){
          var obj = datas;    

          for(var i=0;i < obj.length;i++){

            if(obj[i].City_ID != '' && obj[i].City_title != ''){

                resultHTML += "<li><div class='radio city-radio'><label><input type='radio' data-city_title='"+obj[i].City_title+"' name='location' id='rcity-"+obj[i].City_ID+"' value='"+obj[i].City_ID+"' onclick='placeCity("+obj[i].City_ID+");'>"+obj[i].City_title+"</label></div></li>";
              }
            }
          }
          return resultHTML;
    };
    var count = 1;
    get_field(1);
    $('#load-function').click(function(){
      $('#load-function').html("");
      $('#load-function').html("<img src='img/loadmores.gif' alt='loading' style='width:20px;''> Loading....");
      setTimeout(function() { 
          get_field(count); 
      }, 300)
      count++;
    });
    function get_field(count){
      $('#loading-popular-job').removeClass('hidden');
      setTimeout(function() {
          var url = "<?=$api['field-popular'];?>"+count+"/";
          $.ajax({url: url,success:function(result){
             
              $('#loading-popular-job').addClass('hidden');
              $('#popular-field').append(replace_field_job(result.data));
              $('#load-function').html("see more job functions");
        }});  
      }, 1500)
    }

    function replace_field_job(datas){
    var resultHTML='';resultNext = "";
    var total = 0;previous = "";user_name = "";link="";
    var d = new Date();strip_string ="";
    var current = d.getTime();
        // console.log(current);
        if(datas != null){
          var obj = datas;    

          for(var i=0;i < obj.length;i++){

            if(obj[i].Field_ID != '' && obj[i].Field_title != ''){

              var fieldTitle = obj[i].Field_title;
              var regex = /\\/g;
              var replaceTitle = fieldTitle.replace(regex, "").replace(/&quot;/g, "");
              var lowercase = replaceTitle.toLowerCase();
              var encodeTitle = lowercase.replace(/\s+/g, '-');

                link = "<?=$path['job-search'];?>?function="+obj[i].Field_ID;
                resultHTML += "<div class='col-xs-6 col-md-4 col-popular-job'>";
                resultHTML += "<a href='"+link+"' class='index-pjob-box'>";
                resultHTML += "<div class='pjob-img'><img src='<?php echo $global['img-url'];?>"+encodeURI(obj[i].Field_img)+"' alt='"+fieldTitle+"'></div>";
                resultHTML += "<div class='pjob-title'>"+fieldTitle+"</div>";
                resultHTML += "</a>";
                resultHTML += "</div>";
              }
            }
          }
          return resultHTML;
    };

    get_company();
    function get_company(){
      $('#loading-company').removeClass('hidden'); 
      setTimeout(function() {
          var url = "<?=$api['company-popular'];?>";
          $.ajax({url: url,success:function(result){
             
              $('#loading-company').addClass('hidden');
              $('#popular-company').append(replace_company(result.data));
              $('#popular-company-mob').append(replace_company_mob(result.data));
                var contentSwiperCompany = new Swiper('#swiper-company',{
                loop: true,
                autoResize: true,
                paginationClickable: true,
                autoplay:3000,
                slidesPerView: 2,
                nextButton: '.swiper-right-arrow',
                prevButton: '.swiper-left-arrow',
                });
            }});  
      }, 1500) 
    }
    function replace_company(datas){
    var resultHTML='';resultNext = "";
    var total = 0;previous = "";user_name = "";link="";
    var d = new Date();strip_string ="";
    var current = d.getTime();
        // console.log(current);
        if(datas != null){
          var obj = datas;    

          for(var i=0;i < obj.length;i++){

            if(obj[i].Company_ID != '' && obj[i].Company_title != ''){

              strip_string = obj[i].article_intro;

              var companyTitle = obj[i].Company_title;
              var regex = /\\/g;
              var replaceTitle = companyTitle.replace(regex, "").replace(/&quot;/g, "");
              var lowercase = replaceTitle.toLowerCase();
              var encodeTitle = lowercase.replace(/\s+/g, '-');

                //link = "<?=$global['absolute-url'];?>"+encodeTitle+"_"+obj[i].Company_ID+".html";
                link = "<?=$path['job-search'];?>?company="+obj[i].Company_ID;
                if(obj[i].Company_img != ""){
                  img = "<?=$global['img-url'];?>"+obj[i].Company_img;
                }else{
                  img = "<?=$global['img-url'];?>beta/civimi/img/work.png";
                }

                resultHTML += "<div class='col-xs-2'>";
                resultHTML += "<a href='"+link+"' class='popular-company-box'>";
                resultHTML += "<div class='popular-company-img'>";
                resultHTML += "<img src='"+img+"' alt='Logo Perusahaan "+obj[i].Company_title+" dengan Lowongan Pekerjaan Paling Popular'>";
                resultHTML += "</div>";
                resultHTML += "<div class='popular-company-title'>"+simpleDecodeHtml(obj[i].Company_title)+"</div>";
                resultHTML += "</a></div>";
              }
            }
          }
          return resultHTML;
    };
    function replace_company_mob(datas){
    var resultHTML='';resultNext = "";
    var total = 0;previous = "";user_name = "";link="";
    var d = new Date();strip_string ="";
    var current = d.getTime();
        // console.log(current);
        if(datas != null){
          var obj = datas;    

          for(var i=0;i < obj.length;i++){

            if(obj[i].Company_ID != '' && obj[i].Company_title != ''){

              strip_string = obj[i].article_intro;

              var companyTitle = obj[i].Company_title;
              var regex = /\\/g;
              var replaceTitle = companyTitle.replace(regex, "").replace(/&quot;/g, "");
              var lowercase = replaceTitle.toLowerCase();
              var encodeTitle = lowercase.replace(/\s+/g, '-');

                //link = "<?=$global['absolute-url'];?>"+encodeTitle+"_"+obj[i].Company_ID+".html";
                link = "<?=$path['job-search'];?>?company="+obj[i].Company_ID;
                if(obj[i].Company_img != ""){
                  img = "<?=$global['img-url'];?>"+obj[i].Company_img;
                }else{
                  img = "<?=$global['img-url'];?>beta/civimi/img/work.png";
                }

                resultHTML += "<div class='swiper-slide swiper-slide-desktop height-auto'>";
                resultHTML += "<div class='col-xs-12 col-popular-company'>";
                resultHTML += "<a href='"+link+"' class='popular-company-box'>";
                resultHTML += "<div class='popular-company-img'><img src='"+img+"' alt='Logo Perusahaan "+obj[i].Company_title+" dengan Lowongan Pekerjaan Paling Popular'></div>";
                resultHTML += "<div class='popular-company-title'>"+simpleDecodeHtml(obj[i].Company_title)+"</div>";
                resultHTML += "</a>";
                resultHTML += "</div>";
                resultHTML += "</div>";
              }
            }
          }
          return resultHTML;
    };
</script>
</body>
</html>
