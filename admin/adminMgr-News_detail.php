<?php
ob_start("ob_gzhandler");
require_once("packages/require.php");
include("packages/check_login.php");//USED BY ALL PAGE BUT index.php
include("controller/controller_news_detail.php");
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <?php include('packages/head.php');?>
    <!-- calendar -->
    <link rel="stylesheet" type="text/css" href="packages/jQuery-Tags/jquery-ui.css" />
    <link rel="stylesheet" type="text/css" href="packages/jQuery-Tags/jquery.tagsinput.css" />
    <script src="//code.jquery.com/jquery-1.10.2.js"></script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.0/themes/smoothness/jquery-ui.css">
    <script src="//code.jquery.com/ui/1.11.1/jquery-ui.js"></script>
    <script src="js/tinymce/tinymce.min.js"></script>
    <script type="text/javascript" src="packages/jQuery-Tags/jquery-ui.js"></script>
    <script type="text/javascript" src="packages/jQuery-Tags/jquery.tagsinput.js"></script>
  
    <script>
        tinymce.init({
            selector: "textarea",
            plugins: [
            "advlist autolink lists link image charmap print preview anchor",
            "searchreplace visualblocks code fullscreen",
            "insertdatetime media table contextmenu paste"
            ],
            toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
            relative_urls: false,
            forced_root_block : false,
            height : 300
        });

        $(function() {
            $( ".datepicker" ).datepicker({dateFormat: 'yy-mm-dd' });
        });
    </script>
</head>
<body class="civMain" onload="startTime()">
    <?php require_once("admin-Header.php"); ?>

    <!-- start top nav -->
    <?php $curPage='blog'; ?>
    <?php require_once("admin-top_nav.php");?>
    <!-- end top nav -->

    <div class="container">
        <div id="menuDashboard" class="row-fluid">
            <div id="rightContent" class="span12 photoAlbum">
                <?php 
                if($message!=null)
                {
                    echo "<div class='alert alert-info' id='message1'>" . $message . "</div>";
                }
                ?>
                <div class="row-fluid">
                    <div class="span3">
                        <a href="adminMgr-News.php" class="btn btn-default"><i class="icon-circle-arrow-left"></i> Back</a>
                    </div>
                    <div class="span6">
                        <h4 style="margin: 0; text-align:center;">News Management Edit <br>(<?php echo correctDisplay($datas[0]['News_title']);?>)</h4>
                    </div>
                </div>
                <hr />
                <form name="createNews" action="adminMgr-News_detail.php?action=editNews" method="post" enctype="multipart/form-data" onsubmit="loadingText('btn-submit')" onclick="add_zindex();">
                    <table class="table table-hover">
                    <?php 
                    if(!is_array($datas))
                    {
                        echo "<br />There is no gallery.";
                    }
                    else
                    {   
                    ?>   
                       <tr>
                            <td colspan="2">
                                <div>
                                    <div class="row-fluid">
                                        <div class="span3" align="right"><strong>Category&nbsp;*</strong>:</div>
                                        <div class="span9">
                                            <select name="News_category" type="text" class="text input-block-level" required >
                                                <option value="">-Choose One-</option>
                                                <?php foreach ($categorys as $cat) {?>
                                                <option <?php if($datas[0]['News_ref_ncID'] == $cat['Nc_ID']){ echo "selected=selected"; }?> value="<?php echo $cat['Nc_ID']; ?>"><?php echo $cat['Nc_title']; ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>    
                                    <div class="row-fluid">
                                        <div class="span3" align="right"><strong>Title</strong> :</div>
                                        <div class="span9"><input name="News_title" type="text" class="text input-block-level" required="required" maxlength="256" placeholder="i.e. Title issue (max 256 chars)" value="<?php echo $datas[0]['News_title'];?>" /></div>
                                    </div>
                                    <div class="row-fluid">
                                        <div class="span3" align="right">Featured :</div>
                                        <div class="span9">
                                            <label class="checkbox">
                                                <input name="News_featured" type="checkbox" value="1" <?php if($datas[0]['News_featured'] == 1){echo "checked=checked";}?>>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="row-fluid">
                                        <div class="span3" align="right">Content :</div>
                                        <div class="span9">
                                            <textarea rows="5" name="News_content" class="textarea text input-block-level" maxlength="4096" placeholder="i.e. Description issue (max 4096 chars)"  ><?php echo correctDisplay($datas[0]['News_content']);?></textarea>
                                        </div>
                                    </div><br/>
                                    <div class="row-fluid">
                                        <div class="span3" align="right">Tags :</div>
                                        <div class="span9"><input id="refTag" name="tag" class="tags" value="<?php echo $data_tags;?>" /></div>
                                    </div><br>
                                    <div class="row-fluid">
                                        <div class="span3" align="right">Photographer :</div>
                                        <div class="span9"><input name="News_photographer" type="text" class="text input-block-level" maxlength="256" placeholder="i.e. Photographer, (max 512 chars)" value="<?php echo $datas[0]['News_photographer'];?>" /></div>
                                    </div>
                                    <div class="row-fluid">
                                        <div class="span3" align="right">Photographer Link:</div>
                                        <div class="span9"><input name="News_photographer_link" type="url" class="text input-block-level"  placeholder="input URL of blog, instagram, website (http://www.civimi.com)" value="<?php echo $datas[0]['News_photographer_link'];?>" /></div>
                                    </div>
                                    <div class="row-fluid">
                                        <div class="span3" align="right">Author :</div>
                                        <div class="span9"><input name="News_author" type="text" class="text input-block-level" maxlength="256" placeholder="i.e. Author, (max 512 chars)" value="<?php echo $datas[0]['News_author'];?>" /></div>
                                    </div>
                                    <div class="row-fluid">
                                        <div class="span3" align="right">Author Link:</div>
                                        <div class="span9"><input name="News_author_link" type="url" class="text input-block-level" placeholder="input URL of blog, instagram, website (http://www.civimi.com)" value="<?php echo $datas[0]['News_author_link'];?>" /></div>
                                    </div>
                                    <div class="row-fluid">
                                        <div class="span3" align="right">Video Url:</div>
                                        <div class="span9">
                                            <input name="News_video_link" type="url" class="text input-block-level" placeholder="input URL Video (http://youtu.be/c1v1m1)" value="<?php echo $datas[0]['News_url_video'];?>" /><br/>
                                            <a class="btn" data-toggle="modal" data-target="#dialog-info">Info</a>
                                        </div>
                                    </div><br/>
                                    <div class="row-fluid">
                                        <div class="span3" align="right">Publish :</div>
                                        <div class="span9">
                                            <select name="News_publish" class="text">
                                                <option <?php if($datas[0]['News_publish'] == 'Publish'){echo "selected=selected";}?> value="Publish">Publish</option>
                                                <option <?php if($datas[0]['News_publish'] != 'Publish'){echo "selected=selected";}?> value="Not Publish">Not Publish</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row-fluid">
                                        <div class="span8 offset3">
                                            <?php 
                                            if($datas[0]['Np_img_thmb'] != null && $datas[0]['Np_img_thmb'] !='')
                                            {
                                                echo "<a class='group_images' href='{$datas[0]['Np_img']}' ><img class='img-circle logo-thmb' src='{$datas[0]['Np_img_thmb']}' /></a><br />";
                                            }
                                            ?>
                                        </div>
                                    </div>
                                    <div class="row-fluid">
                                        <div class="span3" align="right"><strong>Primary Photo&nbsp;*</strong></div>
                                        <div class="span9"><input name="logo[]" type="file" class="text" maxlength="196" />
                                        <input type="hidden" name="News_tags" id="data-tags" value=""/>
                                        <input name="News_ID" type="hidden" class="text" value="<?php echo $datas[0]['News_ID']; ?>" />
                                        </div>
                                    </div>
                                    <br><hr>
                                    <div class="row-fluid">
                                        <div class="span3" align="right"></div>
                                        <div class="span9"><h5>-- Photo(s) --</h5> <i>Urutan photo tampil di halaman News_detail adalah mulai dari baris paling atas, dari foto paling kiri ke kanan, lalu baris berikutnya</i></div>
                                    </div>
                                    <div class="row-fluid">
                                        <div class="span3" align="right"></div>
                                        <div class="span9">
                                            <table class="table table-hover">
                                            <?php
                                            $tableCounter = 1;
                                            $m = 0;
                                            foreach ($data_photos as $data_photo)
                                            {
                                                if ($tableCounter > 3 || $tableCounter == 1)
                                                {
                                                    $tableCounter = 1;
                                                    echo "<tr align='center'>";
                                                }  

                                                $onclickDel = "\"if(window.confirm('Are your sure you would like to delete this photo {$data_photo['Np_title']}?')) location.href='adminMgr-News_detail.php?action=deletePhoto&np_ID={$data_photo['Np_ID']}&news_ID={$datas[0]['News_ID']}&news_title=" . urlencode($datas[0]['News_title']) . "';\"";
                                                $divID = "'button_close-" . $m ."'";   
                                                echo "<td>";
                                                echo "<div class='imgCat' style='height: 124px' onmouseover=\"btnOn($divID);\" onmouseout=\"btnOff($divID);\"> ";
                                                echo "<div id=$divID style='position: absolute; margin-left: 80px; margin-top: -24px; z-index: 500;visibility: hidden'><img onclick= " . $onclickDel . " src='img/btn_close.png' title='Delete photo " . $data_photo['Np_title'] . "' /></div>";
                                            
                                                echo "<a class='group_images' rel='civimiImg' title=\"{$datas[0]['News_title']} : {$data_photo['Np_title']}\" href='{$data_photo['Np_img']}'><img src='{$data_photo['Np_img_thmb']}' style='height: 100px; width: 100px' onmouseover='style.opacity=0.45;btnOn($divID);' onmouseout='style.opacity=1;btnOff($divID);' title='" . $data_photo['Np_title'] . "' /></a>";
                                                echo "<br /><input type='text' class='photo-title' name='photoTitle[]' maxlength='96' style='height: 12px;font-size: 8pt;' placeholder='photo title here' value=\"{$data_photo['Np_title']}\" /><input name='photoID[]' type='hidden' maxlength='8' value='{$data_photo['Np_ID']}' >";
                                                echo "</div>";?>
                                                <input type="radio" name="radio1"<?php 
                                                if($data_photo['Np_main']==1){
                                                    echo ' checked ';
                                                }
                                                ?> value="<?php echo $data_photo['Np_ID'];?>"/>
                                                Set as Primary
                                                <?php echo "<td/>";
                                                $tableCounter++;
                                                $m++;
                                                if ($tableCounter == 4)
                                                {
                                                    echo "</tr>";
                                                }
                                            }
                                            ?>
                                           </table>
                                        </div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </table>
                    <?php 
                    $newsNameJS = htmlspecialchars($datas[0]['News_title'],ENT_QUOTES);
                    $onclickDelNews="\"if(window.confirm('Are you sure you would like to delete $newsNameJS?')) location.href='adminMgr-News_detail.php?action=delete&news_title=".urlencode($newsNameJS)."&news_ID={$datas[0]['News_ID']}';\"";?>
                                             
                    <div class="row-fluid" style="background-color: #eee; border-top: black thin; padding: 20px 0 20px 0;">
                        <div class="span12" style="text-align: center;">
                            <div class="btn-group">
                                <a href="#dialog-addPhoto" data-toggle="modal" class="btn"><i class="icon-picture"></i> Add Photo(s)</a>
                                <button name="delFolder" type="button" class="btn btn-danger" style="color: white;" <?php echo "onClick=$onclickDelNews";?> ><i class="icon-trash icon-white"></i> Delete this Album</button>
                                <button id="btn-submit" name="submit" onmouseover="submit_tags();" type="submit" class="btn btn-success"><i class="icon-check icon-white"></i> Save Changes</button>
                            </div>
                        </div>
                    </div>
                    <div class="row-fluid">
                        <div id="SubStatus" class="span12 align-center"></div>
                    </div>
                    <?php
                    }
                    ?>      
                </form> 
                <!--OVERLAY Modal box starts -->        
                <div id="dialog-addPhoto" class="modal hide fade">    
                    <div class="modal-header">
                        <a href="#close" class="close" data-dismiss="modal" aria-hidden="true">×</a>
                        <strong>Upload photo(s) for <?php echo $datas[0]['News_title'];?></strong>
                    </div>
                    <form name="albumPhoto" class="dropzone" id="myAwesomeDropzone" action="<?php echo "adminMgr-News_detail.php?action=addPhoto&news_ID={$datas[0]['News_ID']}&news_title=".urlencode($datas[0]['News_title']); ?>" method="post" enctype="multipart/form-data" onsubmit="loadingText('btn-submit2')">
                    <div class="modal-body">
                        <div class="row-fluid">       
                            <div class="span4 align-right modal-label"><strong>Photo(s)&nbsp;</strong>:</div>
                            <div class="span8">
                                <div id="photoField"><input name="photo[]" type="file" class="file" />&nbsp;*</div>
                            </div>
                        </div>
                        <div class="row-fluid">
                            <div class="offset4 span8"><small>image format has to be jpg, jpeg, gif, png.</small><br/><small>Image will be best shown in 1200px by 800px</small></div>
                        </div>
                        <div class="row-fluid">
                            <div id="SubStatusPhoto" class="span12 align-center"></div>
                        </div>

                        <?php /*<input type="button" class="btn btn-info" value="add more photo(s)" onClick="addInput('photoField');"/> */ ?>
                         <input id="btn-submit2" name="uploadPhoto" type="submit" value="upload photo(s)" class="btn btn-success hide" />
                    </div>
                    </form>
                    <div class="modal-footer">
                        <div class="btn-group">
                            <a href="#" class="btn" data-dismiss="modal">Close</a>
                            <input type="button" class="btn btn-info" value="add more photo(s)" onClick="addInput('photoField');"/>
                            <input id="btn-submit3" type="button" value="upload photo(s)" class="btn btn-success" onclick="$('#btn-submit2').click();" />
                        </div>
                    </div>
                </div>
                <!--OVERLAY Modal box ends -->
            </div>
        </div>
        <div class="row-fluid">
            <div class="span6">
                <?php require_once("admin-Footer.php"); ?>
            </div>
        </div>
    </div>
    <!--Modal box video -->  
    <!--<div id="dialog-info" class="modal hide fade" style="width:50%;">
        <div class="modal-header">
            <a href="#close" class="close" data-dismiss="modal" aria-hidden="true">×</a>
            <strong>Petunjuk untuk Copy link Video Youtube</strong>
        </div>
        <div class="modal-content">
            <div>
                <img src="images/INFO-VIDEO.png" style="width:100%;"/>
            </div>
        </div>
    </div>!-->
    <script type="text/javascript">
        function add_zindex(){
            $('.ui-autocomplete').css("z-index", 1055);
        }
        $("#message1").fadeOut(8400);
        //$(document).ready(function(){$("a.group_images").fancybox({helpers:{title:{type:"inside"}},topRatio:0.5,fixed:true,autoSize:false,closeClick:false,openEffect:"fade",closeEffect:"fade"});$("a.various").fancybox({helpers:{title:{type:"inside"}},type:"iframe",topRatio:0.5,fixed:true,maxWidth:800,maxHeight:600,fitToView:true,width:"80%",height:"72%",autoSize:false,closeClick:false,openEffect:"fade",closeEffect:"fade"})});
    
        var counter = 1;
        var limit = 6;
        var num = 1;
        function addInput(divName){
            if (counter == limit)  {
                alert("You may upload as many photos as you like, but you can only upload "+counter+" photos at a time.");
            }
            else {
                var newdiv = document.createElement('div');
                newdiv.id = "Photo"+ (num + 1); 
                newdiv.innerHTML = "<br /><input name='photo[]' type='file' class='file' /></div>";
                            
                newdiv.innerHTML += "&nbsp;&nbsp;<a href='#js' title='remove field' onClick=\"removeInput('Photo" + (num + 1) + "');\" ><i class='icon-remove'></i></a>";
                document.getElementById(divName).appendChild(newdiv);
                counter++;
                num++;
            }
        }
    
        function removeInput(childId)  
        {  
            var ele = document.getElementById(childId);  
            var parentEle = document.getElementById('photoField');  
                      
            parentEle.removeChild(ele);  
            counter--;
        }  

        function submit_tags(){
            var num = $('.tag > span').length;
            var tags = new Array();
            var data_tags = "";
            for(var i =0; i < num; i++){
                var text = $('.tag > span:eq('+i+')').text();
                tags[i] = text.replace(/^\s\s*/, '').replace(/\s\s*$/, '');
                console.log(tags[i]);
                if(data_tags != ''){
                    data_tags += ", ";
                }
                data_tags += tags[i];
            }
            $('#data-tags').val(data_tags);
        }

        $(document).ready(function(){  
            $('#refTag').tagsInput({
                'width':'auto',
                'autocomplete_url' : '<?=$api["tag-autocomplete"];?>',
                'autocomplete':{selectFirst:true,width:'80px',autoFill:true}
            });
        });
    </script>
    <script src="js/globalJS.js" type="text/javascript"></script>
</body></html>
