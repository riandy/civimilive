<?php
ob_start("ob_gzhandler");
require_once("packages/require.php");
include("packages/check_login.php");//USED BY ALL PAGE BUT index.php
include("controller/controller_growth.php");
$page_name = "adminMgr-Growth.php";
$query_page = "letter=$letter";
$period_page = "rangedate=$O_date";
$previous_page = "previous=$check";
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <?php include('packages/head.php');?>
    <!-- calendar -->
    <link rel="stylesheet" type="text/css" href="packages/jQuery-Tags/jquery-ui.css" />
    <link rel="stylesheet" type="text/css" href="packages/jQuery-Tags/jquery.tagsinput.css" />
    <script src="//code.jquery.com/jquery-1.10.2.js"></script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.0/themes/smoothness/jquery-ui.css">
    <script src="//code.jquery.com/ui/1.11.1/jquery-ui.js"></script>
    <script src="js/jquery.select-to-autocomplete.js"></script>
    <!-- Include Required Prerequisites -->
    <script type="text/javascript" src="//cdn.jsdelivr.net/jquery/1/jquery.min.js"></script>
    <script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap/latest/css/bootstrap.css" />     
    <!-- Include Date Range Picker -->
    <script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />

    <style>
        body .modal {
            width: 50%; /* desired relative width */
            left: 25%; /* (100%-width)/2 */
            /* place center */
            margin-left:auto;
            margin-right:auto; 
        }
    </style>
</head>
<body class="civMain" onload="startTime()">
    <?php require_once("admin-Header.php");?>

    <!-- start top nav -->
    <?php $curPage='growth'; ?>
    <?php require_once("admin-top_nav.php");?>
    <!-- end top nav -->

    <div class="container">
        <div id="menuDashboard" class="row-fluid">
            <div id="rightContent" class="span12">
                
                <?php
                if($message!=null)
                {
                    echo "<div id='message1' class='alert alert-info'>" . $message . "</div><br />";
                }
                ?>
                <div class="row-fluid">
                    <div class="span8 align-left">
                        <h3 style="margin-top: 6px;">Growth Management User</h3>
                    </div>
                    <div class="span4 align-right">
                        <form action="" method="get">
                            <table>
                                <tr>
                                    <td><input id="reportrange" name="rangedate" class="text pull-right" type="text" style="width: 210px; height: 35px; text-align: center;" onchange="return apply();" value="<?php echo $O_date;?>"></td> 
                                    <td><button style="margin-bottom: 10px; width: 125px;" id="btn-submit" type="submit" class="btn btn-info btn-submit"><i class="icon-search icon-white"></i> Search</button></td>
                                </tr>
                                <tr>
                                    <td colspan="2"><input <?php if($check == "yes"){echo "checked";}?> name="previous" style="margin-bottom: 5px" type="checkbox" value="yes"> Compare to previous</td>
                                </tr>
                            </table>
                        </form>
                    </div>
                </div>

                <!-- HIGHCHART !-->
                <?php if($graph == "yes"){ ?>
                <hr>
                <div class="row-fluid">
                    <div class="span12 align-center">
                        <div id="container" style="min-width: 400px; height: 400px; margin: 0 auto"></div>
                    </div>
                </div>
                <?php } ?>
                <!-- END HIGHCHART !-->

                <hr/>
                <!-- STARTS LETTER -->
                <div class="row-fluid">
                    <div class="span12 align-center">
                        <?php for ($i = 'a', $j = 1; $j <= 26; $i++, $j++) {
                                $letters[$j] = $i;
                    ?>
                    <a href="<?php echo "adminMgr-Growth.php?page=1&letter=$letters[$j]&rangedate=$O_date&previous=$check";?>" class="btn-mini <?php  if($letter == $letters[$j] ){echo 'active-alphabet';} ?>" ><b><?php echo strtoupper($letters[$j]);?></b></a>&nbsp;
                    <?php } ?>
                    </div>
                </div><!-- ENDS LETTER -->

                <hr>
                <div class="row-fluid">
                    <div class="span12">
                        <table class="table table-hover up2">
                        <thead >
                            <tr>
                                <th>Date</th>
                                <th>Start With</th>
                                <th>Showing</th>
                                <th>Total Data</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td><span style="font-size: 13px;" class="label label-primary"><?php echo $table_date;?></span></td>
                                <td><span style="font-size: 13px;" class="label label-primary"><?php if($letter == ''){ echo "A-Z"; }else{ echo strtoupper($letter);}?></span></td>
                                <td><span style="font-size: 13px;" class="label label-primary"><?php echo (($O_page-1)*20+1)." - ".(count($data_users)+(($O_page-1)*20));?></span></td>
                                <td><span style="font-size: 13px;" class="label label-primary"><?php echo $total_data;?></span></td>
                            </tr>
                        </tbody>
                        </table>
                    </div>
                </div>

                <!-- TABLE DATA GROWTH USER !-->
                <hr/>
                <table class="table table-hover up2">
                <thead >
                    <tr>
			            <th>#</th>
                        <th>Picture</th>
                        <th>Name</th>
                        <th>Username</th>
                        <th>E-mail</th>
                        <th>Via</th>
                        <th>Progress Personal</th>
                        <th># Edu</th>
                        <th># Work</th>
                        <th>Join Date</th>
                        <th style="text-align:center;width:70px !important;">Action</th>
                    </tr>
                </thead>
                <?php
                if(!is_array($data_users)){
                    echo "
                    <tbody><td colspan='8'><h5>There is no data.</h5></td></tbody>";
                }else{?>
                <tbody>
                <?php $k=1;
                    foreach($data_users as $data_user){
                        $onclickSend = "\"if(window.confirm('Are sure want to resend activation to {$data_user['User_email']}?')) location.href='adminMgr-Growth.php?action=resend&username={$data_user['User_username']}&email={$data_user['User_email']}&code={$data_user['User_actNum']}';\"";
                    
                        //check progress personal
                        $N_array = array(
                            array('module' => 'image', 'field' => $data_user['User_proPhotoThmb']),
                            array('module' => 'profession', 'field' => $data_user['User_interest']),
                            array('module' => 'city', 'field' => $data_user['User_city']),
                            array('module' => 'state', 'field' => $data_user['User_state']),
                            array('module' => 'country', 'field' => $data_user['User_country']),
                            array('module' => 'phone', 'field' => $data_user['User_phone']),
                            array('module' => 'website', 'field' => $data_user['User_website'])
                        );
                        $progress_personal = check_progress("personal", $N_array)."%";
                        //$progress_education = check_progress("education", $data_user['edu_counter'])."%";
                    ?>
                    <tr class="<?php if($data_user['User_actStatus'] == 0){echo 'error';}?>">
	                    <td><?php echo ($O_page-1)*20+$k;?></td>
                        <td><a class="proPhoto" href="<?php echo check_image_url($data_user['User_proPhoto'], 'user');?>"><img class="img-circle" src="<?php echo check_image_url($data_user['User_proPhotoThmb'], 'user');?>" /></a></td>
                        <td><?php echo $data_user['User_fname']." ".$data_user['User_lname'];?></td>
                        <td><?php echo $data_user['User_username'];?></td>
                        <td><?php echo $data_user['User_email'];?></td>
                        <td><?php echo $data_user['User_login_via'];?></td>

                        <!-- progress personal !-->
                        <td><?php if(intval($progress_personal) < 50){$bar = "progress-danger";}else if(intval($progress_personal) < 59){$bar = "progress-warning";}else if(intval($progress_personal) < 79){$bar = "progress-success";}else{$bar = "progress-info";}?>
                            <div class="progress <?=$bar;?> progress-striped">
                                <div class="bar" style="width: <?php if(intval($progress_personal) == 0){echo '100%';}else{echo $progress_personal;}?>">
                                   <?php echo $progress_personal;?>
                                </div>
                            </div>
                        </td>

                        <!-- progress education !-->
                        <td><?php echo $data_user['edu_counter'];?></td>
                        <!--<td><?php if(intval($progress_education) < 50){$bar = "progress-danger";}else if(intval($progress_education) < 85){$bar = "progress-success";}else{$bar = "progress-info";}?>
                            <div class="progress <?=$bar;?> progress-striped">
                                <div class="bar" style="width: <?php if(intval($progress_education) == 0){echo '100%';}else{echo $progress_education;}?>">
                                   <?php echo $progress_education;?>
                                </div>
                            </div>
                        </td>!-->

                        <!-- progress work !-->
                        <td><?php echo $data_user['work_counter'];?></td>
                        <td><?php echo $data_user['User_create_date'];?></td>
                        <td style="text-align: center;">
                            <div class="btn-group btn-group-vertical"> 
                                <a target="_blank" href="<?php echo $global['user-profile'].$data_user['User_username'];?>.cvm" type="button" class="btn btn-info btn-small" style="color: white;"><i class="icon-user icon-white"></i> view profile</a>
                                <?php if($data_user['User_actStatus'] == 0){?>
                                <a href="#" <?php echo "onclick=$onclickSend"; ?> type="button" class="btn btn-success btn-small" style="color: white;"><i class="icon-envelope icon-white"></i> resend</a>
                                <?php } ?>
                            </div>
                        </td>
                    </tr>
                <?php $k++; }?>
                </tbody>
                <? }?>
                </table>

                <!-- START PAGINATION !-->
                <center>
                    <div class="pagination">
                        <ul class="pagination pagination-sm">
                            <?php
                            $batch = getBatch($O_page);
                            if($batch < 1){$batch = 1;}
                            $prevLimit = 1 +(10*($batch-1));
                            $nextLimit = 10 * $batch;

                            if($nextLimit > $total_page){
                                $nextLimit = $total_page;
                            }
                            if ($total_page > 1 && $O_page > 1) {
                                echo "<li><a href='".$page_name."?page=1&".$query_page."&".$period_page."&".$previous_page."' class='pagination-slide'>&lt;&lt; First</a></li>";
                            }
                            if($batch > 1 && $O_page > 10){
                                echo "<li><a href='".$page_name."?page=".($prevLimit-1)."&".$query_page."&".$period_page."&".$previous_page."' class='pagination-slide'>&lt; Previous 10</a></li>";
                            }
                            for($mon = $prevLimit; $mon <= $nextLimit;$mon++){ ?>
                                <li class="<?php if($mon == $O_page){echo 'active';}?>"><span class="pagination-theme"><a href="<?php echo $page_name."?page=".$mon."&".$query_page."&".$period_page."&".$previous_page;?>" ><?php echo $mon;?></a></span></li>
                                <?php if($mon == $nextLimit && $mon < $total_page){
                                    if($mon >= $total_page){ 
                                        $mon -=1;
                                    }
                                    echo "<li><a href='".$page_name."?page=".($mon+1)."&".$query_page."&".$period_page."&".$previous_page."' class='pagination-slide'>Next 10 &gt;</a></li>";
                                }                               
                            }
                            if ($total_page > 1 &&  ($O_page != $total_page)) {
                                echo "<li><a href='".$page_name."?page=".$total_page."&".$query_page."&".$period_page."&".$previous_page."' class='pagination-slide'>Last &gt;&gt;</a></li>";
                            }
                            ?>
                        </ul>
                    </div>
                </center>
                <!-- END PAGINATION !-->
            </div>
        </div>
        <div class="row-fluid">
            <div class="span6">
                <?php require_once("admin-Footer.php"); ?>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $("#message1").fadeOut(8400);
        function add_zindex(){
            $('.ui-autocomplete').css("z-index", 1055);
        }

        $(function() {

            function cb(start, end) {
                $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
            }
            cb(moment().subtract(29, 'days'), moment());

            $('#reportrange').daterangepicker({
                ranges: {
                   'Today': [moment(), moment()],
                   'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                   'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                   'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                   'This Month': [moment().startOf('month'), moment().endOf('month')],
                   'This Year': [moment().startOf('year'), moment().endOf('year')]
                   //'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                }
            }, cb);

        });

        function apply(){
            var reportrange = $('#reportrange').val();
            //$('#result-range').val(reportrange); 
        }
    </script>
    <script src="js/globalJS.js" type="text/javascript"></script>
    <!-- HIGHCART !-->
    <script src="http://code.highcharts.com/highcharts.js"></script>
    <script src="http://code.highcharts.com/modules/exporting.js"></script>
    <script type="text/javascript">
        <?php $result_date = explode("-", $start_date); 
            if($check == "yes"){ $previous_date = explode("-", $previousStart);} ?>
              
        $(function () {
            $('#container').highcharts({
                title: {
                    text: 'Overview',
                    x: -20 //center
                },
                subtitle: {
                    <?php if($check == "yes"){ ?> //if compare to previous
                    text: '<?=$table_date;?> (Result) <br>vs<br> <?=$table_previous;?> (Previous)',
                    <?php }else{ ?>
                    text: '<?=$table_date;?>',
                    <?php } ?>   
                    x: -20
                },
                chart: {
                    renderTo: 'container',
                    width: 940,
                    height: 370
                },
                xAxis: [{
                    type: 'datetime'
                    }

                    <?php if($check == "yes"){ ?> //if compare to previous
                    , {
                    type: 'datetime',
                    opposite: true
                    } 
                    <?php } ?>
                ],
                yAxis: {
                    title: {
                        text: '# of user register'
                    },
                    plotLines: [{
                        value: 0,
                        width: 1,
                        color: '#808080'
                    }]
                },
                tooltip: {
                    valueSuffix: ' user'
                },
                legend: {
                    layout: 'vertical',
                    align: 'right',
                    verticalAlign: 'middle',
                    borderWidth: 0
                },
                series: [{
                    name: 'Result',
                    color: '#4f81bd',
                    data: [<?=$sub_result;?>],
                    pointStart: Date.UTC(<?=$result_date[0];?>, <?=$result_date[1]-1;?>, <?=$result_date[2];?>), //Date.UTC(data.year, data.month(0-11), data.day)
                    pointInterval: 24 * 3600 * 1000 // one day
                    }

                    <?php if($check == "yes"){ ?> //if compare to previous
                    , {
                    name: 'Previous',
                    color: '#c0504d',
                    data: [<?=$sub_previous;?>],
                    pointStart: Date.UTC(<?=$previous_date[0];?>, <?=$previous_date[1]-1;?>, <?=$previous_date[2];?>), //Date.UTC(data.year, data.month(0-11), data.day)
                    pointInterval: 24 * 3600 * 1000, // one day
                    xAxis: 1
                    }
                    <?php } ?>
                ]
            });
        });
        </script>
</body></html>