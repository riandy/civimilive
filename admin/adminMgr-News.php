<?php
ob_start("ob_gzhandler");
require_once("packages/require.php");
include("packages/check_login.php");//USED BY ALL PAGE BUT index.php
include("controller/controller_news.php");
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <?php include('packages/head.php');?>
    <!-- calendar -->
    <link rel="stylesheet" type="text/css" href="packages/jQuery-Tags/jquery-ui.css" />
    <link rel="stylesheet" type="text/css" href="packages/jQuery-Tags/jquery.tagsinput.css" />
    <script src="//code.jquery.com/jquery-1.10.2.js"></script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.0/themes/smoothness/jquery-ui.css">
    <script src="//code.jquery.com/ui/1.11.1/jquery-ui.js"></script>
    <script src="js/tinymce/tinymce.min.js"></script>
    <script type="text/javascript" src="packages/jQuery-Tags/jquery-ui.js"></script>
    <script type="text/javascript" src="packages/jQuery-Tags/jquery.tagsinput.js"></script>
  
    <script>
        tinymce.init({
            selector: "textarea",
            plugins: [
            "advlist autolink lists link image charmap print preview anchor",
            "searchreplace visualblocks code fullscreen",
            "insertdatetime media table contextmenu paste"
            ],
            toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
            relative_urls: false,
            forced_root_block : false,
            height : 300
        });

        $(function() {
            $( ".datepicker" ).datepicker({dateFormat: 'yy-mm-dd' });
        });
    </script>
    <style>
        body .modal {
            width: 50%; /* desired relative width */
            left: 25%; /* (100%-width)/2 */
            /* place center */
            margin-left:auto;
            margin-right:auto; 
        }
    </style>
</head>
<body class="civMain" onload="startTime()">
    <?php require_once("admin-Header.php"); ?>

    <!-- start top nav -->
    <?php $curPage='blog'; ?>
    <?php require_once("admin-top_nav.php");?>
    <!-- end top nav -->

    <div class="container">
        <div id="menuDashboard" class="row-fluid">
            <div id="rightContent" class="span12 photoAlbum">
		        <?php 
                if($message!=null)
                {
                    echo "<div class='alert alert-info' id='message1'>" . $message . "</div>";
                }
                ?>
                <div class="row-fluid">
                    <div class="span8 align-left">
                        <h4 style="margin: 0;">News Management</h4>
                    </div>
                    <div class="span4 align-right">
                        <button class="btn btn-info" type="button" data-toggle="modal" data-target="#dialog-addModule"><i class="icon-plus icon-white"></i> News</button>
                    </div>
                </div>
                <hr />
                <div class="row-fluid">
                    <div class="span12" align="right">
                        Total News : <span class="badge badge-info"><?php echo $O_totalNum;?></span>
                    </div>
                </div>
                <div class="row-fluid">
                    <div class="span12">
                        <table class="table table-hover">
                            <tr>
                                <th>#</th>
                                <th>Create Date</th>
                                <th>Title</th>
                                <th>Category</th>
                                <th>Featured</th>
                                <th>Author</th>
                                <th>Publish</th>
                                <th>Image</th>
                                <th style="text-align: center;">Action</th>
                            </tr>
                            <?php
                            if (!is_array($datas)) {
                                echo "<tr><td colspan='9'><h5>There is currently no news.</h5></td></tr>";
                            } else { $count = 1;
                                foreach($datas as $data) {
                                    $onclickDel = "\"if(window.confirm('Are you sure you want to delete news {$data['News_title']}?')) location.href='adminMgr-News.php?action=delete&news_ID={$data['News_ID']}&news_Title={$data['News_title']}';\"";
                                    ?>
                                    <tr class="<?php if($data['News_publish'] == 'Not Publish'){echo 'error';}?>">
                                        <td><?php echo ($O_page-1)*12+$count;?></td>
                                        <td><?php echo date('d M y, h:i:s',strtotime($data['News_create_date']));?></td>
                                        <td><?php echo charLength($data['News_title'], 40);?></td>
                                        <td><?php echo $data['Nc_title'];?></td>
                                        <td><?php if($data['News_featured'] != 0){ echo "Yes";}else{echo "No";}?></td>
                                        <td><?php echo $data['News_author'];?></td>
                                        <td><?php echo $data['News_publish']?></td>
                                        <td><a class="proPhoto" href="<?php echo $data['Np_img'];?>"><img class="img-circle" src="<?php echo $data['Np_img_thmb'];?>" /></a></td>
                                        <td style="text-align:center;">
                                            <div class="btn-group btn-group-vertical">
                                                <a href="adminMgr-News_detail.php?news_ID=<?php echo $data['News_ID'];?>" type="button" class="btn btn-small"><i class="icon-edit"></i> edit</a>
                                                <a href="#" <?php echo "onclick=$onclickDel"; ?> type="button" class="btn btn-danger btn-small" style="color: white;" ><i class="icon-trash icon-white"></i> delete</a>
                                                <a target="_blank" href="<?=$global['tips'].encode($data['News_title'])."_".$data['News_ID'].".html";?>" type="button" class="btn btn-info btn-small" style="color: white;"><i class="icon-list-alt icon-white"></i> view news</a>
                                            </div>
                                        </td>
                                    </tr>
                                    <?php $count++;
                                }
                            }
                            ?>
                        </table>
                    </div>
                </div>
                <hr/>
                <div class="row-fluid">
                    <div class="offset2 span7" align="center">
                        <div class="pagination">
                            <ul>
                                <?php
                                if ($total_page >= 3 && $O_page > 2) {
                                    echo "<li><a href='adminMgr-News.php?page=1'>First</a></li>";
                                }
                                if ($O_page > 1) {
                                    echo "<li><a href='adminMgr-News.php?page=" . ($O_page - 1) . "'>" . ($O_page - 1) . "</a></li>";
                                }
                                echo "<li class='active'><a href='adminMgr-News.php?page=$O_page' >$O_page</a></li>";
                                if ($total_page > 1 && $O_page <= $total_page - 1) {
                                    echo "<li><a href='adminMgr-News.php?page=" . ($O_page + 1) . "'>" . ($O_page + 1) . "</a></li>";
                                }
                                if ($total_page > 2 && $O_page != $total_page - 1 && $O_page != $total_page) {
                                    echo "<li><a href='adminMgr-News.php?page=$total_page'>Last</a></li>";
                                }
                                ?>
                            </ul>
                        </div>
                    </div>
                    <div class="span2" style="padding:25px 0 20px 0;">
                        <div align="right">
                            <span>Page <?php echo $O_page; ?> of <?php echo $total_page; ?></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row-fluid">
            <div class="span6">
                <?php require_once("admin-Footer.php"); ?>
            </div>
        </div>
    </div>
    <!--Modal box starts -->  
    <div id="dialog-addModule" class="modal hide fade">    
        <form name="createNews" action="adminMgr-News.php?action=addNews" method="post" enctype="multipart/form-data" onsubmit="loadingText()" onclick="add_zindex();">
            <div class="modal-header">
                <a href="#close" class="close" data-dismiss="modal" aria-hidden="true">×</a>
                <strong>Create Data News</strong>
            </div>
            <div class="modal-body">
                <div class="row-fluid">
                    <div class="span3 align-right modal-label"><strong>Category&nbsp;*</strong>:</div>
                    <div class="span9">
                        <select name="News_category" type="text" class="text input-block-level" required>
                            <option value="">-Choose One-</option>
                            <?php foreach ($categorys as $cat) {?>
                            <option value="<?php echo $cat['Nc_ID']; ?>"><?php echo $cat['Nc_title']; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="row-fluid">
                    <div class="span3 align-right modal-label"><strong>Title&nbsp;*</strong>:</div>
                    <div class="span9"><input name="News_title" type="text" class="text input-block-level" required="required" maxlength="256" placeholder="i.e. Title issue (max 256 chars)" /></div>
                </div>
                <div class="row-fluid">
                    <div class="span3 align-right modal-label">Featured :</div>
                    <div class="span9">
                        <label class="checkbox">
                            <input name="News_featured" type="checkbox" value="1">
                        </label>
                    </div>
                </div>
                <div class="row-fluid">       
                    <div class="span3 align-right modal-label"><strong>Photo(s)</strong></div>
                    <div class="span8">
                        <div id="photoField">
                            <input name="photo[]" type="file" class="file" required="required"/>
                            <span><strong>*Primary</strong></span>
                        </div>
                    </div>
                </div>
                <div class="row-fluid">
                    <div class="offset3 span8"><small>image format has to be jpg, jpeg, gif, png.</small><br/><small>Image will be best shown in 1200px by 800px</small></div>
                </div><br/>
                <div class="row-fluid">
                    <div class="span3 align-right modal-label">Content :</div>
                        <div class="span9">
                            <textarea rows="5" name="News_content" class="textarea text input-block-level" maxlength="4096" placeholder="i.e. Description issue (max 4096 chars)"  ></textarea>
                        </div>
                </div><br/>
                <div class="row-fluid">
                    <div class="span3 align-right modal-label">Tags :</div>
                    <div class="span9"><input id="refTag" name="tag" class="tags" value="" /></div>
                </div><br>
                <div class="row-fluid">
                    <div class="span3 align-right modal-label">Photographer :</div>
                    <div class="span9"><input name="News_photographer" type="text" class="text input-block-level" maxlength="256" placeholder="i.e. Photographer, (max 512 chars)" /></div>
                </div>
                <div class="row-fluid">
                    <div class="span3 align-right modal-label">Photographer Link:</div>
                    <div class="span9"><input name="News_photographer_link" type="url" class="text input-block-level" placeholder="input URL of blog, instagram, website (http://www.civimi.com)" /></div>
                </div>
                <div class="row-fluid">
                    <div class="span3 align-right modal-label">Author :</div>
                    <div class="span9"><input name="News_author" type="text" class="text input-block-level" maxlength="256" placeholder="i.e. Author, (max 512 chars)" /></div>
                </div>
                <div class="row-fluid">
                    <div class="span3 align-right modal-label">Author Link :</div>
                    <div class="span9"><input name="News_author_link" type="url" class="text input-block-level" placeholder="input URL of blog, instagram, website (http://www.civimi.com)" /></div>
                </div>
                <div class="row-fluid">
                    <div class="span3 align-right modal-label">Video Url :</div>
                    <div class="span9">
                        <input name="News_video_link" type="url" class="text input-block-level" placeholder="input URL Video (http://youtu.be/c1v1m1)" /><br/>
                        <a class="btn" data-toggle="modal" data-target="#dialog-info">Info</a>
                    </div>
                </div><br/>
                <div class="row-fluid">
                    <div class="span3 align-right modal-label">Publish :</div>
                    <div class="span9">
                        <select id="publish" name="News_publish" class="text">
                            <option value="Publish">Publish</option>
                            <option value="Not Publish">Not Publish</option>
                        </select>
                    </div>
                </div>
                <div class="row-fluid">
                    <div id="SubStatus" class="span12 align-center"></div>
                    <input type="hidden" name="News_tags" id="data-tags" value=""/>
                </div>
            </div>
            <div class="modal-footer">
                <div class="btn-group">
                    <a href="#" class="btn" data-dismiss="modal"><i class="icon-remove"></i> Close</a>
                    <button type="button" class="btn btn-info" onClick="addInput('photoField');"><i class="icon-picture icon-white"></i> Add Photo</button>
                    <button id="btn-submit" name="submitNews" onmouseover="submit_tags();" onclick="submit_notif();" type="submit" class="btn btn-success btn-submit" ><i class="icon-plus-sign icon-white"></i> Create</button>
                </div>
            </div>
        </form> 
    </div>
    <!--Modal box ends --> 
    <!--Modal box video -->  
    <!--<div id="dialog-info" class="modal hide fade" style="width:50%;">
        <div class="modal-header">
            <a href="#close" class="close" data-dismiss="modal" aria-hidden="true">×</a>
            <strong>Petunjuk untuk Copy link Video Youtube</strong>
        </div>
        <div class="modal-content">
            <div>
                <img src="images/INFO-VIDEO.png" style="width:100%;"/>
            </div>
        </div>
    </div>
    
    <div id="dialog-info" class="modal hide fade" style="width:50%;">
        <div class="modal-header">
            <a href="#close" class="close" data-dismiss="modal" aria-hidden="true">×</a>
            <strong>Petunjuk untuk Copy link Video Youtube</strong>
        </div>
        <div class="modal-content">
            <div>
                <img src="images/INFO-VIDEO-COMETO.png" style="width:100%;"/>
            </div>
        </div>
    </div>!-->
    <script type="text/javascript">
        function add_zindex(){
            $('.ui-autocomplete').css("z-index", 1055);
        }
        $("#message1").fadeOut(8400);
        //$(document).ready(function(){$("a.proPhoto").fancybox({helpers: {title: {type: "inside"}}, topRatio: 0.5, fixed: true, autoSize: false, closeClick: false, openEffect: "fade", closeEffect: "fade"})});

        var counter = 1;
        var limit = 6;
        var num = 1;
        function addInput(divName){
            if (counter == limit)  {
                alert("You may upload as many photos as you like, but you can only upload "+counter+" photos at a time.");
            }
            else {
                var newdiv = document.createElement('div');
                newdiv.id = "Photo"+ (num + 1); 
                newdiv.innerHTML = "<br /><input name='photo[]' type='file' class='file' /></div>";
                            
                newdiv.innerHTML += "&nbsp;&nbsp;<a href='#js' title='remove field' onClick=\"removeInput('Photo" + (num + 1) + "');\" ><i class='icon-remove'></i></a>";
                document.getElementById(divName).appendChild(newdiv);
                counter++;
                num++;
            }
        }
    
        function removeInput(childId)  
        {  
            var ele = document.getElementById(childId);  
            var parentEle = document.getElementById('photoField');  
                      
            parentEle.removeChild(ele);  
            counter--;
        }  

        function submit_tags(){
            var num = $('.tag > span').length;
            var tags = new Array();
            var data_tags = "";
            for(var i =0; i < num; i++){
                var text = $('.tag > span:eq('+i+')').text();
                tags[i] = text.replace(/^\s\s*/, '').replace(/\s\s*$/, '');
                console.log(tags[i]);
                if(data_tags != ''){
                    data_tags += ", ";
                }
                data_tags += tags[i];
            }
            $('#data-tags').val(data_tags);
        }

        $(document).ready(function(){  
            $('#refTag').tagsInput({
                'width':'auto',
                'autocomplete_url' : '<?=$api["tag-autocomplete"];?>',
                'autocomplete':{selectFirst:true,width:'80px',autoFill:true}
            });
        });

        function submit_notif(){
            var publish = $('#publish').val();
            if(publish != "Not Publish"){
                //var url = "<?=$api['user-hits'];?>";
                var url = "http://www.civimi.com/beta/api/api_news_user_mobile.php?action=send_notification";
                $.ajax({type:'POST', url: url, success:function(result){
                    console.log(result.message);
                }
              });
            }
        }
    </script>
    <script src="js/globalJS.js" type="text/javascript"></script>
</body></html>