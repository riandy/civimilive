<?php 

require_once("../model/Connection.php");
$obj_con = new Connection();

require_once("../model/Skill.php");
$obj_skill = new Skill();

if($_SESSION['skill'] == "Yes"){ // ACCESS ROLE SKILL PAGE
    if($_GET['action'] == '' || $_GET['action'] == null){
        $obj_con->up();

        $O_page = 1;
        if(isset($_GET['page'])){
            $O_page = mysql_real_escape_string(check_input($_GET['page']));
        }
        if(isset($_GET['letter'])){
            $letter = mysql_real_escape_string(check_input($_GET['letter']));
        }

        $data_lists = $obj_skill->get_index();//GET DATA SKILL
        $total_data = $obj_skill->get_total($letter,0);//GET TOTAL ITEM
        if($total_data == 0){
            echo mysql_error();
        }
        $total_page = $obj_skill->get_total_page($total_data);//GET TOTAL PAGE

        $data_skills = $obj_skill->get_data_by_page($O_page, $letter, 0);//GET DATA PER PAGE
        //var_dump($data_skills);

        $message = $_SESSION['status'];
        unset($_SESSION['status']);
        $obj_con->down();
    }else if($_GET['action'] == 'insert'){
        $obj_con->up();

        $N_name = mysql_real_escape_string(check_input($_POST['name']));
        $N_publish = mysql_real_escape_string(check_input($_POST['publish']));
        $N_admin = $_SESSION['admin_id']; //admin session id
        
        if($_FILES["image"]["name"]!=null){
            $ran = rand();
            $timestamp = time();

            //to upload in server
            $Photo_ImgLink = "../image/images/skill/" . $timestamp . $ran . cleanSpace($_FILES["image"]["name"]);
            $ThmbPhoto_ImgLink = "../image/images/skill-thmb/" . $timestamp . $ran . cleanSpace($_FILES["image"]["name"]);
            //to save in database
            $Photo_ImgLink1 = "image/images/skill/" . $timestamp . $ran . cleanSpace($_FILES["image"]["name"]);
            $ThmbPhoto_ImgLink1 = "image/images/skill-thmb/" . $timestamp . $ran . cleanSpace($_FILES["image"]["name"]);

            if ((($_FILES["image"]["type"] == "image/gif") || ($_FILES["image"]["type"] == "image/jpeg") || ($_FILES["image"]["type"] == "image/jpg") || ($_FILES["image"]["type"] == "image/JPG") || ($_FILES["image"]["type"] == "image/png")) && ($_FILES["image"]["size"] < 10048576) && ($_FILES["image"]["name"] != "")) {
                require_once("packages/imageProcessor.php");
                if ($_FILES["image"]["error"] > 0) {
                    //echo "Return Code: " . $_FILES["file"]["error"][$k] . "<br />";
                } else {
                    $file = $_FILES["image"]["tmp_name"];
                    //Resizing images
                    if (true !== ($pic_error = @resampimage(1280, 500, "$file", "$Photo_ImgLink", 0))) {
                        echo $pic_error;
                        unlink($Photo_ImgLink);
                    }

                    if (true !== ($pic_error1 = @resampimagethmb("$file", "$ThmbPhoto_ImgLink", 0))) {
                        echo $pic_error1;
                        unlink($ThmbPhoto_ImgLink);
                    }

                    $result = $obj_skill->insert_data_image($N_name, $Photo_ImgLink1, $ThmbPhoto_ImgLink1, $N_publish, $N_admin);
                    if(!$result){
                        $message .= "Something is wrong with your submission.<br />";
                    }else if($result){
                        $message .= "<b>\"" . $N_name . "\"</b> has been succesfully added as a new skill image.<br />";
                    }else{
                        //header('Location: logout.php');               header("Location:logout.php");
                        die();
                    }
                }
            }else if ($_FILES["image"]["name"] != "") {
                $message .= "Something is wrong with your uploaded skill image, please make sure that the format is (jpg, png, gif) and the size is less than 9.5MB.<br />";
            }
        }else{
            $result = $obj_skill->insert_data($N_name, $N_publish, $N_admin);
            if(!$result){
                $message .= "Something is wrong with your editing.<br />";
            }else if($result){
                $message .= "<b>\"" . $N_name . "\"</b> has been succesfully added as a new skill.<br />";
            }else{
                //header('Location: logout.php');           header("Location:logout.php");
                die();
            }
        }
       
        $_SESSION['status'] = $message;
        header("Location:adminMgr-Skill.php");
        $obj_con->down();
    }else if($_GET['action'] == 'delete' && $_GET['skill_ID'] != '' && $_SESSION['admin_role'] != 'Inputer'){
        $obj_con->up();
        //$O_type = mysql_real_escape_string(check_input($_GET['type']));
        $O_id = mysql_real_escape_string(check_input($_GET['skill_ID']));
        $O_name = mysql_real_escape_string(check_input($_GET['skill_Name']));
        //start transaction
        mysql_query("autocommit(FALSE)");

        $result_remove = $obj_skill->remove_image($O_id);
        if($result_remove){
            $result = $obj_skill->delete_data($O_id);
            if($result <= 0){
                $message .= "Image is deleted but something wrong while deleting the Data<br />";
            }else if($result == 1){
                $message .= "Skill name <b>\"" . $O_name . "\"</b> has been deleted successfully.<br />";
            }
        }else{
            $result = $obj_skill->delete_data($O_id);
            if($result <= 0){
                $message .= "Something is wrong while deleting the Image, please try again.<br />";
            }else if($result == 1){
                $message .= "Skill name <b>\"" . $O_name . "\"</b> has been deleted successfully.<br />";
            }
        }
        //End of transaction
        mysql_query("commit()");
        mysql_query("autocommit(TRUE)");

        $_SESSION['status'] = $message;
        header("Location:adminMgr-Skill.php"); 
        $obj_con->down();
    }else{
        header('Location:adminMgr.php');
    }
}else{
    header('Location:adminMgr.php');
}
?>