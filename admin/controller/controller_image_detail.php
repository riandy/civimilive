<?php

require_once("../model/Connection.php");
$obj_con = new Connection();

require_once("../model/Tagging.php");
$obj_tagging = new Tagging(); 

require_once("../model/Ref_Tag.php");
$obj_rtag = new Ref_Tag();

require_once("../model/Photo.php");
$obj_photo = new Photo();

if($_SESSION['admin'] == "Yes"){ // ACCESS ROLE IMAGE PAGE
    if($_GET['action'] == '' && $_GET['photo_ID'] != '' && $_SESSION['admin_role'] != 'Inputer'){
    	$obj_con->up();

        $table_tags = "photo";//FOR TAGS
    	$O_id = mysql_real_escape_string(check_input($_GET['photo_ID']));
        $O_title = mysql_real_escape_string(check_input($_GET['photo_Title']));
        
        $O_page = mysql_real_escape_string(check_input($_GET['page']));
        $O_sort = mysql_real_escape_string(check_input($_GET['sort']));

    	$data_photos = $obj_photo->get_data_edit($O_id);
        $data_tags = $obj_tagging->get_data_detail($table_tags, $O_id); 
        //var_dump($data_photos);

        $message = $_SESSION['status'];
        unset($_SESSION['status']);
    	$obj_con->down();
    }else if($_GET['action'] == 'update' && $_SESSION['admin_role'] != 'Inputer'){
    	$obj_con->up();
    	
    	$N_id = mysql_real_escape_string(check_input($_POST['id']));
        $N_title = mysql_real_escape_string(check_input($_POST['title']));
        $N_fullname = mysql_real_escape_string(check_input($_POST['fullname']));
        $N_publish = mysql_real_escape_string(check_input($_POST['publish']));

        $N_page = mysql_real_escape_string(check_input($_POST['page']));
        $N_sort = mysql_real_escape_string(check_input($_POST['sort']));

        $table_name = "photo";
        $N_data = $_POST['tags_input'];
        if($N_data != ""){
            $data = explode(", " , $N_data);
        }
       
    	$save_status = "Something is wrong with your editing.<br/>";
        $result_photo = $obj_photo->update_data($N_id, $N_title, $N_publish);
        $target_id = $N_id;
        $result_delete_tag = $obj_tagging->delete_data($target_id, $table_name);
        if($target_id){
            if(isset($data)){
                for($i = 0;$i < count($data);$i++){//START FOR
                    $check_rtag_id = $obj_tagging->check_exist($data[$i]);//CHECK IF TAG EXIST, IF SO RETURN THE rtag_id, else returns 0

                    if($check_rtag_id == 0){//IF THE TAG IS NOT EXIST
                        $result_rtag = $obj_rtag->insert_data(strip_tags($data[$i]));
                        if($result_rtag >= 1){
                            $result_tag = $obj_tagging->insert_data($table_name, $target_id, $result_rtag);
                        }
                    }else{//IF THE TAG EXIST, GO AHEAD AND INSERT INTO tbl_tagging
                        $result_tag = $obj_tagging->insert_data($table_name, $target_id, $check_rtag_id);
                    }
                }//END FOR
            }
            if($N_title != ""){$title = $N_title;}else{$title = "No title";}
            $message = "Photo title <b>\"" . $title . "\"</b> from " . $N_fullname . " has been succesfully edited.<br />";
        }
        
    	$_SESSION['status'] = $message;
    	header("Location:adminMgr-Image.php?page=".$N_page."&sort=".$N_sort);
    	$obj_con->down();
    }else if($_GET['action'] == 'message' && $_SESSION['admin_role'] != 'Inputer'){
        $N_id = $_POST['id'];
        $N_subject = strtolower($_POST['subject']);
        $N_title = $_POST['title'];
        $N_username = $_POST['username'];
        $N_email = $_POST['email'];
        $N_image = $_POST['image'];
        $N_tags = $_POST['tags_image'];
        $N_message = $_POST['message'];

        $N_page = $_POST['page'];
        $N_sort = $_POST['sort'];

        //mail
        $to = $N_email;
        $username = $N_username;
        $subject = ucwords($N_subject);
        
        $from = "From: no-reply@civimi.com \n";
        $from .= "Content-type: text/html \r\n";
        
        $message = "<html><head></head><body style='margin:0;'>";
        $message .= "<div id='all' style='width:600px;'>";
        $message .= "<div id='header' style='text-align:center;padding:20px 0;border-bottom: solid 2px #B5B5B5;margin-bottom: 30px;'>";
        $message .= "<a target='_blank' href='http://www.civimi.com'>"; 
        $message .= "<img src='http://www.civimi.com/beta/civimi/img/logo/logo-desktop.png' style='width:200px;' alt='civimi'>";
        $message .= "</a></div><div id='content' style='padding: 0 15px;'>";
        $message .= "Halo ".$username.",<br><br>";
        $message .= "<img src='".$N_image."' style='width:100%;'><br><br>";
        $message .= "<li>Photo Title : ".$N_title."</li>";
        $message .= "<li>Photo Tags : ".$N_tags."</li>";
        $message .= "<li>Message : ".$N_message."</li>";
        $message .= "<br><br>";
        $message .= "Regards,<br><br>";
        $message .= "Billy<br>";
        $message .= "Customer Success Manager<br>";
        $message .= "Civimi.com";
        $message .= "</div>";
        $message .= "<div id='footer' style='background-color: #666;padding: 10px 0;margin-top:20px;'>";
        $message .= "<div style='display: block;height: 50px;padding: 0 20px;position: relative;'>";
        $message .= "<div style='font-size: 20px;font-style: italic;color: #FFFFFF;font-weight: bold;display: block;width: 50%;float: left;'>'Reinvent Yourself'</div>";
        $message .= "<div style='display: block;float: left;width: 50%;position: relative;text-align: right;'>";
        $message .= "<a target='_blank' href='https://www.facebook.com/civimi'><img src='http://www.civimi.com/beta/civimi/img/logo/facebook.png' alt='facebook' style='width: 20px;'></a>";
        $message .= "<a target='_blank' href='https://plus.google.com/+Civimi'><img src='http://www.civimi.com/beta/civimi/img/logo/google.png' alt='google' style='width: 20px;margin-left:8px;'></a>";
        $message .= "<a target='_blank' href='https://twitter.com/civimi'><img src='http://www.civimi.com/beta/civimi/img/logo/twitter.png' alt='twitter' style='width: 20px;margin-left:8px;'></a>";
        $message .= "</div></div>";
        $message .= "<div style='text-align:center;font-size: 13px;color: #FFFFFF;margin:20px 0 0 0;padding:0 20px;position:relative;'>";
        $message .= "Copyright &copy; 2015 Civimi. All rights reserved.";
        $message .= "</div></div></div></body></html>";
        //echo $message;
        $sent = mail($to, $subject, $message, $from);
        if($sent){
            $message = "Send message image to <b>\"" . $N_username . "\"</b> has been succesfully.<br />";
        }else{
            $message = "Something is wrong."; 
        }

        $_SESSION['status'] = $message;
        header("Location:adminMgr-Image_detail.php?photo_ID=".$N_id."&photo_Title=".$N_title."&page=".$N_page."&sort=".$N_sort);
    }
    else{
        header('Location:adminMgr.php');
    }
}else{
    header('Location:adminMgr.php');
}
?>