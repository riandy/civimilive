<?php 

require_once("../model/Connection.php");
$obj_con = new Connection();

require_once("../model/Ref_City.php");
$obj_rcity = new Ref_City();

require_once("../model/Ref_Country.php");
$obj_rcountry = new Ref_Country();

if($_SESSION['city'] == "Yes"){ // ACCESS ROLE CITY PAGE
	if($_GET['action'] == '' || $_GET['action'] == null){
		$obj_con->up();

		$O_page = 1;
		if(isset($_GET['page'])){
			$O_page = mysql_real_escape_string(check_input($_GET['page']));
		}
		if(isset($_GET['letter'])){
			$letter = mysql_real_escape_string(check_input($_GET['letter']));
		}
		$table_tags = 'city';//FOR AJAX

		$total_data = $obj_rcity->get_total($letter,0);//GET TOTAL ITEM
		$total_page = $obj_rcity->get_total_page($total_data);//GET TOTAL PAGE

		$data_citys = $obj_rcity->get_data_by_page($O_page, $letter,0);//GET DATA PER PAGE
		$data_countrys = $obj_rcountry->get_index();
		
	    $message = $_SESSION['status'];
	    $_SESSION['status'] = null;
		$obj_con->down();
	}else if($_GET['action'] == 'insert'){
		$obj_con->up();

		$N_country_id = mysql_real_escape_string(check_input($_POST['country_id']));
		$N_title = mysql_real_escape_string(check_input($_POST['title']));
		$N_admin = mysql_real_escape_string(check_input($_SESSION['admin_id'])); //admin session id
		$N_publish = mysql_real_escape_string(check_input($_POST['publish']));

		$result = $obj_rcity->insert_data($N_country_id, $N_title, $N_admin, $N_publish);
		if($result == 1){
			$message .= "City <b>\"$N_title\"</b> is successfully saved.";
		}else{
			$message .= "City <b>\"$N_title\"</b> failed to be saved.";
		}

		$_SESSION['status'] = $message;
		header("Location:adminMgr-City.php");
		$obj_con->down();
	}else if($_GET['action'] == 'delete' && $_GET['city_ID'] != '' && $_SESSION['admin_role'] != 'Inputer'){
		$obj_con->up();

		$O_id = mysql_real_escape_string(check_input($_GET['city_ID']));
		$O_title = mysql_real_escape_string(check_input($_GET['city_Title']));

		$result = $obj_rcity->delete_data($O_id);
		if($result == 1){
			$message .= "City title <b>\"$O_title\"</b> is successfully deleted.";
		}else{
			$message .= "City title <b>\"$O_title\"</b> failed to be deleted.";
		}

		$_SESSION['status'] = $message;
		header("Location:adminMgr-City.php");
		$obj_con->down();
	}else{
    	header('Location:adminMgr.php');
	}
}else{
    header('Location:adminMgr.php');
}
?>