<?php 

require_once("../model/Connection.php");
$obj_con = new Connection();

require_once("../model/Company.php");
$obj_company = new Company();

require_once("../model/Tagging.php");
$obj_tagging = new Tagging();

require_once("../model/Ref_Tag.php");
$obj_rtag = new Ref_Tag();

require_once("../model/Ref_City.php");
$obj_rcity = new Ref_City();

require_once("../model/Ref_Country.php");
$obj_rcountry = new Ref_Country();

require_once("../model/Industry.php");
$obj_industry = new Industry();

if($_SESSION['company'] == "Yes"){ // ACCESS ROLE COMPANY PAGE
    if($_GET['action'] == '' && ($_GET['type'] != '' || $_GET['type'] == null)){
        $obj_con->up();

        $O_page = 1;
        if(isset($_GET['page'])){
            $O_page = mysql_real_escape_string(check_input($_GET['page']));
        }
        if(isset($_GET['letter'])){
            $letter = mysql_real_escape_string(check_input($_GET['letter']));
        }
        $O_sortType = "other";
        if(isset($_GET['sort_type'])){
            $O_sortType = mysql_real_escape_string(check_input($_GET['sort_type']));
        }
        $O_sortOrder = "desc";
        if(isset($_GET['sort_order'])){
            $O_sortOrder = mysql_real_escape_string(check_input($_GET['sort_order']));
        }
        $O_type = "publish";
        if(isset($_GET['type'])){
            $O_type = mysql_real_escape_string(check_input($_GET['type'])); //for type company
            if($O_type == 'publish'){
                $type = "Publish";
            }else if($O_type == 'notpublish'){
                $type = "Not Publish";
            }
        }

        $total_data = $obj_company->get_total($letter, $O_type);//GET TOTAL ITEM
        if($total_data == 0){
            echo mysql_error();
        }
        $total_page = $obj_company->get_total_page($total_data);//GET TOTAL PAGE

        $data_lists = $obj_company->get_index();
        $data_citys = $obj_rcity->get_index();//GET DATA CITY
        $data_countrys = $obj_rcountry->get_index();//GET DATA COUNTRY
        $data_industrys = $obj_industry->get_index();//GET DATA INDUSTRY
        $data_companys = $obj_company->get_data_by_page($O_page, $letter, $O_type, $O_sortType, $O_sortOrder);//GET DATA PER PAGE
        //var_dump($data_companys);

        $message = $_SESSION['status'];
        $_SESSION['status'] = null;
        $obj_con->down();
    }else if($_GET['action'] == 'insert'){
        $obj_con->up();

        $N_cityID = mysql_real_escape_string(check_input($_POST['city_id']));
        $N_countryID = mysql_real_escape_string(check_input($_POST['country_id']));
        $N_industryID = mysql_real_escape_string(check_input($_POST['industry_id']));
        $N_title = mysql_real_escape_string(check_input($_POST['title']));
        $N_motto = mysql_real_escape_string(check_input($_POST['motto']));
        $N_content = mysql_real_escape_string(check_input($_POST['content']));
        $N_address = mysql_real_escape_string(check_input($_POST['address']));
        $N_email = mysql_real_escape_string(check_input($_POST['email']));
        $N_phone = mysql_real_escape_string(check_input($_POST['phone']));
        
        $N_data = $_POST['tags_input'];
        $data = explode(", " ,$N_data);
        $table_name = "company";
        
        $N_admin = mysql_real_escape_string(check_input($_SESSION['admin_id'])); //admin session id
        $N_publish = mysql_real_escape_string(check_input($_POST['publish']));

        $ran = rand();
        $timestamp = time();

        //to upload in server
        $Photo_ImgLink = "../image/images/company/" . $timestamp . $ran . cleanSpace($_FILES["image"]["name"]);
        $ThmbPhoto_ImgLink = "../image/images/company-thmb/" . $timestamp . $ran . cleanSpace($_FILES["image"]["name"]);
        //to save in database
        $Photo_ImgLink1 = "image/images/company/" . $timestamp . $ran . cleanSpace($_FILES["image"]["name"]);
        $ThmbPhoto_ImgLink1 = "image/images/company-thmb/" . $timestamp . $ran . cleanSpace($_FILES["image"]["name"]);

        if ((($_FILES["image"]["type"] == "image/gif") || ($_FILES["image"]["type"] == "image/jpeg") || ($_FILES["image"]["type"] == "image/jpg") || ($_FILES["image"]["type"] == "image/JPG") || ($_FILES["image"]["type"] == "image/png")) && ($_FILES["image"]["size"] < 10048576) && ($_FILES["image"]["name"] != "")) {
            require_once("packages/imageProcessor.php");
            if ($_FILES["image"]["error"] > 0) {
                //echo "Return Code: " . $_FILES["file"]["error"][$k] . "<br />";
            } else {
                $file = $_FILES["image"]["tmp_name"];
                //Resizing images
                if (true !== ($pic_error = @resampimage(1280, 500, "$file", "$Photo_ImgLink", 0))) {
                    echo $pic_error;
                    unlink($Photo_ImgLink);
                }

                if (true !== ($pic_error1 = @resampimagethmb("$file", "$ThmbPhoto_ImgLink", 0))) {
                    echo $pic_error1;
                    unlink($ThmbPhoto_ImgLink);
                }

                $save_status = "Something is wrong with your submission.<br/>"; 
                $result_company = $obj_company->insert_data($N_industryID, $N_cityID, $N_countryID, $N_title, $N_motto, $N_content, $N_address, $N_email, $N_phone, $Photo_ImgLink1, $ThmbPhoto_ImgLink1, $N_admin, $N_publish);
                $target_id = $result_company;
                if($target_id){
                    if(isset($data)){
                        for($i = 0;$i < count($data);$i++){//START FOR
                            $check_rtag_id = $obj_tagging->check_exist($data[$i]);//CHECK IF TAG EXIST, IF SO RETURN THE rtag_id, else returns 0

                            if($check_rtag_id == 0){//IF THE TAG IS NOT EXIST
                                $result_rtag = $obj_rtag->insert_data(strip_tags($data[$i]));
                                if($result_rtag >= 1){
                                    $message[$i] = "Success to create tag {$data[$i]}.";
                                    $result_tag = $obj_tagging->insert_data($table_name, $target_id, $result_rtag);
                                    if($result_tag){
                                        $message[$i] .= "Success to attach tag {$data[$i]}.";
                                    }
                                }else{
                                    $message[$i] = "Failed to create tag {$data[$i]}.";
                                }
                            }else{//IF THE TAG EXIST, GO AHEAD AND INSERT INTO tbl_tagging
                                $message[$i] = "Tag {$data[$i]} already exist. ";
                                $result_tag = $obj_tagging->insert_data($table_name, $target_id, $check_rtag_id);
                                if($result_tag){
                                    $message[$i] .= "Success to insert {$data[$i]}";
                                }else{
                                    $message[$i] .= "Failed to insert {$data[$i]}";
                                }
                            }
                            $R_message = array("status" => "1", "message" => $message);
                        }//END FOR
                    }
                    $save_status = "<b>\"" . $N_title . "\"</b> has been succesfully added as a new company image.<br />";
                }
            }
        }else if ($_FILES["image"]["name"] != "") {
            $message .= "Something is wrong with your uploaded company image, please make sure that the format is (jpg, png, gif) and the size is less than 9.5MB.<br />";
        }

        if($N_publish == "Publish"){
            $N_type = "publish";
        }else if($N_publish == "Not Publish"){
            $N_type = "notpublish";
        }

        $_SESSION['status'] = $save_status;
        header("Location:adminMgr-Company.php?type=$N_type");
        $obj_con->down();
    }else if($_GET['action'] == 'delete' && $_GET['company_ID'] != '' && $_SESSION['admin_role'] != 'Inputer'){
        $obj_con->up();
        $O_type = mysql_real_escape_string(check_input($_GET['type']));
        $O_id = mysql_real_escape_string(check_input($_GET['company_ID']));
        $O_title = mysql_real_escape_string(check_input($_GET['company_Title']));
        
        $result = $obj_company->delete_data($O_id);
        if($result <= 0){
            $message .= "Something is wrong while deleting the Image, please try again.<br />";
        }else if($result == 1){
            $message .= "Company title <b>\"" . $O_title . "\"</b> has been deleted successfully.<br />";
        }
        
        $_SESSION['status'] = $message;
        header("Location:adminMgr-Company.php?type=$O_type");
        $obj_con->down();
    }else{
        header('Location:adminMgr.php');
    }
}else{
    header('Location:adminMgr.php');
}
?>