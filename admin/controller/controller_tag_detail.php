<?php

require_once("../model/Connection.php");
$obj_con = new Connection();

require_once("../model/Ref_Tag.php");
$obj_rtag = new Ref_Tag();

if($_SESSION['tag'] == "Yes"){ // ACCESS ROLE TAG PAGE
	if($_GET['action'] == '' && $_GET['tag_ID'] != '' && $_SESSION['admin_role'] != 'Inputer'){
		$obj_con->up();

		$O_id = mysql_real_escape_string(check_input($_GET['tag_ID']));
		$data_tags = $obj_rtag->get_data_edit($O_id);

		$obj_con->down();
	}else if($_GET['action'] == 'update' && $_SESSION['admin_role'] != 'Inputer'){
		$obj_con->up();

		$N_ID = mysql_real_escape_string(check_input($_POST['id']));
		$N_title = mysql_real_escape_string(check_input($_POST['title']));
		$N_publish = mysql_real_escape_string(check_input($_POST['publish']));

		$result = $obj_rtag->update_data($N_ID, $N_title, $N_publish);
		if($result == 1){
			$message .= "Tag <b>\"$N_title\"</b> is successfully edited.";
		}else{
			$message .= "Tag <b>\"$N_title\"</b> failed to be edited.";
		}

		$_SESSION['status'] = $message;
		header("Location:adminMgr-Tag.php");
		$obj_con->down();
	}else{
    	header('Location:adminMgr.php');
	}
}else{
    header('Location:adminMgr.php');
}
?>