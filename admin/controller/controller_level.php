<?php 

require_once("../model/Connection.php");
$obj_con = new Connection();

require_once("../model/Ref_Level.php");
$obj_rlvl = new Ref_Level();

if($_SESSION['level'] == "Yes"){ // ACCESS ROLE LEVEL PAGE
	if($_GET['action'] == '' || $_GET['action'] == null){
		$obj_con->up();

		$O_page = 1;
		if(isset($_GET['page'])){
			$O_page = mysql_real_escape_string(check_input($_GET['page']));
		}
		if(isset($_GET['letter'])){
			$letter = mysql_real_escape_string(check_input($_GET['letter']));
		}
		$table_tags = 'level';//FOR AJAX

		$total_data = $obj_rlvl->get_total($letter,0);//GET TOTAL ITEM
		$total_page = $obj_rlvl->get_total_page($total_data);//GET TOTAL PAGE

		$data_levels = $obj_rlvl->get_data_by_page($O_page, $letter,0);//GET DATA PER PAGE

		$message = $_SESSION['status'];
	    $_SESSION['status'] = null;
		$obj_con->down();
	}else if($_GET['action'] == 'insert'){
		$obj_con->up();

		$N_title = mysql_real_escape_string(check_input($_POST['title']));
		$N_content = mysql_real_escape_string(check_input($_POST['content']));
		$N_admin = mysql_real_escape_string(check_input($_SESSION['admin_id'])); //admin session id
		$N_publish = mysql_real_escape_string(check_input($_POST['publish']));

		$result = $obj_rlvl->insert_data($N_title, $N_content, $N_admin, $N_publish);
		if($result == 1){
			$message .= "Level <b>\"$N_title\"</b> is successfully saved.";
		}else{
			$message .= "Level <b>\"$N_title\"</b> failed to be saved.";
		}

		$_SESSION['status'] = $message;
		header("Location:adminMgr-Level.php");
		$obj_con->down();
	}else if($_GET['action'] == 'delete' && $_GET['level_ID'] != '' && $_SESSION['admin_role'] != 'Inputer'){
		$obj_con->up();

		$O_id = mysql_real_escape_string(check_input($_GET['level_ID']));
		$O_title = mysql_real_escape_string(check_input($_GET['level_Title']));
		
		$result = $obj_rlvl->delete_data($O_id);
		if($result == 1){
			$message .= "Level title <b>\"$O_title\"</b> is successfully deleted.";
		}else{
			$message .= "Level title <b>\"$O_title\"</b> failed to be deleted.";
		}

		$_SESSION['status'] = $message;
		header("Location:adminMgr-Level.php");
		$obj_con->down();
	}else{
    	header('Location:adminMgr.php');
	}
}else{
    header('Location:adminMgr.php');
}
?>