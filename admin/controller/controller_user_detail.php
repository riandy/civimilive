<?php 
require_once("../model/Connection.php");
$obj_connect = new Connection();

require_once("../model/Admin.php");
$obj_admin = new Admin();

if($_SESSION['admin'] == "Yes" && $_SESSION['admin_role'] == 'Admin'){ // ACCESS ROLE ADMIN PAGE
    if($_GET['action'] == null && $_GET['admin_id'] != null){
        $obj_connect -> up();
        $O_id = mysql_real_escape_string(check_input($_GET['admin_id']));
        
        	$message = $_SESSION['status'];
        	$_SESSION['status'] = null;
        
    	$data_users = $obj_admin->get_admin($O_id);
        	$data_user_id = $O_id;
        	$data_user_email = $data_users[0]['admin_email'];
        	$data_user_username = $data_users[0]['admin_username'];
            $data_user_password = $data_users[0]['admin_password'];
        
        $obj_connect -> down();
        
    }else if($_GET['action'] == 'update' && $_POST['admin_id'] != null){
        $obj_connect -> up();
        $O_id = mysql_real_escape_string(check_input($_POST['admin_id']));
        $O_username = mysql_real_escape_string(check_input($_POST['admin_username']));
        $O_email = mysql_real_escape_string(check_input($_POST['admin_email']));
        $O_admin_id = $_SESSION['admin_id'];
        
        $result = $obj_admin->admin_update($O_id, $O_username, $O_email, $O_admin_id);
        if($result == 1){
    	$save_status = "Admin \"$O_username\" is successfully updated.";
        }else{
    	$save_status = "Admin \"$O_username\" failed to be updated.";
        }
        $_SESSION['status'] = $save_status;
        header('Location:adminMgr-User.php');
        $obj_connect -> down();
    }
    else if($_GET['action'] == 'change_password' && $_POST['admin_id'] != null){
        $obj_connect -> up();
        $save_status = "Admin \"$O_username\" failed to update password.";
        
        $O_id = mysql_real_escape_string(check_input($_POST['admin_id']));
        $O_username = mysql_real_escape_string(check_input($_POST['admin_username']));
        $O_old_password = mysql_real_escape_string(check_input($_POST['old_password']));
        $O_new_password1 = mysql_real_escape_string(check_input($_POST['new_password_1']));
        $O_new_password2 = mysql_real_escape_string(check_input($_POST['new_password_2']));
        
        if($O_new_password1 == $O_new_password2){
    	$result = $obj_admin->admin_update_password($O_id, $O_old_password, $O_new_password2);
    	if($result == 1){
    	    $save_status = "Admin \"$O_username\"'s password update is successful .";
    	}
        }
        $_SESSION['status'] = $save_status;
        header('Location:adminMgr-User.php');
        $obj_connect -> down();
    }else{
        header("Location:adminMgr-User.php");
    }
}else{
    header('Location:adminMgr.php');
}
?>