<?php

require_once("../model/Connection.php");
$obj_con = new Connection();

require_once("../model/News_Category.php");
$obj_nc = new News_Category();

//if user just open the page, try fetching the data from DB
if($_GET['nc_ID'] != null && $_GET['nc_ID'] != ''){
	$obj_con->up();
    $O_id = mysql_real_escape_string(check_input($_GET['nc_ID']));

    $datas = $obj_nc->get_data_edit($O_id);
    //var_dump($datas);
    
    $message = $_SESSION['status'];
    unset($_SESSION['status']);
    $obj_con->down();
}
else if($_GET['action'] != 'editCategory'){
    header('Location: adminMgr-News_category.php');
    die();
}
//if user just open the page, try fetching the data from DB
if($_GET['action'] == 'editCategory'){
	$obj_con->up();
    //Initialize variables from the form
    $N_id = mysql_real_escape_string(check_input($_POST['category_ID']));
    $N_name = mysql_real_escape_string(check_input($_POST['category_name']));
    $N_publish = mysql_real_escape_string(check_input($_POST['category_publish']));
    $N_sort = mysql_real_escape_string(check_input($_POST['category_sort']));

    $result = $obj_nc->update_data($N_id, $N_name, $N_publish, $N_sort);
    if($result <= 0){
        $message = "There is no change to the category.<br />";
    }
    else if($result == 1){//if adding category info succeed
		$message = "News Category title <b>\"" .stripslashes(correctDisplay($N_name)). "\"</b> has been edited successfully.<br />";
    }// END of adding category info succeed
	else{
        header('Location: logout.php');
        die();
    }
    
    $_SESSION['status'] = $message;
    header('Location: adminMgr-News_category.php');
    $obj_con->down();
}
?>