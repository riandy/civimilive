<?php 

require_once("../model/Connection.php");
$obj_con = new Connection();

require_once("../model/News_Category.php");
$obj_nc = new News_Category();

//if user just open the page, try fetching the data from DB
if(!isset($_GET['action']) || $_GET['action'] == ''){
    $obj_con->up();

    $message = $_SESSION['status'];
    unset($_SESSION['status']); 

 	$datas = $obj_nc->get_data();
    $total_data = 0;
    if(is_array($datas)){
        $total_data = count($datas);
    }
 	//var_dump($datas);

 	$obj_con->down();
}
else if($_GET['action'] == 'addCategory'){
    $obj_con->up();
    //Initialize variables from the form
    $N_name = mysql_real_escape_string(check_input($_POST['category_name']));
    $N_publish = mysql_real_escape_string(check_input($_POST['category_publish']));
    $N_sort = mysql_real_escape_string(check_input($_POST['category_sort']));

    $result = $obj_nc->insert_data($N_name, $N_publish, $N_sort);
    if($result <= 0){
        $message = "There is no change to the category.<br />";
    }
    else if($result == 1){//if adding category info succeed
        $message = "News Category title <b>\"" .stripslashes(correctDisplay($N_name)). "\"</b> has been entered as a new category.<br />";
    }// END of adding category info succeed
    else
    {
        header('Location: logout.php');
        die();
    }
    //End of adding category
            
    $_SESSION['status'] = $message;
    header('Location: adminMgr-News_category.php');
  	$obj_con->down();
}
else if($_GET["action"] == 'delete' && $_GET["nc_ID"]!='' && $_GET["nc_ID"]!=null){
    $obj_con->up();
    $O_id = mysql_real_escape_string(check_input($_GET["nc_ID"]));
    $O_title = mysql_real_escape_string(check_input($_GET["nc_Title"]));
    
    $result = $obj_nc->delete_data($O_id);
    if($result <= 0){
        $message .= "Something is wrong with the news category deletion, please try again.<br />";
    }
    else if($result == 1){
        $message .= "News Category title <b>\"" . $O_title . "\"</b> has been succesfully deleted.<br />";  
    }
    
    $_SESSION['status'] = $message;
    header('Location: adminMgr-News_category.php');
    $obj_con->down();
}
?>