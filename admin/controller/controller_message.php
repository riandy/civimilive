<?php

require_once("../model/Connection.php");
$obj_con = new Connection();

require_once("../model/Message.php");
$obj_msg = new Message();

if($_GET['action'] == '' || $_GET['action'] == NULL){
    $obj_con->up();

    $O_page = 1;
    if(isset($_GET['page'])){
        $O_page = mysql_real_escape_string(check_input($_GET['page']));
    }
   
    $total_data = $obj_msg->get_total();//GET TOTAL ITEM
    $total_page = $obj_msg->get_total_page($total_data);//GET TOTAL PAGE
    $data_messages = $obj_msg->get_data_by_page($O_page);//GET DATA MESSAGE PER PAGE
    //var_dump($data_messages);

    $message = $_SESSION['status'];
    unset($_SESSION['status']);
    $obj_con->down();
}else if($_GET['action'] == 'update_message'){
    $obj_con->up();

    $N_id = mysql_real_escape_string(check_input($_POST['message_id']));
    $N_content = mysql_real_escape_string(check_input(str_replace("'", "’", $_POST['content'])));
    $N_url = $_POST['url']; //for redirect url
    
    $result = $obj_msg->update_message($N_id, $N_content);
    if($result <= 0){
        $message .= "Something is wrong<br />";
    }else if($result == 1){
        $message .= "Message <b>\"" . charLength($N_content, 100) . "\"</b> success to be updated.<br />";
    }else{
        //header('Location: logout.php');           header("Location:logout.php");
        die();
    }

    $_SESSION['status'] = $message;
    header("Location:".$N_url);
    $obj_con->down();
}else if(($_GET['action'] == 'send_forward' || $_GET['action'] == 'send_recipient') && $_GET['msg_ID'] != '' && $_GET['to_email'] != ''){
    $obj_con->up();

    $O_id = mysql_real_escape_string(check_input($_GET['msg_ID']));
    $O_to_email = mysql_real_escape_string(check_input($_GET['to_email']));
    $O_from_email = mysql_real_escape_string(check_input($_GET['from_email']));
    $O_purpose = mysql_real_escape_string(check_input($_GET['purpose']));
    $O_username = mysql_real_escape_string(check_input($_GET['username']));
    $O_name = mysql_real_escape_string(check_input($_GET['name']));
    if($_GET['action'] == 'send_forward'){ 
        $O_message = mysql_real_escape_string(check_input($_GET['message'])); //for content send from admin page
    }else if($_GET['action'] == 'send_recipient'){
        $O_message = urldecode($_GET['message']); //for content send from support@civimi.com
    }
    $O_page = mysql_real_escape_string(check_input($_GET['page']));
    
    $result = $obj_msg->send_email($O_to_email, $O_username, $O_from_email, $O_purpose, $O_name, $O_message);
    if($result <= 0){
        $message .= "Something is wrong<br />";
    }else if($result == 1){
        $obj_msg->update_forwarded($O_id);
        $message .= "Send forward to <b>\"" . $O_to_email . "\"</b> success.<br />";
    }else{
        //header('Location: logout.php');           header("Location:logout.php");
        die();
    }

    $_SESSION['status'] = $message;
    header("Location:adminMgr-Message.php?page=$O_page");
    $obj_con->down();
}else if($_GET['action'] == 'delete' && $_GET['message_ID'] != ''){
    $obj_con->up();

    $O_id = mysql_real_escape_string(check_input($_GET['message_ID']));
    $O_title = urldecode($_GET['message_Title']);
    $O_page = mysql_real_escape_string(check_input($_GET['page']));
    
    $result = $obj_msg->delete_data($O_id);
    if($result <= 0){
        $message .= "Delete message has been failed. Something is wrong<br />";
    }else if($result == 1){
        $message .= "Message <b>\"" . charLength($O_title, 100) . "\"</b> has been deleted.<br />";
    }   

    $_SESSION['status'] = $message;
    header("Location:adminMgr-Message.php?page=$O_page");
    $obj_con->down();
}else{
    echo "error";
}
?>