<?php

require_once("../model/Connection.php");
$obj_con = new Connection();

require_once("../model/Ref_City.php");
$obj_rcity = new Ref_City();

require_once("../model/Ref_Country.php");
$obj_rcountry = new Ref_Country();

if($_SESSION['city'] == "Yes"){ // ACCESS ROLE CITY PAGE
	if($_GET['action'] == '' && $_GET['city_ID'] != '' && $_SESSION['admin_role'] != 'Inputer'){
		$obj_con->up();

		$O_id = mysql_real_escape_string(check_input($_GET['city_ID']));
		$data_citys = $obj_rcity->get_data_edit($O_id);
		$data_countrys = $obj_rcountry->get_index();

		$obj_con->down();
	}else if($_GET['action'] == 'update' && $_SESSION['admin_role'] != 'Inputer'){
		$obj_con->up();

		$N_ID = mysql_real_escape_string(check_input($_POST['id']));
		$N_country_id = mysql_real_escape_string(check_input($_POST['country_id']));
		$N_title = mysql_real_escape_string(check_input($_POST['title']));
		$N_admin = mysql_real_escape_string(check_input($_SESSION['admin_id'])); //admin session id
		$N_publish = mysql_real_escape_string(check_input($_POST['publish']));

		$result = $obj_rcity->update_data($N_ID, $N_country_id, $N_title, $N_admin, $N_publish);
		if($result == 1){
			$message .= "City <b>\"$N_title\"</b> is successfully edited.";
		}else{
			$message .= "City <b>\"$N_title\"</b> failed to be edited.";
		}

		$_SESSION['status'] = $message;
		header("Location:adminMgr-City.php");
		$obj_con->down();
	}else{
    	header('Location:adminMgr.php');
	}
}else{
    header('Location:adminMgr.php');
}
?>