<?php

require_once("../model/Connection.php");
$obj_con = new Connection();

require_once("../model/Ref_Level.php");
$obj_rlvl = new Ref_Level();

if($_SESSION['level'] == "Yes"){ // ACCESS ROLE LEVEL PAGE
	if($_GET['action'] == '' && $_GET['level_ID'] != '' && $_SESSION['admin_role'] != 'Inputer'){
		$obj_con->up();

		$O_id = mysql_real_escape_string(check_input($_GET['level_ID']));
		$data_levels = $obj_rlvl->get_data_edit($O_id);

		$obj_con->down();
	}else if($_GET['action'] == 'update' && $_SESSION['admin_role'] != 'Inputer'){
		$obj_con->up();

		$N_ID = mysql_real_escape_string(check_input($_POST['id']));
		$N_title = mysql_real_escape_string(check_input($_POST['title']));
		$N_content = mysql_real_escape_string(check_input($_POST['content']));
		$N_admin = mysql_real_escape_string(check_input($_SESSION['admin_id'])); //admin session id
		$N_publish = mysql_real_escape_string(check_input($_POST['publish']));

		$result = $obj_rlvl->update_data($N_ID, $N_title, $N_content, $N_admin, $N_publish);
		if($result == 1){
			$message .= "Level <b>\"$N_title\"</b> is successfully edited.";
		}else{
			$message .= "Level <b>\"$N_title\"</b> failed to be edited.";
		}

		$_SESSION['status'] = $message;
		header("Location:adminMgr-Level.php");
		$obj_con->down();
	}else{
    	header('Location:adminMgr.php');
	}
}else{
    header('Location:adminMgr.php');
}
?>