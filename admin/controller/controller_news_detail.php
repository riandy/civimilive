<?php 

require_once("../model/Connection.php");
$obj_con = new Connection();

require_once("../model/News_Category.php");
$obj_nc = new News_Category();

require_once("../model/News.php");
$obj_news = new News();

require_once("../model/News_Photo.php");
$obj_np = new News_Photo(); 

require_once("../model/Tagging.php");
$obj_tagging = new Tagging();

require_once("../model/Ref_Tag.php");
$obj_rtag = new Ref_Tag();

//if user just open the page, try fetching the data from DB
if(($_GET['action'] == null || $_GET['action'] == '')&&($_REQUEST['news_ID']!=null && $_REQUEST['news_ID']!=''))
{
    $obj_con->up();
    $table_tags = "news"; //FOR TAGS
    $Q_newsID = mysql_real_escape_string(check_input($_GET['news_ID']));
    
    $datas = $obj_news->get_data_edit($Q_newsID);
    $data_photos = $obj_news->get_data_edit_photo($Q_newsID);
    $data_tags = $obj_tagging->get_data_detail($table_tags, $Q_newsID);
    $categorys = $obj_nc->get_index(); //GET INDEX CATEGORY NEWS
    //var_dump($data_photos);
    //var_dump($datas);

    $message = $_SESSION['status'];
    unset($_SESSION['status']);
    $obj_con->down();
}
//If user save the input
else if($_REQUEST['action'] == 'editNews' && (($_POST['News_title'] != null && $_POST['News_title'] != '')&&($_POST['News_ID'] != null && $_POST['News_ID'] != '')))
{
    $obj_con->up();
    //Initialize variables from the form
    $N_newsID = mysql_real_escape_string(check_input($_POST['News_ID']));
    $N_newsTitle = mysql_real_escape_string(check_input(str_replace("'", "’", $_POST['News_title'])));
    $N_newsCategory = mysql_real_escape_string(check_input($_POST['News_category']));
    if ($_POST['News_featured'] != ''){
        $N_newsFeatured = mysql_real_escape_string(check_input($_POST['News_featured']));
    } else {
        $N_newsFeatured = 0;
    }
    $N_newsContent = str_replace("'", "’", $_POST['News_content']);
    $N_newsTag = mysql_real_escape_string(check_input($_POST['News_tags']));
    $N_newsPhotographer = mysql_real_escape_string(check_input($_POST['News_photographer']));
    $N_newsPhotographerLink = mysql_real_escape_string(check_input($_POST['News_photographer_link']));
    $N_newsAuthor = mysql_real_escape_string(check_input($_POST['News_author']));
    $N_newsAuthorLink = mysql_real_escape_string(check_input($_POST['News_author_link']));
    $N_newsVideoLink = mysql_real_escape_string(check_input($_POST['News_video_link']));
    $N_newsPublish = mysql_real_escape_string(check_input($_POST['News_publish']));
    $N_productMain = mysql_real_escape_string(check_input($_POST['radio1']));

    $table_name = "news";
    $N_data = $_POST['News_tags'];
    if($N_data != ""){
        $data = explode(", " , $N_data);
    }

    //to update logo
    if($_FILES["logo"]["name"][0]!= null && $_FILES["logo"]["name"][0]!= '')
    {
        $i = count($_FILES["logo"]["name"]);
        for ($k=0;$k<$i;$k++)
        {
            $ran = rand();
            $timestamp = time();
            $Logo_ImgLink = "images/news/". $timestamp . $ran . cleanSpace($_FILES["logo"]["name"][$k]);
            $ThmbLogo_ImgLink = "images/news-thmb/". $timestamp . $ran . cleanSpace($_FILES["logo"]["name"][$k]);
  
            if ((($_FILES["logo"]["type"][$k] == "image/gif")|| ($_FILES["logo"]["type"][$k] == "image/jpeg")|| ($_FILES["logo"]["type"][$k] == "image/jpg")||($_FILES["logo"]["type"][$k] == "image/JPG")||($_FILES["logo"]["type"][$k] == "image/png")) && ($_FILES["logo"]["size"][$k] < 10048576) && ($_FILES["logo"]["name"][0] != ""))
            {
                require_once("packages/imageProcessor.php");
                if ($_FILES["logo"]["error"][$k] > 0)
                {
                    //echo "Return Code: " . $_FILES["file"]["error"][$k] . "<br />";
                }
                else
                {
                    $file = $_FILES["logo"]["tmp_name"][$k];
                    //Resizing images
                    // Save edited size image with max width/height of 640
                    if (true !== ($pic_error = @resampimage(1020,500,"$file","$Logo_ImgLink",0))) {
                        echo $pic_error;
                        unlink($Logo_ImgLink);
                    }
                    // Save thumb image with max width/height of 200
                    if (true !== ($pic_error = @resampimagethmb("$file","$ThmbLogo_ImgLink",0))) {
                        echo $pic_error;
                        unlink($ThmbLogo_ImgLink);
                    }
                    //remove photo primary
                    $remove_primary = $obj_np->remove_photo_primary($N_newsID);
                }
            }
            else if ($_FILES["logo"]["name"][$k] != "")
            {
                //echo "<script type='text/javascript'>window.alert(\"Image [" . $k . "] has incorrect format.\");</script>";
                $message.= "Something is wrong with the primary photo that you upload, This system only accept (jpg, png, gif) format and image size less than 9.5MB.<br />";
            }
        }
           
        //query to update primary image
        $update_primary = $obj_np->update_data_primary($N_newsID, $Logo_ImgLink, $ThmbLogo_ImgLink);
        if($update_primary == 1)
        {
            $message.= "Changes photo primary on <b>\"".stripslashes(htmlentities(correctDisplay($N_newsTitle)))."\"</b> has been saved succesfully.<br>";
        }
        else if($update_primary == 0 )
        {
            $message.= "There is no info changes in the album.";
        }
        else
        {
            header('Location: logout.php');
            die();
        }       
    }

    //start update photo title    
    $i=count($_POST['photoTitle']);
    if($i==null){$i=0;}else{$i=$i-1;}
    for($y=0;$y<=$i;$y++){
        $N_photoTitle[$y]=mysql_real_escape_string(check_input($_POST['photoTitle'][$y]));
        $N_photoID[$y]=mysql_real_escape_string(check_input($_POST['photoID'][$y]));
        
        if($N_photoTitle[$y]==' '){
            $N_photoTitle[$y]=null;
        }
        
        $set_title = $obj_np->set_title($N_photoID[$y], $N_photoTitle[$y]);
        if($set_title <= 0){
            //failed
        }else if($set_title == 1){
            $message.="Your photo ".($y+1)." info has been saved succesfully.<br />";
        }
        else{
            header('Location: logout.php');
            die();
        }
    }
    //end update photo title

    //start update new news
    $result = $obj_news->update_data($N_newsID, $N_newsCategory, $N_newsTitle, $N_newsFeatured, $N_newsTag, $N_newsPhotographer, $N_newsPhotographerLink, $N_newsAuthor, $N_newsAuthorLink, $N_newsVideoLink, $N_newsContent, $N_newsPublish);
    if($result == 1)
    {
        $message.= "Changes on <b>\"".stripslashes(htmlentities(correctDisplay($N_newsTitle)))."\"</b> has been saved succesfully.<br>";
    }
    else
    {
        $message.="There is no change in content<br>";
    }
    //start update new news   

    //start update tag new
    $target_id = $N_newsID;
    $result_delete_tag = $obj_tagging->delete_data($target_id, $table_name);
    if($target_id){
        if(isset($data)){
            for($i = 0;$i < count($data);$i++){//START FOR
                $check_rtag_id = $obj_tagging->check_exist($data[$i]);//CHECK IF TAG EXIST, IF SO RETURN THE rtag_id, else returns 0

                if($check_rtag_id == 0){//IF THE TAG IS NOT EXIST
                    $result_rtag = $obj_rtag->insert_data(strip_tags($data[$i]));
                    if($result_rtag >= 1){
                        $result_tag = $obj_tagging->insert_data($table_name, $target_id, $result_rtag);
                    }
                }else{//IF THE TAG EXIST, GO AHEAD AND INSERT INTO tbl_tagging
                    $result_tag = $obj_tagging->insert_data($table_name, $target_id, $check_rtag_id);
                }
            }//END FOR
        }
        $message .= "News tagging has been succesfully edited.<br />";
    }
    //end update tag news 
    
    //start set photo primary
    if ($N_productMain) {
        $unset_main = $obj_np->unset_main($N_newsID);
        if($unset_main >= 0)
        {
            $set_main = $obj_np->set_main($N_newsID, $N_productMain);
            if($set_main == 1){
                $message.= "One of the photo has been set as primary.<br />";
            }
        }
    }
    //end set photo primary
    
    $_SESSION['status'] = $message;
    $locationURL = "Location: adminMgr-News_detail.php?news_ID=$N_newsID";
    header($locationURL);
    $obj_con->down();
}
//if user delete one of the photos
else if($_REQUEST["action"] == 'delete' && $_REQUEST['news_ID'] !=null && $_REQUEST['news_ID'] != '')
{
    $obj_con->up();
    $N_newsID = mysql_real_escape_string(check_input($_REQUEST['news_ID']));
    $N_newsTitle = mysql_real_escape_string(check_input(urldecode($_REQUEST['news_title'])));
    
    $result = $obj_news->delete_data($N_newsID);
    if($result <= 0)
    {
        $message.= "Something is incorrect with your photo deletion<br />";
    }
    else if($result >= 1)
    {
        $message.= "One album in <b>\"$N_newsTitle\"</b> has been deleted successfully.<br />";
    }
     
    $_SESSION['status'] = $message;
    $locationURL = "Location: adminMgr-News.php";
    header($locationURL);
    $obj_con->down();
}
//------------------------------------------------------
else if($_REQUEST['action'] == 'addPhoto' )
{
    //If add a new album
    if(($_REQUEST['news_ID'] != null && $_REQUEST['news_ID'] != ''))
    {
        $obj_con->up();
        //Initialize variables from the form
        $N_news_ID = mysql_real_escape_string(check_input($_REQUEST['news_ID']));
        $N_news_title = mysql_real_escape_string(check_input(urldecode($_REQUEST['news_title'])));
            
        $i = count($_FILES["photo"]["name"]);
        //To insert the images
        for ($k=0;$k<$i;$k++)
        {
            $ran = rand ();
            $timestamp = time();
            $Photo_ImgLink = "images/news/". $timestamp . $ran . cleanSpace($_FILES["photo"]["name"][$k]);
            $ThmbPhoto_ImgLink = "images/news-thmb/". $timestamp . $ran . cleanSpace($_FILES["photo"]["name"][$k]);
            if ((($_FILES["photo"]["type"][$k] == "image/gif")|| ($_FILES["photo"]["type"][$k] == "image/jpeg")|| ($_FILES["photo"]["type"][$k] == "image/jpg")||($_FILES["photo"]["type"][$k] == "image/JPG")||($_FILES["photo"]["type"][$k] == "image/png")) && ($_FILES["photo"]["size"][$k] < 10048576) && ($_FILES["photo"]["name"][0] != ""))
            {
                if ($_FILES["photo"]["error"][$k] > 0)
                {
                //echo "Return Code: " . $_FILES["file"]["error"][$k] . "<br />";
                }
                else
                {
                    require_once("packages/imageProcessor.php");
                    $file = $_FILES["photo"]["tmp_name"][$k];
                    //Resizing images
                    // Save edited size image with max width/height of 640
                    if (true !== ($pic_error = @resampimage(1020,500,"$file","$Photo_ImgLink",0))) {
                        echo $pic_error;
                        unlink($Photo_ImgLink);
                    }

                    // Save thumb image with max width/height of 100
                    if (true !== ($pic_error = @resampimagethmb("$file","$ThmbPhoto_ImgLink",0))) {
                        echo $pic_error;
                        unlink($ThmbPhoto_ImgLink);
                    }

                    $result = $obj_np->insert_sub_data($Photo_ImgLink, $ThmbPhoto_ImgLink, $N_news_ID);
                    if($result >= 1){
                        $message = $i . " photo is uploaded successfully<br/>";
                    }       
                }
            }
            else if ($_FILES["photo"]["name"][$k] != "")
            {
                //echo "<script type='text/javascript'>window.alert(\"Image [" . $k . "] has incorrect format.\");</script>";
                $message.= "Something is wrong with photo-" . ($k+1) . ", please check the format (jpg, png, gif) and make sure that the file size is less than 9.5MB.<br />";
            }
        }//For closing
        
        //to refresh to the new page and still keep the status
        $_SESSION['status'] = $message;
        header("Location: adminMgr-News_detail.php?news_ID=$N_news_ID");
        $obj_con->down();
    }//END OF ADDING NEW photo
}
//if user delete one of the photos
else if($_REQUEST["action"] == 'deletePhoto' && $_REQUEST['news_ID'] != null && $_REQUEST['news_ID'] != '' && $_REQUEST['np_ID'] != null && $_REQUEST['np_ID'] != '')
{
    $obj_con->up();
    $N_news_ID = mysql_real_escape_string(check_input($_REQUEST['news_ID']));
    $N_news_title = mysql_real_escape_string(check_input(urldecode($_REQUEST['news_title'])));
    $N_np_ID = mysql_real_escape_string(check_input($_REQUEST['np_ID']));
    
    if(($N_np_ID != null && $N_np_ID != '')&&($N_news_ID != null && $N_news_ID != ''))
    {
        $result = $obj_np->delete_data($N_news_ID, $N_np_ID);
        if($result <= 0)
        {
            $message.= "Something is wrong with the photo deletion";
        }
        else if($result == 1)
        {
            $message.= "One of the photos for <b>\"$N_news_title\"</b> has been deleted successfully.";
        }
    } 

    //set as primary if primary deleted
    //mysql_query("UPDATE T_NEWS_PHOTO SET np_main=1 WHERE np_newsID='$N_news_ID'");

    $_SESSION['status'] = $message;
    $locationURL = "Location: adminMgr-News_detail.php?news_ID=$N_news_ID";
    header($locationURL);
    $obj_con->down();
}

//If all the above conditions aren't met, go to adminMgr-Portfolio.php
else
{
    header('Location: adminMgr-News.php');
    die();
}
?>