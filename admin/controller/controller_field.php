<?php 

require_once("../model/Connection.php");
$obj_con = new Connection();

require_once("../model/Ref_Field.php");
$obj_rfield = new Ref_Field();

// require_once("../model/Industry.php");
// $obj_industry = new Industry();

if($_SESSION['field'] == "Yes"){ // ACCESS ROLE FIELD PAGE
    if($_GET['action'] == '' || $_GET['action'] == null){
    	$obj_con->up();

    	$O_page = 1;
    	if(isset($_GET['page'])){
    		$O_page = mysql_real_escape_string(check_input($_GET['page']));
    	}
    	if(isset($_GET['letter'])){
    		$letter = mysql_real_escape_string(check_input($_GET['letter']));
    	}
    	$table_tags = 'field';//FOR AJAX

    	$total_data = $obj_rfield->get_total($letter,0);//GET TOTAL ITEM
    	$total_page = $obj_rfield->get_total_page($total_data);//GET TOTAL PAGE

    	$data_fields = $obj_rfield->get_data_by_page($O_page, $letter,0);//GET DATA PER PAGE
        $parent_fields = $obj_rfield->get_parent();
    	// $data_industrys = $obj_industry->get_index();
    	
        $message = $_SESSION['status'];
        unset($_SESSION['status']);
    	$obj_con->down();
    }else if($_GET['action'] == 'insert'){
    	$obj_con->up();

    	$N_parent_id = mysql_real_escape_string(check_input($_POST['parent_id']));
    	$N_title = mysql_real_escape_string(check_input($_POST['title']));
    	$N_content = mysql_real_escape_string(check_input($_POST['content']));
        $N_admin = mysql_real_escape_string(check_input($_SESSION['admin_id'])); //admin session id
    	$N_publish = mysql_real_escape_string(check_input($_POST['publish']));

    	$ran = rand();
        $timestamp = time();

        //to upload in server
        $Photo_ImgLink = "../image/images/field/" . $timestamp . $ran . cleanSpace($_FILES["image"]["name"]);
        $ThmbPhoto_ImgLink = "../image/images/field-thmb/" . $timestamp . $ran . cleanSpace($_FILES["image"]["name"]);
        //to save in database
        $Photo_ImgLink1 = "image/images/field/" . $timestamp . $ran . cleanSpace($_FILES["image"]["name"]);
        $ThmbPhoto_ImgLink1 = "image/images/field-thmb/" . $timestamp . $ran . cleanSpace($_FILES["image"]["name"]);

        if ((($_FILES["image"]["type"] == "image/gif") || ($_FILES["image"]["type"] == "image/jpeg") || ($_FILES["image"]["type"] == "image/jpg") || ($_FILES["image"]["type"] == "image/JPG") || ($_FILES["image"]["type"] == "image/png")) && ($_FILES["image"]["size"] < 10048576) && ($_FILES["image"]["name"] != "")) {
            require_once("packages/imageProcessor.php");
            if ($_FILES["image"]["error"] > 0) {
                //echo "Return Code: " . $_FILES["file"]["error"][$k] . "<br />";
            } else {
                $file = $_FILES["image"]["tmp_name"];
                //Resizing images
                if (true !== ($pic_error = @resampimage(1280, 500, "$file", "$Photo_ImgLink", 0))) {
                    echo $pic_error;
                    unlink($Photo_ImgLink);
                }

                if (true !== ($pic_error1 = @resampimagethmb("$file", "$ThmbPhoto_ImgLink", 0))) {
                    echo $pic_error1;
                    unlink($ThmbPhoto_ImgLink);
                }

                $result = $obj_rfield->insert_data($N_parent_id, $N_title, $N_content, $Photo_ImgLink1, $ThmbPhoto_ImgLink1, $N_admin, $N_publish);
                if($result <= 0){
                    $message .= "Something is wrong with your submission.<br />";
                }else if($result == 1){
                    $message .= "<b>\"" . $N_title . "\"</b> has been succesfully added as a new field image.<br />";
                }else{
                    //header('Location: logout.php');				header("Location:logout.php");
                    die();
                }
            }
        }else if ($_FILES["image"]["name"] != "") {
            $message .= "Something is wrong with your uploaded field image, please make sure that the format is (jpg, png, gif) and the size is less than 9.5MB.<br />";
        }
    	
    	$_SESSION['status'] = $message;
    	header("Location:adminMgr-Field.php");
    	$obj_con->down();
    }else if($_GET['action'] == 'delete' && $_GET['field_ID'] != '' && $_SESSION['admin_role'] != 'Inputer'){
    	$obj_con->up();
    	$O_id = mysql_real_escape_string(check_input($_GET['field_ID']));
        $O_title = mysql_real_escape_string(check_input($_GET['field_Title']));

        $result = $obj_rfield->delete_data($O_id);
        if($result <= 0){
            $message .= "Something is wrong while deleting the Image, please try again.<br />";
        }else if($result == 1){
            $message .= "Field title <b>\"" . $O_title . "\"</b> has been deleted successfully.<br />";
        }

    	$_SESSION['status'] = $message;
    	header("Location:adminMgr-Field.php");
    	$obj_con->down();
    }else{
        header('Location:adminMgr.php');
    }
}else{
    header('Location:adminMgr.php');
}
?>