<?php 
require_once("../model/Connection.php");
$obj_connect = new Connection();

require_once("../model/AT_Admin_Setting.php");
$obj_setting = new AT_Admin_Setting();

if($_SESSION['setting'] == "Yes" && $_SESSION['admin_role'] == 'Admin'){ // ACCESS ROLE ADMIN SETTING PAGE
    if($_GET['action'] == null){
        $obj_connect -> up();
        
        $message = $_SESSION['status'];
        $_SESSION['status'] = null;
        
    	$data_users = $obj_setting->get_data();
        //var_dump($data_users);
        if(is_array($data_users)){
            $total_data = count($data_users);
        }else{
            $total_data = 0;
        }

        $data_admins = $obj_setting->get_index_admin(); // GET INDEX ADMIN
        $data_roles = $obj_setting->get_index_role(); // GET INDEX ROLE

        $obj_connect -> down();
    }else if($_GET['action'] == 'process' ){
        $obj_connect -> up();

        $O_admin_id = mysql_real_escape_string(check_input($_POST['admin_id']));
        $O_role_id = mysql_real_escape_string(check_input($_POST['role_id']));
        $O_edu = mysql_real_escape_string(check_input($_POST['edu']));
        $O_comp = mysql_real_escape_string(check_input($_POST['comp']));
        $O_ind = mysql_real_escape_string(check_input($_POST['ind']));
        $O_field = mysql_real_escape_string(check_input($_POST['field']));
        $O_jobs = mysql_real_escape_string(check_input($_POST['jobs']));
        $O_user = mysql_real_escape_string(check_input($_POST['user']));
        $O_admin = mysql_real_escape_string(check_input($_POST['admin']));
        $O_cert = mysql_real_escape_string(check_input($_POST['cert']));
        $O_skill = mysql_real_escape_string(check_input($_POST['skill']));
        $O_country = mysql_real_escape_string(check_input($_POST['country']));
        $O_city = mysql_real_escape_string(check_input($_POST['city']));
        $O_level = mysql_real_escape_string(check_input($_POST['level']));
        $O_tag = mysql_real_escape_string(check_input($_POST['tag']));
        $O_setting = mysql_real_escape_string(check_input($_POST['setting']));
        $O_admin_login = $_SESSION['admin_id']; // session admin login
        
    	$result = $obj_setting->process_data($O_admin_id, $O_role_id, $O_edu, $O_comp, $O_ind, $O_field, $O_jobs, $O_user, $O_admin, $O_cert, $O_skill, $O_country, $O_city, $O_level, $O_tag, $O_setting, $O_admin_login);
    	if($result == 1){
    	   $save_status = "Admin setting succesfully to process data. ";
    	}else{
    	   $save_status = "Admin setting process data failed. ";
        }
        
        $_SESSION['status'] = $save_status;
        header('Location:adminMgr-Admin_setting.php');
        $obj_connect -> down();
    }else if($_GET['action'] == 'delete' && $_GET['admin_id'] != null){
        $obj_connect -> up();

        $O_id = mysql_real_escape_string(check_input($_GET['admin_id']));
        $O_username = mysql_real_escape_string(check_input($_GET['admin_username']));
        
        $result = $obj_setting->delete_data($O_id);
        if($result == 1){
    	   $save_status = "Admin \"$O_username\" is successfully deleted.";
        }else{
    	   $save_status = "Admin \"$O_username\" failed to be deleted.";
        }

        $_SESSION['status'] = $save_status;
        header('Location:adminMgr-Admin_setting.php');
        $obj_connect -> down();
    }else{
        header("Location:adminMgr-User.php");
    }
}else{
    header('Location:adminMgr.php');
}
?>