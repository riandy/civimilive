<?php 

require_once("../model/Connection.php");
$obj_con = new Connection();

require_once("../model/Certificate.php");
$obj_cert = new Certificate();

if($_SESSION['certificate'] == "Yes"){ // ACCESS ROLE CERTIFICATE PAGE
    if($_GET['action'] == '' || $_GET['action'] == null){
        $obj_con->up();

        $O_page = 1;
        if(isset($_GET['page'])){
            $O_page = mysql_real_escape_string(check_input($_GET['page']));
        }
        if(isset($_GET['letter'])){
            $letter = mysql_real_escape_string(check_input($_GET['letter']));
        }

        $data_lists = $obj_cert->get_index();//GET DATA CERTIFICATE
        $total_data = $obj_cert->get_total($letter,0);//GET TOTAL ITEM
        if($total_data == 0){
            echo mysql_error();
        }
        $total_page = $obj_cert->get_total_page($total_data);//GET TOTAL PAGE

        $data_certs = $obj_cert->get_data_by_page($O_page, $letter, 0);//GET DATA PER PAGE
        //var_dump($data_certs);

        $message = $_SESSION['status'];
        unset($_SESSION['status']);
        $obj_con->down();
    }else if($_GET['action'] == 'insert'){
        $obj_con->up();

        $N_title = mysql_real_escape_string(check_input($_POST['title']));
        $N_url = mysql_real_escape_string(check_input($_POST['url']));
        $N_publish = mysql_real_escape_string(check_input($_POST['publish']));
        $N_admin = $_SESSION['admin_id']; //admin session id
        
        $ran = rand();
        $timestamp = time();

        //to upload in server
        $Photo_ImgLink = "../image/images/certificate/" . $timestamp . $ran . cleanSpace($_FILES["image"]["name"]);
        $ThmbPhoto_ImgLink = "../image/images/certificate-thmb/" . $timestamp . $ran . cleanSpace($_FILES["image"]["name"]);
        //to save in database
        $Photo_ImgLink1 = "image/images/certificate/" . $timestamp . $ran . cleanSpace($_FILES["image"]["name"]);
        $ThmbPhoto_ImgLink1 = "image/images/certificate-thmb/" . $timestamp . $ran . cleanSpace($_FILES["image"]["name"]);

        if ((($_FILES["image"]["type"] == "image/gif") || ($_FILES["image"]["type"] == "image/jpeg") || ($_FILES["image"]["type"] == "image/jpg") || ($_FILES["image"]["type"] == "image/JPG") || ($_FILES["image"]["type"] == "image/png")) && ($_FILES["image"]["size"] < 10048576) && ($_FILES["image"]["name"] != "")) {
            require_once("packages/imageProcessor.php");
            if ($_FILES["image"]["error"] > 0) {
                //echo "Return Code: " . $_FILES["file"]["error"][$k] . "<br />";
            } else {
                $file = $_FILES["image"]["tmp_name"];
                //Resizing images
                if (true !== ($pic_error = @resampimage(1280, 500, "$file", "$Photo_ImgLink", 0))) {
                    echo $pic_error;
                    unlink($Photo_ImgLink);
                }

                if (true !== ($pic_error1 = @resampimagethmb("$file", "$ThmbPhoto_ImgLink", 0))) {
                    echo $pic_error1;
                    unlink($ThmbPhoto_ImgLink);
                }

                $result = $obj_cert->insert_data($N_title, $N_url, $Photo_ImgLink1, $ThmbPhoto_ImgLink1, $N_publish, $N_admin);
                if(!$result){
                    $message .= "Something is wrong with your submission.<br />";
                }else if($result){
                    $message .= "<b>\"" . $N_title . "\"</b> has been succesfully added as a new certificate image.<br />";
                }else{
                    //header('Location: logout.php');               header("Location:logout.php");
                    die();
                }
            }
        }else if ($_FILES["image"]["name"] != "") {
            $message .= "Something is wrong with your uploaded certificate image, please make sure that the format is (jpg, png, gif) and the size is less than 9.5MB.<br />";
        }

        $_SESSION['status'] = $message;
        header("Location:adminMgr-Certificate.php");
        $obj_con->down();
    }else if($_GET['action'] == 'delete' && $_GET['cert_ID'] != '' && $_SESSION['admin_role'] != 'Inputer'){
        $obj_con->up();
        //$O_type = mysql_real_escape_string(check_input($_GET['type']));
        $O_id = mysql_real_escape_string(check_input($_GET['cert_ID']));
        $O_title = mysql_real_escape_string(check_input($_GET['cert_Title']));
    
        $result = $obj_cert->delete_data($O_id);
        if($result <= 0){
            $message .= "Something is wrong while deleting the Image, please try again.<br />";
        }else if($result == 1){
            $message .= "Certificate title <b>\"" . $O_title . "\"</b> has been deleted successfully.<br />";
        }
       
        $_SESSION['status'] = $message;
        header("Location:adminMgr-Certificate.php"); 
        $obj_con->down();
    }else{
        header('Location:adminMgr.php');
    }
}else{
    header('Location:adminMgr.php');
}
?>