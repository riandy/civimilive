<?php 

require_once("../model/Connection.php");
$obj_con = new Connection();

require_once("../model/Growth.php");
$obj_growth = new Growth();

if($_SESSION['user'] == "Yes"){ // ACCESS ROLE USER PAGE
    if($_GET['action'] == '' || $_GET['action'] == null){
    	$obj_con->up();

        $O_page = 1;
        if(isset($_GET['page'])){
            $O_page = mysql_real_escape_string(check_input($_GET['page']));
        }
        if(isset($_GET['letter'])){
            $letter = mysql_real_escape_string(check_input($_GET['letter']));
        }

        //variable for rangedate
        $O_date = date("m/d/Y", strtotime(nowDate()))." - ".date("m/d/Y", strtotime(nowDate()));
        if(isset($_GET['rangedate'])){
            $O_date = mysql_real_escape_string(check_input($_GET['rangedate']));
        }
        $data_rangedate = explode("-", $O_date);
        if(is_array($data_rangedate)){
            $start_date = date("Y-m-d", strtotime($data_rangedate[0]));
            $end_date = date("Y-m-d", strtotime($data_rangedate[1]));
        }

        //variable date for table
        if($start_date != $end_date){
            $graph = "yes";
            $table_date = date("d F Y",strtotime($start_date))." - ".date("d F Y",strtotime($end_date));

            $check = "no";
            if(isset($_GET['previous'])){
                $check = $_GET['previous'];
                if($check == "yes"){
                    //cari selisih
                    $countDay = intval(((abs(strtotime ($start_date) - strtotime ($end_date)))/(60*60*24))) + 1; //selisih hari
                    $previousStart = date ("Y-m-d", strtotime("-$countDay day", strtotime($start_date)));
                    $previousEnd = date ("Y-m-d", strtotime("-$countDay day", strtotime($end_date)));
                    $table_previous = date("d F Y",strtotime($previousStart))." - ".date("d F Y",strtotime($previousEnd));
                    //start graph previous
                    $data_previous = $obj_growth->get_data_graph($previousStart, $previousEnd);
                    $i=0;
                    $datePrevious = $previousStart;
                    while (strtotime($datePrevious) <= strtotime($previousEnd) && $i <= count($data_previous)) {
                        if($datePrevious == $data_previous[$i]['User_create_date']){
                            $_previous .= ", ".$data_previous[$i]['counter_user'];
                            $i++;
                        }
                        else if($datePrevious != $data_previous[$i]['User_create_date']){
                            $_previous .= ", 0";
                        }
                        $datePrevious = date ("Y-m-d", strtotime("+1 day", strtotime($datePrevious)));
                    }    
                    $sub_previous = substr($_previous, 1);
                    //end graph previous
                }
            }
        }else{
            $graph = "no";
            if($start_date == nowDate()){
                $table_date = "Today";
            }else{
                $table_date = "Yesterday";
            }
        }

        $total_data = $obj_growth->get_total($letter, $start_date, $end_date); //GET TOTAL ITEM
        if($total_data == 0){
            echo mysql_error();
        }
        $total_page = $obj_growth->get_total_page($total_data); //GET TOTAL PAGE
        $data_users = $obj_growth->get_data($O_page, $letter, $start_date, $end_date); //GET DATA USER
        //var_dump($data_users);
        //start graph result
        $data_graphs = $obj_growth->get_data_graph($start_date, $end_date);
        $k=0;
        $dateResult = $start_date;
        while (strtotime($dateResult) <= strtotime($end_date) && $k <= count($data_graphs)) {
            if($dateResult == $data_graphs[$k]['User_create_date']){
                $_result .= ", ".$data_graphs[$k]['counter_user'];
                $k++;
            }
            else if($dateResult != $data_graphs[$k]['User_create_date']){
                $_result .= ", 0";
            }
            $dateResult = date ("Y-m-d", strtotime("+1 day", strtotime($dateResult)));
        }    
        $sub_result = substr($_result, 1);
        //end graph result
       
    	$message = $_SESSION['status'];
        unset($_SESSION['status']);
    	$obj_con->down();
    }else if($_GET['action'] == 'resend' && $_GET['username'] != "" && $_GET['email'] != "" && $_GET['code'] != ""){
        //START FUNCTION FOR RESEND ACTIVATION
        function send_email($tujuan, $username, $code){
            //mail
            $to = $tujuan; 
            $subject = "Thank you from civimi (Account activation)";

            //variable url
            $base_url = "http://www.civimi.com/beta/";
            $absolute_url = $base_url."civimi/";
            $facebook_url = "https://www.facebook.com/civimi";
            $facebook_footer = $absolute_url."img/logo/facebook.png";
            $twitter_url = "https://twitter.com/civimi";
            $twitter_footer = $absolute_url."img/logo/twitter.png";
            $google_url = "https://plus.google.com/+Civimi";
            $google_footer = $absolute_url."img/logo/google.png";

            $message = 
            "<!DOCTYPE html><html><head></head><body>
            <div id='all' style='width: 100%;max-width: 800px;margin: 0 auto;'>
            <div id='header' style='padding: 20px;border-bottom: solid 2px #B5B5B5;margin-bottom: 30px;'>
            <div>
            <img src='".$absolute_url."img/logo/logo-desktop.png' alt='logo' style='width: 200px;'>
            </div>
            </div>
            <div id='main' style='font-size:16px;'>
            Dear ".$username.",<br/><br/>
            You're almost there!<br/><br/>
            Thank you for signing up with civimi, there are many things that you can do in our website.
            Nonetheless, we need you to activate your account before starting with us<br/><br/>
            <div style='text-align:center;margin-top:30px;'>Please click the button below to activate.</div>
            <div style='text-align:center;'>
            <a target='_blank' href='".$base_url."activation/".$username."/".$code."/' type='button' style='padding: 12px 28px;display: inline-block;background-color: #8CCFEC;color: #fff;font-weight: bold;text-decoration: none;font-size: 20px;border-radius: 3px;border: solid 2px #ddd;margin: 50px 0;'>Activate Account</a>
            </div>
            (if clicking on the link doesn't work, try copying and pasting the below address into your browser.)<br/><br/><br/>
            <a target='_blank' href='".$base_url."activation/".$username."/".$code."/' style='color: #0899D7;'>".$base_url."activation/".$username."/".$code."/</a><br/><br/><br/>
            We appreciate your support and please contact us if you have any questions or comments.<br/><br/><br/>
            Kind Regards,<br/><br/><br/>
            The civimi team<br/><br/>
            </div>
            <div id='footer' style='background-color: #666;padding: 30px 0 20px 0;width: 100%;'>
            <div style='display: block;height: 50px;padding: 0 20px;position: relative;'>
            <div style='font-size: 28px;font-style: italic;color: #FFFFFF;font-weight: bold;display: block;width: 50%;float: left;'>'Reinvent Yourself'</div>
            <div style='display: block;float: left;width: 50%;position: relative;text-align: right;'>
            <a target='_blank' href='".$facebook_url."' class='social-logo'><img src='".$facebook_footer."' alt='facebook' style='width: 20px;margin-right: 10px;'></a>
            <a target='_blank' href='".$google_url."' class='social-logo'><img src='".$google_footer."' alt='google' style='width: 20px;margin-right:10px;'></a>
            <a target='_blank' href='".$twitter_url."' class='social-logo'><img src='".$twitter_footer."' alt='twitter' style='width: 20px;'></a>
            </div>
            </div>
            <div style='text-align:center;'>
            <img src='".$absolute_url."img/logo/logo-desktop.png' alt='logo' style='width: 200px;'>
            </div>
            <div style='text-align:center;font-size: 13px;color: #FFFFFF;margin:20px 0 0 0;'>© 2015. Created by Passion in Jakarta</div>
            </div>
            </div>
            </body></html>";

            // Always set content-type when sending HTML email
            $headers = "MIME-Version: 1.0" . "\r\n";
            $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

            // More headers
            $headers .= "From: <no-reply@civimi.com>" . "\r\n";
            $sent = mail($to, $subject, $message, $headers);
            return $sent;   
        }

        $O_username = $_GET['username'];
        $O_email = $_GET['email'];
        $O_code = $_GET['code'];

        $result = send_email($O_email, $O_username, $O_code);
        //var_dump($result);
        if($result){
            $message .= "Resend activation to <b>\"" . $O_email . "\"</b> success.<br />";
        }else{
            $message .= "Something is wrong.<br />";            
        }

        $_SESSION['status'] = $message;
        header("Location:adminMgr-Growth.php");
    }else{
        header('Location:adminMgr.php');
    }
}else{
    header('Location:adminMgr.php');
}
?>