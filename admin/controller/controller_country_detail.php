<?php

require_once("../model/Connection.php");
$obj_con = new Connection();

require_once("../model/Ref_Country.php");
$obj_rcountry = new Ref_Country();

if($_SESSION['country'] == "Yes"){ // ACCESS ROLE COUNTRY PAGE
	if($_GET['action'] == '' && $_GET['country_ID'] != '' && $_SESSION['admin_role'] != 'Inputer'){
		$obj_con->up();

		$O_id = mysql_real_escape_string(check_input($_GET['country_ID']));
		$data_countrys = $obj_rcountry->get_data_edit($O_id);

		$obj_con->down();
	}else if($_GET['action'] == 'update' && $_SESSION['admin_role'] != 'Inputer'){
		$obj_con->up();

		$N_ID = mysql_real_escape_string(check_input($_POST['id']));
		$N_title = mysql_real_escape_string(check_input($_POST['title']));
		$N_abbr = mysql_real_escape_string(check_input($_POST['abbr']));
		$N_admin = mysql_real_escape_string(check_input($_SESSION['admin_id'])); //admin session id
		$N_publish = mysql_real_escape_string(check_input($_POST['publish']));

		if($_FILES["image"]["name"]!=null){
            //$ran = rand ();
            //$timestamp = time();

            //to upload in server
            $Photo_ImgLink = "../image/images/flag/" . cleanSpace($_FILES["image"]["name"]);
            //to save in database
            $Photo_ImgLink1 = "image/images/flag/". cleanSpace($_FILES["image"]["name"]);
           
            if ((($_FILES["image"]["type"] == "image/gif")|| ($_FILES["image"]["type"] == "image/jpeg")|| ($_FILES["image"]["type"] == "image/jpg")||($_FILES["image"]["type"] == "image/JPG")||($_FILES["image"]["type"] == "image/png")) && ($_FILES["image"]["size"] < 10048576) && ($_FILES["image"]["name"] != "")){
                require_once("packages/imageProcessor.php");
                if ($_FILES["image"]["error"] > 0){
                    //echo "Return Code: " . $_FILES["file"]["error"][$k] . "<br />";
                }else{
                    $file = $_FILES["image"]["tmp_name"];
                	
    				//Resizing images
                    if (true !== ($pic_error = @resampimage(1280,720,"$file","$Photo_ImgLink",0))) {
                        echo $pic_error;
                        unlink($Photo_ImgLink);
                    }

                    $result = $obj_rcountry->update_data_image($N_ID, $N_title, $N_abbr, $Photo_ImgLink1, $N_admin, $N_publish);
                    if($result <= 0){
                    	$message .= "Something is wrong with your editing.<br />";
                    }else if($result == 1){
                    	$message .= "<b>\"" . $N_title . "\"</b> has been succesfully edited.<br />";
                    }else{
                        //header('Location: logout.php');					header("Location:logout.php");
                        die();
                    }
                }
            }
            else if($_FILES["image"]["name"] != ""){
                $message .= "Something is wrong with your uploaded country flag image, please make sure that the format is (jpg, png, gif) and the size is less than 9.5MB.<br />";
            }
        }else{
            $result = $obj_rcountry->update_data($N_ID, $N_title, $N_abbr, $N_admin, $N_publish);
            if($result <= 0){
    			$message .= "Something is wrong with your editing.<br />";
            }else if($result == 1){
                $message .= "<b>\"" . $N_title . "\"</b> has been succesfully edited.<br />";
            }else{
    			//header('Location: logout.php');			header("Location:logout.php");
                die();
            }
        }

		$_SESSION['status'] = $message;
		header("Location:adminMgr-Country.php");
		$obj_con->down();
	}else{
        header('Location:adminMgr.php');
    }
}else{
    header('Location:adminMgr.php');
}
?>