<?php 

require_once("../model/Connection.php");
$obj_con = new Connection();

require_once("../model/Photo.php");
$obj_photo = new Photo();

if($_SESSION['admin'] == "Yes"){ // ACCESS ROLE IMAGE PAGE
    if($_GET['action'] == '' || $_GET['action'] == null){
        $obj_con->up();

        $O_page = 1;
        if(isset($_GET['page'])){
            $O_page = mysql_real_escape_string(check_input($_GET['page']));
        }

        $O_sort = "newest";
        if(isset($_GET['sort'])){
            $O_sort = mysql_real_escape_string(check_input($_GET['sort']));
        }

        $total_data = $obj_photo->get_total(0);//GET TOTAL ITEM
        if($total_data == 0){
            echo mysql_error();
        }
        $total_page = $obj_photo->get_total_page($total_data);//GET TOTAL PAGE

        $data_photos = $obj_photo->get_data_by_page($O_page, $O_sort);//GET DATA PER PAGE
        //var_dump($data_photos);

        $message = $_SESSION['status'];
        unset($_SESSION['status']);
        $obj_con->down();
    }else if($_GET['action'] == 'delete' && $_GET['photo_ID'] != '' && $_SESSION['admin_role'] != 'Inputer'){
        $obj_con->up();
        $O_id = mysql_real_escape_string(check_input($_GET['photo_ID']));
        $O_title = mysql_real_escape_string(check_input($_GET['photo_Title']));
        $O_fullname = mysql_real_escape_string(check_input($_GET['fullname']));

        $O_page = mysql_real_escape_string(check_input($_GET['page']));
        $O_sort = mysql_real_escape_string(check_input($_GET['sort']));
    
        $result = $obj_photo->delete_data($O_id);
        if($result <= 0){
            $message .= "Something is wrong while deleting the Image, please try again.<br />";
        }else if($result == 1){
            $message .= "Photo title <b>\"" . $O_title . "\"</b> from " . $O_fullname . " has been deleted successfully.<br />";
        }
       
        $_SESSION['status'] = $message;
        header("Location:adminMgr-Image.php?page=".$O_page."&sort=".$O_sort); 
        $obj_con->down();
    }else if($_GET['action'] == 'publish' && $_GET['photo_ID'] != '' && $_SESSION['admin_role'] != 'Inputer'){
        $obj_con->up();
        $O_id = mysql_real_escape_string(check_input($_GET['photo_ID']));
        $O_title = mysql_real_escape_string(check_input($_GET['photo_Title']));
        $O_fullname = mysql_real_escape_string(check_input($_GET['fullname']));

        $O_page = mysql_real_escape_string(check_input($_GET['page']));
        $O_sort = mysql_real_escape_string(check_input($_GET['sort']));
    
        $result = $obj_photo->publish_data($O_id);
        if($result <= 0){
            $message .= "Something is wrong.<br />";
        }else if($result == 1){
            $message .= "Photo title <b>\"" . $O_title . "\"</b> from " . $O_fullname . " set to publish data.<br />";
        }
       
        $_SESSION['status'] = $message;
        header("Location:adminMgr-Image.php?page=".$O_page."&sort=".$O_sort); 
        $obj_con->down();
    }else if($_GET['action'] == 'unpublish' && $_GET['photo_ID'] != '' && $_SESSION['admin_role'] != 'Inputer'){
        $obj_con->up();
        $O_id = mysql_real_escape_string(check_input($_GET['photo_ID']));
        $O_title = mysql_real_escape_string(check_input($_GET['photo_Title']));
        $O_fullname = mysql_real_escape_string(check_input($_GET['fullname']));

        $O_page = mysql_real_escape_string(check_input($_GET['page']));
        $O_sort = mysql_real_escape_string(check_input($_GET['sort']));
    
        $result = $obj_photo->unpublish_data($O_id);
        if($result <= 0){
            $message .= "Something is wrong.<br />";
        }else if($result == 1){
            $message .= "Photo title <b>\"" . $O_title . "\"</b> from " . $O_fullname . " set to unpublish data.<br />";
        }
       
        $_SESSION['status'] = $message;
        header("Location:adminMgr-Image.php?page=".$O_page."&sort=".$O_sort); 
        $obj_con->down();
    }else if($_GET['action'] == 'popular' && $_GET['photo_ID'] != '' && $_SESSION['admin_role'] != 'Inputer'){
        $obj_con->up();
        $O_id = mysql_real_escape_string(check_input($_GET['photo_ID']));
        $O_title = mysql_real_escape_string(check_input($_GET['photo_Title']));
        $O_fullname = mysql_real_escape_string(check_input($_GET['fullname']));

        $O_page = mysql_real_escape_string(check_input($_GET['page']));
        $O_sort = mysql_real_escape_string(check_input($_GET['sort']));
    
        $result = $obj_photo->popular_data($O_id);
        if($result <= 0){
            $message .= "Something is wrong.<br />";
        }else if($result == 1){
            $message .= "Photo title <b>\"" . $O_title . "\"</b> from " . $O_fullname . " set to popular data.<br />";
        }
       
        $_SESSION['status'] = $message;
        header("Location:adminMgr-Image.php?page=".$O_page."&sort=".$O_sort); 
        $obj_con->down();
    }else if($_GET['action'] == 'unpopular' && $_GET['photo_ID'] != '' && $_SESSION['admin_role'] != 'Inputer'){
        $obj_con->up();
        $O_id = mysql_real_escape_string(check_input($_GET['photo_ID']));
        $O_title = mysql_real_escape_string(check_input($_GET['photo_Title']));
        $O_fullname = mysql_real_escape_string(check_input($_GET['fullname']));

        $O_page = mysql_real_escape_string(check_input($_GET['page']));
        $O_sort = mysql_real_escape_string(check_input($_GET['sort']));
    
        $result = $obj_photo->unpopular_data($O_id);
        if($result <= 0){
            $message .= "Something is wrong.<br />";
        }else if($result == 1){
            $message .= "Photo title <b>\"" . $O_title . "\"</b> from " . $O_fullname . " set to unpopular data.<br />";
        }
       
        $_SESSION['status'] = $message;
        header("Location:adminMgr-Image.php?page=".$O_page."&sort=".$O_sort); 
        $obj_con->down();
    }else{
        header('Location:adminMgr.php');
    }
}else{
    header('Location:adminMgr.php');
}
?>