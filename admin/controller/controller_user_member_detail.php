<?php

require_once("../model/Connection.php");
$obj_con = new Connection();

require_once("../model/User.php");
$obj_user = new User();

if($_SESSION['user'] == "Yes"){ // ACCESS ROLE USER PAGE
    if($_GET['action'] == '' && $_GET['user_ID'] != '' && $_SESSION['admin_role'] != 'Inputer'){
    	$obj_con->up();

    	$O_id = mysql_real_escape_string(check_input($_GET['user_ID']));
        $O_type = mysql_real_escape_string(check_input($_GET['type']));
    	$data_users = $obj_user->get_data_edit($O_id);

    	$obj_con->down();
    }else if($_GET['action'] == 'update' && $_SESSION['admin_role'] != 'Inputer'){
    	$obj_con->up();

        $N_type = mysql_real_escape_string(check_input($_POST['type']));
    	$N_ID = mysql_real_escape_string(check_input($_POST['id']));
    	$N_username = mysql_real_escape_string(check_input($_POST['username']));
        $N_email = mysql_real_escape_string(check_input($_POST['email']));
        $N_fname = mysql_real_escape_string(check_input($_POST['fname']));
        $N_lname = mysql_real_escape_string(check_input($_POST['lname']));
        $N_sex = mysql_real_escape_string(check_input($_POST['sex']));
        $N_desc = mysql_real_escape_string(check_input($_POST['desc']));
        $N_day = mysql_real_escape_string(check_input($_POST['birthday1']));
        $N_month = mysql_real_escape_string(check_input($_POST['birthday2']));
        $N_year = mysql_real_escape_string(check_input($_POST['birthday3']));
        $N_interest = mysql_real_escape_string(check_input($_POST['interest']));
        $N_obj = mysql_real_escape_string(check_input($_POST['obj']));
        $N_address = mysql_real_escape_string(check_input($_POST['address']));
        $N_city = mysql_real_escape_string(check_input($_POST['city']));
        $N_state = mysql_real_escape_string(check_input($_POST['state']));
        $N_country = mysql_real_escape_string(check_input($_POST['country']));
    	$N_phone = mysql_real_escape_string(check_input($_POST['phone']));

        $result = $obj_user->update_data($N_ID, $N_username, $N_email, $N_fname, $N_lname, $N_sex, $N_desc, $N_day, $N_month, $N_year, $N_interest, $N_obj, $N_address, $N_city, $N_state, $N_country, $N_phone);
        if($result == 1){
            $message .= "User <b>\"$N_username\"</b> is successfully edited.";
        }else{
            $message .= "User <b>\"$N_username\"</b> failed to be edited.";
        }

    	$_SESSION['status'] = $message;
    	header("Location:adminMgr-User_member.php?type=$N_type");
    	$obj_con->down();
    }else{
        header('Location:adminMgr.php');
    }
}else{
    header('Location:adminMgr.php');
}
?>