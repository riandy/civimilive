<?php 

require_once("../model/Connection.php");
$obj_con = new Connection();

require_once("../model/Industry.php");
$obj_ind = new Industry();

if($_SESSION['industry'] == "Yes"){ // ACCESS ROLE INDUSTRY PAGE
	if($_GET['action'] == '' || $_GET['action'] == null){
		$obj_con->up();

		$O_page = 1;
		if(isset($_GET['page'])){
			$O_page = mysql_real_escape_string(check_input($_GET['page']));
		}
		if(isset($_GET['letter'])){
			$letter = mysql_real_escape_string(check_input($_GET['letter']));
		}
		$table_tags = 'industry';//FOR AJAX

		$total_data = $obj_ind->get_total($letter,0);//GET TOTAL ITEM
		$total_page = $obj_ind->get_total_page($total_data);//GET TOTAL PAGE

		$data_industrys = $obj_ind->get_data_by_page($O_page, $letter,0);//GET DATA PER PAGE

		$data_lists = $obj_ind->get_index();//GET DATA INDUSTRY
		
	    $message = $_SESSION['status'];
	    $_SESSION['status'] = null;
		$obj_con->down();
	}else if($_GET['action'] == 'insert'){
		$obj_con->up();

		$N_title = mysql_real_escape_string(check_input($_POST['title']));
		$N_content = mysql_real_escape_string(check_input($_POST['content']));
		$N_admin = mysql_real_escape_string(check_input($_SESSION['admin_id'])); //admin session id
		$N_publish = mysql_real_escape_string(check_input($_POST['publish']));

		$result = $obj_ind->insert_data($N_title, $N_content, $N_admin, $N_publish);
		if($result == 1){
			$message .= "Industry <b>\"$N_title\"</b> is successfully saved.";
		}else{
			$message .= "Industry <b>\"$N_title\"</b> failed to be saved.";
		}

		$_SESSION['status'] = $message;
		header("Location:adminMgr-Industry.php");
		$obj_con->down();
	}else if($_GET['action'] == 'delete' && $_GET['industry_ID'] != '' && $_SESSION['admin_role'] != 'Inputer'){
		$obj_con->up();

		$O_id = mysql_real_escape_string(check_input($_GET['industry_ID']));
		
		$result = $obj_ind->delete_data($O_id);
		if($result == 1){
			$message .= "Industry ID <b>\"$O_id\"</b> is successfully deleted.";
		}else{
			$message .= "Indsutry ID <b>\"$O_id\"</b> failed to be deleted.";
		}

		$_SESSION['status'] = $message;
		header("Location:adminMgr-Industry.php");
		$obj_con->down();
	}else{
        header('Location:adminMgr.php');
    }
}else{
    header('Location:adminMgr.php');
}
?>