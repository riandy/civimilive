<?php

require_once("../model/Connection.php");
$obj_con = new Connection();

require_once("../model/Organization.php");
$obj_org = new Organization();

require_once("../model/Ref_City.php");
$obj_rcity = new Ref_City();

require_once("../model/Ref_Country.php");
$obj_rcountry = new Ref_Country();

require_once("../model/Industry.php");
$obj_industry = new Industry();

require_once("../model/Tagging.php");
$obj_tagging = new Tagging(); 

require_once("../model/Ref_Tag.php");
$obj_rtag = new Ref_Tag();

if($_SESSION['company'] == "Yes"){ // ACCESS ROLE COMPANY PAGE
    if($_GET['action'] == '' && $_GET['org_ID'] != '' && $_SESSION['admin_role'] != 'Inputer'){
    	$obj_con->up();

        $table_tags = "organization";//FOR TAGS
    	$O_id = mysql_real_escape_string(check_input($_GET['org_ID']));
        $O_type = mysql_real_escape_string(check_input($_GET['type']));
    	$datas = $obj_org->get_data_edit($O_id);
        $data_tags = $obj_tagging->get_data_detail($table_tags, $O_id); 

        $data_citys = $obj_rcity->get_index();//GET DATA CITY
        $data_countrys = $obj_rcountry->get_index();//GET DATA COUNTRY
        $data_industrys = $obj_industry->get_index();//GET DATA INDUSTRY

    	$obj_con->down();
    }else if($_GET['action'] == 'update' && $_SESSION['admin_role'] != 'Inputer'){
    	$obj_con->up();
    	
    	$N_id = mysql_real_escape_string(check_input($_POST['id']));
        $N_industryID = mysql_real_escape_string(check_input($_POST['industry_id']));
        $N_cityID = mysql_real_escape_string(check_input($_POST['city_id']));
        $N_countryID = mysql_real_escape_string(check_input($_POST['country_id']));
    	$N_title = mysql_real_escape_string(check_input($_POST['title']));
    	$N_motto = mysql_real_escape_string(check_input($_POST['motto']));
    	$N_content = mysql_real_escape_string(check_input($_POST['content']));
    	$N_address = mysql_real_escape_string(check_input($_POST['address']));
    	$N_email = mysql_real_escape_string(check_input($_POST['email']));
    	$N_phone = mysql_real_escape_string(check_input($_POST['phone']));
        $N_publish = mysql_real_escape_string(check_input($_POST['publish']));
        $N_admin = $_SESSION['admin_id']; //admin session id

        $N_data = $_POST['tags_input'];
        if($N_data != ""){
            $data = explode(", " ,$N_data);
            $table_name = "organization";
        }
        
    	if($_FILES["image"]["name"]!=null){
            $ran = rand ();
            $timestamp = time();

            //to upload in server
            $Photo_ImgLink = "../image/images/organization/" . $timestamp . $ran . cleanSpace($_FILES["image"]["name"]);
            $ThmbPhoto_ImgLink = "../image/images/organization-thmb/" . $timestamp . $ran . cleanSpace($_FILES["image"]["name"]);
            //to save in database
            $Photo_ImgLink1 = "image/images/organization/". $timestamp . $ran . cleanSpace($_FILES["image"]["name"]);
            $ThmbPhoto_ImgLink1 = "image/images/organization-thmb/". $timestamp . $ran . cleanSpace($_FILES["image"]["name"]);
        
            if ((($_FILES["image"]["type"] == "image/gif")|| ($_FILES["image"]["type"] == "image/jpeg")|| ($_FILES["image"]["type"] == "image/jpg")||($_FILES["image"]["type"] == "image/JPG")||($_FILES["image"]["type"] == "image/png")) && ($_FILES["image"]["size"] < 10048576) && ($_FILES["image"]["name"] != "")){
                require_once("packages/imageProcessor.php");
                if ($_FILES["image"]["error"] > 0){
                    //echo "Return Code: " . $_FILES["file"]["error"][$k] . "<br />";
                }else{
                    $file = $_FILES["image"]["tmp_name"];
                	
    				//Resizing images
                    if (true !== ($pic_error = @resampimage(1280,720,"$file","$Photo_ImgLink",0))) {
                        echo $pic_error;
                        unlink($Photo_ImgLink);
                    }
                    
                    if (true !== ($pic_error1 = @resampimagethmb("$file","$ThmbPhoto_ImgLink",0))) {
                        echo $pic_error1;
                        unlink($ThmbPhoto_ImgLink);
                    }

                    $save_status = "Something is wrong with your editing.<br/>";    
                    $result_organization = $obj_org->update_data_image($N_id, $N_industryID, $N_cityID, $N_countryID, $N_title, $N_motto, $N_content, $N_address, $N_email, $N_phone, $Photo_ImgLink1, $ThmbPhoto_ImgLink1, $N_admin, $N_publish);
                    $target_id = $N_id;
                    if($target_id){
                        if(isset($data)){//start insert tags
                            $result_delete_tag = $obj_tagging->delete_data($target_id, $table_name);
                            for($i = 0;$i < count($data);$i++){//START FOR
                                $check_rtag_id = $obj_tagging->check_exist($data[$i]);//CHECK IF TAG EXIST, IF SO RETURN THE rtag_id, else returns 0
                                if($check_rtag_id == 0){//IF THE TAG IS NOT EXIST
                                    $result_rtag = $obj_rtag->insert_data(strip_tags($data[$i]));
                                    if($result_rtag >= 1){
                                        $result_tag = $obj_tagging->insert_data($table_name, $target_id, $result_rtag);
                                    }
                                }else{//IF THE TAG EXIST, GO AHEAD AND INSERT INTO tbl_tagging
                                    $result_tag = $obj_tagging->insert_data($table_name, $target_id, $check_rtag_id);
                                }
                            }//END FOR
                        }//end insert tags
                        $save_status = "<b>\"" . $N_title . "\"</b> has been succesfully edited.<br />";
                    }
                }
            }
            else if($_FILES["image"]["name"] != ""){
                $message .= "Something is wrong with your uploaded organization image, please make sure that the format is (jpg, png, gif) and the size is less than 9.5MB.<br />";
            }
        }else{
            $save_status = "Something is wrong with your editing.<br/>";
            $result_organization = $obj_org->update_data($N_id, $N_industryID, $N_cityID, $N_countryID, $N_title, $N_motto, $N_content, $N_address, $N_email, $N_phone, $N_admin, $N_publish);
            $target_id = $N_id;
            if($target_id){
                if(isset($data)){//start insert tags
                    $result_delete_tag = $obj_tagging->delete_data($target_id, $table_name);
                    for($i = 0;$i < count($data);$i++){//START FOR
                        $check_rtag_id = $obj_tagging->check_exist($data[$i]);//CHECK IF TAG EXIST, IF SO RETURN THE rtag_id, else returns 0
                        if($check_rtag_id == 0){//IF THE TAG IS NOT EXIST
                            $result_rtag = $obj_rtag->insert_data(strip_tags($data[$i]));
                            if($result_rtag >= 1){
                                $result_tag = $obj_tagging->insert_data($table_name, $target_id, $result_rtag);             
                            }
                        }else{//IF THE TAG EXIST, GO AHEAD AND INSERT INTO tbl_tagging
                            $result_tag = $obj_tagging->insert_data($table_name, $target_id, $check_rtag_id);
                        }
                    }//END FOR
                }//end insert tags
                $save_status = "<b>\"" . $N_title . "\"</b> has been succesfully edited.<br />";
            }
        }
        
        if($N_publish == "Publish"){
            $N_type = "publish";
        }else if($N_publish == "Not Publish"){
            $N_type = "notpublish";
        }

    	$_SESSION['status'] = $save_status;
    	header("Location:adminMgr-Organization.php?type=$N_type");
    	$obj_con->down();
    }else{
        header('Location:adminMgr.php');
    }
}else{
    header('Location:adminMgr.php');
}
?>