<?php

require_once("../model/Connection.php");
$obj_con = new Connection();

require_once("../model/Edu.php");
$obj_edu = new Edu();

require_once("../model/Ref_City.php");
$obj_rcity = new Ref_City();

require_once("../model/Ref_Country.php");
$obj_rcountry = new Ref_Country();

if($_SESSION['education'] == "Yes"){ // ACCESS ROLE EDUCATION PAGE
    if($_GET['action'] == '' && $_GET['edu_ID'] != '' && $_SESSION['admin_role'] != 'Inputer'){
    	$obj_con->up();

    	$O_id = mysql_real_escape_string(check_input($_GET['edu_ID']));
        $O_type = mysql_real_escape_string(check_input($_GET['type'])); //for type education
        if($O_type == 'publish'){
            $type = "Publish";
        }else if($O_type == 'notpublish'){
            $type = "Not Publish";
        }else if($O_type == 'new'){
            $type = "Fresh Content";
        }
        
    	$data_edus = $obj_edu->get_data_edit($O_id);
        $data_citys = $obj_rcity->get_index();//GET DATA CITY
        $data_countrys = $obj_rcountry->get_index();//GET DATA COUNTRY

        //var_dump($data_edus);
        $message = $_SESSION['status'];
        unset($_SESSION['status']);
    	$obj_con->down();
    }else if($_GET['action'] == 'update' && $_SESSION['admin_role'] != 'Inputer'){
    	$obj_con->up();
    	mysql_query("autocommit(FALSE)");

    	$N_ID = mysql_real_escape_string(check_input($_POST['id']));
        $N_name = mysql_real_escape_string(check_input($_POST['name']));
    	$N_city = mysql_real_escape_string(check_input($_POST['city']));
    	$N_country = mysql_real_escape_string(check_input($_POST['country']));
        $N_descEN = mysql_real_escape_string(check_input($_POST['descEN']));
        $N_descIN = mysql_real_escape_string(check_input($_POST['descIN']));
        $N_publish = mysql_real_escape_string(check_input($_POST['publish']));
        $N_admin = mysql_real_escape_string(check_input($_SESSION['admin_id'])); //admin session id
        $N_type = mysql_real_escape_string(check_input($_POST['type'])); //type page

    	if($_FILES["image"]["name"]!=null){
            $ran = rand ();
            $timestamp = time();

            //to upload in server
            $Photo_ImgLink = "../image/images/edu/" . $timestamp . $ran . cleanSpace($_FILES["image"]["name"]);
            $ThmbPhoto_ImgLink = "../image/images/edu-thmb/" . $timestamp . $ran . cleanSpace($_FILES["image"]["name"]);
            //to save in database
            $Photo_ImgLink1 = "image/images/edu/". $timestamp . $ran . cleanSpace($_FILES["image"]["name"]);
            $ThmbPhoto_ImgLink1 = "image/images/edu-thmb/". $timestamp . $ran . cleanSpace($_FILES["image"]["name"]);

            if ((($_FILES["image"]["type"] == "image/gif")|| ($_FILES["image"]["type"] == "image/jpeg")|| ($_FILES["image"]["type"] == "image/jpg")||($_FILES["image"]["type"] == "image/JPG")||($_FILES["image"]["type"] == "image/png")) && ($_FILES["image"]["size"] < 10048576) && ($_FILES["image"]["name"] != "")){
                require_once("packages/imageProcessor.php");
                if ($_FILES["image"]["error"] > 0){
                    //echo "Return Code: " . $_FILES["file"]["error"][$k] . "<br />";
                }else{
                    $file = $_FILES["image"]["tmp_name"];
                	
    				//Resizing images
                    if (true !== ($pic_error = @resampimage(1280,720,"$file","$Photo_ImgLink",0))) {
                        echo $pic_error;
                        unlink($Photo_ImgLink);
                    }
                    
                    if (true !== ($pic_error1 = @resampimagethmb("$file","$ThmbPhoto_ImgLink",0))) {
                        echo $pic_error1;
                        unlink($ThmbPhoto_ImgLink);
                    }

                    $result = $obj_edu->update_data_image($N_ID, $N_name, $N_city, $N_country, $N_descEN, $N_descIN, $Photo_ImgLink1, $ThmbPhoto_ImgLink1, $N_admin, $N_publish);
                    if($result <= 0){
                    	$message .= "Something is wrong with your editing.<br />";
                    }else if($result == 1){
                    	$message .= "<b>\"" . $N_name . "\"</b> has been succesfully edited.<br />";
                    }else{
                        //header('Location: logout.php');					header("Location:logout.php");
                        die();
                    }
                }
            }
            else if($_FILES["image"]["name"] != ""){
                $message .= "Something is wrong with your uploaded education logo, please make sure that the format is (jpg, png, gif) and the size is less than 9.5MB.<br />";
            }
        }else{
            $result = $obj_edu->update_data($N_ID, $N_name, $N_city, $N_country, $N_descEN, $N_descIN, $N_admin, $N_publish);
            if($result <= 0){
    			$message .= "Something is wrong with your editing.<br />";
            }else if($result == 1){
                $message .= "<b>\"" . $N_name . "\"</b> has been succesfully edited.<br />";
            }else{
    			//header('Location: logout.php');			header("Location:logout.php");
                die();
            }
        }
        //End of transaction
        mysql_query("commit()");
    	mysql_query("autocommit(TRUE)");

    	$_SESSION['status'] = $message;
    	// header("Location:adminMgr-Edu.php?type=$N_type");
        header("Location:adminMgr-Edu_detail.php?edu_ID=$N_ID");
    	$obj_con->down();
    }else{
        header('Location:adminMgr.php');
    }
}else{
    header('Location:adminMgr.php');
}
?>