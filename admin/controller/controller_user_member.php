<?php 

require_once("../model/Connection.php");
$obj_con = new Connection();

require_once("../model/User.php");
$obj_user = new User();

require_once("../model/Ref_City.php");
$obj_rcity = new Ref_City();

require_once("../model/Ref_Country.php");
$obj_rcountry = new Ref_Country();

if($_SESSION['user'] == "Yes"){ // ACCESS ROLE USER PAGE
    if($_GET['action'] == '' || $_GET['action'] == null){
    	$obj_con->up();

    	$O_page = 1;
    	if(isset($_GET['page'])){
    		$O_page = mysql_real_escape_string(check_input($_GET['page']));
    	}
    	if(isset($_GET['letter'])){
    		$letter = mysql_real_escape_string(check_input($_GET['letter']));
    	}
        
        $O_sortType = "join";
        if(isset($_GET['sort_type'])){
            $O_sortType = mysql_real_escape_string(check_input($_GET['sort_type']));
        }
        $O_sortOrder = "desc";
        if(isset($_GET['sort_order'])){
            $O_sortOrder = mysql_real_escape_string(check_input($_GET['sort_order']));
        }
        
        $O_name = mysql_real_escape_string(check_input($_GET['name']));
        $O_username = mysql_real_escape_string(check_input($_GET['username']));
        $O_profession = mysql_real_escape_string(check_input($_GET['profession']));
        $O_year = mysql_real_escape_string(check_input($_GET['year']));
        $O_city = mysql_real_escape_string(check_input($_GET['city']));
        $O_country = mysql_real_escape_string(check_input($_GET['country']));
        
        $total_data = $obj_user->admin_get_total($letter, $O_name, $O_username, $O_profession, $O_year, $O_city, $O_country, $O_sortType, $O_sortOrder);//GET TOTAL ITEM
        if($total_data == 0){
            echo mysql_error();
        }
        $total_page = $obj_user->get_total_page($total_data);//GET TOTAL PAGE

        $countrys = $obj_rcountry->get_index(); //GET INDEX COUNTRY
        $citys = $obj_rcity->get_index(); //GET INDEX CITY
        $data_lists = $obj_user->admin_get_index(); //GET INDEX USER
        $data_users = $obj_user->admin_get_data_by_page($O_page, $letter, $O_name, $O_username, $O_profession, $O_year, $O_city, $O_country, $O_sortType, $O_sortOrder);//GET DATA PER PAGE
        //var_dump($data_users);

    	$message = $_SESSION['status'];
        unset($_SESSION['status']);
    	$obj_con->down();
    }else if($_GET['action'] == 'unpublish' && $_GET['user_ID'] != '' && $_SESSION['admin_role'] != 'Inputer'){
    	$obj_con->up();

    	$O_id = mysql_real_escape_string(check_input($_GET['user_ID']));
    	$O_name = mysql_real_escape_string(check_input($_GET['user_Name']));
    	
    	$result = $obj_user->unpublish_data($O_id);
        if($result <= 0){
            $message .= "Something is wrong while unpublish user, please try again.<br />";
        }else if($result == 1){
            $message .= "User name <b>\"" . $O_name . "\"</b> has been set unpublish successfully.<br />";
        }

    	$_SESSION['status'] = $message;
    	header("Location:adminMgr-User_member.php?type=notpublish");
    	$obj_con->down();
    }else if($_GET['action'] == 'publish' && $_GET['user_ID'] != '' && $_SESSION['admin_role'] != 'Inputer'){
        $obj_con->up();

        $O_id = mysql_real_escape_string(check_input($_GET['user_ID']));
        $O_name = mysql_real_escape_string(check_input($_GET['user_Name']));
        
        $result = $obj_user->publish_data($O_id);
        if($result <= 0){
            $message .= "Something is wrong while publish user, please try again.<br />";
        }else if($result == 1){
            $message .= "User name <b>\"" . $O_name . "\"</b> has been set publish successfully.<br />";
        }

        $_SESSION['status'] = $message;
        header("Location:adminMgr-User_member.php?type=publish");
        $obj_con->down();
    }else{
        header('Location:adminMgr.php');
    }
}else{
    header('Location:adminMgr.php');
}
?>