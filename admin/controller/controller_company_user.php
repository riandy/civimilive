<?php

require_once("../model/Connection.php");
$obj_con = new Connection();

require_once("../model/AT_Work.php");
$obj_atework = new AT_Work();

require_once("../model/Company.php");
$obj_comp = new Company();

if($_SESSION['company'] == "Yes"){ // ACCESS ROLE EDUCATION PAGE
    if($_GET['action'] == '' && $_GET['company_ID'] != '' && $_SESSION['admin_role'] != 'Inputer'){
    	$obj_con->up();

        $O_page = 1;
        if(isset($_GET['page'])){
            $O_page = mysql_real_escape_string(check_input($_GET['page']));
        }
        $O_type = mysql_real_escape_string(check_input($_GET['type']));

        $O_id = mysql_real_escape_string(check_input($_GET['company_ID']));
        $total_data = $obj_atework->get_total($O_id);//GET TOTAL ITEM
        if($total_data == 0){
            echo mysql_error();
        }
        $data_company_lists = $obj_comp->get_index();//GET DATA COMPANY for select box
        $total_page = $obj_atework->get_total_page($total_data);//GET TOTAL PAGE
    	$data_companys = $obj_atework->get_data_by_company($O_page, $O_id);
        //var_dump($data_companys);
        $data_lists = $obj_atework->get_index($O_id);
        //var_dump($data_lists);

        $message = $_SESSION['status'];
        unset($_SESSION['status']);
    	$obj_con->down();
    }else if($_GET['action'] == 'update' && $_SESSION['admin_role'] != 'Inputer'){
    	$obj_con->up();

        $N_companyID = mysql_real_escape_string(check_input($_POST['companyID'])); //for redirect url
    	$N_id = mysql_real_escape_string(check_input($_POST['id']));
        $N_company_id = mysql_real_escape_string(check_input($_POST['company_id']));
        $N_username = mysql_real_escape_string(check_input($_POST['username']));
        //$N_name = mysql_real_escape_string(check_input($_POST['name'])); //company title
    	
        $N_fromMonth = mysql_real_escape_string(check_input($_POST['from_month']));
        $N_fromYear = mysql_real_escape_string(check_input($_POST['from_year']));
        $N_progress = mysql_real_escape_string(check_input($_POST['progress']));
        if($N_progress == 1){
            $N_toMonth = 0;
            $N_toYear = 0;
            $N_current = 2;
        }else{
            $N_toMonth = mysql_real_escape_string(check_input($_POST['to_month']));
            $N_toYear = mysql_real_escape_string(check_input($_POST['to_year']));
            $N_current = 0;
        }
        $N_title = mysql_real_escape_string(check_input($_POST['title'])); //work title
        $N_desc = mysql_real_escape_string(check_input($_POST['desc']));
        $N_type = mysql_real_escape_string(check_input($_POST['type'])); //type page
    	
        $result = $obj_atework->update_data($N_id, $N_company_id, $N_title, $N_fromMonth, $N_fromYear, $N_toMonth, $N_toYear, $N_current, $N_desc);
        //var_dump($result);
        if($result <= 0){
    	    $message .= "Something is wrong with your editing.<br />";
        }else if($result == 1){
            $message .= "<b>\"" . $N_username . "\"</b> has been succesfully edited.<br />";
        }else{
    	    //header('Location: logout.php');			header("Location:logout.php");
            die();
        }

    	$_SESSION['status'] = $message;
    	header("Location:adminMgr-Company_user.php?type=$N_type&company_ID=$N_companyID");
    	$obj_con->down();
    }else if($_GET['action'] == 'delete' && $_GET['company_ID'] != '' && $_GET['atework_ID'] != '' && $_GET['username'] != '' && $_SESSION['admin_role'] != 'Inputer'){
        $obj_con->up();

        $O_companyID = mysql_real_escape_string(check_input($_GET['company_ID']));
        $O_id = mysql_real_escape_string(check_input($_GET['atework_ID']));
        $O_user = mysql_real_escape_string(check_input($_GET['username']));
        $O_type = mysql_real_escape_string(check_input($_GET['type'])); //type page
        
        $result = $obj_atework->delete_data($O_id);
        if($result <= 0){
            $message .= "Something is wrong<br />";
        }else if($result == 1){
            $message .= "User name <b>\"" . $O_user . "\"</b> has been deleted successfully.<br />";
        }  

        $_SESSION['status'] = $message;
        header("Location:adminMgr-Company_user.php?type=$O_type&company_ID=$O_companyID");
        $obj_con->down();
    }else{
        header('Location:adminMgr.php');
    }
}else{
    header('Location:adminMgr.php');
}
?>