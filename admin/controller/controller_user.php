<?php 
require_once("../model/Connection.php");
$obj_connect = new Connection();

require_once("../model/Admin.php");
$obj_admin = new Admin();

if($_SESSION['admin'] == "Yes" && $_SESSION['admin_role'] == 'Admin'){ // ACCESS ROLE ADMIN PAGE
    if($_GET['action'] == null){
        $obj_connect -> up();
        
        	$message = $_SESSION['status'];
        	$_SESSION['status'] = null;
        
    	$data_users = $obj_admin->get_admin();
        
        $obj_connect -> down();
        
    }else if($_GET['action'] == 'register'){
        $obj_connect -> up();
        $O_email = mysql_real_escape_string(check_input($_POST['email']));
        $O_username = mysql_real_escape_string(check_input($_POST['username']));
        $O_password1 = mysql_real_escape_string(check_input($_POST['password1']));
        $O_password2 = mysql_real_escape_string(check_input($_POST['password2']));
        $O_admin_id = $_SESSION['admin_id'];
        
        if($O_email != null && $O_username != null){
    	if($O_password1 == $O_password2){
    	$result = $obj_admin->admin_create($O_username, $O_email,$O_password2, $O_admin_id);
    	    if($result == 1){
    		$save_status = "Admin \"$O_username\" succesfully registered. ";
    	    }else{
    		$save_status = "Admin \"$O_username\" registration failed. ";
    	    }
    	}
        }
        $_SESSION['status'] = $save_status;
        header('Location:adminMgr-User.php');
        $obj_connect -> down();
    }else if($_GET['action'] == 'delete' && $_GET['admin_id'] != null){
        $obj_connect -> up();
        $O_id = mysql_real_escape_string(check_input($_GET['admin_id']));
        $O_username = mysql_real_escape_string(check_input($_GET['admin_username']));
        
        $result = $obj_admin->admin_delete($O_id);
        if($result == 1){
    	$save_status = "Admin \"$O_username\" is successfully deleted.";
        }else{
    	$save_status = "Admin \"$O_username\" failed to be deleted.";
        }
        $_SESSION['status'] = $save_status;
        header('Location:adminMgr-User.php');
        $obj_connect -> down();
    }else{
        header('Location:adminMgr.php');
    }
}else{
    header('Location:adminMgr.php');
}
?>