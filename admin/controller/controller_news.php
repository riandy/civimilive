<?php 

require_once("../model/Connection.php");
$obj_con = new Connection();

require_once("../model/News_Category.php");
$obj_nc = new News_Category();

require_once("../model/News.php");
$obj_news = new News();

require_once("../model/News_Photo.php");
$obj_np = new News_Photo();

require_once("../model/Tagging.php");
$obj_tagging = new Tagging();

require_once("../model/Ref_Tag.php");
$obj_rtag = new Ref_Tag();

//if user just open the page, try fetching the data from DB
if (($_GET['action'] == null || $_GET['action'] == '')) {
    $obj_con->up();
    $O_page = 1;
    if (isset($_GET['page'])) {
        $O_page = mysql_real_escape_string(check_input($_GET['page']));
    }

    $datas = $obj_news->get_data_by_page($O_page);
    $categorys = $obj_nc->get_index(); //GET INDEX NEWS CATEGORY
    //var_dump($datas);
    if(is_array($datas)){
        $O_totalNum = $datas[0]['total_data_all'];
        $total_page = $datas[0]['total_page'];
    }else{
        $O_totalNum = 0;
        $total_page = 0;
    }

    $message = $_SESSION['status'];
    unset($_SESSION['status']);
    $obj_con->down();
}
//If user save the input
else if ($_REQUEST['action'] == 'addNews' && ($_POST['News_title'] != null && $_POST['News_title'] != '')) {
    $obj_con->up();
    //Initialize variables from the form
    $N_newsTitle = mysql_real_escape_string(check_input(str_replace("'", "’", $_POST['News_title'])));
    $N_newsCategory = mysql_real_escape_string(check_input($_POST['News_category']));
    if ($_POST['News_featured'] != ''){
        $N_newsFeatured = mysql_real_escape_string(check_input($_POST['News_featured']));
    } else {
        $N_newsFeatured = 0;
    }
    $N_newsContent = str_replace("'", "’", $_POST['News_content']);
    $N_newsTag = mysql_real_escape_string(check_input($_POST['News_tags']));
    $N_newsPhotographer = mysql_real_escape_string(check_input($_POST['News_photographer']));
    $N_newsPhotographerLink = mysql_real_escape_string(check_input($_POST['News_photographer_link']));
    $N_newsAuthor = mysql_real_escape_string(check_input($_POST['News_author']));
    $N_newsAuthorLink = mysql_real_escape_string(check_input($_POST['News_author_link']));
    $N_newsVideoLink = mysql_real_escape_string(check_input($_POST['News_video_link']));
    $N_newsPublish = mysql_real_escape_string(check_input($_POST['News_publish']));

    $N_data = $_POST['News_tags'];
    if($N_data != ""){
        $data = explode(", " , $N_data);
        $table_name = "news";
    }

    $result = $obj_news->insert_data($N_newsCategory, $N_newsTitle, $N_newsContent, $N_newsTag, $N_newsFeatured, $N_newsPhotographer, $N_newsPhotographerLink, $N_newsAuthor, $N_newsAuthorLink, $N_newsVideoLink, $N_newsPublish);
    if (!$result) {
        $message.= "Something is wrong with your submission.<br />";
    } else if ($result) {
        //insert tag
        $target_id = $result;
        if($target_id){
            if(isset($data)){
                for($i = 0;$i < count($data);$i++){//START FOR
                    $check_rtag_id = $obj_tagging->check_exist($data[$i]);//CHECK IF TAG EXIST, IF SO RETURN THE rtag_id, else returns 0

                    if($check_rtag_id == 0){//IF THE TAG IS NOT EXIST
                        $result_rtag = $obj_rtag->insert_data(strip_tags($data[$i]));
                        if($result_rtag >= 1){
                            $result_tag = $obj_tagging->insert_data($table_name, $target_id, $result_rtag);
                        }
                    }else{//IF THE TAG EXIST, GO AHEAD AND INSERT INTO tbl_tagging
                        $result_tag = $obj_tagging->insert_data($table_name, $target_id, $check_rtag_id);
                    }
                }//END FOR
            }
        }

        $message.= "News title <b>\"$N_newsTitle\"</b> has been added succesfully.<br />";
        $i = count($_FILES["photo"]["name"]);
        //To insert the images
        for ($k = 0; $k < $i; $k++) {
            if($k == 0){
                $N_photoMain = '1';
            }else{
                $N_photoMain = '0';
            }
            $ran = rand();
            $timestamp = time();
            $Photo_ImgLink = "images/news/" . $timestamp . $ran . cleanSpace($_FILES["photo"]["name"][$k]);
            $ThmbPhoto_ImgLink = "images/news-thmb/" . $timestamp . $ran . cleanSpace($_FILES["photo"]["name"][$k]);

            if ((($_FILES["photo"]["type"][$k] == "image/gif") || ($_FILES["photo"]["type"][$k] == "image/jpeg") || ($_FILES["photo"]["type"][$k] == "image/jpg") || ($_FILES["photo"]["type"][$k] == "image/JPG") || ($_FILES["photo"]["type"][$k] == "image/png")) && ($_FILES["photo"]["size"][$k] < 10048576) && ($_FILES["photo"]["name"][0] != "")) {
                require_once("packages/imageProcessor.php");
                if ($_FILES["photo"]["error"][$k] > 0) {
                    //echo "Return Code: " . $_FILES["file"]["error"][$k] . "<br />";
                } else {
                    $file = $_FILES["photo"]["tmp_name"][$k];
                    //Resizing images
                    // Save edited size image with max width/height of 640
                    if (true !== ($pic_error = @resampimage(1020, 500, "$file", "$Photo_ImgLink", 0))) {
                        echo $pic_error;
                        unlink($Photo_ImgLink);
                    }
                    // Save thumb image with max width/height of 200
                    if (true !== ($pic_error = @resampimagethmb("$file", "$ThmbPhoto_ImgLink", 0))) {
                        echo $pic_error;
                        unlink($ThmbPhoto_ImgLink);
                    }

                    $save_photo = $obj_np->insert_data($Photo_ImgLink, $ThmbPhoto_ImgLink, $result, $N_photoMain);
                }
            } else if ($_FILES["photo"]["name"][$k] != "") {
                //echo "<script type='text/javascript'>window.alert(\"Image [" . $k . "] has incorrect format.\");</script>";
                $message.= "Something is incorrent with photo " . ($k + 1) . " that you upload, please make sure that the format is (jpg, png, gif) and size less than 9.5MB.<br /";
            }
        }//For closing
    } else {
        $message.= "Something is wrong with your photo submission.<br />";
    }
    
    //to refresh to the new page and still keep the status
    $_SESSION['status'] = $message;
    header("Location: adminMgr-News.php");
    $obj_con->down();
}
//if user delete one of the photos
else if ($_REQUEST["action"] == 'delete' && $_REQUEST['news_ID'] != null && $_REQUEST['news_ID'] != '') {
    $obj_con->up();
    $N_newsID = mysql_real_escape_string(check_input($_REQUEST['news_ID']));
    $N_newsTitle = mysql_real_escape_string(check_input($_REQUEST['news_Title']));

    $result = $obj_news->delete_data($N_newsID);
    if ($result <= 0) {
        $message.= "Something is incorrect with your photo deletion<br />";
    } else if ($result >= 1) {
        $message.= "One album in <b>\"$N_newsTitle\"</b> has been deleted successfully.<br />";
    }

    $_SESSION['status'] = $message;
    $locationURL = "Location: adminMgr-News.php";
    header($locationURL);
    $obj_con->down();
}
?>