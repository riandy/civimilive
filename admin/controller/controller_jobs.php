<?php 

require_once("../model/Connection.php");
$obj_con = new Connection();

require_once("../model/Jobs.php");
$obj_job = new Jobs();

require_once("../model/Tagging.php");
$obj_tagging = new Tagging();

require_once("../model/Ref_Tag.php");
$obj_rtag = new Ref_Tag();

require_once("../model/Company.php");
$obj_company = new Company();

require_once("../model/Ref_Field.php");
$obj_rfield = new Ref_Field();

require_once("../model/Ref_Level.php");
$obj_rlvl = new Ref_Level();

require_once("../model/Ref_City.php");
$obj_rcity = new Ref_City();

if($_SESSION['jobs'] == "Yes"){ // ACCESS ROLE JOBS PAGE
	if($_GET['action'] == '' || $_GET['action'] == null){
		$obj_con->up();

		$O_page = 1;
		if(isset($_GET['page'])){
			$O_page = mysql_real_escape_string(check_input($_GET['page']));
		}
		if(isset($_GET['letter'])){
			$letter = mysql_real_escape_string(check_input($_GET['letter']));
		}

		//GET INDEX
		$data_companys = $obj_company->get_index();
		$data_fields = $obj_rfield->get_index();
		$data_levels = $obj_rlvl->get_index();
		$data_citys = $obj_rcity->get_index();

		if($_GET['fieldID'] != '' || $_GET['cityID'] != '' || $_GET['levelID'] != '' || $_GET['cintern'] != '' || $_GET['cfulltime'] != '' || $_GET['cparttime'] != '' || $_GET['cfreelance'] != '' || $_GET['keyword'] != ''){
			$O_field = mysql_real_escape_string(check_input($_GET['fieldID']));
			$O_city = mysql_real_escape_string(check_input($_GET['cityID']));
			$O_level = mysql_real_escape_string(check_input($_GET['levelID']));
			$O_intern = mysql_real_escape_string(check_input($_GET['cintern']));
			$O_fulltime = mysql_real_escape_string(check_input($_GET['cfulltime']));
			$O_parttime = mysql_real_escape_string(check_input($_GET['cparttime']));
			$O_freelance = mysql_real_escape_string(check_input($_GET['cfreelance']));
			$O_keyword = mysql_real_escape_string(check_input($_GET['keyword']));

			$total_data = $obj_job->get_total_search($O_page, $letter, 0, $O_field, $O_city, $O_level, $O_intern, $O_fulltime, $O_parttime, $O_freelance, $O_keyword);//GET TOTAL ITEM
			//var_dump($total_data);
			$total_page = $obj_job->get_total_page($total_data);//GET TOTAL PAGE

			$data_jobss = $obj_job->get_data_search($O_page, $letter, 0, $O_field, $O_city, $O_level, $O_intern, $O_fulltime, $O_parttime, $O_freelance, $O_keyword);//GET DATA PER PAGE
			//var_dump($data_jobss);
		}
		else{
			$total_data = $obj_job->get_total($letter,0);//GET TOTAL ITEM
			$total_page = $obj_job->get_total_page($total_data);//GET TOTAL PAGE

			$data_jobss = $obj_job->get_data_by_page($O_page, $letter,0);//GET DATA PER PAGE
		}

		$message = $_SESSION['status'];
	    unset($_SESSION['status']);
		$obj_con->down();
	}
	else if($_GET['action'] == 'insert'){
		$save_status = "";
		$obj_con->up();

		$N_title = mysql_real_escape_string(check_input($_POST['title']));
		$N_content = mysql_real_escape_string(check_input($_POST['content']));
		$N_company_id = mysql_real_escape_string(check_input($_POST['company_id']));
		$N_field_id = mysql_real_escape_string(check_input($_POST['field_id']));
		$N_level_id = mysql_real_escape_string(check_input($_POST['level_id']));
		$N_city_id = mysql_real_escape_string(check_input($_POST['city_id']));
		$N_exp = mysql_real_escape_string(check_input($_POST['exp']));
		$N_start = mysql_real_escape_string(check_input($_POST['start']));
		$N_end = mysql_real_escape_string(check_input($_POST['end']));
		$N_qualification = mysql_real_escape_string(check_input($_POST['qualification']));
		$N_apply = mysql_real_escape_string(check_input($_POST['apply']));
		if ($_POST['intern'] != ''){$N_intern = mysql_real_escape_string(check_input($_POST['intern']));} 
			else {$N_intern = '0';}
	    if ($_POST['fulltime'] != ''){$N_fulltime = mysql_real_escape_string(check_input($_POST['fulltime']));} 
	    	else {$N_fulltime = '0';}
	    if ($_POST['parttime'] != ''){$N_parttime = mysql_real_escape_string(check_input($_POST['parttime']));} 
	    	else {$N_parttime = '0';}
	    if ($_POST['freelance'] != ''){$N_freelance = mysql_real_escape_string(check_input($_POST['freelance']));} 
	    	else {$N_freelance = '0';}
	    $N_salary_type = mysql_real_escape_string(check_input($_POST['salary_type']));
	    $N_salary_period = mysql_real_escape_string(check_input($_POST['salary_period']));
	    $N_salary_min = mysql_real_escape_string(check_input($_POST['salary_min']));
	    if ($_POST['salary_type'] == 'negotiable'){$N_salary_max = mysql_real_escape_string(check_input($_POST['salary_max']));} 
	    	else {$N_salary_max = '0';}
	    //start $_POST for tags
	    $N_data = $_POST['tags_input'];
		$data = explode(", " ,$N_data);
		$table_name = "jobs";
		//end
		$N_admin = mysql_real_escape_string(check_input($_SESSION['admin_id'])); //admin session id
		$N_publish = mysql_real_escape_string(check_input($_POST['publish']));
		$N_status = mysql_real_escape_string(check_input($_POST['status']));

		//GET COMPANY TITLE
		$data_company = $obj_company->get_data_edit($N_company_id);
		if(is_array($data_company)){
			$companyTitle = $data_company[0]['Company_title'];
		}
		
		$save_status = "Job Posting <b>\"$companyTitle - $N_title\"</b> insertion failed.";
		$result_jobs = $obj_job->insert_data($N_title, $N_content, $N_company_id, $N_field_id, $N_level_id, $N_city_id, $N_exp, $N_start, $N_end, $N_qualification, $N_apply, $N_intern, $N_fulltime, $N_parttime, $N_freelance, $N_salary_type, $N_salary_period, $N_salary_min, $N_salary_max, $N_admin, $N_publish, $N_status);
		$target_id = $result_jobs;
		if($target_id){
			if(isset($data)){
				for($i = 0;$i < count($data);$i++){//START FOR
					$check_rtag_id = $obj_tagging->check_exist($data[$i]);//CHECK IF TAG EXIST, IF SO RETURN THE rtag_id, else returns 0

					if($check_rtag_id == 0){//IF THE TAG IS NOT EXIST
						$result_rtag = $obj_rtag->insert_data(strip_tags($data[$i]));
						if($result_rtag >= 1){
							$message[$i] = "Success to create tag {$data[$i]}.";
							$result_tag = $obj_tagging->insert_data($table_name, $target_id, $result_rtag);
							if($result_tag){
								$message[$i] .= "Success to attach tag {$data[$i]}.";
							}
						}else{
							$message[$i] = "Failed to create tag {$data[$i]}.";
						}
					}else{//IF THE TAG EXIST, GO AHEAD AND INSERT INTO tbl_tagging
						$message[$i] = "Tag {$data[$i]} already exist. ";
						$result_tag = $obj_tagging->insert_data($table_name, $target_id, $check_rtag_id);
						if($result_tag){
							$message[$i] .= "Success to insert {$data[$i]}";
						}else{
							$message[$i] .= "Failed to insert {$data[$i]}";
						}
					}
					$R_message = array("status" => "1", "message" => $message);
				}//END FOR
			}
			$save_status = "Job Posting <b>\"$companyTitle - $N_title\"</b> insertion succeed.<br>";
			$save_status .= "Active from ".date('d M Y',strtotime($N_start))." - ".date('d M Y',strtotime($N_end));
		}

		$_SESSION['status'] = $save_status;
		header("Location:adminMgr-Jobs.php");
		$obj_con->down();
	}else if($_GET['action'] == 'delete' && $_GET['jobs_ID'] != '' && $_SESSION['admin_role'] != 'Inputer'){
		$obj_con->up();

		$O_id = mysql_real_escape_string(check_input($_GET['jobs_ID']));
		$O_title = mysql_real_escape_string(check_input($_GET['jobs_Title']));

		//GET COMPANY ID
		$data_jobs = $obj_job->get_data_edit($O_id);
		if($data_jobs){
			$O_company_id = $data_jobs[0]['Jobs_ref_companyID'];
			//GET COMPANY TITLE
			$data_company = $obj_company->get_data_edit($O_company_id);
			if($data_company){
				$O_companyTitle = $data_company[0]['Company_title'];
			}
		}

		$result = $obj_job->delete_data($O_id);
		if($result == 1){
			$message .= "Job Posting <b>\"$O_companyTitle - $O_title\"</b> delete succeed.";
		}else{
			$message .= "Jobs Posting <b>\"$O_companyTitle - $O_title\"</b> delete failed.";
		}

		$_SESSION['status'] = $message;
		header("Location:adminMgr-Jobs.php");
		$obj_con->down();
	}else{
        header('Location:adminMgr.php');
    }
}else{
    header('Location:adminMgr.php');
}
?>