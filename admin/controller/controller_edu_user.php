<?php

require_once("../model/Connection.php");
$obj_con = new Connection();

require_once("../model/Edu.php");
$obj_edu = new Edu();

require_once("../model/AT_Edu.php");
$obj_atedu = new AT_Edu();

if($_SESSION['education'] == "Yes"){ // ACCESS ROLE EDUCATION PAGE
    if($_GET['action'] == '' && $_GET['edu_ID'] != '' && $_SESSION['admin_role'] != 'Inputer'){
    	$obj_con->up();

        $O_page = 1;
        if(isset($_GET['page'])){
            $O_page = mysql_real_escape_string(check_input($_GET['page']));
        }
        $O_type = mysql_real_escape_string(check_input($_GET['type'])); //for type education
        if($O_type == 'publish'){
            $type = "Publish";
        }else if($O_type == 'notpublish'){
            $type = "Not Publish";
        }else if($O_type == 'new'){
            $type = "Fresh Content";
        }

        $O_id = mysql_real_escape_string(check_input($_GET['edu_ID']));
        $total_data = $obj_atedu->get_total($O_id);//GET TOTAL ITEM
        if($total_data == 0){
            echo mysql_error();
        }
        $data_edu_lists = $obj_edu->get_index();//GET DATA EDUCATION for select box
        $total_page = $obj_atedu->get_total_page($total_data);//GET TOTAL PAGE
    	$data_edus = $obj_atedu->get_data_by_edu($O_page, $O_id);
        //var_dump($data_edus);
        $data_lists = $obj_atedu->get_index($O_id);

        $message = $_SESSION['status'];
        unset($_SESSION['status']);
    	$obj_con->down();
    }else if($_GET['action'] == 'update' && $_SESSION['admin_role'] != 'Inputer'){
    	$obj_con->up();

        $N_eduID = mysql_real_escape_string(check_input($_POST['eduID'])); //for redirect url
    	$N_id = mysql_real_escape_string(check_input($_POST['id']));
        $N_edu_id = mysql_real_escape_string(check_input($_POST['edu_id']));
        $N_username = mysql_real_escape_string(check_input($_POST['username']));
        $N_name = mysql_real_escape_string(check_input($_POST['name']));

        $N_fromMonth = mysql_real_escape_string(check_input($_POST['from_month']));
        $N_fromYear = mysql_real_escape_string(check_input($_POST['from_year']));

        $N_progress = mysql_real_escape_string(check_input($_POST['progress']));
        if($N_progress == 1){
            $N_toMonth = 0;
            $N_toYear = 0;
        }else{
            $N_toMonth = mysql_real_escape_string(check_input($_POST['to_month']));
            $N_toYear = mysql_real_escape_string(check_input($_POST['to_year']));
        }
        $N_degree = mysql_real_escape_string(check_input($_POST['degree']));
        $N_field = mysql_real_escape_string(check_input($_POST['field']));
        $N_gpa = mysql_real_escape_string(check_input($_POST['gpa']));
        $N_desc = mysql_real_escape_string(check_input($_POST['desc']));
        $N_type = $_POST['type'];
    	
        $result = $obj_atedu->update_data($N_id, $N_edu_id, $N_degree, $N_field, NULL, $N_fromMonth, $N_fromYear, $N_toMonth, $N_toYear, $N_gpa, $N_desc, $N_progress);
        //var_dump($result);
        if($result <= 0){
    	    $message .= "Something is wrong with your editing.<br />";
        }else if($result == 1){
            $message .= "<b>$N_username</b>'s Educational info has been succesfully edited.<br />";
        }else{
    	    //header('Location: logout.php');			header("Location:logout.php");
            die();
        }

    	$_SESSION['status'] = $message;
    	header("Location:adminMgr-Edu_user.php?edu_ID=$N_eduID&type=$N_type");
    	$obj_con->down();
    }else if($_GET['action'] == 'delete' && $_GET['edu_ID'] != '' && isset($_GET['atedu_ID']) && isset($_GET['username']) && $_SESSION['admin_role'] != 'Inputer'){
        $obj_con->up();

        $O_eduID = mysql_real_escape_string(check_input($_GET['edu_ID']));
        $O_id = mysql_real_escape_string(check_input($_GET['atedu_ID']));
        $O_user = mysql_real_escape_string(check_input($_GET['username']));
        
        $result = $obj_atedu->delete_data($O_id);
        if($result <= 0){
            $message .= "Something is wrong<br />";
        }else if($result == 1){
            $message .= "User name <b>\"" . $O_user . "\"</b> has been deleted successfully.<br />";
        }  

        $_SESSION['status'] = $message;
        header("Location:adminMgr-Edu_user.php?edu_ID=$O_eduID");
        $obj_con->down();
    }else{
        header('Location:adminMgr.php');
    }
}else{
    header('Location:adminMgr.php');
}
?>