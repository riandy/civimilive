<?php

require_once("../model/Connection.php");
$obj_con = new Connection();

require_once("../model/Skill.php");
$obj_skill = new Skill();

if($_SESSION['skill'] == "Yes"){ // ACCESS ROLE SKILL PAGE
    if($_GET['action'] == '' && $_GET['skill_ID'] != '' && $_SESSION['admin_role'] != 'Inputer'){
    	$obj_con->up();

    	$O_id = mysql_real_escape_string(check_input($_GET['skill_ID']));
        //$O_type = mysql_real_escape_string(check_input($_GET['type']));
    	$data_skills = $obj_skill->get_data_edit($O_id);
        //var_dump($data_certs);

    	$obj_con->down();
    }else if($_GET['action'] == 'update' && $_SESSION['admin_role'] != 'Inputer'){
    	$obj_con->up();
    	mysql_query("autocommit(FALSE)");

    	$N_ID = mysql_real_escape_string(check_input($_POST['id']));
    	$N_name = mysql_real_escape_string(check_input($_POST['name']));
    	$N_publish = mysql_real_escape_string(check_input($_POST['publish']));
        $N_admin = $_SESSION['admin_id']; //admin session id

    	if($_FILES["image"]["name"]!=null){
            $ran = rand ();
            $timestamp = time();

            //to upload in server
            $Photo_ImgLink = "../image/images/skill/" . $timestamp . $ran . cleanSpace($_FILES["image"]["name"]);
            $ThmbPhoto_ImgLink = "../image/images/skill-thmb/" . $timestamp . $ran . cleanSpace($_FILES["image"]["name"]);
            //to save in database
            $Photo_ImgLink1 = "image/images/skill/". $timestamp . $ran . cleanSpace($_FILES["image"]["name"]);
            $ThmbPhoto_ImgLink1 = "image/images/skill-thmb/". $timestamp . $ran . cleanSpace($_FILES["image"]["name"]);

            if ((($_FILES["image"]["type"] == "image/gif")|| ($_FILES["image"]["type"] == "image/jpeg")|| ($_FILES["image"]["type"] == "image/jpg")||($_FILES["image"]["type"] == "image/JPG")||($_FILES["image"]["type"] == "image/png")) && ($_FILES["image"]["size"] < 10048576) && ($_FILES["image"]["name"] != "")){
                require_once("packages/imageProcessor.php");
                if ($_FILES["image"]["error"] > 0){
                    //echo "Return Code: " . $_FILES["file"]["error"][$k] . "<br />";
                }else{
                    $file = $_FILES["image"]["tmp_name"];
                    
                    //Resizing images
                    if (true !== ($pic_error = @resampimage(1280,720,"$file","$Photo_ImgLink",0))) {
                        echo $pic_error;
                        unlink($Photo_ImgLink);
                    }
                    
                    if (true !== ($pic_error1 = @resampimagethmb("$file","$ThmbPhoto_ImgLink",0))) {
                        echo $pic_error1;
                        unlink($ThmbPhoto_ImgLink);
                    }

                    $result = $obj_skill->update_data_image($N_ID, $N_name, $Photo_ImgLink1, $ThmbPhoto_ImgLink1, $N_publish, $N_admin);
                    if($result <= 0){
                        $message .= "Something is wrong with your editing.<br />";
                    }else if($result == 1){
                        $message .= "<b>\"" . $N_name . "\"</b> has been succesfully edited.<br />";
                    }else{
                        //header('Location: logout.php');                   header("Location:logout.php");
                        die();
                    }
                }
            }
            else if($_FILES["image"]["name"] != ""){
                $message .= "Something is wrong with your uploaded skill image, please make sure that the format is (jpg, png, gif) and the size is less than 9.5MB.<br />";
            }
        }else{
            $result = $obj_skill->update_data($N_ID, $N_name, $N_publish, $N_admin);
            if($result <= 0){
                $message .= "Something is wrong with your editing.<br />";
            }else if($result == 1){
                $message .= "<b>\"" . $N_name . "\"</b> has been succesfully edited.<br />";
            }else{
                //header('Location: logout.php');           header("Location:logout.php");
                die();
            }
        }
        //End of transaction
        mysql_query("commit()");
    	mysql_query("autocommit(TRUE)");

    	$_SESSION['status'] = $message;
    	header("Location:adminMgr-Skill.php");
    	$obj_con->down();
    }else{
        header('Location:adminMgr.php');
    }
}else{
    header('Location:adminMgr.php');
}
?>