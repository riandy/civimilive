<?php 

require_once("../model/Connection.php");
$obj_con = new Connection();

require_once("../model/Testimonial.php");
$obj_testi = new Testimonial();

if($_GET['action'] == '' || $_GET['action'] == NULL){
    $obj_con->up();

    $O_page = 1;
    if(isset($_GET['page'])){
        $O_page = mysql_real_escape_string(check_input($_GET['page']));
    }

    $O_type = "new";
    if(isset($_GET['type'])){
        $O_type = mysql_real_escape_string(check_input($_GET['type']));
    }
   
    $datas = $obj_testi->get_data_by_page($O_page, $O_type);//GET DATA MESSAGE PER PAGE
    //var_dump($datas);
    if(is_array($datas)){
    	$total_data = $datas[0]['total_data_all'];
    	$total_page = $datas[0]['total_page'];
    }else{
    	$total_data = 0;
    	$total_page = 0;
    }

    $message = $_SESSION['status'];
    unset($_SESSION['status']);
    $obj_con->down();
}else if($_GET['action'] == 'update_testimonial'){
    $obj_con->up();

    $N_id = mysql_real_escape_string(check_input($_POST['testimonial_id']));
    $N_featured = mysql_real_escape_string(check_input($_POST['featured']));
    $N_publish = mysql_real_escape_string(check_input($_POST['publish']));
    $N_content = mysql_real_escape_string(check_input(str_replace("'", "’", $_POST['content'])));
    $N_url = $_POST['url']; //for redirect url
    
    $result = $obj_testi->update_testimonial($N_id, $N_featured, $N_publish, $N_content);
    if($result <= 0){
        $message .= "Something is wrong<br />";
    }else if($result == 1){
        $message .= "Testimonial <b>\"" . charLength($N_content, 100) . "\"</b> success to be updated.<br />";
    }else{
        //header('Location: logout.php');           header("Location:logout.php");
        die();
    }

    $_SESSION['status'] = $message;
    header("Location:".$N_url);
    $obj_con->down();
}else if($_GET['action'] == 'delete' && $_GET['testimonial_ID'] != ''){
    $obj_con->up();

    $O_id = mysql_real_escape_string(check_input($_GET['testimonial_ID']));
    $O_title = urldecode($_GET['testimonial_Title']);
    $O_page = mysql_real_escape_string(check_input($_GET['page']));
    $O_type = mysql_real_escape_string(check_input($_GET['type']));
    
    $result = $obj_testi->delete_data($O_id);
    if($result <= 0){
        $message .= "Delete testimonial has been failed. Something is wrong<br />";
    }else if($result == 1){
        $message .= "Testimonial <b>\"" . charLength($O_title, 100) . "\"</b> has been deleted.<br />";
    }   

    $_SESSION['status'] = $message;
    header("Location:adminMgr-Testimonial.php?page=".$O_page."&type=".$O_type);
    $obj_con->down();
}else{
    echo "error";
}
?>