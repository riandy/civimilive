<?php 

require_once("../model/Connection.php");
$obj_con = new Connection();

require_once("../model/Ref_Tag.php");
$obj_rtag = new Ref_Tag();

if($_SESSION['tag'] == "Yes"){ // ACCESS ROLE TAG PAGE
	if($_GET['action'] == '' || $_GET['action'] == null){
		$obj_con->up();

		$O_page = 1;
		if(isset($_GET['page'])){
			$O_page = mysql_real_escape_string(check_input($_GET['page']));
		}
		if(isset($_GET['letter'])){
			$letter = mysql_real_escape_string(check_input($_GET['letter']));
		}
		$table_tags = 'tag';//FOR AJAX

		$total_data = $obj_rtag->get_total($letter,0);//GET TOTAL ITEM
		$total_page = $obj_rtag->get_total_page($total_data);//GET TOTAL PAGE

		$data_tags = $obj_rtag->get_data_by_page($O_page, $letter,0);//GET DATA PER PAGE

	    $message = $_SESSION['status'];
	    $_SESSION['status'] = null;
		$obj_con->down();
	}else if($_GET['action'] == 'insert'){
		$obj_con->up();

		$N_title = mysql_real_escape_string(check_input($_POST['title']));
		$N_publish = mysql_real_escape_string(check_input($_POST['publish']));

		$result = $obj_rtag->insert_data($N_title, $N_publish);
		if($result == 1){
			$message .= "Tag <b>\"$N_title\"</b> is successfully saved.";
		}else{
			$message .= "Tag <b>\"$N_title\"</b> failed to be saved.";
		}

		$_SESSION['status'] = $message;
		header("Location:adminMgr-Tag.php");
		$obj_con->down();
	}else if($_GET['action'] == 'delete' && $_GET['tag_ID'] != '' && $_SESSION['admin_role'] != 'Inputer'){
		$obj_con->up();

		$O_id = mysql_real_escape_string(check_input($_GET['tag_ID']));
		$result = $obj_rtag->delete_data($O_id);
		if($result == 1){
			$message .= "Tag ID <b>\"$O_id\"</b> is successfully deleted.";
		}else{
			$message .= "Tag ID <b>\"$O_id\"</b> failed to be deleted.";
		}

		$_SESSION['status'] = $message;
		header("Location:adminMgr-Tag.php");
		$obj_con->down();
	}else{
    	header('Location:adminMgr.php');
	}
}else{
    header('Location:adminMgr.php');
}
?>