<?php

require_once("../model/Connection.php");
$obj_con = new Connection();

require_once("../model/Ref_Field.php");
$obj_rfield = new Ref_Field();

// require_once("../model/Industry.php");
// $obj_industry = new Industry();

if($_SESSION['field'] == "Yes"){ // ACCESS ROLE FIELD PAGE
    if($_GET['action'] == '' && $_GET['field_ID'] != '' && $_SESSION['admin_role'] != 'Inputer'){
    	$obj_con->up();

    	$O_id = mysql_real_escape_string(check_input($_GET['field_ID']));
    	$data_fields = $obj_rfield->get_data_edit($O_id);
        $parent_fields = $obj_rfield->get_parent();
    	// $data_industrys = $obj_industry->get_index();

    	$obj_con->down();
    }else if($_GET['action'] == 'update' && $_SESSION['admin_role'] != 'Inputer'){
    	$obj_con->up();
    	mysql_query("autocommit(FALSE)");

    	$N_ID = mysql_real_escape_string(check_input($_POST['id']));
    	$N_parent_id = mysql_real_escape_string(check_input($_POST['parent_id']));
    	$N_title = mysql_real_escape_string(check_input($_POST['title']));
    	$N_content = mysql_real_escape_string(check_input($_POST['content']));
        $N_admin = mysql_real_escape_string(check_input($_SESSION['admin_id'])); //admin session id
    	$N_publish = mysql_real_escape_string(check_input($_POST['publish']));

    	if($_FILES["image"]["name"]!=null){
            $ran = rand ();
            $timestamp = time();

            //to upload in server
            $Photo_ImgLink = "../image/images/field/" . $timestamp . $ran . cleanSpace($_FILES["image"]["name"]);
            $ThmbPhoto_ImgLink = "../image/images/field-thmb/" . $timestamp . $ran . cleanSpace($_FILES["image"]["name"]);
            //to save in database
            $Photo_ImgLink1 = "image/images/field/". $timestamp . $ran . cleanSpace($_FILES["image"]["name"]);
            $ThmbPhoto_ImgLink1 = "image/images/field-thmb/". $timestamp . $ran . cleanSpace($_FILES["image"]["name"]);

            if ((($_FILES["image"]["type"] == "image/gif")|| ($_FILES["image"]["type"] == "image/jpeg")|| ($_FILES["image"]["type"] == "image/jpg")||($_FILES["image"]["type"] == "image/JPG")||($_FILES["image"]["type"] == "image/png")) && ($_FILES["image"]["size"] < 10048576) && ($_FILES["image"]["name"] != "")){
                require_once("packages/imageProcessor.php");
                if ($_FILES["image"]["error"] > 0){
                    //echo "Return Code: " . $_FILES["file"]["error"][$k] . "<br />";
                }else{
                    $file = $_FILES["image"]["tmp_name"];
                	
    				//Resizing images
                    if (true !== ($pic_error = @resampimage(1280,720,"$file","$Photo_ImgLink",0))) {
                        echo $pic_error;
                        unlink($Photo_ImgLink);
                    }
                    
                    if (true !== ($pic_error1 = @resampimagethmb("$file","$ThmbPhoto_ImgLink",0))) {
                        echo $pic_error1;
                        unlink($ThmbPhoto_ImgLink);
                    }

                    $result = $obj_rfield->update_data_image($N_ID, $N_parent_id, $N_title, $N_content, $Photo_ImgLink1, $ThmbPhoto_ImgLink1, $N_admin, $N_publish);
                    if($result <= 0){
                    	$message .= "Something is wrong with your editing.<br />";
                    }else if($result == 1){
                    	$message .= "<b>\"" . $N_title . "\"</b> has been succesfully edited.<br />";
                    }else{
                        //header('Location: logout.php');					header("Location:logout.php");
                        die();
                    }
                }
            }
            else if($_FILES["image"]["name"] != ""){
                $message .= "Something is wrong with your uploaded field image, please make sure that the format is (jpg, png, gif) and the size is less than 9.5MB.<br />";
            }
        }else{
            //$result = $obj_rfield->update_data($N_ID, $N_industry_id, $N_title, $N_content, $N_publish);
            $result = $obj_rfield->update_data($N_ID, $N_parent_id, $N_title, $N_content, $N_admin, $N_publish);
            if($result <= 0){
    			$message .= "Something is wrong with your editing.<br />";
            }else if($result == 1){
                $message .= "<b>\"" . $N_title . "\"</b> has been succesfully edited.<br />";
            }else{
    			//header('Location: logout.php');			header("Location:logout.php");
                die();
            }
        }
        //End of transaction
        mysql_query("commit()");
    	mysql_query("autocommit(TRUE)");

    	$_SESSION['status'] = $message;
    	header("Location:adminMgr-Field.php");
    	$obj_con->down();
    }else{
        header('Location:adminMgr.php');
    }
}else{
    header('Location:adminMgr.php');
}
?>