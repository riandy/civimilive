<?php 

require_once("../model/Connection.php");
$obj_con = new Connection();

require_once("../model/Edu.php");
$obj_edu = new Edu();

require_once("../model/Ref_City.php");
$obj_rcity = new Ref_City();

require_once("../model/Ref_Country.php");
$obj_rcountry = new Ref_Country();

if($_SESSION['education'] == "Yes"){ // ACCESS ROLE EDUCATION PAGE
    if($_GET['action'] == '' || $_GET['action'] == null){
    	$obj_con->up();

    	$O_page = 1;
    	if(isset($_GET['page'])){
    		$O_page = mysql_real_escape_string(check_input($_GET['page']));
    	}
    	if(isset($_GET['letter'])){
    		$letter = mysql_real_escape_string(check_input($_GET['letter']));
    	}
        $O_sort = "";
        if(isset($_GET['sort'])){
            $O_sort = mysql_real_escape_string(check_input($_GET['sort']));
        }
        $O_type = "publish";
        if(isset($_GET['type'])){
            $O_type = mysql_real_escape_string(check_input($_GET['type'])); //for type education
            if($O_type == 'publish'){
                $type = "Publish";
            }else if($O_type == 'notpublish'){
                $type = "Not Publish";
            }else if($O_type == 'new'){
                $type = "Fresh Content";
            }
        }
        
        $data_lists = $obj_edu->get_index();//GET DATA EDUCATION
        $data_citys = $obj_rcity->get_index();//GET DATA CITY
        $data_countrys = $obj_rcountry->get_index();//GET DATA COUNTRY
        
        if($_GET['name'] != '' || $_GET['city'] != '' || $_GET['country'] != '' || $_GET['select_edu'] != ''){
            $O_name = mysql_real_escape_string(check_input($_GET['name']));
            $O_city = mysql_real_escape_string(check_input($_GET['city']));
            $O_country = mysql_real_escape_string(check_input($_GET['country']));
            $O_select = mysql_real_escape_string(check_input($_GET['select_edu']));

            $total_data = $obj_edu->get_total_search($O_page, $letter, $O_type, $O_name, $O_city, $O_country, $O_select);//GET TOTAL ITEM
            $total_page = $obj_edu->get_total_page($total_data);//GET TOTAL PAGE
            $data_edus = $obj_edu->get_data_search($O_page, $letter, $O_type, $O_name, $O_city, $O_country, $O_select, $O_sort);//GET DATA PER PAGE
        }
        else{
            $total_data = $obj_edu->get_total($letter, $O_type);//GET TOTAL ITEM
            if($total_data == 0){
                echo mysql_error();
            }
            $total_page = $obj_edu->get_total_page($total_data);//GET TOTAL PAGE
            $data_edus = $obj_edu->get_data_by_page($O_page, $letter, $O_type, $O_sort);//GET DATA PER PAGE
        }

        //var_dump($total_data);
        //var_dump($data_edus);

    	$message = $_SESSION['status'];
        unset($_SESSION['status']);
    	$obj_con->down();
    }else if($_GET['action'] == 'insert'){
    	$obj_con->up();

    	$N_name = mysql_real_escape_string(check_input($_POST['name']));
    	$N_city = mysql_real_escape_string(check_input($_POST['city']));
    	$N_country = mysql_real_escape_string(check_input($_POST['country']));
        $N_descEN = mysql_real_escape_string(check_input($_POST['descEN']));
        $N_descIN = mysql_real_escape_string(check_input($_POST['descIN']));
        $N_publish = mysql_real_escape_string(check_input($_POST['publish']));
        $N_admin = mysql_real_escape_string(check_input($_SESSION['admin_id'])); //admin session id
        $N_type = mysql_real_escape_string(check_input($_POST['type'])); //type page

    	$ran = rand();
        $timestamp = time();

        //to upload in server
        $Photo_ImgLink = "../image/images/edu/" . $timestamp . $ran . cleanSpace($_FILES["image"]["name"]);
        $ThmbPhoto_ImgLink = "../image/images/edu-thmb/" . $timestamp . $ran . cleanSpace($_FILES["image"]["name"]);
        //to save in database
        $Photo_ImgLink1 = "image/images/edu/" . $timestamp . $ran . cleanSpace($_FILES["image"]["name"]);
        $ThmbPhoto_ImgLink1 = "image/images/edu-thmb/" . $timestamp . $ran . cleanSpace($_FILES["image"]["name"]);

        if ((($_FILES["image"]["type"] == "image/gif") || ($_FILES["image"]["type"] == "image/jpeg") || ($_FILES["image"]["type"] == "image/jpg") || ($_FILES["image"]["type"] == "image/JPG") || ($_FILES["image"]["type"] == "image/png")) && ($_FILES["image"]["size"] < 10048576) && ($_FILES["image"]["name"] != "")) {
            require_once("packages/imageProcessor.php");
            if ($_FILES["image"]["error"] > 0) {
                //echo "Return Code: " . $_FILES["file"]["error"][$k] . "<br />";
            } else {
                $file = $_FILES["image"]["tmp_name"];
                //Resizing images
                if (true !== ($pic_error = @resampimage(1280, 500, "$file", "$Photo_ImgLink", 0))) {
                    echo $pic_error;
                    unlink($Photo_ImgLink);
                }

                if (true !== ($pic_error1 = @resampimagethmb("$file", "$ThmbPhoto_ImgLink", 0))) {
                    echo $pic_error1;
                    unlink($ThmbPhoto_ImgLink);
                }

                $result = $obj_edu->insert_data($N_name, $N_city, $N_country, $N_descEN, $N_descIN, $Photo_ImgLink1, $ThmbPhoto_ImgLink1, $N_admin, $N_publish);
                if(!$result){
                    $message .= "Something is wrong with your submission.<br />";
                }else if($result){
                    $message .= "<b>\"" . $N_name . "\"</b> has been succesfully added as a new education logo.<br />";
                }else{
                    //header('Location: logout.php');               header("Location:logout.php");
                    die();
                }
            }
        }else if ($_FILES["image"]["name"] != "") {
            $message .= "Something is wrong with your uploaded education logo, please make sure that the format is (jpg, png, gif) and the size is less than 9.5MB.<br />";
        }

    	$_SESSION['status'] = $message;
    	header("Location:adminMgr-Edu.php?type=$N_type");
    	$obj_con->down();
    }else if($_GET['action'] == 'delete' && $_GET['edu_ID'] != '' && $_SESSION['admin_role'] != 'Inputer'){
    	$obj_con->up();

        //for link redirect
        $O_page = mysql_real_escape_string(check_input($_GET['page']));
        $O_letter = mysql_real_escape_string(check_input($_GET['letter']));
        $O_type = mysql_real_escape_string(check_input($_GET['type']));
        $O_name = mysql_real_escape_string(check_input($_GET['name']));
        $O_city = mysql_real_escape_string(check_input($_GET['city']));
        $O_country = mysql_real_escape_string(check_input($_GET['country']));
        $O_select = mysql_real_escape_string(check_input($_GET['select_edu']));
        $O_sort = mysql_real_escape_string(check_input($_GET['sort']));
        //for delete
    	$O_id = mysql_real_escape_string(check_input($_GET['edu_ID']));
    	$O_title = mysql_real_escape_string(check_input($_GET['edu_school']));
    	
        $result = $obj_edu->delete_data($O_id);
        if($result <= 0){
            $message .= "Something is wrong while deleting the logo, please try again.<br />";
        }else if($result == 1){
            $message .= "School name <b>\"" . $O_title . "\"</b> has been deleted successfully.<br />";
        }

    	$_SESSION['status'] = $message;
    	header("Location:adminMgr-Edu.php?page=$O_page&letter=$O_letter&type=$O_type&name=$O_name&city=$O_city&country=$O_country&select_edu=$O_select&sort=$O_sort");
    	$obj_con->down();
    }else{
        header('Location:adminMgr.php');
    }
}else{
    header('Location:adminMgr.php');
}
?>