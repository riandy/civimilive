<?php
ob_start("ob_gzhandler");
require_once("packages/require.php");
include("packages/check_login.php");//USED BY ALL PAGE BUT index.php
include("controller/controller_user_member_detail.php");
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <?php include('packages/head.php');?>
    <!-- calendar -->

    <link rel="stylesheet" type="text/css" href="packages/jQuery-Tags/jquery-ui.css" />
    <link rel="stylesheet" type="text/css" href="packages/jQuery-Tags/jquery.tagsinput.css" />
    <script src="//code.jquery.com/jquery-1.10.2.js"></script>
    <script type="text/javascript" src="packages/jQuery-Tags/jquery-ui.js"></script>
    <script type="text/javascript" src="packages/jQuery-Tags/jquery.tagsinput.js"></script>

    <script>
  $(function() {
    $( ".datepicker" ).datepicker({dateFormat: 'yy-mm-dd' });
  });
  </script>
    <style>
        body .modal {
            width: 50%; /* desired relative width */
            left: 25%; /* (100%-width)/2 */
            /* place center */
            margin-left:auto;
            margin-right:auto; 
        }
    </style>
</head>
<body class="civMain" onload="startTime()">
    <?php require_once("admin-Header.php");?>
    <div class="container">
        <div id="breadcrumb-parent" class="row-fluid">
            <div id="breadcrumb" class="span6">
                <a id="dashboard-link" href="#dashboard"><i class="icon-th-large"></i> Dashboard</a>
                : <a href='adminMgr-User_member.php?type=<?php echo $O_type;?>'>User Member Management</a>
            </div>
            <div id="notifications" class="span6 align-right">
                <span>Welcome on <span id="date"></span></span>
            </div>
        </div>
        <div id="menuDashboard" class="row-fluid">
            
            <div id="leftNav-Parent" class="span3">
                <?php $curPage='user-member'; ?>
                <?php require_once("admin-Sidebar.php");?>
            </div>
            
            <div id="rightContent" class="span9">
                
                <?php
                if($message!=null)
                {
                    echo "<div class='alert alert-info'>" . $message . "</div><br />";
                }
                ?>
                <div class="row-fluid">
                    <div class="span3">
                        <a href="adminMgr-User_member.php?type=<?php echo $O_type;?>" class="btn btn-default btn-small"><i class="icon-circle-arrow-left"></i> Back</a>
                    </div>
                    <div class="span6">
                        <h4 style="margin: 0;text-align:center;">User Member Edit (<?php echo $data_users[0]['User_fname']." ".$data_users[0]['User_lname'];?>)</h4>
                    </div>
                </div>
                <hr/>
                <?php $k=0;
                foreach($data_users as $data_user){?>
                <form name="addUser" action="adminMgr-User_member_detail.php?action=update" method="post" enctype="multipart/form-data" onsubmit="loadingText()">
                    <div class="row-fluid up1">
                        <div class="span4 modal-label"><strong>Username&nbsp;*</strong>:</div>
                        <div class="span8"><input name="username" type="text" class="text input-block-level" placeholder="Username" required value="<?php echo $data_user['User_username'];?>"></div>
                    </div>
                    <div class="row-fluid up1">
                        <div class="span4 modal-label"><strong>E-mail&nbsp;*</strong>:</div>
                        <div class="span8"><input name="email" type="email" class="text input-block-level" placeholder="E-mail" required value="<?php echo $data_user['User_email'];?>"></div>
                    </div>
                    <div class="row-fluid up1">
                        <div class="span4 modal-label"><strong>First Name&nbsp;*</strong>:</div>
                        <div class="span8"><input name="fname" type="text" class="text input-block-level" placeholder="First Name" required value="<?php echo $data_user['User_fname'];?>"></div>
                    </div>
                    <div class="row-fluid up1">
                        <div class="span4 modal-label"><strong>Last Name&nbsp;*</strong>:</div>
                        <div class="span8"><input name="lname" type="text" class="text input-block-level" placeholder="Last Name" required value="<?php echo $data_user['User_lname'];?>"></div>
                    </div>
                    <div class="row-fluid up1">
                        <div class="span4 modal-label"><strong>Sex&nbsp;*</strong>:</div>
                        <div class="span8">
                            <select name="sex" class="text" required>
                                <option value="">Select Sex</option>
                                <option <?php if($data_user['User_sex'] == "Male"){ echo "selected=selected"; }?> value="Male">Male</option>
                                <option <?php if($data_user['User_sex'] == "Female"){ echo "selected=selected"; }?> value="Female">Female</option>
                            </select>
                        </div>
                    </div>
                    <div class="row-fluid up1">
                        <div class="span4 modal-label">Bio Description :</div>
                        <div class="span8"><textarea name="desc" class="input-block-level" wrap="soft" placeholder="description" rows="5"><?php echo $data_user['User_bioDesc'];?></textarea></div>
                    </div>
                    <div class="row-fluid up1">
                        <div class="span4 modal-label"><strong>DOB Day&nbsp;*</strong>:</div>
                        <div class="span8">
                            <select name="birthday1" class="text" required>
                                <option value="">Select Day</option>
                                <?php for($i=1; $i <= 31; $i++){ ?>
                                <option <?php if($data_user['User_DOBday'] == $i){ echo "selected=selected"; }?> value="<?php echo $i;?>"><?php echo $i;?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="row-fluid up1">
                        <div class="span4 modal-label"><strong>DOB Month&nbsp;*</strong>:</div>
                        <div class="span8">
                            <select name="birthday2" class="text" required>
                                <option value="">Select Month</option>
                                <?php $month = '12';
                                for ($months = '1'; $months <= $month; $months++) { ?>
                                <option <?php if($data_user['User_DOBmonth'] == $months){ echo "selected=selected"; }?> value="<?php echo $months;?>"><?php echo date("F", strtotime("$months/12/10"));?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="row-fluid up1">
                        <div class="span4 modal-label"><strong>DOB Year&nbsp;*</strong>:</div>
                        <div class="span8">
                            <select name="birthday3" class="text" required>
                                <option value="">Select Year</option>
                                <?php for($i=date("Y"); $i >= 1950;  $i--){ ?>
                                <option <?php if($data_user['User_DOB'] == $i){ echo "selected=selected"; }?> value="<?php echo $i;?>"><?php echo $i;?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="row-fluid up1">
                        <div class="span4 modal-label">Interest :</div>
                        <div class="span8">
                            <select name="interest" class="text">
                                <option value="">Select Interest</option>
                                <option <?php if($data_user['User_interest'] == "1"){ echo "selected=selected"; }?> value="1">Interest 1</option>
                                <option <?php if($data_user['User_interest'] == "2"){ echo "selected=selected"; }?> value="2">Interest 2</option>
                                <option <?php if($data_user['User_interest'] == "3"){ echo "selected=selected"; }?> value="3">Interest 3</option>
                                <option <?php if($data_user['User_interest'] == "4"){ echo "selected=selected"; }?> value="4">Interest 4</option>
                                <option <?php if($data_user['User_interest'] == "5"){ echo "selected=selected"; }?> value="5">Interest 5</option>
                            </select>
                        </div>
                    </div>
                    <div class="row-fluid up1">
                        <div class="span4 modal-label">User Obj :</div>
                        <div class="span8"><textarea name="obj" class="input-block-level" wrap="soft" placeholder="description" rows="5"><?php echo $data_user['User_obj'];?></textarea></div>
                    </div>
                    <div class="row-fluid up1">
                        <div class="span4 modal-label">Address :</div>
                        <div class="span8"><textarea name="address" class="input-block-level" wrap="soft" placeholder="description" rows="5"><?php echo $data_user['User_address'];?></textarea></div>
                    </div>
                    <div class="row-fluid up1">
                        <div class="span4 modal-label">City :</div>
                        <div class="span8"><input name="city" type="text" class="text input-block-level" placeholder="City" value="<?php echo $data_user['User_city'];?>"/></div>
                    </div>
                    <div class="row-fluid up1">
                        <div class="span4 modal-label">State :</div>
                        <div class="span8"><input name="state" type="text" class="text input-block-level" placeholder="State" value="<?php echo $data_user['User_state'];?>"/></div>
                    </div>
                    <div class="row-fluid up1">
                        <div class="span4 modal-label">Country :</div>
                        <div class="span8"><input name="country" type="text" class="text input-block-level" placeholder="Country" value="<?php echo $data_user['User_country'];?>"/></div>
                    </div>
                    <div class="row-fluid up1">
                        <div class="span4 modal-label">Phone :</div>
                        <div class="span8"><input name="phone" type="text" class="text input-block-level" placeholder="Phone" value="<?php echo $data_user['User_phone'];?>"/></div>
                    </div>
                    <div class="row-fluid">
                        <div id="SubStatus" class="span12 align-center"></div>
                    </div>
                    <div class="row-fluid">
                    <div class="span12">
                    <div class="well" align="center">
                        <input name="id" type="hidden" value="<?php echo $data_user['User_ID'];?>"/>
                        <input name="type" type="hidden" value="<?php echo $O_type;?>"/>
                        <div class="btn-group" align="center">
                            <a href="adminMgr-User_member.php?type=<?php echo $O_type;?>" class="btn btn-danger" style="color: white;"><i class="icon-remove icon-white"></i> Cancel</a>
                            <button id="btn-submit" name="saveUser" type="submit" class="btn btn-success"><i class="icon-ok icon-white"></i> Save User</button>
                        </div>
                    </div>
                    </div>
                    </div>
                </form>
                <?php $k++; }?>
            </div>
        </div>
        <div class="row-fluid">
            <div class="span6">
                <?php require_once("admin-Footer.php"); ?>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $("#message1").fadeOut(8400);
        function add_zindex(){
            $('.ui-autocomplete').css("z-index", 1055);
        }
    </script>
    <script src="js/globalJS.js" type="text/javascript"></script>
</body></html>