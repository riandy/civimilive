<?php
ob_start("ob_gzhandler");
require_once("packages/require.php");
include("packages/check_login.php");//USED BY ALL PAGE BUT index.php
include("controller/controller_skill_detail.php");
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <?php include('packages/head.php');?>
    <!-- calendar -->

    <link rel="stylesheet" type="text/css" href="packages/jQuery-Tags/jquery-ui.css" />
    <link rel="stylesheet" type="text/css" href="packages/jQuery-Tags/jquery.tagsinput.css" />
    <script src="//code.jquery.com/jquery-1.10.2.js"></script>
    <script>
  $(function() {
    $( ".datepicker" ).datepicker({dateFormat: 'yy-mm-dd' });
  });
  </script>
    <style>
        body .modal {
            width: 50%; /* desired relative width */
            left: 25%; /* (100%-width)/2 */
            /* place center */
            margin-left:auto;
            margin-right:auto; 
        }
    </style>
</head>
<body class="civMain" onload="startTime()">
    <?php require_once("admin-Header.php");?>
    <div class="container">
        <div id="breadcrumb-parent" class="row-fluid">
            <div id="breadcrumb" class="span6">
                <a id="dashboard-link" href="#dashboard"><i class="icon-th-large"></i> Dashboard</a>
                : <a href='adminMgr-Skill.php'>Skill Management</a>
            </div>
            <div id="notifications" class="span6 align-right">
                <span>Welcome on <span id="date"></span></span>
            </div>
        </div>
        <div id="menuDashboard" class="row-fluid">
            
            <div id="leftNav-Parent" class="span3">
                <?php $curPage='skill'; ?>
                <?php require_once("admin-Sidebar.php");?>
            </div>
            
            <div id="rightContent" class="span9">
                
                <?php
                if($message!=null)
                {
                    echo "<div class='alert alert-info'>" . $message . "</div><br />";
                }
                ?>
                <div class="row-fluid">
                    <div class="span3">
                        <a href="adminMgr-Skill.php" class="btn btn-default btn-small"><i class="icon-circle-arrow-left"></i> Back</a>
                    </div>
                    <div class="span6">
                        <h4 style="margin: 0;text-align:center;">Skill Edit (<?php echo $data_skills[0]['Skill_name'];?>)</h4>
                    </div>
                </div>
                <hr/>
                <?php $k=0;
                foreach($data_skills as $data_skill){?>
                <form name="addSkill" action="adminMgr-Skill_detail.php?action=update" method="post" enctype="multipart/form-data" onsubmit="loadingText()">
                    <div class="row-fluid">
                        <div class="span4 modal-label"><strong>Skill Name&nbsp;*</strong>:</div>
                        <div class="span8"><input name="name" type="text" class="text input-block-level" placeholder="Skill Name, Max 48 Character" required value="<?php echo $data_skill['Skill_name'];?>"></div>
                    </div>
                    <div class="row-fluid up1">
                        <div class="span4 modal-label">Skill Image (Optional) :</div>
                        <div class="span8">
                            <a class="proPhoto" href="<?php echo check_image_url($data_skill['Skill_img'], 'skill');?>">
                                <img class="img-circle" src="<?php echo check_image_url($data_skill['Skill_img_thmb'], 'skill');?>" class="eduAdminImg" />
                            </a>
                        </div>
                    </div>
                    <div class="row-fluid up1">
                        <div class="span8 offset4">
                            <br />
                            <input name="image" type="file" class="text" maxlength="128" placeholder="skill image" />
                            <br />
                        </div>
                    </div>
                    <div class="row-fluid">
                        <div class="offset4 span8"><small>image format has to be jpg, jpeg, gif, png.</small></div>
                    </div>
                    <div class="row-fluid up1">
                        <div class="span4 modal-label">Skill Publish :</div>
                        <div class="span8">
                            <select name="publish" class="text">
                                <option <?php if($data_skill['Skill_publish'] == "Publish"){ echo "selected=selected"; }?> value="Publish">Publish</option>
                                <option <?php if($data_skill['Skill_publish'] == "Not Publish"){ echo "selected=selected"; }?> value="Not Publish">Not Publish</option>
                            </select>
                        </div>
                    </div>
                    <div class="row-fluid">
                        <div id="SubStatus" class="span12 align-center"></div>
                    </div>
                    <div class="row-fluid">
                    <div class="span12">
                    <div class="well" align="center">
                        <input name="id" type="hidden" value="<?php echo $data_skill['Skill_ID'];?>"/>
                        <div class="btn-group" align="center">
                            <?php $onclickDel = "\"if(window.confirm('Are you sure you want to delete {$data_skill['Skill_name']}?')) location.href='adminMgr-Skill.php?action=delete&skill_ID={$data_skill['Skill_ID']}&skill_Name={$data_skill['Skill_name']}';\""; ?>
                            <a href="#" <? echo "onclick=$onclickDel";?> class="btn btn-danger" style="color: white;"><i class="icon-remove icon-white"></i> Delete Skill</a>
                            <button id="btn-submit" name="saveSkill" type="submit" class="btn btn-success"><i class="icon-ok icon-white"></i> Save Skill</button>
                        </div>
                    </div>
                    </div>
                    </div>
                </form>
                <?php $k++; }?>
            </div>
        </div>
        <div class="row-fluid">
            <div class="span6">
                <?php require_once("admin-Footer.php"); ?>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $("#message1").fadeOut(8400);
    </script>
    <script src="js/globalJS.js" type="text/javascript"></script>
</body></html>