<?php
ob_start("ob_gzhandler");
require_once("packages/require.php");
include("packages/check_login.php");//USED BY ALL PAGE BUT index.php
include("controller/controller_edu_detail.php");
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <?php include('packages/head.php');?>
    <!-- calendar -->

    <link rel="stylesheet" type="text/css" href="packages/jQuery-Tags/jquery-ui.css" />
    <link rel="stylesheet" type="text/css" href="packages/jQuery-Tags/jquery.tagsinput.css" />
    <script src="//code.jquery.com/jquery-1.10.2.js"></script>
    <script src="js/jquery.select-to-autocomplete.js"></script>
    <!-- select 2 -->
    <link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/css/select2.min.css" rel="stylesheet" />
    <script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/select2.min.js"></script>
    <style>
        body .modal {
            width: 50%; /* desired relative width */
            left: 25%; /* (100%-width)/2 */
            /* place center */
            margin-left:auto;
            margin-right:auto; 
        }
        .select2 {
            width: 100% !important;
            margin-bottom: 10px !important;
        }

        .select2-selection {
            border-color: #ccc !important;
            min-height: 30px !important;
        }
        .select2-search__field {
            padding: 6px !important;
            min-height: 28px;
            margin-bottom: 0px !important;
        }
    </style>
</head>
<body class="civMain" onload="startTime()">
    <?php require_once("admin-Header.php");?>

    <!-- start top nav -->
    <?php $curPage='module'; ?>
    <?php require_once("admin-top_nav.php");?>
    <!-- end top nav -->

    <div class="container">
        <div id="menuDashboard" class="row-fluid">
            <div id="rightContent" class="span12">
                
                <?php
                if($message!=null)
                {
                    echo "<div id='message1' class='alert alert-info'>" . $message . "</div><br />";
                }
                ?>
                <div class="row-fluid">
                    <div class="span3">
                        <a href="adminMgr-Edu.php?type=<?php echo $O_type;?>" class="btn btn-default btn-small"><i class="icon-circle-arrow-left"></i> Back</a>
                    </div>
                    <div class="span6">
                        <h4 style="margin: 0;text-align:center;">Education <?php echo $type;?> Edit <br>(<?php echo correctDisplay($data_edus[0]['Edu_schoolName']);?>)</h4>
                    </div>
                </div>
                <hr/>
                <?php $k=0;
                foreach($data_edus as $data_edu){?>
                <form name="addEducation" action="adminMgr-Edu_detail.php?action=update" method="post" enctype="multipart/form-data" onsubmit="loadingText()">
                    <!--
                    <div class="row-fluid up1">
                        <div class="span4 modal-label"><strong>Industry Title&nbsp;*</strong>:</div>
                        <div class="span8">
                            <select name="industry_id" class="text" required="required">
                                <option value="">Select Industry</option>
                                <?php foreach($data_industrys as $data_industry){ ?>
                                <option <?php if($data_company['Company_ref_industryID'] == $data_industry['Industry_ID']){ echo "selected=selected"; }?> value="<?php echo $data_industry['Industry_ID'];?>"><?php echo $data_industry['Industry_title'];?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    !-->
                    <div class="row-fluid up2">
                        <div class="span4 align-right modal-label"><strong>Education Name&nbsp;*</strong>:</div>
                        <div class="span5"><input name="name" type="text" class="text input-block-level" placeholder="Education Name" value="<?php echo correctDisplay($data_edu['Edu_schoolName']);?>"></div>
                    </div>
                    <div class="row-fluid">
                        <div class="span4 align-right modal-label"><strong>Education City&nbsp;*</strong>:</div>
                        <div class="span5">
                            <!-- <input name="city" type="text" class="text input-block-level" placeholder="Education City" value="<?php echo $data_edu['Edu_schoolCity'];?>"> -->
                            <select name="city" class="text input-block-level city-select" required="required">
                                <option></option>
                                <?php foreach($data_citys as $data_city){ ?>
                                <option value="<?php echo $data_city['City_title'];?>" <?php if($data_edu['Edu_schoolCity'] == $data_city['City_title']){ ?>selected="selected"<?php } ?>><?php echo $data_city['City_title'];?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="row-fluid">
                        <div class="span4 align-right modal-label"><strong>Education Country&nbsp;*</strong>:</div>
                        <div class="span5">
                            <!-- <input name="country" type="text" class="text input-block-level" placeholder="Education Name" value="<?php echo $data_edu['Edu_schoolCountry'];?>"> -->
                            <select name="country" class="text input-block-level country-select" required="required">
                                <option></option>
                                <?php foreach($data_countrys as $data_country){ ?>
                                <option value="<?php echo $data_country['Country_title'];?>" <?php if($data_edu['Edu_schoolCountry'] == $data_country['Country_title']){ ?>selected="selected"<?php } ?>><?php echo $data_country['Country_title'];?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="row-fluid up1">
                        <div class="span4 align-right modal-label">Logo :</div>
                        <div class="span5">
                            <a class="proPhoto" href="<?php echo check_image_url($data_edu['Edu_img'], 'education');?>">
                                <img class="img-circle" src="<?php echo check_image_url($data_edu['Edu_img_thmb'], 'education');?>" class="eduAdminImg" />
                            </a>
                        </div>
                    </div>
                    <div class="row-fluid up1">
                        <div class="span5 offset4">
                            <br />
                            <input name="image" type="file" class="text" maxlength="128" placeholder="school logo" />
                            <br />
                        </div>
                    </div>
                    <div class="row-fluid">
                        <div class="offset4 span5"><small>image format has to be jpg, jpeg, gif, png.</small></div>
                    </div>
                    <div class="row-fluid up1">
                        <div class="span4 align-right modal-label">Education Description EN :</div>
                        <div class="span8"><textarea name="descEN" class="text input-block-level" rows="5" placeholder="Description about education in english"><?php echo $data_edu['Edu_desc_EN'];?></textarea></div>
                    </div>
                    <div class="row-fluid up1">
                        <div class="span4 align-right modal-label">Education Description IN :</div>
                        <div class="span8"><textarea name="descIN" class="text input-block-level" rows="5" placeholder="Description about education in bahasa"><?php echo $data_edu['Edu_desc_IN'];?></textarea></div>
                    </div>
                    <div class="row-fluid up1">
                        <div class="span4 align-right modal-label">Education Publish :</div>
                        <div class="span5">
                            <select name="publish" class="text">
                                <option <?php if($data_edu['Edu_publish'] == "Publish"){ echo "selected=selected"; }?> value="Publish">Publish</option>
                                <option <?php if($data_edu['Edu_publish'] == "Not Publish"){ echo "selected=selected"; }?> value="Not Publish">Not Publish</option>
                            </select>
                        </div>
                    </div>
                    <div class="row-fluid">
                        <div id="SubStatus" class="span12 align-center"></div>
                    </div>
                    <div class="row-fluid">
                    <div class="span12">
                    <div class="well" align="center">
                        <input name="id" type="hidden" value="<?php echo $data_edu['Edu_ID'];?>"/>
                        <input name="type" type="hidden" value="<?php echo $O_type;?>"/>
                        <div class="btn-group" align="center">
                            <a href="adminMgr-Edu.php?type=<?php echo $O_type;?>" class="btn btn-danger" style="color: white;"><i class="icon-remove icon-white"></i> Cancel</a>
                            <button id="btn-submit" name="saveEducation" type="submit" class="btn btn-success"><i class="icon-check icon-white"></i> Save Education</button>
                        </div>
                    </div>
                    </div>
                    </div>
                </form>
                <?php $k++; }?>
            </div>
        </div>
        <div class="row-fluid">
            <div class="span6">
                <?php require_once("admin-Footer.php"); ?>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $("#message1").fadeOut(8400);
        $(document).ready(function() {
          $(".city-select").select2({
            placeholder: "Select City",
            allowClear: true
          });
          $(".country-select").select2({
            placeholder: "Select Country",
            allowClear: true
          });
        });
    </script>
    <script src="js/globalJS.js" type="text/javascript"></script>
</body></html>