<?php
ob_start("ob_gzhandler");
require_once("packages/require.php");
include("packages/check_login.php");//USED BY ALL PAGE BUT index.php
include("controller/controller_company_user.php");
$page_name = "adminMgr-Company_user.php";
$type_page = "type=$O_type";
$company_page = "company_ID=$O_id";
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <?php include('packages/head.php');?>
    <!-- calendar -->
    <link rel="stylesheet" type="text/css" href="packages/jQuery-Tags/jquery-ui.css" />
    <link rel="stylesheet" type="text/css" href="packages/jQuery-Tags/jquery.tagsinput.css" />
    <script src="//code.jquery.com/jquery-1.10.2.js"></script>

    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.0/themes/smoothness/jquery-ui.css">
    <script src="//code.jquery.com/ui/1.11.1/jquery-ui.js"></script>
    <script src="js/jquery.select-to-autocomplete.js"></script>
    <!-- select 2 -->
    <link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/css/select2.min.css" rel="stylesheet" />
    <script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/select2.min.js"></script>

    <script>
  $(function() {
    $( ".datepicker" ).datepicker({dateFormat: 'yy-mm-dd' });
    $('.autodrop').selectToAutocomplete();
  });
  </script>
    <style>
        body .modal {
            width: 50%; /* desired relative width */
            left: 25%; /* (100%-width)/2 */
            /* place center */
            margin-left:auto;
            margin-right:auto; 
        }
    </style>
</head>
<body class="civMain" onload="startTime()">
    <?php require_once("admin-Header.php");?>

    <!-- start top nav -->
    <?php $curPage='module'; ?>
    <?php require_once("admin-top_nav.php");?>
    <!-- end top nav -->

    <div class="container">
        <div id="menuDashboard" class="row-fluid">
            <div id="rightContent" class="span12">
                
                <?php
                if($message!=null)
                {
                    echo "<div id='message1' class='alert alert-info'>" . $message . "</div><br />";
                }
                ?>
                <?php if(is_array($data_companys)){ ?>
                <div class="row-fluid">
                    <div class="span12 align-center">
                        <h4 style="margin: 0;">Users who have <br>"<?php echo $data_companys[0]['Company_title'];?>"<br> in their Company</h4>
                    </div>
                </div>
                <hr/>
                <?php } ?>
                <div class="row-fluid">
                    <div class="span6 align-left">
                        <a href="adminMgr-Company.php?type=<?php echo $O_type;?>" class="btn btn-default btn-small"><i class="icon-circle-arrow-left"></i> Back</a>
                    </div>
                    <div class="span6 align-right">
                        Total Employee : <span class="badge badge-info"><?php echo $total_data;?></span><br/>
                        Showing : <span class="badge badge-info"><?php echo (($O_page-1)*20+1)." - ".(count($data_companys)+(($O_page-1)*20));?></span> of <span class="badge badge-info"><?php echo $total_data;?></span> 
                    </div>
                </div>
                <hr/>
                <!-- STARTS LETTER -->
                <div class="row-fluid">
                    <div class="span8">
                        <div class="input-append">
                            <select id="auto_search" class="autodrop"  > 
                            <option value=''>start typing...</option>
                                <?php $ii =0;foreach($data_lists as $data_list){ ?>
                                    <option id="company-<?=$ii;?>" value="<?php echo $data_list['ATwork_ID'];?>"><?php echo $data_list['User_fname']." ".$data_list['User_lname'].", ".$data_list['User_email'];?></option>
                                    <?php $ii++;} ?>
                            </select>
                            <!-- <span>please type what you are looking</span> -->
                            <a href="javascript:;" onclick="goDetail();" data-toggle="modal" data-target="#modalCompany" class="btn btn-info" style="color: white;">Search</a>
                        </div>
                        <script>
                        function goDetail(){
                            var detail = $('#auto_search').val();
                            if(detail > 0 && isNaN(detail) == false){
                                opencompany(detail);
                            }
                        }
                        </script>
                    </div><!-- ENDS LETTER -->
                </div>
                <hr>
                <table class="table table-hover up2">
                <thead>
                    <tr>
			            <th>#</th>
                        <th>Pic</th>
                        <th>Name</th>
                        <th>E-mail</th>
                        <th>From</th>
                        <th>To</th>
                         <th>Job Title</th>
                        <th style="text-align:center;width:70px !important;">Action</th>
                    </tr>
                </thead>
                <?php
                if(!is_array($data_companys)){
                    echo "
                    <tbody><td colspan='8'><h5>There is no data.</h5></td></tbody>";
                }else{?>
                <tbody>
                <?php $k=1;
                    foreach($data_companys as $data_company){
                        $onclickDel = "\"if(window.confirm('Are you sure you want to delete {$data_company['User_fname']} {$data_company['User_lname']}, {$data_company['User_email']}?')) location.href='adminMgr-Company_user.php?action=delete&type={$O_type}&company_ID={$data_company['Company_ID']}&atework_ID={$data_company['ATwork_ID']}&username={$data_company['User_username']}';\"";
                    ?>
                    <tr>
	                    <td><?php echo ($O_page-1)*20+$k;?></td>
                        <td id="src-company-<?=$data_lists[$k-1]['ATwork_ID'];?>" data-company-fullname="<?=$data_lists[$k-1]['User_fname']." ".$data_lists[$k-1]['User_lname'];?>" data-company-username="<?=$data_lists[$k-1]['User_username'];?>" data-company-id="<?=$data_lists[$k-1]['Company_ID'];?>" data-company-from-mon="<?=$data_lists[$k-1]['ATwork_from_month'];?>" data-company-from-ye="<?=$data_lists[$k-1]['ATwork_from_year'];?>" data-company-to-mon="<?=$data_lists[$k-1]['ATwork_to_month'];?>" data-company-to-ye="<?=$data_lists[$k-1]['ATwork_to_year'];?>" data-company-progress="<?=$data_lists[$k-1]['ATwork_cuurent'];?>" data-work-title="<?=$data_lists[$k-1]['ATwork_title'];?>" data-work-desc="<?=$data_lists[$k-1]['ATwork_desc'];?>">
                            <a class="proPhoto" href="<?php echo check_image_url($data_company['User_proPhoto'], 'user');?>"><img class="img-circle" src="<?php echo check_image_url($data_company['User_proPhotoThmb'], 'user');?>" /></a>
                        </td>
                        <td><?php echo $data_company['User_fname']." ".$data_company['User_lname'];?></td>
                        <td><?php echo $data_company['User_email'];?></td>
                        <td><?php echo $data_company['ATwork_from_month'].", ".$data_company['ATwork_from_year'];?></td>
                        <?php if($data_company['ATwork_to_month'] == 0 && $data_company['ATwork_to_year'] == 0){$to = "In Progress";}
                            else{$to = $data_company['ATwork_to_month'].", ".$data_company['ATwork_to_year'];}?>
                        <td><?php echo $to;?></td>
                        <td><?php echo $data_company['ATwork_title'];?></td>
                        <td style="text-align: center;">
                            <div class="btn-group">
                                <a href="javascript:;" onclick="copyValue(<?php echo $data_company['ATwork_ID'];?>);" type="button" class="btn btn-small" data-toggle="modal" data-target="#modalCompany"><i class="icon-edit"></i> edit</a>
                                <a href="#" <? echo "onclick=$onclickDel";?> type="button" class="btn btn-danger btn-small" style="color: white;" ><i class="icon-trash icon-white"></i> delete</a>
                                <a target="_blank" href="http://www.civimi.com/beta/profile/<?php echo $data_company['User_username'];?>.cvm" type="button" class="btn btn-info btn-small" style="color: white;"><i class="icon-user icon-white"></i> view profile</a>
                            </div>
                        </td>
                    </tr>
                <?php $k++; }?>
                </tbody>
                <? }?>
                </table>
                <br/>
                <!-- START PAGINATION COMPANY USER !-->
                <div class="pagination align-center">
                  <ul class="pagination pagination-sm">
                    <?php
                    $batch = getBatch($O_page);
                    if($batch < 1){$batch = 1;}
                    $prevLimit = 1 +(10*($batch-1));
                    $nextLimit = 10 * $batch;

                    if($nextLimit > $total_page){$nextLimit = $total_page;}
                    if ($total_page > 1 && $O_page > 1) {
                        echo "<li><a href='".$page_name."?page=1&".$type_page."&".$company_page."' class='pagination-slide'>&lt;&lt; First</a></li>";
                    }
                    if($batch > 1 && $O_page > 10){
                        echo "<li><a href='".$page_name."?page=".($prevLimit-1)."&".$type_page."&".$company_page."' class='pagination-slide'>&lt; Previous 10</a></li>";
                    }
                    for($mon = $prevLimit; $mon <= $nextLimit;$mon++){?>
                    <li class="<?php if($mon == $O_page){echo 'active';}?>"><span class="pagination-theme"><a href="<?php echo $page_name."?page=".$mon."&".$type_page."&".$company_page;?>" ><?php echo $mon;?></a></span></li>
                    <?php if($mon == $nextLimit && $mon < $total_page){if($mon >= $total_page){ $mon -=1;}
                    echo "<li><a href='".$page_name."?page=".($mon+1)."&".$type_page."&".$company_page."' class='pagination-slide'>Next 10 &gt;</a></li>";
                }                               }
                if ($total_page > 1 &&  ($O_page != $total_page)) {
                    echo "<li><a href='".$page_name."?page=".$total_page."&".$type_page."&".$company_page."' class='pagination-slide'>Last &gt;&gt;</a></li>";
                }
                ?>
                </ul>
                </div>
                <!-- END PAGINATION COMPANY USER !-->
            </div>
        </div>
        <div class="row-fluid">
            <div class="span6">
                <?php require_once("admin-Footer.php"); ?>
            </div>
        </div>
    </div>
    <!--Modal box starts -->  
    <div id="modalCompany" class="modal hide fade">    
        <form name="addCompany" action="adminMgr-Company_user.php?action=update" method="post" enctype="multipart/form-data" onsubmit="loadingText()" onclick="add_zindex();">         
            <div class="modal-header">
                <a href="#close" class="close" data-dismiss="modal" aria-hidden="true">×</a>
                <strong>Update <span id="company_fullname"></span> - <?php echo $data_companys[0]['Company_title'];?> Company</strong>
            </div>
            <div class="modal-body">
                <div class="row-fluid">
                    <div class="span3 modal-label">Company Name :</div>
                    <div class="span8">
                        <div id="company-name">
                            <select id="company-name-select" name="company_id" class="input-xlarge">
                                <?php foreach($data_company_lists as $data_company_list){?>
                                <option value="<?=$data_company_list['Company_ID'];?>" ><?=$data_company_list['Company_title'];?> (<?=$data_company_list['num_employee'];?>)</option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                </div> 
                <br>
                <div class="row-fluid">
                    <div class="span3 modal-label">Work Title :</div>
                    <div class="span8"><input id="work-title" name="title" type="text" class="form-control"/></div>
                </div> 
                <br>
                <div class="row-fluid">
                    <div class="span3 modal-label"></div>
                    <div class="span8 modal-label">
                        <input id="company-progress" name="progress" type="checkbox" value="1" onchange="toggleTo()"> In Progress
                    </div>
                </div>
                <div class="row-fluid">
                    <div class="span3 modal-label">From Month :</div>
                    <div class="span8">
                        <select id="company-fromMonth" name="from_month">
                            <option>Please select</option>
                            <?php for($mm=1;$mm <= 12;$mm++){?>
                            <option value="<?=$mm;?>"><?=$month_name = date("F", mktime(0, 0, 0, $mm, 15));?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="row-fluid">
                    <div class="span3 modal-label">From Year :</div>
                    <div class="span8">
                     <select id="company-fromYear" name="from_year">
                        <?php $year = '1950';
                        for ($years = date("Y"); $years >= $year; $years--) { ?>
                        <option value="<?php echo $years; ?>"><?php echo $years; ?></option>
                        <?php } ?>
                    </select>
                </div>
            </div>
            <div id="box">
                <div class="row-fluid toggle-to">
                    <div class="span3 modal-label">To Month :</div>
                    <div class="span8">
                        <select id="company-toMonth" name="to_month">
                            <option>Please select</option>
                            <?php for($mm=1;$mm <= 12;$mm++){?>
                            <option value="<?=$mm;?>"> <?=$month_name = date("F", mktime(0, 0, 0, $mm, 15));?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="row-fluid toggle-to">
                    <div class="span3 modal-label">To Year :</div>
                    <div class="span8">
                        <select id="company-toYear" name="to_year">
                            <?php $year = '1950';
                            for ($years = date("Y"); $years >= $year; $years--) { ?>
                            <option value="<?php echo $years; ?>"><?php echo $years; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
            </div>
            <div class="row-fluid">
                <div class="span3 modal-label">Description :</div>
                <div class="span9"><textarea id="work-desc" name="desc" class="form-control input-block-level" wrap="soft" placeholder="description" rows="5"></textarea></div>
            </div>  
        </div>
        <div class="modal-footer">
            <div class="btn-group">
                <input type="hidden" name="type" value="<?php echo $O_type;?>"/>
                <input type="hidden" name="companyID" value="<?php echo $data_companys[0]['Company_ID'];?>"/>
                <input type="hidden" id="atework_username" name="username" value=""/>
                <input type="hidden" id="atework_id" name="id" value=""/>
                <a href="#" class="btn" data-dismiss="modal"><i class="icon-circle-arrow-left"></i> Cancel</a>
                <button id="btn-submit" name="submitCompany" type="submit" class="btn btn-success btn-submit"><i class="icon-ok icon-white"></i> Update</button>
            </div>
        </div>
    </form>
    </div><!--Modal box ends -->

    <script type="text/javascript">
        $("#message1").fadeOut(8400);
        function add_zindex(){
            $('.ui-autocomplete').css("z-index", 1055);
        }

        $(document).ready(function () {    
            $("#cek").bind("change", function () {
                if ($('#cek').is(':checked')) {$("#box").slideUp();}
                else {$("#box").slideDown();}
            });
        });

        function copyValue(id){
            var company_fullname = $('#src-company-'+id).attr('data-company-fullname');
            var company_username = $('#src-company-'+id).attr('data-company-username');
            var company_id = $('#src-company-'+id).attr('data-company-id');
            var company_fromMon = $('#src-company-'+id).attr('data-company-from-mon');
            var company_fromYe = $('#src-company-'+id).attr('data-company-from-ye');
            var company_toMon = $('#src-company-'+id).attr('data-company-to-mon');
            var company_toYe = $('#src-company-'+id).attr('data-company-to-ye');
            var work_title = $('#src-company-'+id).attr('data-work-title');
            var work_desc = $('#src-company-'+id).attr('data-work-desc');

            $('#atework_id').val(id);
            $('#atework_username').val(company_username);
            $('#company_fullname').text(company_fullname);
            $('#company_id').val(company_id);
            $('#company-name-select option[value="'+company_id+'"]').attr('selected', 'selected');
            $('#company-fromMonth').val(company_fromMon);
            $('#company-fromYear').val(company_fromYe);
            $('#company-toMonth').val(company_toMon);
            $('#company-toYear').val(company_toYe);
            $('.toggle-to').removeClass('hide');
            //$('#edu-progress').attr("checked", false);
            if(company_toMon == 0 && company_toYe == 0){
                $('#company-progress').attr("checked", true);
                $('.toggle-to').addClass('hide');
            }
            $('#work-title').val(work_title);
            $('#work-desc').text(work_desc);
            //console.log(company_progress);
            $("#company-name-select").select2();
        }

        function toggleTo(){
            $('.toggle-to').toggle();
        }
    </script>
    <script src="js/globalJS.js" type="text/javascript"></script>
</body></html>