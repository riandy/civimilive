<?php
ob_start("ob_gzhandler");
require_once("packages/require.php");
include("packages/check_login.php");//USED BY ALL PAGE BUT index.php
include("controller/controller_edu_user.php");
$page_name = "adminMgr-Edu_user.php";
$edu_page = "edu_ID=$O_id";
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <?php include('packages/head.php');?>
    <!-- calendar -->
    <link rel="stylesheet" type="text/css" href="packages/jQuery-Tags/jquery-ui.css" />
    <link rel="stylesheet" type="text/css" href="packages/jQuery-Tags/jquery.tagsinput.css" />
    <script src="//code.jquery.com/jquery-1.10.2.js"></script>

    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.0/themes/smoothness/jquery-ui.css">
    <script src="//code.jquery.com/ui/1.11.1/jquery-ui.js"></script>
    <script src="js/jquery.select-to-autocomplete.js"></script>
    <!-- select 2 -->
    <link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/css/select2.min.css" rel="stylesheet" />
    <script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/select2.min.js"></script>

    <script>
    $(function() {
        $( ".datepicker" ).datepicker({dateFormat: 'yy-mm-dd' });
        $('.autodrop').selectToAutocomplete();
    });
    </script>
    <style>
    body .modal {
        width: 50%; /* desired relative width */
        left: 25%; /* (100%-width)/2 */
        /* place center */
        margin-left:auto;
        margin-right:auto; 
    }
    .select2 {
        width: 100% !important;
        margin-bottom: 10px !important;
    }
    .select2-selection {
        border-color: #ccc !important;
        min-height: 30px !important;
    }
    .select2-search__field {
        padding: 6px !important;
        min-height: 28px;
        margin-bottom: 0px !important;
    }
    .edu-link{
        display: block;
        text-decoration: none !important;
        padding: 5px 3px;
        border-bottom: solid 1px #ddd;
    }
    .edu-link:hover{
        background-color: #1F9BDE !important;
        color: #FFFFFF !important;
    }
    .edu-link:hover > .edu-name{
        color: #FFFFFF !important;
    }
    .edu-link:hover > .edu-city{
        color: #FFFFFF !important;
    }
    #edu-contain {
        display: none;
        position: absolute;
        top: 30px;
        left: 0;
        width: 96.5%;
        padding: 5px;
        z-index: 10;
        background-color: #EFECEC;
    }
    #edu-input{
        margin-bottom: 0 !important;
    }
    #edu-list{
        max-height:200px;
        overflow-y:auto;
        overflow-x:hidden;
        background-color: #ffffff;
        /*border: solid 1px #ddd;*/
    }
    #edu-load {
        background-color: #EFECEC;
        padding: 5px 5px 0 5px;
        font-size: 12px;
        color: #AFA8A8;
    }
    #edu-layer{
        position: absolute;
        top: 0;
        left: 0;
        display: none;
        background-color: transparent;
        width: 100%;
        height: 100%;
        z-index: 5;
    }
    </style>
</head>
<body class="civMain" onload="startTime()">
    <?php require_once("admin-Header.php");?>

    <!-- start top nav -->
    <?php $curPage='module'; ?>
    <?php require_once("admin-top_nav.php");?>
    <!-- end top nav -->

    <div class="container">
        <div id="menuDashboard" class="row-fluid">
            <div id="rightContent" class="span12">

                <?php
                if($message!=null)
                {
                    echo "<div id='message1' class='alert alert-info'>" . $message . "</div><br />";
                }
                ?>
                <?php if(is_array($data_edus)){ ?>
                <div class="row-fluid">
                    <div class="span12 align-center">
                        <h4 style="margin: 0;">Users who have <br>"<?php echo $data_edus[0]['Edu_schoolName'];?>"<br> in their Education</h4>
                    </div>
                </div>
                <hr/>
                <?php } ?>
                <div class="row-fluid">
                    <div class="span6 align-left">
                        <a href="adminMgr-Edu.php?type=<?php echo $O_type;?>" class="btn btn-default btn-small"><i class="icon-circle-arrow-left"></i> Back</a>
                    </div>
                    <div class="span6 align-right">
                        Total Student / Alumni : <span class="badge badge-info"><?php echo $total_data;?></span><br/>
                        Showing : <span class="badge badge-info"><?php echo (($O_page-1)*20+1)." - ".(count($data_edus)+(($O_page-1)*20));?></span> of <span class="badge badge-info"><?php echo $total_data;?></span> 
                    </div>
                </div>
                <hr/>
                <!-- STARTS LETTER -->
                <div class="row-fluid">
                    <div class="span8">
                        <div class="input-append">
                            <select id="auto_search" class="autodrop"  > 
                                <option value=''>start typing...</option>
                                <?php $ii =0;foreach($data_lists as $data_list){ ?>
                                <option id="edu-<?=$ii;?>" value="<?php echo $data_list['ATedu_ID'];?>"><?php echo $data_list['User_fname']." ".$data_list['User_lname'].", ".$data_list['User_email'];?></option>
                                <?php $ii++;} ?>
                            </select>
                            <!-- <span>please type what you are looking</span> -->
                            <a href="#" onclick="goDetail();" data-toggle="modal" data-target="#modalEducation" class="btn btn-info" style="color: white;">Search</a>
                        </div>
                        <script>
                        function goDetail(){
                            var detail = $('#auto_search').val();
                            if(detail > 0 && isNaN(detail) == false){
                                openedu(detail);
                            }
                        }
                        </script>
                    </div><!-- ENDS LETTER -->
                </div>
                <hr>
                <table class="table table-hover up2">
                    <thead>
                        <tr>
                           <th>#</th>
                           <th>Pic</th>
                           <th>Name</th>
                           <th>E-mail</th>
                           <th>From</th>
                           <th>To</th>
                           <th>Degree</th>
                           <th>Field of Study</th>
                           <th>GPA</th>
                           <th style="text-align:center;width:70px !important;">Action</th>
                       </tr>
                   </thead>
                   <?php
                   if(!is_array($data_edus)){
                    echo "
                    <tbody><td colspan='10'><h5>There is no student / alumni.</h5></td></tbody>";
                }else{?>
                <tbody>
                    <?php $k=1;
                    foreach($data_edus as $data_edu){
                        $onclickDel = "\"if(window.confirm('Are you sure you want to delete {$data_edu['User_fname']} {$data_edu['User_lname']}, {$data_edu['User_email']}?')) location.href='adminMgr-Edu_user.php?action=delete&edu_ID={$data_edu['Edu_ID']}&atedu_ID={$data_edu['ATedu_ID']}&username={$data_edu['User_fname']}';\"";
                        ?>
                    <tr>
                        <td><?php echo ($O_page-1)*20+$k;?></td>
                        <td id="src-edu-<?=$data_lists[$k-1]['ATedu_ID'];?>" data-edu-username="<?=$data_lists[$k-1]['User_fname']." ".$data_lists[$k-1]['User_lname'];?>" data-edu-id="<?=$data_lists[$k-1]['Edu_ID'];?>" data-edu-from-mon="<?=$data_lists[$k-1]['ATedu_fromMonth'];?>" data-edu-from-ye="<?=$data_lists[$k-1]['ATedu_fromYear'];?>" data-edu-to-mon="<?=$data_lists[$k-1]['ATedu_toMonth'];?>" data-edu-to-ye="<?=$data_lists[$k-1]['ATedu_toYear'];?>" data-edu-progress="<?=$data_lists[$k-1]['ATedu_status'];?>" data-edu-field="<?=$data_lists[$k-1]['ATedu_major'];?>" data-edu-degree="<?=$data_lists[$k-1]['ATedu_degree'];?>" data-edu-gpa="<?=$data_lists[$k-1]['ATedu_GPA'];?>" data-edu-desc="<?=$data_lists[$k-1]['ATedu_desc'];?>">
                            <a class="proPhoto" href="<?php echo check_image_url($data_edu['User_pic'], 'user');?>"><img class="img-circle" src="<?php echo check_image_url($data_edu['User_pic_thmb'], 'user');?>" /></a>
                        </td>
                        <td><?php echo $data_edu['User_fname']." ".$data_edu['User_lname'];?></td>
                        <td><?php echo $data_edu['User_email'];?></td>
                        <td><?php echo $data_edu['ATedu_fromMonth'].", ".$data_edu['ATedu_fromYear'];?></td>
                        <?php if($data_edu['ATedu_toMonth'] == 0 && $data_edu['ATedu_toYear'] == 0){$to = "In Progress";}
                        else{$to = $data_edu['ATedu_toMonth'].", ".$data_edu['ATedu_toYear'];}?>
                        <td><?php echo $to;?></td>
                        <td><?php echo $data_edu['ATedu_degree'];?></td>
                        <td><?php echo $data_edu['ATedu_major'];?></td>
                        <td><?php echo $data_edu['ATedu_GPA'];?></td>
                        <td style="text-align: center;">
                            <div class="btn-group btn-group-horizontal">
                                <a href="#" onclick="copyValue(<?php echo $data_edu['ATedu_ID'];?>);" type="button" class="btn btn-small" data-toggle="modal" data-target="#modalEducation"><i class="icon-edit"></i> Edit</a>
                                <a href="#" <? echo "onclick=$onclickDel";?> type="button" class="btn btn-danger btn-small" style="color: white;" ><i class="icon-trash icon-white"></i> Delete</a>
                                <a target="_blank" href="http://www.civimi.com/beta/profile/<?php echo $data_edu['User_username'];?>.cvm" type="button" class="btn btn-info btn-small" style="color: white;"><i class="icon-user icon-white"></i> view profile</a>
                            </div>
                        </td>
                    </tr>
                    <?php $k++; }?>
                </tbody>
                <? }?>
            </table>
            <br/>
            <!-- START PAGINATION EDU USER !-->
            <div class="pagination align-center">
              <ul class="pagination pagination-sm">
                <?php
                $batch = getBatch($O_page);
                if($batch < 1){$batch = 1;}
                $prevLimit = 1 +(10*($batch-1));
                $nextLimit = 10 * $batch;

                if($nextLimit > $total_page){$nextLimit = $total_page;}
                if ($total_page > 1 && $O_page > 1) {
                    echo "<li><a href='".$page_name."?page=1&".$edu_page."' class='pagination-slide'>&lt;&lt; First</a></li>";
                }
                if($batch > 1 && $O_page > 10){
                    echo "<li><a href='".$page_name."?page=".($prevLimit-1)."&".$edu_page."' class='pagination-slide'>&lt; Previous 10</a></li>";
                }
                for($mon = $prevLimit; $mon <= $nextLimit;$mon++){?>
                <li class="<?php if($mon == $O_page){echo 'active';}?>"><span class="pagination-theme"><a href="<?php echo $page_name."?page=".$mon."&".$edu_page;?>" ><?php echo $mon;?></a></span></li>
                <?php if($mon == $nextLimit && $mon < $total_page){if($mon >= $total_page){ $mon -=1;}
                echo "<li><a href='".$page_name."?page=".($mon+1)."&".$edu_page."' class='pagination-slide'>Next 10 &gt;</a></li>";
            }                               }
            if ($total_page > 1 &&  ($O_page != $total_page)) {
                echo "<li><a href='".$page_name."?page=".$total_page."&".$edu_page."' class='pagination-slide'>Last &gt;&gt;</a></li>";
            }
            ?>
            </ul>
            </div>
            <!-- END PAGINATION EDU USER !-->
        </div>
    </div>
    <div class="row-fluid">
        <div class="span6">
            <?php require_once("admin-Footer.php"); ?>
        </div>
    </div>
</div>
<!--Modal box starts -->  
<div id="modalEducation" class="modal hide fade">    
    <form name="addEducation" action="adminMgr-Edu_user.php?action=update" method="post" enctype="multipart/form-data" onsubmit="loadingText()" onclick="add_zindex();">         
        <div id="edu-layer"></div>
        <div class="modal-header">
            <a href="#close" class="close" data-dismiss="modal" aria-hidden="true">×</a>
            <strong>Update <span id="fullname"></span> - <?php echo $data_edus[0]['Edu_schoolName'];?> Education</strong>
        </div>
        <div class="modal-body">
            <div class="row-fluid">
                <div class="span4 align-right modal-label">School Name :</div>
                <div class="span6">
                    <div id="edu-school" style="position:relative">
                        <input id="edu-value" value="" name="edu_id" type="hidden" class="input-block-level" placeholder="value id for save">
                        <input id="edu-text" value="" name="edu_text" style="margin:0;" onfocus="selectEdu();" type="text" class="input-block-level" placeholder="Insert School/University">
                        <div id="edu-contain">
                            <input id="edu-input" autocomplete="off" value="<?php echo $data_edus[0]['Edu_schoolName'];?>" name="edu_input" type="text" class="input-block-level" oninput="get_univ();">
                            <div id="edu-list"></div>
                        </div>
                    </div>
                </div>
            </div> 
                <!-- <div class="row-fluid">
                    <div class="span4 modal-label">Country</div>
                    <div class="span8"><div id="edu-country"></div></div>
                </div> 
                <div class="row-fluid">
                    <div class="span4 modal-label">City</div>
                    <div class="span8"><div id="edu-city"></div></div>
                </div>  -->
                
            <div class="row-fluid up1">
                <div class="span4 modal-label"></div>
                <div class="span8 modal-label">
                    <input id="edu-progress" name="progress" type="checkbox" value="1" onchange="toggleTo()"> In Progress
                </div>
            </div>
            <div class="row-fluid">
                <div class="span4 align-right modal-label">From Month :</div>
                <div class="span6">
                    <select id="edu-fromMonth" name="from_month" class="text input-block-level">
                        <option>Please select</option>
                        <?php for($mm=1;$mm <= 12;$mm++){?>
                        <option value="<?=$mm;?>"><?=$month_name = date("F", mktime(0, 0, 0, $mm, 15));?></option>
                        <?php } ?>
                    </select>
                </div>
            </div>
            <div class="row-fluid">
                <div class="span4 align-right modal-label">From Year :</div>
                <div class="span6">
                 <select id="edu-fromYear" name="from_year" class="text input-block-level">
                    <?php $year = '1950';
                    for ($years = date("Y"); $years >= $year; $years--) { ?>
                    <option value="<?php echo $years; ?>"><?php echo $years; ?></option>
                    <?php } ?>
                </select>
            </div>
        </div>
        <div id="box">
            <div class="row-fluid toggle-to">
                <div class="span4 align-right modal-label">To Month :</div>
                <div class="span6">
                    <select id="edu-toMonth" name="to_month" class="text input-block-level">
                        <option>Please select</option>
                        <?php for($mm=1;$mm <= 12;$mm++){?>
                        <option value="<?=$mm;?>"> <?=$month_name = date("F", mktime(0, 0, 0, $mm, 15));?></option>
                        <?php } ?>
                    </select>
                </div>
            </div>
            <div class="row-fluid toggle-to">
                <div class="span4 align-right modal-label">To Year :</div>
                <div class="span6">
                    <select id="edu-toYear" name="to_year" class="text input-block-level">
                        <?php $year = '1950';
                        for ($years = date("Y"); $years >= $year; $years--) { ?>
                        <option value="<?php echo $years; ?>"><?php echo $years; ?></option>
                        <?php } ?>
                    </select>
                </div>
            </div>
        </div>
        <div class="row-fluid">
            <div class="span4 align-right modal-label">Degree :</div>
            <div class="span6"><input id="edu-degree" name="degree" type="text" class="input-block-level"/></div>
        </div> 
        <div class="row-fluid">
            <div class="span4 align-right modal-label">Field of Study :</div>
            <div class="span6"><input id="edu-field" name="field" type="text" class="input-block-level"/></div>
        </div> 
        <div class="row-fluid">
            <div class="span4 align-right modal-label">GPA :</div>
            <div class="span6"><input id="edu-gpa" name="gpa" type="text" class="text input-block-level"/></div>
        </div> 
        <div class="row-fluid">
            <div class="span4 align-right modal-label">Description :</div>
            <div class="span6"><textarea id="edu-desc" name="desc" class="text input-block-level"></textarea></div>
        </div>  
    </div>
    <div class="modal-footer">
        <div id="edu-hidden"></div>
        <div class="btn-group">
            <input type="hidden" name="eduID" value="<?php echo $data_edus[0]['Edu_ID'];?>"/>
            <input type="hidden" id="atedu_username" name="username" value=""/>
            <input type="hidden" id="atedu_id" name="id" value=""/>
            <input type="hidden" name="type" value="<?php echo $O_type;?>"/>
            <a href="#" class="btn" data-dismiss="modal"><i class="icon-remove"></i> Cancel</a>
            <button id="btn-submit" name="submitEducation" type="submit" class="btn btn-success btn-submit"><i class="icon-check icon-white"></i> Update</button>
        </div>
    </div>
</form>
</div><!--Modal box ends -->
<div id="date" style="display:none;"></div>
<div id="leftNav" style="display:none;"></div>
<script type="text/javascript">
$("#message1").fadeOut(8400);
function add_zindex(){
    $('.ui-autocomplete').css("z-index", 1055);
}
$(document).ready(function () {    
    $("#cek").bind("change", function () {
        if ($('#cek').is(':checked')) {$("#box").slideUp();}
        else {$("#box").slideDown();}
    });
});
function copyValue(id){
    var edu_username = $('#src-edu-'+id).attr('data-edu-username');
    var edu_id = "<?php echo $data_edus[0]['Edu_ID'];?>";
    var edu_name = "<?php echo $data_edus[0]['Edu_schoolName'];?>";
    var edu_fromMon = $('#src-edu-'+id).attr('data-edu-from-mon');
    var edu_fromYe = $('#src-edu-'+id).attr('data-edu-from-ye');
    var edu_toMon = $('#src-edu-'+id).attr('data-edu-to-mon');
    var edu_toYe = $('#src-edu-'+id).attr('data-edu-to-ye');
    var edu_progress = $('#src-edu-'+id).attr('data-edu-progress');
    var edu_field = $('#src-edu-'+id).attr('data-edu-field');
    var edu_degree = $('#src-edu-'+id).attr('data-edu-degree');
    var edu_gpa = $('#src-edu-'+id).attr('data-edu-gpa');
    var edu_desc = $('#src-edu-'+id).attr('data-edu-desc');
    $('#edu-value').val(edu_id);
    $('#edu-text').val(edu_name);
    $('#fullname').text(edu_username);
    $('#atedu_username').val(edu_username);
    $('#atedu_id').val(id);
    $('#edu-fromMonth').val(edu_fromMon);
    $('#edu-fromYear').val(edu_fromYe);
    $('#edu-toMonth').val(edu_toMon);
    $('#edu-toYear').val(edu_toYe);
    $('.toggle-to').removeClass('hide');
    $('#edu-progress').attr("checked", false);
    if(edu_progress == 1){
        $('#edu-progress').attr("checked", true);
        $('.toggle-to').addClass('hide');
    }
    $('#edu-field').val(edu_field);
    $('#edu-degree').val(edu_degree);
    $('#edu-gpa').val(edu_gpa);
    $('#edu-desc').text(edu_desc);
}
function toggleTo(){
    $('.toggle-to').toggle();
}
function selectEdu(){
    $("#edu-contain").show();
    $("#edu-layer").show();
    $("#edu-contain").addClass('in');
    $("#edu-input").focus();
}
function get_univ(){
  var q = $("#edu-input").val();
  $("#edu-list").html("<div id='edu-load'>Searching...</div>")
  var url = "http://www.civimi.com/beta/api/api_education.php?action=get_data_all&q="+q;                            
  $.ajax({url: url,success:function(result){
    $("#edu-list").html(replace_univ(result.data));
  }}); 
}
function replace_univ(datas){
  var resultHTML='';
  if(datas != null){
    var obj = datas;    
    for(var i=0;i < obj.length;i++){
      if(obj[i].Edu_id != '' && obj[i].Edu_schoolName != ''){
        resultHTML += "<a href='#' class='edu-link' id='univ-"+obj[i].Edu_id+"' onclick='selectUniv("+obj[i].Edu_id+")' data-sc_title='"+obj[i].Edu_schoolName+"'>";
        resultHTML += "<div class='edu-name' style='font-weight:bold;font-size:13px;color:#444444;'>"+obj[i].Edu_schoolName+"</div>";
        resultHTML += "<div class='edu-city' style='font-size:13px;color:#808080;'>"+obj[i].Edu_schoolCity+", "+obj[i].Edu_schoolCountry+"</div>";
        resultHTML += "</a>";
      }
    }
  }
  return resultHTML;
}
function selectUniv(param){
  var title = $('#univ-'+param).data('sc_title');

    $('#edu-value').val(param);
    $('#edu-text').val(title);
    if($("#edu-contain").hasClass('in')){
        $('#edu-contain').hide();
        $('#edu-layer').hide();
    }
}
$('#edu-layer').click(function() {
    if($("#edu-contain").hasClass('in')){
        $('#edu-contain').hide();
        $('#edu-layer').hide();
    }
});
</script>
<script src="js/globalJS.js" type="text/javascript"></script>
</body>
</html>