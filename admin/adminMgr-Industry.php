<?php
ob_start("ob_gzhandler");
require_once("packages/require.php");
include("packages/check_login.php");//USED BY ALL PAGE BUT index.php
include("controller/controller_industry.php");
$page_name = "adminMgr-Industry.php";
$query_page = "letter=$letter";
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <?php include('packages/head.php');?>
    <!-- calendar -->
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.0/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.1/jquery-ui.js"></script>
<script src="js/jquery.select-to-autocomplete.js"></script>
    <script>
  $(function() {
    $( ".datepicker" ).datepicker({dateFormat: 'yy-mm-dd' });
    $('.autodrop').selectToAutocomplete();
  });
  </script>
    <style>
        body .modal {
            width: 50%; /* desired relative width */
            left: 25%; /* (100%-width)/2 */
            /* place center */
            margin-left:auto;
            margin-right:auto; 
        }
    </style>
</head>
<body class="civMain" onload="startTime()">
    <?php require_once("admin-Header.php");?>
    <div class="container">
        <div id="breadcrumb-parent" class="row-fluid">
            <div id="breadcrumb" class="span6">
                <a id="dashboard-link" href="#dashboard"><i class="icon-th-large"></i> Dashboard</a>
                : <a href='adminMgr-Industry.php'>Industry Management</a>
            </div>
            <div id="notifications" class="span6 align-right">
                <span>Welcome on <span id="date"></span></span>
            </div>
        </div>
        <div id="menuDashboard" class="row-fluid">
            
            <div id="leftNav-Parent" class="span3">
                <?php $curPage='industry'; ?>
                <?php require_once("admin-Sidebar.php");?>
            </div>
            
            <div id="rightContent" class="span9">
                
                <?php
                if($message!=null)
                {
                    echo "<div id='message1' class='alert alert-info'>" . $message . "</div><br />";
                }
                ?>
                <div class="row-fluid">
                    <div class="span6 align-left">
                        <h4 style="margin: 0;">Industry Management</h4>
                    </div>
                    <div class="span6 align-right">
                        <button class="btn btn-info" type="button" data-toggle="modal" data-target="#modalIndustry"><i class="icon-plus icon-white"></i> Industry</button>
                    </div>
                </div>
                <hr/>
                <?php /*
                <form action="adminMgr-Campaign.php?action=filter" method="post">
                <div class="row-fluid">
                    <div class="span3">
                    <div style="font-weight:bold;text-align:left;">Filter</div>
                        <select name="filter_publish" class="input-block-level" required="required">
                            <option value="">---Choose Filter---</option>
                            <option <?php if($O_publish == "Publish"){ echo "selected=selected"; }?> value="Publish">Publish</option>
                            <option <?php if($O_publish == "Draft"){ echo "selected=selected"; }?> value="Draft">Draft</option>
                            <option <?php if($O_publish == "Expired"){ echo "selected=selected"; }?> value="Expired">Expired</option>
                        </select>
                    </div>
                    <div class="span3">
                    <div style="font-weight:bold;text-align:left;">From</div>
                        <input type="text" class="span10 datepicker" name="from_date" placeholder="from date" required="required" value="<?php echo $O_from;?>">
                        
                    </div>
                    <div class="span3">
                    <div style="font-weight:bold;text-align:left;">To</div>
                        <input type="text" class="span10 datepicker" name="to_date" placeholder="to date" required="required" value="<?php echo $O_to;?>">
                        
                    </div>
                    <div class="span3">
                    <br/>
                        <button type="submit" class="btn"><i class="icon-circle-arrow-left"></i> Filter</button>
                    </div>
                </div>
                </form>
                
                <div class="row-fluid">
                    <div class="span12 align-right">
                        Total Industry : <span class="badge badge-info"><?php echo $total_data_all;?></span><br/>
                        Showing : <span class="badge badge-info"><?php echo $total_start." - ".$total_data;?></span> of <span class="badge badge-info"><?php echo $total_data_all;?></span> 
                    </div>
                </div>
                */ ?>
                <div class="row-fluid">
                    <div class="span8">
                        <div class="input-append">
                        <select id="auto_search" class="autodrop"  > 
                        <option value=''>start typing...</option>
                            <?php foreach($data_lists as $data_list){ ?>
                                <option value="<?php echo $data_list['Industry_ID'];?>"><?php echo $data_list['Industry_title'];?></option>
                                <?php } ?>
                        </select>
                        <!-- <span>please type what you are looking</span> -->
                        <a onclick="goDetail();" class="btn btn-info">Search</a>
                    </div>
                        <script>
                        function goDetail(){
                            var detail = $('#auto_search').val();
                            if(detail > 0 && isNaN(detail) == false){
                                location.href = "<?=$admin['path-industry_detail'];?>"+detail;
                            }
                        }
                        </script>
                    </div>
                    
                    <div class="span4 align-right">
                        Total Data starts with <span class="badge badge-important"><?php if($letter == ''){ echo "A-Z"; }else{ echo strtoupper($letter);}?></span> : <span class="badge badge-info"><?php echo $total_data;?></span><br/>
                        Showing : <span class="badge badge-info"><?php echo (($O_page-1)*20+1)." - ".(count($data_industrys)+(($O_page-1)*20));?></span> of <span class="badge badge-info"><?php echo $total_data;?></span> 
                    </div>
                </div>
                <hr/>
                <!-- STARTS LETTER -->
                <div class="row-fluid">
                    <div class="span-12">
                        <?php for ($i = 'a', $j = 1; $j <= 26; $i++, $j++) {
                                $letters[$j] = $i;
                    ?>
                    <a href="<?php echo "adminMgr-Industry.php?page=1&letter=$letters[$j]";?>" class="btn-mini <?php  if($letter == $letters[$j] ){echo 'active-alphabet';} ?>" ><b><?php echo strtoupper($letters[$j]);?></b></a>&nbsp;
                    <?php } ?>
                    </div>
                </div><!-- ENDS LETTER -->

                <table class="table table-bordered table-hover up2">
                <thead>
                    <tr>
			            <th>#</th>
                        <th>Title</th>
                        <th>Content</th>
                        <th>Publish</th>
                        <th style="text-align:center;width:70px !important;">Action</th>
                    </tr>
                </thead>
                <?php
                if(!is_array($data_industrys)){
                    echo "
                    <tbody><td colspan='8'><h5>There is no data.</h5></td></tbody>";
                }else{?>
                <tbody>
                <?php $k=1;
                    foreach($data_industrys as $data_industry){
                    if($k >= 1 && $total_data >= 1){
                        $onclickDel = "\"if(window.confirm('Are you sure you want to delete {$data_industry['Industry_title']}?')) location.href='adminMgr-Industry.php?action=delete&industry_ID={$data_industry['Industry_ID']}';\"";
                    }
                    ?>
                    <tr class="<?php if($data_industry['Industry_publish'] == 'Not Publish'){echo 'error';}?>">
	                    <td><?php echo ($O_page-1)*20+$k;?></td>
                        <td><?php echo $data_industry['Industry_title'];?></td>
                        <td><?php echo charLength($data_industry['Industry_content'], 30);?></td>
                        <td><?php echo $data_industry['Industry_publish'];?></td>
                        <td style="text-align: center;">
                            <div class="btn-group-vertical">
                                <div class="btn-group">
                                  <a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
                                    <i class=" icon-cog"></i>
                                  </a>
                                  <ul class="dropdown-menu align-left">
                                    <li><a href="#" <? echo "onclick=$onclickDel";?><?php if($k == 1 && $total_data == 1){echo " disabled";}?>><i class=" icon-remove"></i> Delete</a></li>
                                    <li><a href="adminMgr-Industry_detail.php?industry_ID=<?php echo $data_industry['Industry_ID'];?>"><i class=" icon-edit"></i> Edit</a></li>
                                  </ul>
                                </div>
                            </div>
                        </td>
                    </tr>
                <?php $k++; }?>
                </tbody>
                <? }?>
                </table>
                <br/>
                <?php include("part-pagination.php");?>
            </div>
        </div>
        <div class="row-fluid">
            <div class="span6">
                <?php require_once("admin-Footer.php"); ?>
            </div>
        </div>
    </div>
    <!--Modal box starts -->  
    <div id="modalIndustry" class="modal hide fade">    
        <form name="addIndustry" action="adminMgr-Industry.php?action=insert" method="post" enctype="multipart/form-data" onsubmit="loadingText()">         
            <div class="modal-header">
                <a href="#close" class="close" data-dismiss="modal" aria-hidden="true">×</a>
                <strong>Create New Industry</strong>
            </div>
            <div class="modal-body">
                <div class="row-fluid">
                    <div class="span4 modal-label"><strong>Industry Title&nbsp;*</strong>:</div>
                    <div class="span8"><input name="title" type="text" class="text input-block-level" placeholder="Industry Name" required="required" /></div>
                </div>
                <div class="row-fluid">
                    <div class="span4 modal-label">Indsutry Description :</div>
                    <div class="span8"><textarea name="content" class="input-block-level" wrap="soft" placeholder="description" rows="5" ></textarea></div>
                </div>
                <div class="row-fluid up1">
                    <div class="span4 modal-label">Industry Publish :</div>
                    <div class="span8">
                        <select name="publish" class="text">
                            <option value="Publish">Publish</option>
                            <option value="Not Publish">Not Publish</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="btn-group">
                    <a href="#" class="btn" data-dismiss="modal"><i class="icon-circle-arrow-left"></i> Close</a>
                     <button id="btn-submit" name="submitIndustry" type="submit" class="btn btn-success btn-submit"><i class="icon-ok icon-white"></i> Create</button>
                </div>
            </div>
        </form>
    </div><!--Modal box ends -->
    <script type="text/javascript">
        $("#message1").fadeOut(8400);
    </script>
    <script src="js/globalJS.js" type="text/javascript"></script>
</body></html>