<?php
ob_start("ob_gzhandler");
require_once("packages/require.php");
include("packages/check_login.php");//USED BY ALL PAGE BUT index.php
include("controller/controller_certificate_detail.php");
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <?php include('packages/head.php');?>
    <!-- calendar -->

    <link rel="stylesheet" type="text/css" href="packages/jQuery-Tags/jquery-ui.css" />
    <link rel="stylesheet" type="text/css" href="packages/jQuery-Tags/jquery.tagsinput.css" />
    <script src="//code.jquery.com/jquery-1.10.2.js"></script>
    <script>
  $(function() {
    $( ".datepicker" ).datepicker({dateFormat: 'yy-mm-dd' });
  });
  </script>
    <style>
        body .modal {
            width: 50%; /* desired relative width */
            left: 25%; /* (100%-width)/2 */
            /* place center */
            margin-left:auto;
            margin-right:auto; 
        }
    </style>
</head>
<body class="civMain" onload="startTime()">
    <?php require_once("admin-Header.php");?>

    <!-- start top nav -->
    <?php $curPage='module'; ?>
    <?php require_once("admin-top_nav.php");?>
    <!-- end top nav -->

    <div class="container">
        <div id="menuDashboard" class="row-fluid">
            <div id="rightContent" class="span12">
                
                <?php
                if($message!=null)
                {
                    echo "<div class='alert alert-info'>" . $message . "</div><br />";
                }
                ?>
                <div class="row-fluid">
                    <div class="span3">
                        <a href="adminMgr-Certificate.php" class="btn btn-default btn-small"><i class="icon-circle-arrow-left"></i> Back</a>
                    </div>
                    <div class="span6">
                        <h4 style="margin: 0;text-align:center;">Certificate Edit <br>(<?php echo $data_certs[0]['Cert_title'];?>)</h4>
                    </div>
                </div>
                <hr/>
                <?php $k=0;
                foreach($data_certs as $data_cert){?>
                <form name="addCertificate" action="adminMgr-Certificate_detail.php?action=update" method="post" enctype="multipart/form-data" onsubmit="loadingText()">
                    <div class="row-fluid up2">
                        <div class="span4 align-right modal-label"><strong>Certificate Title&nbsp;*</strong>:</div>
                        <div class="span5"><input name="title" type="text" class="text input-block-level" placeholder="Certificate Title" value="<?php echo $data_cert['Cert_title'];?>" required></div>
                    </div>
                    <div class="row-fluid">
                        <div class="span4 align-right modal-label"><strong>Certificate URL&nbsp;*</strong>:</div>
                        <div class="span5"><input name="url" type="url" class="text input-block-level" placeholder="http://www.example.com" value="<?php echo $data_cert['Cert_url'];?>" required></div>
                    </div>
                    <div class="row-fluid up1">
                        <div class="span4 align-right modal-label">Certificate Image :</div>
                        <div class="span5">
                            <a class="proPhoto" href="<?php echo check_image_url($data_cert['Cert_img'], 'certificate');?>">
                                <img class="img-circle" src="<?php echo check_image_url($data_cert['Cert_img_thmb'], 'certificate');?>" class="eduAdminImg" />
                            </a>
                        </div>
                    </div>
                    <div class="row-fluid up1">
                        <div class="span5 offset4">
                            <br />
                            <input name="image" type="file" class="text" maxlength="128" placeholder="certificate image" />
                            <br />
                        </div>
                    </div>
                    <div class="row-fluid">
                        <div class="offset4 span5"><small>image format has to be jpg, jpeg, gif, png.</small></div>
                    </div>
                    <div class="row-fluid up1">
                        <div class="span4 align-right modal-label">Certificate Publish :</div>
                        <div class="span5">
                            <select name="publish" class="text">
                                <option <?php if($data_cert['Cert_publish'] == "Publish"){ echo "selected=selected"; }?> value="Publish">Publish</option>
                                <option <?php if($data_cert['Cert_publish'] == "Not Publish"){ echo "selected=selected"; }?> value="Not Publish">Not Publish</option>
                            </select>
                        </div>
                    </div>
                    <div class="row-fluid">
                        <div id="SubStatus" class="span12 align-center"></div>
                    </div>
                    <div class="row-fluid">
                    <div class="span12">
                    <div class="well" align="center">
                        <input name="id" type="hidden" value="<?php echo $data_cert['Cert_ID'];?>"/>
                        <div class="btn-group" align="center">
                            <?php $onclickDel = "\"if(window.confirm('Are you sure you want to delete {$data_cert['Cert_title']}?')) location.href='adminMgr-Certificate.php?action=delete&cert_ID={$data_cert['Cert_ID']}&cert_Title={$data_cert['Cert_title']}';\""; ?>
                            <a href="#" <? echo "onclick=$onclickDel";?> class="btn btn-danger" style="color: white;"><i class="icon-trash icon-white"></i> Delete Certificate</a>
                            <button id="btn-submit" name="saveCertificate" type="submit" class="btn btn-success"><i class="icon-check icon-white"></i> Save Certificate</button>
                        </div>
                    </div>
                    </div>
                    </div>
                </form>
                <?php $k++; }?>
            </div>
        </div>
        <div class="row-fluid">
            <div class="span6">
                <?php require_once("admin-Footer.php"); ?>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $("#message1").fadeOut(8400);
    </script>
    <script src="js/globalJS.js" type="text/javascript"></script>
</body></html>