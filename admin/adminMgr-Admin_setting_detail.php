<?php
ob_start("ob_gzhandler");
require_once("packages/require.php");
include("packages/check_login.php");//USED BY ALL PAGE BUT index.php
include("controller/controller_admin_setting_detail.php");
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php include('packages/head.php');?>
</head>
<body class="civMain" onload="startTime()">
    <?php require_once("admin-Header.php"); ?>

    <!-- start top nav -->
    <?php $curPage='setting'; ?>
    <?php require_once("admin-top_nav.php");?>
    <!-- end top nav -->

    <div class="container">
        <div id="menuDashboard" class="row-fluid">
            <div id="rightContent" class="span12">
                <?php
                if($saveStatus!=null)
                {
                    echo "<div class='alert alert-info'>" . $saveStatus . "</div><br />";
                }
                ?>
                <div class="row-fluid">
                    <div class="span3">
                        <a href="adminMgr-Admin_setting.php" class="btn btn-default btn-small"><i class="icon-circle-arrow-left"></i> Back</a>
                    </div>
                    <div class="span6">
                        <h4 style="margin: 0;text-align:center;">Admin Setting Management <br>(<?php echo $data_users[0]['Admin_username'];?>)</h4>
                    </div>
                </div>
                <hr />
                    <form name="addSetting" action="<?php $_SERVER['PHP_SELF'];?>?action=process" method="post" onsubmit="loadingText()">
                        <div class="row-fluid up2">
                            <div class="span5 align-right"><strong>Admin Username&nbsp;*</strong>:</div>
                            <div class="span3">
                                <select name="admin_id" class="text input-block-level" required="required">
                                    <option value="">Select Admin</option>
                                    <?php foreach($data_admins as $data_admin){ ?>
                                    <option <?php if($data_users[0]['Admin_ID'] == $data_admin['id']){ echo "selected=selected"; }?> value="<?php echo $data_admin['id'];?>"><?php echo $data_admin['username'];?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="row-fluid">
                            <div class="span5 align-right"><strong>Admin Role&nbsp;*</strong>:</div>
                            <div class="span3">
                                <select name="role_id" class="text input-block-level" required="required">
                                    <option value="">Select Role</option>
                                    <?php foreach($data_roles as $data_role){ ?>
                                    <option <?php if($data_users[0]['Role_ID'] == $data_role['Role_ID']){ echo "selected=selected"; }?> value="<?php echo $data_role['Role_ID'];?>"><?php echo $data_role['Role_name'];?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="row-fluid">
                            <div class="span5 align-right"><strong>Education Show&nbsp;*</strong>:</div>
                            <div class="span7">
                                <input name="edu" type="radio" class="text" required="required" <?php if($data_users[0]['Education_show'] == "Yes"){echo "checked";}?> value="Yes" />Yes&nbsp;&nbsp;&nbsp;
                                <input name="edu" type="radio" class="text" required="required" <?php if($data_users[0]['Education_show'] == "No"){echo "checked";}?> value="No" />No
                            </div>
                        </div>
                        <div class="row-fluid up1">
                            <div class="span5 align-right"><strong>Company Show&nbsp;*</strong>:</div>
                            <div class="span7">
                                <input name="comp" type="radio" class="text" required="required" <?php if($data_users[0]['Company_show'] == "Yes"){echo "checked";}?> value="Yes" />Yes&nbsp;&nbsp;&nbsp;
                                <input name="comp" type="radio" class="text" required="required" <?php if($data_users[0]['Company_show'] == "No"){echo "checked";}?> value="No" />No
                            </div>
                        </div>
                        <div class="row-fluid up1">
                            <div class="span5 align-right"><strong>Industry Show&nbsp;*</strong>:</div>
                            <div class="span7">
                                <input name="ind" type="radio" class="text" required="required" <?php if($data_users[0]['Industry_show'] == "Yes"){echo "checked";}?> value="Yes" />Yes&nbsp;&nbsp;&nbsp;
                                <input name="ind" type="radio" class="text" required="required" <?php if($data_users[0]['Industry_show'] == "No"){echo "checked";}?> value="No" />No
                            </div>
                        </div>
                        <div class="row-fluid up1">
                            <div class="span5 align-right"><strong>Field Show&nbsp;*</strong>:</div>
                            <div class="span7">
                                <input name="field" type="radio" class="text" required="required" <?php if($data_users[0]['Field_show'] == "Yes"){echo "checked";}?> value="Yes" />Yes&nbsp;&nbsp;&nbsp;
                                <input name="field" type="radio" class="text" required="required" <?php if($data_users[0]['Field_show'] == "No"){echo "checked";}?> value="No" />No
                            </div>
                        </div>
                        <div class="row-fluid up1">
                            <div class="span5 align-right"><strong>Jobs Show&nbsp;*</strong>:</div>
                            <div class="span7">
                                <input name="jobs" type="radio" class="text" required="required" <?php if($data_users[0]['Jobs_show'] == "Yes"){echo "checked";}?> value="Yes" />Yes&nbsp;&nbsp;&nbsp;
                                <input name="jobs" type="radio" class="text" required="required" <?php if($data_users[0]['Jobs_show'] == "No"){echo "checked";}?> value="No" />No
                            </div>
                        </div>
                        <div class="row-fluid up1">
                            <div class="span5 align-right"><strong>User Show&nbsp;*</strong>:</div>
                            <div class="span7">
                                <input name="user" type="radio" class="text" required="required" <?php if($data_users[0]['User_show'] == "Yes"){echo "checked";}?> value="Yes" />Yes&nbsp;&nbsp;&nbsp;
                                <input name="user" type="radio" class="text" required="required" <?php if($data_users[0]['User_show'] == "No"){echo "checked";}?> value="No" />No
                            </div>
                        </div>
                        <div class="row-fluid up1">
                            <div class="span5 align-right"><strong>Admin Show&nbsp;*</strong>:</div>
                            <div class="span7">
                                <input name="admin" type="radio" class="text" required="required" <?php if($data_users[0]['Admin_show'] == "Yes"){echo "checked";}?> value="Yes" />Yes&nbsp;&nbsp;&nbsp;
                                <input name="admin" type="radio" class="text" required="required" <?php if($data_users[0]['Admin_show'] == "No"){echo "checked";}?> value="No" />No
                            </div>
                        </div>
                        <div class="row-fluid up1">
                            <div class="span5 align-right"><strong>Certificate Show&nbsp;*</strong>:</div>
                            <div class="span7">
                                <input name="cert" type="radio" class="text" required="required" <?php if($data_users[0]['Certificate_show'] == "Yes"){echo "checked";}?> value="Yes" />Yes&nbsp;&nbsp;&nbsp;
                                <input name="cert" type="radio" class="text" required="required" <?php if($data_users[0]['Certificate_show'] == "No"){echo "checked";}?> value="No" />No
                            </div>
                        </div>
                        <div class="row-fluid up1">
                            <div class="span5 align-right"><strong>Skill Show&nbsp;*</strong>:</div>
                            <div class="span7">
                                <input name="skill" type="radio" class="text" required="required" <?php if($data_users[0]['Skill_show'] == "Yes"){echo "checked";}?> value="Yes" />Yes&nbsp;&nbsp;&nbsp;
                                <input name="skill" type="radio" class="text" required="required" <?php if($data_users[0]['Skill_show'] == "No"){echo "checked";}?> value="No" />No
                            </div>
                        </div>
                        <div class="row-fluid up1">
                            <div class="span5 align-right"><strong>Country Show&nbsp;*</strong>:</div>
                            <div class="span7">
                                <input name="country" type="radio" class="text" required="required" <?php if($data_users[0]['Country_show'] == "Yes"){echo "checked";}?> value="Yes" />Yes&nbsp;&nbsp;&nbsp;
                                <input name="country" type="radio" class="text" required="required" <?php if($data_users[0]['Country_show'] == "No"){echo "checked";}?> value="No" />No
                            </div>
                        </div>
                        <div class="row-fluid up1">
                            <div class="span5 align-right"><strong>City Show&nbsp;*</strong>:</div>
                            <div class="span7">
                                <input name="city" type="radio" class="text" required="required" <?php if($data_users[0]['City_show'] == "Yes"){echo "checked";}?> value="Yes" />Yes&nbsp;&nbsp;&nbsp;
                                <input name="city" type="radio" class="text" required="required" <?php if($data_users[0]['City_show'] == "No"){echo "checked";}?> value="No" />No
                            </div>
                        </div>
                        <div class="row-fluid up1">
                            <div class="span5 align-right"><strong>Level Show&nbsp;*</strong>:</div>
                            <div class="span7">
                                <input name="level" type="radio" class="text" required="required" <?php if($data_users[0]['Level_show'] == "Yes"){echo "checked";}?> value="Yes" />Yes&nbsp;&nbsp;&nbsp;
                                <input name="level" type="radio" class="text" required="required" <?php if($data_users[0]['Level_show'] == "No"){echo "checked";}?> value="No" />No
                            </div>
                        </div>
                        <div class="row-fluid up1">
                            <div class="span5 align-right"><strong>Tag Show&nbsp;*</strong>:</div>
                            <div class="span7">
                                <input name="tag" type="radio" class="text" required="required" <?php if($data_users[0]['Tag_show'] == "Yes"){echo "checked";}?> value="Yes" />Yes&nbsp;&nbsp;&nbsp;
                                <input name="tag" type="radio" class="text" required="required" <?php if($data_users[0]['Tag_show'] == "No"){echo "checked";}?> value="No" />No
                            </div>
                        </div>
                        <div class="row-fluid up1">
                            <div class="span5 align-right"><strong>Setting Show&nbsp;*</strong>:</div>
                            <div class="span7">
                                <input name="setting" type="radio" class="text" required="required" <?php if($data_users[0]['Setting_show'] == "Yes"){echo "checked";}?> value="Yes" />Yes&nbsp;&nbsp;&nbsp;
                                <input name="setting" type="radio" class="text" required="required" <?php if($data_users[0]['Setting_show'] == "No"){echo "checked";}?> value="No" />No
                            </div>
                        </div>
                        <div class="row-fluid">
                            <div class="span8 offset4">
                                <div id="SubStatus"></div>
                            </div>
                        </div>
                        <div class="row-fluid" style="background-color: #eee; border-top: black thin; padding: 10px 0 10px 0;">
                        <div class="span12 align-center">
                            <div class="btn-group">
                                <a href="adminMgr-Admin_setting.php" class="btn btn-danger" style="color: white;"><i class="icon-remove icon-white"></i> Cancel Changes</a>
                                <button id="btn-submit" name="saveSetting" type="submit" class="btn btn-success" ><i class="icon-check icon-white"></i> Save Changes</button>
                            </div>
                        </div>
                        </div>
                    </form>   
            </div>
        </div>
        <div class="row-fluid">
            <div class="span6">
                <?php require_once("admin-Footer.php"); ?>
            </div>
        </div>
    </div>
<script src="js/userPassVldt.js" type="text/javascript"></script>
<script type="text/javascript">
    $("#message1").fadeOut(8400);
</script>
<script src="js/globalJS.js" type="text/javascript"></script>
</body></html>