<?php
ob_start("ob_gzhandler");
require_once("packages/require.php");
include("packages/check_login.php");//USED BY ALL PAGE BUT index.php
include("controller/controller_user.php");
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <?php include('packages/head.php');?>
    <script src="js/user/userVldt.js" type="text/javascript"></script>
</head>
<body class="civMain" onload="startTime()">
    <?php require_once("admin-Header.php");?>

    <!-- start top nav -->
    <?php $curPage='admin'; ?>
    <?php require_once("admin-top_nav.php");?>
    <!-- end top nav -->

    <div class="container">
        <div id="menuDashboard" class="row-fluid">
            <div id="rightContent" class="span12">
                
                <?php
                if($message!=null)
                {
                    echo "<div class='alert alert-info'><button type='button' class='close' data-dismiss='alert'>&times;</button>" . $message . "</div><br />";
                }
                ?>
                <div class="row-fluid">
                    <div class="span8 align-left">
                        <h4 style="margin: 0;">User Admin Management</h4>
                    </div>
                    <div class="span4 align-right">
                        <button class="btn btn-info" type="button" data-toggle="modal" data-target="#dialog-addUser"><i class="icon-plus icon-white"></i> User Admin</button>
                    </div>
                </div>
                <hr />
                <div class="row-fluid">
                    <div class="span12 align-right">
                        Total User : <span class="badge badge-info"><?php echo count($data_users);?></span>
                    </div>
                </div>
                <table class="table table-hover">
                    <tr>
                        <th>Email</th>
                        <th>Username</th>
                        <th style="text-align:center;">Action</th>
                    </tr>
                    <?php if(is_array($data_users)){foreach($data_users as $data_user){
                       $onclickDel = "\"if(window.confirm('Are you sure you would like to delete {$data_user['admin_username']}?')) location.href='{$admin['path-user']}?action=delete&admin_username={$data_user['admin_username']}&admin_id={$data_user['admin_id']}';\"";?>
                       <tr>
                        <td><?php echo $data_user['admin_email']; ?></td>
                        <td><?php echo $data_user['admin_username']; ?></td>
                        <td style="text-align: center;"><div class="btn-group"><a href="adminMgr-User_detail.php?admin_id=<?php echo $data_user['admin_id'];?> " type="button" class="btn btn-small"><i class="icon-edit"></i> edit</a><a href="#" <?php echo "onclick=$onclickDel"; ?> type="button" class="btn btn-danger btn-small" style="color: white;" ><i class="icon-trash icon-white"></i> delete</a></div></td>
                    </tr>
                    <?php }} ?>
                </table>
                <!--Modal box starts -->   
                <div id="dialog-addUser" class="modal hide fade">    
                    <form name="addUser" action="<?php echo $_SERVER['PHP_SELF'];?>?action=register" method="post" onsubmit="loadingText()">
                        <div class="modal-header">
                            <a href="#close" class="close" data-dismiss="modal" aria-hidden="true">×</a>
                            <strong>Create New User Admin</strong>
                        </div>
                        <div class="modal-body">
                            <div class="row-fluid">
                                <div class="span4 align-right modal-label"><strong>email&nbsp;*</strong>:</div>
                                <div class="span8"><input name="email" type="email" class="text" required="required" maxlength="64" placeholder="email address (max. 64 chars)" /></div>
                            </div>
                            <div class="row-fluid">
                                <div class="span4 align-right modal-label"><strong>username&nbsp;*</strong>:</div>
                                <div class="span8"><input name="username" type="text" class="text" required="required" maxlength="64" placeholder="username (max. 24 chars)" /></div>
                            </div>
                            <div class="row-fluid">       
                                <div class="span4 align-right modal-label"><strong>password&nbsp;*</strong>:</div>
                                <div class="span8"><input name="password1" min="5" type="password" required="required" class="text" maxlength="16" placeholder="password (max. 16 chars)" /></div>
                            </div>
                            <div class="row-fluid">
                                <div class="span4 align-right modal-label"><strong>re-enter password&nbsp;*</strong>:</div>
                                <div class="span8"><input name="password2" min="5" type="password" required="required" class="text" maxlength="16" placeholder="re-enter password (max. 16 chars)" /></div>
                            </div>
                            <div class="row-fluid">
                                <div id="SubStatus" class="span12 align-center"></div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <div class="btn-group">
                                <a href="#" class="btn" data-dismiss="modal"><i class="icon-remove"></i> Close</a>
                                <button id="btn-submit" name="submitUser" type="submit" class="btn btn-success btn-submit" onclick="return validateFormOnSubmit(addUser);" ><i class="icon-plus-sign icon-white"></i> Create</button>
                            </div>
                        </div>
                    </form> 
                </div>
                <!--Modal box ends --> 
            </div>
        </div>
        <div class="row-fluid">
            <div class="span6">
                <?php require_once("admin-Footer.php"); ?>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $("#message1").fadeOut(8400);
    </script>
    <script src="js/globalJS.js" type="text/javascript"></script>
</body></html>