<?php
ob_start("ob_gzhandler");
require_once("packages/require.php");
include("packages/check_login.php");//USED BY ALL PAGE BUT index.php
include("controller/controller_field.php");
$page_name = "adminMgr-Field.php";
$query_page = "letter=$letter";
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <?php include('packages/head.php');?>
    <!-- calendar -->
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.0/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.1/jquery-ui.js"></script>
    <script>
  $(function() {
    $( ".datepicker" ).datepicker({dateFormat: 'yy-mm-dd' });
  });
  </script>
    <style>
        body .modal {
            width: 50%; /* desired relative width */
            left: 25%; /* (100%-width)/2 */
            /* place center */
            margin-left:auto;
            margin-right:auto; 
        }
    </style>
</head>
<body class="civMain" onload="startTime()">
    <?php require_once("admin-Header.php");?>
    <div class="container">
        <div id="breadcrumb-parent" class="row-fluid">
            <div id="breadcrumb" class="span6">
                <a id="dashboard-link" href="#dashboard"><i class="icon-th-large"></i> Dashboard</a>
                : <a href='adminMgr-Field.php'>Field Management</a>
            </div>
            <div id="notifications" class="span6 align-right">
                <span>Welcome on <span id="date"></span></span>
            </div>
        </div>
        <div id="menuDashboard" class="row-fluid">
            
            <div id="leftNav-Parent" class="span3">
                <?php $curPage='field'; ?>
                <?php require_once("admin-Sidebar.php");?>
            </div>
            
            <div id="rightContent" class="span9">
                
                <?php
                if($message!=null)
                {
                    echo "<div id='message1' class='alert alert-info'>" . $message . "</div><br />";
                }
                ?>
                <div class="row-fluid">
                    <div class="span6 align-left">
                        <h4 style="margin: 0;">Field Management</h4>
                    </div>
                    <div class="span6 align-right">
                        <button class="btn btn-info" type="button" data-toggle="modal" data-target="#modalField"><i class="icon-plus icon-white"></i> Field</button>
                    </div>
                </div>
                <hr/>
                <div class="row-fluid">
                    <div class="span12 align-right">
                        Total Data starts with <span class="badge badge-important"><?php if($letter == ''){ echo "A-Z"; }else{ echo strtoupper($letter);}?></span> : <span class="badge badge-info"><?php echo $total_data;?></span><br/>
                        Showing : <span class="badge badge-info"><?php echo (($O_page-1)*20+1)." - ".(count($data_fields)+(($O_page-1)*20));?></span> of <span class="badge badge-info"><?php echo $total_data;?></span> 
                    </div>
                </div>
                <hr/>
                <!-- STARTS LETTER -->
                <div class="row-fluid">
                    <div class="span-12">
                        <?php for ($i = 'a', $j = 1; $j <= 26; $i++, $j++) {
                                $letters[$j] = $i;
                    ?>
                    <a href="<?php echo "adminMgr-Field.php?page=1&letter=$letters[$j]";?>" class="btn-mini <?php  if($letter == $letters[$j] ){echo 'active-alphabet';} ?>" ><b><?php echo strtoupper($letters[$j]);?></b></a>&nbsp;
                    <?php } ?>
                    </div>
                </div><!-- ENDS LETTER -->

                <table class="table table-bordered table-hover up2">
                <thead>
                    <tr>
			            <th>#</th>
                        <th>Field Title</th>
                        <th>Parent Field</th>
                        <th># of Jobs</th>
                        <th>Image</th>
                        <th>Publish</th>
                        <th>Last Modified</th>
                        <th>Create Date</th>
                        <th style="text-align:center;width:70px !important;">Action</th>
                    </tr>
                </thead>
                <?php
                if(!is_array($data_fields)){
                    echo "
                    <tbody><td colspan='8'><h5>There is no data.</h5></td></tbody>";
                }else{?>
                <tbody>
                <?php $k=1;
                    foreach($data_fields as $data_field){
                    if($k >= 1 && $total_data >= 1){
                        $onclickDel = "\"if(window.confirm('Are you sure you want to delete {$data_field['Field_title']}?')) location.href='adminMgr-Field.php?action=delete&field_ID={$data_field['Field_ID']}&field_Title={$data_field['Field_title']}';\"";
                    }
                    ?>
                    <tr class="<?php if($data_field['Field_publish'] == 'Not Publish'){echo 'error';}?>">
	                    <td><?php echo ($O_page-1)*20+$k;?></td>
                        <td><?php echo $data_field['Field_title'];?></td>
                        <td><?php echo $data_field['Field_parent'];?></td>						
                        <td><?php echo $data_field['no_jobs'];?></td>
                        <td><a class="proPhoto" href="<?php echo $global['img-url'].$data_field['Field_img']; ?>"><img class="img-circle" src="<?php echo $global['img-url'].$data_field['Field_img_thmb']; ?>" /></a></td>
                        <td><?php echo $data_field['Field_publish'];?></td>
                        <td><?php echo $data_field['Field_modify_date'];?></td>
                        <td><?php echo $data_field['Field_create_date'];?></td>
                        <td style="text-align: center;">
                            <div class="btn-group-vertical">
                                <div class="btn-group">
                                  <a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
                                    <i class=" icon-cog"></i>
                                  </a>
                                  <ul class="dropdown-menu align-left">
                                    <li><a href="#" <? echo "onclick=$onclickDel";?><?php if($k >= 1 && $total_data >=1){echo " disabled";}?>><i class=" icon-remove"></i> Delete</a></li>
                                    <li><a href="adminMgr-Field_detail.php?field_ID=<?php echo $data_field['Field_ID'];?>"><i class=" icon-edit"></i> Edit</a></li>
                                  </ul>
                                </div>
                            </div>
                        </td>
                    </tr>
                <?php $k++; }?>
                </tbody>
                <? }?>
                </table>
                <br/>
                <?php include("part-pagination.php");?>
            </div>
        </div>
        <div class="row-fluid">
            <div class="span6">
                <?php require_once("admin-Footer.php"); ?>
            </div>
        </div>
    </div>
    <!--Modal box starts -->  
    <div id="modalField" class="modal hide fade">    
        <form name="addField" action="adminMgr-Field.php?action=insert" method="post" enctype="multipart/form-data" onsubmit="loadingText()">         
            <div class="modal-header">
                <a href="#close" class="close" data-dismiss="modal" aria-hidden="true">×</a>
                <strong>Create New Field</strong>
            </div>
            <div class="modal-body">
                <div class="row-fluid">
                    <div class="span4 modal-label">Parent Field :</div>
                    <div class="span8">
                        <select name="parent_id" class="text">
                            <option value="">Select Parent Field</option>
                            <?php foreach($parent_fields as $parent_field){ ?>
                            <option value="<?php echo $parent_field['Field_ID'];?>"><?php echo $parent_field['Field_title'];?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="row-fluid">
                    <div class="span4 modal-label"><strong>Field Title&nbsp;*</strong>:</div>
                    <div class="span8"><input name="title" type="text" class="text input-block-level" placeholder="Field Name" required="required" /></div>
                </div>
                <div class="row-fluid up1">
                    <div class="span4 modal-label">Field Description :</div>
                    <div class="span8"><textarea name="content" class="input-block-level" wrap="soft" placeholder="description" rows="5" ></textarea></div>
                </div>
                <div class="row-fluid up1">       
                    <div class="span4 modal-label"><strong>Field Image&nbsp;*</strong>:</div>
                    <div class="span8"><input name="image" type="file" class="text input-block-level" maxlength="256" placeholder="field image" required="required" /></div>
                </div>
                <div class="row-fluid">
                    <div class="offset4 span8"><small>image format has to be jpg, jpeg, gif, png.</small></div>
                </div>
                <div class="row-fluid up1">
                    <div class="span4 modal-label">Field Publish :</div>
                    <div class="span8">
                        <select name="publish" class="text">
                            <option value="Publish">Publish</option>
                            <option value="Not Publish">Not Publish</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="btn-group">
                    <a href="#" class="btn" data-dismiss="modal"><i class="icon-circle-arrow-left"></i> Close</a>
                     <button id="btn-submit" name="submitField" type="submit" class="btn btn-success btn-submit"><i class="icon-ok icon-white"></i> Create</button>
                </div>
            </div>
        </form>
    </div><!--Modal box ends -->
    <script type="text/javascript">
        $("#message1").fadeOut(8400);
    </script>
    <script src="js/globalJS.js" type="text/javascript"></script>
</body></html>