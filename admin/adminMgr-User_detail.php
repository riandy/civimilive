<?php
ob_start("ob_gzhandler");
require_once("packages/require.php");
include("packages/check_login.php");//USED BY ALL PAGE BUT index.php
include("controller/controller_user_detail.php");
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php include('packages/head.php');?>
</head>
<body class="civMain" onload="startTime()">
    <?php require_once("admin-Header.php"); ?>

    <!-- start top nav -->
    <?php $curPage='admin'; ?>
    <?php require_once("admin-top_nav.php");?>
    <!-- end top nav -->
    
    <div class="container">
        <div id="menuDashboard" class="row-fluid">
            <div id="rightContent" class="span12">
            <?php
            if($saveStatus!=null)
            {
                echo "<div class='alert alert-info'>" . $saveStatus . "</div><br />";
            }
            ?>
            <div class="row-fluid">
                <div class="span3">
                    <a href="adminMgr-User.php" class="btn btn-default btn-small"><i class="icon-circle-arrow-left"></i> Back</a>
                </div>
                <div class="span6">
                    <h4 style="margin: 0;text-align:center;">User Admin Edit (Detail Info)</h4>
                </div>
                <div class="span3 align-right">
                    <button class="btn btn-info btn-small" type="button" data-toggle="modal" data-target="#dialog-password"><i class="icon-lock icon-white"></i> Change Password</button>
                </div>
            </div>
            <hr />
                <form name="addUser" action="<?php $_SERVER['PHP_SELF'];?>?action=update" method="post" onsubmit="loadingText()">
                    <div class="row-fluid up2">
                        <div class="span5 align-right"><strong>email&nbsp;*</strong>:</div>
                        <div class="span7">
                            <input name="admin_email" type="email" class="email" required="required" maxlength="64" value="<?php echo $data_user_email;?>" placeholder="email address (max. 64 chars)" />
                        </div>
                    </div>
                    <div class="row-fluid">
                        <div class="span5 align-right"><strong>username&nbsp;*</strong>:</div>
                        <div class="span7">
                            <input name="admin_username" type="text" class="text" required="required" maxlength="64" value="<?php echo $data_user_username;?>" placeholder="username (max. 64 chars)" />
                        </div>
                    </div>
                    <div class="row-fluid">
                        <div class="span8 offset4">
                            <div id="SubStatus"></div>
                        </div>
                    </div>
                    <div class="row-fluid" style="background-color: #eee; border-top: black thin; padding: 10px 0 10px 0;">
                        <div class="span12 align-center">
                            <input name="admin_id" type="hidden" value="<?php echo $data_user_id;?>"/>
                            <div class="btn-group" align="center">
                                <a href="adminMgr-User.php" class="btn btn-danger" style="color: white;"><i class="icon-remove icon-white"></i> Cancel</a>
                                <button id="btn-submit" name="saveUser" type="submit" class="btn btn-success" ><i class="icon-check icon-white"></i> Save User</button>
                            </div>
                        </div>
                    </div>
                </form>
                <!--Modal box starts -->   
                <div id="dialog-password" class="modal hide fade">    
                    <form name="changePass" action="adminMgr-User_detail.php?action=change_password" method="post" onsubmit="loadingText()">
                        <div class="modal-header">
                            <a href="#close" class="close" data-dismiss="modal" aria-hidden="true">×</a>
                            <strong>Change Password</strong>
                        </div>
                        <div class="modal-body">
                            <div class="row-fluid">
                                <div class="span4 align-right"><strong>current password&nbsp;*</strong>:</div>
                                <div class="span8">
                                    <input id="oldpass2" pattern=".{5,}" required title="5 characters minimum" name="old_password" type="password" required="required" class="input-block-level" maxlength="16" placeholder="current password (max. 16 chars)" />
                                    <input id="oldpass1" type="hidden" value="<?php echo $data_user_password;?>"/>
                                </div>
                            </div>
                            <div class="row-fluid">
                                <div class="span4 align-right"><strong>new password&nbsp;</strong>:</div>
                                <div class="span8">
                                    <input id="newpass1" pattern=".{5,}" required title="5 characters minimum" name="new_password_1" type="password" class="input-block-level" maxlength="16" placeholder="new password (max. 16 chars)" />
                                </div>
                            </div>
                            <div class="row-fluid">
                                <div class="span4 align-right"><strong>re-enter new password&nbsp;</strong>:</div>
                                <div class="span8">
                                    <input id="newpass2" pattern=".{5,}" required title="5 characters minimum" name="new_password_2" type="password" class="input-block-level" maxlength="16" placeholder="re-enter new password (max. 16 chars)" />
                                </div>
                            </div>
                            <div class="row-fluid">
                                <div id="SubStatus2" class="span12 align-center"></div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <input name="admin_id" type="hidden" value="<?php echo $data_user_id;?>"/>
                            <input name="admin_username" type="hidden" value="<?php echo $data_user_username;?>"/>
                            <div class="btn-group">
                                <a href="#" class="btn" data-dismiss="modal"><i class="icon-remove"></i> Close</a>
                                <button id="btn-submit" name="submitPass" type="submit" class="btn btn-success btn-submit" onclick="return validateFormOnSubmit(changePass);"><i class="icon-ok icon-white"></i> Change Password</button>
                            </div>
                        </div>
                    </form> 
                </div>
                <!--Modal box ends -->        
            </div>
        </div>
        <div class="row-fluid">
            <div class="span6">
                <?php require_once("admin-Footer.php"); ?>
            </div>
        </div>
    </div>
    <script src="js/userPassVldt.js" type="text/javascript"></script>
    <script type="text/javascript">
        $("#message1").fadeOut(8400);
    </script>
    <script src="js/globalJS.js" type="text/javascript"></script>
</body></html>