<div id="headerBack">
    <div class="container">
        <div class="row-fluid">
            <!-- <div id="admin-logo" class="span6"> -->
            <div  class="span6">
                <a title="Admin of <?php echo $config['company-name'];?>" href="<?php echo $admin['path-home'];?>">
                    <img src="<?php echo $global['logo-desktop'];?>" alt="<?php echo $config['company-name'];?>" border="0" style="max-height: 40px;margin:10px;" />
                </a>
            </div>
            <div class="span6 align-right">
                <div id="logout"><i class="icon-user"></i>&nbsp;<?php echo $_SESSION['admin_username'];?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <i class="icon-heart"></i>&nbsp;<?php echo $_SESSION['admin_role'];?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <a href="<?php echo $_SERVER['PHP_SELF'];?>?action=logout"><i class="icon-off"></i>&nbsp;logout</a></div>
                <div id="previewBox">
                    <a id="web-link" title="Preview of website" target="_blank" href="<?=$global['base'];?>">Preview Website</a>
                </div>
            </div>
        </div>
    </div>
</div>