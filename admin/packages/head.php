<title>Admin Manager of <?php echo $global['company-name']; ?></title>
<link rel="shortcut icon" href="<?php echo $global['favicon'];?>"/>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="robots" content="noindex, nofollow" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
<link href="css/bootstrap.min.css" rel="stylesheet" media="screen"/>
<link href="css/bootstrap-responsive.min.css" rel="stylesheet" media="screen"/>
<link href="stylesheets/dashboard.css" rel="stylesheet" type="text/css" />
<link href="stylesheets/admin-global.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="<?php echo $global['absolute-url'];?>/stylesheets/libcvm.css"/>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/scroll_civimiDB.js" type="text/javascript"></script>
<script src="js/getDate.js" type="text/javascript"></script>