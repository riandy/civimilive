<?php
    require_once('../model/Admin.php'); 
    $obj_admin = new Admin();

if($obj_admin->admin_check_login() == 0){
    header("Location:index.php?notLogin");
}

if($_GET['action'] == 'logout'){
    if($obj_admin->admin_logout()){
	header("Location:index.php?logout");
    }
}
?>