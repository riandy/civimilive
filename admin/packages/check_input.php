<?php

function check_progress($value, $arr_value){
    $data = 0;

    if($value == "personal"){
        for($i=0; $i<count($arr_value); $i++){
            if($arr_value[$i]['module'] == "image"){
                if($arr_value[$i]['field'] != ""){
                    $data += 30;
                }
            }else if($arr_value[$i]['module'] == "profession"){
                if($arr_value[$i]['field'] != ""){
                    $data += 25;
                }
            }else if($arr_value[$i]['module'] == "city"){
                if($arr_value[$i]['field'] != ""){
                    $data += 15;
                }
            }else if($arr_value[$i]['module'] == "state"){
                if($arr_value[$i]['field'] != ""){
                    $data += 10;
                }
            }else if($arr_value[$i]['module'] == "country"){
                if($arr_value[$i]['field'] != ""){
                    $data += 10;
                }
            }else if($arr_value[$i]['module'] == "phone"){
                if($arr_value[$i]['field'] != ""){
                    $data += 5;
                }
            }else if($arr_value[$i]['module'] == "website"){
                if($arr_value[$i]['field'] != ""){
                    $data += 5;
                }
            }
        }
    }else if($value == "education"){
        if($arr_value > 1){
            $data = 100;
        }else if($arr_value == 1){
            $data = 80;
        }else{
            $data = 0;
        }
    }

    return $data;
}

function encode($param){
    $new_result = $param;
    if($param != null){
    
    $result = clean($param);
    
    $new_result = rawurlencode($result);
    }
    return $new_result;
}

function clean($string) {
    $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
    $string = preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
    $string = strtolower($string); // Convert to lowercase
 
    return $string;
}

//function for check retrieved image in admin page
function check_image_url($image_url, $module){
    $result = 0;

    if($image_url != null){
        if(strpos($image_url, "http://") > -1 || strpos($image_url, "https://") > -1){
            $result = $image_url;
        }//end check if contains http://
        else{
            $result = "http://www.civimi.com/".$image_url;
        }//end else if not contains http://
    }else{
        if($module == "user"){
            $result = "http://www.civimi.com/beta/civimi/img/user.png";
        }else if($module == "company"){
            $result = "http://www.civimi.com/beta/civimi/img/work.png";
        }else if($module == "education"){
            $result = "http://www.civimi.com/beta/civimi/img/education.png";
        }else if($module == "certificate"){
            $result = "http://www.civimi.com/beta/civimi/img/certificate.png";
        }else{
            $result = "http://www.civimi.com/beta/civimi/img/thumbs-up.png";
        }
    }
    return $result;
}

function cleanSpace($string) {
    $string = trim($string);
    while(strpos($string,' ')){
        $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
    }
    return $string;
}

function currency($value) {
    if ($value > 0) {
        $value = number_format($value, 0, ',', '.');
    }
    return $value;
}

function getBatch($page){
    $test = 10;
    $batch =1;
    $flag = false;
    while(!$flag){
      if($page > $test){
          $test = $test + 10;
          $batch++;
      }else{  
          $flag = true;
      }
  }
  return $batch;
}

function checkUrl($N_url,$param,$value){
    if(strpos($N_url,'?') || strpos($N_url,'&')){
            $url = $N_url."&".$param."=".$value;
        }else{
            $url = $N_url."?".$param."=".$value;
        }
        return $url;
}

function dropLang($newLang){
    if(strpos(curPageName(),'?lang')){
        $urlTest = substr(curPageName(),0,-2);
        $url = $urlTest."$newLang";
                                // echo "1";
    }
    else if(strpos(curPageName(),'&lang')){
        $urlTest = substr(curPageName(),0,-2);
        $url = $urlTest."$newLang";
                                // echo "2";
    }
    else if(strpos(curPageName(),'&') || strpos(curPageName(),'?')){
        $url = curPageName()."&lang=".$newLang;
                                // echo "3";
    }
    else{
        $url = curPageName()."?lang=".$newLang;
                                // echo "4";
    }
    return $url;
}

function relative_time($ts)
{
    if(!ctype_digit($ts))
        $ts = strtotime($ts);

    $diff = time() - $ts;
    if($diff == 0)
        return 'now';
    elseif($diff > 0)
    {
        $day_diff = floor($diff / 86400);
        if($day_diff == 0)
        {
            if($diff < 60) return 'just now';
            if($diff < 120) return '1 minute ago';
            if($diff < 3600) return floor($diff / 60) . ' minutes ago';
            if($diff < 7200) return '1 hour ago';
            if($diff < 86400) return floor($diff / 3600) . ' hours ago';
        }
        if($day_diff == 1) return 'Yesterday';
        if($day_diff < 7) return $day_diff . ' days ago';
        if($day_diff < 31) return ceil($day_diff / 7) . ' weeks ago';
        if($day_diff < 60) return 'last month';
        return date('F Y', $ts);
    }
    else
    {
        $diff = abs($diff);
        $day_diff = floor($diff / 86400);
        if($day_diff == 0)
        {
            if($diff < 120) return 'in a minute';
            if($diff < 3600) return 'in ' . floor($diff / 60) . ' minutes';
            if($diff < 7200) return 'in an hour';
            if($diff < 86400) return 'in ' . floor($diff / 3600) . ' hours';
        }
        if($day_diff == 1) return 'Tomorrow';
        if($day_diff < 4) return date('l', $ts);
        if($day_diff < 7 + (7 - date('w'))) return 'next week';
        if(ceil($day_diff / 7) < 4) return 'in ' . ceil($day_diff / 7) . ' weeks';
        if(date('n', $ts) == date('n') + 1) return 'next month';
        return date('F Y', $ts);
    }
}

function sessionText(){
    if($_SESSION['lang'] != null){
        $langText = $_SESSION['lang'];
        if($langText == 'EN'){
            $messageText = "English";
        }
        else if($langText == 'IN'){
            $messageText = "Indonesian";
        }
        else if($langText == 'CN'){
            $messageText = "Chinese";
        }
    }else{
        $messageText = "English";
    }
    return $messageText;
}
function checkSession($newLang=null){
    $timeout = 1800;
    if($newLang == null){
        $newLang = 'EN';
    }

    if($_GET['lang'] != null){
        if($_GET['lang'] != null){
            $newLang = $_GET['lang'];               
        }
        if(isset($_SESSION['lang'])){
            // session_destroy();
            unset($_SESSION['lang']);
        }
        session_start();
        $_SESSION['lang'] = $newLang;
        $_SESSION['time'] = time();
        $lang = $_SESSION['lang'];
    }
    else if($_SESSION['lang'] == null){
        if(isset($_SESSION['lang'])){
            unset($_SESSION['lang']);
        }
        session_start();
        $_SESSION['lang'] = $newLang;
        $_SESSION['time'] = time();
        $lang = $_SESSION['lang'];
    }
    else if($_GET['lang'] == null && $_SESSION['lang'] != null){
        $duration = time() - (int)$_SESSION['time'];
// echo time()."<br/>";
// echo (int)$_SESSION['time'];
        if($duration > $timeout){
            unset($_SESSION['lang']);
            $lang = checkSession($newLang);
        }
        $lang = $_SESSION['lang'];
    }

    return $lang;
}


function curPageName() {
    return substr($_SERVER["REQUEST_URI"],strrpos($_SERVER["SCRIPT_NAME"],"/")+1);
}

function checkMaterial($material, $productID){
    $countMaterial = count($material);

    for($kl = 0; $kl < $countMaterial; $kl++){
        if($productID == $material[$kl]){
            echo "selected";
        }
    }
}

//BILLY - MPJ - function to display default image on categories.php
function defaultPic($catID,$defaultIMG,$desiredIMG){
    if($catID == null || $catID == ''){
        return $defaultIMG;
    }else{
        return $desiredIMG;
    }
}


// Return specified number of tabs to improve readability of HTML output
function itemButton($num,$O_item){
    if($O_item == $num)echo "active";
}

function labelStatus($status){
    if($status == 'IN PROCESS'){
        echo "<span class='label label-info'>" .$status. "</span>";
    }
    else if($status == 'CANCELED'){
        echo "<span class='label label-important'>" .$status. "</span>";
    }
    else if($status == 'COMPLETE'){
        echo "<span class='label label-success'>" .$status. "</span>";
    }
}
function tab($n) {
    $tabs = null;
    while ($n > 0) {
        $tabs .= "\t";
        --$n;
    }
    return $tabs;
}

function orderRow($status){
    if($status == 'IN PROCESS'){
        echo "info";
    }
    else if($status == 'CANCELED'){
        echo "error";
    }
    else if($status == 'COMPLETE'){
        echo "success";
    }
}

function checkImage($photoThmb) {
    if ($photoThmb == null) {
        $temp = "img/img-empty.png";
    } else {
        $temp = "$photoThmb";
    }
    return $temp;
}

function checkImage1($photoThmb,$brandThmb) {
    if ($photoThmb == NULL) {
        $temp = "$brandThmb";
    } else {
        $temp = "$photoThmb";
    }
    return $temp;
}

function nowDate() {
    $date = date_create("", timezone_open('Asia/Jakarta'));
    $date = date_format($date, 'Y-m-d');
    return $date;
}

function generatePODate() {
    $date = date_create("", timezone_open('Asia/Jakarta'));
    $date = date_format($date, 'ymd-His');
    return $date;
}

function nowDateComplete() {
    $date = date_create("", timezone_open('Asia/Jakarta'));
    $date = date_format($date, 'jS F Y - g:i A');
    return $date;
}

//Function To check and secure the login info from hacker attack
function check_input($data) {
    $data = trim($data);
    $data = stripslashes($data);
    $data = addslashes($data);
    $data = htmlspecialchars($data, ENT_QUOTES);
    return $data;
}

function get_rows($table_and_query) {
    $total = mysql_query("SELECT COUNT(*) FROM $table_and_query");
    $total = mysql_fetch_array($total);
    return $total[0];
}

//Function get the extension of the document source
function getDocType($data) {
    $data = substr($data, -5);
    $pos = strpos($data, ".");

    if ($pos !== false) {
        $data = substr($data, $pos + 1);
        if ($data == "pdf" || $data == "doc" || $data == "docx") {
            return $data;
        } else {
            return "link";
        }
    } else {
        return "link";
    }
}

//Function to check "http://"
function checkLink($data) {
    $preFix = substr($data, 0, 4);
    if ($preFix != 'http') {
        $data = "http://" . $data;
    }
    return $data;
}

//function to put 3dots after lengthy string
function charLength($data, $length) {
    $strLength = strlen($data);
    if ($strLength > $length) {
        $data = substr($data, 0, $length) . "...";
    }
    return $data;
}

//function
function checkSource($data) {
    $preFix = substr($data, 0, 4);
    if ($preFix != 'http') {
        $data = "http://www.civimi.com/" . $data;
    }
    return $data;
}

function correctDisplay($data) {
    $data = htmlspecialchars_decode(stripslashes($data), ENT_QUOTES);
    return $data;
}

function getInterest($data) {
    switch ($data) {
        case 1:
        $curInterest = "Open for any new opportunities";
        break;
        case 2:
        $curInterest = "Available for freelance work";
        break;
        case 3:
        $curInterest = "Looking for a full-time job";
        break;
        case 4:
        $curInterest = "Looking for an internship";
        break;
        case 5:
        $curInterest = "Starting-up a company";
        break;
        case 6:
        $curInterest = "Getting higher education";
        break;
        case 7:
        $curInterest = "Finishing college";
        break;
        case 8:
        $curInterest = "Not so sure yet";
        break;
        default:
        $curInterest = "";
    }
    return $curInterest;
}

function getShopInterest($data) {
    switch ($data) {
        case 1:
        $curInterest = "Open for any new opportunities";
        break;
        case 2:
        $curInterest = "Available for freelance work";
        break;
        case 3:
        $curInterest = "Currently Hiring";
        break;
        case 4:
        $curInterest = "Grand Opening";
        break;
        case 5:
        $curInterest = "Offering Discounts";
        break;
        case 6:
        $curInterest = "Having New Products";
        break;
        case 7:
        $curInterest = "For Sale";
        break;
        case 8:
        $curInterest = "Not so sure yet";
        break;
        default:
        $curInterest = "";
    }
    return $curInterest;
}

//function
function checkVideoSource($data) {
    $findme = "youtube.com/watch?v=";
    $findme2 = "youtu.be/";
    $pos = strpos($data, $findme);
    $pos2 = strpos($data, $findme2);
    $dataLength = strlen($data);

    if ($pos !== false) {
        $findAmp = stripos($data, '&');

        if ($findAmp === false) {
            $data = str_replace("youtube.com/watch?v=", "youtube.com/embed/", $data);
            return $data;
        } else {
            $lengthTrimmed = $dataLength - ($findAmp);
            $data = substr($data, 0, -$lengthTrimmed);
            $data = str_replace("youtube.com/watch?v=", "youtube.com/embed/", $data);
            return $data;
        }
    } else if ($pos2 !== false) {
        $data = str_replace("youtu.be/", "youtube.com/embed/", $data);
        return $data;
    } else {
        return $data;
    }
}

function encodeURLslash($data) {
    $data = str_replace("/", "&slash", $data);
    return $data;
}

function decodeURLslash($data) {
    $data = str_replace("&slash", "/", $data);
    return $data;
}

function encodeURLspace($data) {
    $data = str_replace(" ", "%20", $data);
    return $data;
}

function curPageURL() {
    $pageURL = 'http';
    if ($_SERVER["HTTPS"] == "on") {
        $pageURL .= "s";
    }
    $pageURL .= "://";
    if ($_SERVER["SERVER_PORT"] != "80") {
        $pageURL .= $_SERVER["SERVER_NAME"] . ":" . $_SERVER["SERVER_PORT"] . $_SERVER["REQUEST_URI"];
    } else {
        $pageURL .= $_SERVER["SERVER_NAME"] . $_SERVER["REQUEST_URI"];
    }
    return $pageURL;
}

function getCurrentRating($shopID) {
    require("Shop-dbConn.php");
    $shopIDIn = mysql_real_escape_string(check_input($shopID));
    $sql = "SELECT AVG(Review_rating) as avgRating FROM T_Review WHERE Review_shopID= '$shopIDIn'";
    $result = @mysql_query($sql);
    $rs = @mysql_fetch_array($result);
    return @round($rs[avgRating], 1);
    mysql_close($dbh);
}

function countRating($shopID) {
    require("Shop-dbConn.php");
    $shopIDIn = mysql_real_escape_string(check_input($shopID));
    $sql = "SELECT COUNT(Review_rating) as countRating FROM T_Review WHERE Review_shopID= '$shopIDIn'";
    $result = @mysql_query($sql);
    $rs = @mysql_fetch_array($result);
    return $rs[countRating];
    mysql_close($dbh);
}

function getCurrentProduct($shopID) {
    require("Shop-dbConn.php");
    $shopIDIn = mysql_real_escape_string(check_input($shopID));
    $sql = "SELECT Product_ID, Product_name FROM T_Product WHERE Product_userID= '$shopIDIn'  ORDER BY Product_name ASC";
    $result = @mysql_query($sql);
    $productCounter = 0;
    while ($row = mysql_fetch_array($result, MYSQL_ASSOC)) {
        $O_productID[$productCounter] = correctDisplay($row['Product_ID']);
        $O_productName[$productCounter] = correctDisplay($row['Product_name']);
        echo "<option value='$O_productID[$productCounter]'>$O_productName[$productCounter]</option>";
        $productCounter++;
    }
//return $productCounter;
    mysql_close($dbh);
}

function getRatingColor($rating) {
    if ($rating == null || $rating == '' || $rating == 0) {
        $rating = '?';
        $color = "#3366cc";
    } else if ($rating < 3) {
        $color = "#CA0606";
    } else if ($rating >= 4) {
        $color = "#64AA2B";
    } else {
        $color = "#999";
    }
    $result = "<font color='$color'>$rating</font>";
    return $result;
}

function getRatingTitle($rating) {
    if ($rating == '?' || $rating == null || $rating == '' || $rating == 0) {
        $title = "Be the first person to review...";
    } else if ($rating < 3) {
        $title = "Need Improvements";
    } else if ($rating >= 4) {
        $title = "Recommended Seller";
    } else {
        $title = "So Far So Good";
    }
    return $title;
}

function checkPlural($data) {
    if ($data > 1) {
        $plural = 's';
    }
    return $plural;
}

function doHash($secData) {
//creates a random 5 character sequence
    $salt = substr(md5(time()), 0, 5);
    $secData = hash('sha256', $salt . $secData);
    return $secData;
}

?>
