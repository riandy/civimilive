<?php 
$global['base'] = "http://www.civimi.com/";
$global['tips'] = $global['base']."tips/";
$global['api'] = $global['base']."api/";
$global['img-url'] = "http://www.civimi.com/";
$global['absolute-url'] = $global['base'];
$global['absolute-url-admin'] = $global['base']."admin/";
$global['user-profile'] = $global['base']."profile/";
$admin['path-home'] = $global['absolute-url-admin']."adminMgr.php";
$admin['path-industry_detail'] = $global['absolute-url-admin']."adminMgr-Industry_detail.php?industry_ID=";
$admin['path-company_detail'] = $global['absolute-url-admin']."adminMgr-Company_detail.php?company_ID=";
$admin['path-organization_detail'] = $global['absolute-url-admin']."adminMgr-Organization_detail.php?org_ID=";
$admin['path-edu_detail'] = $global['absolute-url-admin']."adminMgr-Edu_detail.php?edu_ID=";
$admin['path-cert_detail'] = $global['absolute-url-admin']."adminMgr-Certificate_detail.php?cert_ID=";
$admin['path-skill_detail'] = $global['absolute-url-admin']."adminMgr-Skill_detail.php?skill_ID=";
$admin['path-user_detail'] = $global['absolute-url-admin']."adminMgr-User_member_detail.php?user_ID=";

$global['favicon'] = $global['absolute-url']."img/icon/favicon.ico";
$global['logo-mobile'] = $global['absolute-url']."img/logo/logo-mobile.png";
$global['logo-desktop'] = $global['absolute-url']."img/logo/logo-desktop.png";
$global['company-name'] = "CIVIMI";
$global['copyright-desktop'] = "Copyright © ".date('Y')." CIVIMI. All rights reserved. Terms and Conditions";
?>