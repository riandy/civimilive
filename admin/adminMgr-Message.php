<?php
ob_start("ob_gzhandler");
require_once("packages/require.php");
include("packages/check_login.php");//USED BY ALL PAGE BUT index.php
include("controller/controller_message.php");
$page_name = "adminMgr-Message.php";
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <?php include('packages/head.php');?>
    <!-- calendar -->
    <link rel="stylesheet" type="text/css" href="packages/jQuery-Tags/jquery-ui.css" />
    <link rel="stylesheet" type="text/css" href="packages/jQuery-Tags/jquery.tagsinput.css" />
    <script src="//code.jquery.com/jquery-1.10.2.js"></script>
    <script type="text/javascript" src="packages/jQuery-Tags/jquery-ui.js"></script>
    <script type="text/javascript" src="packages/jQuery-Tags/jquery.tagsinput.js"></script>
    <script src="js/tinymce/tinymce.min.js"></script>

    <script>
      $(function() {
        $( ".datepicker" ).datepicker({dateFormat: 'yy-mm-dd' });
      });
    </script>
    <style>
        body .modal {
            width: 50%; /* desired relative width */
            left: 25%; /* (100%-width)/2 */
            /* place center */
            margin-left:auto;
            margin-right:auto; 
        }
    </style>
</head>
<body class="civMain" onload="startTime()">
    <?php require_once("admin-Header.php");?>

    <!-- start top nav -->
    <?php $curPage='message'; ?>
    <?php require_once("admin-top_nav.php");?>
    <!-- end top nav -->

    <div class="container">
        <div id="menuDashboard" class="row-fluid">
            <div id="rightContent" class="span12">
                
                <?php
                if($message!=null)
                {
                    echo "<div id='message1' class='alert alert-info'>" . $message . "</div><br />";
                }
                ?>
                <div class="row-fluid">
                    <div class="span12 align-center">
                        <h4 style="margin: 0;">Message Management</h4>
                    </div>
                </div>
                <hr>
                <div class="row-fluid">
                    <div class="span12 align-right">
                        Total Message : <span class="badge badge-info"><?php echo $total_data;?></span><br/>
                        Showing : <span class="badge badge-info"><?php echo (($O_page-1)*20+1)." - ".(count($data_messages)+(($O_page-1)*20));?></span> of <span class="badge badge-info"><?php echo $total_data;?></span> 
                    </div>
                </div>
                <hr/>
                <br>
                <table class="table table-hover">
                    <tr>
                        <th>#</th>
                        <th>Purpose</th>
                        <th>Message</th>
                        <th>To Email</th>
                        <th>Message By</th>
                        <th>Forwarded</th>
                        <th>Message Date</th>
                        <th style="text-align:center;width:70px !important;">Action</th>
                    </tr>
                <?php
                if(!is_array($data_messages)){
                    echo "
                    <tbody><td colspan='8'><h5>There is no message.</h5></td></tbody>";
                }else{?>
                <?php $k=1;
                    foreach($data_messages as $data_message){
                        $onclickDel = "\"if(window.confirm('Are sure want to delete message from {$data_message['Message_from_name']}?')) location.href='adminMgr-Message.php?action=delete&message_ID={$data_message['Message_ID']}&page={$O_page}&message_Title=".urlencode($data_message['Message_content'])."';\"";
                        $onclickSend = "\"if(window.confirm('Are sure want to send forward to {$data_message['Message_to_email']}?')) location.href='adminMgr-Message.php?action=send_forward&msg_ID={$data_message['Message_ID']}&to_email={$data_message['Message_to_email']}&from_email={$data_message['Message_from_email']}&username={$data_message['Message_to_user']}&name={$data_message['Message_from_name']}&purpose={$data_message['Message_purpose']}&page={$O_page}&message=".urlencode($data_message['Message_content'])."';\"";
                    ?>
                    <tr class="<?php if($data_message['Message_publish'] == 'Not Publish'){echo 'error';}?>">
                        <td><?php echo ($O_page-1)*20+$k;?></td>
                        <td class="message_purpose<?php echo $k;?>"><?php echo $data_message['Message_purpose'];?></td>
                        <td><?php echo charLength(correctDisplay($data_message['Message_content']), 100);?></td>
                        <td class="message_to_email<?php echo $k;?>"><?php echo $data_message['Message_to_email'];?></td>
                        <td class="message_name<?php echo $k;?>"><?php echo $data_message['Message_from_name'];?></td>
                        <td><?php echo $data_message['Message_forwarded'];?></td>
                        <td class="message_create<?php echo $k;?>"><?php echo date("d M Y, h:i:s", strtotime($data_message['Message_create_date']));?></td>
                        <td class="hide message_to_user<?php echo $k;?>"><?php echo $data_message['Message_to_user'];?></td>
                        <td class="hide message_from_user<?php echo $k;?>"><?php echo $data_message['Message_from_user'];?></td>
                        <td class="hide message_from_email<?php echo $k;?>"><?php echo $data_message['Message_from_email'];?></td>
                        <td class="hide message_content<?php echo $k;?>"><?php echo correctDisplay($data_message['Message_content']);?></td>
                        <td class="hide message_id<?php echo $k;?>"><?php echo $data_message['Message_ID'];?></td>
                        <td style="text-align: center;">
                            <div class="btn-group btn-group-horizontal">   
                                <a href="#" type="button" class="btn btn-small" data-toggle="modal" data-target="#modal-editMessage" onclick="return copyValue(<?php echo $k; ?>);"><i class="icon-edit"></i> Edit</a>
                                <a href="#" <?php echo "onclick=$onclickDel"; ?> type="button" class="btn btn-danger btn-small" style="color: white;"><i class="icon-trash icon-white"></i> Delete</a>
                                <a href="#" <?php echo "onclick=$onclickSend"; ?> type="button" class="btn btn-info btn-small" style="color: white;"><i class="icon-envelope icon-white"></i> Send</a>
                            </div>
                        </td>
                    </tr>
                <?php $k++; }?>
                <? }?>
                </table>
                <br/>
                <!-- start pagination !-->
                <div class="pagination align-center">
                  <ul class="pagination pagination-sm">
                    <?php
                    $batch = getBatch($O_page);
                    if($batch < 1){$batch = 1;}
                    $prevLimit = 1 +(10*($batch-1));
                    $nextLimit = 10 * $batch;

                    if($nextLimit > $total_page){$nextLimit = $total_page;}
                    if ($total_page > 1 && $O_page > 1) {
                        echo "<li><a href='".$page_name."?page=1&"."' class='pagination-slide'>&lt;&lt; First</a></li>";
                    }
                    if($batch > 1 && $O_page > 10){
                        echo "<li><a href='".$page_name."?page=".($prevLimit-1)."' class='pagination-slide'>&lt; Previous 10</a></li>";
                    }
                    for($mon = $prevLimit; $mon <= $nextLimit;$mon++){?>
                    <li class="<?php if($mon == $O_page){echo 'active';}?>"><span class="pagination-theme"><a href="<?php echo $page_name."?page=".$mon;?>" ><?php echo $mon;?></a></span></li>
                    <?php if($mon == $nextLimit && $mon < $total_page){if($mon >= $total_page){ $mon -=1;}
                    echo "<li><a href='".$page_name."?page=".($mon+1)."' class='pagination-slide'>Next 10 &gt;</a></li>";
                    }                               }
                    if ($total_page > 1 &&  ($O_page != $total_page)) {
                        echo "<li><a href='".$page_name."?page=".$total_page."&"."' class='pagination-slide'>Last &gt;&gt;</a></li>";
                    }
                    ?>
                  </ul>
                </div>
                <!-- end pagination !-->
            </div>
        </div>
        <div class="row-fluid">
            <div class="span6">
                <?php require_once("admin-Footer.php"); ?>
            </div>
        </div>
    </div>
    <!--Modal box starts -->  
    <div id="modal-editMessage" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">    
        <form name="editMessage" action="adminMgr-Message.php?action=update_message" method="post" enctype="multipart/form-data" onsubmit="loadingText()">         
            <div class="modal-header">
                <a href="#close" class="close" data-dismiss="modal" aria-hidden="true">×</a>
                <span class="badge badge-info">Edit Message</span>
            </div>
            <div class="modal-body">
                <div class="row-fluid">
                    <div class="span12 modal-label">To Email :</div>
                </div>
                <div class="row-fluid">
                    <div class="span12"><input name="to_email" type="email" class="text uneditable-input" required="required" placeholder="to email" disabled="true" /></div>
                </div>
                <div class="row-fluid">
                    <div class="span12"><a target="_blank" href="" id="href-to-user"><span id="to-user"></span></a></div>
                </div>
                <div class="row-fluid up1">
                    <div class="span12 modal-label">From Email :</div>
                </div>
                <div class="row-fluid">
                    <div class="span12"><input name="from_email" type="email" class="text uneditable-input" required="required" placeholder="from email" disabled="true" /></div>
                </div>
                <div class="row-fluid">
                    <div class="span12"><a target="_blank" href="" id="href-from-user"><span id="from-user"></span></a></div>
                </div>
                <div class="row-fluid up1">
                    <div class="span12 modal-label">Purpose :</div>
                </div>
                <div class="row-fluid">
                    <div class="span12"><input name="purpose" type="text" class="text uneditable-input" required="required" placeholder="purpose" disabled="true" /></div>
                </div>
                <div class="row-fluid">
                    <div class="span12 modal-label"><strong>Message&nbsp;*</strong>:</div>
                </div>
                <div class="row-fluid">
                    <div class="span12"><textarea name="content" type="text" class="text input-block-level" required="required" placeholder="Description your message" rows="5"></textarea></div>
                </div>
                <div class="row-fluid">
                    <div class="span12 modal-label">Create Date :</div>
                </div>
                <div class="row-fluid">
                    <div class="span12"><input name="create" type="text" class="text uneditable-input" required="required" placeholder="date" disabled="true" /></div>
                </div>
                <div class="row-fluid">
                    <div class="span12"><span class="modal-label">Message By : </span><span id="create-by"></span></div>
                </div>
                <div class="row-fluid">
                    <div id="SubStatus" class="span12 align-center"></div>
                    <input name="purpose" type="hidden" value=""/>
                    <input name="message_id" type="hidden" value=""/>
                    <input name="url" type="hidden" value="adminMgr-Message.php?page=<?php echo $O_page;?>"/>
                </div>
            </div>
            <div class="modal-footer">
                <div class="btn-group">
                    <a href="#" class="btn" data-dismiss="modal"><i class="icon-remove"></i> Cancel</a>
                     <button id="btn-submit" name="submitMessage" type="submit" class="btn btn-success btn-submit"><i class="icon-check icon-white"></i> Update</button>
                </div>
            </div>
        </form>
    </div><!--Modal box ends -->
    <script type="text/javascript">
        $("#message1").fadeOut(8400);

        function copyValue(para) {
            var to_user = $('.message_to_user' + para).text();
            var from_user = $('.message_from_user' + para).text();
            var purpose = $('.message_purpose' + para).text();
            var name = $('.message_name' + para).text();
            var to_email = $('.message_to_email' + para).text();
            var from_email = $('.message_from_email' + para).text();
            var content = $('.message_content' + para).text();
            var create = $('.message_create' + para).text();
            var msgID = $('.message_id' + para).text();

            $('#to-user').text('http://www.civimi.com/beta/resume/'+to_user+'.cvm');
            $('#from-user').text('http://www.civimi.com/beta/resume/'+from_user+'.cvm');
            $("input[name='to_email']").val(to_email);
            $("input[name='from_email']").val(from_email);
            $("textarea[name='content']").text(content);
            $("input[name='create']").val(create);
            $("input[name='purpose']").val(purpose);
            $("input[name='message_id']").val(msgID);
            $('#create-by').text(name);
            $('#href-to-user').attr('href', 'http://www.civimi.com/beta/resume/'+to_user+'.cvm');
            $('#href-from-user').attr('href', 'http://www.civimi.com/beta/resume/'+from_user+'.cvm');
        }
    </script>
    <script src="js/globalJS.js" type="text/javascript"></script>
</body></html>