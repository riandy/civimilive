<?php
ob_start("ob_gzhandler");
require_once("packages/require.php");
include("packages/check_login.php");//USED BY ALL PAGE BUT index.php
include("controller/controller_jobs_detail.php");
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <?php include('packages/head.php');?>
    <!-- calendar -->

    <link rel="stylesheet" type="text/css" href="packages/jQuery-Tags/jquery-ui.css" />
    <link rel="stylesheet" type="text/css" href="packages/jQuery-Tags/jquery.tagsinput.css" />
    <script src="//code.jquery.com/jquery-1.10.2.js"></script>
    <script src="js/jquery.select-to-autocomplete.js"></script>
    <script type="text/javascript" src="packages/jQuery-Tags/jquery-ui.js"></script>
    <script type="text/javascript" src="packages/jQuery-Tags/jquery.tagsinput.js"></script>
    <script src="js/tinymce/tinymce.min.js"></script>
    <script>
        tinymce.init({
            selector: "textarea",
            plugins: [
            "advlist autolink lists link image charmap print preview anchor",
            "searchreplace visualblocks code fullscreen",
            "insertdatetime media table contextmenu paste"
            ],
            relative_urls: false,
            forced_root_block : false,
            toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
        });
    </script>
    <script>
  $(function() {
    $('.autodrop').selectToAutocomplete();
    $( ".datepicker" ).datepicker({dateFormat: 'yy-mm-dd' });
  });
  </script>
    <style>
        body .modal {
            width: 50%; /* desired relative width */
            left: 25%; /* (100%-width)/2 */
            /* place center */
            margin-left:auto;
            margin-right:auto; 
        }
    </style>
</head>
<body class="civMain" onload="startTime()">
    <?php require_once("admin-Header.php");?>
    <div class="container">
        <div id="breadcrumb-parent" class="row-fluid">
            <div id="breadcrumb" class="span6">
                <a id="dashboard-link" href="#dashboard"><i class="icon-th-large"></i> Dashboard</a>
                : <a href='adminMgr-Jobs.php'>Jobs Management</a>
            </div>
            <div id="notifications" class="span6 align-right">
                <span>Welcome on <span id="date"></span></span>
            </div>
        </div>
        <div id="menuDashboard" class="row-fluid">
            
            <div id="leftNav-Parent" class="span3">
                <?php $curPage='jobs'; ?>
                <?php require_once("admin-Sidebar.php");?>
            </div>
            
            <div id="rightContent" class="span9">
                
                <?php
                if($message!=null)
                {
                    echo "<div class='alert alert-info'>" . $message . "</div><br />";
                }
                ?>
                <div class="row-fluid">
                    <div class="span3">
                        <a href="adminMgr-Jobs.php" class="btn btn-default btn-small"><i class="icon-circle-arrow-left"></i> Back</a>
                    </div>
                    <div class="span6">
                        <h4 style="margin: 0;text-align:center;">Jobs Edit (<?php echo $data_jobss[0]['Jobs_title'];?>)</h4>
                    </div>
                </div>
                <hr/>
                <?php $k=0;
                foreach($data_jobss as $data_jobs){?>
                <form name="addJobs" action="adminMgr-Jobs_detail.php?action=update" method="post" enctype="multipart/form-data" onsubmit="loadingText()">
                    <div class="row-fluid">
                        <div class="span4 modal-label"><strong>Jobs Title&nbsp;*</strong>:</div>
                        <div class="span8"><input name="title" type="text" class="text input-block-level" placeholder="Jobs Name" required="required" value="<?php echo $data_jobs['Jobs_title'];?>"></div>
                    </div>
                    <div class="row-fluid">
                        <div class="span4 modal-label">Jobs Description :</div>
                        <div class="span8"><textarea name="content" class="textarea" wrap="soft" placeholder="description" rows="5" ><?php echo $data_jobs['Jobs_content'];?></textarea></div>
                    </div>
                    <div class="row-fluid up1">
                        <div class="span4 modal-label"><strong>Company&nbsp;*</strong>:</div>
                        <div class="span8">
                            <select name="company_id" class="text autodrop" required="required">
                                <option value="">Select Company</option>
                                <?php foreach($data_companys as $data_company){ ?>
                                <option <?php if($data_jobs['Jobs_ref_companyID'] == $data_company['Company_ID']){ echo "selected=selected"; }?> value="<?php echo $data_company['Company_ID'];?>"><?php echo correctDisplay($data_company['Company_title']);?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="row-fluid up1">
                        <div class="span4 modal-label"><strong>Field&nbsp;*</strong>:</div>
                        <div class="span8">
                            <select name="field_id" class="text" required="required">
                                <option value="">Select Field</option>
                                <?php foreach($data_fields as $data_field){ ?>
                                <option <?php if($data_jobs['Jobs_ref_fieldID'] == $data_field['Field_ID']){ echo "selected=selected"; }?> value="<?php echo $data_field['Field_ID'];?>"><?php echo $data_field['Field_parent']." - ".$data_field['Field_title'];?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="row-fluid up1">
                        <div class="span4 modal-label"><strong>Level&nbsp;*</strong>:</div>
                        <div class="span8">
                            <select name="level_id" class="text" required="required">
                                <option value="">Select Level</option>
                                <?php foreach($data_levels as $data_level){ ?>
                                <option <?php if($data_jobs['Jobs_ref_levelID'] == $data_level['Level_ID']){ echo "selected=selected"; }?> value="<?php echo $data_level['Level_ID'];?>"><?php echo $data_level['Level_title'];?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="row-fluid up1">
                        <div class="span4 modal-label"><strong>City&nbsp;*</strong>:</div>
                        <div class="span8">
                            <select name="city_id" class="text autodrop" required="required">
                                <option value="">Select City</option>
                                <?php foreach($data_citys as $data_city){ ?>
                                <option <?php if($data_jobs['Jobs_ref_cityID'] == $data_city['City_ID']){ echo "selected=selected"; }?> value="<?php echo $data_city['City_ID'];?>"><?php echo $data_city['City_title'].", ".$data_city['Country_abbr'];?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="row-fluid">
                        <div class="span4 modal-label">Jobs Experience</strong>:</div>
                        <div class="span8">
                            <div class="input-append">
                                <input class="span4" id="appendedInput" name="exp" type="text" class="text input-block-level" placeholder="Experience Year" value="<?php echo $data_jobs['Jobs_exp_year'];?>" />
                                <span class="add-on">Year</span>
                            </div>
                        </div>    
                    </div>
                    <div class="row-fluid up1">
                        <div class="span4 modal-label"><strong>Jobs Post Start&nbsp;*</strong> :</div>
                        <div class="span8"><input type="text" class="form-control datepicker" name="start" placeholder="Start Date" required="required" value="<?php echo $data_jobs['Jobs_post_start'];?>" ></div>
                    </div>
                    <div class="row-fluid up1">
                        <div class="span4 modal-label"><strong>Jobs Post End&nbsp;</strong> :</div>
                        <div class="span8"><input type="text" class="form-control datepicker" name="end" placeholder="End Date" required="required" value="<?php echo $data_jobs['Jobs_post_end'];?>" ></div>
                    </div>
                    <div class="row-fluid up1">
                        <div class="span4 modal-label">Jobs Qualification Info :</div>
                        <div class="span8"><textarea name="qualification" class="textarea" wrap="soft" placeholder="i.e. Description issue (max 2048 chars)" rows="5" ><?php echo $data_jobs['Jobs_qualification_info'];?></textarea></div>
                    </div>
                    <div class="row-fluid up1">
                        <div class="span4 modal-label">Jobs Apply Info :</div>
                        <div class="span8"><textarea name="apply" class="textarea" wrap="soft" placeholder="i.e. Description issue (max 2048 chars)" rows="5" ><?php echo $data_jobs['Jobs_apply_info'];?></textarea></div>
                    </div>
                    <div class="row-fluid up1">
                        <div class="span4 modal-label">Jobs Intern :</div>
                        <div class="span8">
                            <label class="checkbox">
                                <input name="intern" type="checkbox" value="1" <?php if($data_jobs['Jobs_intern'] == '1'){echo "checked=checked";}?>>
                            </label>
                        </div>
                    </div>
                    <div class="row-fluid up1">
                        <div class="span4 modal-label">Jobs Fulltime :</div>
                        <div class="span8">
                            <label class="checkbox">
                                <input name="fulltime" type="checkbox" value="1" <?php if($data_jobs['Jobs_fulltime'] == '1'){echo "checked=checked";}?>>
                            </label>
                        </div>
                    </div>
                    <div class="row-fluid up1">
                        <div class="span4 modal-label">Jobs Parttime :</div>
                        <div class="span8">
                            <label class="checkbox">
                                <input name="parttime" type="checkbox" value="1" <?php if($data_jobs['Jobs_parttime'] == '1'){echo "checked=checked";}?>>
                            </label>
                        </div>
                    </div>
                    <div class="row-fluid up1">
                        <div class="span4 modal-label">Jobs Freelance :</div>
                        <div class="span8">
                            <label class="checkbox">
                                <input name="freelance" type="checkbox" value="1" <?php if($data_jobs['Jobs_freelance'] == '1'){echo "checked=checked";}?>>
                            </label>
                        </div>
                    </div>
                    <div class="row-fluid up1">
                        <div class="span4 modal-label"><strong>Salary Type&nbsp;*</strong>:</div>
                        <div class="span8">
                            <select id="selectMenu" name="salary_type" class="text" required="required">
                                <option <?php if($data_jobs['Jobs_salary_type'] == "fixed"){ echo "selected=selected"; }?> value="fixed">Fixed</option>
                                <option <?php if($data_jobs['Jobs_salary_type'] == "negotiable"){ echo "selected=selected"; }?> value="negotiable">Negotiable</option>
                            </select>
                        </div>
                    </div>
                    <div class="row-fluid up1">
                        <div class="span4 modal-label"><strong>Salary Period&nbsp;*</strong>:</div>
                        <div class="span8">
                            <select name="salary_period" class="text" required="required">
                                <option <?php if($data_jobs['Jobs_salary_period'] == "monthly"){ echo "selected=selected"; }?> value="monthly">Monthly</option>
                                <option <?php if($data_jobs['Jobs_salary_period'] == "bi-weekly"){ echo "selected=selected"; }?> value="bi-weekly">Bi-weekly</option>
                                <option <?php if($data_jobs['Jobs_salary_period'] == "weekly"){ echo "selected=selected"; }?> value="weekly">Weekly</option>
                                <option <?php if($data_jobs['Jobs_salary_period'] == "daily"){ echo "selected=selected"; }?> value="daily">Daily</option>
                                <option <?php if($data_jobs['Jobs_salary_period'] == "hourly"){ echo "selected=selected"; }?> value="hourly">Hourly</option>
                            </select>
                        </div>
                    </div>
                    <div class="row-fluid up1">
                        <div class="span4 modal-label"><strong><span id="text-min">Salary <?php if($data_jobs['Jobs_salary_type'] == 'negotiable'){echo "Min";}?></span>&nbsp;*</strong>:</div>
                        <div class="span8">
                            <select name="salary_min" class="text" required="required">
                                <option value="">Select Salary</option>
                                <option <?php if($data_jobs['Jobs_salary_min'] == "0"){ echo "selected=selected"; }?> value="0">0</option>
                                <option <?php if($data_jobs['Jobs_salary_min'] == "1"){ echo "selected=selected"; }?> value="1">1 Juta</option>
                                <option <?php if($data_jobs['Jobs_salary_min'] == "2"){ echo "selected=selected"; }?> value="2">2 Juta</option>
                                <option <?php if($data_jobs['Jobs_salary_min'] == "3"){ echo "selected=selected"; }?> value="3">3 Juta</option>
                                <option <?php if($data_jobs['Jobs_salary_min'] == "5"){ echo "selected=selected"; }?> value="5">5 Juta</option>
                                <option <?php if($data_jobs['Jobs_salary_min'] == "7"){ echo "selected=selected"; }?> value="7">7 Juta</option>
                                <option <?php if($data_jobs['Jobs_salary_min'] == "10"){ echo "selected=selected"; }?> value="10">10 Juta</option>
                                <option <?php if($data_jobs['Jobs_salary_min'] == "15"){ echo "selected=selected"; }?> value="15">15 Juta</option>
                                <option <?php if($data_jobs['Jobs_salary_min'] == "20"){ echo "selected=selected"; }?> value="20">20 Juta</option>
                                <option <?php if($data_jobs['Jobs_salary_min'] == "25"){ echo "selected=selected"; }?> value="25">25 Juta</option>
                                <option <?php if($data_jobs['Jobs_salary_min'] == "30"){ echo "selected=selected"; }?> value="30">30 Juta</option>
                                <option <?php if($data_jobs['Jobs_salary_min'] == "45"){ echo "selected=selected"; }?> value="45">45 Juta</option>
                                <option <?php if($data_jobs['Jobs_salary_min'] == "60"){ echo "selected=selected"; }?> value="60">60 Juta</option>
                                <option <?php if($data_jobs['Jobs_salary_min'] == "75"){ echo "selected=selected"; }?> value="75">75 Juta</option>
                            </select>
                        </div>
                    </div>
                    <div id="showMax" class="row-fluid up1 <?php if($data_jobs['Jobs_salary_type'] == 'fixed'){ echo "hide";}?>">
                        <div class="span4 modal-label"><strong>Salary Max&nbsp;*</strong>:</div>
                        <div class="span8">
                            <select name="salary_max" class="text">
                                <option value="">Select Salary</option>
                                <option <?php if($data_jobs['Jobs_salary_max'] == "1"){ echo "selected=selected"; }?> value="1">1 Juta</option>
                                <option <?php if($data_jobs['Jobs_salary_max'] == "2"){ echo "selected=selected"; }?> value="2">2 Juta</option>
                                <option <?php if($data_jobs['Jobs_salary_max'] == "3"){ echo "selected=selected"; }?> value="3">3 Juta</option>
                                <option <?php if($data_jobs['Jobs_salary_max'] == "5"){ echo "selected=selected"; }?> value="5">5 Juta</option>
                                <option <?php if($data_jobs['Jobs_salary_max'] == "7"){ echo "selected=selected"; }?> value="7">7 Juta</option>
                                <option <?php if($data_jobs['Jobs_salary_max'] == "10"){ echo "selected=selected"; }?> value="10">10 Juta</option>
                                <option <?php if($data_jobs['Jobs_salary_max'] == "15"){ echo "selected=selected"; }?> value="15">15 Juta</option>
                                <option <?php if($data_jobs['Jobs_salary_max'] == "20"){ echo "selected=selected"; }?> value="20">20 Juta</option>
                                <option <?php if($data_jobs['Jobs_salary_max'] == "25"){ echo "selected=selected"; }?> value="25">25 Juta</option>
                                <option <?php if($data_jobs['Jobs_salary_max'] == "30"){ echo "selected=selected"; }?> value="30">30 Juta</option>
                                <option <?php if($data_jobs['Jobs_salary_max'] == "45"){ echo "selected=selected"; }?> value="45">45 Juta</option>
                                <option <?php if($data_jobs['Jobs_salary_max'] == "60"){ echo "selected=selected"; }?> value="60">60 Juta</option>
                                <option <?php if($data_jobs['Jobs_salary_max'] == "75"){ echo "selected=selected"; }?> value="75">75 Juta</option>
                            </select>
                        </div>
                    </div>
                    <div class="row-fluid up1">
                        <div class="span4"><strong>Tags *:</strong></div>
                        <div class="span8">
                            <input id="refTag" name="tag" class="tags" value="<?php echo $data_tags;?>"/>
                        </div>
                    </div>
                    <div class="row-fluid up1">
                        <div class="span4 modal-label">Jobs Publish :</div>
                        <div class="span8">
                            <select name="publish" class="text">
                                <option <?php if($data_jobs['Jobs_publish'] == "Publish"){ echo "selected=selected"; }?> value="Publish">Publish</option>
                                <option <?php if($data_jobs['Jobs_publish'] == "Not Publish"){ echo "selected=selected"; }?> value="Not Publish">Not Publish</option>
                            </select>
                        </div>
                    </div>
                    <div class="row-fluid up1">
                        <div class="span4 modal-label">Jobs Status :</div>
                        <div class="span8">
                            <select name="status" class="text">
                                <option <?php if($data_jobs['Jobs_status'] == "Open"){ echo "selected=selected"; }?> value="Open">Open</option>
                                <option <?php if($data_jobs['Jobs_status'] == "Close"){ echo "selected=selected"; }?> value="Close">Close</option>
                            </select>
                        </div>
                    </div>
                    <div class="row-fluid">
                        <div id="SubStatus" class="span12 align-center"></div>
                    </div>
                    <div class="row-fluid">
                    <div class="span12">
                    <div class="well" align="center">
                        <input name="id" type="hidden" value="<?php echo $O_id;?>"/>
                        <input name="tags_input" type="hidden" id="data-tags" value=""/>
                        <div class="btn-group" align="center">
                            <a href="adminMgr-Jobs.php" class="btn btn-danger" style="color: white;"><i class="icon-remove icon-white"></i> Cancel</a>
                            <button id="btn-submit" name="saveJobs" onmouseover="submit_tags();" type="submit" class="btn btn-success"><i class="icon-ok icon-white"></i> Save Jobs</button>
                        </div>
                    </div>
                    </div>
                    </div>
                </form>
                <?php $k++; }?>
            </div>
        </div>
        <div class="row-fluid">
            <div class="span6">
                <?php require_once("admin-Footer.php"); ?>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function () {    
            $("#selectMenu").bind("change", function () {
                if ($(this).val() == "negotiable") {
                    $("#showMax").slideDown();
                    $('#text-min').text("Salary Min");
                }
                else{
                    $("#showMax").slideUp();
                    $('#text-min').text("Salary");
                }
            });
        });
    </script>
    <script type="text/javascript">
        $("#message1").fadeOut(8400);
        function add_zindex(){
            $('.ui-autocomplete').css("z-index", 1055);
        }

        function submit_tags(){
                    var num = $('.tag > span').length;
                    var tags = new Array();
                    var data_tags = "";
                    for(var i =0; i < num; i++){
                        var text = $('.tag > span:eq('+i+')').text();
                        tags[i] = text.replace(/^\s\s*/, '').replace(/\s\s*$/, '');
                        console.log(tags[i]);
                        if(data_tags != ''){
                            data_tags += ", ";
                        }
                        data_tags += tags[i];
                    }
                    $('#data-tags').val(data_tags);
                }


        $("#message1").fadeOut(8400);  

        $(document).ready(function(){
            
            $('#refTag').tagsInput({
                        'width':'auto',
                        'autocomplete_url' : '<?=$api["tag-autocomplete"];?>',
                        'autocomplete':{selectFirst:true,width:'80px',autoFill:true}
                    });
            // $('#refTag').importTags(<?=$data_tags;?>);
        });
    </script>

    <script src="js/globalJS.js" type="text/javascript"></script>
</body></html>