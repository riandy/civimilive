<?php
ob_start("ob_gzhandler");
require_once("packages/require.php");
include("packages/check_login.php");//USED BY ALL PAGE BUT index.php
include("controller/controller_user_member.php");
$page_name = "adminMgr-User_member.php";
$letter_page = "letter=$letter";
$name_page = "name=$O_name";
$username_page = "username=$O_username";
$profession_page = "profession=$O_profession";
$year_page = "year=$O_year";
$city_page = "city=$O_city";
$country_page = "country=$O_country";
$sort_type_page = "sort_type=$O_sortType";
$sort_order_page = "sort_order=$O_sortOrder";
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <?php include('packages/head.php');?>
    <!-- calendar -->
    <link rel="stylesheet" type="text/css" href="packages/jQuery-Tags/jquery-ui.css" />
    <link rel="stylesheet" type="text/css" href="packages/jQuery-Tags/jquery.tagsinput.css" />
    <script src="//code.jquery.com/jquery-1.10.2.js"></script>

    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.0/themes/smoothness/jquery-ui.css">
    <script src="//code.jquery.com/ui/1.11.1/jquery-ui.js"></script>
    <script src="js/jquery.select-to-autocomplete.js"></script>

    <script>
        $(function() {
            $( ".datepicker" ).datepicker({dateFormat: 'yy-mm-dd' });
            $('.autodrop').selectToAutocomplete();
        });
    </script>
    <style>
        body .modal {
            width: 50%; /* desired relative width */
            left: 25%; /* (100%-width)/2 */
            /* place center */
            margin-left:auto;
            margin-right:auto; 
        }
    </style>
</head>
<body class="civMain" onload="startTime()">
    <?php require_once("admin-Header.php");?>

    <!-- start top nav -->
    <?php $curPage='module'; ?>
    <?php require_once("admin-top_nav.php");?>

    <div class="container">
        <div id="menuDashboard" class="row-fluid">
            <div id="rightContent" class="span12">
                <?php
                if($message!=null)
                {
                    echo "<div id='message1' class='alert alert-info'>" . $message . "</div><br />";
                }
                ?>
                <div class="row-fluid">
                    <div class="span6 align-left">
                        <h4 style="margin: 0;">User Management</h4>
                    </div>
                    <div class="span6 align-right">
                        <!--<button class="btn btn-info" type="button" data-toggle="modal" data-target="#modalSort"><i class="icon-search icon-white"></i> Sort By</button>!-->
                        <button class="btn btn-info" type="button" data-toggle="modal" data-target="#modalFilter"><i class="icon-search icon-white"></i> Advance Search</button>
                    </div>
                </div>
                <hr/>
                <div class="row-fluid">
                    <div class="span8">
                        <div class="input-append">
                            <select id="auto_search" class="autodrop"  > 
                            <option value=''>start typing...</option>
                                <?php foreach($data_lists as $data_list){ ?>
                                    <option value="<?php echo $data_list['User_ID'];?>"><?php echo $data_list['User_fname']." ".$data_list['User_lname'].", ".$data_list['User_email'];?></option>
                                    <?php } ?>
                            </select>
                            <!-- <span>please type what you are looking</span> -->
                            <a onclick="goDetail();" class="btn btn-info" style="color: white;">Search</a>
                        </div>
                        <script>
                        function goDetail(){
                            var detail = $('#auto_search').val();
                            if(detail > 0 && isNaN(detail) == false){
                                location.href = "<?=$admin['path-user_detail'];?>"+detail;
                            }
                        }
                        </script>
                    </div>
                    
                    <div class="span4 align-right">
                        Total Data starts with <span class="badge badge-important"><?php if($letter == ''){ echo "A-Z"; }else{ echo strtoupper($letter);}?></span> : <span class="badge badge-info"><?php echo $total_data;?></span><br/>
                        Showing : <span class="badge badge-info"><?php echo (($O_page-1)*20+1)." - ".(count($data_users)+(($O_page-1)*20));?></span> of <span class="badge badge-info"><?php echo $total_data;?></span> 
                    </div>
                </div>
                <hr/>
                <!-- STARTS LETTER -->
                <div class="row-fluid">
                    <div class="span12 align-center">
                        <?php for ($i = 'a', $j = 1; $j <= 26; $i++, $j++) {
                                $letters[$j] = $i;
                    ?>
                    <a href="<?php echo "adminMgr-User_member.php?page=1&letter=$letters[$j]&$name_page&$username_page&$profession_page&$year_page&$city_page&$country_page&$sort_type_page&$sort_order_page";?>" class="btn-mini <?php  if($letter == $letters[$j] ){echo 'active-alphabet';} ?>" ><b><?php echo strtoupper($letters[$j]);?></b></a>&nbsp;
                    <?php } ?>
                    </div>
                </div><!-- ENDS LETTER -->
                <hr>
                <table class="table table-hover up2">
                <thead >
                    <tr>
			            <th>#</th>
                        <th>Picture</th>
                        <th>Name</th>
                        <th>Username</th>
                        <th>Age</th>
                        <th>Join Date</th>
                        <th>Personal %</th>
                        <th># Edu</th>
                        <th># Work</th>
                        <th style="text-align:center;width:70px !important;">Action</th>
                    </tr>
                </thead>
                <?php
                if(!is_array($data_users)){
                    echo "
                    <tbody><td colspan='10'><h5>There is no data.</h5></td></tbody>";
                }else{?>
                <tbody>
                <?php $k=1;
                    foreach($data_users as $data_user){
                    if($O_type == "publish"){
                        $onclickDel = "\"if(window.confirm('Are you sure you want to set unpublish {$data_user['User_fname']} {$data_user['User_lname']}, {$data_user['User_email']}?')) location.href='adminMgr-User_member.php?action=unpublish&user_ID={$data_user['User_ID']}&user_Name={$data_user['User_fname']}';\"";
                        $view = "Set Unpublish";
                    }else{
                        $onclickDel = "\"if(window.confirm('Are you sure you want to set publish {$data_user['User_fname']} {$data_user['User_lname']}, {$data_user['User_email']}?')) location.href='adminMgr-User_member.php?action=publish&user_ID={$data_user['User_ID']}&user_Name={$data_user['User_fname']}';\"";
                        $view = "Set Publish";
                    }
                    ?>
                    <tr class="<?php if($data_user['User_actStatus'] == '0'){echo 'error';}?>">
	                    <td><?php echo ($O_page-1)*20+$k;?></td>
                        <td><a class="proPhoto" href="<?php echo check_image_url($data_user['User_proPhoto'], 'user');?>"><img class="img-circle" src="<?php echo check_image_url($data_user['User_proPhotoThmb'], 'user');?>" /></a></td>
                        <td><?php echo $data_user['User_fname']." ".$data_user['User_lname'];?></td>
                        <td><?php echo $data_user['User_username'];?></td>
                        <td><?php if($data_user['User_DOB'] != ""){echo calculate_age($data_user['User_DOBday']."-".$data_user['User_DOBmonth']."-".$data_user['User_DOB']);}else{echo "-";}?></td>
                        <td><?php echo relative_time($data_user['User_create_date']);?></td>
                        <td><?php echo $data_user['ATpro_personal'];?></td>
                        <td><?php echo $data_user['edu_counter'];?></td>
                        <td><?php echo $data_user['work_counter'];?></td>
                        <td style="text-align: center;">
                            <div class="btn-group-vertical">
                                <div class="btn-group">
                                  <a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
                                    <i class=" icon-cog"></i>
                                  </a>
                                  <ul class="dropdown-menu align-left">
                                    <li><a target="_blank" href="<?php echo $global['user-profile'].$data_user['User_username'];?>.cvm"><i class="icon-user"></i> View Profile</a></li>
                                    <li><a href="adminMgr-User_member_detail.php?user_ID=<?php echo $data_user['User_ID']."&type=".$O_type;?>"><i class=" icon-edit"></i> Edit Personal</a></li>
                                    <li><a href="#" <? echo "onclick=$onclickDel";?><?php if($k >= 1 && $total_data >= 1){echo " disabled";}?>><i class=" icon-remove"></i> <?php echo $view;?></a></li>
                                  </ul>
                                </div>
                            </div>
                        </td>
                    </tr>
                <?php $k++; }?>
                </tbody>
                <? }?>
                </table>
                <br/>
                <!-- START PAGINATION USER !-->
                <div class="pagination align-center">
                    <ul class="pagination pagination-sm">
                    <?php
                        $batch = getBatch($O_page);
                        if($batch < 1){$batch = 1;}
                        $prevLimit = 1 +(10*($batch-1));
                        $nextLimit = 10 * $batch;
                        if($nextLimit > $total_page){$nextLimit = $total_page;}
                        if ($total_page > 1 && $O_page > 1) {
                            echo "<li><a href='".$page_name."?page=1&".$letter_page."&".$name_page."&".$username_page."&".$profession_page."&".$year_page."&".$city_page."&".$country_page."&".$sort_type_page."&".$sort_order_page."' class='pagination-slide'>&lt;&lt; First</a></li>";
                        }
                        if($batch > 1 && $O_page > 10){
                            echo "<li><a href='".$page_name."?page=".($prevLimit-1)."&".$letter_page."&".$name_page."&".$username_page."&".$profession_page."&".$year_page."&".$city_page."&".$country_page."&".$sort_type_page."&".$sort_order_page."' class='pagination-slide'>&lt; Previous 10</a></li>";
                        }
                        for($mon = $prevLimit; $mon <= $nextLimit;$mon++){?>
                        <li class="<?php if($mon == $O_page){echo 'active';}?>"><span class="pagination-theme"><a href="<?php echo $page_name."?page=".$mon."&".$letter_page."&".$name_page."&".$username_page."&".$profession_page."&".$year_page."&".$city_page."&".$country_page."&".$sort_type_page."&".$sort_order_page;?>" ><?php echo $mon;?></a></span></li>
                        <?php if($mon == $nextLimit && $mon < $total_page){
                            if($mon >= $total_page){ $mon -=1;}
                            echo "<li><a href='".$page_name."?page=".($mon+1)."&".$letter_page."&".$name_page."&".$username_page."&".$profession_page."&".$year_page."&".$city_page."&".$country_page."&".$sort_type_page."&".$sort_order_page."' class='pagination-slide'>Next 10 &gt;</a></li>";
                        }                               }
                        if ($total_page > 1 &&  ($O_page != $total_page)) {
                            echo "<li><a href='".$page_name."?page=".$total_page."&".$letter_page."&".$name_page."&".$username_page."&".$profession_page."&".$year_page."&".$city_page."&".$country_page."&".$sort_type_page."&".$sort_order_page."' class='pagination-slide'>Last &gt;&gt;</a></li>";
                        } 
                    ?>
                    </ul>
                </div>
                <!-- END PAGINATION USER !-->
            </div>
        </div>
        <div class="row-fluid">
            <div class="span6">
                <?php require_once("admin-Footer.php"); ?>
            </div>
        </div>
    </div>

    <!--Modal box filter starts -->  
    <div id="modalFilter" class="modal hide fade">    
        <form name="filterUser" action="adminMgr-User_member.php" method="get" onsubmit="loadingText()" onclick="add_zindex();">         
            <div class="modal-header">
                <a href="#close" class="close" data-dismiss="modal" aria-hidden="true">×</a>
                <strong>Advance Search User</strong>
            </div>
            <div class="modal-body">
                <div class="row-fluid">
                    <div class="span4 align-right modal-label">Name :</div>
                    <div class="span6"><input name="name" type="text" class="text input-block-level" placeholder="search keyword by name" value="<?php echo $O_name;?>" /></div>
                </div> 
                <div class="row-fluid">
                    <div class="span4 align-right modal-label">Username :</div>
                    <div class="span6"><input name="username" type="text" class="text input-block-level" placeholder="search keyword by username" value="<?php echo $O_username;?>" /></div>
                </div> 
                <div class="row-fluid">
                    <div class="span4 align-right modal-label">Profession :</div>
                    <div class="span6"><input name="profession" type="text" class="text input-block-level" placeholder="search keyword by profession" value="<?php echo $O_profession;?>" /></div>
                </div> 
                <div class="row-fluid">
                    <div class="span4 align-right modal-label">Age :</div>
                    <div class="span6">
                        <select name="year" class="text">
                            <option value="">Select Age</option>
                            <?php for($i=date('Y'); $i>=1955; $i--){ $age = date('Y')-$i;?>
                            <option <?php if($O_year == $i){ echo "selected=selected"; }?> value="<?php echo $i;?>"><?php echo $age." years old";?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="row-fluid">
                    <div class="span4 align-right modal-label">City :</div>
                    <div class="span6">
                        <select name="city" class="text">
                            <option value="">Select City</option>
                            <?php foreach($citys as $city){ ?>
                            <option <?php if($O_city == $city['City_title']){ echo "selected=selected"; }?> value="<?php echo $city['City_title'];?>"><?php echo $city['City_title'];?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="row-fluid">
                    <div class="span4 align-right modal-label">Country :</div>
                    <div class="span6">
                        <select name="country" class="text">
                            <option value="">Select Country</option>
                            <?php foreach($countrys as $country){ ?>
                            <option <?php if($O_country == $country['Country_title']){ echo "selected=selected"; }?> value="<?php echo $country['Country_title'];?>"><?php echo $country['Country_title'];?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
            </div>
            <div class="modal-header">
                <strong>Sorting By</strong>
            </div>
            <div class="modal-body">
                <div class="row-fluid">
                    <div class="span4 align-right modal-label">Sort Type :</div>
                    <div class="span6">
                        <select name="sort_type" class="text">
                            <option <?php if($O_sortType == "join"){ echo "selected=selected"; }?> value="join">Join Date</option>
                            <option <?php if($O_sortType == "edu"){ echo "selected=selected"; }?> value="edu">Education</option>
                            <option <?php if($O_sortType == "personal"){ echo "selected=selected"; }?> value="personal">Personal %</option>
                            <option <?php if($O_sortType == "work"){ echo "selected=selected"; }?> value="work">Work</option>
                        </select>
                    </div>
                </div>
                <div class="row-fluid">
                    <div class="span4 align-right modal-label">Sort Order :</div>
                    <div class="span6">
                        <select name="sort_order" class="text">
                            <option <?php if($O_sortOrder == "asc"){ echo "selected=selected"; }?> value="asc">Ascending</option>
                            <option <?php if($O_sortOrder == "desc"){ echo "selected=selected"; }?> value="desc">Descending</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="btn-group">
                    <a href="#" class="btn" data-dismiss="modal"><i class="icon-remove"></i> Close</a>
                     <button id="btn-submit" type="submit" class="btn btn-success btn-submit"><i class="icon-ok icon-white"></i> Search</button>
                </div>
            </div>
        </form>
    </div><!--Modal box filter ends -->
    <script type="text/javascript">
        $("#message1").fadeOut(8400);
        function add_zindex(){
            $('.ui-autocomplete').css("z-index", 1055);
        }
    </script>
    <script src="js/globalJS.js" type="text/javascript"></script>
</body></html>