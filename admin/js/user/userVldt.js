function validateFormOnSubmit(theForm) {
    if (theForm == "addUser")
    {
        theForm = document.addUser;
    }
    var reason = "";
    var reason2 = "";
    var reason3 = "";
    var statusDiv = document.getElementById('SubStatus');
    reason += validateInput(theForm.elements[0]);
    reason += validateInput(theForm.elements[1]);
    reason += validateInput(theForm.elements[2]);
    reason += validateInput(theForm.elements[3]);
    
    reason2 += validatePassword(theForm.elements[2],theForm.elements[3]);
    
    reason3 += validateLength(theForm.elements[0], 4);
    reason3 += validateLength(theForm.elements[1], 4);
    reason3 += validateLength(theForm.elements[2], 4);
    reason3 += validateLength(theForm.elements[3], 4);

  if (reason != "" || reason2 != "" || reason3 !="") {
    var messageAll = '';
    messageAll += reason2;
    if(reason != "")
        {messageAll += "<font color='#FF0000'>All fields with (*) asterisks must be filled out <br /></font>";}
        
    if(reason3 != "")
        {messageAll += "<font color='#FF0000'>The data is too short (min. 5 chars)<br /></font>";}
        
        statusDiv.innerHTML = messageAll;
        messageAll = '';
    return false;
    }
    else if (reason == null && reason2 == null && reason3 == null)
    {
        alert("Success");
        return false;
        //return true;
    }
}

function validateInput(fld) {
    var error = "";
    if (fld.value == null || fld.value == "" || fld.value == " ") {
        fld.style.background = 'Yellow'; 
        error = ".";
    } else {
        fld.style.background = 'White';
    } 
    return error;
}

function validatePassword(pass1, pass2) {
    var message = "";
    if (pass1.value != pass2.value) { 
        pass1.style.background = 'Red';
        pass2.style.background = 'Red';
        message = "<font color='#FF0000'>Please reconfirm password<br /></font>";
    }
    return message;
}

function validateLength(data, len) {
    var message = "";
    var dataVal= data.value;
    var dataLen = dataVal.length;
    if (dataLen <= len) {
        data.style.background = 'Yellow';
        message = ".";
    }
    return message;
}