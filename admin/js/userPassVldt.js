function validateFormOnSubmit(theForm) {
    if (theForm == "changePass")
    {
        theForm = document.changePass;
    }

    var reason = "";

    var statusDiv2 = document.getElementById('SubStatus2');
   
    reason += validateInput(byName('old_password'));
    reason += validateInput(byName('new_password_1'));
    reason += validateInput(byName('new_password_2'));
    
    
        var oldpass1 = document.getElementById("oldpass1").value;
        var oldpass2 = document.getElementById("oldpass2").value;
        var newpass1 = document.getElementById("newpass1").value;
        var newpass2 = document.getElementById("newpass2").value;
        if (oldpass1 != oldpass2) {
            statusDiv2.innerHTML = "<font color='#FF0000'>Old Passwords not correct<br /></font>";
        return false;
        }
        else if (newpass1 != newpass2) {
            statusDiv2.innerHTML = "<font color='#FF0000'>New Passwords Do not match<br /></font>";
        return false;
        }
        else {
        return true;
        }
        
        

}




function validateInput(fld) {

    var error = "";

    if (fld.value == null || fld.value == "") {

        fld.style.background = 'Yellow'; 

        error = ".";

    } else {

        fld.style.background = 'White';

    } 

    return error;

}


function byName(name){

    var nameSelector = document.getElementsByName(name)[0];

    return nameSelector;

}