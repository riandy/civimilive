<?php
ob_start("ob_gzhandler");
require_once("packages/require.php");
include("packages/check_login.php");//USED BY ALL PAGE BUT index.php
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <?php include('packages/head.php');?>
</head>
<body class="civMain" onload="startTime()">
    <?php require_once("admin-Header.php");?>

    <!-- start top nav -->
    <?php $curPage='home'; ?>
    <?php require_once("admin-top_nav.php");?>
    <!-- end top nav -->

    <div class="container">
        <div id="menuDashboard" class="row-fluid">
            <div id="rightContent" class="span12" style="height:350px;">
		          <div class="row-fluid">
                    <div class="span8 align-left">
                        <h4 style="margin: 0;">Home</h4>
                    </div>
                </div>
                <hr />
        		<div class="hero-unit">
                  <h2>Welcome, <?php echo $_SESSION['admin_username'];?></h2>
                </div>
            </div>
        </div>
        <div class="row-fluid">
            <div class="span6">
                <?php require_once("admin-Footer.php"); ?>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $("#message1").fadeOut(8400);
    </script>
    <script src="js/globalJS.js" type="text/javascript"></script>
</body></html>