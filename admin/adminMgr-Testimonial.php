<?php
ob_start("ob_gzhandler");
require_once("packages/require.php");
include("packages/check_login.php");//USED BY ALL PAGE BUT index.php
include("controller/controller_testimonial.php");
$page_name = "adminMgr-Testimonial.php";
$type_page = "type=$O_type";
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <?php include('packages/head.php');?>
    <!-- calendar -->
    <link rel="stylesheet" type="text/css" href="packages/jQuery-Tags/jquery-ui.css" />
    <link rel="stylesheet" type="text/css" href="packages/jQuery-Tags/jquery.tagsinput.css" />
    <script src="//code.jquery.com/jquery-1.10.2.js"></script>
    <script type="text/javascript" src="packages/jQuery-Tags/jquery-ui.js"></script>
    <script type="text/javascript" src="packages/jQuery-Tags/jquery.tagsinput.js"></script>
    <script src="js/tinymce/tinymce.min.js"></script>

    <script>
      $(function() {
        $( ".datepicker" ).datepicker({dateFormat: 'yy-mm-dd' });
      });
    </script>
    <style>
        body .modal {
            width: 50%; /* desired relative width */
            left: 25%; /* (100%-width)/2 */
            /* place center */
            margin-left:auto;
            margin-right:auto; 
        }
    </style>
</head>
<body class="civMain">
    <?php require_once("admin-Header.php");?>

    <!-- start top nav -->
    <?php $curPage='testimonial'; ?>
    <?php require_once("admin-top_nav.php");?>
    <!-- end top nav -->

    <div class="container">
        <div id="menuDashboard" class="row-fluid">
            <div id="rightContent" class="span12">
                
                <?php
                if($message!=null)
                {
                    echo "<div id='message1' class='alert alert-info'>" . $message . "</div><br />";
                }
                ?>
                <div class="row-fluid">
                    <div class="span12 align-center">
                        <h4 style="margin: 0;">Testimonial Management</h4>
                    </div>
                </div>
                <hr>
                <div class="row-fluid">
                    <div class="span6">
                        <select name="type" class="text" onChange="window.location.href=this.options[this.selectedIndex].value">
                            <option <?php if($O_type == "new"){echo "selected";}?> value="adminMgr-Testimonial.php?page=1&type=new">New</option>
                            <option <?php if($O_type == "approved"){echo "selected";}?> value="adminMgr-Testimonial.php?page=1&type=approved">Approved</option>
                        </select>
                    </div>
                    <div class="span6 align-right">
                        Total Testimonial : <span class="badge badge-info"><?php echo $total_data;?></span><br/>
                        Showing : <span class="badge badge-info"><?php echo (($O_page-1)*20+1)." - ".(count($datas)+(($O_page-1)*20));?></span> of <span class="badge badge-info"><?php echo $total_data;?></span> 
                    </div>
                </div>
                <hr/>
                <br>
                <table class="table table-hover">
                    <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>Featured</th>
                        <th>Content</th>
                        <th>Create Date</th>
                        <th style="text-align:center;width:70px !important;">Action</th>
                    </tr>
                <?php
                if(!is_array($datas)){
                    echo "
                    <tbody><td colspan='8'><h5>There is no testimonial.</h5></td></tbody>";
                }else{?>
                <?php $k=1;
                    foreach($datas as $data){
                        $onclickDel = "\"if(window.confirm('Are sure want to delete testimonial from {$data['User_fname']}?')) location.href='adminMgr-Testimonial.php?action=delete&testimonial_ID={$data['Testimonial_ID']}&page={$O_page}&type={$O_type}&testimonial_Title=".urlencode($data['Testimonial_content'])."';\"";
                    ?>
                    <tr class="<?php if($data['Testimonial_publish'] == 'Not Publish'){echo 'error';}?>">
                        <td><?php echo ($O_page-1)*20+$k;?></td>
                        <td class="hide" id="src-testimonial-<?=$datas[$k-1]['Testimonial_ID'];?>" data-testimonial-name="<?=$datas[$k-1]['User_fname']." ".$data[$k-1]['User_lname'];?>" data-testimonial-featured="<?=$datas[$k-1]['Testimonial_featured'];?>" data-testimonial-publish="<?=$datas[$k-1]['Testimonial_publish'];?>" data-testimonial-content="<?=$datas[$k-1]['Testimonial_content'];?>" data-testimonial-date="<?=$datas[$k-1]['Testimonial_create_date'];?>"></td>
                        <td class="testimonial_name<?php echo $k;?>"><?php echo $data['User_fname'];?></td>
                        <td class="testimonial_featured<?php echo $k;?>"><?php echo $data['Testimonial_featured'];?></td>
                        <td><?php echo charLength(correctDisplay($data['Testimonial_content']), 100);?></td>
                        <td class="testimonial_create<?php echo $k;?>"><?php echo date("d M Y, h:i:s", strtotime($data['Testimonial_create_date']));?></td>
                        <td class="hide testimonial_content<?php echo $k;?>"><?php echo correctDisplay($data['Testimonial_content']);?></td>
                        <td class="hide testimonial_id<?php echo $k;?>"><?php echo $data['Testimonial_ID'];?></td>
                        <td style="text-align: center;">
                            <div class="btn-group btn-group-horizontal">   
                                <a href="#" type="button" class="btn btn-small" data-toggle="modal" data-target="#modal-editTestimonial" onclick="return copyValue(<?php echo $data['Testimonial_ID'];?>);"><i class="icon-edit"></i> Edit</a>
                                <a href="#" <?php echo "onclick=$onclickDel"; ?> type="button" class="btn btn-danger btn-small" style="color: white;"><i class="icon-trash icon-white"></i> Delete</a>
                            </div>
                        </td>
                    </tr>
                <?php $k++; }?>
                <? }?>
                </table>
                <br/>
                <!-- start pagination !-->
                <div class="pagination align-center">
                  <ul class="pagination pagination-sm">
                    <?php
                    $batch = getBatch($O_page);
                    if($batch < 1){$batch = 1;}
                    $prevLimit = 1 +(10*($batch-1));
                    $nextLimit = 10 * $batch;

                    if($nextLimit > $total_page){$nextLimit = $total_page;}
                    if ($total_page > 1 && $O_page > 1) {
                        echo "<li><a href='".$page_name."?page=1&".$type_page."' class='pagination-slide'>&lt;&lt; First</a></li>";
                    }
                    if($batch > 1 && $O_page > 10){
                        echo "<li><a href='".$page_name."?page=".($prevLimit-1)."&".$type_page."' class='pagination-slide'>&lt; Previous 10</a></li>";
                    }
                    for($mon = $prevLimit; $mon <= $nextLimit;$mon++){?>
                    <li class="<?php if($mon == $O_page){echo 'active';}?>"><span class="pagination-theme"><a href="<?php echo $page_name."?page=".$mon."&".$type_page;?>" ><?php echo $mon;?></a></span></li>
                    <?php if($mon == $nextLimit && $mon < $total_page){if($mon >= $total_page){ $mon -=1;}
                    echo "<li><a href='".$page_name."?page=".($mon+1)."&".$type_page."' class='pagination-slide'>Next 10 &gt;</a></li>";
                    }                               }
                    if ($total_page > 1 &&  ($O_page != $total_page)) {
                        echo "<li><a href='".$page_name."?page=".$total_page."&".$type_page."' class='pagination-slide'>Last &gt;&gt;</a></li>";
                    }
                    ?>
                  </ul>
                </div>
                <!-- end pagination !-->
            </div>
        </div>
        <div class="row-fluid">
            <div class="span6">
                <?php require_once("admin-Footer.php"); ?>
            </div>
        </div>
    </div>
    <!--Modal box starts -->  
    <div id="modal-editTestimonial" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">    
        <form name="editTestimonial" action="adminMgr-Testimonial.php?action=update_testimonial" method="post" enctype="multipart/form-data" onsubmit="loadingText()">         
            <div class="modal-header">
                <a href="#close" class="close" data-dismiss="modal" aria-hidden="true">×</a>
                <span class="badge badge-info">Edit Testimonial</span>
            </div>
            <div class="modal-body">
                <div class="row-fluid">
                    <div class="span12 modal-label"><strong>Content&nbsp;*</strong>:</div>
                </div>
                <div class="row-fluid">
                    <div class="span12"><textarea id="testimonial_content" name="content" type="text" class="text input-block-level" required="required" placeholder="Description your testimonial" rows="5"></textarea></div>
                </div>
                <div class="row-fluid">
                    <div class="span12 modal-label">Create Date :</div>
                </div>
                <div class="row-fluid">
                    <div class="span12"><input id="testimonial_create_date" name="create" type="text" class="text uneditable-input" required="required" placeholder="date" disabled="true" /></div>
                </div>
                <div class="row-fluid">
                    <div class="span12 modal-label">Featured :</div>
                </div>
                <div class="row-fluid">
                    <div class="span12">
                     <select id="testimonial_featured" name="featured" class="text">
                        <option value="yes">Yes</option>
                        <option value="no">No</option>
                    </select>
                </div>
                <div class="row-fluid">
                    <div class="span12 modal-label">Publish :</div>
                </div>
                <div class="row-fluid">
                    <div class="span12">
                     <select id="testimonial_publish" name="publish" class="text">
                        <option value="Publish">Publish</option>
                        <option value="Not Publish">Not Publish</option>
                    </select>
                </div>
                <div class="row-fluid">
                    <div class="span12"><span class="modal-label">Testimonial By : </span><span id="create-by"></span></div>
                </div>
                <div class="row-fluid">
                    <div id="SubStatus" class="span12 align-center"></div>
                    <input id="testimonial_id" name="testimonial_id" type="hidden" value=""/>
                    <input name="url" type="hidden" value="adminMgr-Testimonial.php?page=<?php echo $O_page."&type=".$O_type;?>"/>
                </div>
            </div>
            <div class="modal-footer">
                <div class="btn-group">
                    <a href="#" class="btn" data-dismiss="modal"><i class="icon-remove"></i> Cancel</a>
                     <button id="btn-submit" name="submitTestimonial" type="submit" class="btn btn-success btn-submit"><i class="icon-check icon-white"></i> Update</button>
                </div>
            </div>
        </form>
    </div><!--Modal box ends -->
    <script type="text/javascript">
        $("#message1").fadeOut(8400);

        function copyValue(id){
            var name = $('#src-testimonial-'+id).attr('data-testimonial-name');
            var featured = $('#src-testimonial-'+id).attr('data-testimonial-featured');
            var publish = $('#src-testimonial-'+id).attr('data-testimonial-publish');
            var content = $('#src-testimonial-'+id).attr('data-testimonial-content');
            var create_date = $('#src-testimonial-'+id).attr('data-testimonial-date');

            $('#testimonial_id').val(id);
            $('#testimonial_content').text(content);
            $('#testimonial_featured').val(featured);
            $('#testimonial_publish').val(publish);
            $('#testimonial_create_date').val(create_date);
            $('#create-by').text(name);
        }
    </script>
    <script src="js/globalJS.js" type="text/javascript"></script>
</body></html>