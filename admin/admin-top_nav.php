<div id="admin-navigation">
    <ul class="navigation-list">
        <li><a href="adminMgr-Country.php" class="top-nav <?php if($curPage=='home'){ echo 'active-nav';}?>">Sidebar</a></li>

        <!-- Home !-->
        <li><a href="<?=$admin['path-home'];?>" class="top-nav <?php if($curPage=='home'){ echo 'active-nav';}?>">Home</a></li>
        
        <!-- Blog !-->
        <li class="dropdown">
            <a href="#" data-toggle="dropdown" class="top-nav <?php if($curPage=='blog'){ echo 'active-nav';}?>">Blog <span class="caret caret-white"></span></a>
            <ul class="dropdown-menu dropdown-nav" role="menu" aria-labelledby="dLabel">
                <li><a tabindex="-1" href="adminMgr-News_category.php">Manage Category</a></li>
                <li><a tabindex="-1" href="adminMgr-News.php">Manage News</a></li>
            </ul>
        </li>

        <!-- Module !-->
        <li class="dropdown">
            <a href="#" data-toggle="dropdown" class="top-nav <?php if($curPage=='module'){ echo 'active-nav';}?>">Module <span class="caret caret-white"></span></a>
            <ul class="dropdown-menu dropdown-nav" role="menu" aria-labelledby="dLabel">
                <!-- Module Company !-->
                <?php if($_SESSION['company'] == "Yes"){ ?>
                <li class="dropdown-submenu">
                    <a tabindex="-1" href="#">Manage Company</a>
                    <ul class="dropdown-menu">
                        <li><a tabindex="-1" href="adminMgr-Company.php?type=publish">Publish</a></li>
                        <li><a tabindex="-1" href="adminMgr-Company.php?type=notpublish">Not Publish</a></li>
                    </ul>
                </li>
                <?php } ?>

                <!-- Module Company !-->
                <?php if($_SESSION['company'] == "Yes"){ ?>
                <li class="dropdown-submenu">
                    <a tabindex="-1" href="#">Manage Organization</a>
                    <ul class="dropdown-menu">
                        <li><a tabindex="-1" href="adminMgr-Organization.php?type=publish">Publish</a></li>
                        <li><a tabindex="-1" href="adminMgr-Organization.php?type=notpublish">Not Publish</a></li>
                    </ul>
                </li>
                <?php } ?>

                <!-- Module Education !-->
                <?php if($_SESSION['education'] == "Yes"){ ?>
                <li class="dropdown-submenu">
                    <a tabindex="-1" href="#">Manage Education</a>
                    <ul class="dropdown-menu">
                        <li><a tabindex="-1" href="adminMgr-Edu.php?type=publish">Publish</a></li>
                        <li><a tabindex="-1" href="adminMgr-Edu.php?type=notpublish">Not Publish</a></li>
                        <li><a tabindex="-1" href="adminMgr-Edu.php?type=new">Fresh Content</a></li>
                    </ul>
                </li>
                <?php } ?>

                <!-- Module Certificate !-->
                <?php if($_SESSION['certificate'] == "Yes"){ ?>
                <li><a tabindex="-1" href="adminMgr-Certificate.php">Manage Certificate</a></li>
                <?php } ?>

                <!-- Module Images !-->
                <?php if($_SESSION['admin'] == "Yes"){ ?>
                <li><a tabindex="-1" href="adminMgr-Image.php">Manage Images</a></li>
                <?php } ?>

                <!-- Module User Member !-->
                <?php if($_SESSION['user'] == "Yes"){ ?>
                <li><a tabindex="-1" href="adminMgr-User_member.php">Manage User</a></li>
                <?php } ?>
            </ul>
        </li>

        <!-- Growth !-->
        <?php if($_SESSION['user'] == "Yes"){ ?>
        <li><a href="adminMgr-Growth.php" class="top-nav <?php if($curPage=='growth'){ echo 'active-nav';}?>">Growth</a></li>
        <?php } ?>

        <!-- Message !-->
        <li><a href="adminMgr-Message.php" class="top-nav <?php if($curPage=='message'){ echo 'active-nav';}?>">Message</a></li>

        <!-- Testimonial !-->
        <li><a href="adminMgr-Testimonial.php" class="top-nav <?php if($curPage=='testimonial'){ echo 'active-nav';}?>">Testimonial</a></li>

        <!-- User Admin Setting !-->
        <?php if($_SESSION['setting'] == "Yes"){ ?>
        <li><a href="adminMgr-Admin_setting.php" class="top-nav <?php if($curPage=='setting'){ echo 'active-nav';}?>">Setting</a></li>
        <?php } ?>

        <!-- User Admin !-->
        <?php if($_SESSION['admin'] == "Yes"){ ?>
        <li class="dropdown">
            <a href="#" data-toggle="dropdown" class="top-nav <?php if($curPage=='admin'){ echo 'active-nav';}?>">Admin <span class="caret caret-white"></span></a>
            <ul class="dropdown-menu dropdown-nav" role="menu" aria-labelledby="dLabel">
                <li><a tabindex="-1" href="adminMgr-User.php">Manage Admin</a></li>
                <li><a tabindex="-1" href="adminMgr-User_detail.php?admin_id=<?=$_SESSION['admin_id'];?>">Edit Account</a></li>
                <li><a tabindex="-1" href="<?php echo $_SERVER['PHP_SELF'];?>?action=logout">Logout</a></li>
            </ul>
        </li>
        <?php } ?>
    </ul>
</div>