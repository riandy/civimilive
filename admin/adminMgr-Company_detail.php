<?php
ob_start("ob_gzhandler");
require_once("packages/require.php");
include("packages/check_login.php");//USED BY ALL PAGE BUT index.php
include("controller/controller_company_detail.php");
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <?php include('packages/head.php');?>
    <!-- calendar -->

    <link rel="stylesheet" type="text/css" href="packages/jQuery-Tags/jquery-ui.css" />
    <link rel="stylesheet" type="text/css" href="packages/jQuery-Tags/jquery.tagsinput.css" />
    <script src="//code.jquery.com/jquery-1.10.2.js"></script>
    <script type="text/javascript" src="packages/jQuery-Tags/jquery-ui.js"></script>
    <script type="text/javascript" src="packages/jQuery-Tags/jquery.tagsinput.js"></script>

    <script>
  $(function() {
    $( ".datepicker" ).datepicker({dateFormat: 'yy-mm-dd' });
  });
  </script>
    <style>
        body .modal {
            width: 50%; /* desired relative width */
            left: 25%; /* (100%-width)/2 */
            /* place center */
            margin-left:auto;
            margin-right:auto; 
        }
    </style>
</head>
<body class="civMain" onload="startTime()">
    <?php require_once("admin-Header.php");?>

    <!-- start top nav -->
    <?php $curPage='module'; ?>
    <?php require_once("admin-top_nav.php");?>
    <!-- end top nav -->

    <div class="container">
        <div id="menuDashboard" class="row-fluid">
            <div id="rightContent" class="span12">
                
                <?php
                if($message!=null)
                {
                    echo "<div class='alert alert-info'>" . $message . "</div><br />";
                }
                ?>
                <div class="row-fluid">
                    <div class="span3">
                        <a href="adminMgr-Company.php?type=<?php echo $O_type;?>" class="btn btn-default btn-small"><i class="icon-circle-arrow-left"></i> Back</a>
                    </div>
                    <div class="span6">
                        <h4 style="margin: 0;text-align:center;">Company Edit <br>(<?php echo correctDisplay($data_companys[0]['Company_title']);?>)</h4>
                    </div>
                </div>
                <hr/>
                <?php $k=0;
                foreach($data_companys as $data_company){?>
                <form name="addCompany" action="adminMgr-Company_detail.php?action=update" method="post" enctype="multipart/form-data" onsubmit="loadingText()">
                    <div class="row-fluid up2">
                        <div class="span4 align-right modal-label"><strong>Industry Title&nbsp;*</strong>:</div>
                        <div class="span5">
                            <select name="industry_id" class="text" required="required">
                                <option value="">Select Industry</option>
                                <?php foreach($data_industrys as $data_industry){ ?>
                                <option <?php if($data_company['Company_ref_industryID'] == $data_industry['Industry_ID']){ echo "selected=selected"; }?> value="<?php echo $data_industry['Industry_ID'];?>"><?php echo $data_industry['Industry_title'];?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="row-fluid">
                        <div class="span4 align-right modal-label"><strong>City Title&nbsp;*</strong>:</div>
                        <div class="span5">
                            <select name="city_id" class="text" required="required">
                                <option value="">Select City</option>
                                <?php foreach($data_citys as $data_city){ ?>
                                <option <?php if($data_company['Company_ref_cityID'] == $data_city['City_ID']){ echo "selected=selected"; }?> value="<?php echo $data_city['City_ID'];?>"><?php echo $data_city['City_title'];?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="row-fluid">
                        <div class="span4 align-right modal-label"><strong>Country Title&nbsp;*</strong>:</div>
                        <div class="span5">
                            <select name="country_id" class="text" required="required">
                                <option value="">Select Country</option>
                                <?php foreach($data_countrys as $data_country){ ?>
                                <option <?php if($data_company['Company_ref_countryID'] == $data_country['Country_ID']){ echo "selected=selected"; }?> value="<?php echo $data_country['Country_ID'];?>"><?php echo $data_country['Country_title'];?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="row-fluid up1">
                        <div class="span4 align-right modal-label"><strong>Company Title&nbsp;*</strong>:</div>
                        <div class="span5"><input name="title" type="text" class="text input-block-level" placeholder="Company Title" value="<?php echo correctDisplay($data_company['Company_title']);?>"></div>
                    </div>
                    <div class="row-fluid up1">
                        <div class="span4 align-right modal-label">Company Motto :</div>
                        <div class="span5"><textarea name="motto" class="input-block-level" wrap="soft" placeholder="description" rows="3"><?php echo correctDisplay($data_company['Company_motto']);?></textarea></div>
                    </div>
                    <div class="row-fluid up1">
                        <div class="span4 align-right modal-label">Company Description :</div>
                        <div class="span5"><textarea name="content" class="input-block-level" wrap="soft" placeholder="description" rows="5"><?php echo $data_company['Company_content'];?></textarea></div>
                    </div>
                    <div class="row-fluid up1">
                        <div class="span4 align-right modal-label">Company Address :</div>
                        <div class="span5"><textarea name="address" class="input-block-level" wrap="soft" placeholder="description" rows="3"><?php echo correctDisplay($data_company['Company_address']);?></textarea></div>
                    </div>
                    <div class="row-fluid up1">
                        <div class="span4 align-right modal-label">Company E-mail :</div>
                        <div class="span5"><input name="email" type="text" class="text input-block-level" placeholder="E-mail" value="<?php echo $data_company['Company_email'];?>"/></div>
                    </div>
                    <div class="row-fluid up1">
                        <div class="span4 align-right modal-label">Company Phone :</div>
                        <div class="span5"><input name="phone" type="text" class="text input-block-level" placeholder="Phone" value="<?php echo $data_company['Company_phone'];?>"/></div>
                    </div>
                    <div class="row-fluid up1">
                        <div class="span4 align-right modal-label">Company Image :</div>
                        <div class="span5">
                            <a class="proPhoto" href="<?php echo check_image_url($data_company['Company_img'], 'company');?>">
                                <img class="img-circle" src="<?php echo check_image_url($data_company['Company_img_thmb'], 'company');?>" class="companyAdminImg" />
                            </a>
                        </div>
                    </div>
                    <div class="row-fluid up1">
                        <div class="span5 offset4">
                            <br />
                            <input name="image" type="file" class="text" maxlength="128" placeholder="company image" />
                            <br />
                        </div>
                    </div>
                    <div class="row-fluid">
                        <div class="offset4 span5"><small>image format has to be jpg, jpeg, gif, png.</small></div>
                    </div>
                    <div class="row-fluid up1">
                        <div class="span4 align-right modal-label"><strong>Tags *:</strong></div>
                        <div class="span5">
                            <input id="refTag" name="tag" class="tags" value="<?php echo $data_tags;?>" required="required"/>
                        </div>
                    </div>
                    <div class="row-fluid up1">
                        <div class="span4 align-right modal-label">Company Publish :</div>
                        <div class="span5">
                            <select name="publish" class="text">
                                <option <?php if($data_company['Company_publish'] == "Publish"){ echo "selected=selected"; }?> value="Publish">Publish</option>
                                <option <?php if($data_company['Company_publish'] == "Not Publish"){ echo "selected=selected"; }?> value="Not Publish">Not Publish</option>
                            </select>
                        </div>
                    </div>
                    <div class="row-fluid">
                        <div id="SubStatus" class="span12 align-center"></div>
                    </div>
                    <div class="row-fluid">
                    <div class="span12">
                    <div class="well" align="center">
                        <input name="id" type="hidden" value="<?php echo $data_company['Company_ID'];?>"/>
                        <input name="tags_input" type="hidden" id="data-tags" value=""/>
                        <div class="btn-group" align="center">
                            <a href="adminMgr-Company.php?type=<?php echo $O_type;?>" class="btn btn-danger" style="color: white;"><i class="icon-remove icon-white"></i> Cancel</a>
                            <button id="btn-submit" name="saveCompany" onmouseover="submit_tags();" type="submit" class="btn btn-success"><i class="icon-check icon-white"></i> Save Company</button>
                        </div>
                    </div>
                    </div>
                    </div>
                </form>
                <?php $k++; }?>
            </div>
        </div>
        <div class="row-fluid">
            <div class="span6">
                <?php require_once("admin-Footer.php"); ?>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $("#message1").fadeOut(8400);
        function add_zindex(){
            $('.ui-autocomplete').css("z-index", 1055);
        }

        function submit_tags(){
                    var num = $('.tag > span').length;
                    var tags = new Array();
                    var data_tags = "";
                    for(var i =0; i < num; i++){
                        var text = $('.tag > span:eq('+i+')').text();
                        tags[i] = text.replace(/^\s\s*/, '').replace(/\s\s*$/, '');
                        console.log(tags[i]);
                        if(data_tags != ''){
                            data_tags += ", ";
                        }
                        data_tags += tags[i];
                    }
                    $('#data-tags').val(data_tags);
                }


        $("#message1").fadeOut(8400);  

        $(document).ready(function(){
            
            $('#refTag').tagsInput({
                        'width':'auto',
                        'autocomplete_url' : '<?=$api["tag-autocomplete"];?>',
                        'autocomplete':{selectFirst:true,width:'80px',autoFill:true}
                    });
        });
    </script>
    <script src="js/globalJS.js" type="text/javascript"></script>
</body></html>