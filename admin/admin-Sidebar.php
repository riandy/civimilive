<div id="leftNav">
    <span id="menu-title">Menu</span><br /><hr align="left" style="width: 96%; margin: 3% 1%;" />
        <!-- Home !-->
        <div <?php if($curPage=='home'){echo "class='leftButton'";} ?>><a href="adminMgr.php"><i class="icon-home"></i> Home</a></div>
	    <br>
        
        <h4>
            Blog:
        </h4>
        <!-- Module News !-->
        <div <?php if($curPage=='nc'){echo "class='leftButton'";} ?>><a href="adminMgr-News_category.php"><i class="icon-th-list"></i> Category</a></div>
        <div <?php if($curPage=='news'){echo "class='leftButton'";} ?>><a href="adminMgr-News.php"><i class="icon-inbox"></i> News</a></div>
        <br>

        <h4>
            Module:
        </h4>
        <!-- Module Company !-->
        <?php if($_SESSION['company'] == "Yes"){ ?>
        <div style="margin:0!important;"><a href="#company-list" data-toggle="collapse" onclick="dropCompany();"><i class="icon-screenshot"></i> Company <span id="company-arrow" style="margin: 0 0 0 5px;" class="icon-chevron-down"></span></a></div>
        <div id="company-list" class="accordion-body collapse" style="padding: 0px 0px 0px 30px;">
            <div><a href="adminMgr-Company.php?type=publish">Publish</a></div>
            <div><a href="adminMgr-Company.php?type=notpublish">Not Publish</a></div>
        </div>
        <?php } ?>

        <!-- Module Organization !-->
        <?php if($_SESSION['company'] == "Yes"){ ?>
        <div style="margin:0!important;"><a href="#organization-list" data-toggle="collapse" onclick="dropOrganization();"><i class="icon-bookmark"></i> Organization <span id="organization-arrow" style="margin: 0 0 0 5px;" class="icon-chevron-down"></span></a></div>
        <div id="organization-list" class="accordion-body collapse" style="padding: 0px 0px 0px 30px;">
            <div><a href="adminMgr-Organization.php?type=publish">Publish</a></div>
            <div><a href="adminMgr-Organization.php?type=notpublish">Not Publish</a></div>
        </div>
        <?php } ?>

        <!-- Module Industry !-->
        <?php if($_SESSION['industry'] == "Yes"){ ?>
        <div <?php if($curPage=='industry'){echo "class='leftButton'";} ?>><a href="adminMgr-Industry.php"><i class="icon-fire"></i> Industry</a></div>
        <?php } ?>

        <!-- Module Field !-->
        <?php if($_SESSION['field'] == "Yes"){ ?>
        <div <?php if($curPage=='field'){echo "class='leftButton'";} ?>><a href="adminMgr-Field.php"><i class="icon-book"></i> Field</a></div>
        <?php } ?>        

        <!-- Module Jobs !-->
        <?php if($_SESSION['jobs'] == "Yes"){ ?>
        <div <?php if($curPage=='jobs'){echo "class='leftButton'";} ?>><a href="adminMgr-Jobs.php"><i class="icon-briefcase"></i> Jobs</a></div>
	    <?php } ?>     

        <!-- Module User Member !-->
        <?php if($_SESSION['user'] == "Yes"){ ?>
        <div style="margin:0!important;"><a href="#user-member-list" data-toggle="collapse" onclick="dropUserMember();"><i class="icon-eye-open"></i> User Member <span id="user-member-arrow" style="margin: 0 0 0 5px;" class="icon-chevron-down"></span></a></div>
        <div id="user-member-list" class="accordion-body collapse" style="padding: 0px 0px 0px 30px;">
            <div><a href="adminMgr-User_member.php?type=publish">Publish</a></div>
            <div><a href="adminMgr-User_member.php?type=notpublish">Not Publish</a></div>
        </div>
        <?php } ?>

        <!-- Module Growth !-->
        <?php if($_SESSION['user'] == "Yes"){ ?>
        <div <?php if($curPage=='growth'){echo "class='leftButton'";} ?>><a href="adminMgr-Growth.php"><i class="icon-calendar"></i> Growth</a></div>
        <?php } ?>
    <div class="ref" style="margin:20px 0 0 30px;">
        <?php if($_SESSION['education'] == "Yes" || $_SESSION['certificate'] == "Yes" || $_SESSION['skill'] == "Yes" || $_SESSION['country'] == "Yes" || $_SESSION['city'] == "Yes" || $_SESSION['level'] == "Yes" || $_SESSION['tag'] == "Yes"){ ?>
		<h4>
			Reference:
		</h4>
        <?php } ?>

        <!-- Module Education !-->
        <?php if($_SESSION['education'] == "Yes"){ ?>
        <div style="margin:0!important;"><a href="#education-list" data-toggle="collapse" onclick="dropEducation();"><i class="icon-list-alt"></i> Education <span id="education-arrow" style="margin: 0 0 0 5px;" class="icon-chevron-down"></span></a></div>
        <div id="education-list" class="accordion-body collapse" style="padding: 0px 0px 0px 30px;">
            <div><a href="adminMgr-Edu.php?type=publish">Publish</a></div>
            <div><a href="adminMgr-Edu.php?type=notpublish">Not Publish</a></div>
            <div><a href="adminMgr-Edu.php?type=new">Fresh Content</a></div>
        </div>
		<?php } ?>

        <!-- Module Certificate !-->
        <?php if($_SESSION['certificate'] == "Yes"){ ?>
        <div <?php if($curPage=='cert'){echo "class='leftButton'";} ?>><a href="adminMgr-Certificate.php"><i class="icon-certificate"></i> Certificate</a></div>
        <?php } ?>

        <!-- Module Skill !-->
        <?php if($_SESSION['skill'] == "Yes"){ ?>
        <div <?php if($curPage=='skill'){echo "class='leftButton'";} ?>><a href="adminMgr-Skill.php"><i class="icon-thumbs-up"></i> Skill</a></div>
        <?php } ?>        
        <br/>

        <!-- Module Ref Country !-->
        <?php if($_SESSION['country'] == "Yes"){ ?>
        <div <?php if($curPage=='country'){echo "class='leftButton'";} ?>><a href="adminMgr-Country.php"><i class="icon-globe"></i> Country</a></div>
        <?php } ?>

        <!-- Module Ref City !-->
        <?php if($_SESSION['city'] == "Yes"){ ?>
        <div <?php if($curPage=='city'){echo "class='leftButton'";} ?>><a href="adminMgr-City.php"><i class=" icon-leaf"></i> City</a></div>
		<?php } ?>

        <!-- Module Ref Level !-->
        <?php if($_SESSION['level'] == "Yes"){ ?>
        <div <?php if($curPage=='level'){echo "class='leftButton'";} ?>><a href="adminMgr-Level.php"><i class="icon-star"></i> Level</a></div>
		<?php } ?>

        <!-- Module Ref Tag !-->
        <?php if($_SESSION['tag'] == "Yes"){ ?>
        <div <?php if($curPage=='tag'){echo "class='leftButton'";} ?>><a href="adminMgr-Tag.php"><i class="icon-plus-sign"></i> Tag</a></div>
	    <?php } ?>
    </div>
        <!-- Module Message !-->
        <div <?php if($curPage=='message'){echo "class='leftButton'";} ?>><a href="adminMgr-Message.php"><i class="icon-comment"></i> Message</a></div>
        
        <!-- Module User Setting !-->
        <?php if($_SESSION['setting'] == "Yes"){ ?>
        <div <?php if($curPage=='setting'){echo "class='leftButton'";} ?>><a href="adminMgr-Admin_setting.php"><i class="icon-move"></i> Setting</a></div>
        <?php } ?>
        
        <!-- Module User Admin !-->
        <?php if($_SESSION['admin'] == "Yes"){ ?>
        <div <?php if($curPage=='user'){echo "class='leftButton'";} ?>><a href="adminMgr-User.php"><i class="icon-user"></i> Users</a></div>
        <?php } ?>  

        <span style="font-size: 0.8em"><em>*Always save after making changes</em></span>
        <br /><br />
</div>

<script type="text/javascript">
function dropCompany () {
    if($("#company-list").hasClass('in')){
        $("#company-arrow").removeClass('icon-chevron-up');
        $("#company-arrow").addClass('icon-chevron-down');
    } else {
        $("#company-arrow").removeClass('icon-chevron-down');
        $("#company-arrow").addClass('icon-chevron-up');
    }
}

function dropOrganization () {
    if($("#organization-list").hasClass('in')){
        $("#organization-arrow").removeClass('icon-chevron-up');
        $("#organization-arrow").addClass('icon-chevron-down');
    } else {
        $("#organization-arrow").removeClass('icon-chevron-down');
        $("#organization-arrow").addClass('icon-chevron-up');
    }
}

function dropUserMember () {
    if($("#user-member-list").hasClass('in')){
        $("#user-member-arrow").removeClass('icon-chevron-up');
        $("#user-member-arrow").addClass('icon-chevron-down');
    } else {
        $("#user-member-arrow").removeClass('icon-chevron-down');
        $("#user-member-arrow").addClass('icon-chevron-up');
    }
}

function dropEducation () {
    if($("#education-list").hasClass('in')){
        $("#education-arrow").removeClass('icon-chevron-up');
        $("#education-arrow").addClass('icon-chevron-down');
    } else {
        $("#education-arrow").removeClass('icon-chevron-down');
        $("#education-arrow").addClass('icon-chevron-up');
    }
}
</script>