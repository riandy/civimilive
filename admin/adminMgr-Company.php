<?php
ob_start("ob_gzhandler");
require_once("packages/require.php");
include("packages/check_login.php");//USED BY ALL PAGE BUT index.php
include("controller/controller_company.php");
$page_name = "adminMgr-Company.php";
$letter_page = "letter=$letter";
$type_page = "type=$O_type";
$sortType_page = "sort_type=$O_sortType";
$sortOrder_page = "sort_order=$O_sortOrder";
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <?php include('packages/head.php');?>
    <!-- calendar -->
<link rel="stylesheet" type="text/css" href="packages/jQuery-Tags/jquery-ui.css" />
    <link rel="stylesheet" type="text/css" href="packages/jQuery-Tags/jquery.tagsinput.css" />
    <script src="//code.jquery.com/jquery-1.10.2.js"></script>
    <script type="text/javascript" src="packages/jQuery-Tags/jquery-ui.js"></script>
    <script type="text/javascript" src="packages/jQuery-Tags/jquery.tagsinput.js"></script>
    <script src="js/jquery.select-to-autocomplete.js"></script>
    <script>
  $(function() {
    $( ".datepicker" ).datepicker({dateFormat: 'yy-mm-dd' });
     $('.autodrop').selectToAutocomplete();
  });
  </script>
    <style>
        body .modal {
            width: 50%; /* desired relative width */
            left: 25%; /* (100%-width)/2 */
            /* place center */
            margin-left:auto;
            margin-right:auto; 
        }
    </style>
</head>
<body class="civMain" onload="startTime()">
    <?php require_once("admin-Header.php");?>

    <!-- start top nav -->
    <?php $curPage='module'; ?>
    <?php require_once("admin-top_nav.php");?>
    <!-- end top nav -->

    <div class="container">
        <div id="menuDashboard" class="row-fluid">
            <div id="rightContent" class="span12">
                
                <?php
                if($message!=null)
                {
                    echo "<div id='message1' class='alert alert-info'>" . $message . "</div><br />";
                }
                ?>
                <div class="row-fluid">
                    <div class="span6 align-left">
                        <h4 style="margin: 0;">Company (<?php echo $type;?>) Management</h4>
                    </div>
                    <div class="span6 align-right">
                        <button class="btn btn-info" type="button" data-toggle="modal" data-target="#modalSorting"><i class="icon-list-alt icon-white"></i> Sorting</button>
                        <button class="btn btn-info" type="button" data-toggle="modal" data-target="#modalCompany"><i class="icon-plus icon-white"></i> Company</button>
                    </div>
                </div>
                <hr/>
                <!--<div class="row-fluid">
                    <div class="span12 align-right">
                        Total Data starts with <span class="badge badge-important"><?php if($letter == ''){ echo "A-Z"; }else{ echo strtoupper($letter);}?></span> : <span class="badge badge-info"><?php echo $total_data;?></span><br/>
                        Showing : <span class="badge badge-info"><?php echo (($O_page-1)*20+1)." - ".(count($data_companys)+(($O_page-1)*20));?></span> of <span class="badge badge-info"><?php echo $total_data;?></span> 
                    </div>
                </div>!-->
                <div class="row-fluid">
                    <div class="span8">
                        <div class="input-append">
                            <select id="auto_search" class="autodrop"  > 
                            <option value=''>start typing...</option>
                                <?php foreach($data_lists as $data_list){ ?>
                                    <option value="<?php echo $data_list['Company_ID'];?>"><?php echo $data_list['Company_title'];?></option>
                                    <?php } ?>
                            </select>
                            <!-- <span>please type what you are looking</span> -->
                            <a onclick="goDetail();" class="btn btn-info" style="color: white;">Search</a>
                        </div>
                        <script>
                        function goDetail(){
                            var detail = $('#auto_search').val();
                            if(detail > 0 && isNaN(detail) == false){
                                location.href = "<?=$admin['path-company_detail'];?>"+detail+"&type="+"<?=$O_type;?>";
                            }
                        }
                        </script>
                    </div>
                    
                    <div class="span4 align-right">
                        Total Data starts with <span class="badge badge-important"><?php if($letter == ''){ echo "A-Z"; }else{ echo strtoupper($letter);}?></span> : <span class="badge badge-info"><?php echo $total_data;?></span><br/>
                        Showing : <span class="badge badge-info"><?php echo (($O_page-1)*20+1)." - ".(count($data_companys)+(($O_page-1)*20));?></span> of <span class="badge badge-info"><?php echo $total_data;?></span> 
                    </div>
                </div>
                <hr/>
                <!-- STARTS LETTER -->
                <div class="row-fluid">
                    <div class="span-12 align-center">
                        <?php for ($i = 'a', $j = 1; $j <= 26; $i++, $j++) {
                                $letters[$j] = $i;
                    ?>
                    <a href="<?php echo "adminMgr-Company.php?page=1&letter=$letters[$j]&type=$O_type&sort_type=$O_sortType&sort_order=$O_sortOrder";?>" class="btn-mini <?php  if($letter == $letters[$j] ){echo 'active-alphabet';} ?>" ><b><?php echo strtoupper($letters[$j]);?></b></a>&nbsp;
                    <?php } ?>
                    </div>
                </div><!-- ENDS LETTER -->
                <hr>
                <table class="table table-hover up2">
                <thead>
                    <tr>
			            <th>#</th>
                        <th>Title</th>
                        <th>Industry</th>
                        <th>Content</th>
                        <th>Employee</th>
                        <th>Jobs</th>
                        <th>Logo</th>
                        <th>Publish</th>
                        <th style="text-align:center;width:70px !important;">Action</th>
                    </tr>
                </thead>
                <?php
                if(!is_array($data_companys)){
                    echo "
                    <tbody><td colspan='9'><h5>There is no data.</h5></td></tbody>";
                }else{?>
                <tbody>
                <?php $k=1;
                    foreach($data_companys as $data_company){
                        $onclickDel = "\"if(window.confirm('Are you sure you want to delete {$data_company['Company_title']}?')) location.href='adminMgr-Company.php?action=delete&type={$O_type}&company_ID={$data_company['Company_ID']}&company_Title={$data_company['Company_title']}';\"";
                    ?>
                    <tr class="<?php if($data_company['Company_publish'] == 'Not Publish'){echo 'error';}?>">
	                    <td><?php echo ($O_page-1)*20+$k;?></td>
                        <td><?php echo correctDisplay($data_company['Company_title']);?></td>
                        <td><?php echo $data_company['Industry_title'];?></td>
                        <td><?php echo charLength($data_company['Company_content'],80);?></td>
                        <td><?php echo $data_company['work_counter'];?></td>
                        <td><?php echo $data_company['jobs_counter'];?></td>
                        <td><a class="proPhoto" href="<?php echo check_image_url($data_company['Company_img'], 'company');?>"><img class="img-circle" src="<?php echo check_image_url($data_company['Company_img_thmb'], 'company');?>" /></a></td>
                        <td><?php echo $data_company['Company_publish'];?></td>
                        <td style="text-align: center;">
                            <div class="btn-group btn-group-horizontal">  
                                <a href="adminMgr-Company_user.php?type=<?php echo $O_type."&company_ID=".$data_company['Company_ID'];?>" type="button" class="btn btn-info btn-small" style="color: white;"><i class="icon-user icon-white"></i> view</a>
                                <a href="adminMgr-Company_detail.php?company_ID=<?php echo $data_company['Company_ID'];?>&type=<?php echo $O_type;?>" type="button" class="btn btn-small"><i class="icon-edit"></i> edit</a>
                                <a href="#" <? echo "onclick=$onclickDel";?> type="button" class="btn btn-danger btn-small" style="color: white;"><i class="icon-trash icon-white"></i> delete</a>
                            </div>
                            <!--<div class="btn-group-vertical">
                                <div class="btn-group">
                                  <a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
                                    <i class=" icon-cog"></i>
                                  </a>
                                  <ul class="dropdown-menu align-left">
                                    <li><a href=""><i class=" icon-search"></i> View Employee</a></li>
                                    <li><a href="#" <? echo "onclick=$onclickDel";?><?php if($k >= 1 && $total_data >= 1){echo " disabled";}?>><i class=" icon-remove"></i> Delete</a></li>
                                    <li><a href=""><i class=" icon-edit"></i> Edit</a></li>
                                  </ul>
                                </div>
                            </div>!-->
                        </td>
                    </tr>
                <?php $k++; }?>
                </tbody>
                <? }?>
                </table>
                <br/>
                <!-- START PAGINATION COMPANY !-->
                <div class="pagination align-center">
                  <ul class="pagination pagination-sm">
                    <?php
                    $batch = getBatch($O_page);
                    if($batch < 1){$batch = 1;}
                    $prevLimit = 1 +(10*($batch-1));
                    $nextLimit = 10 * $batch;

                    if($nextLimit > $total_page){$nextLimit = $total_page;}
                    if ($total_page > 1 && $O_page > 1) {
                        echo "<li><a href='".$page_name."?page=1&".$letter_page."&".$type_page."&".$sortType_page."&".$sortOrder_page."' class='pagination-slide'>&lt;&lt; First</a></li>";
                    }
                    if($batch > 1 && $O_page > 10){
                        echo "<li><a href='".$page_name."?page=".($prevLimit-1)."&".$letter_page."&".$type_page."&".$sortType_page."&".$sortOrder_page."' class='pagination-slide'>&lt; Previous 10</a></li>";
                    }
                    for($mon = $prevLimit; $mon <= $nextLimit;$mon++){?>
                    <li class="<?php if($mon == $O_page){echo 'active';}?>"><span class="pagination-theme"><a href="<?php echo $page_name."?page=".$mon."&".$letter_page."&".$type_page."&".$sortType_page."&".$sortOrder_page;?>" ><?php echo $mon;?></a></span></li>
                    <?php if($mon == $nextLimit && $mon < $total_page){
                            if($mon >= $total_page){ $mon -=1;}
                            echo "<li><a href='".$page_name."?page=".($mon+1)."&".$letter_page."&".$type_page."&".$sortType_page."&".$sortOrder_page."' class='pagination-slide'>Next 10 &gt;</a></li>";
                        }                               
                    }
                    if ($total_page > 1 &&  ($O_page != $total_page)) {
                        echo "<li><a href='".$page_name."?page=".$total_page."&".$letter_page."&".$type_page."&".$sortType_page."&".$sortOrder_page."' class='pagination-slide'>Last &gt;&gt;</a></li>";
                    }
                    ?>
                  </ul>
                </div>
            </div>
        </div>
        <div class="row-fluid">
            <div class="span6">
                <?php require_once("admin-Footer.php"); ?>
            </div>
        </div>
    </div>
    <!--Modal box filter starts -->  
    <div id="modalSorting" class="modal hide fade">    
        <form name="filterCompany" action="adminMgr-Company.php" method="get" onsubmit="loadingText()" onclick="add_zindex();">         
            <div class="modal-header">
                <a href="#close" class="close" data-dismiss="modal" aria-hidden="true">×</a>
                <strong>Sorting By</strong>
            </div>
            <div class="modal-body">
                <input type="hidden" name="type" value="<?php echo $O_type;?>">
                <div class="row-fluid up1">
                    <div class="span4 modal-label">Sort Type :</div>
                    <div class="span8">
                        <select name="sort_type" class="text">
                            <option value="">Select type</option>
                            <option <?php if($O_sortType == "employee"){ echo "selected=selected"; }?> value="employee">Employee</option>
                            <option <?php if($O_sortType == "jobs"){ echo "selected=selected"; }?> value="jobs">Jobs</option>
                            <option <?php if($O_sortType == "other"){ echo "selected=selected"; }?> value="other">Other</option>
                        </select>
                    </div>
                </div>
                <div class="row-fluid up1">
                    <div class="span4 modal-label">Sort Order :</div>
                    <div class="span8">
                        <select name="sort_order" class="text">
                            <option <?php if($O_sortOrder == "asc"){ echo "selected=selected"; }?> value="asc">Ascending</option>
                            <option <?php if($O_sortOrder == "desc"){ echo "selected=selected"; }?> value="desc">Descending</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="btn-group">
                    <a href="#" class="btn" data-dismiss="modal"><i class="icon-remove"></i> Close</a>
                     <button id="btn-submit" type="submit" class="btn btn-success btn-submit"><i class="icon-list-alt icon-white"></i> Sorting</button>
                </div>
            </div>
        </form>
    </div><!--Modal box filter ends -->
    <!--Modal box starts -->  
    <div id="modalCompany" class="modal hide fade">    
        <form name="addCompany" action="adminMgr-Company.php?action=insert" method="post" enctype="multipart/form-data" onsubmit="loadingText()" onclick="add_zindex();">         
            <div class="modal-header">
                <a href="#close" class="close" data-dismiss="modal" aria-hidden="true">×</a>
                <strong>Create New Company</strong>
            </div>
            <div class="modal-body">
                <div class="row-fluid">
                    <div class="span4 modal-label"><strong>Industry Title&nbsp;*</strong>:</div>
                    <div class="span8">
                        <select name="industry_id" class="text" required="required">
                            <option value="">Select Industry</option>
                            <?php foreach($data_industrys as $data_industry){ ?>
                            <option value="<?php echo $data_industry['Industry_ID'];?>"><?php echo $data_industry['Industry_title'];?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="row-fluid">
                    <div class="span4 modal-label"><strong>City Title&nbsp;*</strong>:</div>
                    <div class="span8">
                        <select name="city_id" class="text" required="required">
                            <option value="">Select City</option>
                            <?php foreach($data_citys as $data_city){ ?>
                            <option value="<?php echo $data_city['City_ID'];?>"><?php echo $data_city['City_title'];?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="row-fluid">
                    <div class="span4 modal-label"><strong>Country Title&nbsp;*</strong>:</div>
                    <div class="span8">
                        <select name="country_id" class="text" required="required">
                            <option value="">Select Country</option>
                            <?php foreach($data_countrys as $data_country){ ?>
                            <option value="<?php echo $data_country['Country_ID'];?>"><?php echo $data_country['Country_title'];?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="row-fluid up1">
                    <div class="span4 modal-label"><strong>Company Title&nbsp;*</strong>:</div>
                    <div class="span8"><input name="title" type="text" class="text input-block-level" placeholder="Company Name" required="required" /></div>
                </div> 
                <div class="row-fluid up1">
                    <div class="span4 modal-label">Company Motto :</div>
                    <div class="span8"><textarea name="motto" class="input-block-level" wrap="soft" placeholder="desciption" rows="2" ></textarea></div>
                </div>  
                <div class="row-fluid up1">
                    <div class="span4 modal-label">Company Description :</div>
                    <div class="span8"><textarea name="content" class="input-block-level" wrap="soft" placeholder="description" rows="5" ></textarea></div>
                </div>
                <div class="row-fluid up1">
                    <div class="span4 modal-label">Company Address :</div>
                    <div class="span8"><textarea name="address" class="input-block-level" wrap="soft" placeholder="desciption" rows="3" ></textarea></div>
                </div>  
                <div class="row-fluid up1">
                    <div class="span4 modal-label">Company E-mail :</div>
                    <div class="span8"><input name="email" type="text" class="text input-block-level" placeholder="E-mail" /></div>
                </div>
                <div class="row-fluid up1">
                    <div class="span4 modal-label">Company Phone :</div>
                    <div class="span8"><input name="phone" type="text" class="text input-block-level" placeholder="Phone" /></div>
                </div>
                <div class="row-fluid up1">       
                    <div class="span4 modal-label"><strong>Company Image&nbsp;*</strong>:</div>
                    <div class="span8"><input name="image" type="file" class="text input-block-level" maxlength="256" placeholder="company image" required="required" /></div>
                </div>
                <div class="row-fluid">
                    <div class="offset4 span8"><small>image format has to be jpg, jpeg, gif, png.</small></div>
                </div>
                <div class="row-fluid up1">
                    <div class="span4 modal-label"><strong>Tags *:</strong></div>
                    <div class="span8">
                        <input id="refTag" name="tag" class="tags" value="" required="required" />
                    </div>
                </div>
                <div class="row-fluid up1">
                    <div class="span4 modal-label">Company Publish :</div>
                    <div class="span8">
                        <select name="publish" class="text">
                            <option value="Publish">Publish</option>
                            <option value="Not Publish">Not Publish</option>
                        </select>
                    </div>
                </div>
            </div>
            <input type="hidden" name="tags_input" id="data-tags" value=""/>
            <div class="modal-footer">
                <div class="btn-group">
                    <a href="#" class="btn" data-dismiss="modal"><i class="icon-remove"></i> Close</a>
                     <button id="btn-submit" name="submitCompany" onmouseover="submit_tags();" type="submit" class="btn btn-success btn-submit"><i class="icon-plus-sign icon-white"></i> Create</button>
                </div>
            </div>
        </form>
    </div><!--Modal box ends -->
    <script type="text/javascript">
        $("#message1").fadeOut(8400);
        function add_zindex(){
            $('.ui-autocomplete').css("z-index", 1055);
        }

        function submit_tags(){
                    var num = $('.tag > span').length;
                    var tags = new Array();
                    var data_tags = "";
                    for(var i =0; i < num; i++){
                        var text = $('.tag > span:eq('+i+')').text();
                        tags[i] = text.replace(/^\s\s*/, '').replace(/\s\s*$/, '');
                        console.log(tags[i]);
                        if(data_tags != ''){
                            data_tags += ", ";
                        }
                        data_tags += tags[i];
                    }
                    $('#data-tags').val(data_tags);
                }

        $("#message1").fadeOut(8400);  

        $(document).ready(function(){
            
            $('#refTag').tagsInput({
                        'width':'auto',
                        'autocomplete_url' : '<?=$api["tag-autocomplete"];?>',
                        'autocomplete':{selectFirst:true,width:'80px',autoFill:true}
                    });
        });

    </script>
    <script src="js/globalJS.js" type="text/javascript"></script>
</body></html>