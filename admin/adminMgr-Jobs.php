<?php
ob_start("ob_gzhandler");
require_once("packages/require.php");
include("packages/check_login.php");//USED BY ALL PAGE BUT index.php
include("controller/controller_jobs.php");
//part-pagination
$page_name = "adminMgr-Jobs.php";
$letter_page = "letter=$letter";
$field_page = "fieldID=$O_field";
$city_page = "cityID=$O_city";
$level_page = "levelID=$O_level";
$intern_page = "cintern=$O_intern";
$fulltime_page = "cfulltime=$O_fulltime";
$parttime_page = "cparttime=$O_parttime";
$freelance_page = "cfreelance=$O_freelance";
$keyword_page = "keyword=$O_keyword";
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <?php include('packages/head.php');?>
    <!-- calendar -->
    <link rel="stylesheet" type="text/css" href="packages/jQuery-Tags/jquery-ui.css" />
    <link rel="stylesheet" type="text/css" href="packages/jQuery-Tags/jquery.tagsinput.css" />

    <script src="//code.jquery.com/jquery-1.10.2.js"></script>
    <script src="js/jquery.select-to-autocomplete.js"></script>
    <script type="text/javascript" src="packages/jQuery-Tags/jquery-ui.js"></script>
    <script type="text/javascript" src="packages/jQuery-Tags/jquery.tagsinput.js"></script>
    <script src="js/tinymce/tinymce.min.js"></script>


    <script>
        tinymce.init({
            selector: "textarea",
            plugins: [
            "advlist autolink lists link image charmap print preview anchor",
            "searchreplace visualblocks code fullscreen",
            "insertdatetime media table contextmenu paste"
            ],
            toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
            relative_urls: false,
            forced_root_block : false
        });
$( document ).ready(function() {
          $('.autodrop').selectToAutocomplete();
            $(function() {

    $( ".datepicker" ).datepicker({dateFormat: 'yy-mm-dd' });
  });
        });
    </script>
    <style>
        body .modal {
            width: 50%; /* desired relative width */
            left: 25%; /* (100%-width)/2 */
            /* place center */
            margin-left:auto;
            margin-right:auto; 
        }

    </style>
</head>
<body class="civMain" onload="startTime()">
    <?php require_once("admin-Header.php");?>
    <div class="container">
        <div id="breadcrumb-parent" class="row-fluid">
            <div id="breadcrumb" class="span6">
                <a id="dashboard-link" href="#dashboard"><i class="icon-th-large"></i> Dashboard</a>
                : <a href='adminMgr-Jobs.php'>Jobs Management</a>
            </div>
            <div id="notifications" class="span6 align-right">
                <span>Welcome on <span id="date"></span></span>
            </div>
        </div>
        <div id="menuDashboard" class="row-fluid">
            
            <div id="leftNav-Parent" class="span3">
                <?php $curPage='jobs'; ?>
                <?php require_once("admin-Sidebar.php");?>
            </div>
            
            <div id="rightContent" class="span9">
                
                <?php
                if($message!=null)
                {
                    echo "<div id='message1' class='alert alert-info'>" . $message . "</div><br />";
                }
                ?>
                <div class="row-fluid">
                    <div class="span6 align-left">
                        <h4 style="margin: 0;">Jobs Management</h4>
                    </div>
                    <div class="span6 align-right">
                        <button class="btn btn-info" type="button" data-toggle="modal" data-target="#modalJobs"><i class="icon-plus icon-white"></i> Jobs</button>
                    </div>
                </div>
                <hr/>
                <div class="row-fluid">
                    <div class="span12 align-right">
                        Total Data starts with <span class="badge badge-important"><?php if($letter == ''){ echo "A-Z"; }else{ echo strtoupper($letter);}?></span> : <span class="badge badge-info"><?php echo $total_data;?></span><br/>
                        Showing : <span class="badge badge-info"><?php echo (($O_page-1)*20+1)." - ".(count($data_jobss)+(($O_page-1)*20));?></span> of <span class="badge badge-info"><?php echo $total_data;?></span> 
                    </div>
                </div>
                <br/>
                <form action="adminMgr-Jobs.php" method="get">
                <div class="row-fluid">
                    <div class="span4">
                        <div style="font-weight:bold;text-align:left;">Field</div>
                        <select name="fieldID" class="input-block-level">
                            <option value="">Select Field</option>
                            <?php foreach($data_fields as $data_field){ ?>
                            <option <?php if($O_field == $data_field['Field_ID']){ echo "selected=selected"; }?> value="<?php echo $data_field['Field_ID'];?>"><?php echo $data_field['Field_parent']." - ".$data_field['Field_title'];?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="span4">
                        <div style="font-weight:bold;text-align:left;">City</div>
                        <select name="cityID" class="input-block-level">
                            <option value="">Select City</option>
                            <?php foreach($data_citys as $data_city){ ?>
                            <option <?php if($O_city == $data_city['City_ID']){ echo "selected=selected"; }?> value="<?php echo $data_city['City_ID'];?>"><?php echo $data_city['City_title'];?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="span4">
                        <div style="font-weight:bold;text-align:left;">Level</div>
                        <select name="levelID" class="input-block-level">
                            <option value="">Select Level</option>
                            <?php foreach($data_levels as $data_level){ ?>
                            <option <?php if($O_level == $data_level['Level_ID']){ echo "selected=selected"; }?> value="<?php echo $data_level['Level_ID'];?>"><?php echo $data_level['Level_title'];?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="row-fluid">
                    <div class="span2">
                        <div style="font-weight:bold;text-align:left;">Intern</div>
                        <label class="checkbox">
                            <input name="cintern" type="checkbox" value="1" <?php if($O_intern == '1'){echo "checked=checked";}?>>
                        </label>
                    </div>
                    <div class="span2">
                        <div style="font-weight:bold;text-align:left;">Fulltime</div>
                        <label class="checkbox">
                            <input name="cfulltime" type="checkbox" value="1" <?php if($O_fulltime == '1'){echo "checked=checked";}?>>
                        </label>
                    </div>
                    <div class="span2">
                        <div style="font-weight:bold;text-align:left;">Parttime</div>
                        <label class="checkbox">
                            <input name="cparttime" type="checkbox" value="1" <?php if($O_parttime == '1'){echo "checked=checked";}?>>
                        </label>
                    </div>
                    <div class="span2">
                        <div style="font-weight:bold;text-align:left;">Freelance</div>
                        <label class="checkbox">
                            <input name="cfreelance" type="checkbox" value="1" <?php if($O_freelance == '1'){echo "checked=checked";}?>>
                        </label>
                    </div>
                    <div class="span2">
                        <div style="font-weight:bold;text-align:left;">Keyword</div>
                        <input name="keyword" type="text" class="text input-block-level" placeholder="keyword jobs" value="<?php echo $O_keyword;?>">
                    </div>
                    <div class="span2">
                        <br>
                        <button type="submit" class="btn"><i class="icon-circle-arrow-left"></i> Filter</button>
                    </div>
                </div>
                </form>
                <hr/>
                <!-- STARTS LETTER -->
                <div class="row-fluid">
                    <div class="span-12">
                        <?php for ($i = 'a', $j = 1; $j <= 26; $i++, $j++) {
                                $letters[$j] = $i;
                    ?>
                    <?php if($O_field != '' || $O_city != '' || $O_level != '' || $O_intern != '' || $O_fulltime != '' || $O_parttime != '' || $O_freelance != '' || $O_keyword != ''){?>
                        <a href="<?php echo "adminMgr-Jobs.php?page=1&letter=$letters[$j]&fieldID=$O_field&cityID=$O_city&levelID=$O_level&cintern=$O_intern&cfulltime=$O_fulltime&cparttime=$O_parttime&cfreelance=$O_freelance&keyword=$O_keyword";?>" class="btn-mini <?php  if($letter == $letters[$j] ){echo 'active-alphabet';} ?>" ><b><?php echo strtoupper($letters[$j]);?></b></a>&nbsp;
                    <?php }else{?>
                        <a href="<?php echo "adminMgr-Jobs.php?page=1&letter=$letters[$j]";?>" class="btn-mini <?php  if($letter == $letters[$j] ){echo 'active-alphabet';} ?>" ><b><?php echo strtoupper($letters[$j]);?></b></a>&nbsp;
                    <?php }
                     } ?>
                    </div>
                </div><!-- ENDS LETTER -->

                <table class="table table-bordered table-hover up2">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Title</th>
                        <th>Company</th>
                        <th>Field</th>
                        <th>Level</th>
                        <th>City</th>
                        <th>INT</th>
                        <th>FT</th>
                        <th>PT</th>
                        <th>FL</th>
                        <th>Status</th>
                        <th style="text-align:center;width:70px !important;">Action</th>
                    </tr>
                </thead>
                <?php
                if(!is_array($data_jobss)){
                    echo "
                    <tbody><td colspan='12'><h5>There is no data.</h5></td></tbody>";
                }else{?>
                <tbody>
                <?php $k=1;
                    foreach($data_jobss as $data_jobs){
                    if($k >= 1 && $total_data >= 1){
                        $onclickDel = "\"if(window.confirm('Are you sure you want to delete {$data_jobs['Jobs_title']}?')) location.href='adminMgr-Jobs.php?action=delete&jobs_ID={$data_jobs['Jobs_ID']}&jobs_Title={$data_jobs['Jobs_title']}';\"";
                    }
                    ?>
                    <tr class="<?php if($data_jobs['Jobs_publish'] == 'Not Publish'){echo 'error';}?>">
                        <td><?php echo ($O_page-1)*20+$k;?></td>
                        <td><?php echo $data_jobs['Jobs_title'];?></td>
                        <td><?php echo correctDisplay($data_jobs['Company_title']);?></td>
                        <td><?php echo $data_jobs['Field_title'];?></td>
                        <td><?php echo $data_jobs['Level_title'];?></td>
                        <td><?php echo $data_jobs['City_title'];?></td>
                        <td><?php if($data_jobs['Jobs_intern'] == '1'){echo "Yes";}else{echo "No";}?></td>
                        <td><?php if($data_jobs['Jobs_fulltime'] == '1'){echo "Yes";}else{echo "No";}?></td>
                        <td><?php if($data_jobs['Jobs_parttime'] == '1'){echo "Yes";}else{echo "No";}?></td>
                        <td><?php if($data_jobs['Jobs_freelance'] == '1'){echo "Yes";}else{echo "No";}?></td>
                        <td><?php echo $data_jobs['Jobs_status'];?></td>
                        <td style="text-align: center;">
                            <div class="btn-group-vertical">
                                <div class="btn-group">
                                  <a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
                                    <i class=" icon-cog"></i>
                                  </a>
                                  <ul class="dropdown-menu align-left">
                                    <li><a href="#" <? echo "onclick=$onclickDel";?><?php if($k >= 1 && $total_data >=1){echo " disabled";}?>><i class=" icon-remove"></i> Delete</a></li>
                                    <li><a href="adminMgr-Jobs_detail.php?jobs_ID=<?php echo $data_jobs['Jobs_ID'];?>"><i class=" icon-edit"></i> Edit</a></li>
                                  </ul>
                                </div>
                            </div>
                        </td>
                    </tr>
                <?php $k++; }?>
                </tbody>
                <? }?>
                </table>
                <br/>
                <?php if($O_field != '' || $O_city != '' || $O_level != '' || $O_intern != '' || $O_fulltime != '' || $O_parttime != '' || $O_freelance != '' || $O_keyword != ''){
                        include("part-pagination-jobs.php");
                    }else
                        {include("part-pagination.php");
                    }
                ?>
            </div>
        </div>
        <div class="row-fluid">
            <div class="span6">
                <?php require_once("admin-Footer.php"); ?>
            </div>
        </div>
    </div>
    <!--Modal box starts -->  
    <div id="modalJobs" class="modal hide fade">    
        <form name="addJobs" action="adminMgr-Jobs.php?action=insert" method="post" enctype="multipart/form-data" onsubmit="loadingText()" onclick="add_zindex();">         
            <div class="modal-header">
                <a href="#close" class="close" data-dismiss="modal" aria-hidden="true">×</a>
                <strong>Create New Jobs</strong>
            </div>
            <div class="modal-body">
                <div class="row-fluid">
                    <div class="span4 modal-label"><strong>Jobs Title&nbsp;*</strong>:</div>
                    <div class="span8"><input name="title" type="text" class="text input-block-level" placeholder="Jobs Name" required="required" /></div>
                </div>
                <div class="row-fluid up1">
                    <div class="span4 modal-label">Jobs Description :</div>
                    <div class="span8"><textarea name="content" class="textarea" wrap="soft" placeholder="description" rows="5" ></textarea></div>
                </div>
                <div class="row-fluid up1">
                    <div class="span4 modal-label"><strong>Company&nbsp;*</strong>:</div>
                    <div class="span8">
                        <select name="company_id" class="text autodrop" required="required">
                            <option value="">Select Company</option>
                            <?php foreach($data_companys as $data_company){ ?>
                            <option value="<?php echo $data_company['Company_ID'];?>"><?php echo correctDisplay($data_company['Company_title']);?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="row-fluid up1">
                    <div class="span4 modal-label"><strong>Field&nbsp;*</strong>:</div>
                    <div class="span8">
                        <select name="field_id" class="text" required="required">
                            <option value="">Select Field</option>
                            <?php foreach($data_fields as $data_field){ ?>
                            <option value="<?php echo $data_field['Field_ID'];?>"><?php echo $data_field['Field_parent']." - ".$data_field['Field_title'];?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="row-fluid up1">
                    <div class="span4 modal-label"><strong>Level&nbsp;*</strong>:</div>
                    <div class="span8">
                        <select name="level_id" class="text" required="required">
                            <option value="">Select Level</option>
                            <?php foreach($data_levels as $data_level){ ?>
                            <option value="<?php echo $data_level['Level_ID'];?>"><?php echo $data_level['Level_title'];?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="row-fluid up1">
                    <div class="span4 modal-label"><strong>City&nbsp;*</strong>:</div>
                    <div class="span8">
                        <select name="city_id" class="text autodrop" required="required">
                            <option value="">Select City</option>
                            <?php foreach($data_citys as $data_city){ ?>
                            <option value="<?php echo $data_city['City_ID'];?>"><?php echo $data_city['City_title'].", ".$data_city['Country_abbr'];?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="row-fluid">
                    <div class="span4 modal-label">Jobs Experience :</div>
                    <div class="span8">
                        <div class="input-append">
                            <input class="span5" id="appendedInput" name="exp" type="text" class="text input-block-level" placeholder="Experience Year" />
                            <span class="add-on">Year</span>
                        </div>
                    </div>
                </div>
                <div class="row-fluid up1">
                    <div class="span4 modal-label"><strong>Jobs Post Start&nbsp;*</strong> :</div>
                    <div class="span8"><input type="text" class="form-control datepicker" name="start" placeholder="Start Date" required="required"></div>
                </div>
                <div class="row-fluid up1">
                    <div class="span4 modal-label"><strong>Jobs Post End&nbsp;*</strong> :</div>
                    <div class="span8"><input type="text" class="form-control datepicker" name="end" placeholder="End Date" required="required"></div>
                </div>
                <div class="row-fluid up1">
                    <div class="span4 modal-label">Jobs Qualification Info :</div>
                    <div class="span8"><textarea name="qualification" class="textarea" wrap="soft" placeholder="i.e. Description issue (max 2048 chars)" rows="5" ></textarea></div>
                </div>
                <div class="row-fluid up1">
                    <div class="span4 modal-label">Jobs Apply Info :</div>
                    <div class="span8"><textarea name="apply" class="textarea" wrap="soft" placeholder="i.e. Description issue (max 2048 chars)" rows="5" ></textarea></div>
                </div>
                <div class="row-fluid up1">
                    <div class="span4 align modal-label">Jobs Intern :</div>
                    <div class="span8">
                        <label class="checkbox">
                            <input name="intern" type="checkbox" value="1">
                        </label>
                    </div>
                </div>
                <div class="row-fluid up1">
                    <div class="span4 align modal-label">Jobs Fulltime :</div>
                    <div class="span8">
                        <label class="checkbox">
                            <input name="fulltime" type="checkbox" value="1">
                        </label>
                    </div>
                </div>
                <div class="row-fluid up1">
                    <div class="span4 align modal-label">Jobs Parttime :</div>
                    <div class="span8">
                        <label class="checkbox">
                            <input name="parttime" type="checkbox" value="1">
                        </label>
                    </div>
                </div>
                <div class="row-fluid up1">
                    <div class="span4 align modal-label">Jobs Freelance :</div>
                    <div class="span8">
                        <label class="checkbox">
                            <input name="freelance" type="checkbox" value="1">
                        </label>
                    </div>
                </div>
                <div class="row-fluid up1">
                    <div class="span4 modal-label"><strong>Salary Type&nbsp;*</strong>:</div>
                    <div class="span8">
                        <select id="selectMenu" name="salary_type" class="text" required="required">
                            <option value="fixed">Fixed</option>
                            <option value="negotiable">Negotiable</option>
                        </select>
                    </div>
                </div>
                <div class="row-fluid up1">
                    <div class="span4 modal-label"><strong>Salary Period&nbsp;*</strong>:</div>
                    <div class="span8">
                        <select name="salary_period" class="text" required="required">
                            <option value="monthly">Monthly</option>
                            <option value="bi-weekly">Bi-weekly</option>
                            <option value="weekly">Weekly</option>
                            <option value="daily">Daily</option>
                            <option value="hourly">Hourly</option>
                        </select>
                    </div>
                </div>
                <div class="row-fluid up1">
                    <div class="span4 modal-label"><strong>Salary<span id="text-min"></span>&nbsp;*</strong>:</div>
                    <div class="span8">
                        <select name="salary_min" class="text" required="required">
                            <option value="">Select Salary</option>
                            <option value="0">0</option>
                            <option value="1">1 Juta</option>
                            <option value="2">2 Juta</option>
                            <option value="3">3 Juta</option>
                            <option value="5">5 Juta</option>
                            <option value="7">7 Juta</option>
                            <option value="10">10 Juta</option>
                            <option value="15">15 Juta</option>
                            <option value="20">20 Juta</option>
                            <option value="25">25 Juta</option>
                            <option value="30">30 Juta</option>
                            <option value="45">45 Juta</option>
                            <option value="60">60 Juta</option>
                            <option value="75">75 Juta</option>
                        </select>
                    </div>
                </div>
                <div id="showMax" class="row-fluid up1 hide">
                    <div class="span4 modal-label"><strong>Salary Max&nbsp;*</strong>:</div>
                    <div class="span8">
                        <select name="salary_max" class="text">
                            <option value="">Select Salary</option>
                            <option value="1">1 Juta</option>
                            <option value="2">2 Juta</option>
                            <option value="3">3 Juta</option>
                            <option value="5">5 Juta</option>
                            <option value="7">7 Juta</option>
                            <option value="10">10 Juta</option>
                            <option value="15">15 Juta</option>
                            <option value="20">20 Juta</option>
                            <option value="25">25 Juta</option>
                            <option value="30">30 Juta</option>
                            <option value="45">45 Juta</option>
                            <option value="60">60 Juta</option>
                            <option value="75">75 Juta</option>
                        </select>
                    </div>
                </div>
                <div class="row-fluid up1">
                    <div class="span4"><strong>Tags *:</strong></div>
                    <div class="span8">
                        <input id="refTag" name="tag" class="tags"  value="" required="required"/>
                    </div>
                </div>
                <div class="row-fluid up1">
                    <div class="span4 modal-label">Jobs Publish :</div>
                    <div class="span8">
                        <select name="publish" class="text">
                            <option value="Publish">Publish</option>
                            <option value="Not Publish">Not Publish</option>
                        </select>
                    </div>
                </div>
                <div class="row-fluid up1">
                    <div class="span4 modal-label">Jobs Status :</div>
                    <div class="span8">
                        <select name="status" class="text">
                            <option value="Open">Open</option>
                            <option value="Close">Close</option>
                        </select>
                    </div>
                </div>
            </div>
            <input type="hidden" name="tags_input" id="data-tags" value=""/>
            <div class="modal-footer">
                <div class="btn-group">
                    <a href="#" class="btn" data-dismiss="modal"><i class="icon-circle-arrow-left"></i> Close</a>
                     <button id="btn-submit" name="submitJobs" onmouseover="submit_tags();" type="submit" class="btn btn-success btn-submit"><i class="icon-ok icon-white"></i> Create</button>
                </div>
            </div>
        </form>
    </div><!--Modal box ends -->
    <script>
        $(document).ready(function () {    
            $("#selectMenu").bind("change", function () {
                if ($(this).val() == "negotiable") {
                    $("#showMax").slideDown();
                    $('#text-min').text(" Min");
                }
                else{
                    $("#showMax").slideUp();
                    $('#text-min').text("");
                }
            });
        });
    </script>
    <script type="text/javascript">
        $("#message1").fadeOut(8400);
        function add_zindex(){
            $('.ui-autocomplete').css("z-index", 1055);
        }

        function submit_tags(){
                    var num = $('.tag > span').length;
                    var tags = new Array();
                    var data_tags = "";
                    for(var i =0; i < num; i++){
                        var text = $('.tag > span:eq('+i+')').text();
                        tags[i] = text.replace(/^\s\s*/, '').replace(/\s\s*$/, '');
                        console.log(tags[i]);
                        if(data_tags != ''){
                            data_tags += ", ";
                        }
                        data_tags += tags[i];
                    }
                    $('#data-tags').val(data_tags);
                }


        $("#message1").fadeOut(8400);  

        $(document).ready(function(){
            $('#refTag').tagsInput({
                        'width':'auto',
                        'autocomplete_url' : '<?=$api["tag-autocomplete"];?>',
                        'autocomplete':{selectFirst:true,width:'80px',autoFill:true}
                    });
        });
    </script>
    <script src="js/globalJS.js" type="text/javascript"></script>
</body></html>