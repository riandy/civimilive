<?php
ob_start("ob_gzhandler");
require_once("packages/require.php");
include("packages/check_login.php");//USED BY ALL PAGE BUT index.php
include("controller/controller_image.php");
$page_name = "adminMgr-Image.php";
$sort_page = "sort=$O_sort";
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <?php include('packages/head.php');?>
    <script src="//code.jquery.com/jquery-1.10.2.js"></script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.0/themes/smoothness/jquery-ui.css">
    <script src="//code.jquery.com/ui/1.11.1/jquery-ui.js"></script>
    <?php require_once("packages/fancyBoxJS.php"); ?>
</head>
<body class="civMain" onload="startTime()">
    <?php require_once("admin-Header.php");?>

    <!-- start top nav -->
    <?php $curPage='module'; ?>
    <?php require_once("admin-top_nav.php");?>
    <!-- end top nav -->

    <div class="container">
        <div id="menuDashboard" class="row-fluid">
            <div id="rightContent" class="span12">
                
                <?php
                if($message!=null)
                {
                    echo "<div id='message1' class='alert alert-info'>" . $message . "</div><br />";
                }
                ?>
                <div class="row-fluid">
                    <div class="span12 align-left">
                        <h4 style="margin: 0;">Image Management</h4>
                    </div>
                </div>
                <hr/>
                <div class="row-fluid">
                    <div class="span8">
                        <select name="sort" class="text" onChange="window.location.href=this.options[this.selectedIndex].value">
                            <option value="adminMgr-Image.php?page=1&sort=newest" <?php if($O_sort == "newest"){echo "selected=selected";}?>>Newest</option>
                            <option value="adminMgr-Image.php?page=1&sort=viewed" <?php if($O_sort == "viewed"){echo "selected=selected";}?>>Most Viewed</option>
                        </select>
                    </div>
                    <div class="span4 align-right">
                        Total Data starts with <span class="badge badge-important"><?php if($letter == ''){ echo "A-Z"; }else{ echo strtoupper($letter);}?></span> : <span class="badge badge-info"><?php echo $total_data;?></span><br/>
                        Showing : <span class="badge badge-info"><?php echo (($O_page-1)*20+1)." - ".(count($data_photos)+(($O_page-1)*20));?></span> of <span class="badge badge-info"><?php echo $total_data;?></span> 
                    </div>
                </div>
                <hr/>
                <div class="row-fluid">
                    <?php foreach($data_photos as $data_photo){ 
                        $obj_con->up();
                        $count_tags = $obj_photo->get_count_tags("photo", $data_photo['Photo_ID']);
                        $obj_con->down();

                        if($data_photo['Photo_title'] != ""){
                            $photo_title = $data_photo['Photo_title'];
                        }else{
                            $photo_title = "No title";
                        }
                        $fullname = $data_photo['User_fname']." ".$data_photo['User_lname'];

                        $onclickDel = "\"if(window.confirm('Are sure want to delete $photo_title from $fullname')) location.href='adminMgr-Image.php?action=delete&photo_ID={$data_photo['Photo_ID']}&page={$O_page}&".$sort_page."&fullname={$fullname}&photo_Title=".urlencode($photo_title)."';\"";
                        //publish || unplublish data
                        if($data_photo['Photo_publish'] == "Publish"){
                            $publish = "Unpublish"; 
                            $class_publish = "";
                            $icon_publish = "icon-remove";
                            $onclickPublish = "\"if(window.confirm('Are sure want to set unpublish $photo_title from $fullname')) location.href='adminMgr-Image.php?action=unpublish&photo_ID={$data_photo['Photo_ID']}&page={$O_page}&".$sort_page."&fullname={$fullname}&photo_Title=".urlencode($photo_title)."';\"";
                        }else{
                            $publish = "Publish";
                            $class_publish = "image-unpublish";
                            $icon_publish = "icon-ok";
                            $onclickPublish = "\"if(window.confirm('Are sure want to set publish $photo_title from $fullname')) location.href='adminMgr-Image.php?action=publish&photo_ID={$data_photo['Photo_ID']}&page={$O_page}&".$sort_page."&fullname={$fullname}&photo_Title=".urlencode($photo_title)."';\"";
                        }

                        //set popular || unset popular
                        if($data_photo['Photo_popular'] == "yes"){
                            $popular = "Unset popular"; 
                            $icon_popular = "icon-file";
                            $onclickPopular = "\"if(window.confirm('Are sure want to unset popular $photo_title from $fullname')) location.href='adminMgr-Image.php?action=unpopular&photo_ID={$data_photo['Photo_ID']}&page={$O_page}&".$sort_page."&fullname={$fullname}&photo_Title=".urlencode($photo_title)."';\"";
                        }else{
                            $popular = "Set Popular";
                            $icon_popular = "icon-bookmark";
                            $onclickPopular = "\"if(window.confirm('Are sure want to set popular $photo_title from $fullname')) location.href='adminMgr-Image.php?action=popular&photo_ID={$data_photo['Photo_ID']}&page={$O_page}&".$sort_page."&fullname={$fullname}&photo_Title=".urlencode($photo_title)."';\"";
                        }
                    ?>
                    <div class="span3 col-image <?=$class_publish;?>">
                        <div class="image-sec">
                            <a class="proPhoto" href="<?=check_image_url($data_photo['Photo_imgLink'], 'image');?>"><img src="<?=check_image_url($data_photo['Photo_ThmbImgLink'], 'image');?>" title="<?=$photo_title;?>" /></a>
                            <?php if($data_photo['Photo_popular'] == "yes"){ ?>
                                <div class="pop-icon"><img src="http://www.civimi.com/beta/admin/img/flag.png"/></div>
                            <?php } ?>  
                            <div class="dropdown dropdown-image">
                              <a class="dropdown-toggle btn-image" id="dLabel" role="button" data-toggle="dropdown"href="#">
                                <i class="icon-cog"></i>
                              </a>
                              <ul class="dropdown-menu menu-image" role="menu" aria-labelledby="dLabel">
                                <li style="border-bottom:solid 1px #ddd;"><a href="adminMgr-Image_detail.php?photo_ID=<?php echo $data_photo['Photo_ID']."&photo_Title=".$photo_title."&page=".$O_page."&".$sort_page;?>"><i class="icon-pencil"></i> Edit</a></li>
                                <li><a href="#" <?php echo "onclick=$onclickPopular"; ?>><i class="<?=$icon_popular;?>"></i> <?php echo $popular;?></a></li>
                                <li><a href="#" <?php echo "onclick=$onclickPublish"; ?>><i class="<?=$icon_publish;?>"></i> <?php echo $publish;?></a></li>
                                <li><a href="#" <?php echo "onclick=$onclickDel"; ?>><i class="icon-trash"></i> Delete</a></li>
                              </ul>
                            </div>
                        </div>
                        <div class="image-title"><?php echo charLength($photo_title, 50);?></div>
                        <div class="image-user">
                            <div class="image-user-part">
                                <img src="<?=check_image_url($data_photo['User_proPhotoThmb'], 'user');?>">
                            </div>
                            <div class="image-user-name">
                                <div class="name"><a target="_blank" href="http://www.civimi.com/beta/profile/<?php echo $data_photo['User_username'];?>.cvm"><?php echo charLength($fullname, 25);?></a></div>
                                <div class="image-viewer">
                                    <i class="icon-eye-open"></i> <?php echo $data_photo['hits_image'];?>&nbsp; 
                                    <i class="icon-tag"></i> <?php echo $count_tags;?>&nbsp;
                                    <i class="icon-heart"></i> <?php echo $data_photo['hits_like'];?>
                                </div>     
                            </div>
                        </div>
                        <div class="image-time"><i class="icon-time"></i> <?php echo relative_time($data_photo['Photo_create_date']);?></div> 
                    </div>
                    <?php } ?>
                </div>
                <br/>
                <!-- START PAGINATION !-->
                <div class="pagination align-center">
                  <ul class="pagination pagination-sm">
                    <?php
                    $batch = getBatch($O_page);
                    if($batch < 1){$batch = 1;}
                    $prevLimit = 1 +(10*($batch-1));
                    $nextLimit = 10 * $batch;

                    if($nextLimit > $total_page){$nextLimit = $total_page;}
                    if ($total_page > 1 && $O_page > 1) {
                        echo "<li><a href='".$page_name."?page=1&".$sort_page."' class='pagination-slide'>&lt;&lt; First</a></li>";
                    }
                    if($batch > 1 && $O_page > 10){
                        echo "<li><a href='".$page_name."?page=".($prevLimit-1)."&".$sort_page."' class='pagination-slide'>&lt; Previous 10</a></li>";
                    }
                    for($mon = $prevLimit; $mon <= $nextLimit;$mon++){?>
                    <li class="<?php if($mon == $O_page){echo 'active';}?>"><span class="pagination-theme"><a href="<?php echo $page_name."?page=".$mon."&".$sort_page;?>" ><?php echo $mon;?></a></span></li>
                    <?php if($mon == $nextLimit && $mon < $total_page){if($mon >= $total_page){ $mon -=1;}
                    echo "<li><a href='".$page_name."?page=".($mon+1)."&".$sort_page."' class='pagination-slide'>Next 10 &gt;</a></li>";
                }                               }
                if ($total_page > 1 &&  ($O_page != $total_page)) {
                    echo "<li><a href='".$page_name."?page=".$total_page."&".$sort_page."' class='pagination-slide'>Last &gt;&gt;</a></li>";
                }
                ?>
                </ul>
                </div>
                <!-- END PAGINATION !-->
            </div>
        </div>
        <div class="row-fluid">
            <div class="span6">
                <?php require_once("admin-Footer.php"); ?>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        $("#message1").fadeOut(8400);
        function add_zindex(){
            $('.ui-autocomplete').css("z-index", 1055);
        }

        $(document).ready(function() {
            $("a.proPhoto").fancybox({helpers: {title: {type: "inside"}}, topRatio: 0.5, fixed: true, autoSize: false, closeClick: false, openEffect: "fade", closeEffect: "fade"})
        });
    </script>
    <script src="js/globalJS.js" type="text/javascript"></script>
</body></html>