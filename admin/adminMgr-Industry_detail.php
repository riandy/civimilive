<?php
ob_start("ob_gzhandler");
require_once("packages/require.php");
include("packages/check_login.php");//USED BY ALL PAGE BUT index.php
include("controller/controller_industry_detail.php");
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <?php include('packages/head.php');?>
    <!-- calendar -->
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.0/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.1/jquery-ui.js"></script>
    <script>
  $(function() {
    $( ".datepicker" ).datepicker({dateFormat: 'yy-mm-dd' });
  });
  </script>
    <style>
        body .modal {
            width: 50%; /* desired relative width */
            left: 25%; /* (100%-width)/2 */
            /* place center */
            margin-left:auto;
            margin-right:auto; 
        }
    </style>
</head>
<body class="civMain" onload="startTime()">
    <?php require_once("admin-Header.php");?>
    <div class="container">
        <div id="breadcrumb-parent" class="row-fluid">
            <div id="breadcrumb" class="span6">
                <a id="dashboard-link" href="#dashboard"><i class="icon-th-large"></i> Dashboard</a>
                : <a href='adminMgr-Industry.php'>Industry Management</a>
            </div>
            <div id="notifications" class="span6 align-right">
                <span>Welcome on <span id="date"></span></span>
            </div>
        </div>
        <div id="menuDashboard" class="row-fluid">
            
            <div id="leftNav-Parent" class="span3">
                <?php $curPage='industry'; ?>
                <?php require_once("admin-Sidebar.php");?>
            </div>
            
            <div id="rightContent" class="span9">
                
                <?php
                if($message!=null)
                {
                    echo "<div class='alert alert-info'>" . $message . "</div><br />";
                }
                ?>
                <div class="row-fluid">
                    <div class="span3">
                        <a href="adminMgr-Industry.php" class="btn btn-default btn-small"><i class="icon-circle-arrow-left"></i> Back</a>
                    </div>
                    <div class="span6">
                        <h4 style="margin: 0;text-align:center;">Industry Edit (<?php echo $data_industrys[0]['Industry_title'];?>)</h4>
                    </div>
                </div>
                <hr/>
                <?php $k=0;
                foreach($data_industrys as $data_industry){?>
                <form name="addIndustry" action="adminMgr-Industry_detail.php?action=update" method="post" enctype="multipart/form-data" onsubmit="loadingText()">
                    <div class="row-fluid">
                        <div class="span4 modal-label"><strong>Industry Title&nbsp;*</strong>:</div>
                        <div class="span8"><input name="title" type="text" class="text input-block-level" placeholder="Company Title"/ value="<?php echo $data_industry['Industry_title'];?>"></div>
                    </div>
                    <div class="row-fluid up1">
                        <div class="span4 modal-label">Industry Description :</div>
                        <div class="span8"><textarea name="content" class="input-block-level" wrap="soft" placeholder="description" rows="5"><?php echo $data_industry['Industry_content'];?></textarea></div>
                    </div>
                    <div class="row-fluid up1">
                        <div class="span4 modal-label">Industry Publish :</div>
                        <div class="span8">
                            <select name="publish" class="text">
                                <option <?php if($data_industry['Industry_publish'] == "Publish"){ echo "selected=selected"; }?> value="Publish">Publish</option>
                                <option <?php if($data_industry['Industry_publish'] == "Not Publish"){ echo "selected=selected"; }?> value="Not Publish">Not Publish</option>
                            </select>
                        </div>
                    </div>
                    <div class="row-fluid">
                        <div id="SubStatus" class="span12 align-center"></div>
                    </div>
                    <div class="row-fluid">
                    <div class="span12">
                    <div class="well" align="center">
                        <input name="id" type="hidden" value="<?php echo $data_industry['Industry_ID'];?>"/>
                        <div class="btn-group" align="center">
                            <a href="adminMgr-Industry.php" class="btn btn-danger" style="color: white;"><i class="icon-remove icon-white"></i> Cancel</a>
                            <button id="btn-submit" name="saveIndustry" type="submit" class="btn btn-success"><i class="icon-ok icon-white"></i> Save Industry</button>
                        </div>
                    </div>
                    </div>
                    </div>
                </form>
                <?php $k++; }?>
            </div>
        </div>
        <div class="row-fluid">
            <div class="span6">
                <?php require_once("admin-Footer.php"); ?>
            </div>
        </div>
    </div>
    <script src="js/globalJS.js" type="text/javascript"></script>
</body></html>