<?php
ob_start("ob_gzhandler");
require_once("packages/require.php");
include("packages/check_login.php");//USED BY ALL PAGE BUT index.php
include("controller/controller_news_category_detail.php");
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <?php include('packages/head.php');?>
    <!-- calendar -->
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.0/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.1/jquery-ui.js"></script>
    <script>
  $(function() {
    $( ".datepicker" ).datepicker({dateFormat: 'yy-mm-dd' });
  });
  </script>
    <style>
        body .modal {
            width: 50%; /* desired relative width */
            left: 25%; /* (100%-width)/2 */
            /* place center */
            margin-left:auto;
            margin-right:auto; 
        }
    </style>
</head>
<body class="civMain" onload="startTime()">
    <?php require_once("admin-Header.php"); ?>

    <!-- start top nav -->
    <?php $curPage='blog'; ?>
    <?php require_once("admin-top_nav.php");?>
    <!-- end top nav -->

    <div class="container">
        <div id="menuDashboard" class="row-fluid">      
            <div id="rightContent" class="span12">
            <?php
            if($message!=null)
            {
                echo "<div id='message1' class='alert alert-info'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>" . $message . "</div><br />";
            }
            ?>
            <div class="row-fluid">
                <div class="span3">
                    <a href="adminMgr-News_category.php" class="btn btn-default"><i class="icon-circle-arrow-left"></i> Back</a>
                </div>
                <div class="span6">
                    <h4 style="margin: 0; text-align:center;">News Category Edit <br>(<?php echo $datas[0]['Nc_title'];?>)</h4>
                </div>
            </div>
            <hr />
            <form name="editCategory" action="adminMgr-News_category_detail.php?action=editCategory" enctype="multipart/form-data" method="post" onsubmit="loadingText()">          
            <div class="row-fluid up2">
                <div class="span5 align-right modal-label"><strong>Category&nbsp;*</strong>:</div>
                    <div class="span7"><input name="category_name" type="text" class="text" required="required" maxlength="256" placeholder="news category name" value="<?php echo $datas[0]['Nc_title']; ?>" /></div>
                </div>
                <div class="row-fluid">
                    <div class="span5 align-right modal-label">Category sort #:</div>
                    <div class="span7"><input name="category_sort" type="number" class="text" required="required" maxlength="4" placeholder="sort #" value="<?php echo $datas[0]['Nc_sort']; ?>" /></div>
                </div>
                <div class="row-fluid">
                    <div class="span5" align="right">Publish :</div>
                    <div class="span7">
                        <select name="category_publish" class="text">
                            <option <?php if($datas[0]['Nc_publish'] == 'Publish'){echo "selected=selected";}?> value="Publish">Publish</option>
                            <option <?php if($datas[0]['Nc_publish'] != 'Publish'){echo "selected=selected";}?> value="Not Publish">Not Publish</option>
                        </select>
                    </div>
                </div>
                <div class="row-fluid">
                    <div id="SubStatus" class="span12 align-center"></div>
                </div>
                <hr/>
                <div class="row-fluid" style="background-color: #eee; border-top: black thin; padding: 10px 0 10px 0;">
                    <div class="span12 align-center">
                        <input type="hidden" name="category_ID" value="<?php echo $datas[0]['Nc_ID']; ?>">
                        <div class="btn-group">
                            <a href="adminMgr-News_category.php" class="btn btn-danger" style="color: white;"><i class="icon-remove icon-white"></i> Cancel</a>
                            <button id="btn-submit" name="saveCategory" type="submit" class="btn btn-success" ><i class="icon-check icon-white"></i> Save Category</button>
                        </div>
                    </div>
                </div>
            </form>
            </div>
        </div>
        <div class="row-fluid">
            <div class="span6">
                <?php require_once("admin-Footer.php"); ?>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $("#message1").fadeOut(8400);
    </script>
    <script src="js/globalJS.js" type="text/javascript"></script>
</body></html>