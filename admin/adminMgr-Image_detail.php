<?php
ob_start("ob_gzhandler");
require_once("packages/require.php");
include("packages/check_login.php");//USED BY ALL PAGE BUT index.php
include("controller/controller_image_detail.php");
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <?php include('packages/head.php');?>
    <!-- calendar -->

    <link rel="stylesheet" type="text/css" href="packages/jQuery-Tags/jquery-ui.css" />
    <link rel="stylesheet" type="text/css" href="packages/jQuery-Tags/jquery.tagsinput.css" />
    <script src="//code.jquery.com/jquery-1.10.2.js"></script>
    <script type="text/javascript" src="packages/jQuery-Tags/jquery-ui.js"></script>
    <script type="text/javascript" src="packages/jQuery-Tags/jquery.tagsinput.js"></script>

    <style>
        body .modal {
            width: 50%; /* desired relative width */
            left: 25%; /* (100%-width)/2 */
            /* place center */
            margin-left:auto;
            margin-right:auto; 
        }
    </style>
</head>
<body class="civMain" onload="startTime()">
    <?php require_once("admin-Header.php");?>

    <!-- start top nav -->
    <?php $curPage='module'; ?>
    <?php require_once("admin-top_nav.php");?>
    <!-- end top nav -->

    <div class="container">
        <div id="menuDashboard" class="row-fluid">
            <div id="rightContent" class="span12">
                
                <?php
                if($message!=null)
                {
                    echo "<div class='alert alert-info'>" . $message . "</div><br />";
                }
                ?>
                <div class="row-fluid">
                    <div class="span3">
                        <a href="adminMgr-Image.php?page=<?php echo $O_page."&sort=".$O_sort;?>" class="btn btn-default btn-small"><i class="icon-circle-arrow-left"></i> Back</a>
                    </div>
                    <div class="span6">
                        <h4 style="margin: 0;text-align:center;">Image Edit <br>(<?php echo correctDisplay($O_title);?>)</h4>
                    </div>
                </div>
                <hr/>
                <form name="updateImage" action="adminMgr-Image_detail.php?action=update" method="post" enctype="multipart/form-data" onsubmit="loadingText()">
                    <div class="row-fluid up2">
                        <div class="span4 align-right modal-label">Image :</div>
                        <div class="span5">
                            <a class="proPhoto" href="<?php echo check_image_url($data_photos[0]['Photo_imgLink'], 'image');?>">
                                <img src="<?php echo check_image_url($data_photos[0]['Photo_imgLink'], 'image');?>" class="companyAdminImg" style="width: 100%" />
                            </a>
                        </div>
                    </div>
                    <div class="row-fluid up1">
                        <div class="span4 align-right modal-label">Photo Title :</div>
                        <div class="span5"><input name="title" type="text" class="text input-block-level" placeholder="Photo Title" value="<?php echo correctDisplay($data_photos[0]['Photo_title']);?>"></div>
                    </div>
                    <div class="row-fluid">
                        <div class="span4 align-right modal-label">Tags :</div>
                        <div class="span5">
                            <input id="refTag" name="tag" class="tags" value="<?php echo $data_tags;?>"/>
                        </div>
                    </div>
                    <div class="row-fluid up1">
                        <div class="span4 align-right modal-label">Photo Publish :</div>
                        <div class="span5">
                            <select name="publish" class="text">
                                <option <?php if($data_photos[0]['Photo_publish'] == "Publish"){ echo "selected=selected"; }?> value="Publish">Publish</option>
                                <option <?php if($data_photos[0]['Photo_publish'] == "Not Publish"){ echo "selected=selected"; }?> value="Not Publish">Not Publish</option>
                            </select>
                        </div>
                    </div>
                    <div class="row-fluid">
                        <div id="SubStatus" class="span12 align-center"></div>
                    </div>
                    <div class="row-fluid">
                    <div class="span12">
                    <div class="well" align="center">
                        <input type="hidden" name="page" value="<?php echo $O_page;?>">
                        <input type="hidden" name="sort" value="<?php echo $O_sort;?>">
                        <input name="id" type="hidden" value="<?php echo $data_photos[0]['Photo_ID'];?>"/>
                        <input name="fullname" type="hidden" value="<?php echo $data_photos[0]['User_fname']." ".$data_photos[0]['User_lname'];?>"/>
                        <input name="tags_input" type="hidden" id="data-tags" value=""/>
                        <div class="btn-group" align="center">
                            <a href="adminMgr-Image.php?page=<?php echo $O_page."&sort=".$O_sort;?>" class="btn btn-danger" style="color: white;"><i class="icon-remove icon-white"></i> Cancel</a>
                            <button class="btn btn-info" type="button" data-toggle="modal" data-target="#modalMessage"><i class="icon-envelope icon-white"></i> Send Message</button>
                            <button id="btn-submit" name="saveImage" onmouseover="submit_tags();" type="submit" class="btn btn-success"><i class="icon-check icon-white"></i> Save Image</button>
                        </div>
                    </div>
                    </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="row-fluid">
            <div class="span6">
                <?php require_once("admin-Footer.php"); ?>
            </div>
        </div>
    </div>
    <!--Modal box starts -->  
    <div id="modalMessage" class="modal hide fade">    
        <form name="message" action="adminMgr-Image_detail.php?action=message" method="post" enctype="multipart/form-data" onsubmit="loadingText()" onclick="add_zindex();">         
            <div class="modal-header">
                <a href="#close" class="close" data-dismiss="modal" aria-hidden="true">×</a>
                <strong>Send Message to <?php echo $data_photos[0]['User_fname']." ".$data_photos[0]['User_lname'];?></strong>
            </div>
            <div class="modal-body">
                <div class="row-fluid up1">
                    <div class="span3 align-right modal-label"><strong>Subject </strong>:</div>
                    <div class="span7"><input name="subject" type="text" required="required" class="text input-block-level" placeholder="Subject"></div>
                    </div>
                <div class="row-fluid up1">
                    <div class="span3 align-right modal-label"><strong>Message </strong>:</div>
                    <div class="span7"><textarea name="message" class="input-block-level" required="required" wrap="soft" placeholder="description" rows="5" ></textarea></div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="btn-group">
                    <input type="hidden" name="page" value="<?php echo $O_page;?>">
                    <input type="hidden" name="sort" value="<?php echo $O_sort;?>">
                    <input type="hidden" name="id" value="<?php echo $data_photos[0]['Photo_ID'];?>">
                    <input type="hidden" name="title" value="<?php echo $data_photos[0]['Photo_title'];?>">
                    <input type="hidden" name="username" value="<?php echo $data_photos[0]['User_fname'];?>">
                    <input type="hidden" name="email" value="<?php echo $data_photos[0]['User_email'];?>">
                    <input type="hidden" name="image" value="<?php echo check_image_url($data_photos[0]['Photo_imgLink'], 'image');?>">
                    <input type="hidden" name="tags_image" value="<?php echo $data_tags;?>">
                    <a href="#" class="btn" data-dismiss="modal"><i class="icon-remove"></i> Close</a>
                    <button id="btn-submit" name="submitMessage" type="submit" class="btn btn-success btn-submit"><i class="icon-envelope icon-white"></i> Send</button>
                </div>
            </div>
        </form>
    </div><!--Modal box ends -->
    <script type="text/javascript">
        $("#message1").fadeOut(8400);
        function add_zindex(){
            $('.ui-autocomplete').css("z-index", 1055);
        }

        function submit_tags(){
                    var num = $('.tag > span').length;
                    var tags = new Array();
                    var data_tags = "";
                    for(var i =0; i < num; i++){
                        var text = $('.tag > span:eq('+i+')').text();
                        tags[i] = text.replace(/^\s\s*/, '').replace(/\s\s*$/, '');
                        console.log(tags[i]);
                        if(data_tags != ''){
                            data_tags += ", ";
                        }
                        data_tags += tags[i];
                    }
                    $('#data-tags').val(data_tags);
                }


        $("#message1").fadeOut(8400);  

        $(document).ready(function(){
            
            $('#refTag').tagsInput({
                        'width':'auto',
                        'autocomplete_url' : '<?=$api["tag-autocomplete"];?>',
                        'autocomplete':{selectFirst:true,width:'80px',autoFill:true}
                    });
        });
    </script>
    <script src="js/globalJS.js" type="text/javascript"></script>
</body></html>