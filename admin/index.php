<?php
// <copyright file="AuthenticationCivimi.cs" company="Civimi">
// Copyright (c) 2014 All Right Reserved, http://civimi.com/
//
// This source is subject to the Civimi Permissive License.
// All other rights reserved.
//
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
// </copyright>
// <author>Kelvin Gani & Billy Gani</author>
// <date>2014-08-15</date>
ob_start("ob_gzhandler");
require("packages/require.php");
include("controller/controller_index.php");
if($_GET['attempt']=='1'){$message='You have incorrect login info, please try again.';}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html id="bg-admin" xmlns="http://www.w3.org/1999/xhtml">
<head>
    <?php include('packages/head.php');?>
    <script type="text/javascript">
        function validateForm(){var b=document.forms.login1["username"].value;var a=document.forms.login1["password"].value;if((b==null||b=="")||(a==null||a=="")){alert("Please enter an authorized username with the correct password");return false}};
    </script>
</head>
<body id="admin-login" class="container">
    <div class="up5">
	
    </div>
    <div class="row-fluid">
        <div id="admin-logo" class="span12">
            <a href="index.php">
                <img style="width:30%;" id="admin-logo-area" src="<?php echo $global['logo-desktop'];?>" alt="<?php echo $config['companyName']; ?>" border="0" onmouseover="style.opacity=0.65" onmouseout="style.opacity=1" />
            </a>
        </div>
    </div>
    <div class="row-fluid">
        <div class="span12">
            <form id="login" name="login1" action="<?php echo $_SERVER['PHP_SELF'];?>?action=login" method="post" onsubmit="return validateForm()">
                <div class="row-fluid">
                    <div class="span5"><input name="username" type="text" class="text" maxlength="30" placeholder="username" required="required" /></div>
                    <div class="span5"><input name="password" type="password" class="text" maxlength="30" placeholder="password" required="required" /></div>
                    <div class="span2"><input value="Sign In" id="admin-button-signin" type="submit" class="btn" onclick="return validateForm()" /></div>
                </div>
            </form>
        </div>
    </div>
    <div class="row-fluid">
        <div class="span12">
            <?php echo "<div class='redFont'>$message</div>";?>
        </div>
    </div>
    <div class="row-fluid">
        <div id="footer" class="span12">
            <?php echo $global['copyright-desktop']; ?><br />
            <a title="www.civimi.com - engineered by passion" href="http://www.civimi.com">engineered by civimi</a>
        </div>
    </div>
</body>
</html>
