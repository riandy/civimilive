<?php
ob_start("ob_gzhandler");
require_once("packages/require.php");
include("packages/check_login.php");//USED BY ALL PAGE BUT index.php
include("controller/controller_admin_setting.php");
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <?php include('packages/head.php');?>
    <script src="js/user/userVldt.js" type="text/javascript"></script>
</head>
<body class="civMain" onload="startTime()">
    <?php require_once("admin-Header.php");?>

    <!-- start top nav -->
    <?php $curPage='setting'; ?>
    <?php require_once("admin-top_nav.php");?>
    <!-- end top nav -->

    <div class="container">
        <div id="menuDashboard" class="row-fluid">
            <div id="rightContent" class="span12">
                
                <?php
                if($message!=null)
                {
                    echo "<div id='message1' class='alert alert-info'>" . $message . "</div><br />";
                }
                ?>
                <div class="row-fluid">
                    <div class="span8 align-left">
                        <h4 style="margin: 0;">Admin Setting Management</h4>
                    </div>
                    <div class="span4 align-right">
                        <button class="btn btn-info" type="button" data-toggle="modal" data-target="#dialog-addSetting"><i class="icon-plus icon-white"></i> Admin Setting</button>
                    </div>
                </div>
                <hr />
                <div class="row-fluid">
                    <div class="span12 align-right">
                        Total Admin Setting : <span class="badge badge-info"><?php echo $total_data;?></span>
                    </div>
                </div>
                <table class="table table-hover">
                    <tr bgcolor="#E6E6FA">
                        <th>Username</th>
                        <th>Role Name</th>
                        <th>User Page</th>
                        <th>Admin Page</th>
                        <th>Certificate Page</th>
                        <th>Skill Page</th>
                        <th>Country Page</th>
                        <th>City Page</th>
                        <th>Tag Page</th>
                        <th>Setting Page</th>
                        <th style="text-align:center;">Action</th>
                    </tr>
                    <?php if(is_array($data_users)){foreach($data_users as $data_user){
                       $onclickDel = "\"if(window.confirm('Are you sure you would like to delete {$data_user['Admin_username']}?')) location.href='adminMgr-Admin_setting.php?action=delete&admin_username={$data_user['Admin_username']}&admin_id={$data_user['Admin_ID']}';\"";?>
                       <tr>
                        <td><?php echo $data_user['Admin_username']; ?></td>
                        <td><?php echo $data_user['Admin_role']; ?></td>
                        <td><?php if($data_user['User_show'] == "Yes"){echo "<font color='#0000ff'>";}else{echo "<font color='#ff0000'>";}?>
                            <?php echo $data_user['User_show']; ?></font></td>
                        <td><?php if($data_user['Admin_show'] == "Yes"){echo "<font color='#0000ff'>";}else{echo "<font color='#ff0000'>";}?>
                            <?php echo $data_user['Admin_show']; ?></font></td>
                        <td><?php if($data_user['Certificate_show'] == "Yes"){echo "<font color='#0000ff'>";}else{echo "<font color='#ff0000'>";}?>
                            <?php echo $data_user['Certificate_show']; ?></font></td>
                        <td><?php if($data_user['Skill_show'] == "Yes"){echo "<font color='#0000ff'>";}else{echo "<font color='#ff0000'>";}?>
                            <?php echo $data_user['Skill_show']; ?></font></td>
                        <td><?php if($data_user['Country_show'] == "Yes"){echo "<font color='#0000ff'>";}else{echo "<font color='#ff0000'>";}?>
                            <?php echo $data_user['Country_show']; ?></font></td>
                        <td><?php if($data_user['City_show'] == "Yes"){echo "<font color='#0000ff'>";}else{echo "<font color='#ff0000'>";}?>
                            <?php echo $data_user['City_show']; ?></font></td>
                        <td><?php if($data_user['Tag_show'] == "Yes"){echo "<font color='#0000ff'>";}else{echo "<font color='#ff0000'>";}?>
                            <?php echo $data_user['Tag_show']; ?></font></td>
                        <td><?php if($data_user['Setting_show'] == "Yes"){echo "<font color='#0000ff'>";}else{echo "<font color='#ff0000'>";}?>
                            <?php echo $data_user['Setting_show']; ?></font></td>
                        <td style="text-align: center;"><div class="btn-group"><a href="adminMgr-Admin_setting_detail.php?admin_id=<?php echo $data_user['Admin_ID'];?> " type="button" class="btn btn-small"><i class="icon-edit"></i> edit</a>
                            <a href="#" <?php echo "onclick=$onclickDel"; ?> type="button" class="btn btn-danger btn-small" style="color: white;" ><i class="icon-trash icon-white"></i> delete</a></div></td>
                    </tr>
                    <?php }}else{ ?>
                        <td colspan="14"><?php echo "<h5>there is no data</h5>";?></td>
                    <?php } ?>
                </table>

                <!--Modal box starts -->   
                <div id="dialog-addSetting" class="modal hide fade">    
                    <form name="addSetting" action="<?php echo $_SERVER['PHP_SELF'];?>?action=process" method="post" onsubmit="loadingText()">
                        <div class="modal-header">
                            <a href="#close" class="close" data-dismiss="modal" aria-hidden="true">×</a>
                            <strong>Create New Admin Setting</strong>
                        </div>
                        <div class="modal-body">
                            <div class="row-fluid">
                                <div class="span4 modal-label"><strong>Admin Username&nbsp;*</strong>:</div>
                                <div class="span8">
                                    <select name="admin_id" class="text" required="required">
                                        <option value="">Select Admin</option>
                                        <?php foreach($data_admins as $data_admin){ ?>
                                        <option value="<?php echo $data_admin['id'];?>"><?php echo $data_admin['username'];?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="row-fluid">
                                <div class="span4 modal-label"><strong>Admin Role&nbsp;*</strong>:</div>
                                <div class="span8">
                                    <select name="role_id" class="text" required="required">
                                        <option value="">Select Role</option>
                                        <?php foreach($data_roles as $data_role){ ?>
                                        <option value="<?php echo $data_role['Role_ID'];?>"><?php echo $data_role['Role_name'];?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="row-fluid">
                                <div class="span4 modal-label"><strong>Education Show&nbsp;*</strong>:</div>
                                <div class="span8">
                                    <input name="edu" type="radio" class="text" required="required" value="Yes" />Yes&nbsp;&nbsp;&nbsp;
                                    <input name="edu" type="radio" class="text" required="required" value="No" />No
                                </div>
                            </div>
                            <div class="row-fluid up1">
                                <div class="span4 modal-label"><strong>Company Show&nbsp;*</strong>:</div>
                                <div class="span8">
                                    <input name="comp" type="radio" class="text" required="required" value="Yes" />Yes&nbsp;&nbsp;&nbsp;
                                    <input name="comp" type="radio" class="text" required="required" value="No" />No
                                </div>
                            </div>
                            <div class="row-fluid up1">
                                <div class="span4 modal-label"><strong>Industry Show&nbsp;*</strong>:</div>
                                <div class="span8">
                                    <input name="ind" type="radio" class="text" required="required" value="Yes" />Yes&nbsp;&nbsp;&nbsp;
                                    <input name="ind" type="radio" class="text" required="required" value="No" />No
                                </div>
                            </div>
                            <div class="row-fluid up1">
                                <div class="span4 modal-label"><strong>Field Show&nbsp;*</strong>:</div>
                                <div class="span8">
                                    <input name="field" type="radio" class="text" required="required" value="Yes" />Yes&nbsp;&nbsp;&nbsp;
                                    <input name="field" type="radio" class="text" required="required" value="No" />No
                                </div>
                            </div>
                            <div class="row-fluid up1">
                                <div class="span4 modal-label"><strong>Jobs Show&nbsp;*</strong>:</div>
                                <div class="span8">
                                    <input name="jobs" type="radio" class="text" required="required" value="Yes" />Yes&nbsp;&nbsp;&nbsp;
                                    <input name="jobs" type="radio" class="text" required="required" value="No" />No
                                </div>
                            </div>
                            <div class="row-fluid up1">
                                <div class="span4 modal-label"><strong>User Show&nbsp;*</strong>:</div>
                                <div class="span8">
                                    <input name="user" type="radio" class="text" required="required" value="Yes" />Yes&nbsp;&nbsp;&nbsp;
                                    <input name="user" type="radio" class="text" required="required" value="No" />No
                                </div>
                            </div>
                            <div class="row-fluid up1">
                                <div class="span4 modal-label"><strong>Admin Show&nbsp;*</strong>:</div>
                                <div class="span8">
                                    <input name="admin" type="radio" class="text" required="required" value="Yes" />Yes&nbsp;&nbsp;&nbsp;
                                    <input name="admin" type="radio" class="text" required="required" value="No" />No
                                </div>
                            </div>
                            <div class="row-fluid up1">
                                <div class="span4 modal-label"><strong>Certificate Show&nbsp;*</strong>:</div>
                                <div class="span8">
                                    <input name="cert" type="radio" class="text" required="required" value="Yes" />Yes&nbsp;&nbsp;&nbsp;
                                    <input name="cert" type="radio" class="text" required="required" value="No" />No
                                </div>
                            </div>
                            <div class="row-fluid up1">
                                <div class="span4 modal-label"><strong>Skill Show&nbsp;*</strong>:</div>
                                <div class="span8">
                                    <input name="skill" type="radio" class="text" required="required" value="Yes" />Yes&nbsp;&nbsp;&nbsp;
                                    <input name="skill" type="radio" class="text" required="required" value="No" />No
                                </div>
                            </div>
                            <div class="row-fluid up1">
                                <div class="span4 modal-label"><strong>Country Show&nbsp;*</strong>:</div>
                                <div class="span8">
                                    <input name="country" type="radio" class="text" required="required" value="Yes" />Yes&nbsp;&nbsp;&nbsp;
                                    <input name="country" type="radio" class="text" required="required" value="No" />No
                                </div>
                            </div>
                            <div class="row-fluid up1">
                                <div class="span4 modal-label"><strong>City Show&nbsp;*</strong>:</div>
                                <div class="span8">
                                    <input name="city" type="radio" class="text" required="required" value="Yes" />Yes&nbsp;&nbsp;&nbsp;
                                    <input name="city" type="radio" class="text" required="required" value="No" />No
                                </div>
                            </div>
                            <div class="row-fluid up1">
                                <div class="span4 modal-label"><strong>Level Show&nbsp;*</strong>:</div>
                                <div class="span8">
                                    <input name="level" type="radio" class="text" required="required" value="Yes" />Yes&nbsp;&nbsp;&nbsp;
                                    <input name="level" type="radio" class="text" required="required" value="No" />No
                                </div>
                            </div>
                            <div class="row-fluid up1">
                                <div class="span4 modal-label"><strong>Tag Show&nbsp;*</strong>:</div>
                                <div class="span8">
                                    <input name="tag" type="radio" class="text" required="required" value="Yes" />Yes&nbsp;&nbsp;&nbsp;
                                    <input name="tag" type="radio" class="text" required="required" value="No" />No
                                </div>
                            </div>
                            <div class="row-fluid up1">
                                <div class="span4 modal-label"><strong>Setting Show&nbsp;*</strong>:</div>
                                <div class="span8">
                                    <input name="setting" type="radio" class="text" required="required" value="Yes" />Yes&nbsp;&nbsp;&nbsp;
                                    <input name="setting" type="radio" class="text" required="required" value="No" />No
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <div class="btn-group">
                                <a href="#" class="btn" data-dismiss="modal"><i class="icon-remove"></i> Close</a>
                                <button id="btn-submit" name="submitSetting" type="submit" class="btn btn-success btn-submit"><i class="icon-plus-sign icon-white"></i> Create</button>
                            </div>
                        </div>
                    </form> 
                </div>
                <!--Modal box ends --> 
            </div>
        </div>
        <div class="row-fluid">
            <div class="span6">
                <?php require_once("admin-Footer.php"); ?>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $("#message1").fadeOut(8400);
    </script>
    <script src="js/globalJS.js" type="text/javascript"></script>
</body></html>