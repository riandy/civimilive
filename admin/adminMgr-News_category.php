<?php
ob_start("ob_gzhandler");
require_once("packages/require.php");
include("packages/check_login.php");//USED BY ALL PAGE BUT index.php
include("controller/controller_news_category.php");
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <?php include('packages/head.php');?>
    <!-- calendar -->
    <link rel="stylesheet" type="text/css" href="packages/jQuery-Tags/jquery-ui.css" />
    <link rel="stylesheet" type="text/css" href="packages/jQuery-Tags/jquery.tagsinput.css" />
    <script src="//code.jquery.com/jquery-1.10.2.js"></script>

    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.0/themes/smoothness/jquery-ui.css">
    <script src="//code.jquery.com/ui/1.11.1/jquery-ui.js"></script>
    <script src="js/jquery.select-to-autocomplete.js"></script>

    <script>
  $(function() {
    $( ".datepicker" ).datepicker({dateFormat: 'yy-mm-dd' });
    $('.autodrop').selectToAutocomplete();
  });
  </script>
    <style>
        body .modal {
            width: 50%; /* desired relative width */
            left: 25%; /* (100%-width)/2 */
            /* place center */
            margin-left:auto;
            margin-right:auto; 
        }
    </style>
</head>
<body class="civMain" onload="startTime()">
    <?php require_once("admin-Header.php"); ?>

    <!-- start top nav -->
    <?php $curPage='blog'; ?>
    <?php require_once("admin-top_nav.php");?>
    <!-- end top nav -->

    <div class="container">
        <div id="menuDashboard" class="row-fluid">
            <div id="rightContent" class="span12">
                        
                <?php
                if($message!=null)
                {
                    echo "<div id='message1' class='alert alert-info'>". $message ."</div><br />";
                }
                ?>
                <div class="row-fluid">
                    <div class="span8 align-left">
                        <h4 style="margin: 0;">News Category Management</h4>
                    </div>
                    <div class="span4 align-right">
                        <button class="btn btn-info" type="button" data-toggle="modal" data-target="#dialog-addCategory"><i class="icon-plus icon-white"></i> News Category</button>
                    </div>
                </div>
                <hr />
                <div class="row-fluid">
                    <div class="span12" align="right">
                        Total News Category : <span class="badge badge-info"><?php echo $total_data;?></span>
                    </div>
                </div><hr />
                <table class="table table-hover">
                    <tr>
                        <th>No</th>
                        <th>Title</th>
                        <th># News</th>
                        <th>Publish</th>
                        <th style="text-align:center;">Action</th>
                    </tr>
                    <?php if(is_array($datas)){ $k=1; foreach($datas as $data){
                       if($k >= 1 && $total_data > 1){
                        $onclickDel = "\"if(window.confirm('Are you sure you want to delete {$data['Nc_title']}?')) location.href='adminMgr-News_category.php?action=delete&nc_ID={$data['Nc_ID']}&nc_Title={$data['Nc_title']}';\"";
                    }?>
                    <tr class="<?php if($data['Nc_publish'] == 'Not Publish'){echo 'error';}?>">
                        <td><?php echo $k;?></td>
                        <td><?php echo correctDisplay($data['Nc_title']);?></td>
                        <td><?php echo $data['counter_news'];?></td>
                        <td><?php echo $data['Nc_publish'];?></td>
                        <td style="text-align: center;"><div class="btn-group"><a href="adminMgr-News_category_detail.php?nc_ID=<?php echo $data['Nc_ID'];?>" type="button" class="btn btn-small"><i class="icon-edit"></i> edit</a>
                            <a href="#" <?php echo "onclick=$onclickDel"; ?> type="button" class="btn btn-danger btn-small" style="color: white;" <?php if($k == 1 && $total_data == 1){echo " disabled";}?>><i class="icon-trash icon-white"></i> delete</a></div>
                        </td>
                    </tr>
                    <?php $k++; }}else{
                        echo "<tr><td colspan='5'><h5>There is no category.</h5></td></tr>";
                    } ?>
                </table>
                <?php /*
                if(is_array($datas))
                {
                    foreach($datas as $data)
                    {
                        $onclickDel = "\"if(window.confirm('Are you sure to delete {$data['Nc_title']}?')) location.href='adminMgr-News_category.php?action=delete&nc_ID={$data['Nc_ID']}&nc_Title={$data['Nc_title']}';\"";
                            echo "<div class='row-fluid'><div class='' style='float:left;'><strong>{$data['Nc_title']}</strong></div>"; 
                        ?>
                        <div class=" align-right" style='float:right;'>
                            <span class="editLink"><strong><a href="adminMgr-News_category_detail.php?nc_ID=<?=$data['Nc_ID'];?>">edit</a> | <a href="#" <?php echo "onclick=$onclickDel";?> >delete</a></strong></span>
                        </div>
                        <?php echo "</div>";         
                    }//END OF FOR Statement
                }*/
                ?>
                <!--Modal box starts --> 
                <div id="dialog-addCategory" class="modal hide fade">    
                    <form name="addCategory" action="adminMgr-News_category.php?action=addCategory" enctype="multipart/form-data" method="post" onsubmit="loadingText()">
                        <div class="modal-header">
                            <a href="#close" class="close" data-dismiss="modal" aria-hidden="true">×</a>
                            <strong>Create New Category for News</strong>
                        </div>
                        <div class="modal-body">
                            <div class="row-fluid">
                                <div class="span4 align-right modal-label"><strong>Category&nbsp;*</strong>:</div>
                                <div class="span8"><input name="category_name" type="text" class="text" required="required" maxlength="256" placeholder="news category name" /></div>
                            </div>
                            <div class="row-fluid">
                                <div class="span4 align-right modal-label"><strong>Category sort #:</strong></div>
                                <div class="span8"><input name="category_sort" type="number" class="text" maxlength="4" placeholder="sort #" /></div>
                            </div>
                            <div class="row-fluid">
                                <div class="span4 align-right modal-label">Publish :</div>
                                <div class="span8">
                                    <select name="category_publish" class="text">
                                        <option value="Publish">Publish</option>
                                        <option value="Not Publish">Not Publish</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row-fluid">
                                <div id="SubStatus" class="span12 align-center"></div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <div class="btn-group">
                                <a href="#" class="btn" data-dismiss="modal"><i class="icon-remove"></i> Close</a>
                                <button id="btn-submit" name="submitCategory" type="submit" class="btn btn-success btn-submit"><i class="icon-plus-sign icon-white"></i> Create</button>
                            </div>
                        </div>
                    </form> 
                </div>
                <!--Modal box ends --> 
            </div>
        </div>
        <div class="row-fluid">
            <div class="span6">
                <?php require_once("admin-Footer.php"); ?>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $("#message1").fadeOut(8400);
    </script>
    <script src="js/globalJS.js" type="text/javascript"></script>
</body></html>
