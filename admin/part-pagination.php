<div class="pagination align-center">
  <ul class="pagination pagination-sm">
    <?php
    $batch = getBatch($O_page);
    if($batch < 1){$batch = 1;}
    $prevLimit = 1 +(10*($batch-1));
    $nextLimit = 10 * $batch;

    if($nextLimit > $total_page){$nextLimit = $total_page;}
    if ($total_page > 1 && $O_page > 1) {
        echo "<li><a href='".$page_name."?page=1&".$query_page."&".$type_page."' class='pagination-slide'>&lt;&lt; First</a></li>";
    }
    if($batch > 1 && $O_page > 10){
        echo "<li><a href='".$page_name."?page=".($prevLimit-1)."&".$query_page."&".$type_page."' class='pagination-slide'>&lt; Previous 10</a></li>";
    }
    for($mon = $prevLimit; $mon <= $nextLimit;$mon++){?>
    <li class="<?php if($mon == $O_page){echo 'active';}?>"><span class="pagination-theme"><a href="<?php echo $page_name."?page=".$mon."&".$query_page."&".$type_page;?>" ><?php echo $mon;?></a></span></li>
    <?php if($mon == $nextLimit && $mon < $total_page){if($mon >= $total_page){ $mon -=1;}
    echo "<li><a href='".$page_name."?page=".($mon+1)."&".$query_page."&".$type_page."' class='pagination-slide'>Next 10 &gt;</a></li>";
}                               }
if ($total_page > 1 &&  ($O_page != $total_page)) {
    echo "<li><a href='".$page_name."?page=".$total_page."&".$query_page."&".$type_page."' class='pagination-slide'>Last &gt;&gt;</a></li>";
}
?>
</ul>
</div>