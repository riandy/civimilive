<?php
ob_start("ob_gzhandler");
require_once("packages/require.php");
include("packages/check_login.php");//USED BY ALL PAGE BUT index.php
include("controller/controller_skill.php");
$page_name = "adminMgr-Skill.php";
$query_page = "letter=$letter";
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <?php include('packages/head.php');?>
    <!-- calendar -->
    <link rel="stylesheet" type="text/css" href="packages/jQuery-Tags/jquery-ui.css" />
    <link rel="stylesheet" type="text/css" href="packages/jQuery-Tags/jquery.tagsinput.css" />
    <script src="//code.jquery.com/jquery-1.10.2.js"></script>

    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.0/themes/smoothness/jquery-ui.css">
    <script src="//code.jquery.com/ui/1.11.1/jquery-ui.js"></script>
    <script src="js/jquery.select-to-autocomplete.js"></script>

    <script>
  $(function() {
    $( ".datepicker" ).datepicker({dateFormat: 'yy-mm-dd' });
    $('.autodrop').selectToAutocomplete();
  });
  </script>
    <style>
        body .modal {
            width: 50%; /* desired relative width */
            left: 25%; /* (100%-width)/2 */
            /* place center */
            margin-left:auto;
            margin-right:auto; 
        }
    </style>
</head>
<body class="civMain" onload="startTime()">
    <?php require_once("admin-Header.php");?>
    <div class="container">
        <div id="breadcrumb-parent" class="row-fluid">
            <div id="breadcrumb" class="span6">
                <a id="dashboard-link" href="#dashboard"><i class="icon-th-large"></i> Dashboard</a>
                : <a href='adminMgr-Skill.php'>Skill Management</a>
            </div>
            <div id="notifications" class="span6 align-right">
                <span>Welcome on <span id="date"></span></span>
            </div>
        </div>
        <div id="menuDashboard" class="row-fluid">
            
            <div id="leftNav-Parent" class="span3">
                <?php $curPage='skill'; ?>
                <?php require_once("admin-Sidebar.php");?>
            </div>
            
            <div id="rightContent" class="span9">
                
                <?php
                if($message!=null)
                {
                    echo "<div id='message1' class='alert alert-info'>" . $message . "</div><br />";
                }
                ?>
                <div class="row-fluid">
                    <div class="span6 align-left">
                        <h4 style="margin: 0;">Skill Management</h4>
                    </div>
                    <div class="span6 align-right">
                        <!--<button class="btn btn-info" type="button" data-toggle="modal" data-target="#modalFilter"><i class="icon-search icon-white"></i> Advance Filter</button>!-->
                        <button class="btn btn-info" type="button" data-toggle="modal" data-target="#modalSkill"><i class="icon-plus icon-white"></i> Skill</button>
                    </div>
                </div>
                <hr/>
                <?php/*
                <div class="row-fluid">
                    <div class="span12 align-right">
                        Total Data starts with <span class="badge badge-important"><?php if($letter == ''){ echo "A-Z"; }else{ echo strtoupper($letter);}?></span> : <span class="badge badge-info"><?php echo $total_data;?></span><br/>
                        Showing : <span class="badge badge-info"><?php echo (($O_page-1)*20+1)." - ".(count($data_edus)+(($O_page-1)*20));?></span> of <span class="badge badge-info"><?php echo $total_data;?></span> 
                    </div>
                </div>*/?>
                <div class="row-fluid">
                    <div class="span8">
                        <div class="input-append">
                            <select id="auto_search" class="autodrop"  > 
                            <option value=''>start typing...</option>
                                <?php foreach($data_lists as $data_list){ ?>
                                    <option value="<?php echo $data_list['Skill_ID'];?>"><?php echo $data_list['Skill_name'];?></option>
                                    <?php } ?>
                            </select>
                            <!-- <span>please type what you are looking</span> -->
                            <a onclick="goDetail();" class="btn btn-info">Search</a>
                        </div>
                        <script>
                        function goDetail(){
                            var detail = $('#auto_search').val();
                            if(detail > 0 && isNaN(detail) == false){
                                location.href = "<?=$admin['path-skill_detail'];?>"+detail;
                            }
                        }
                        </script>
                    </div>
                    
                    <div class="span4 align-right">
                        Total Data starts with <span class="badge badge-important"><?php if($letter == ''){ echo "A-Z"; }else{ echo strtoupper($letter);}?></span> : <span class="badge badge-info"><?php echo $total_data;?></span><br/>
                        Showing : <span class="badge badge-info"><?php echo (($O_page-1)*20+1)." - ".(count($data_skills)+(($O_page-1)*20));?></span> of <span class="badge badge-info"><?php echo $total_data;?></span> 
                    </div>
                </div>
                <hr/>
                <!-- STARTS LETTER -->
                <div class="row-fluid">
                    <div class="span-12">
                        <?php for ($i = 'a', $j = 1; $j <= 26; $i++, $j++) {
                                $letters[$j] = $i;
                    ?>
                    <a href="<?php echo "adminMgr-Skill.php?page=1&letter=$letters[$j]";?>" class="btn-mini <?php  if($letter == $letters[$j] ){echo 'active-alphabet';} ?>" ><b><?php echo strtoupper($letters[$j]);?></b></a>&nbsp;
                    <?php } ?>
                    </div>
                </div><!-- ENDS LETTER -->

                <table class="table table-bordered table-hover up2">
                <thead>
                    <tr>
			            <th>#</th>
                        <th>Skill Name</th>
                        <th># of possesed</th>
                        <th>Image (Optional)</th>
                        <th>Publish</th>
                        <th style="text-align:center;width:70px !important;">Action</th>
                    </tr>
                </thead>
                <?php
                if(!is_array($data_skills)){
                    echo "
                    <tbody><td colspan='6'><h5>There is no data.</h5></td></tbody>";
                }else{?>
                <tbody>
                <?php $k=1;
                    foreach($data_skills as $data_skill){
                    if($k >= 1 && $total_data >= 1){
                        $onclickDel = "\"if(window.confirm('Are you sure you want to delete {$data_skill['Skill_name']}?')) location.href='adminMgr-Skill.php?action=delete&skill_ID={$data_skill['Skill_ID']}&skill_Name={$data_skill['Skill_name']}';\"";
                    }
                    ?>
                    <tr class="<?php if($data_skill['Skill_publish'] == 'Not Publish'){echo 'error';}?>">
	                    <td><?php echo ($O_page-1)*20+$k;?></td>
                        <td><?php echo $data_skill['Skill_name'];?></td>
                        <td><?php echo $data_skill['skill_counter'];?></td>
                        <td><a class="proPhoto" href="<?php echo check_image_url($data_skill['Skill_img'], 'skill');?>"><img class="img-circle" src="<?php echo check_image_url($data_skill['Skill_img_thmb'], 'skill');?>" /></a></td>
                        <td><?php echo $data_skill['Skill_publish'];?></td>
                        <td style="text-align: center;">
                            <div class="btn-group-vertical">
                                <div class="btn-group">
                                  <a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
                                    <i class=" icon-cog"></i>
                                  </a>
                                  <ul class="dropdown-menu align-left">
                                    <li><a href="adminMgr-Skill_detail.php?skill_ID=<?php echo $data_skill['Skill_ID'];?>"><i class=" icon-edit"></i> Edit</a></li>
                                    <li><a href="#" <? echo "onclick=$onclickDel";?><?php if($k >= 1 && $total_data >= 1){echo " disabled";}?>><i class=" icon-remove"></i> Delete</a></li>
                                  </ul>
                                </div>
                            </div>
                        </td>
                    </tr>
                <?php $k++; }?>
                </tbody>
                <? }?>
                </table>
                <br/>
                <?php include("part-pagination.php"); ?>
            </div>
        </div>
        <div class="row-fluid">
            <div class="span6">
                <?php require_once("admin-Footer.php"); ?>
            </div>
        </div>
    </div>
    <!--Modal box starts -->  
    <div id="modalSkill" class="modal hide fade">    
        <form name="addSkill" action="adminMgr-Skill.php?action=insert" method="post" enctype="multipart/form-data" onsubmit="loadingText()" onclick="add_zindex();">         
            <div class="modal-header">
                <a href="#close" class="close" data-dismiss="modal" aria-hidden="true">×</a>
                <strong>Create New Skill</strong>
            </div>
            <div class="modal-body">
                <div class="row-fluid">
                    <div class="span4 modal-label"><strong>Skill Name&nbsp;*</strong>:</div>
                    <div class="span8"><input name="name" type="text" class="text input-block-level" placeholder="Skill Name, Max. 48 Character" required="required" /></div>
                </div> 
                <div class="row-fluid">       
                    <div class="span4 modal-label">Skill Image (Optional) :</div>
                    <div class="span8"><input name="image" type="file" class="text input-block-level" maxlength="256" placeholder="skill image"/></div>
                </div>
                <div class="row-fluid">
                    <div class="offset4 span8"><small>image format has to be jpg, jpeg, gif, png.</small></div>
                </div>
                <div class="row-fluid">
                    <div class="span4 modal-label">Skill Publish :</div>
                    <div class="span8">
                        <select name="publish" class="text">
                            <option value="Publish">Publish</option>
                            <option value="Not Publish">Not Publish</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="btn-group">
                    <a href="#" class="btn" data-dismiss="modal"><i class="icon-circle-arrow-left"></i> Close</a>
                     <button id="btn-submit" name="submitSkill" type="submit" class="btn btn-success btn-submit"><i class="icon-ok icon-white"></i> Create</button>
                </div>
            </div>
        </form>
    </div><!--Modal box ends -->

    <script type="text/javascript">
        $("#message1").fadeOut(8400);
        function add_zindex(){
            $('.ui-autocomplete').css("z-index", 1055);
        }
    </script>
    <script src="js/globalJS.js" type="text/javascript"></script>
</body></html>