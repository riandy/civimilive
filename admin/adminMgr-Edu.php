<?php
ob_start("ob_gzhandler");
require_once("packages/require.php");
include("packages/check_login.php");//USED BY ALL PAGE BUT index.php
include("controller/controller_edu.php");
$page_name = "adminMgr-Edu.php";
$letter_page = "letter=$letter";
$type_page = "type=$O_type";
$name_page = "name=$O_name";
$city_page = "city=$O_city";
$country_page = "country=$O_country";
$select_page = "select_edu=$O_select";
$sort_page = "sort=$O_sort";
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <?php include('packages/head.php');?>
    <!-- calendar -->
    <link rel="stylesheet" type="text/css" href="packages/jQuery-Tags/jquery-ui.css" />
    <link rel="stylesheet" type="text/css" href="packages/jQuery-Tags/jquery.tagsinput.css" />
    <script src="//code.jquery.com/jquery-1.10.2.js"></script>

    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.0/themes/smoothness/jquery-ui.css">
    <script src="//code.jquery.com/ui/1.11.1/jquery-ui.js"></script>
    <script src="js/jquery.select-to-autocomplete.js"></script>
    <!-- select 2 -->
    <link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/css/select2.min.css" rel="stylesheet" />
    <script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/select2.min.js"></script>

    <script>
  $(function() {
    $('.autodrop').selectToAutocomplete();
  });
  </script>
    <style>
        body .modal {
            width: 50%; /* desired relative width */
            left: 25%; /* (100%-width)/2 */
            /* place center */
            margin-left:auto;
            margin-right:auto; 
        }
        .select2 {
            width: 100% !important;
            margin-bottom: 10px !important;
        }

        .select2-selection {
            border-color: #ccc !important;
            min-height: 30px !important;
        }
        .select2-search__field {
            padding: 6px !important;
            min-height: 28px;
            margin-bottom: 0px !important;
        }
    </style>
</head>
<body class="civMain" onload="startTime()">
    <?php require_once("admin-Header.php");?>

    <!-- start top nav -->
    <?php $curPage='module'; ?>
    <?php require_once("admin-top_nav.php");?>
    <!-- end top nav -->

    <div class="container">
        <div id="menuDashboard" class="row-fluid">
            <div id="rightContent" class="span12">
                
                <?php
                if($message!=null)
                {
                    echo "<div id='message1' class='alert alert-info'>" . $message . "</div><br />";
                }
                ?>
                <div class="row-fluid">
                    <div class="span6 align-left">
                        <h4 style="margin: 0;">Education (<?php echo $type;?>) Management</h4>
                    </div>
                    <div class="span6 align-right">
                        <button class="btn btn-info" type="button" data-toggle="modal" data-target="#modalFilter"><i class="icon-filter icon-white"></i> Advance Filter</button>
                        <button class="btn btn-info" type="button" data-toggle="modal" data-target="#modalEducation"><i class="icon-plus icon-white"></i> Education</button>
                    </div>
                </div>
                <hr/>
                <?php/*
                <div class="row-fluid">
                    <div class="span12 align-right">
                        Total Data starts with <span class="badge badge-important"><?php if($letter == ''){ echo "A-Z"; }else{ echo strtoupper($letter);}?></span> : <span class="badge badge-info"><?php echo $total_data;?></span><br/>
                        Showing : <span class="badge badge-info"><?php echo (($O_page-1)*20+1)." - ".(count($data_edus)+(($O_page-1)*20));?></span> of <span class="badge badge-info"><?php echo $total_data;?></span> 
                    </div>
                </div>*/?>
                <div class="row-fluid">
                    <div class="span8">
                        <div class="input-append">
                            <select id="auto_search" class="autodrop"  > 
                            <option value=''>start typing...</option>
                                <?php foreach($data_lists as $data_list){ ?>
                                    <option value="<?php echo $data_list['Edu_ID'];?>"><?php echo $data_list['Edu_schoolName'];?></option>
                                    <?php } ?>
                            </select>
                            <!-- <span>please type what you are looking</span> -->
                            <a onclick="goDetail();" class="btn btn-info" style="color: white;">Search</a>
                        </div>
                        <script>
                        function goDetail(){
                            var detail = $('#auto_search').val();
                            if(detail > 0 && isNaN(detail) == false){
                                location.href = "<?=$admin['path-edu_detail'];?>"+detail;
                            }
                        }
                        </script>
                    </div>
                    
                    <div class="span4 align-right">
                        Total Data starts with <span class="badge badge-important"><?php if($letter == ''){ echo "A-Z"; }else{ echo strtoupper($letter);}?></span> : <span class="badge badge-info"><?php echo $total_data;?></span><br/>
                        Showing : <span class="badge badge-info"><?php echo (($O_page-1)*20+1)." - ".(count($data_edus)+(($O_page-1)*20));?></span> of <span class="badge badge-info"><?php echo $total_data;?></span> 
                    </div>
                </div>
                <hr/>
                <!-- STARTS LETTER -->
                <div class="row-fluid">
                    <div class="span-12 align-center">
                        <?php for ($i = 'a', $j = 1; $j <= 26; $i++, $j++) {
                                $letters[$j] = $i;
                    ?>              
                    <a href="<?php echo "adminMgr-Edu.php?page=1&letter=$letters[$j]&$type_page&$name_page&$city_page&$country_page&$select_page&$sort_page";?>" class="btn-mini <?php  if($letter == $letters[$j] ){echo 'active-alphabet';} ?>" ><b><?php echo strtoupper($letters[$j]);?></b></a>&nbsp;
                    <?php } ?>
                    </div>
                </div><!-- ENDS LETTER -->
                <hr>
                <table class="table table-hover">
                <thead>
                    <tr>
			            <th>#</th>
                        <th>Logo</th>
                        <th>School Name</th>
                        <th>City</th>
                        <th>Country</th>
                        <th>
                            Student/Alumni<br>
                            <a href="<?php echo "adminMgr-Edu.php?page=$O_page&$letter_page&$type_page&$name_page&$city_page&$country_page&$select_page&sort=asc";?>"><i class="icon-chevron-up"></i><?php if($O_sort == "asc"){ echo "<span class='badge badge-info'>asc</span>";}else{echo "asc";}?></a>&nbsp;
                            <a href="<?php echo "adminMgr-Edu.php?page=$O_page&$letter_page&$type_page&$name_page&$city_page&$country_page&$select_page&sort=desc";?>"><i class="icon-chevron-down"></i><?php if($O_sort == "desc"){ echo "<span class='badge badge-info'>desc</span>";}else{echo "desc";}?></a>
                        </th>
                        <th>Create Date</th>
                        <th>Publish</th>
                        <th style="text-align:center;width:70px !important;">Action</th>
                    </tr>
                </thead>
                <?php
                if(!is_array($data_edus)){
                    echo "
                    <tbody><td colspan='9'><h5>There is no data.</h5></td></tbody>";
                }else{?>
                <tbody>
                <?php $k=1;
                    foreach($data_edus as $data_edu){
                        $onclickDel = "\"if(window.confirm('Are you sure you want to delete {$data_edu['Edu_schoolName']}?')) location.href='adminMgr-Edu.php?action=delete&page=$O_page&letter_page&$type_page&$name_page&city_page&$country_page&$select_page&$sort_page&edu_ID={$data_edu['Edu_ID']}&edu_school={$data_edu['Edu_schoolName']}';\"";
                    ?>
                    <tr class="<?php if($data_edu['Edu_publish'] == 'Not Publish'){echo 'error';}?>">
	                    <td><?php echo ($O_page-1)*20+$k;?></td>
                        <td><a class="proPhoto" href="<?php echo check_image_url($data_edu['Edu_img'], 'education');?>"><img class="img-circle" src="<?php echo check_image_url($data_edu['Edu_img_thmb'], 'education');?>" /></a></td>
                        <td><?php echo correctDisplay($data_edu['Edu_schoolName']);?></td>
                        <td><?php echo $data_edu['Edu_schoolCity'];?></td>
                        <td><?php echo $data_edu['Edu_schoolCountry'];?></td>
                        <td><?php echo $data_edu['edu_counter'];?></td>
                        <td><?php echo relative_time($data_edu['Edu_create_date']);?></td>
                        <td><?php echo $data_edu['Edu_publish'];?></td>
                        <td style="text-align: center;">
                            <div class="btn-group btn-group-horizontal">  
                                <a href="adminMgr-Edu_user.php?edu_ID=<?php echo $data_edu['Edu_ID']."&type=".$O_type;?>" type="button" class="btn btn-info btn-small" style="color: white;"><i class="icon-user icon-white"></i> view</a>
                                <a href="adminMgr-Edu_detail.php?edu_ID=<?php echo $data_edu['Edu_ID']."&type=".$O_type;?>" type="button" class="btn btn-small"><i class="icon-edit"></i> edit</a>
                                <a href="#" <? echo "onclick=$onclickDel";?> type="button" class="btn btn-danger btn-small" style="color: white;"><i class="icon-trash icon-white"></i> delete</a>
                            </div>
                            <!--<div class="btn-group">
                                <a href="adminMgr-Edu_user.php?edu_ID=<?php echo $data_edu['Edu_ID']."&type=".$O_type;?>"><i class=" icon-search"></i> View Student / Alumni</a>
                                <a href="adminMgr-Edu_detail.php?edu_ID=<?php echo $data_edu['Edu_ID']."&type=".$O_type;?>"><i class=" icon-edit"></i> Edit</a>
                                <a href="#" <? echo "onclick=$onclickDel";?><?php if($k >= 1 && $total_data >= 1){echo " disabled";}?>><i class=" icon-remove"></i> Delete</a>
                            </div>!-->
                        </td>
                    </tr>
                <?php $k++; }?>
                </tbody>
                <? }?>
                </table>
                <br/>
                <!-- START PAGINATION EDUCATION !-->
                <div class="pagination align-center">
                  <ul class="pagination pagination-sm">
                    <?php
                    $batch = getBatch($O_page);
                    if($batch < 1){$batch = 1;}
                    $prevLimit = 1 +(10*($batch-1));
                    $nextLimit = 10 * $batch;
                    if($nextLimit > $total_page){$nextLimit = $total_page;}
                    if ($total_page > 1 && $O_page > 1) {
                        echo "<li><a href='".$page_name."?page=1&".$letter_page."&".$type_page."&".$name_page."&".$city_page."&".$country_page."&".$select_page."&".$sort_page."' class='pagination-slide'>&lt;&lt; First</a></li>";
                    }
                    if($batch > 1 && $O_page > 10){
                        echo "<li><a href='".$page_name."?page=".($prevLimit-1)."&".$letter_page."&".$type_page."&".$name_page."&".$city_page."&".$country_page."&".$select_page."&".$sort_page."' class='pagination-slide'>&lt; Previous 10</a></li>";
                    }
                    for($mon = $prevLimit; $mon <= $nextLimit;$mon++){?>
                        <li class="<?php if($mon == $O_page){echo 'active';}?>"><span class="pagination-theme"><a href="<?php echo $page_name."?page=".$mon."&".$letter_page."&".$type_page."&".$name_page."&".$city_page."&".$country_page."&".$select_page."&".$sort_page;?>" ><?php echo $mon;?></a></span></li>
                        <?php if($mon == $nextLimit && $mon < $total_page){
                            if($mon >= $total_page){ $mon -=1;}
                            echo "<li><a href='".$page_name."?page=".($mon+1)."&".$letter_page."&".$type_page."&".$name_page."&".$city_page."&".$country_page."&".$select_page."&".$sort_page."' class='pagination-slide'>Next 10 &gt;</a></li>";
                        }                               
                    }
                    if ($total_page > 1 &&  ($O_page != $total_page)) {
                        echo "<li><a href='".$page_name."?page=".$total_page."&".$letter_page."&".$type_page."&".$name_page."&".$city_page."&".$country_page."&".$select_page."&".$sort_page."' class='pagination-slide'>Last &gt;&gt;</a></li>";
                    }
                    ?>
                  </ul>
                </div>
                <!-- END PAGINATION EDU !-->
            </div>
        </div>
        <div class="row-fluid">
            <div class="span6">
                <?php require_once("admin-Footer.php"); ?>
            </div>
        </div>
    </div>
    <!--Modal box starts -->  
    <div id="modalEducation" class="modal hide fade">    
        <form name="addEducation" action="adminMgr-Edu.php?action=insert" method="post" enctype="multipart/form-data" onsubmit="loadingText()" onclick="add_zindex();">         
            <div class="modal-header">
                <a href="#close" class="close" data-dismiss="modal" aria-hidden="true">×</a>
                <strong>Create New Education</strong>
            </div>
            <div class="modal-body">
                <!--
                <div class="row-fluid up1">
                    <div class="span4 modal-label"><strong>Industry Title&nbsp;*</strong>:</div>
                    <div class="span8">
                        <select name="industry_id" class="text" required="required">
                            <option value="">Select Industry</option>
                            <?php foreach($data_industrys as $data_industry){ ?>
                            <option value="<?php echo $data_industry['Industry_ID'];?>"><?php echo $data_industry['Industry_title'];?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                !-->
                <div class="row-fluid">
                    <div class="span4 modal-label"><strong>Education Name&nbsp;*</strong>:</div>
                    <div class="span8"><input name="name" type="text" class="text input-block-level" placeholder="School Name" required="required" /></div>
                </div> 
                <div class="row-fluid">
                    <div class="span4 modal-label"><strong>Education City&nbsp;*</strong>:</div>
                    <div class="span8">
                        <!-- <input name="city" type="text" class="text input-block-level" placeholder="School City" required="required" /> -->
                        <select name="city" class="text input-block-level city-select" required="required">
                            <option></option>
                            <?php foreach($data_citys as $data_city){ ?>
                            <option value="<?php echo $data_city['City_title'];?>"><?php echo $data_city['City_title'];?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div> 
                <div class="row-fluid">
                    <div class="span4 modal-label"><strong>Education Country&nbsp;*</strong>:</div>
                    <div class="span8">
                        <!-- <input name="country" type="text" class="text input-block-level" placeholder="School Country" required="required" /> -->
                        <select name="country" class="text input-block-level country-select" required="required">
                            <option></option>
                            <?php foreach($data_countrys as $data_country){ ?>
                            <option value="<?php echo $data_country['Country_title'];?>"><?php echo $data_country['Country_title'];?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div> 
                <div class="row-fluid up1">       
                    <div class="span4 modal-label"><strong>Logo&nbsp;*</strong>:</div>
                    <div class="span8"><input name="image" type="file" class="text input-block-level" maxlength="256" placeholder="company image" required="required" /></div>
                </div>
                <div class="row-fluid">
                    <div class="offset4 span8"><small>image format has to be jpg, jpeg, gif, png.</small></div>
                </div>
                <div class="row-fluid">
                    <div class="span4 modal-label">Education Description EN :</div>
                    <div class="span8"><textarea name="descEN" class="text input-block-level" rows="5" placeholder="Description about education in english"></textarea></div>
                </div>
                <div class="row-fluid">
                    <div class="span4 modal-label">Education Description IN :</div>
                    <div class="span8"><textarea name="descIN" class="text input-block-level" rows="5" placeholder="Description about education in bahasa"></textarea></div>
                </div>  
                <div class="row-fluid up1">
                    <div class="span4 modal-label">Education Publish :</div>
                    <div class="span8">
                        <select name="publish" class="text">
                            <option value="Publish">Publish</option>
                            <option value="Not Publish">Not Publish</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="btn-group">
                    <input type="hidden" name="type" value="<?php echo $O_type;?>">  
                    <a href="#" class="btn" data-dismiss="modal"><i class="icon-remove"></i> Close</a>
                     <button id="btn-submit" name="submitEducation" type="submit" class="btn btn-success btn-submit"><i class="icon-plus-sign icon-white"></i> Create</button>
                </div>
            </div>
        </form>
    </div><!--Modal box ends -->

    <!--Modal box filter starts -->  
    <div id="modalFilter" class="modal hide fade">    
        <form name="filterEducation" action="adminMgr-Edu.php" method="get" onsubmit="loadingText()" onclick="add_zindex();">         
            <div class="modal-header">
                <input type="hidden" name="type" value="<?php echo $O_type;?>">  
                <a href="#close" class="close" data-dismiss="modal" aria-hidden="true">×</a>
                <strong>Advance Filter Education</strong>
            </div>
            <div class="modal-body">
                <div class="row-fluid">
                    <div class="span4 align-right modal-label">Education Name :</div>
                    <div class="span6"><input name="name" type="text" class="text input-block-level" placeholder="School Name" value="<?php echo $O_name;?>" /></div>
                </div>
                <div class="row-fluid">
                    <div class="span4 align-right modal-label">Education City :</div>
                    <div class="span6"><input name="city" type="text" class="text input-block-level" placeholder="School City" value="<?php echo $O_city;?>" /></div>
                </div> 
                <div class="row-fluid">
                    <div class="span4 align-right modal-label">Education Country :</div>
                    <div class="span6"><input name="country" type="text" class="text input-block-level" placeholder="School Country" value="<?php echo $O_country;?>" /></div>
                </div> 
                <div class="row-fluid">
                    <div class="span4 align-right modal-label">Education Select :</div>
                    <div class="span6">
                        <select name="select_edu" class="text">
                            <option <?php if($O_select == "all"){echo "selected=selected";} ?> value="all">All</option>
                            <option <?php if($O_select == "other"){echo "selected=selected";} ?> value="other">Other</option>
                            <option <?php if($O_select == "indo"){echo "selected=selected";} ?> value="indo">Indonesia</option>
                        </select>
                    </div>
                </div>
                <!--<div class="row-fluid up1">
                    <div class="span4 modal-label">School City :</div>
                    <div class="span8">
                        <select name="city" class="text autodrop">
                            <option value="">Select City</option>
                            <?php foreach($data_citys as $data_city){ ?>
                            <option <?php if($O_city == $data_city['City_title']){ echo "selected=selected"; }?> value="<?php echo $data_city['City_title'];?>"><?php echo $data_city['City_title'];?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="row-fluid up1">
                    <div class="span4 modal-label">School Country :</div>
                    <div class="span8">
                        <select name="country" class="text autodrop">
                            <option value="">Select Country</option>
                            <?php foreach($data_countrys as $data_country){ ?>
                            <option <?php if($O_country == $data_country['Country_title']){ echo "selected=selected"; }?> value="<?php echo $data_country['Country_title'];?>"><?php echo $data_country['Country_title'];?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>!-->
            </div>
            <div class="modal-footer">
                <div class="btn-group">
                    <a href="#" class="btn" data-dismiss="modal"><i class="icon-remove"></i> Close</a>
                     <button id="btn-submit" type="submit" class="btn btn-success btn-submit"><i class="icon-filter icon-white"></i> Filter</button>
                </div>
            </div>
        </form>
    </div><!--Modal box filter ends -->
    <script type="text/javascript">
        $("#message1").fadeOut(8400);
        function add_zindex(){
            $('.ui-autocomplete').css("z-index", 1055);
        }
        $(document).ready(function() {
          $(".city-select").select2({
            placeholder: "Select City",
            allowClear: true
          });
          $(".country-select").select2({
            placeholder: "Select Country",
            allowClear: true
          });
        });
    </script>
    <script src="js/globalJS.js" type="text/javascript"></script>
</body></html>