<?php 
include("packages/civ_require.php");
include("controller/controller_civ_global.php");
include("controller/controller_civ_testimoni.php");
$curpage='testimoni';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title><?=$seo['title-testimoni'];?></title>
    <meta name="keywords" content="<?=$seo['keyword-testimoni'];?>">
    <meta name="description" content="<?=$seo['desc-testimoni'];?>">
    <?php include("packages/civ_head.php");?>
</head>
<body>
    <!-- HEADER -->
    <?php include("parts/part-header.php");?>
    <!-- END HEADER -->
    
    <div class="testimoni-part">
        <div class="container container-max">
            <div class="row">
                <div class="col-xs-12">
                    <div class="content-head"><?=$lang['testimoni-head'];?></div> 
                    <div class="content-desc">
                        <?=$lang['testimoni-note'];?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-offset-1 col-md-10 col-sm-12 col-xs-12 col-testimoni">
                    <?php if(is_array($datas)){ ?>
                    <!-- DESKTOP TESTIMONIAL -->
                    <div class="testimoni-desktop hidden-xs">
                        
                        <?php $i=1; foreach ($datas as $data) { ?>

                        <?php if($i%2 == 1){ ?>
                        <!-- RIGHT CONTENT VIEW -->
                        <div class="testicounter-warp">
                            <div class="testicounter-left">
                                <div class="testimoni-text">
                                    "<?=correctDisplay($data['Testimonial_content']);?>"
                                </div>
                                <div class="testimoni-user">
                                    - <?=$data['User_fname']." ".$data['User_lname'];?> -
                                </div>
                                <a href="<?=$path['user-cv'].$data['User_username'].".cvm";?>" class="testimoni-cv">See <?=$data['User_fname'];?>'s CV ></a>
                            </div>
                            <div class="testicounter-right">
                                <?php if($data['Testimonial_img'] != ""){ ?>
                                    <div class="testimoni-image" style="background-image: url('<?=$global['img-url'].$data['Testimonial_img'];?>');background-size:cover;"></div>
                                <?php }else{ ?>
                                    <!-- <div class="testimoni-image" style="background-image: url('<?=$global['img-url'];?>img/basic-img/grey-img-lg.png')"></div>    -->
                                <?php } ?>
                            </div>
                        </div>
                        <?php } else { ?>
                        <!-- LEFT CONTENT VIEW -->
                        <div class="terimoni-warp">
                            <div class="testimoni-left">
                                <?php if($data['Testimonial_img'] != ""){ ?>
                                    <div class="testimoni-image" style="background-image: url('<?=$global['img-url'].$data['Testimonial_img'];?>');background-size:cover;"></div>
                                <?php }else{ ?>
                                    <!-- <div class="testimoni-image" style="background-image: url('<?=$global['img-url'];?>img/basic-img/grey-img-lg.png')"></div>    -->
                                <?php } ?>
                            </div>
                            <div class="testimoni-right">
                                <div class="testimoni-text">
                                    "<?=correctDisplay($data['Testimonial_content']);?>"
                                </div>
                                <div class="testimoni-user">
                                    - <?=$data['User_fname']." ".$data['User_lname'];?> -
                                </div>
                                <a href="<?=$path['user-cv'].$data['User_username'].".cvm";?>" class="testimoni-cv">See <?=$data['User_fname'];?>'s CV ></a>
                            </div>
                        </div>
                        <?php } ?>

                        <?php $i++; } ?>

                    </div>
                    <!-- END DESKTOP TESTIMONIAL -->

                    <!-- MOBILE TESTIMONIAL -->
                    <div class="testimoni-mobile visible-xs">
                        <?php foreach ($datas as $data) { ?>
                        <div class="terimoni-warp">
                            <div class="testimoni-left">
                                <?php if($data['Testimonial_img'] != ""){ ?>
                                    <div class="testimoni-image" style="background-image: url('<?=$global['img-url'].$data['Testimonial_img'];?>');background-size:cover;"></div>
                                <?php }else{ ?>
                                    <!-- <div class="testimoni-image" style="background-image: url('<?=$global['img-url'];?>img/basic-img/grey-img-lg.png')"></div>    -->
                                <?php } ?>
                            </div>
                            <div class="testimoni-right">
                                <div class="testimoni-text">
                                    "<?=correctDisplay($data['Testimonial_content']);?>"
                                </div>
                                <div class="testimoni-user">
                                    - <?=$data['User_fname']." ".$data['User_lname'];?> -
                                </div>
                                <a href="<?=$path['user-cv'].$data['User_username'].".cvm";?>" class="testimoni-cv">See <?=$data['User_fname'];?>'s CV ></a>
                            </div>
                        </div>
                        <?php } ?>
                    </div>
                    <!-- END MOBILE TESTIMONIAL -->
                    <?php } ?>

                </div>
            </div>

            <!-- START PART TESTIMONI -->
            <?php include("parts/part-testimoni.php");?>
            <!-- END PART TESTIMONI -->

        </div>
    </div>
    <!-- START PART FOOTER -->
    <?php include("parts/part-footer.php");?>
    <!-- END PART FOOTER --> 
</body>
</html>