<?php 
include("packages/require.php");
include("controller/controller_global.php");
include("controller/controller_user_profile.php");

$curpage='user_profile';
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title><?=$_SESSION['userData']['username']."'s ".$global['title-profile'];?></title>
  <meta name="keywords" content="">
  <meta name="description" content="">
  <meta name="robots" content="<?=$seo['robot_no'];?>">
  <?php include("packages/head.php");?>
  <link rel="stylesheet" href="<?php echo $global['absolute-url'];?>css/dropzone.css"/>
  <script src="<?php echo $global['absolute-url'];?>js/dropzone.js"></script>
  <link href="<?php echo $global['absolute-url'];?>css/bootstrap-tour.min.css" rel="stylesheet">
  <script src="<?php echo $global['absolute-url'];?>js/bootstrap-tour.min.js"></script>
  <script>
  $(function() { 
    $( "#profile-wrapper" ).sortable({
      placeholder: "ui-state-highlight",
      forcePlaceholderSize: true,
      update: function( event, ui ) {
        saveSort();
      }
  })});

  function saveSort(){
    var element = $('.job-sbox');
    for(var ab = 0; ab < element.length; ab++){
      element.eq(ab).attr("data-order", (ab+1));
      console.log(ab+": "+(ab+1));
    }
        // console.log(element);

        var order = new Array(
          $('#personal-summary-section').attr('data-order'), 
          $('#personal-education-section').attr('data-order'),
          $('#personal-work-section').attr('data-order'),
          $('#personal-skill-section').attr('data-order'),
          $('#personal-cert-section').attr('data-order'), 
          $('#personal-lang-section').attr('data-order'),
          $('#personal-org-section').attr('data-order'),
          $('#personal-achievement-section').attr('data-order')
          );
        // console.log(order);
        var data = {
          data : order
        }

        var url = "<?=$api['user-sort'];?>&user_id=<?=$_SESSION['userData']['id'];?>&auth_code=<?=$_SESSION['userData']['auth_code'];?>";
        $.ajax({type:'POST',url: url,data : data, success:function(result){
          // console.log(result);
        }
      });
        // save_sort();
      }
    </script>
  </head>
  <body>
    <div id="all">
      <!-- start top nav -->
      <?php include("section-top-nav.php");?>
      <!-- end top nav -->
      <div id="user-section">
        
        <div class="container">
          <div id="profile-content">
            <div class="up5 visible-xs"></div>
            <div class="row">
              <div class="col-xs-12 pad0-xs mobile-nav-section">
                <div class="profile-header hidden-xs"><?=$lang['nav-profil'];?></div>
                <div class="row visible-xs mobile-nav-pad">
                  <div class="col-xs-4 mobile-nav-link text-center border-right-ccc"><a class="active" href="<?=$path['user-profile'];?>"><span class="glyphicon glyphicon-user"></span>&nbsp;<br class="visible-xs" />&nbsp;<?=strtoupper($lang['nav-profil']);?></a></div>
                  <div class="col-xs-4 mobile-nav-link text-center border-right-ccc"><a href="<?=$path['user-portfolio'];?>"><span class="glyphicon glyphicon-folder-open"></span>&nbsp;<br class="visible-xs" />&nbsp;<?=strtoupper($lang['nav-portfolio']);?></a></div>
                  <div class="col-xs-4 mobile-nav-link text-center"><a href="<?=$path['user-setting'];?>"><span class="glyphicon glyphicon-cog"></span>&nbsp;<br class="visible-xs" />&nbsp;<?=strtoupper($lang['nav-setting']);?></a></div>
                </div>
              </div>
            </div>
            <div class="row">
              <!-- profile sidebar -->
              <div class="col-sm-3 col-xs-12">
                <!-- start sidebar -->
                <?php include("user_part-sidebar.php");?>
                <!-- end sidebar -->
              </div><!-- profile sidebar -->
              <div id="profile-container" class="pad0 col-sm-9 col-xs-12 up1">
                <!-- start personal -->
                <?php include("user_part-personal.php");?>
                <!-- end personal -->

                <div id="profile-wrapper" class="up1">

                  
                  <!-- start sort order -->
                  <?php foreach ($data_sort as $data) {
                    include ("{$data['page_name']}");
                  } ?>
                  <!-- end sort order -->
                </div>

              </div>
            </div>
          </div>
        </div>
        
      </div>
      <!-- end center content -->
      <!-- start footer section -->
      <?php include("section-footer.php");?>
      <!-- end footer section -->
    </div><!--  end all div -->
    <div class="modal fade" id="welcome" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static">
      <div class="wp-dialog modal-dialog" role="document">
        <div class="modal-content bg-geo">
          <div class="modal-body">
            <div class="wp-text up3">Terima kasih telah bergabung di <br/>civimi.com</div>
            <div class="w-btn"><a href="javascript:;" class="btn btn-info" onclick="openTutorial();" aria-label="Close" style="padding: 6px 25px;font-size: 14px;">Next &raquo;</a></div>
          </div>
        </div>
      </div>
    </div>
    <script src="<?php echo $global['absolute-url'];?>js/function.js"></script>
    <script type="text/javascript">
    // TOUR PERSONAL
    var tourPersonals = new Tour({
      onEnd: function (tour) {
        $("#personal-profile").removeClass("disabled");
        $("#personal-edit").removeClass("disabled");
        if($("#personal-profile").hasClass("hide")){
          $("#personal-edit-box").addClass("hide");
          $("#personal-profile").removeClass("hide");
        }
      },
      steps: [
      {
        element: "#edit-personal",
        title: "<div class='tour-title'><?=$tour['personal-1-title'];?></div>",
        content: "<?=$tour['personal-1-content'];?>",
        placement: "left",
        backdrop: true,
        onShow: function (tourPersonals) {
          $("#personal-profile").addClass("disabled");
          $("#personal-edit").addClass("disabled");
        },
        onNext: function (tourPersonals) {
          if($("#personal-edit-box").hasClass("hide")){
            $("#personal-profile").addClass("hide");
            $("#personal-edit-box").removeClass("hide");
          }
        },
      },
      {
        element: "#personal-form-edit",
        title: "<div class='tour-title'><?=$tour['personal-2-title'];?></div>",
        content: "<?=$tour['personal-2-content'];?>",
        placement: "bottom",
        backdrop: true,
        onPrev: function (tourPersonals) {
          $("#personal-edit-box").addClass("hide");
          $("#personal-profile").removeClass("hide");
        },
      },
      {
        element: "#drop-personal",
        title: "<div class='tour-title'><?=$tour['personal-3-title'];?></div>",
        content: "<?=$tour['personal-3-content'];?>",
        placement: "bottom",
        backdrop: true,
      }
      ]
    });
    // TOUR SUMMARY
    var tourSummarys = new Tour({
      onEnd: function (tourSummarys) {
        $("#personal-summary-section").removeClass("disabled");
        if($(".summary-box").hasClass("hide")){
          $(".summary-edit-box").addClass("hide");
          $(".summary-box").removeClass("hide");
        }
      },
      steps: [
        {
        element: "#personal-summary-section",
        title: "<div class='tour-title'><?=$tour['summary-1-title'];?></div>",
        content: "<?=$tour['summary-1-content'];?>",
        placement: "top",
        backdrop: true,
        onShow: function (tourSummarys) {
          $("#personal-summary-section").addClass("disabled");
        },
        onNext: function (tourSummarys) {
          if($(".summary-edit-box").hasClass("hide")){
            $(".summary-box").addClass("hide");
            $(".summary-edit-box").removeClass("hide");
          }
        },
      },
      {
        element: "#summary-edit-box",
        title: "<div class='tour-title'><?=$tour['summary-2-title'];?></div>",
        content: "<?=$tour['summary-2-content'];?>",
        placement: "top",
        backdrop: true,
        onPrev: function (tourSummarys) {
          $(".summary-edit-box").addClass("hide");
          $(".summary-box").removeClass("hide");
        },
        onEnd: function (tourSummarys) {
          $(".summary-edit-box").addClass("hide");
          $(".summary-box").removeClass("hide");
        },
      }
      ]
    });
    // TOUR EDUCATION
    var tourEducations = new Tour({
      onEnd: function (tourEducations) {
        $("#personal-education-section").removeClass("disabled");
        if($("#slide-education").hasClass("in")){
          $("#slide-education").removeClass("in");
        }
      },
      steps: [
      {
        element: "#personal-education-section",
        title: "<div class='tour-title'><?=$tour['edu-1-title'];?></div>",
        content: "<?=$tour['edu-1-content'];?>",
        placement: "top",
        backdrop: true,
        onShow: function (tourEducations) {
          $("#personal-education-section").addClass("disabled");
        },
      },
      {
        element: "#add-education",
        title: "<div class='tour-title'><?=$tour['edu-2-title'];?></div>",
        content: "<?=$tour['edu-2-content'];?>",
        placement: "left",
        backdrop: true,
        onNext: function (tourEducations) {
          $("#slide-education").addClass("in");
        },
      },
      {
        element: "#slide-education",
        title: "<div class='tour-title'><?=$tour['edu-3-title'];?></div>",
        content: "<?=$tour['edu-3-content'];?>",
        placement: "top",
        backdrop: true,
        onPrev: function (tourEducations) {
          $("#slide-education").removeClass("in");
        },
      },
      ]
    });
    // TOUR WORK
    var tourWorks = new Tour({
      onEnd: function (tourWorks) {
        $("#personal-work-section").removeClass("disabled");
        if($("#slide-work").hasClass("in")){
          $("#slide-work").removeClass("in");
        }
      },
      steps: [
      {
        element: "#personal-work-section",
        title: "<div class='tour-title'><?=$tour['work-1-title'];?></div>",
        content: "<?=$tour['work-1-content'];?>",
        placement: "top",
        backdrop: true,
        onShow: function (tourWorks) {
          $("#personal-work-section").addClass("disabled");
        },
      },
      {
        element: "#add-work",
        title: "<div class='tour-title'><?=$tour['work-2-title'];?></div>",
        content: "<?=$tour['work-2-content'];?>",
        placement: "left",
        backdrop: true,
        onNext: function (tourWorks) {
          $("#slide-work").addClass("in");
        },
      },
      {
        element: "#slide-work",
        title: "<div class='tour-title'><?=$tour['work-3-title'];?></div>",
        content: "<?=$tour['work-3-content'];?>",
        placement: "top",
        backdrop: true,
        onPrev: function (tourWorks) {
          $("#slide-work").removeClass("in");
        },
      },
      ]
    });
    // TOUR SKILL
    var tourSkills = new Tour({
      onEnd: function (tourSkills) {
        $("#personal-skill-section").removeClass("disabled");
        if($("#slide-skill").hasClass("in")){
          $("#slide-skill").removeClass("in");
        }
      },
      steps: [
      {
        element: "#personal-skill-section",
        title: "<div class='tour-title'><?=$tour['skill-1-title'];?></div>",
        content: "<?=$tour['skill-1-content'];?>",
        placement: "top",
        backdrop: true,
        onShow: function (tourSkills) {
          $("#personal-skill-section").addClass("disabled");
        },
      },
      {
        element: "#add-skill",
        title: "<div class='tour-title'><?=$tour['skill-2-title'];?></div>",
        content: "<?=$tour['skill-2-content'];?>",
        placement: "left",
        backdrop: true,
        onNext: function (tourSkills) {
          $("#slide-skill").addClass("in");
        },
      },
      {
        element: "#slide-skill",
        title: "<div class='tour-title'><?=$tour['skill-3-title'];?></div>",
        content: "<?=$tour['skill-3-content'];?>",
        placement: "top",
        backdrop: true,
        onPrev: function (tourSkills) {
          $("#slide-skill").removeClass("in");
        },
      },
      ]
    });
    // TOUR LANGUAGE
    var tourLanguages = new Tour({
      onEnd: function (tourLanguages) {
        $("#personal-lang-section").removeClass("disabled");
        if($("#slide-language").hasClass("in")){
          $("#slide-language").removeClass("in");
        }
      },
      steps: [
      {
        element: "#personal-lang-section",
        title: "<div class='tour-title'><?=$tour['lang-1-title'];?></div>",
        content: "<?=$tour['lang-1-content'];?>",
        placement: "top",
        backdrop: true,
        onShow: function (tourLanguages) {
          $("#personal-lang-section").addClass("disabled");
        },
      },
      {
        element: "#add-language",
        title: "<div class='tour-title'><?=$tour['lang-2-title'];?></div>",
        content: "<?=$tour['lang-2-content'];?>",
        placement: "left",
        backdrop: true,
        onNext: function (tourLanguages) {
          $("#slide-language").addClass("in");
        },
      },
      {
        element: "#slide-language",
        title: "<div class='tour-title'><?=$tour['lang-3-title'];?></div>",
        content: "<?=$tour['lang-3-content'];?>",
        placement: "top",
        backdrop: true,
        onPrev: function (tourLanguages) {
          $("#slide-language").removeClass("in");
        },
      },
      ]
    });
    // TOUR CERTIFICATE
    var tourCertificates = new Tour({
      onEnd: function (tourCertificates) {
        $("#personal-cert-section").removeClass("disabled");
        if($("#slide-cert").hasClass("in")){
          $("#slide-cert").removeClass("in");
        }
      },
      steps: [
      {
        element: "#personal-cert-section",
        title: "<div class='tour-title'><?=$tour['cert-1-title'];?></div>",
        content: "<?=$tour['cert-1-content'];?>",
        placement: "top",
        backdrop: true,
        onShow: function (tourCertificates) {
          $("#personal-cert-section").addClass("disabled");
        },
      },
      {
        element: "#add-cert",
        title: "<div class='tour-title'><?=$tour['cert-2-title'];?></div>",
        content: "<?=$tour['cert-2-content'];?>",
        placement: "left",
        backdrop: true,
        onNext: function (tourCertificates) {
          $("#slide-cert").addClass("in");
        },
      },
      {
        element: "#slide-cert",
        title: "<div class='tour-title'><?=$tour['cert-3-title'];?></div>",
        content: "<?=$tour['cert-3-content'];?>",
        placement: "top",
        backdrop: true,
        onPrev: function (tourCertificates) {
          $("#slide-cert").removeClass("in");
        },
      },
      ]
    });
    // TOUR ORGANIZATION
    var tourOrganizations = new Tour({
      onEnd: function (tourOrganizations) {
        $("#personal-org-section").removeClass("disabled");
        if($("#slide-org").hasClass("in")){
          $("#slide-org").removeClass("in");
        }
      },
      steps: [
      {
        element: "#personal-org-section",
        title: "<div class='tour-title'><?=$tour['org-1-title'];?></div>",
        content: "<?=$tour['org-3-content'];?>",
        placement: "top",
        backdrop: true,
        onShow: function (tourOrganizations) {
          $("#personal-org-section").addClass("disabled");
        },
      },
      {
        element: "#add-org",
        title: "<div class='tour-title'><?=$tour['org-2-title'];?></div>",
        content: "<?=$tour['org-2-content'];?>",
        placement: "left",
        backdrop: true,
        onNext: function (tourOrganizations) {
          $("#slide-org").addClass("in");
        },
      },
      {
        element: "#slide-org",
        title: "<div class='tour-title'><?=$tour['org-3-title'];?></div>",
        content: "<?=$tour['org-3-content'];?>",
        placement: "top",
        backdrop: true,
        onPrev: function (tourOrganizations) {
          $("#slide-org").removeClass("in");
        },
      },
      ]
    });
    // TOUR ACHIEVEMENT
    var tourAchievements = new Tour({
      onEnd: function (tourAchievements) {
        $("#personal-achievement-section").removeClass("disabled");
        if($("#slide-achievement").hasClass("in")){
          $("#slide-achievement").removeClass("in");
        }
      },
      steps: [
      {
        element: "#personal-achievement-section",
        title: "<div class='tour-title'><?=$tour['achieve-1-title'];?></div>",
        content: "<?=$tour['achieve-1-content'];?>",
        placement: "top",
        backdrop: true,
        onShow: function (tourAchievements) {
          $("#personal-achievement-section").addClass("disabled");
        },
      },
      {
        element: "#add-achievement",
        title: "<div class='tour-title'><?=$tour['achieve-2-title'];?></div>",
        content: "<?=$tour['achieve-2-content'];?>",
        placement: "left",
        backdrop: true,
        onNext: function (tourAchievements) {
          $("#slide-achievement").addClass("in");
        },
      },
      {
        element: "#slide-achievement",
        title: "<div class='tour-title'><?=$tour['achieve-3-title'];?></div>",
        content: "<?=$tour['achieve-3-content'];?>",
        placement: "top",
        backdrop: true,
        onPrev: function (tourAchievements) {
          $("#slide-achievement").removeClass("in");
        },
      },
      ]
    });
    function tourPersonal() {
      tourPersonals.init();
      tourPersonals.restart();
    }
    function tourSummary() {
      tourSummarys.init();
      tourSummarys.restart();
    }
    function tourEducation() {
      tourEducations.init();
      tourEducations.restart();
    }
    function tourWork() {
      tourWorks.init();
      tourWorks.restart();
    }
    function tourSkill() {
      tourSkills.init();
      tourSkills.restart();
    }
    function tourLanguage() {
      tourLanguages.init();
      tourLanguages.restart();
    }
    function tourCertificate() {
      tourCertificates.init();
      tourCertificates.restart();
    }
    function tourOrganization() {
      tourOrganizations.init();
      tourOrganizations.restart();
    }
    function tourAchievement() {
      tourAchievements.init();
      tourAchievements.restart();
    }
    // TOUR WELCOME
    var tour = new Tour({
      onEnd: function (tour) {
        $("#profile-container").removeClass("disabled");
        $("#top-sidebar").removeClass("disabled");
        $("#bottom-sidebar").removeClass("disabled");
      },
      steps: [
      {
        element: "#profile-container",
        title: "<div class='tour-title'><?=$tour['profil-1-title'];?></div>",
        content: "<div class='text-center'><?=$tour['profil-1-content'];?></div>",
        placement: "left",
        backdrop: true,
        onShow: function (tour) {
          $("#profile-container").addClass("disabled");
        },
        onNext: function (tour) {
          $("#profile-container").removeClass("disabled");
        },
      },
      {
        element: "#top-sidebar",
        title: "<div class='tour-title'><?=$tour['profil-2-title'];?></div>",
        content: "<div class='text-center'><?=$tour['profil-2-content'];?></div>",
        placement: "right",
        backdrop: true,
        onShow: function (tour) {
          $("#top-sidebar").addClass("disabled");
        },
        onNext: function (tour) {
          $("#top-sidebar").removeClass("disabled");
        },
      },
      {
        element: "#bottom-sidebar", 
        title: "<div class='tour-title'><?=$tour['profil-3-title'];?></div>",
        content: "<div class='text-center'><?=$tour['profil-3-content'];?></div>",
        placement: "right",
        backdrop: true,
        onShow: function (tour) {
          $("#bottom-sidebar").addClass("disabled");
        },
        onNext: function (tour) {
          $("#bottom-sidebar").removeClass("disabled");
        },
      },
      ]
    });
    // END TOUR WELCOME

    $(document).ready(function(){
        // Tutorial Welcome Part
        if($(window).width() > 1200) {
          //$('#welcome').modal('show');//FIRST TUTORIAL
        }
        tour.end(); // reset tour welcome on open
        tourPersonals.end(); // reset tour personal on open
        tourSummarys.end(); // reset tour summary on open
        tourEducations.end(); // reset tour education on open
        tourWorks.end(); // reset tour work on open
        tourSkills.end(); // reset tour skill on open
        tourLanguages.end(); // reset tour languages on open
        tourCertificates.end(); // reset tour certificate on open
        tourOrganizations.end(); // reset tour organization on open
        tourAchievements.end(); // reset tour achievement on open
    });

    //START WELCOME TUTORIAL
    function openTutorial(){
      $('#welcome').modal('hide');
      tour.init();
      tour.restart();
    }
    //END WELCOME TUTORIAL

    
    // $(window).on("load", function () {
    //   var urlHash = window.location.href.split("#")[1];
    //   $('html,body').animate({
    //     scrollTop: $('#' + urlHash).offset().top-42
    //   }, 1000);
    // });
    </script>
  </body>
  </html>
