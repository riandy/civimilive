<div id="popular-profile-content">
    <div class="container container-max">
        <div class="row">
            <div class="col-xs-12">
                <div class="popular-profile-header"><?=$lang['home-pop_univ'];?></div>
                <div class="popular-profile-header-child"><?=$lang['home-pop_univ_note'];?></div>
            </div>
        </div>
        <div id="swiper-univ" class="swiper-container height-auto hidden-xs">
            <div class="swiper-wrapper height-auto">
                <?php foreach($data_edus as $data_edu){ ?>
                <div class="swiper-slide swiper-slide-desktop height-auto">
                    <div class="col-xs-12 col-popular-profile">
                        <a href="#" title="<?=$data_edu['Edu_schoolName'].' '.$data_edu['Edu_schoolCity'].' - '.$data_edu['Edu_schoolCountry'];?>" class="popular-profile-box">
                            <div class="popular-profile-img"><img src="<?php if($data_edu['Edu_img_thmb'] != ""){ echo $global['img-url'].$data_edu['Edu_img_thmb'];} else { echo $global['absolute-url']."img/education.png";}?>" alt="<?=$data_edu['Edu_schoolName'].' '.$data_edu['Edu_schoolCountry'].' '.$data_edu['Edu_schoolCity'];?>"></div>
                            <div class="popular-profile-name"><?=charLength($data_edu['Edu_schoolName'], 50);?></div>
                            <div class="popular-profile-job"><?=$data_edu['Edu_schoolCity'].', '.$data_edu['Edu_schoolCountry'];?></div>
                        </a>
                    </div>
                </div>
                <?php } ?>
            </div>
        </div>
        <div id="swiper-univ-xs" class="swiper-container height-auto visible-xs">
            <div class="swiper-wrapper height-auto">
                <?php foreach($data_edus as $data_edu){ ?>
                <div class="swiper-slide swiper-slide-desktop height-auto">
                    <div class="col-xs-12 col-popular-profile">
                        <a href="#" title="<?=$data_edu['Edu_schoolName'].' '.$data_edu['Edu_schoolCity'].' - '.$data_edu['Edu_schoolCountry'];?>" class="popular-profile-box">
                            <div class="popular-profile-img"><img src="<?php if($data_edu['Edu_img_thmb'] != ""){ echo $global['img-url'].$data_edu['Edu_img_thmb'];} else { echo $global['absolute-url']."img/education.png";}?>" alt="<?=$data_edu['Edu_schoolName'].' '.$data_edu['Edu_schoolCountry'].' '.$data_edu['Edu_schoolCity'];?>"></div>
                            <div class="popular-profile-name"><?=charLength($data_edu['Edu_schoolName'], 50);?></div>
                            <div class="popular-profile-job"><?=$data_edu['Edu_schoolCity'].', '.$data_edu['Edu_schoolCountry'];?></div>
                        </a>
                    </div>
                </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>