<?php 
include("packages/require.php");
include("controller/controller_global.php");
include("controller/controller_home.php");
$curpage='main';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title><?=$global['title-home'];?></title>
    <meta name="keywords" content="<?=$seo['keyword-home'];?>">
    <meta name="description" content="<?=$seo['desc-home'];?>">
    <meta name="robots" content="<?=$seo['robot_yes'];?>">
    <?php include("packages/head.php");?>
    <link rel="stylesheet" href="<?php echo $global['absolute-url'];?>stylesheets/main-global-style.css"/>
    <link rel="stylesheet" href="<?php echo $global['absolute-url'];?>css/swiper.min.css"/>
    <script src="<?php echo $global['absolute-url'];?>js/swiper.min.js"></script>

    <style type="text/css">
    .e-mail:before {
        content: attr(data-website) "\0040" attr(data-user);
        unicode-bidi: bidi-override;
        direction: rtl;
    }
    </style>
</head>
<body>
    <div id="all">
        <div class="top-content">
            <div class="top-nav-notif hidden-xs">
                <small><?=$lang['home-notice'];?></small>
            </div>
            <div class="top-wrapper">
                <div class="container">
                    <!-- <div class="row up7 visible-xs">
                        <div class="col-xs-12">
                            <div class="alert alert-info" role="alert" style="font-size:15px;">
                                <span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span>
                                <small><?=$lang['home-notice'];?></small>
                            </div>
                        </div>
                    </div> -->
                </div>
                <!-- start top nav -->
                <?php include("section-top-nav.php");?>
                <!-- end top nav -->
                <div class="container container-header-cvm">
                    <div class="index-top-header"><?=$lang['home-header'];?></div>
                    <div class="index-top-header-child"><?=$lang['home-note'];?></div>
                    <div class="up3"></div>
                </div>
                <!-- start comment section -->
                <div id="swiper-comment" class="swiper-container container container-comment hidden-xs" style="padding: 0 20px !important;">
                    <div class="swiper-wrapper height-auto">
                      <?php $num = 1; $top = 0 ;foreach($data_feats as $data_feat){ ?>
                      <div class="swiper-slide height-auto">
                        <?php if($num % 2 == 0){ ?>
                        <div class="index-content-comment-right" style="top:<?=$top;?>px;">
                            <div class="comment-message">
                                <div class="comment-text hidden-xs"><?=charLength($data_feat['User_obj'],100);?></div>
                                <div class="comment-text visible-xs"><?=charLength($data_feat['User_obj'],55);?></div>
                                <div class="comment-see"><a href="<?=$path['user-profile'].$data_feat['User_username'].".cvm";?>">see details</a></div>
                            </div>
                            <div class="comment-profile">
                                <a href="<?=$path['user-profile'].$data_feat['User_username'].".cvm";?>">
                                    <span class="angle-right"></span>
                                    <img src="<?php if($data_feat['User_proPhotoThmb'] != ""){ echo $global['img-url'].$data_feat['User_proPhotoThmb'];} else { echo $global['absolute-url']."img/basic-img/user.jpg";}?>" alt="<?=$data_feat['User_fname'].' '.$data_feat['User_lname'];?>" class="comment-avatar">
                                    <div class="comment-name"><?=$data_feat['User_fname'];?></div>
                                </a>
                            </div>
                        </div> 
                        <?php } else { ?>
                        <div class="index-content-comment" style="top:<?=$top;?>px;">
                            <div class="comment-profile">
                                <a href="<?=$path['user-profile'].$data_feat['User_username'].".cvm";?>">
                                    <span class="angle"></span>
                                    <img src="<?php if($data_feat['User_proPhotoThmb'] != ""){ echo $global['img-url'].$data_feat['User_proPhotoThmb'];} else { echo $global['absolute-url']."img/basic-img/user.jpg";}?>" alt="<?=$data_feat['User_fname'].' '.$data_feat['User_lname'];?>" class="comment-avatar">
                                    <div class="comment-name"><?=$data_feat['User_fname'];?></div>
                                </a>
                            </div>
                            <div class="comment-message">
                                <div class="comment-text hidden-xs"><?=charLength($data_feat['User_obj'],100);?></div>
                                <div class="comment-text visible-xs"><?=charLength($data_feat['User_obj'],55);?></div>
                                <div class="comment-see" style="text-align:left;"><a href="<?=$path['user-profile'].$data_feat['User_username'].".cvm";?>">see details</a></div>
                            </div>
                        </div>
                        <?php } ?>
                    </div>
                    <?php $top += 65; $num++;} ?>
                </div>
            </div>
            <div id="swiper-comment-mobile" class="swiper-container container container-comment visible-xs" style="padding: 0 20px !important;">
                <div class="swiper-wrapper height-auto">
                  <?php $num = 1; $top = 0 ;foreach($data_feats as $data_feat){ ?>
                  <div class="swiper-slide height-auto">
                    <?php if($num % 2 == 0){ ?>
                    <div class="index-content-comment-right" style="top:<?=$top;?>px;">
                        <div class="comment-message">
                            <div class="comment-text hidden-xs"><?=charLength($data_feat['User_obj'],100);?></div>
                            <div class="comment-text visible-xs"><?=charLength($data_feat['User_obj'],55);?></div>
                            <div class="comment-see"><a href="<?=$path['user-profile'].$data_feat['User_username'].".cvm";?>">see details</a></div>
                        </div>
                        <div class="comment-profile">
                            <a href="<?=$path['user-profile'].$data_feat['User_username'].".cvm";?>">
                                <span class="angle-right"></span>
                                <img src="<?php if($data_feat['User_proPhotoThmb'] != ""){ echo $global['img-url'].$data_feat['User_proPhotoThmb'];} else { echo $global['absolute-url']."img/basic-img/user.jpg";}?>" alt="<?=$data_feat['User_fname'].' '.$data_feat['User_lname'];?>" class="comment-avatar">
                                <div class="comment-name"><?=$data_feat['User_fname'];?></div>
                            </a>
                        </div>
                    </div> 
                    <?php } else { ?>
                    <div class="index-content-comment" style="top:<?=$top;?>px;">
                        <div class="comment-profile">
                            <a href="<?=$path['user-profile'].$data_feat['User_username'].".cvm";?>">
                                <span class="angle"></span>
                                <img src="<?php if($data_feat['User_proPhotoThmb'] != ""){ echo $global['img-url'].$data_feat['User_proPhotoThmb'];} else { echo $global['absolute-url']."img/basic-img/user.jpg";}?>" alt="<?=$data_feat['User_fname'].' '.$data_feat['User_lname'];?>" class="comment-avatar">
                                <div class="comment-name"><?=$data_feat['User_fname'];?></div>
                            </a>
                        </div>
                        <div class="comment-message">
                            <div class="comment-text hidden-xs"><?=charLength($data_feat['User_obj'],100);?></div>
                            <div class="comment-text visible-xs"><?=charLength($data_feat['User_obj'],55);?></div>
                            <div class="comment-see" style="text-align:left;"><a href="<?=$path['user-profile'].$data_feat['User_username'].".cvm";?>">see details</a></div>
                        </div>
                    </div>
                    <?php } ?>
                </div>
                <?php $top += 65; $num++;} ?>
            </div>
        </div>
        <!-- end comment section -->
    </div><!--  end wrapper -->
</div><!--  end top content -->
<div id="center-content">
    <!-- start popular profile -->
    <div id="popular-profile-content">
        <div class="container container-max">
            <div class="row">
                <div class="col-xs-12">
                    <div class="popular-profile-header"><?=$lang['home-pop_profil'];?></div>
                    <div class="popular-profile-header-child"><?=$lang['home-pop_profil_note'];?></div>
                </div>
            </div>
            <div class="row row-popular-profile hidden-xs">
                <?php foreach($data_pops as $data_pop){ ?>
                <div class="col-xs-2 col-popular-profile">
                    <a href="<?=$path['user-profile'].$data_pop['User_username'].".cvm";?>" title="<?=$data_pop['User_fname'].' '.$data_pop['User_lname'].' - '.$data_pop['User_bioDesc'];?>" class="popular-profile-box">
                        <div class="popular-profile-img"><img src="<?php if($data_pop['User_proPhotoThmb'] != ""){ echo $global['img-url'].$data_pop['User_proPhotoThmb'];} else { echo $global['absolute-url']."img/basic-img/user.jpg";}?>" alt="<?=$data_pop['User_fname'].' '.$data_pop['User_lname'];?>"></div>
                        <div class="popular-profile-name"><?=$data_pop['User_fname'].' '.$data_pop['User_lname'];?></div>
                        <div class="popular-profile-job"><?=charLength($data_pop['User_bioDesc'], 16);?></div>
                    </a>
                </div>
                <?php } ?>
            </div>
            <div id="swiper-profile" class="swiper-container height-auto visible-xs">
                <div class="swiper-wrapper height-auto">
                    <?php foreach($data_pops as $data_pop){ ?>
                    <div class="swiper-slide swiper-slide-desktop height-auto">
                        <div class="col-xs-12 col-popular-profile">
                            <a href="<?=$path['user-profile'].$data_pop['User_username'].".cvm";?>" title="<?=$data_pop['User_fname'].' '.$data_pop['User_lname'].' - '.$data_pop['User_bioDesc'];?>" class="popular-profile-box">
                                <div class="popular-profile-img"><img src="<?php if($data_pop['User_proPhotoThmb'] != ""){ echo $global['img-url'].$data_pop['User_proPhotoThmb'];} else { echo $global['absolute-url']."img/basic-img/user.jpg";}?>" alt="<?=$data_pop['User_fname'].' '.$data_pop['User_lname'];?>"></div>
                                <div class="popular-profile-name"><?=$data_pop['User_fname'].' '.$data_pop['User_lname'];?></div>
                                <div class="popular-profile-job"><?=charLength($data_pop['User_bioDesc'], 16);?></div>
                            </a>
                        </div>
                    </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div><!-- end popular profile -->

    <!-- start popular university -->
    <div id="popular-profile-content">
        <div class="container container-max">
            <div class="row">
                <div class="col-xs-12">
                    <div class="popular-profile-header"><?=$lang['home-pop_univ'];?></div>
                    <div class="popular-profile-header-child"><?=$lang['home-pop_univ_note'];?></div>
                </div>
            </div>
            <div id="swiper-univ" class="swiper-container height-auto hidden-xs">
                <div class="swiper-wrapper height-auto">
                    <?php foreach($data_edus as $data_edu){ ?>
                    <div class="swiper-slide swiper-slide-desktop height-auto">
                        <div class="col-xs-12 col-popular-profile">
                            <a href="#" title="<?=$data_edu['Edu_schoolName'].' '.$data_edu['Edu_schoolCity'].' - '.$data_edu['Edu_schoolCountry'];?>" class="popular-profile-box">
                                <div class="popular-profile-img"><img src="<?php if($data_edu['Edu_img_thmb'] != ""){ echo $global['img-url'].$data_edu['Edu_img_thmb'];} else { echo $global['absolute-url']."img/education.png";}?>" alt="<?=$data_edu['Edu_schoolName'].' '.$data_edu['Edu_schoolCountry'].' '.$data_edu['Edu_schoolCity'];?>"></div>
                                <div class="popular-profile-name"><?=charLength($data_edu['Edu_schoolName'], 50);?></div>
                                <div class="popular-profile-job"><?=$data_edu['Edu_schoolCity'].', '.$data_edu['Edu_schoolCountry'];?></div>
                            </a>
                        </div>
                    </div>
                    <?php } ?>
                </div>
            </div>
            <div id="swiper-univ-xs" class="swiper-container height-auto visible-xs">
                <div class="swiper-wrapper height-auto">
                    <?php foreach($data_edus as $data_edu){ ?>
                    <div class="swiper-slide swiper-slide-desktop height-auto">
                        <div class="col-xs-12 col-popular-profile">
                            <a href="#" title="<?=$data_edu['Edu_schoolName'].' '.$data_edu['Edu_schoolCity'].' - '.$data_edu['Edu_schoolCountry'];?>" class="popular-profile-box">
                                <div class="popular-profile-img"><img src="<?php if($data_edu['Edu_img_thmb'] != ""){ echo $global['img-url'].$data_edu['Edu_img_thmb'];} else { echo $global['absolute-url']."img/education.png";}?>" alt="<?=$data_edu['Edu_schoolName'].' '.$data_edu['Edu_schoolCountry'].' '.$data_edu['Edu_schoolCity'];?>"></div>
                                <div class="popular-profile-name"><?=charLength($data_edu['Edu_schoolName'], 50);?></div>
                                <div class="popular-profile-job"><?=$data_edu['Edu_schoolCity'].', '.$data_edu['Edu_schoolCountry'];?></div>
                            </a>
                        </div>
                    </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div><!-- end popular university -->

            <!-- start signup content -->
            <div id="index-signup-content">
                <div class="container container-max">
                    <form name="register" action="<?=$path['register'];?>" method="post" enctype="multipart/form-data" onsubmit="return reinventValid()">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="index-signup-header"><h2><strong><?=$lang['home-reinvent'];?></strong></h2></div>
                                <div class="index-signup-header"><?=$lang['home-reinvent_note'];?></div>
                            </div>
                        </div>
                        <div class="row up1">
                            <div class="col-xs-10 col-xs-offset-1 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4">
                                <input id="reinvent" type="email" class="form-control form-textbox" name="email" placeholder="your email address">
                            </div>
                        </div>
                        <div class="row up1">
                            <div class="col-xs-12 text-center">
                                <button class="btn btn-signup1" type="submit"><?=$lang['home-reinvent_btn'];?></button>
                            </div>
                        </div>
                    </form>
                </div>
        </div>
    <script type="text/javascript">
    function reinventValid(){
        var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        var email = $("#reinvent").val();

                    // validation email
                    if(email != ""){
                        if(email.match(mailformat)) {
                            $("#reinvent").removeClass('input-error');
                        } else {  
                            $("#reinvent").addClass('input-error');
                            $("#reinvent").focus();
                            var message = "Your format email is incorect!";
                            popup(message);
                            return false;  
                        }
                    } else {
                        $("#reinvent").addClass('input-error');
                        $("#reinvent").focus();
                        var message = "Insert your email to process!";
                        popup(message);
                        return false;
                    } //end validation email
                    return true;
                }
                </script>
                <!-- end signup content -->
                <!-- start popular job -->
            <!-- <div id="popular-job-content">
                <div class="container container-job">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="popular-job-header">Most Popular Jobs</div>
                            <div class="popular-job-header-child">Here are the current most popular jobs.</div>
                        </div>
                    </div>
                    <div class="row row-popular-job hidden-xs">
                        <?php for($j=1;$j<=5;$j++){?>
                        <div class="col-xs-12 col-popular-job">
                            <div class="popular-job-box" style="background-image:url('img/jobs.jpg')">
                                <div class="popular-job-text">
                                    <div class="popular-job-title">IT Infrastructure Developer</div>
                                    <a href="" class="popular-job-link">see openings</a>
                                </div>
                            </div>
                        </div>
                        <?php } ?>
                    </div>
                    <div id="swiper-job" class="swiper-container height-auto visible-xs">
                        <div class="swiper-wrapper height-auto">
                            <?php for($p=1;$p<=6;$p++) { ?>
                            <div class="swiper-slide swiper-slide-desktop height-auto">
                                <div class="col-xs-12 col-popular-job">
                                    <div class="popular-job-box" style="background-image:url('img/jobs.jpg')">
                                        <div class="popular-job-text">
                                            <div class="popular-job-title">IT Infrastructure Developer</div>
                                            <a href="" class="popular-job-link">see openings</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="row up3">
                        <div class="col-xs-12">
                            <a href="" class="popular-job-all-link">see all job openings</a>
                        </div>
                    </div>
                </div>
            </div> -->
            <!-- end popular job -->
        </div><!-- end center content -->
        <!-- start footer section -->
        <?php include("section-footer.php");?>
        <!-- end footer section -->
    </div><!--  end all div -->

    <!-- popup alert -->
    <div id="alert-popup" class="popup">
      <div id="popup-text" class="popup-text"></div>
      <a id="popup-close" href="javascript:;" class="popup-close"><i class="glyphicon glyphicon-remove"></i></a>
  </div>
  <!-- Modal Welcome-->
    <div class="modal fade" id="modal-welcome" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog wmodal-dialog" role="document">
        <div class="modal-content wmodal-content bg-geo">
          <div class="modal-body wmodal-body">
            <a href="" class="w-close" data-dismiss="modal" aria-label="Close"><i class="glyphicon glyphicon-remove"></i></a>
            <div class="w-logo"><img src="<?=$global['logo-desktop'];?>" alt="logo civimi"></div>
            <div class="w-container">
                <div class="w-desc-head"></div>
                <div class="w-desc">
                    <?=$lang['home-modal_desc'];?>
                </div>
                <div class="w-btn"><a href="#" class="btn btn-info" data-dismiss="modal" aria-label="Close"><?=$lang['home-modal_action'];?></a></div>
                <div class="w-link"><?=$lang['home-modal_shop'];?></div>
            </div>
          </div>
        </div>
      </div>
    </div>
  <script src="<?php echo $global['absolute-url'];?>js/function.js"></script>
  <script type="text/javascript">

  function popup(message){
    $("#alert-popup").fadeIn();
    $("#popup-text").text(message);
    setTimeout(function(){
        $("#alert-popup").fadeOut();
    }, 3000);
}
$("#popup-close").click(function(){
    $("#alert-popup").fadeOut();
})
$(document).ready(function () {
   $('#look-job').tooltip();
   //modal welcome trigger
   //$('#modal-welcome').modal('show');
});
var contentSwiper = new Swiper('#swiper-univ',{
    loop: true,
    autoResize: true,
    paginationClickable: true,
    autoplay:6000,
    slidesPerView: 6,
});
var contentSwiper = new Swiper('#swiper-univ-xs',{
    loop: true,
    autoResize: true,
    paginationClickable: true,
    autoplay:6000,
    slidesPerView: 2,
});
var contentSwiper = new Swiper('#swiper-profile',{
    loop: true,
    autoResize: true,
    paginationClickable: true,
    autoplay:6000,
    slidesPerView: 2,
});
var contentSwiperJob = new Swiper('#swiper-job',{
    loop: true,
    autoResize: true,
    paginationClickable: true,
    autoplay:3000,
    slidesPerView: 2,
});
var contentSwiperComment = new Swiper('#swiper-comment',{
    speed : 1000,
    autoplay:3000,
    slidesPerView: 5,
    direction: 'vertical'
});
var contentSwiperComment = new Swiper('#swiper-comment-mobile',{
    loop: true,
    speed : 1000,
    autoplay:3000,
    slidesPerView: 3,
    direction: 'vertical'
});
</script>
</body>
</html>
