<?php 
include("packages/require.php");
include("controller/controller_form_industry.php");
$curpage='form_personal';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title><?=$global['title-form_industry'];?></title>
    <meta name="keywords" content="">
    <meta name="description" content="">
    <?php include("packages/head.php");?>
  <link rel="stylesheet" href="<?php echo $global['absolute-url'];?>css/dropzone.css"/>
  <script src="<?php echo $global['absolute-url'];?>js/dropzone.js"></script>
</head>
<body>
    <div id="all">
        <!-- start top nav -->
        <?php include("section-top-nav.php");?>
        <!-- end top nav -->
        <?php if(isset($_SESSION['userData']['id']) && ($data_user[0]['User_step_industry'] == "No" || $data_user[0]['User_step_industry'] == "")){ ?>
        <div class="register-section" style="background: url('<?=$global['base'];?>admin/img/pattern/pattern.png');">
            <div class="register-wrapper" style="background-color: transparent;">
                <div class="container container-max">
                    <div class="up7 visible-xs"></div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="findustry-welcome"><?=$lang['findustry-welcome'];?></div>
                            <div class="findustry-info"><?=$lang['findustry-info'];?></div>
                        </div>
                    </div>
                    <form name="formPersonal" action="<?=$path['form-industry-action'];?>" method="post" enctype="multipart/form-data" onsubmit="return industryValid()" style="max-width: 600px;margin: 0 auto;">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="job-level marg0" style="text-align:left;">
                                    <ul id="level-list">
                                        <?php $c=0; $i=0; foreach($industrys as $ind){ ?>
                                        <label class="hide check-inline check-inline-industry">
                                          <input <?php if($datas[$i]['Industry_ID'] == $ind['Industry_ID']){ echo "checked"; $i++;}?> class="check-industry" type="checkbox" onclick="displayIndustry(this.form)" name="ind" value="<?php echo $ind['Industry_ID'];?>">
                                          <div class="checked-industry"><?php echo $ind['Industry_title'];?></div>
                                        </label>
                                        <?php $c++; }?>
                                        <span id="more-industry" class="btn btn-info btn-more-industry">See More &raquo;</span>
                                        <!--<?php $i=0; foreach($industrys as $ind){ ?>
                                        <label class="check-inline check-inline-industry">
                                          <input class="check-industry" type="checkbox" onclick="displayIndustry(this.form)" name="ind" value="<?php echo $ind['Industry_ID'];?>">
                                          <div class="checked-industry"><?php echo $ind['Industry_title'];?></div>
                                        </label>
                                        <?php } ?>!-->
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12"><div id="alert-industry" class="control-alert text-center up1"></div></div>
                        </div>
                        <div class="row up2">
                            <div class="col-xs-12 text-right">
                                <input type="hidden" id="profile-img">
                                <input type="hidden" name="id" value="<?php echo $_SESSION['userData']['id'];?>">
                                <input type="hidden" name="industry" id="result-industry">
                                <button type="submit" class="btn btn-info btn-fpersonal"><?=$lang['findustry-continue'];?></button>
                            </div>
                        </div>
                    </form>
                    <div class="up3"></div>
                </div>
            </div>
        </div>
        <?php }else{ ?>
        <div class="register-section" style="background: url('<?=$global['base'];?>admin/img/pattern/pattern.png');">
            <div class="register-wrapper" style="background-color: transparent;">
                <div class="container container-max">
                    <div class="up7 visible-xs"></div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="findustry-welcome">Not Connected</div>
                        </div>
                    </div>
                    <div class="up3"></div>
                </div>
            </div>
        </div>
        <?php } ?>
        <!-- start footer section -->
        <?php include("section-footer.php");?>
        <!-- end footer section -->
    </div><!--  end all div -->
    <script type="text/javascript">
        $(document).ready(function () {
            size = $(".check-inline-industry").size();
            x=20;
            $('.check-inline-industry:lt('+x+')').removeClass('hide');
            $('#more-industry').on('click', function(){
                x= (x+20 <= size) ? x+20 : size;
                $('.check-inline-industry:lt('+x+')').removeClass('hide');
                if($(".check-inline-industry:lt("+size+")").hasClass('hide')){
                    //do nothing
                } else {
                    $('#more-industry').addClass('hide');
                } 
            });
        });
        function industryValid(){
            var len = $(".check-industry:checked").length;
            if(len>0){
                $("#alert-industry").text("");
                $("#alert-industry").show();
            }else {
                $("#alert-industry").text("you must choose at least one of industry!");
                $("#alert-industry").show();
                return false;
            }
            return true;
        }

        function displayIndustry(frm){     
            var selected="";  
            for (i = 0; i < frm.ind.length; i++){
                if (frm.ind[i].checked){     
                    selected += frm.ind[i].value +",";  
                }  
            }    
            document.getElementById("result-industry").value=selected;  
        }  
    </script>
  <script src="<?php echo $global['absolute-url'];?>js/function.js"></script>
</body>
</html>
