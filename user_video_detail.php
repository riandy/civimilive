<?php 
include("packages/require.php");
include("controller/controller_global.php");
include("controller/controller_user_video_detail.php");

$curpage='user_video';
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title><?=$_SESSION['userData']['username']."'s ".$data_video[0]['Video_title']." - ".$global['title-portfolio_video'];?></title>
  <meta name="keywords" content="">
  <meta name="description" content="">
  <meta name="robots" content="<?=$seo['robot_no'];?>">
  <?php include("packages/head.php");?>
  <link rel="stylesheet" href="<?php echo $global['absolute-url'];?>css/dropzone.css"/>
  <script src="<?php echo $global['absolute-url'];?>js/dropzone.js"></script>
  <script src="<?php echo $global['absolute-url'];?>js/function.js"></script>
</head>
<body>
  <div id="all">
    <!-- start top nav -->
    <?php include("section-top-nav.php");?>
    <!-- end top nav -->
    <div id="user-section" class="container-section">
    
    <div class="container">
      <div id="profile-content">
        <div class="up5 visible-xs"></div>
        <!-- start fixed mobile nav -->
        <?php $header_content = "Video";  include("user_part-mobile-nav.php");?>
        <!-- end fixed mobile nav-->
        <div class="row">
        <div class="up5 visible-xs"></div>
          <!-- profile sidebar -->
          <div class="col-sm-3 col-xs-12">
            <!-- start sidebar -->
            <?php include("user_part-sidebar.php");?>
            <!-- end sidebar -->
          </div><!-- profile sidebar -->
          <div class="col-sm-9 col-xs-12 pad0-xs">
              <div id="personal-portfolio-section" class="job-sbox">
                <div id="education-header" class="user-section-header row border-bottom-eee">
                  <div class="col-xs-12">
                    <div class="profile-title hidden-xs" style="font-size:14px;"><i class="glyphicon glyphicon-facetime-video"></i>&nbsp;&nbsp;
                      <a href="<?=$path['user-video'];?>" class="module-breadcrumb">Video</a> &gt;
                      <a id="folder-name" href="<?=$path['user-video-folder'];?><?=$data_video[0]['VideoCat_ID'];?>_<?=encode($data_video[0]['VideoCat_name']);?>.html" class="module-breadcrumb"><?=$data_video[0]['VideoCat_name'];?></a> &gt;&nbsp;
                      <span id="video-name" class="module-breadcrumb"><?=$data_video[0]['Video_title'];?></span>
                    </div>
                    <div class="profile-title visible-xs" style="font-size:14px;">
                      <a id="folder-back" href="<?=$path['user-video-folder'];?><?=$data_video[0]['VideoCat_ID'];?>_<?=encode($data_video[0]['VideoCat_name']);?>.html" class="module-breadcrumb">&lt; Back</a>
                    </div>
                  </div>
                </div>
                
                <div id="portfolio-profile">
                  <div class="row">
                    <div class="col-xs-12">
                      <div class="embed-responsive embed-responsive-16by9">
                        <iframe id="video-player" class="embed-responsive-item" src="" frameborder="0" allowfullscreen></iframe>
                      </div>
                    </div>
                  </div>
                  <script>
                //generate videoID
                  var iframe_src = "<?=$data_video[0]['Video_link'];?>";
                  var youtube_video_id = parseVideoID(iframe_src);
                  var embedURL = "https://www.youtube.com/embed/"+youtube_video_id;
                  $('#video-player').attr('src',embedURL);
                </script>
                  <div class="row up2">
                    <div class="col-sm-4 col-xs-12">
                      <div class="portfolio-label bold">Video Folder *</div>
                    </div>
                    <div class="col-sm-8 col-xs-12">
                      <select id="video-folder" class="form-control no-radius">
                      </select>
                    </div>
                  </div>
                  <div class="row up1">
                    <div class="col-sm-4 col-xs-12">
                      <div class="portfolio-label bold">Video Name *</div>
                    </div>
                    <div class="col-sm-8 col-xs-12">
                      <input id="video-title" type="text" class="form-control no-radius" placeholder="name of the video" value="<?=$data_video[0]['Video_title'];?>">
                    </div>
                  </div>
                  <div class="row up1">
                    <div class="col-sm-4 col-xs-12">
                      <div class="portfolio-label">Description</div>
                    </div>
                    <div class="col-sm-8 col-xs-12">
                      <textarea  id="video-desc" class="form-control no-radius" rows="3" placeholder="tell story about this video"><?=$data_video[0]['Video_desc'];?></textarea>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-xs-12">
                      <div id="videoInstruction">
                      <h3>Video embedding instructions</h3>
                      &lt;iframe width="640" height="360" src="<span id="videoInstructionText">http://www.youtube.com/embed/ tyEpaPEbjzI?rel=0</span>" frameborder="0" allowfullscreen&gt;&lt;/iframe&gt;&nbsp;&nbsp;<br><h5>(copy the highlighted link from your embedded iframe into the below box)</h5>
                      </div>
                    </div>
                  </div>
                  <div class="row up15">
                    <div class="col-sm-4 col-xs-12">
                      <div class="portfolio-label bold">Video Link *</div>
                    </div>
                    <div class="col-sm-8 col-xs-12">
                      <input id="video-link" type="text" class="form-control no-radius" placeholder="i.e. http://youtu.be/c1v1m1" value="<?=$data_video[0]['Video_link'];?>">
                      <div class="video-notice">*Paste your online video links here, i.e. http://youtu.be/c1v1m1</div>
                    </div>
                  </div>
                  <div class="up2"></div>
                  <div class="row">
                    <div class="col-xs-12">
                      <div id="alert-video" class="alert alert-success" role="alert" style="display:none;"></div>
                    </div>
                  </div>
                  <hr/>
                  <div class="row up2">
                    <div class="col-xs-12">
                      <input id="video-id" type="hidden" value="<?=$data_video[0]['Video_ID'];?>">
                      <input id="videocat-id" type="hidden" value="<?=$data_video[0]['VideoCat_ID'];?>">
                      <div class="btn-group">
                        <button id="save-video" type="button" class="btn btn-success btn-video">Save</button>
                        <a href="<?=$path['user-video-folder'];?><?=$data_video[0]['VideoCat_ID'];?>_<?=encode($data_video[0]['VideoCat_name']);?>.html" id="cancel-video" type="button" class="btn btn-default btn-video" data-dismiss="modal">Cancel</a>
                        <button id="delete-video" type="button" class="btn btn-danger btn-video">Delete</button>
                      </div>
                      <div id="load-video" class="edit-state hide">
                        <img src="<?=$global['absolute-url'];?>img/loadmores.gif" alt="loading">
                      </div>
                    </div>
                  </div>
                  <div class="pad1"></div>
                </div>
              </div>
          </div>
      </div>
    </div>
            
    </div>
    <!-- end center content -->
  </div>
    <!-- start footer section -->
    <?php include("section-footer.php");?>
    <!-- end footer section -->
  </div><!--  end all div -->
  <script src="<?php echo $global['absolute-url'];?>js/function.js"></script>
  <script type="text/javascript">
  $('#save-video').on('click', function(){
    update_video();
  })
  $('#delete-video').on('click', function(){
    delete_video();
  })
  get_folder();
  function get_data_category(id){
    var cat = $('#video-folder option:selected').text();
    $('#folder-name').text(cat);
  }
  function get_folder(){
    var url = "<?=$api['videocat-data'];?>&user_id=<?php echo $_SESSION['userData']['id'];?>&auth_code=<?php echo $_SESSION['userData']['auth_code'];?>";                            
    $.ajax({url: url,success:function(result){
    $('#video-folder').html(replace_folder(result.data));
    }});
  }
  function replace_folder(datas){
    var videocat_id = $("#videocat-id").val();
    var resultHTML='';
        resultHTML += "<option value=''>Choose folder</option>";
      if(datas != null){
        var obj = datas;    
        for(var i=0;i < obj.length;i++){
          if(videocat_id == obj[i].VideoCat_ID){
            var selected = "selected=selected";
          } else {
            var selected = "";
          }
          resultHTML += "<option value='"+obj[i].VideoCat_ID+"' "+selected+">"+obj[i].VideoCat_name+"</option>";
        }
      }
    return resultHTML;
  }
  // update function video
  function update_video(){

    var url = "<?=$api['video-update'];?>";
    var video_title = $("#video-title").val();
    var video_desc = $("#video-desc").val();
    var video_link = $("#video-link").val();
    var video_id = $("#video-id").val();
    var video_catid = $("#video-folder").val();
    var cat = $('#video-folder option:selected').text();
    var name = cat;
    var regex = /\\/g;
    var replaceName = name.replace(regex, "").replace(/&quot;/g, "");
    var lowercase = replaceName.toLowerCase();
    var encodeName = lowercase.replace(/\s+/g, '-');

    $(".btn-video").addClass("disabled");
    $("#load-video").removeClass("hide");
    var data = {
      user_id : "<?=$_SESSION['userData']['id'];?>",
      auth_code : "<?=$_SESSION['userData']['auth_code'];?>",
      id : video_id,
      videocat_id : video_catid,
      title : video_title,
      desc : video_desc,
      link : video_link
    }
    $.ajax({url: url,data : data, success:function(result){
      console.log(result);
      setTimeout(function() {
          $(".btn-video").removeClass("disabled");
          $("#load-video").addClass("hide");
          $("#video-title").val(video_title);
          $("#video-desc").val(video_desc);
          $("#videocat-id").val(video_catid);
          $("#video-link").val(video_link);
          $("#video-player").attr("src",video_link);
          $("#alert-video").show();
          $("#alert-video").text("Success update video");
          $("#alert-video").fadeOut(5400);
          $('#video-name').text(video_title);   
          var newlinks = "<?=$path['user-video-folder'];?>"+video_catid+"_"+encodeName+".html";
          $('#folder-name').attr("href",newlinks);
          $('#cancel-video').attr("href",newlinks);
          $('#folder-back').attr("href",newlinks);
          get_data_category(video_catid);
          get_folder();
      }, 1500)
    }});
  }//delete function video
  function delete_video(){
    if (confirm("Are you sure to delete this video ?")) {
      var url = "<?=$api['video-delete'];?>";
      var video_id = $("#video-id").val();
      var videocat_id = $("#videocat-id").val();

      var cat = $('#video-folder option:selected').text();
      var name = cat;
      var regex = /\\/g;
      var replaceName = name.replace(regex, "").replace(/&quot;/g, "");
      var lowercase = replaceName.toLowerCase();
      var encodeName = lowercase.replace(/\s+/g, '-');

      $(".btn-video").addClass("disabled");
      $("#load-video").removeClass("hide");
      var data = {
        user_id : "<?=$_SESSION['userData']['id'];?>",
        auth_code : "<?=$_SESSION['userData']['auth_code'];?>",
        id : video_id
      }
      $.ajax({url: url,data : data, success:function(result){
        console.log(result);
        setTimeout(function() {
          window.location = "<?=$path['user-video-folder'];?>"+videocat_id+"_"+encodeName+".html";
          $(".btn-video").removeClass("disabled");
          $("#load-video").addClass("hide");
        }, 1500)
      }});
    }
  }
  function profileNav(){
    $('#profile-arrow').toggleClass('nav-close');
  }
  </script>
</body>
</html>
