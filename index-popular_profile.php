<div id="popular-profile-content">
    <div class="container container-max">
        <div class="row">
            <div class="col-xs-12">
                <div class="popular-profile-header"><?=$lang['home-pop_profil'];?></div>
                <div class="popular-profile-header-child"><?=$lang['home-pop_profil_note'];?></div>
            </div>
        </div>
        <div class="row row-popular-profile hidden-xs">
            <?php foreach($data_pops as $data_pop){ ?>
            <div class="col-xs-2 col-popular-profile">
                <a href="<?=$path['user-profile'].$data_pop['User_username'].".cvm";?>" title="<?=$data_pop['User_fname'].' '.$data_pop['User_lname'].' - '.$data_pop['User_bioDesc'];?>" class="popular-profile-box">
                    <div class="popular-profile-img"><img src="<?php if($data_pop['User_proPhotoThmb'] != ""){ echo $global['img-url'].$data_pop['User_proPhotoThmb'];} else { echo $global['absolute-url']."img/basic-img/user.jpg";}?>" alt="<?=$data_pop['User_fname'].' '.$data_pop['User_lname'];?>"></div>
                    <div class="popular-profile-name"><?=$data_pop['User_fname'].' '.$data_pop['User_lname'];?></div>
                    <div class="popular-profile-job"><?=charLength($data_pop['User_bioDesc'], 16);?></div>
                </a>
            </div>
            <?php } ?>
        </div>
        <div id="swiper-profile" class="swiper-container height-auto visible-xs">
            <div class="swiper-wrapper height-auto">
                <?php foreach($data_pops as $data_pop){ ?>
                <div class="swiper-slide swiper-slide-desktop height-auto">
                    <div class="col-xs-12 col-popular-profile">
                        <a href="<?=$path['user-profile'].$data_pop['User_username'].".cvm";?>" title="<?=$data_pop['User_fname'].' '.$data_pop['User_lname'].' - '.$data_pop['User_bioDesc'];?>" class="popular-profile-box">
                            <div class="popular-profile-img"><img src="<?php if($data_pop['User_proPhotoThmb'] != ""){ echo $global['img-url'].$data_pop['User_proPhotoThmb'];} else { echo $global['absolute-url']."img/basic-img/user.jpg";}?>" alt="<?=$data_pop['User_fname'].' '.$data_pop['User_lname'];?>"></div>
                            <div class="popular-profile-name"><?=$data_pop['User_fname'].' '.$data_pop['User_lname'];?></div>
                            <div class="popular-profile-job"><?=charLength($data_pop['User_bioDesc'], 16);?></div>
                        </a>
                    </div>
                </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>