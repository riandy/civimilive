<div id="personal-cert-section" class="job-sbox" data-order="5">
  <div id="cert-header" class="user-section-header row border-bottom-eee">
    <div class="col-xs-6">
      <div class="profile-title"><i class="glyphicon glyphicon-certificate"></i>&nbsp;&nbsp;<?=$lang['cert-title'];?></div>
    </div>
    <div class="col-xs-6">
        <div class="edit-button text-right">
            <span class="sort-icon"><i class="glyphicon glyphicon-sort"></i></span>
            <a href="Javascript:;" id="add-cert" onclick="hideEmptyCert();" data-toggle="collapse" data-target="#slide-cert" aria-expanded="true" aria-controls="slide-cert">
              <i class="glyphicon glyphicon-plus"></i> <span class="hidden-xs"><?=$lang['cert-btn'];?></span>
            </a>
        </div>
    </div>
  </div>

  <div id="cert-profile">
    <!-- start add cert -->
    <div id="slide-cert" class="collapse panel-input">
      <div class="panel-input-body panel-input-work">
        <div class="row up15">
          <div class="col-xs-12">
            <div class="form-label bold"><?=$lang['cert-name'];?> *</div>
          </div>
          <div class="col-sm-10 col-xs-12">
            <div id="cname-box" class="cname-box">
              <input id="cert-title" onfocus="$('#cname-list').show();" oninput="get_cname();$('#cname-list').show();" type="text" name="" class="form-control no-radius" placeholder="<?=$lang['cert-name_placeholder'];?>">
              <div id="cname-list" class="cname-list" onmouseover="$('#cname-list').show();">
                <ul id="cname-ul">

                </ul>
              </div>
            </div>
            <div id="cert-vtitle" class="valid-message"></div>
          </div>
        </div>
        <!-- <div class="row up1">
          <div class="col-xs-12">
            <div class="form-label bold"><?=$lang['cert-number'];?> *</div>
          </div>
          <div class="col-sm-10 col-xs-12">
              <input id="cert-number" type="text" name="" class="form-control no-radius" placeholder="<?=$lang['cert-number_placeholder'];?>">
              <div id="cert-vnumber" class="valid-message"></div>
          </div>
        </div> -->
        <div class="row up1">
          <div class="col-xs-12">
            <div class="form-label"><?=$lang['cert-url'];?></div>
          </div>
          <div class="col-sm-10 col-xs-12">
              <input id="cert-url"  type="text" name="" class="form-control no-radius" placeholder="<?=$lang['cert-url_placeholder'];?>">
              <div id="cert-vurl" class="valid-message"></div>
          </div>
        </div>
        <div class="row up1">

          <div class="col-sm-6 col-xs-12">
            <div class="checkbox">
              <label>
                <input id="cert-status" type="checkbox" onclick="checkStatCert();" value="yes"> <?=$lang['cert-expire'];?>
              </label>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-xs-6 up1">
            <div class="form-label bold"><?=$lang['cert-from'];?> *</div>
            <div class="row">
              <div class="col-xs-6" style="padding: 0 5px 0 0;">
                <select id="cert-fmonth" name="" class="form-control no-radius">
                  <option value="">Month</option>
                  <?php $month = '12';
                  for ($months = '1'; $months <= $month; $months++) { ?>
                  <option value="<?php echo $months; ?>"><?php echo date("F", strtotime("$months/12/10")); ?></option>
                  <?php } ?>
                </select>
              </div>
              <div class="col-xs-6" style="padding: 0 0 0 5px;">
                <select id="cert-fyear" name="" class="form-control no-radius">
                  <option value="">--</option>
                  <?php $year = '1950';
                  for ($years = date("Y"); $years >= $year; $years--) { ?>
                  <option value="<?php echo $years; ?>"><?php echo $years; ?></option>
                  <?php } ?>
                </select>
              </div>
            </div>
          </div>
          <div id="to-cert" class="col-xs-6 up1">
            <div class="form-label bold"><?=$lang['cert-to'];?> *</div>
            <div class="row">
              <div class="col-xs-6" style="padding: 0 5px 0 0;">
                <select id="cert-tmonth" name="" class="form-control no-radius">
                  <option value="" class="cert-tmonth">Month</option>
                  <?php $month = '12';
                  for ($months = '1'; $months <= $month; $months++) { ?>
                  <option value="<?php echo $months; ?>" class="cert-tmonth"><?php echo date("F", strtotime("$months/12/10")); ?></option>
                  <?php } ?>
                </select>
              </div>
              <div class="col-xs-6" style="padding: 0 0 0 5px;">
                <select id="cert-tyear" name="" class="form-control no-radius">
                  <option value="" class="cert-tyear">--</option>
                  <?php $year = '1950';
                  for ($years = date("Y"); $years >= $year; $years--) { ?>
                  <option value="<?php echo $years; ?>" class="cert-tyear"><?php echo $years; ?></option>
                  <?php } ?>
                </select>
              </div>
            </div>
          </div>
          <div class="col-xs-12">
            <div id="cert-vdate" class="valid-message"></div>
          </div>
        </div>
        <div class="row up1">
          <div class="col-xs-12">
            <div class="form-label"><?=$lang['cert-desc'];?></div>
          </div>
          <div class="col-sm-12 col-xs-12">
            <textarea id="cert-desc" name="desc" class="form-control no-radius" rows="4" placeholder="<?=$lang['cert-desc_placeholder'];?>"></textarea>
          </div>
        </div>
        <div class="row up2">
          <div class="col-xs-12">
            <a href="Javascript:;" class="btn btn-info" onclick="save_cert();">Save</a><span class="pad05"></span>
            <a href="Javascript:;" onclick="hideEmptyCert();" type="reset" class="btn btn-default" data-toggle="collapse" data-target="#slide-cert" aria-expanded="true" aria-controls="slide-cert">Cancel</a>
          </div>
        </div>
      </div>
    </div><!-- end panel add cert -->


    <!-- main cert -->
    <div id="main-cert" class="panel-group marg0" role="tablist" aria-multiselectable="true">
    </div> <!-- end main cert -->
    <div class="see-more"><a href="javascript:;" id="see-cert" class="hide">SEE <span id="seeCert-num"></span> MORE</a></div>
    <div id='load-cert' class='text-center'></div>

    <!-- start edit cert -->
    <div id="edit-cert-place" class="hide">
      <div id="edit-cert">
        <div class="panel-input-cert">
          <div class="row up15">
            <div class="col-xs-12">
              <div class="form-label bold"><?=$lang['cert-name'];?> *</div>
            </div>
            <div class="col-sm-10 col-xs-12">
              <div id="cname-boxEdit" class="cname-box">
                <input id="cert-etitle" onfocus="$('#cname-listEdit').show();" oninput="get_cnameEdit();$('#cname-listEdit').show();" type="text" name="" class="form-control no-radius" placeholder="<?=$lang['cert-name_placeholder'];?>">
                <div id="cname-listEdit" class="cname-list" onmouseover="$('#cname-listEdit').show();">
                  <ul id="cname-eul">

                  </ul>
                </div>
              </div>
              <div id="cert-evtitle" class="valid-message"></div>
            </div>
          </div>
          <!-- <div class="row up1">
            <div class="col-xs-12">
              <div class="form-label bold"><?=$lang['cert-number'];?> *</div>
            </div>
            <div class="col-sm-10 col-xs-12">
              <input id="cert-enumber" type="text" name="" class="form-control no-radius" placeholder="<?=$lang['cert-number_placeholder'];?>">
              <div id="cert-evnumber" class="valid-message"></div>
            </div>
          </div> -->
          <div class="row up1">
            <div class="col-xs-12">
              <div class="form-label"><?=$lang['cert-url'];?></div>
            </div>
            <div class="col-sm-10 col-xs-12">
              <input id="cert-eurl" type="text" name="" class="form-control no-radius" placeholder="<?=$lang['cert-url_placeholder'];?>">
              <div id="cert-evurl" class="valid-message"></div>
            </div>
          </div>
          <div class="row up1">
            <div class="col-sm-6 col-xs-12">
              <div class="checkbox">
                <label>
                  <input id="cert-estatus" type="checkbox" onclick="checkStatCert2();" value="yes"> <?=$lang['cert-expire'];?>
                </label>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-xs-6 up1">
              <div class="form-label bold"><?=$lang['cert-from'];?> *</div>
              <div class="row">
                <div class="col-xs-6" style="padding: 0 5px 0 0;">
                  <select id="cert-efmonth" name="" class="form-control no-radius">
                    <option value="" class="cert-efmonth">Month</option>
                    <?php $month = '12';
                    for ($months = '1'; $months <= $month; $months++) { ?>
                    <option value="<?php echo $months; ?>" class="cert-efmonth"><?php echo date("F", strtotime("$months/12/10")); ?></option>
                    <?php } ?>
                  </select>
                </div>
                <div class="col-xs-6" style="padding: 0 0 0 5px;">
                  <select id="cert-efyear" name="" class="form-control no-radius">
                    <option value="" class="cert-efyear">--</option>
                    <?php $year = '1950';
                    for ($years = date("Y"); $years >= $year; $years--) { ?>
                    <option value="<?php echo $years; ?>" class="cert-efyear"><?php echo $years; ?></option>
                    <?php } ?>
                  </select>
                </div>
              </div>
            </div>
            <div id="to-ecert" class="col-xs-6 up1">
              <div class="form-label bold"><?=$lang['cert-to'];?> *</div>
              <div class="row">
                <div class="col-xs-6" style="padding: 0 5px 0 0;">
                  <select id="cert-etmonth" name="" class="form-control no-radius">
                    <option value="" class="cert-etmonth">Month</option>
                    <?php $month = '12';
                    for ($months = '1'; $months <= $month; $months++) { ?>
                    <option value="<?php echo $months; ?>" class="cert-etmonth"><?php echo date("F", strtotime("$months/12/10")); ?></option>
                    <?php } ?>
                  </select>
                </div>
                <div class="col-xs-6" style="padding: 0 0 0 5px;">
                  <select id="cert-etyear" name="" class="form-control no-radius">
                    <option value="" class="cert-etyear">--</option>
                    <?php $year = '1950';
                    for ($years = date("Y"); $years >= $year; $years--) { ?>
                    <option value="<?php echo $years; ?>" class="cert-etyear"><?php echo $years; ?></option>
                    <?php } ?>
                  </select>
                </div>
              </div>
            </div>
            <div class="col-xs-12">
              <div id="cert-evdate" class="valid-message"></div>
            </div>
          </div>
          <div class="row up1">
            <div class="col-xs-12">
              <div class="form-label"><?=$lang['cert-desc'];?></div>
            </div>
            <div class="col-sm-12 col-xs-12">
              <textarea id="cert-edesc" name="desc" class="form-control no-radius" rows="3" placeholder="<?=$lang['cert-desc_placeholder'];?>"></textarea>
            </div>
          </div>
          <div class="row up1">
            <div class="col-xs-12">
              <input id="edit_cert_old_title" type="hidden">
              <input id="edit_cert_ATid" type="hidden">
              <a class="btn btn-info" onclick="update_cert();" >Save</a><span class="pad05"></span>
              <a id="reset_edit_cert" type="reset" class="btn btn-default" onclick="close_edit_cert()">Cancel</a>
              <a href="javascript:;" id="delete-cert" onclick="delete_cert();" class="delete-text">Delete</a>
            </div>
          </div>
        </div>
      </div>
    </div><!-- end edit cert -->
  </div> <!-- end cert profile -->
<script>
  //get company
  $('html').click(function() {
    $('#cname-list').hide();
    $("#cert-title").removeClass("load-input");
  });
  $('#cname-box').click(function(event){
    event.stopPropagation();
    $("#cert-title").removeClass("load-input");
  });
  $('html').click(function() {
    $('#cname-listEdit').hide();
    $("#cert-etitle").removeClass("load-input");
  });
  $('#cname-boxEdit').click(function(event){
    event.stopPropagation();
    $("#cert-etitle").removeClass("load-input");
  });
  function get_cname(){
    var q = $("#cert-title").val();
    $("#cert-title").addClass("load-input");
    var url = "<?=$api['cert-autocomplete'];?>"+q;                            
    $.ajax({url: url,success:function(result){
      $("#cert-title").removeClass("load-input");
      $('#cname-ul').html(replace_cname(result.data,q));
    }}); 
  }
  function get_cnameEdit(){
    var q = $("#cert-etitle").val();
    $("#cert-etitle").addClass("load-input");
    var url = "<?=$api['cert-autocomplete'];?>"+q;                            
    $.ajax({url: url,success:function(result){
      $("#cert-etitle").removeClass("load-input");
      $('#cname-eul').html(replace_cnameEdit(result.data,q));
    }}); 
  }
  function replace_cname(datas,text){
    var resultHTML='';

    if(datas != null){
      var obj = datas;    
      for(var i=0;i < obj.length;i++){
        if(obj[i].Cert_ID != '' && obj[i].Cert_title != ''){
          resultHTML += "<li class='cname-li' id='cname-"+obj[i].Cert_ID+"' onclick='selectCname("+obj[i].Cert_ID+")' data-cn_title='"+obj[i].Cert_title+"' data-cn_url='"+obj[i].Cert_url+"'>";
          if(obj[i].Cert_img != "" && obj[i].Cert_img != "image/"){
            resultHTML += "<div class='cname-image'><img src='<?=$global['img-url'];?>"+obj[i].Cert_img+"' alt='image'></div>";
          }else {
            resultHTML += "<div class='cname-image'><img src='<?=$placeholder['img-certificate'];?>' alt='image'></div>";
          }
          resultHTML += "<div class='cname-text'>";
          resultHTML += "<div class='cname-title'>"+obj[i].Cert_title+"</div>";
          resultHTML += "</div>";
          resultHTML += "</li>";
        }
      }
    } else {
      resultHTML += "<li class='no-list'><div class='no-list-text'><?=$lang['entry-result'];?> \"<span>"+text+"</span>\" <?=$lang['entry-result_text'];?></div></li>";
    }
    return resultHTML;
  }
  function replace_cnameEdit(datas,text){
    var resultHTML='';

    if(datas != null){
      var obj = datas;    
      for(var i=0;i < obj.length;i++){
        if(obj[i].Cert_ID != '' && obj[i].Cert_title != ''){
          resultHTML += "<li class='cname-li' id='ecname-"+obj[i].Cert_ID+"' onclick='selectCnameEdit("+obj[i].Cert_ID+")' data-cn_etitle='"+obj[i].Cert_title+"' data-cn_eurl='"+obj[i].Cert_url+"'>";
          if(obj[i].Cert_img != "" && obj[i].Cert_img != "image/"){
            resultHTML += "<div class='cname-image'><img src='<?=$global['img-url'];?>"+obj[i].Cert_img+"' alt='image'></div>";
          }else {
            resultHTML += "<div class='cname-image'><img src='<?=$placeholder['img-certificate'];?>' alt='image'></div>";
          }
          resultHTML += "<div class='cname-text'>";
          resultHTML += "<div class='cname-title'>"+obj[i].Cert_title+"</div>";
          resultHTML += "</div>";
          resultHTML += "</li>";
        }
      }
    } else {
      resultHTML += "<li class='no-list'><div class='no-list-text'><?=$lang['entry-result'];?> \"<span>"+text+"</span>\" <?=$lang['entry-result_text'];?></div></li>";
    }
    return resultHTML;
  }
  function selectCname(param){
    var title = $('#cname-'+param).data('cn_title');
    var curl = $('#cname-'+param).data('cn_url');
    $("#cert-title").removeClass("load-input");
    $('#cert-title').val(title);
    $('#cert-url').val(curl);
    $('#cname-list').hide();
  }
  function selectCnameEdit(param){
    var title = $('#ecname-'+param).data('cn_etitle');
    var curl = $('#cname-'+param).data('cn_eurl');
    $("#cert-etitle").removeClass("load-input");
    $('#cert-etitle').val(title);
    $('#cert-eurl').val(curl);
    $('#cname-listEdit').hide();
  }
  get_cert();
  function get_cert(){
    $('#see-cert').addClass('hide');
    $('#load-cert').html("<img src='<?=$global['absolute-url'];?>img/load-blue.gif' style='margin:70px 0' />");
    $('#main-cert').html('');
    var url = "<?=$api['cert-data'];?>&user_id=<?php echo $_SESSION['userData']['id'];?>&auth_code=<?php echo $_SESSION['userData']['auth_code'];?>";                            
    $.ajax({url: url,success:function(result){
      setTimeout(function() {
      $('#load-cert').html('');
      $('#main-cert').html(replace_cert(result.data));
      seeMoreCert();
      },1000);
    }}); 
  }
  function replace_cert(datas){
    var resultHTML='';resultNext = "";
    var total = 0;previous = "";user_name = "";link="";
    var d = new Date();strip_string ="";
    var current = d.getTime();
    // console.log(datas);
    if(datas != null){
      var obj = datas;    
      for(var i=0;i < obj.length;i++){
        if(obj[i].ATcert_ID != '' && obj[i].Cert_title != ''){
          resultHTML += "<div class='row row-cert hide hm-cert' role='tab'>";
          resultHTML += "<div class='col-xs-12'>";
          resultHTML += "<a href='#ccollapse-"+obj[i].ATcert_ID+"' aria-controls='ccollapse-"+obj[i].ATcert_ID+"' data-parent='#main-cert' onclick='edit_box_cert("+obj[i].ATcert_ID+");' data-toggle='collapse' aria-expanded='false' id='cert-"+obj[i].ATcert_ID+"' class='cert-box' data-cert_atid='"+obj[i].ATcert_ID+"' data-cert_number='"+obj[i].ATcert_number+"' data-cert_url='"+obj[i].Cert_url+"' data-cert_status='"+obj[i].ATcert_not_expire+"' data-cert_title='"+obj[i].Cert_title+"' data-cert_fmonth='"+obj[i].ATcert_from_month+"' data-cert_fyear='"+obj[i].ATcert_from_year+"' data-cert_tmonth='"+obj[i].ATcert_to_month+"' data-cert_tyear='"+obj[i].ATcert_to_year+"' data-cert_desc='"+obj[i].ATcert_desc+"'>";
          resultHTML += "<div class='row'>";
          resultHTML += "<div class='col-md-10 col-xs-9 pad0'>";
          resultHTML += "<div class='cert-title'>"+obj[i].Cert_title+" <span class='cert-etext'><i class='glyphicon glyphicon-edit'></i> <span class='hidden-xs'>Edit</span></span></div>";
          if(obj[i].ATcert_to_year == "" || obj[i].ATcert_to_year == 0){
            var toYear = "";
          } else {
            var toYear = " - "+obj[i].ATcert_to_year;
          }
          resultHTML += "<div class='cert-year'>"+obj[i].ATcert_from_year+toYear+"</div>";
          resultHTML += "</div>";
          resultHTML += "<div class='col-md-2 col-xs-3'>";
          if(obj[i].Cert_img != "" && obj[i].Cert_img != "image/"){
            resultHTML += "<div class='cert-image'><img src='<?=$global['img-url'];?>"+obj[i].Cert_img+"' alt='"+obj[i].Cert_title+"'></div>";
          }else {
            resultHTML += "<div class='cert-image'><img src='<?=$placeholder['img-certificate'];?>' alt='image'></div>";
          }
          resultHTML += "</div>";
          resultHTML += "</div>";
          resultHTML += "</a>";
          resultHTML += "<div id='load-cert-"+obj[i].ATcert_ID+"' class='text-center'></div>";
          resultHTML += "<a id='close-cert-"+obj[i].ATcert_ID+"' href='#ccollapse-"+obj[i].ATcert_ID+"' aria-controls='ccollapse-"+obj[i].ATcert_ID+"' data-parent='#main-cert' data-toggle='collapse' aria-expanded='false' class='hide'></a>";
          resultHTML += "</div>";
          resultHTML += "</div>";
          resultHTML += "<div id='ccollapse-"+obj[i].ATcert_ID+"' aria-labelledby='cert-"+obj[i].ATcert_ID+"' class='panel-collapse collapse collapse-cert pad0' role='tabpanel'></div>";
        }
      }
    } else {
      resultHTML += "<div id='empty-cert' class='empty-file'><?=$lang['cert-placeholder'];?></div>";//cert placeholder text
    }
    return resultHTML;
  }
  function seeMoreCert(){
    size = $(".row-cert").size();
    x=2;

    $('.row-cert:lt('+x+')').removeClass('hide');
    $('.row-cert:lt('+x+')').removeClass('hm-cert'); 
    size_left= $(".hm-cert").size();
    if(size > 2){
      $('#see-cert').removeClass('hide');
      $("#seeCert-num").text('('+size_left+')');
    } else {
      $('#see-cert').addClass('hide');
    }

    $('#see-cert').on('click', function(){
        $('.row-cert').removeClass('hide');
        $('.row-cert').removeClass('hm-cert');
        $('#see-cert').addClass('hide');
    });
  };
  function wshowhide(param){
    var teks = $('#cert-link'+param).text();
    if(teks == 'Hide Desc'){
      $('#cert-link'+param).text('Show Desc');
    } else{
      $('#cert-link'+param).text('Hide Desc');
    }
  }
  //clear value add cert
  function emptyAddcert(){
    $("#cert-title").val('');
    // $("#cert-number").val('');
    $("#cert-url").val('');
    $("#cert-desc").val('');
    $("#cert-fmonth").val('');
    $("#cert-fyear").val('');
    $("#cert-tmonth").val('');
    $("#cert-tyear").val('');
    document.getElementById("cert-status").checked = false;
    $("#to-cert").removeClass("hide");
  }
  //open slide edit
  function edit_box_cert(param){
    if ($('#slide-cert').hasClass('in')){
      $('#slide-cert').collapse('hide');
    }
    if($('.cert-box').hasClass('hide-cert')){
      $('.cert-box').removeClass('hide-cert');
      if($('.collapse-cert').hasClass('in')){
        $('.collapse-cert').removeClass('in')
      }
      $('#cert-'+param).addClass('hide-cert');
    } else {
      $('#cert-'+param).addClass('hide-cert');
    }
    $('#edit-cert').detach().appendTo('#ccollapse-'+param+'');
    var ATid = $('#cert-'+param).data('cert_atid');
    var title = $('#cert-'+param).data('cert_title');
    // var number = $('#cert-'+param).data('cert_number');
    var status = $('#cert-'+param).data('cert_status');
    var curl = $('#cert-'+param).data('cert_url');
    var desc = $('#cert-'+param).data('cert_desc');
    var fmonth = $('#cert-'+param).data('cert_fmonth');
    var fyear = $('#cert-'+param).data('cert_fyear');
    var tmonth = $('#cert-'+param).data('cert_tmonth');
    var tyear = $('#cert-'+param).data('cert_tyear');

    $('#reset_edit_cert').data("target") === "#ccollapse";
    $('#cert-etitle').val(title);
    // $('#cert-enumber').val(number);
    $('#cert-eurl').val(curl);
    $('#cert-edesc').val(desc);
    $('#edit_cert_old_title').val(title);
    $('#edit_cert_ATid').val(ATid);
    $('option[value="' + fmonth + '"].cert-efmonth').attr("selected", true);
    $('option[value="' + fyear + '"].cert-efyear').attr("selected", true);
    if(status == 'yes') {
      document.getElementById("cert-estatus").checked = true;
    } else {
      document.getElementById("cert-estatus").checked = false;
      $('option[value="' + tmonth + '"].cert-etmonth').attr("selected", true);
      $('option[value="' + tyear + '"].cert-etyear').attr("selected", true);
    }
    checkStatCert2();
  }
  //close edit cert
  function close_edit_cert(){
    var id = $('#edit_cert_ATid').val();
    $('#close-cert-'+id+'').click();
    $('#cert-'+id).removeClass('hide-cert');
  }
  //hide empty word when add new
  function hideEmptyCert(){
    var myElem = document.getElementById('empty-cert');
    if(myElem != null){
      $('#empty-cert').toggle();
    }
    var id = $('#edit_cert_ATid').val();
    if (id != ''){
      if ($('#ccollapse-'+id).hasClass('in')){
        $('#close-cert-'+id+'').click();
        $('#cert-'+id).removeClass('hide-cert');
      }
    }
    emptyAddcert();
  }
  //update function
  function update_cert(){
    if(validCertEdit()){
    $('#edit-cert').detach().appendTo('#edit-cert-place');
    var icert = $('#edit_cert_ATid').val();
    $('#close-cert-'+icert).click();
    $('html, body').animate({
      scrollTop: $('#cert-header').offset().top-80
    }, 'fast');
    //$('#load-cert-'+icert).html('<img src="img/load-blue.gif" class="marg1" />');

    
      var url = "<?=$api['cert-update'];?>";
      var cert_id = $("#edit_cert_ATid").val();
      var cert_title = $("#cert-etitle").val();
      // var cert_number = $("#cert-enumber").val();
      var cert_number = 0;
      var cert_link = $("#cert-eurl").val();
      var cert_desc = $("#cert-edesc").val();
      var cert_fmonth = $("#cert-efmonth").val();
      var cert_fyear = $("#cert-efyear").val();
      var cert_tmonth = $("#cert-etmonth").val();
      var cert_tyear = $("#cert-etyear").val();
      var cert_status = document.getElementById('cert-estatus');
      if (cert_status.checked) {
        cert_status = 'yes';
      } else {
        cert_status = 'no';
      }
      var data = {
        user_id : "<?=$_SESSION['userData']['id'];?>",
        auth_code : "<?=$_SESSION['userData']['auth_code'];?>",
        id : cert_id,
        title : cert_title,
        number : cert_number,
        not_expire : cert_status,
        from_month : cert_fmonth,
        from_year : cert_fyear,
        to_month : cert_tmonth,
        to_year : cert_tyear,
        cert_url : cert_link,
        desc : cert_desc
      }
      $.ajax({url: url,data : data, success:function(result){
        get_cert();
        console.log(result);
      }});
    }
  }
  //insert function
  function save_cert(){
    if(validCert()){
    $('#edit-cert').detach().appendTo('#edit-cert-place');
    $('html, body').animate({
      scrollTop: $('#cert-header').offset().top-80
    }, 'fast');

    var url = "<?=$api['cert-update'];?>";
    var cert_title = $("#cert-title").val();
    // var cert_number = $("#cert-number").val();
    var cert_number = 0;
    var cert_link = $("#cert-url").val();
    var cert_desc = $("#cert-desc").val();
    var cert_fmonth = $("#cert-fmonth").val();
    var cert_fyear = $("#cert-fyear").val();
    var cert_tmonth = $("#cert-tmonth").val();
    var cert_tyear = $("#cert-tyear").val();
    var cert_status = document.getElementById('cert-status');
    if (cert_status.checked) {
      cert_status = 'yes';
    } else {
      cert_status = 'no';
    }
    var data = {
      user_id : "<?=$_SESSION['userData']['id'];?>",
      auth_code : "<?=$_SESSION['userData']['auth_code'];?>",
      title : cert_title,
      number : cert_number,
      not_expire : cert_status,
      from_month : cert_fmonth,
      from_year : cert_fyear,
      to_month : cert_tmonth,
      to_year : cert_tyear,
      cert_url : cert_link,
      desc : cert_desc
    }
    $.ajax({url: url,data : data, success:function(result){
      $('#slide-cert').collapse('hide');
        get_cert();
        emptyAddcert();
      console.log(result);
    }});
    }
  }
  //delete function
  function delete_cert(){
    if (confirm("Are you sure to delete this information ?")) {
      $('#edit-cert').detach().appendTo('#edit-cert-place');
      $('html, body').animate({
        scrollTop: $('#cert-header').offset().top-80
      }, 'fast');
      
      var url = "<?=$api['cert-delete'];?>";
      var cert_id = $("#edit_cert_ATid").val();
      var data = {
        user_id : "<?=$_SESSION['userData']['id'];?>",
        auth_code : "<?=$_SESSION['userData']['auth_code'];?>",
        id : cert_id
      }
      $('#main-cert').html('');
      $.ajax({url: url,data : data, success:function(result){
          get_cert();
        console.log(result);
      }});
    }
  }
  function checkStatCert() {
  var s = document.getElementById('cert-status');
    if (s.checked) {
      if($("#to-cert").hasClass("hide")){
            //nothing happen
      } else {
        $("#to-cert").addClass("hide");
      $('option[value=""].cert-tmonth').attr("selected", true);
      $('option[value=""].cert-tyear').attr("selected", true);
      }
    } else {
      if($("#to-cert").hasClass("hide")){
        $("#to-cert").removeClass("hide");
      }
    }
  }
  function checkStatCert2() {
  var s = document.getElementById('cert-estatus');
    if (s.checked) {
      if($("#to-ecert").hasClass("hide")){
            //nothing happen
      } else {
        $("#to-ecert").addClass("hide");
      $('option[value=""].cert-etmonth').attr("selected", true);
      $('option[value=""].cert-etyear').attr("selected", true);
      }
    } else {
      if($("#to-ecert").hasClass("hide")){
        $("#to-ecert").removeClass("hide");
      }
    }
  }
  function validCert(){
  var cert_title = $("#cert-title").val();
  // var cert_number = $("#cert-number").val();
  var cert_link = $("#cert-url").val();
  var cert_desc = $("#cert-desc").val();
  var cert_fmonth = $("#cert-fmonth").val();
  var cert_fyear = $("#cert-fyear").val();
  var cert_tmonth = $("#cert-tmonth").val();
  var cert_tyear = $("#cert-tyear").val();
  var cert_status = document.getElementById('cert-status');
  var number_format = /^(\d+(?:[\.\,]\d{1,2})?)$/;
  var message = "";

  if(cert_title != ""){
    if(cert_title.length >= 3){
      message = "";
      $("#cert-title").removeClass('input-error');
      $("#cert-vtitle").text(message);
    } else {
      message = "Certificate Name has to be at least 3 characters!";
      $("#cert-title").addClass('input-error');
      $("#cert-title").focus();
      $("#cert-vtitle").text(message);
      return false;
    }
  } else {
    message = "Please input or select your Certificate Name!";
    $("#cert-title").addClass('input-error');
    $("#cert-title").focus();
    $("#cert-vtitle").text(message);
    return false;
  }
  // if(cert_number != ""){
  //     message = "";
  //     $("#cert-number").removeClass('input-error');
  //     $("#cert-vnumber").text(message);
  // } else {
  //   message = "Please input the certificate number!";
  //   $("#cert-number").addClass('input-error');
  //   $("#cert-number").focus();
  //   $("#cert-vnumber").text(message);
  //   return false;
  // }
  if (cert_status.checked) {
    if(cert_fmonth != ""){
      message = "";
      $("#cert-fmonth").removeClass('input-error');
      $("#cert-vdate").text(message);
    } else {
      message = "Start Month cannot be empty!";
      $("#cert-fmonth").addClass('input-error');
      $("#cert-fmonth").focus();
      $("#cert-vdate").text(message);
      return false;
    }
    if(cert_fyear != ""){
      message = "";
      $("#cert-fyear").removeClass('input-error');
      $("#cert-vdate").text(message);
    } else {
      message = "Start Year cannot be empty!";
      $("#cert-fyear").addClass('input-error');
      $("#cert-fyear").focus();
      $("#cert-vdate").text(message);
      return false;
    }
  } else {
    if(cert_fmonth != ""){
      message = "";
      $("#cert-fmonth").removeClass('input-error');
      $("#cert-vdate").text(message);
    } else {
      message = "Start Month cannot be empty!";
      $("#cert-fmonth").addClass('input-error');
      $("#cert-fmonth").focus();
      $("#cert-vdate").text(message);
      return false;
    }
    if(cert_fyear != ""){
      message = "";
      $("#cert-fyear").removeClass('input-error');
      $("#cert-vdate").text(message);
    } else {
      message = "Start Year cannot be empty!";
      $("#cert-fyear").addClass('input-error');
      $("#cert-fyear").focus();
      $("#cert-vdate").text(message);
      return false;
    }
    if(cert_tmonth != ""){
      if(cert_tyear == cert_fyear){
        if(eval(cert_tmonth) >= eval(cert_fmonth)){
          message = "";
          $("#cert-tmonth").removeClass('input-error');
          $("#cert-vdate").text(message);
        } else {
          message = "End Date has to be later than Start Date!";
          $("#cert-tmonth").addClass('input-error');
          $("#cert-tmonth").focus();
          $("#cert-vdate").text(message);
          return false;
        }
      } else {
        message = "";
        $("#cert-tmonth").removeClass('input-error');
        $("#cert-vdate").text(message);
      }
    } else {
      message = "End Month cannot be empty!";
      $("#cert-tmonth").addClass('input-error');
      $("#cert-tmonth").focus();
      $("#cert-vdate").text(message);
      return false;
    }
    if(cert_tyear != ""){
      if(cert_tyear >= cert_fyear){
        message = "";
        $("#cert-tyear").removeClass('input-error');
        $("#cert-vdate").text(message);
      } else {
        message = "End Date has to be later than Start Date!";
        $("#cert-tyear").addClass('input-error');
        $("#cert-tyear").focus();
        $("#cert-vdate").text(message);
        return false;
      } 
    } else {
      message = "End Year cannot be empty!";
      $("#cert-tyear").addClass('input-error');
      $("#cert-tyear").focus();
      $("#cert-vdate").text(message);
      return false;
    }
  }
  return true;
}
function validCertEdit(){
  var cert_title = $("#cert-etitle").val();
  // var cert_number = $("#cert-enumber").val();
  var cert_link = $("#cert-eurl").val();
  var cert_desc = $("#cert-edesc").val();
  var cert_fmonth = $("#cert-efmonth").val();
  var cert_fyear = $("#cert-efyear").val();
  var cert_tmonth = $("#cert-etmonth").val();
  var cert_tyear = $("#cert-etyear").val();
  var cert_status = document.getElementById('cert-estatus');
  var number_format = /^(\d+(?:[\.\,]\d{1,2})?)$/;
  var message = "";

  if(cert_title != ""){
    if(cert_title.length >= 3){
      message = "";
      $("#cert-etitle").removeClass('input-error');
      $("#cert-evtitle").text(message);
    } else {
      message = "Certificate Name has to be at least 3 characters!";
      $("#cert-etitle").addClass('input-error');
      $("#cert-etitle").focus();
      $("#cert-evtitle").text(message);
      return false;
    }
  } else {
    message = "Please input or select your Certificate Name!";
    $("#cert-etitle").addClass('input-error');
    $("#cert-etitle").focus();
    $("#cert-evtitle").text(message);
    return false;
  }
  // if(cert_number != ""){
  //     message = "";
  //     $("#cert-enumber").removeClass('input-error');
  //     $("#cert-evnumber").text(message);
  // } else {
  //   message = "Please input the certificate number!";
  //   $("#cert-enumber").addClass('input-error');
  //   $("#cert-enumber").focus();
  //   $("#cert-evnumber").text(message);
  //   return false;
  // }
  if (cert_status.checked) {
    if(cert_fmonth != ""){
      message = "";
      $("#cert-efmonth").removeClass('input-error');
      $("#cert-evdate").text(message);
    } else {
      message = "Start Month cannot be empty!";
      $("#cert-efmonth").addClass('input-error');
      $("#cert-efmonth").focus();
      $("#cert-evdate").text(message);
      return false;
    }
    if(cert_fyear != ""){
      message = "";
      $("#cert-efyear").removeClass('input-error');
      $("#cert-evdate").text(message);
    } else {
      message = "Start Year cannot be empty!";
      $("#cert-efyear").addClass('input-error');
      $("#cert-efyear").focus();
      $("#cert-evdate").text(message);
      return false;
    }
  } else {
    if(cert_fmonth != ""){
      message = "";
      $("#cert-efmonth").removeClass('input-error');
      $("#cert-evdate").text(message);
    } else {
      message = "Start Month cannot be empty!";
      $("#cert-efmonth").addClass('input-error');
      $("#cert-efmonth").focus();
      $("#cert-evdate").text(message);
      return false;
    }
    if(cert_fyear != ""){
      message = "";
      $("#cert-efyear").removeClass('input-error');
      $("#cert-evdate").text(message);
    } else {
      message = "Start Year cannot be empty!";
      $("#cert-efyear").addClass('input-error');
      $("#cert-efyear").focus();
      $("#cert-evdate").text(message);
      return false;
    }
    if(cert_tmonth != ""){
      if(cert_tyear == cert_fyear){
        if(eval(cert_tmonth) >= eval(cert_fmonth)){
          message = "";
          $("#cert-etmonth").removeClass('input-error');
          $("#cert-evdate").text(message);
        } else {
          message = "End Date has to be later than Start Date!";
          $("#cert-etmonth").addClass('input-error');
          $("#cert-etmonth").focus();
          $("#cert-evdate").text(message);
          return false;
        }
      } else {
        message = "";
        $("#cert-etmonth").removeClass('input-error');
        $("#cert-evdate").text(message);
      }
    } else {
      message = "End Month cannot be empty!";
      $("#cert-etmonth").addClass('input-error');
      $("#cert-etmonth").focus();
      $("#cert-evdate").text(message);
      return false;
    }
    if(cert_tyear != ""){
      if(cert_tyear >= cert_fyear){
        message = "";
        $("#cert-etyear").removeClass('input-error');
        $("#cert-evdate").text(message);
      } else {
        message = "End Date has to be later than Start Date!";
        $("#cert-etyear").addClass('input-error');
        $("#cert-etyear").focus();
        $("#cert-evdate").text(message);
        return false;
      } 
    } else {
      message = "End Year cannot be empty!";
      $("#cert-etyear").addClass('input-error');
      $("#cert-etyear").focus();
      $("#cert-evdate").text(message);
      return false;
    }
  }
  return true;
}
</script>
</div> <!-- end personal cert section -->