<?php 
include("packages/require.php");
include("controller/controller_global.php");
include("controller/controller_blog_detail.php");

$curpage='blog';
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title><?php echo correctDisplay($datas[0]['News_title']);?> | <?=$global['title-blog_detail'];?> |</title>
  <meta name="author" content="CIVIMI">
  <meta name="keywords" content="<?=$datas[0]['News_tag'];?>">
  <meta name="description" content="<?php echo metaDesc($datas[0]['News_content'],200);?>">
  <meta name="robots" content="<?=$seo['robot_yes'];?>">
  <meta itemprop="dateCreated" content="<?php echo date('d M Y',strtotime($datas[0]['News_create_date']));?>">
  <meta itemprop="name" content="<?php echo correctDisplay($datas[0]['News_title']);?>">
  <meta itemprop="description" content="<?php echo charLength(correctDisplay($datas[0]['News_content']),200);?>">
  <meta itemprop="image" content="<?=$global['absolute-url-admin'].$data_photos[0]['Np_img'];?>">
  <!-- SOCIAL MEDIA META -->
  <meta property="og:description" content="<?php echo charLength(correctDisplay($datas[0]['News_content']),200);?>">
  <meta property="og:image" content="<?=$global['absolute-url-admin'].$data_photos[0]['Np_img'];?>">
  <meta property="og:site_name" content="civimi">
  <meta property="og:title" content="<?php echo correctDisplay($datas[0]['News_title']);?>">
  <meta property="og:type" content="website">
  <meta property="og:url" content="<?php echo $path['blog'].$datas[0]['News_ID']."_".encode($datas[0]['News_title']).".html";?>">

  <!-- twitter meta -->
  <meta name="twitter:card" content="summary_large_image">
  <meta name="twitter:description" content="<?php echo charLength(correctDisplay($datas[0]['News_content']),200);?>">
  <meta name="twitter:title" content="<?php echo correctDisplay($datas[0]['News_title']);?>">
  <meta name="twitter:site" content="@civimi">
  <meta name="twitter:creator" content="@civimi">
  <meta name="twitter:image" content="<?=$global['absolute-url-admin'].$data_photos[0]['Np_img'];?>">
  <meta name="twitter:image:src" content="<?=$global['absolute-url-admin'].$data_photos[0]['Np_img'];?>">
  <?php include("packages/head.php");?>
    <link rel="stylesheet" href="<?php echo $global['absolute-url'];?>css/swiper.min.css"/>
    <script src="<?php echo $global['absolute-url'];?>js/swiper.min.js"></script>
    <script language="javascript">
  var popupWindow = null;
  function centeredPopup(url,winName,w,h,scroll){
    LeftPosition = (screen.width) ? (screen.width-w)/2 : 0;
    TopPosition = (screen.height) ? (screen.height-h)/2 : 0;
    settings ='height='+h+',width='+w+',top='+TopPosition+',left='+LeftPosition+',scrollbars='+scroll+',resizable'
    popupWindow = window.open(url,winName,settings)
  }
  </script>
  </head>
  <body>
    <div id="all">
      <!-- start top nav -->
      <?php include("section-top-nav.php");?>
      <!-- end top nav -->

      <!-- start blog part -->
      <div id="blog-section" class="down3">
        <div class="visible-xs up6"></div>
        <div class="blog-head-detail">
          <div class="container">
            <div style="position:relative;">
              <div style="position:absolute;"><h1 class="blog-breadcrumb">Blogs</h1></div>
            </div>
          </div>
        </div>
        <div class="container">
          <div class="row">
            <?php if(is_array($datas)){?>
            <div class="col-md-9 col-sm-8 col-xs-12 pad0">
              <!-- blog image -->
              <div class="row">
                <div class="col-xs-12 xs-pad0" style="position:relative">
                  <?php $total_img=count($data_photos); if ( $total_img > 1 ){ ?>
                  <a href="javascript:;" class="swiper-left hidden-xs hidden-sm"><img class="swiper-arrow-news" alt="prev" src="<?php echo $global['absolute-url'];?>img/cvm-left.png"></a>
                  <a href="javascript:;" class="swiper-right hidden-xs hidden-sm"><img class="swiper-arrow-news" alt="next" src="<?php echo $global['absolute-url'];?>img/cvm-right.png"></a>
                  <?php } ?>
                  <div id="swiper-news" class="swiper-container height-auto">
                    <div class="swiper-wrapper height-auto">
                    <?php for($i=0;$i<$total_img;$i++){?>
                      <div class="swiper-slide height-auto <?php if ( $total_img <= 1 ){ ?>swiper-no-swiping <?php } ?>">
                        <div class="content-slide">
                          <div class="main-swiper">
                            <img src="<?php echo $global['base'].'admin/'.$data_photos[$i]['Np_img'];?>" alt="blog civimi <?php echo $data_photos[$i]['Np_title'];?>" class="swiper-image-news">
                          </div>
                        </div>
                      </div>
                    <?php } ?>
                    </div>
                  </div>
                </div>
              </div>
              <!-- end blog image -->
              <div class="row up1">
                <div class="col-xs-6"><div class="news-date"><?php echo $datas[0]['News_create_date'];?></div></div>
                <div class="col-xs-6">
                  <div class="news-share">
                    <a onclick="centeredPopup('https://www.facebook.com/sharer/sharer.php?u=<?php echo $path['blog'].$datas[0]['News_ID']."_".encode($datas[0]['News_title']).".html";?>','myWindow','500','300','yes');return false" href="#" class="share-icon s-facebook"></a>
                    <a onclick="centeredPopup('https://twitter.com/intent/tweet?url=<?php echo $path['blog'].$datas[0]['News_ID']."_".encode($datas[0]['News_title']).".html";?>&text=<?php echo correctDisplay($datas[0]['News_title']);?>&via=civimi');return false" href="#" class="share-icon s-twitter"></a>
                    <a onclick="centeredPopup('https://plus.google.com/share?url=<?php echo $path['blog'].$datas[0]['News_ID']."_".encode($datas[0]['News_title']).".html";?>','myWindow','500','300','yes');return false" href="#" class="share-icon s-google"></a>
                  </div>
                </div>
              </div>
              <div class="row up2">
                <div class="col-xs-12">
                  <h1 class="bs-title"><?php echo correctDisplay($datas[0]['News_title']);?></h1>
                  <?php if($datas[0]['News_author'] != ""){ ?>
                  <div class="bs-author">by <a href="<?php if($datas[0]['News_author_link'] != ""){ echo $datas[0]['News_author_link']; } else { echo "#"; }?>" <?php if($datas[0]['News_author_link'] != ""){ ?>target="_blank"<?php } ?>><?php echo correctDisplay($datas[0]['News_author']);?></a></div>
                  <?php } ?>
                </div>
              </div>
              <div class="row up2">
                <div class="col-xs-12">
                  <div class="news-desc">
                    <?php echo correctDisplay($datas[0]['News_content']);?>
                  </div>
                </div>
              </div>
              <div class="footer-profile">
                <div class="row">
                  <div class="fp-border"></div>
                  <div class="col-xs-12 col-sm-3 col-md-3 fp-left">
                    <div class="fp-logo"><img src="<?=$global['logo-desktop'];?>" alt="logo"></div>
                  </div>
                  <div class="col-xs-12 col-sm-6 col-md-7 fp-center">
                    <div class="fp-text"><?=$lang['footer-profile'];?></div>
                  </div>
                  <div class="col-xs-12 col-sm-3 col-md-2 fp-right">
                    <div class="btn-fp"><a href="<?=$path['register'];?>">SIGN UP</a></div>
                  </div>
                  <div class="fp-border"></div>
                </div>
              </div>
              <div class="row up2">
                <div class="col-xs-12">
                  <div id="disqus_thread"></div>
<script type="text/javascript">
    /* * * CONFIGURATION VARIABLES * * */
    var disqus_shortname = 'civimi';
    
    /* * * DON'T EDIT BELOW THIS LINE * * */
    (function() {
        var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
        dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
        (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
    })();
</script>
<noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript" rel="nofollow">comments powered by Disqus.</a></noscript>
                </div>
              </div>
              <!-- disqus -->
            </div>
            <?php }else{ ?>
            <div class="col-md-9 col-sm-8 col-xs-12">
              <div class="row">
                <div class="col-xs-12">
                  <h3 class="bs-title">Oops.. there is no article at the moment.</h3>
                </div>
              </div> 
            </div>
            <?php } ?>
            <?php if(is_array($data_relateds)){ ?>
            <div class="col-md-3 col-sm-4 col-xs-12">
              <h3 class="nsrelated-head">
                Related News
              </h3>
              <?php foreach($data_relateds as $data_related){?>
              <a href="<?php echo $path['blog'].$data_related['News_ID']."_".encode($data_related['News_title']).".html";?>" class="nsrelated-box">
                <div class="nsrealted-img"><img src="<?php echo $global['base'].'admin/'.$data_related['Np_img'];?>" alt="blog civimi <?php echo $data_related['Np_title'];?>"></div>
                <div class="nsrealted-title"><?php echo $data_related['News_title'];?></div>
              </a>
              <?php } ?>
              <!-- <div class="subscribe-news">
                <form name="subscribe" action="" method="post">
                  <span>Keep me updated</span> 
                  <input type="email" class="form-control" name="email" placeholder="enter email">
                  <button type="submit" class="btn btn-info btn-block">Give Me Update</button>
                </form>
              </div> -->
            </div>
            <?php } ?>
          </div>
        </div>
      </div>
      <!-- end blog part -->

      <!-- start footer section -->
      <?php include("section-footer.php");?>
      <!-- end footer section -->
    </div><!--  end all div -->
    <script type="text/javascript">
        var contentSwiper = new Swiper('#swiper-news',{
        <?php if ( $total_img > 1 ){ ?>
        autoplay:3000,
        grabCursor: true,
        loop:true,
        <?php } else { ?>
        noSwiping:true,
        <?php } ?>
        speed : 1000,
        });
        $('.swiper-left').on('click', function(e){
            e.preventDefault()
            contentSwiper.swipePrev()
        });
        $('.swiper-right').on('click', function(e){
            e.preventDefault()
            contentSwiper.swipeNext()
        });
    </script>
  </body>
  </html>
