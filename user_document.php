<?php 
include("packages/require.php");
include("controller/controller_global.php");
if(!isset($_SESSION['userData']['id'])){
  $message = "You must login first!";
  $_SESSION['statusLogin'] = $message;
  header("Location:{$path['user-login-failed']}");
}
$curpage='user_document';
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title><?=$_SESSION['userData']['username']."'s ".$global['title-portfolio_doc'];?></title>
  <meta name="keywords" content="">
  <meta name="description" content="">
  <meta name="robots" content="<?=$seo['robot_no'];?>">
  <?php include("packages/head.php");?>
  <link rel="stylesheet" href="<?php echo $global['absolute-url'];?>css/dropzone.css"/>
  <!--<link rel="stylesheet" href="<?php echo $global['absolute-url'];?>css/osx-folder.css"/>!-->
  <script src="<?php echo $global['absolute-url'];?>js/dropzone.js"></script>
<style type="text/css">
.folder {
  /* This will enable the 3D effect. Decrease this value 
   * to make the perspective more pronounced: */
  
  -webkit-perspective: 800px;
  -moz-perspective: 800px;
  perspective: 800px;
  
  /*position: absolute;*/
  top: 50%;
  left: 50%;
  z-index: 0;
  
  width: 160px;
  height: 120px;
  /*margin: -100px 0 0 -60px;*/
  /*margin: 50px 0 0 -80px;*/
}

.folder div{
    width:150px;
    height:115px;
    
    background-color:#93bad8;
    
    /* Enabling 3d space for the transforms */
    -webkit-transform-style: preserve-3d;
    -moz-transform-style: preserve-3d;
    transform-style: preserve-3d;
    
    /* Enabling a smooth animated transition */
    -webkit-transition:0.5s;
    -moz-transition:0.5s;
    transition:0.5s;
    
    /* Disable text seleltion on the folder */
    -webkit-user-select: none;
    -moz-user-select: none;
    user-select: none;
    
    position:absolute;
    top:0;
    left:50%;
    margin-left:-75px;
}

.folder .front{
    border-radius:5px 5px 0 0;
    
    -moz-transform:rotateX(-30deg);
    -webkit-transform:rotateX(-30deg);
    transform:rotateX(-30deg);
    
    -moz-transform-origin:50% 100%;
    -webkit-transform-origin:50% 100%;
    transform-origin:50% 100%;
    
    background-image: -moz-linear-gradient(top, #93bad8 0%, #6c9dc0 85%, #628faf 100%);
    background-image: -webkit-linear-gradient(top, #93bad8 0%, #6c9dc0 85%, #628faf 100%);
    background-image: linear-gradient(top, #93bad8 0%, #6c9dc0 85%, #628faf 100%);
    
    box-shadow:0 -2px 2px rgba(0,0,0,0.1), 0 1px rgba(255,255,255,0.35) inset;
    
    z-index:10;
    
    font: bold 26px sans-serif;
  color: #5A88A9;
  text-align: center;
  text-shadow: 1px 1px 1px rgba(255, 255, 255, 0.1);
  line-height: 115px;
}

.folder .back{
    background-image: -webkit-linear-gradient(top, #93bad8 0%, #89afcc 10%, #5985a5 60%);
    background-image: -moz-linear-gradient(top, #93bad8 0%, #89afcc 10%, #5985a5 60%);
    background-image: linear-gradient(top, #93bad8 0%, #89afcc 10%, #5985a5 60%);
    
    border-radius:0 5px 0 0;
    box-shadow:0 -1px 1px rgba(0,0,0,0.15);
}

.folder .back:before{
    content:'';
    width:60px;
    height:10px;
    border-radius:4px 4px 0 0;
    background-color:#93bad8;
    position:absolute;
    top:-10px;
    left:0px;
    box-shadow:0 -1px 1px rgba(0,0,0,0.15);
}

.folder .back:after{
    content:'';
    width:100%;
    height:4px;
    border-radius:5px;
    position:absolute;
    bottom:5px;
    left:0px;
    box-shadow:0 4px 8px #333;
}

.folder.open .front{
    -moz-transform:rotateX(-50deg);
    -webkit-transform:rotateX(-50deg);
    transform:rotateX(-50deg);
}
</style>
</head>
<body>
  <div id="all">
    <!-- start top nav -->
    <?php include("section-top-nav.php");?>
    <!-- end top nav -->
    <div id="user-section">
    
    <div class="container">
      <div id="profile-content">
        <div class="up5 visible-xs"></div>
        <!-- start fixed mobile nav -->
        <?php $header_content = "Document";  include("user_part-mobile-nav.php");?>
        <!-- end fixed mobile nav-->
        <div class="row">
        <div class="up5 visible-xs"></div>
          <!-- profile sidebar -->
          <div class="col-sm-3 col-xs-12">
            <!-- start sidebar -->
            <?php include("user_part-sidebar.php");?>
            <!-- end sidebar -->
          </div><!-- profile sidebar -->
          <div class="col-sm-9 col-xs-12">
              <div id="personal-portfolio-section" class="job-sbox">
                <div id="education-header" class="user-section-header row border-bottom-eee">
                  <div class="col-xs-8">
                    <div class="profile-title"><i class="glyphicon glyphicon-file"></i>&nbsp;&nbsp;
                      <span class="module-breadcrumb">Document</span>
                      <span class="module-breadcrumb hidden-xs"></span>
                    </div>
                  </div>
                  <div class="col-xs-4">
                      <div class="edit-button text-right">
                        <a href="javascript:;" class="trash-portfolio" title="show remove doc category"><i class="glyphicon glyphicon-trash"></i></a>
                        <a href="javascript:;" class="done-portfolio hide" title="done remove doc category"><i class="glyphicon glyphicon-ok"></i> Done</a>
                      </div>
                  </div>
                </div>
                <div id="portfolio-profile">
                  <div id="doccat-list" class="row"></div>
                </div>
              </div>
          </div>
      </div>
    </div>
            
    </div>
    <!-- end center content -->
  </div>
    <!-- start footer section -->
    <?php include("section-footer.php");?>
    <!-- end footer section -->
  </div><!--  end all div -->
  <!-- Modal -->
  <div class="modal fade" id="modal-portfolio" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content" style="border-radius:0;">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="font-size: 35px;position: absolute;right: 10px;top: 3px;"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel" style="text-align: center;font-size: 20px;font-weight: bold;color: #717171;">Create New Folder</h4>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-sm-4 col-xs-12">
              <div class="portfolio-label bold">Title *</div>
            </div>
            <div class="col-sm-8 col-xs-12">
              <input id="doccat-title" type="text" class="form-control no-radius" placeholder="name of the doc category">
            </div>
          </div>
          <div class="row up1">
            <div class="col-sm-4 col-xs-12">
              <div class="portfolio-label">Description</div>
            </div>
            <div class="col-sm-8 col-xs-12">
              <textarea  id="doccat-desc" class="form-control" rows="3" placeholder="tell story about this doc category"></textarea>
            </div>
          </div>
        </div>
        <div class="modal-footer" style="text-align:left;">
          <div class="btn-group">
            <button id="save-doccat" type="button" class="btn btn-success btn-album">Save</button>
            <button id="cancel-doccat" type="button" class="btn btn-default btn-album" data-dismiss="modal">Cancel</button>
          </div>
          <div id="load-album" class="edit-state hide">
            <img src="<?=$global['absolute-url'];?>img/loadmores.gif" alt="loading">
          </div>
        </div>
      </div>
    </div>
  </div>
  <script src="<?php echo $global['absolute-url'];?>js/function.js"></script>
  <script type="text/javascript">
  $('.trash-portfolio').on('click', function(){
    $('.remove-album').removeClass('hide');
    $('.trash-portfolio').addClass('hide');
    $('.done-portfolio').removeClass('hide');
  });
  $('.done-portfolio').on('click', function(){
    $('.remove-album').addClass('hide');
    $('.done-portfolio').addClass('hide');
    $('.trash-portfolio').removeClass('hide');
  });
  function resetDelete(){
    $('.remove-album').addClass('hide');
    $('.done-portfolio').addClass('hide');
    $('.trash-portfolio').removeClass('hide');
  }
  function imageHover(){
    $('#create-img').attr('src',"<?=$global['absolute-url'];?>img/add-document2.png");
  }
  function imageBack(){
    $('#create-img').attr('src',"<?=$global['absolute-url'];?>img/add-document.png");
  }
  $('#save-doccat').on('click', function(){
    save_doccat();
  })
  get_doccat();
  function get_doccat(){
    var url = "<?=$api['doccat-data'];?>&user_id=<?php echo $_SESSION['userData']['id'];?>&auth_code=<?php echo $_SESSION['userData']['auth_code'];?>";                            
    $.ajax({url: url,success:function(result){
    resetDelete();
    $('#doccat-list').html(replace_doccat(result.data));
    }});
  }
  function replace_doccat(datas){
    var resultHTML='';
        // resultHTML += "<div class='col-sm-3 col-xs-6 col-portfolio'>";
        // resultHTML += "<a href='#modal-portfolio' class='add-portfolio' onmouseover='imageHover();' onmouseout='imageBack();' data-toggle='modal' data-target='#modal-portfolio'>";
        // resultHTML += "<div class='portfolio-content'>";
        // resultHTML += "<img id='create-img' src='img/create-album.png' alt='create doc category' class='portfolio-img'>";
        // resultHTML += "<div class='folder'><div class='front'></div><div class='back'></div></div>";
        // resultHTML += "<div class='portfolio-name'>Create New Folder</div>";
        // resultHTML += "</div>";
        // resultHTML += "</a>";
        // resultHTML += "</div>";

        resultHTML += "<div class='col-sm-3 col-xs-6 col-portfolio'>";
        resultHTML += "<a href='#modal-portfolio' class='add-portfolio' onmouseover='imageHover();' onmouseout='imageBack();' data-toggle='modal' data-target='#modal-portfolio'>";
        resultHTML += "<div class='portfolio-thmb text-center'><img id='create-img' src='<?=$global['absolute-url'];?>img/add-document.png' alt='create folder' class='portfolio-img'></div>";
        resultHTML += "<div class='portfolio-name'>Create Folder</div>";
        resultHTML += "</a>";
        resultHTML += "</div>";
      if(datas != null){
        var obj = datas;    
        for(var i=0;i < obj.length;i++){

        var name = obj[i].DocCat_name;
        var regex = /\\/g;
        var replaceName = name.replace(regex, "").replace(/&quot;/g, "");
        var lowercase = replaceName.toLowerCase();
        var encodeName = lowercase.replace(/\s+/g, '-');

        resultHTML += "<div id='doccat-"+obj[i].DocCat_ID+"' data-doccat_name='"+obj[i].DocCat_name+"' class='col-sm-3 col-xs-6 col-portfolio'>";
        resultHTML += "<div class='portfolio-content'>";
        resultHTML += "<a href='javascript:;' class='remove-album hide' onclick='removeDoccat("+obj[i].DocCat_ID+");' title='delete doc category'><i class='glyphicon glyphicon-remove'></i></a>";
        resultHTML += "<a href='<?=$path['user-doc-folder'];?>"+obj[i].DocCat_ID +"_"+encodeName+".html' class='portfolio-link' title='"+obj[i].DocCat_name+"'>";
        resultHTML += "<div id='pthmb-"+obj[i].DocCat_ID+"' class='portfolio-thmb'><img src='<?=$global['absolute-url'];?>img/placeholder-document.png' alt='primary of "+obj[i].DocCat_name+"' class='portfolio-img'></div>";
        // resultHTML += "<div class='folder'><div class='front'></div><div class='back'></div></div>";
        resultHTML += "<div class='portfolio-name hidden-xs hidden-sm'>"+charString(obj[i].DocCat_name,12)+"</div>";
        resultHTML += "<div class='portfolio-name visible-xs visible-sm'>"+charString(obj[i].DocCat_name,5)+"</div>";
        resultHTML += "</a>";
        resultHTML += "</div>";
        resultHTML += "</div>";
        }
      }
    return resultHTML;
  }
  function profileNav(){
    $('#profile-arrow').toggleClass('nav-close');
  }
  function emptyAddDoccat(){
    $("#doccat-title").val("");
    $("#doccat-desc").val("");
  }
  //delete function
  function removeDoccat(doccat_id){
    var name = $('#doccat-'+doccat_id).data('doccat_name');
    if (confirm("Are you sure to delete \'"+name+"\' ?")) { 

      $('#pthmb-'+doccat_id).append('<img src="<?=$global['absolute-url'];?>img/loader.gif" class="remove-loader" />');
      
      var url = "<?=$api['doccat-delete'];?>";
      var data = {
        user_id : "<?=$_SESSION['userData']['id'];?>",
        auth_code : "<?=$_SESSION['userData']['auth_code'];?>",
        id : doccat_id
      }
      $.ajax({url: url,data : data, success:function(result){
        console.log(result);
        setTimeout(function() {
          $('#doccat-'+doccat_id).remove();
        }, 1500)
      }});

    }
  }
  // save function
  function save_doccat(){
    $(".btn-album").addClass("disabled");
    $("#load-album").removeClass("hide");

    var url = "<?=$api['doccat-insert'];?>";
    var doccat_title = $("#doccat-title").val();
    var doccat_desc = $("#doccat-desc").val();

    var data = {
      user_id : "<?=$_SESSION['userData']['id'];?>",
      auth_code : "<?=$_SESSION['userData']['auth_code'];?>",
      name : doccat_title,
      desc : doccat_desc
    }
    $.ajax({url: url,data : data, success:function(result){
      console.log(result);
      setTimeout(function() {
        get_doccat();
        emptyAddDoccat();
        $('#modal-portfolio').modal('hide');
        $(".btn-album").removeClass("disabled");
        $("#load-album").addClass("hide");
      }, 1500)
    }});
  }
  </script>
</body>
</html>
