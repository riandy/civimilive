<div id="personal-org-section" class="job-sbox" data-order="7">
  <div id="org-header" class="user-section-header row border-bottom-eee">
    <div class="col-xs-6">
      <div class="profile-title"><i class="glyphicon glyphicon-tower"></i>&nbsp;&nbsp;<?=$lang['org-title'];?></div>
    </div>
    <div class="col-xs-6">
        <div class="edit-button text-right">
            <span class="sort-icon"><i class="glyphicon glyphicon-sort"></i></span>
            <a href="Javascript:;" id="add-org" onclick="hideEmptyOrganization();" data-toggle="collapse" data-target="#slide-org" aria-expanded="true" aria-controls="slide-org">
        <i class="glyphicon glyphicon-plus"></i> <span class="hidden-xs"><?=$lang['org-btn'];?></span>
      </a>
        </div>
    </div>
  </div>
  <div id="org-profile" class="">
    <!-- start add org -->
    <div id="slide-org" class="collapse panel-input">
      <div class="panel-input-body panel-input-org">
        <div class="row up15">
          <div class="col-xs-12">
            <div class="form-label bold"><?=$lang['org-job'];?> *</div>
          </div>
          <div class="col-sm-10 col-xs-12">
            <input id="org-title" type="text" name="" class="form-control no-radius" placeholder="<?=$lang['org-job_placeholder'];?>">
            <div id="org-vtitle" class="valid-message"></div>
          </div>
        </div>
        <div class="row up1">
          <div class="col-xs-12">
            <div class="form-label bold"><?=$lang['org-organization'];?> *</div>
          </div>
          <div class="col-sm-10 col-xs-12">
            <div id="org-box" class="organization-box">
              <input id="org-organization" onfocus="$('#org-list').show();" oninput="get_organization();$('#org-list').show();" type="text" name="" class="form-control no-radius" placeholder="<?=$lang['org-organization_placeholder'];?>">
              <div id="org-list" class="organization-list" onmouseover="$('#org-list').show();">
                <ul id="org-ul">

                </ul>
              </div>
            </div>
            <div id="org-vorganization" class="valid-message"></div>
          </div>
        </div>
        <div class="row up1">
          <div class="col-xs-6">
            <div class="form-label bold"><?=$lang['org-country'];?> *</div>
          </div>
          <div class="col-xs-6">
            <div class="form-label bold"><?=$lang['org-city'];?> *</div>
          </div>
        </div>
        <div class="row">
          <div class="col-xs-6">
            <input id="org-country" type="text" class="form-control no-radius" name="country" placeholder="<?=$lang['org-country_placeholder'];?>">
          </div>
          <div class="col-xs-6">
            <input id="org-city" type="text" class="form-control no-radius" name="city" placeholder="<?=$lang['org-city_placeholder'];?>">
          </div>
          <div class="col-xs-12">
            <div id="org-vcountry" class="valid-message"></div>
          </div>
        </div>
        <div class="row up1">
          <div class="col-sm-6 col-xs-12">
            <div class="checkbox">
              <label>
                <input id="org-status" type="checkbox" onclick="checkStatOrg();" value="yes"> <?=$lang['org-attending'];?>
              </label>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-xs-6 up1">
            <div class="form-label bold"><?=$lang['org-from'];?> *</div>
            <div class="row">
              <div class="col-xs-6" style="padding: 0 5px 0 0;">
                <select id="org-fmonth" name="" class="form-control no-radius">
                  <option value="">Month</option>
                  <?php $month = '12';
                  for ($months = '1'; $months <= $month; $months++) { ?>
                  <option value="<?php echo $months; ?>"><?php echo date("F", strtotime("$months/12/10")); ?></option>
                  <?php } ?>
                </select>
              </div>
              <div class="col-xs-6" style="padding: 0 0 0 5px;">
                <select id="org-fyear" name="" class="form-control no-radius">
                  <option value="">--</option>
                  <?php $year = '1950';
                  for ($years = date("Y"); $years >= $year; $years--) { ?>
                  <option value="<?php echo $years; ?>"><?php echo $years; ?></option>
                  <?php } ?>
                </select>
              </div>
            </div>
          </div>
          <div id="to-stat-org" class="col-xs-6 up1">
            <div class="form-label bold"><?=$lang['org-to'];?> *</div>
            <div class="row">
              <div class="col-xs-6" style="padding: 0 5px 0 0;">
                <select id="org-tmonth" name="" class="form-control no-radius">
                  <option value="" class="org-tmonth">Month</option>
                  <?php $month = '12';
                  for ($months = '1'; $months <= $month; $months++) { ?>
                  <option value="<?php echo $months; ?>" class="org-tmonth"><?php echo date("F", strtotime("$months/12/10")); ?></option>
                  <?php } ?>
                </select>
              </div>
              <div class="col-xs-6" style="padding: 0 0 0 5px;">
                <select id="org-tyear" name="" class="form-control no-radius">
                  <option value="" class="org-tyear">--</option>
                  <?php $year = '1950';
                  for ($years = date("Y"); $years >= $year; $years--) { ?>
                  <option value="<?php echo $years; ?>" class="org-tyear"><?php echo $years; ?></option>
                  <?php } ?>
                </select>
              </div>
            </div>
          </div>
          <div class="col-xs-12">
            <div id="org-vdate" class="valid-message"></div>
          </div>
        </div>
        <div class="row up1">
          <div class="col-xs-12">
            <div class="form-label"><?=$lang['org-desc'];?></div>
          </div>
          <div class="col-sm-12 col-xs-12">
            <textarea id="org-desc" name="desc" class="form-control no-radius" rows="4" placeholder="<?=$lang['org-desc_placeholder'];?>"></textarea>
          </div>
        </div>
        <div class="row up2">
          <div class="col-xs-12">
            <a href="Javascript:;" class="btn btn-info" onclick="save_org();">Save</a><span class="pad05"></span>
            <a href="Javascript:;" onclick="hideEmptyOrganization();" type="reset" class="btn btn-default" data-toggle="collapse" data-target="#slide-org" aria-expanded="true" aria-controls="slide-org">Cancel</a>
          </div>
        </div>
      </div>
    </div><!-- end panel add org -->


    <!-- main org -->
    <div id="main-org" class="panel-group marg0" role="tablist" aria-multiselectable="true">
    </div> <!-- end main org -->
    <div class="see-more"><a href="javascript:;" id="see-org" class="hide">SEE <span id="seeOrg-num"></span> MORE</a></div>
    <div id='load-org' class='text-center'></div>

    <!-- start edit org -->
    <div id="edit-org-place" class="hide">
      <div id="edit-org">
        <div class="panel-input-org">
          <div class="row up15">
            <div class="col-xs-12">
              <div class="form-label bold"><?=$lang['org-job'];?> *</div>
            </div>
            <div class="col-sm-10 col-xs-12">
              <input id="org-etitle" type="text" name="" class="form-control no-radius" placeholder="<?=$lang['org-job_placeholder'];?>">
              <div id="org-evtitle" class="valid-message"></div>
            </div>
          </div>
          <div class="row up1">
            <div class="col-xs-12">
              <div class="form-label bold"><?=$lang['org-organization'];?> *</div>
            </div>
            <div class="col-sm-10 col-xs-12">
              <div id="org-boxEdit" class="organization-box">
                <input id="org-eorganization" onfocus="$('#org-listEdit').show();" oninput="get_organizationEdit();$('#org-listEdit').show();" type="text" name="" class="form-control no-radius" placeholder="<?=$lang['org-organization_placeholder'];?>">
                <div id="org-listEdit" class="organization-list" onmouseover="$('#org-listEdit').show();">
                  <ul id="org-eul">

                  </ul>
                </div>
              </div>
              <div id="org-evorganization" class="valid-message"></div>
            </div>
          </div>
          <div class="row up1">
            <div class="col-xs-6">
              <div class="form-label bold"><?=$lang['org-country'];?> *</div>
            </div>
            <div class="col-xs-6">
              <div class="form-label bold"><?=$lang['org-city'];?> *</div>
            </div>
          </div>
          <div class="row">
            <div class="col-xs-6">
              <input id="org-ecountry" type="text" class="form-control no-radius" name="country" placeholder="<?=$lang['org-country_placeholder'];?>">
            </div>
            <div class="col-xs-6">
              <input id="org-ecity" type="text" class="form-control no-radius" name="city" placeholder="<?=$lang['org-city_placeholderi'];?>">
            </div>
            <div class="col-xs-12">
              <div id="org-evcountry" class="valid-message"></div>
            </div>
          </div>
          <div class="row up1">
            <div class="col-sm-6 col-xs-12">
              <div class="checkbox">
                <label>
                  <input id="org-estatus" type="checkbox" onclick="checkStatOrg2();" value="yes"> <?=$lang['org-attending'];?>
                </label>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-xs-6 up1">
              <div class="form-label bold"><?=$lang['org-from'];?> *</div>
              <div class="row">
                <div class="col-xs-6" style="padding: 0 5px 0 0;">
                  <select id="org-efmonth" name="" class="form-control no-radius">
                    <option value="" class="org-efmonth">Month</option>
                    <?php $month = '12';
                    for ($months = '1'; $months <= $month; $months++) { ?>
                    <option value="<?php echo $months; ?>" class="org-efmonth"><?php echo date("F", strtotime("$months/12/10")); ?></option>
                    <?php } ?>
                  </select>
                </div>
                <div class="col-xs-6" style="padding: 0 0 0 5px;">
                  <select id="org-efyear" name="" class="form-control no-radius">
                    <option value="" class="org-efyear">--</option>
                    <?php $year = '1950';
                    for ($years = date("Y"); $years >= $year; $years--) { ?>
                    <option value="<?php echo $years; ?>" class="org-efyear"><?php echo $years; ?></option>
                    <?php } ?>
                  </select>
                </div>
              </div>
            </div>
            <div id="to-estat-org" class="col-xs-6 up1">
              <div class="form-label bold"><?=$lang['org-to'];?> *</div>
              <div class="row">
                <div class="col-xs-6" style="padding: 0 5px 0 0;">
                  <select id="org-etmonth" name="" class="form-control no-radius">
                    <option value="" class="org-etmonth">Month</option>
                    <?php $month = '12';
                    for ($months = '1'; $months <= $month; $months++) { ?>
                    <option value="<?php echo $months; ?>" class="org-etmonth"><?php echo date("F", strtotime("$months/12/10")); ?></option>
                    <?php } ?>
                  </select>
                </div>
                <div class="col-xs-6" style="padding: 0 0 0 5px;">
                  <select id="org-etyear" name="" class="form-control no-radius">
                    <option value="" class="org-etyear">--</option>
                    <?php $year = '1950';
                    for ($years = date("Y"); $years >= $year; $years--) { ?>
                    <option value="<?php echo $years; ?>" class="org-etyear"><?php echo $years; ?></option>
                    <?php } ?>
                  </select>
                </div>
              </div>
            </div>
            <div class="col-xs-12">
              <div id="org-evdate" class="valid-message"></div>
            </div>
          </div>
          <div class="row up1">
            <div class="col-xs-12">
              <div class="form-label"><?=$lang['org-desc'];?></div>
            </div>
            <div class="col-sm-12 col-xs-12">
              <textarea id="org-edesc" name="desc" class="form-control no-radius" rows="3" placeholder="<?=$lang['org-desc_placeholder'];?>"></textarea>
            </div>
          </div>
          <div class="row up1">
            <div class="col-xs-12">
              <input id="edit_org_old_title" type="hidden">
              <input id="edit_org_ATid" type="hidden">
              <a class="btn btn-info" onclick="update_org();" >Save</a><span class="pad05"></span>
              <a id="reset_edit_org" type="reset" class="btn btn-default" onclick="close_edit_org()">Cancel</a>
              <a href="javascript:;" id="delete-org" onclick="delete_org();" class="delete-text">Delete</a>
            </div>
          </div>
        </div>
      </div>
    </div><!-- end edit org -->
  </div> <!-- end org profile -->

<script>
//get organization
$('html').click(function() {
  $('#org-list').hide();
  $("#org-organization").removeClass("load-input");
});
$('#org-box').click(function(event){
  event.stopPropagation();
  $("#org-organization").removeClass("load-input");
});
$('html').click(function() {
  $('#org-listEdit').hide();
  $("#org-eorganization").removeClass("load-input");
});
$('#organization-boxEdit').click(function(event){
  event.stopPropagation();
  $("#org-eorganization").removeClass("load-input");
});
function get_organization(){
  $("#org-organization").addClass("load-input");
  var q = $("#org-organization").val();
  var url = "<?=$api['org-autocomplete'];?>"+q;                            
  $.ajax({url: url,success:function(result){
    $("#org-organization").removeClass("load-input");
    $('#org-ul').html(replace_organization(result.data,q));
  }}); 
}
function get_organizationEdit(){
  $("#org-eorganization").addClass("load-input");
  var q = $("#org-eorganization").val();
  var url = "<?=$api['org-autocomplete'];?>"+q;                            
  $.ajax({url: url,success:function(result){
    $("#org-eorganization").removeClass("load-input");
    $('#org-eul').html(replace_organizationEdit(result.data,q));
  }}); 
}
function replace_organization(datas,text){
  var resultHTML='';

  if(datas != null){
    var obj = datas;    
    for(var i=0;i < obj.length;i++){
      if(obj[i].Org_id != '' && obj[i].Org_title != ''){
        if(obj[i].Country_title !== null ){
          var country = obj[i].Country_title;
        } else {
          var country = '';
        }
        if(obj[i].City_title !== null ){
          var city = obj[i].City_title;
          var coma = ", ";
        } else {
          var city = '';
          var coma = " ";
        }
        resultHTML += "<li class='organization-li' id='org-"+obj[i].Org_id+"' onclick='selectOrganization("+obj[i].Org_id+")' data-og_image='"+obj[i].Org_img+"' data-og_title='"+obj[i].Org_title+"' data-og_country='"+country +"' data-og_city='"+city+"'>";
        if(obj[i].Org_img != ""){
          resultHTML += "<div class='organization-image'><img src='<?=$global['img-url'];?>"+obj[i].Org_img+"' alt='image'></div>";
        }else {
          resultHTML += "<div class='organization-image'><img src='<?=$placeholder['img-org'];?>' alt='image'></div>";
        }
        resultHTML += "<div class='organization-text'>";
        resultHTML += "<div class='organization-title'>"+obj[i].Org_title+"</div>";
        resultHTML += "<div class='organization-place'>"+country+coma+city+"</div>";
        resultHTML += "</div>";
        resultHTML += "</li>";
      }
    }
  } else {
    resultHTML += "<li class='no-list'><div class='no-list-text'><?=$lang['entry-result'];?> \"<span>"+text+"</span>\" <?=$lang['entry-result_text'];?></div></li>";
  }
  return resultHTML;
}
function replace_organizationEdit(datas,text){
  var resultHTML='';

  if(datas != null){
    var obj = datas;    
    for(var i=0;i < obj.length;i++){
      if(obj[i].Org_id != '' && obj[i].Org_title != ''){
        if(obj[i].Country_title !== null ){
          var country = obj[i].Country_title;
        } else {
          var country = '';
        }
        if(obj[i].City_title !== null ){
          var city = obj[i].City_title;
          var coma = ", ";
        } else {
          var city = '';
          var coma = " ";
        }
        resultHTML += "<li class='organization-li' id='orge-"+obj[i].Org_id+"' onclick='selectOrganizationEdit("+obj[i].Org_id+")' data-og_eimage='"+obj[i].Org_img+"' data-og_etitle='"+obj[i].Org_title+"' data-og_ecountry='"+country +"' data-og_ecity='"+city+"'>";
        if(obj[i].Org_img != ""){
          resultHTML += "<div class='organization-image'><img src='<?=$global['img-url'];?>"+obj[i].Org_img+"' alt='image'></div>";
        }else {
          resultHTML += "<div class='organization-image'><img src='<?=$placeholder['img-org'];?>' alt='image'></div>";
        }
        resultHTML += "<div class='organization-text'>";
        resultHTML += "<div class='organization-title'>"+obj[i].Org_title+"</div>";
        resultHTML += "<div class='organization-place'>"+country+coma+city+"</div>";
        resultHTML += "</div>";
        resultHTML += "</li>";
      }
    }
  } else {
    resultHTML += "<li class='no-list'><div class='no-list-text'><?=$lang['entry-result'];?> \"<span>"+text+"</span>\" <?=$lang['entry-result_text'];?></div></li>";
  }
  return resultHTML;
}
function selectOrganization(param){
  var title = $('#org-'+param).data('og_title');
  var country = $('#org-'+param).data('og_country');
  var city = $('#org-'+param).data('og_city');
  $("#org-organization").removeClass("load-input");
  $('#org-organization').val(title);
  $('#org-country').val(country);
  $('#org-city').val(city);
  $('#org-list').hide();
}
function selectOrganizationEdit(param){
  var title = $('#orge-'+param).data('og_etitle');
  var country = $('#orge-'+param).data('og_ecountry');
  var city = $('#orge-'+param).data('og_ecity');
  $("#org-eorganization").removeClass("load-input");
  $('#org-eorganization').val(title);
  $('#org-ecountry').val(country);
  $('#org-ecity').val(city);
  $('#org-listEdit').hide();
}
get_org();
function get_org(){
  $('#see-org').addClass('hide');
  $('#load-org').html("<img src='<?=$global['absolute-url'];?>img/load-blue.gif' style='margin:70px 0' />");
  $('#main-org').html('');
  var url = "<?=$api['org-data'];?>&user_id=<?php echo $_SESSION['userData']['id'];?>&auth_code=<?php echo $_SESSION['userData']['auth_code'];?>";                            
  $.ajax({url: url,success:function(result){
    setTimeout(function() {
      $('#load-org').html('');
      $('#main-org').html(replace_org(result.data));
      seeMoreOrg();
    }, 1000)
  }}); 
}
function replace_org(datas){
  var resultHTML='';resultNext = "";
  var total = 0;previous = "";user_name = "";link="";
  var d = new Date();strip_string ="";
  var current = d.getTime();
  // console.log(datas);
  if(datas != null){
    var obj = datas;    
    for(var i=0;i < obj.length;i++){
      if(obj[i].ATorg_ID != '' && obj[i].ATorg_title != ''){
        resultHTML += "<div class='row row-org hide hm-org' role='tab'>";
        resultHTML += "<div class='col-xs-12'>";
        resultHTML += "<a href='#ogcollapse-"+obj[i].ATorg_ID+"' aria-controls='ogcollapse-"+obj[i].ATorg_ID+"' data-parent='#main-org' onclick='edit_box_org("+obj[i].ATorg_ID+");' data-toggle='collapse' aria-expanded='false' id='org-"+obj[i].ATorg_ID+"' class='org-box' data-org_atid='"+obj[i].ATorg_ID+"' data-org_country='"+obj[i].Country_title+"' data-org_city='"+obj[i].City_title+"' data-org_status='"+obj[i].ATorg_current+"' data-org_organization='"+obj[i].Org_title+"' data-org_title='"+obj[i].ATorg_title+"' data-org_fmonth='"+obj[i].ATorg_from_month+"' data-org_fyear='"+obj[i].ATorg_from_year+"' data-org_tmonth='"+obj[i].ATorg_to_month+"' data-org_tyear='"+obj[i].ATorg_to_year+"' data-org_desc='"+obj[i].ATorg_desc+"'>";
        resultHTML += "<div class='row'>";
        resultHTML += "<div class='col-md-10 col-xs-9 pad0'>";
        resultHTML += "<div class='org-title'>"+obj[i].ATorg_title+" <span class='org-etext edit-button'><i class='glyphicon glyphicon-edit'></i> <span class='hidden-xs'>Edit</span></span></div>";
        resultHTML += "<div class='org-organization'>"+obj[i].Org_title+"</div>";
        resultHTML += "<div class='org-location'>"+obj[i].City_title+", "+obj[i].Country_title+"</div>";
        if(obj[i].ATorg_to_year == "" || obj[i].ATorg_to_year == 0){
          var toYear = "Present";
        } else {
          var toYear = obj[i].ATorg_to_year;
        }
        resultHTML += "<div class='org-year-mobile visible-xs'>"+obj[i].ATorg_from_year+" - "+toYear+"</div>";
        resultHTML += "</div>";
        resultHTML += "<div class='col-md-2 col-xs-3'>";
        if(obj[i].Org_img != ''){
          resultHTML += "<div class='org-image'><img src='<?=$global['img-url'];?>"+obj[i].Org_img+"' alt='"+obj[i].Org_title+"'></div>";
        }else {
          resultHTML += "<div class='org-image'><img src='<?=$placeholder['img-org'];?>' alt='image'></div>";
        }
          
        resultHTML += "<div class='org-year hidden-xs'>"+obj[i].ATorg_from_year+" - "+toYear+"</div>";
        resultHTML += "</div>";
        resultHTML += "</div>";
        resultHTML += "</a>";
        resultHTML += "<div id='load-org-"+obj[i].ATorg_ID+"' class='text-center'></div>";
        resultHTML += "<a id='close-org-"+obj[i].ATorg_ID+"' href='#ogcollapse-"+obj[i].ATorg_ID+"' aria-controls='ogcollapse-"+obj[i].ATorg_ID+"' data-parent='#main-org' data-toggle='collapse' aria-expanded='false' class='hide'></a>";
        resultHTML += "</div>";
        resultHTML += "</div>";
        resultHTML += "<div id='ogcollapse-"+obj[i].ATorg_ID+"' aria-labelledby='org-"+obj[i].ATorg_ID+"' class='panel-collapse collapse collapse-org pad0' role='tabpanel'></div>";
      }
    }
  } else {
    resultHTML += "<div id='empty-org' class='empty-file'><?=$lang['org-placeholder'];?></div>";
  }
  return resultHTML;
}
function seeMoreOrg(){
    size = $(".row-org").size();
    x=2;

    $('.row-org:lt('+x+')').removeClass('hide');
    $('.row-org:lt('+x+')').removeClass('hm-org'); 
    size_left= $(".hm-org").size();
    if(size > 2){
      $('#see-org').removeClass('hide');
      $("#seeOrg-num").text('('+size_left+')');
    } else {
      $('#see-org').addClass('hide');
    }

    $('#see-org').on('click', function(){
        $('.row-org').removeClass('hide');
        $('.row-org').removeClass('hm-org');
        $('#see-org').addClass('hide');
    });
};
function ogshowhide(param){
  var teks = $('#org-link'+param).text();
  if(teks == 'Hide Desc'){
    $('#org-link'+param).text('Show Desc');
  } else{
    $('#org-link'+param).text('Hide Desc');
  }
}
//clear value add org
function emptyAddOrganization(){
  $("#org-title").val('');
  $("#org-organization").val('');
  $("#org-country").val('');
  $("#org-city").val('');
  $("#org-desc").val('');
  $("#org-fmonth").val('');
  $("#org-fyear").val('');
  $("#org-tmonth").val('');
  $("#org-tyear").val('');
  document.getElementById("org-status").checked = false;
  $("#to-stat-org").removeClass("hide");
}
//open slide edit
function edit_box_org(param){
  if ($('#slide-org').hasClass('in')){
    $('#slide-org').collapse('hide');
  }
  if($('.org-box').hasClass('hide-org')){
    $('.org-box').removeClass('hide-org');
    if($('.collapse-org').hasClass('in')){
      $('.collapse-org').removeClass('in')
    }
    $('#org-'+param).addClass('hide-org');
  } else {
    $('#org-'+param).addClass('hide-org');
  }
  $('#edit-org').detach().appendTo('#ogcollapse-'+param+'');
  var ATid = $('#org-'+param).data('org_atid');
  var title = $('#org-'+param).data('org_title');
  var organization = $('#org-'+param).data('org_organization');
  var status = $('#org-'+param).data('org_status');
  var country = $('#org-'+param).data('org_country');
  var city = $('#org-'+param).data('org_city');
  var desc = $('#org-'+param).data('org_desc');
  var fmonth = $('#org-'+param).data('org_fmonth');
  var fyear = $('#org-'+param).data('org_fyear');
  var tmonth = $('#org-'+param).data('org_tmonth');
  var tyear = $('#org-'+param).data('org_tyear');

  $('#reset_edit_org').data("target") === "#ogcollapse";
  $('#org-etitle').val(title);
  $('#org-eorganization').val(organization);
  $('#org-ecountry').val(country);
  $('#org-ecity').val(city);
  $('#org-edesc').val(desc);
  $('#edit_org_old_title').val(title);
  $('#edit_org_ATid').val(ATid);
  $('option[value="' + fmonth + '"].org-efmonth').attr("selected", true);
  $('option[value="' + fyear + '"].org-efyear').attr("selected", true);
  if(status == 'yes') {
    document.getElementById("org-estatus").checked = true;
  } else {
    document.getElementById("org-estatus").checked = false;
    $('option[value="' + tmonth + '"].org-etmonth').attr("selected", true);
    $('option[value="' + tyear + '"].org-etyear').attr("selected", true);
  }
  checkStatOrg2();
}
//close edit org
function close_edit_org(){
  var id = $('#edit_org_ATid').val();
  $('#close-org-'+id+'').click();
  $('#org-'+id).removeClass('hide-org');
}
//hide empty word when add new
function hideEmptyOrganization(){
  var myElem = document.getElementById('empty-org');
  if(myElem != null){
    $('#empty-org').toggle();
  }
  var id = $('#edit_org_ATid').val();
  if (id != ''){
    if ($('#ogcollapse-'+id).hasClass('in')){
      $('#close-org-'+id+'').click();
      $('#org-'+id).removeClass('hide-org');
    }
  }
  emptyAddOrganization();
}
//update function
function update_org(){
  if( validOrganizationEdit() ){
    $('#edit-org').detach().appendTo('#edit-org-place');
    var iorg = $('#edit_org_ATid').val();
    $('#close-org-'+iorg).click();
    $('html, body').animate({
      scrollTop: $('#org-header').offset().top-80
    }, 'fast');

    var url = "<?=$api['org-update'];?>";
    var org_id = $("#edit_org_ATid").val();
    var org_title = $("#org-etitle").val();
    var org_organization = $("#org-eorganization").val();
    var org_country = $("#org-ecountry").val();
    var org_city = $("#org-ecity").val();
    var org_desc = $("#org-edesc").val();
    var org_fmonth = $("#org-efmonth").val();
    var org_fyear = $("#org-efyear").val();
    var org_tmonth = $("#org-etmonth").val();
    var org_tyear = $("#org-etyear").val();
    var org_status = document.getElementById('org-estatus');
    if (org_status.checked) {
      org_status = 'yes';
    } else {
      org_status = 'no';
    }
    var data = {
      user_id : "<?=$_SESSION['userData']['id'];?>",
      auth_code : "<?=$_SESSION['userData']['auth_code'];?>",
      id : org_id,
      title : org_title,
      city : org_city,
      country : org_country,
      organization : org_organization,
      current : org_status,
      from_month : org_fmonth,
      from_year : org_fyear,
      to_month : org_tmonth,
      to_year : org_tyear,
      desc : org_desc
    }
    $.ajax({url: url,data : data, success:function(result){
      get_org();
      console.log(result);
    }});
  }
}
//insert function
function save_org(){
  if( validOrganization() ){
    $('#edit-org').detach().appendTo('#edit-org-place');
    $('html, body').animate({
      scrollTop: $('#org-header').offset().top-80
    }, 'fast');

    var url = "<?=$api['org-update'];?>";
    var org_title = $("#org-title").val();
    var org_organization = $("#org-organization").val();
    var org_country = $("#org-country").val();
    var org_city = $("#org-city").val();
    var org_desc = $("#org-desc").val();
    var org_fmonth = $("#org-fmonth").val();
    var org_fyear = $("#org-fyear").val();
    var org_tmonth = $("#org-tmonth").val();
    var org_tyear = $("#org-tyear").val();
    var org_status = document.getElementById('org-status');
    if (org_status.checked) {
      org_status = 'yes';
    } else {
      org_status = 'no';
    }
    var data = {
      user_id : "<?=$_SESSION['userData']['id'];?>",
      auth_code : "<?=$_SESSION['userData']['auth_code'];?>",
      organization : org_organization,
      title : org_title,
      city : org_city,
      country : org_country,
      current : org_status,
      from_month : org_fmonth,
      from_year : org_fyear,
      to_month : org_tmonth,
      to_year : org_tyear,
      desc : org_desc
    }
    $.ajax({url: url,data : data, success:function(result){
      $('#slide-org').collapse('hide');
      get_org();
      emptyAddOrganization();
      console.log(result);
    }});
  }
}
//delete function
function delete_org(){
  if (confirm("Are you sure to delete this information ?")) {
    $('#edit-org').detach().appendTo('#edit-org-place');
    $('html, body').animate({
      scrollTop: $('#org-header').offset().top-80
    }, 'fast');
    
    var url = "<?=$api['org-delete'];?>";
    var org_id = $("#edit_org_ATid").val();
    var data = {
      user_id : "<?=$_SESSION['userData']['id'];?>",
      auth_code : "<?=$_SESSION['userData']['auth_code'];?>",
      id : org_id
    }
    $.ajax({url: url,data : data, success:function(result){
      get_org();
      console.log(result);
    }});
  }
}
function checkStatOrg() {
var s = document.getElementById('org-status');
  if (s.checked) {
    if($("#to-stat-org").hasClass("hide")){
          //nothing happen
    } else {
      $("#to-stat-org").addClass("hide");
    $('option[value=""].org-tmonth').attr("selected", true);
    $('option[value=""].org-tyear').attr("selected", true);
    }
  } else {
    if($("#to-stat-org").hasClass("hide")){
      $("#to-stat-org").removeClass("hide");
    }
  }
}
function checkStatOrg2() {
var s = document.getElementById('org-estatus');
  if (s.checked) {
    if($("#to-estat-org").hasClass("hide")){
          //nothing happen
    } else {
      $("#to-estat-org").addClass("hide");
    $('option[value=""].org-etmonth').attr("selected", true);
    $('option[value=""].org-etyear').attr("selected", true);
    }
  } else {
    if($("#to-estat-org").hasClass("hide")){
      $("#to-estat-org").removeClass("hide");
    }
  }
}
function validOrganization(){
  var org_title = $("#org-title").val();
  var org_organization = $("#org-organization").val();
  var org_country = $("#org-country").val();
  var org_city = $("#org-city").val();
  var org_desc = $("#org-desc").val();
  var org_fmonth = $("#org-fmonth").val();
  var org_fyear = $("#org-fyear").val();
  var org_tmonth = $("#org-tmonth").val();
  var org_tyear = $("#org-tyear").val();
  var org_status = document.getElementById('org-status');
  var message = "";


  if(org_organization != ""){
    if(org_organization.length >= 3){
      message = "";
      $("#org-organization").removeClass('input-error');
      $("#org-vorganization").text(message);
    } else {
      message = "organization Name has to be at least 3 characters!";
      $("#org-organization").addClass('input-error');
      $("#org-organization").focus();
      $("#org-vorganization").text(message);
      return false;
    }
  } else {
    message = "Please input or select your organization name!";
    $("#org-organization").addClass('input-error');
    $("#org-organization").focus();
    $("#org-vorganization").text(message);
    return false;
  }
  if(org_title != ""){
    if(org_title.length >= 3){
      message = "";
      $("#org-title").removeClass('input-error');
      $("#org-vtitle").text(message);
    } else {
      message = "Job title has to be at least 3 characters!";
      $("#org-title").addClass('input-error');
      $("#org-title").focus();
      $("#org-vtitle").text(message);
      return false;
    }
  } else {
    message = "Please input your Job Title!";
    $("#org-title").addClass('input-error');
    $("#org-title").focus();
    $("#org-vtitle").text(message);
    return false;
  }
  if(org_country != ""){
    message = "";
    $("#org-country").removeClass('input-error');
    $("#org-vcountry").text(message);
  } else {
    message = "Please input Country of organization!";
    $("#org-country").addClass('input-error');
    $("#org-country").focus();
    $("#org-vcountry").text(message);
    return false;
  }
  if(org_city != ""){
    message = "";
    $("#org-city").removeClass('input-error');
    $("#org-vcountry").text(message);
  } else {
    message = "Please input City of organization!";
    $("#org-city").addClass('input-error');
    $("#org-city").focus();
    $("#org-vcountry").text(message);
    return false;
  }
  if (org_status.checked) {
    if(org_fmonth != ""){
      message = "";
      $("#org-fmonth").removeClass('input-error');
      $("#org-vdate").text(message);
    } else {
      message = "Start Month cannot be empty!";
      $("#org-fmonth").addClass('input-error');
      $("#org-fmonth").focus();
      $("#org-vdate").text(message);
      return false;
    }
    if(org_fyear != ""){
      message = "";
      $("#org-fyear").removeClass('input-error');
      $("#org-vdate").text(message);
    } else {
      message = "Start Year cannot be empty!";
      $("#org-fyear").addClass('input-error');
      $("#org-fyear").focus();
      $("#org-vdate").text(message);
      return false;
    }
  } else {
    if(org_fmonth != ""){
      message = "";
      $("#org-fmonth").removeClass('input-error');
      $("#org-vdate").text(message);
    } else {
      message = "Start Month cannot be empty!";
      $("#org-fmonth").addClass('input-error');
      $("#org-fmonth").focus();
      $("#org-vdate").text(message);
      return false;
    }
    if(org_fyear != ""){
      message = "";
      $("#org-fyear").removeClass('input-error');
      $("#org-vdate").text(message);
    } else {
      message = "Start Year cannot be empty!";
      $("#org-fyear").addClass('input-error');
      $("#org-fyear").focus();
      $("#org-vdate").text(message);
      return false;
    }
    if(org_tmonth != ""){
      if(org_tyear == org_fyear){
        if(eval(org_tmonth) >= eval(org_fmonth)){
          message = "";
          $("#org-tmonth").removeClass('input-error');
          $("#org-vdate").text(message);
        } else {
          message = "End Date has to be later than Start Date!";
          $("#org-tmonth").addClass('input-error');
          $("#org-tmonth").focus();
          $("#org-vdate").text(message);
          return false;
        }
      } else {
        message = "";
        $("#org-tmonth").removeClass('input-error');
        $("#org-vdate").text(message);
      }
    } else {
      message = "End Month cannot be empty!";
      $("#org-tmonth").addClass('input-error');
      $("#org-tmonth").focus();
      $("#org-vdate").text(message);
      return false;
    }
    if(org_tyear != ""){
      if(org_tyear >= org_fyear){
        message = "";
        $("#org-tyear").removeClass('input-error');
        $("#org-vdate").text(message);
      } else {
        message = "End Date has to be later than Start Date!";
        $("#org-tyear").addClass('input-error');
        $("#org-tyear").focus();
        $("#org-vdate").text(message);
        return false;
      } 
    } else {
      message = "End Year cannot be empty!";
      $("#org-tyear").addClass('input-error');
      $("#org-tyear").focus();
      $("#org-vdate").text(message);
      return false;
    }
  }
  return true;
}
function validOrganizationEdit(){
  var org_title = $("#org-etitle").val();
  var org_organization = $("#org-eorganization").val();
  var org_country = $("#org-ecountry").val();
  var org_city = $("#org-ecity").val();
  var org_desc = $("#org-edesc").val();
  var org_fmonth = $("#org-efmonth").val();
  var org_fyear = $("#org-efyear").val();
  var org_tmonth = $("#org-etmonth").val();
  var org_tyear = $("#org-etyear").val();
  var org_status = document.getElementById('org-estatus');
  var message = "";

  if(org_organization != ""){
    if(org_organization.length >= 3){
      message = "";
      $("#org-eorganization").removeClass('input-error');
      $("#org-evorganization").text(message);
    } else {
      message = "organization Name has to be at least 3 characters!";
      $("#org-eorganization").addClass('input-error');
      $("#org-eorganization").focus();
      $("#org-evorganization").text(message);
      return false;
    }
  } else {
    message = "Please input or select your organization name!";
    $("#org-eorganization").addClass('input-error');
    $("#org-eorganization").focus();
    $("#org-evorganization").text(message);
    return false;
  }
  if(org_title != ""){
    if(org_title.length >= 3){
      message = "";
      $("#org-etitle").removeClass('input-error');
      $("#org-evtitle").text(message);
    } else {
      message = "Job title has to be at least 3 characters!";
      $("#org-etitle").addClass('input-error');
      $("#org-etitle").focus();
      $("#org-evtitle").text(message);
      return false;
    }
  } else {
    message = "Please input your Job Title!";
    $("#org-etitle").addClass('input-error');
    $("#org-etitle").focus();
    $("#org-evtitle").text(message);
    return false;
  }
  if(org_country != ""){
    message = "";
    $("#org-ecountry").removeClass('input-error');
    $("#org-evcountry").text(message);
  } else {
    message = "Please input Country of organization!";
    $("#org-ecountry").addClass('input-error');
    $("#org-ecountry").focus();
    $("#org-evcountry").text(message);
    return false;
  }
  if(org_city != ""){
    message = "";
    $("#org-ecity").removeClass('input-error');
    $("#org-evcountry").text(message);
  } else {
    message = "Please input City of organization!";
    $("#org-ecity").addClass('input-error');
    $("#org-ecity").focus();
    $("#org-evcountry").text(message);
    return false;
  }
  if (org_status.checked) {
    if(org_fmonth != ""){
      message = "";
      $("#org-efmonth").removeClass('input-error');
      $("#org-evdate").text(message);
    } else {
      message = "Start Month cannot be empty!";
      $("#org-efmonth").addClass('input-error');
      $("#org-efmonth").focus();
      $("#org-evdate").text(message);
      return false;
    }
    if(org_fyear != ""){
      message = "";
      $("#org-efyear").removeClass('input-error');
      $("#org-evdate").text(message);
    } else {
      message = "Start Year cannot be empty!";
      $("#org-efyear").addClass('input-error');
      $("#org-efyear").focus();
      $("#org-evdate").text(message);
      return false;
    }
  } else {
    if(org_fmonth != ""){
      message = "";
      $("#org-efmonth").removeClass('input-error');
      $("#org-evdate").text(message);
    } else {
      message = "Start Month cannot be empty!";
      $("#org-efmonth").addClass('input-error');
      $("#org-efmonth").focus();
      $("#org-evdate").text(message);
      return false;
    }
    if(org_fyear != ""){
      message = "";
      $("#org-efyear").removeClass('input-error');
      $("#org-evdate").text(message);
    } else {
      message = "Start Year cannot be empty!";
      $("#org-efyear").addClass('input-error');
      $("#org-efyear").focus();
      $("#org-evdate").text(message);
      return false;
    }
    if(org_tmonth != ""){
      if(org_tyear == org_fyear){
        if(eval(org_tmonth) >= eval(org_fmonth)){
          message = "";
          $("#org-etmonth").removeClass('input-error');
          $("#org-evdate").text(message);
        } else {
          message = "End Date has to be later than Start Date!";
          $("#org-etmonth").addClass('input-error');
          $("#org-etmonth").focus();
          $("#org-evdate").text(message);
          return false;
        }
      } else {
        message = "";
        $("#org-etmonth").removeClass('input-error');
        $("#org-evdate").text(message);
      }
    } else {
      message = "End Month cannot be empty!";
      $("#org-etmonth").addClass('input-error');
      $("#org-etmonth").focus();
      $("#org-evdate").text(message);
      return false;
    }
    if(org_tyear != ""){
      if(org_tyear >= org_fyear){
        message = "";
        $("#org-etyear").removeClass('input-error');
        $("#org-evdate").text(message);
      } else {
        message = "End Date has to be later than Start Date!";
        $("#org-etyear").addClass('input-error');
        $("#org-etyear").focus();
        $("#org-evdate").text(message);
        return false;
      } 
    } else {
      message = "End Year cannot be empty!";
      $("#org-etyear").addClass('input-error');
      $("#org-etyear").focus();
      $("#org-evdate").text(message);
      return false;
    }
  }
  return true;
}
</script>
</div> <!-- end personal org section -->