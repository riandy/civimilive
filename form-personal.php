<?php 
include("packages/require.php");
include("controller/controller_form_personal.php");
$curpage='form_personal';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title><?=$global['title-form_personal'];?></title>
    <meta name="keywords" content="">
    <meta name="description" content="">
    <?php include("packages/head.php");?>
  <link rel="stylesheet" href="<?php echo $global['absolute-url'];?>css/dropzone.css"/>
  <script src="<?php echo $global['absolute-url'];?>js/dropzone.js"></script>
</head>
<body>
    <div id="all">
        <!-- start top nav -->
        <?php include("section-top-nav.php");?>
        <!-- end top nav -->
        <?php if(isset($_SESSION['userData']['id']) && ($datas[0]['User_step_personal'] == "No" || $datas[0]['User_step_personal'] == "")){ ?>
        <div class="register-section" style="background: url('<?=$global['base'];?>admin/img/pattern/pattern.png');">
            <div class="register-wrapper" style="background-color: transparent;">
                <div class="container container-max">
                    <div class="up7 visible-xs"></div>
                    <div class="row up5">
                        <div class="col-xs-12">
                            <div class="fpersonal-header"><?=$lang['fpersonal-welcome'];?></div>
                            <div align="center" style="color: #a94442;font-weight: bold;font-size: 13px;margin: 3px 0;"><?php echo $message;?></div>
                            <div class="text-center up3">
                                <form action="" class="uploadform dropzone-profile dropzone no-margin dz-clickable" style="max-width: 200px;margin: 0 auto;">
                                    <div class="profile-img-box">
                                        <img id="img-eprofile" class="img-profile" src="<?php echo check_image_url($datas[0]['User_proPhotoThmb']);?>" style="border-radius: 50%;" />
                                        <div id="load-profile" class="drop-load hide" style="border-radius: 50%;"><div class="drop-load-img"><img src="<?=$global['absolute-url'];?>img/loader.gif" alt="load image"></div></div>
                                    </div>
                                </form>
                            </div>
                            <div class="fpersonal-img-text"><?=$lang['fpersonal-upload'];?></div>
                            <div class="fpersonal-welcome">Hi, <?php if($datas[0]['User_fname'] != ""){echo $datas[0]['User_fname']." ".$datas[0]['User_lname'];}else{echo $datas[0]['User_username'];}?>!</div>
                            <div class="fpersonal-info"><?=$lang['fpersonal-info'];?></div>
                        </div>
                    </div>
                    <form name="formPersonal" action="<?=$path['form-personal-action'];?>" method="post" enctype="multipart/form-data" onsubmit="return personalValid()" style="max-width: 500px;margin: 0 auto;">
                        <div class="row up3">
                            <div class="col-sm-3 col-xs-12"><?=$lang['fpersonal-first'];?></div>
                            <div class="col-sm-9 col-xs-12"><input id="fname" type="text" name="fname" class="form-control no-radius" placeholder="<?=$lang['fpersonal-first'];?>" value="<?php echo $datas[0]['User_fname'];?>" autocomplete="off" /></div>
                        </div>
                        <div class="row up15">
                            <div class="col-sm-3 col-xs-12"><?=$lang['fpersonal-last'];?></div>
                            <div class="col-sm-9 col-xs-12"><input id="lname" type="text" name="lname" class="form-control no-radius" placeholder="<?=$lang['fpersonal-last'];?>" value="<?php echo $datas[0]['User_lname'];?>" autocomplete="off" /></div>
                        </div>
                        <div class="row up15">
                            <div class="col-sm-3 col-xs-12"><?=$lang['fpersonal-email'];?></div>
                            <div class="col-sm-9 col-xs-12"><input id="email" type="email" name="email" class="form-control no-radius" placeholder="<?=$lang['fpersonal-email'];?>" value="<?php echo $datas[0]['User_email'];?>" <?php if($datas[0]['User_email'] != ''){ echo "readonly";}?> autocomplete="off"/></div>
                        </div>
                        <div class="row up15">
                            <div class="col-sm-3 col-xs-12"><?=$lang['fpersonal-username'];?></div>
                            <div class="col-sm-9 col-xs-12">
                                <input id="username" type="text" name="username" class="form-control no-radius" placeholder="<?=$lang['fpersonal-username'];?>" oninput="checkUser();" value="<?php echo $datas[0]['User_username'];?>" <?php if($datas[0]['User_username'] != ''){ echo "readonly";}?> autocomplete="off"/>
                                <input id="username_status" type="hidden">
                            </div>
                        </div>
                        <div class="row up15">
                            <div class="col-sm-3 col-xs-12"><?=$lang['fpersonal-dob'];?></div>
                            <div class="col-sm-3 col-xs-4">
                                <select id="day" class="form-control no-radius" name="birthday1" style="padding:6px;">
                                  <option value="">day</option>
                                  <?php for($dd=1;$dd <= 31;$dd++){?>
                                  <option value="<?=$dd;?>" <?php if($datas[0]['User_DOBday'] == $dd){echo "selected";};?> ><?=$dd;?></option>
                                  <?php } ?>
                                </select>
                            </div>
                            <div class="col-sm-3 col-xs-4 pad0">
                                <select id="month" class="form-control no-radius" name="birthday2" style="padding:6px;">
                                  <option value="">month</option>
                                  <?php $arr_month = array("Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec");for($mm=1;$mm <= 12;$mm++){?>
                                  <option value="<?=$mm;?>" <?php if($datas[0]['User_DOBmonth'] == $mm){echo "selected";};?>><?=$arr_month[$mm-1];?></option>
                                  <?php } ?>
                                </select>
                            </div>
                            <div class="col-sm-3 col-xs-4">
                                <select id="year" class="form-control no-radius" name="birthday3" style="padding:6px;">
                                  <option value="">year</option>
                                  <?php $curr_year = date("Y");$begin_year = $curr_year -70;for($yy=$begin_year;$yy <= $curr_year-10;$yy++){?>
                                  <option value="<?=$yy;?>" <?php if($datas[0]['User_DOB'] == $yy){echo "selected";};?> ><?=$yy;?></option>
                                  <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="row up15">
                            <div class="col-sm-3 col-xs-12"><?=$lang['fpersonal-location'];?></div>
                            <div class="col-sm-9 col-xs-12 col-location">
                                <select id="location" class="form-control js-example-basic-single no-radius" name="location" name="location" style="height:34px !important;">
                                    <option value="">Choose Location</option>
                                    <?php foreach ($data_country as $data) { ?>
                                    <option <?php if($datas[0]['User_country'] == $data['Country_title']){ echo "selected";}?> value="<?php echo $data['Country_title'];?>"><?php echo $data['Country_title'];?></option>
                                    <?php }?>
                                </select>
                            </div>
                        </div>
                        <div class="row up15">
                            <div class="col-sm-3 col-xs-12"><?=$lang['fpersonal-city'];?></div>
                            <div class="col-sm-9 col-xs-12"><input id="city" type="text" name="city" class="form-control no-radius" placeholder="<?=$lang['fpersonal-city_placeholder'];?>" value="<?php echo $datas[0]['User_city'];?>" autocomplete="off" /></div>
                        </div>
                        <div class="row up15">
                            <div class="col-sm-3 col-xs-12"><?=$lang['fpersonal-state'];?></div>
                            <div class="col-sm-9 col-xs-12"><input id="state" type="text" name="state" class="form-control no-radius" placeholder="<?=$lang['fpersonal-state_placeholder'];?>" value="<?php echo $datas[0]['User_state'];?>" autocomplete="off" /></div>
                        </div>
                        <div class="row up15">
                            <div class="col-sm-3 col-xs-12"><?=$lang['fpersonal-phone'];?></div>
                            <div class="col-sm-9 col-xs-12"><input id="phone" type="text" name="phone" class="form-control no-radius" placeholder="<?=$lang['fpersonal-phone_placeholder'];?>" value="<?php echo $datas[0]['User_phone'];?>" autocomplete="off" /></div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12"><div id="alert-personal" class="control-alert text-center up1"></div></div>
                        </div>
                        <div class="row up2">
                            <div class="col-xs-12 text-right">
                                <input type="hidden" name="id" value="<?php echo $_SESSION['userData']['id'];?>">
                                <input type="hidden" name="auth_code" value="<?php echo $datas[0]['User_auth_code'];?>">
                                <input type="hidden" name="via" value="<?php echo $datas[0]['User_login_via'];?>">
                                <!--<input type="hidden" id="profile-img" value="<?php echo $datas[0]['User_proPhotoThmb'];?>">!-->
                                <button type="submit" class="btn btn-info btn-fpersonal"><?=$lang['fpersonal-next'];?> <i class="glyphicon glyphicon-chevron-right"></i></button>
                            </div>
                        </div>
                    </form>
                    <div class="up3"></div>
                </div>
            </div>
        </div>
        <?php }else{ ?>
        <div class="register-section" style="background: url('<?=$global['base'];?>admin/img/pattern/pattern.png');">
            <div class="register-wrapper" style="background-color: transparent;">
                <div class="container container-max">
                    <div class="up7 visible-xs"></div>
                    <div class="row up5">
                        <div class="col-xs-12">
                            <div class="fpersonal-header">Not Connected</div>
                        </div>
                    </div>
                    <div class="up3"></div>
                </div>
            </div>
        </div>
         <?php } ?>
        <!-- start footer section -->
        <?php include("section-footer.php");?>
        <!-- end footer section -->
    </div><!--  end all div -->
    <script type="text/javascript">
        function checkUser(){
            var username = $("#username").val();
            var userformat = /^[a-zA-Z0-9\_]+$/;
            if(username.length >= 5){
                if(username.match(userformat)){
                    $("#username").addClass("load-input");
                    var url = "<?=$api['check-username'];?>"+username;                            
                    $.ajax({url: url,success:function(result){
                        var stat = result.status;
                        $("#username_status").val(stat);
                        if(stat != 0){
                            $("#username").removeClass("load-input");
                            $("#alert-personal").text("");
                            $("#alert-personal").hide();
                        } else {
                            $("#username").removeClass("load-input");
                            $("#username").focus();
                            $("#alert-personal").text("username already exist input another one");
                            $("#alert-personal").show();
                            return false;
                        }
                    }});
                } else {
                    $("#username").focus();
                    $("#alert-personal").text("username only contain alphabet, numeric and underscore");
                    $("#alert-personal").show();
                    return false;
                } 
            } else if (username.length < 5){
                $("#username").focus();
                $("#alert-personal").text("username has to be more than 4 characters");
                $("#alert-personal").show();
                return false;
            }
        }
        function personalValid(){
            //var image = $("#profile-img").val();
            var fname = $("#fname").val();
            var lname = $("#lname").val();
            var username = $("#username").val();
            var location = $("#location").val();
            var city = $("#city").val();
            var state = $("#state").val();
            var phone = $("#phone").val();
            var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
            var numberformat = /^(\d+(?:[\.\,]\d{1,2})?)$/;
            var userformat = /^[a-zA-Z0-9\_]+$/;
            var email = $("#email").val();
            var day = $("#day").val();
            var month = $("#month").val();
            var year = $("#year").val();
            var username_stat = $("#username_status").val();

            /*if (image != ""){
                $("#alert-personal").text("");
                $("#alert-personal").hide();
            } else {
                $("#alert-personal").text("insert your profile image");
                $("#alert-personal").show();
                return false;
            }*/ // end validation image
            //validation first and last name
            if(fname != "" && lname != ""){
                if(fname.length <= 3 && lname.length <= 3){
                    $("#fname").removeClass('input-error');
                    $("#lname").removeClass('input-error');
                    $("#fname").focus();
                    $("#alert-personal").text("first name and last name has to be more than 3 letter");
                    $("#alert-personal").show();
                    return false;
                } else if (fname.length <= 3 && lname.length > 3){
                    $("#fname").addClass('input-error');
                    $("#lname").removeClass('input-error');
                    $("#fname").focus();
                    $("#alert-personal").text("first name has to be more than 3 letter");
                    $("#alert-personal").show();
                    return false;
                } else if (fname.length > 3 && lname.length <= 3){
                    $("#fname").removeClass('input-error');
                    $("#lname").addClass('input-error');
                    $("#lname").focus();
                    $("#alert-personal").text("last name has to be more than 3 letter");
                    $("#alert-personal").show();
                    return false;
                } else if (fname.length > 3 && lname.length > 3){
                    $("#fname").removeClass('input-error');
                    $("#lname").removeClass('input-error');
                    $("#alert-personal").text("");
                    $("#alert-personal").hide();
                }
            } else if(fname == "" && lname != "") {
                if (lname.length <= 3){
                    $("#fname").addClass('input-error');
                    $("#lname").addClass('input-error');
                    $("#fname").focus();
                    $("#alert-personal").text("first name cannot be empty and last name has to be more than 3 letter");
                    $("#alert-personal").show();
                    return false;
                } else if (lname.length > 3){
                    $("#fname").addClass('input-error');
                    $("#lname").removeClass('input-error');
                    $("#fname").focus();
                    $("#alert-personal").text("first name cannot be empty");
                    $("#alert-personal").show();
                    return false;
                }
            } else if(fname != "" && lname == "") {
                if (fname.length <= 3){
                    $("#fname").addClass('input-error');
                    $("#lname").addClass('input-error');
                    $("#fname").focus();
                    $("#alert-personal").text("first name has to be more than 3 letter and last name cannot be empty");
                    $("#alert-personal").show();
                    return false;
                } else if (fname.length > 3){
                    $("#fname").removeClass('input-error');
                    $("#lname").addClass('input-error');
                    $("#lname").focus();
                    $("#alert-personal").text("last name cannot be empty");
                    $("#alert-personal").show();
                    return false;
                }
            } else if(fname == "" && lname == "") {
                $("#fname").addClass('input-error');
                $("#lname").addClass('input-error');
                $("#fname").focus();
                $("#alert-personal").text("first name and last name cannot be empty");
                $("#alert-personal").show();
                return false;
            } // end validation first and last name
            //validation username
            if(username != ""){ 
                if(username.length >= 5){
                    if(username.match(userformat)){
                        $("#username").removeClass('input-error');
                        $("#username").focus();
                        $("#alert-personal").text("");
                        $("#alert-personal").hide();
                    } else {
                        $("#username").addClass('input-error');
                        $("#username").focus();
                        $("#alert-personal").text("username only contain alphabet, numeric and underscore");
                        $("#alert-personal").show();
                        return false;
                    }
                    // else {
                    //     if(username_stat == 0){
                    //         $("#username").addClass('input-error');
                    //         $("#username").focus();
                    //         $("#alert-personal").text("username already exist input another one");
                    //         $("#alert-personal").show();
                    //         return false;
                    //     } else {
                    //         $("#username").removeClass('input-error');
                    //         $("#alert-personal").text("");
                    //         $("#alert-personal").hide();
                    //     }
                    // } 
                } else if (username.length < 5){
                    $("#username").addClass('input-error');
                    $("#username").focus();
                    $("#alert-personal").text("username has to be more than 4 characters");
                    $("#alert-personal").show();
                    return false;
                }
            } else {
                $("#username").addClass('input-error');
                $("#username").focus();
                $("#alert-personal").text("Username cannot be empty");
                $("#alert-personal").show();
                return false;
            } 
            //end validation username
            // validation email
            if(email != ""){
                if(email.match(mailformat)) {
                    $("#location").removeClass('input-error');
                    $("#alert-personal").text("");
                    $("#alert-personal").hide();
                } else {  
                    $("#location").addClass('input-error');
                    $("#email").focus();
                    $("#alert-personal").text("your email is not correct");
                    $("#alert-personal").show();
                    return false;  
                }
            } else {
                $("#location").addClass('input-error');
                $("#email").focus();
                $("#alert-personal").text("insert your email address");
                $("#alert-personal").show();
                return false;
            } //end validation email
            if(day != ""){
                message = "";
                $("#day").removeClass('input-error');
                $("#alert-personal").text("");
                $("#alert-personal").hide();
            } else {
                $("#day").addClass('input-error');
                $("#day").focus();
                $("#alert-personal").text("Please select your date of birth!");
                $("#alert-personal").show();
                return false;
            }
            if(month != ""){
                message = "";
                $("#month").removeClass('input-error');
                $("#alert-personal").text("");
                $("#alert-personal").hide();
            } else {
                $("#month").addClass('input-error');
                $("#month").focus();
                $("#alert-personal").text("Please select your month of birth!");
                $("#alert-personal").show();
                return false;
            }
            if(year != ""){
                $("#year").removeClass('input-error');
                $("#alert-personal").text("");
                $("#alert-personal").hide();
            } else {
                $("#year").addClass('input-error');
                $("#year").focus();
                $("#alert-personal").text("Please select your year of birth!");
                $("#alert-personal").show();
                return false;
            }
            if (location != ""){
                $("#location").removeClass('input-error');
                $("#alert-personal").text("");
                $("#alert-personal").hide();
            } else {
                $("#location").addClass('input-error');
                $("#location").focus();
                $("#alert-personal").text("choose your country location");
                $("#alert-personal").show();
                return false;
            } // end validation location
            if (city != ""){
                $("#city").removeClass('input-error');
                $("#alert-personal").text("");
                $("#alert-personal").hide();
            } else {
                $("#city").addClass('input-error');
                $("#city").focus();
                $("#alert-personal").text("choose your city location");
                $("#alert-personal").show();
                return false;
            } // end validation city
            if (state != ""){
                $("#state").removeClass('input-error');
                $("#alert-personal").text("");
                $("#alert-personal").hide();
            } else {
                $("#state").addClass('input-error');
                $("#state").focus();
                $("#alert-personal").text("choose your state location");
                $("#alert-personal").show();
                return false;
            } // end validation state
            // if(phone != ""){
            //     if(phone.match(numberformat)) {
            //       $("#phone").removeClass('input-error');
            //       $("#alert-personal").text("");
            //       $("#alert-personal").hide();
            //     } else {
            //       $("#phone").addClass('input-error');
            //       $("#phone").focus();
            //       $("#alert-personal").text("Phone has to be in number!");
            //       $("#alert-personal").show();
            //       return false;
            //     }
            // } else {
            //     $("#phone").addClass('input-error');
            //     $("#phone").focus();
            //     $("#alert-personal").text("Please input your phone number!");
            //     $("#alert-personal").show();
            //     return false;
            //  } 
             // end validation phone
            return true;
        }

        function add_zindex(){
            $('.ui-autocomplete').css("z-index", 1055);
        }
        $("#location").select2({ 
            //nothing
        });

        $(document).ready(function(){
        Dropzone.autoDiscover = false; // keep this line if you have multiple dropzones in the same page
        $(".uploadform").dropzone({
        acceptedFiles: "image/jpeg,image/png",
        url: "<?=$global['absolute-url'];?>upload.php",
        maxFiles: 1, // Number of files at a time
        maxFilesize: 1, //in MB
        error: function(file){
        alert('Your uploaded file is not supported!');
        },
        maxfilesexceeded: function(file)
        {
        alert('You have uploaded more than 1 Image. Only the first file will be uploaded!');
        },
        processing: function () {
        $('#load-profile').removeClass('hide');//on process show loading progress
        },
        success: function (response) {
        var x = JSON.parse(response.xhr.responseText);
        //$('.img').attr('src',x.img); // Set src for the image
        $('#load-profile').addClass('hide');// hide loading progress
        $('#img-profile').attr('src',x.thumb); // Set src for the thumbnail
        $('#profile-img').val(x.thumb); //for valid
        $('#img-eprofile').attr('src',x.thumb); // Set src for the thumbnail
        this.removeAllFiles(); // This removes all files after upload to reset dropzone for next upload
        console.log('Image -> '+x.img+', Thumb -> '+x.thumb); // Just to return the JSON to the console.
        },
        addRemoveLinks: true,
        removedfile: function(file) {
        var _ref; // Remove file on clicking the 'Remove file' button
        return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;
        }
        });
        });
    </script>
  <script src="<?php echo $global['absolute-url'];?>js/function.js"></script>
<script src="<?=$global['absolute-url-admin'];?>js/globalJS.js" type="text/javascript"></script>
</body>
</html>
