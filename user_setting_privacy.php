<?php 
include("packages/require.php");
include("controller/controller_user_profile.php");
include("controller/controller_user_setting_privacy.php");

$curpage='user_setting';
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title><?=$_SESSION['userData']['username']."'s ".$global['title-setting_privacy'];?></title>
  <meta name="keywords" content="">
  <meta name="description" content="">
  <?php include("packages/head.php");?>
  <link rel="stylesheet" href="<?php echo $global['absolute-url'];?>css/dropzone.css"/>
  <script src="<?php echo $global['absolute-url'];?>js/dropzone.js"></script>
</head>
<body>
  <div id="all">
    <!-- start top nav -->
    <?php include("section-top-nav.php");?>
    <!-- end top nav -->
    <!--<div id="dashboard-setting" class="">
      <ul class="dashboard-setting-nav">
        <li><a href="#">Dashboard</a></li>
        <li><a href="#">Profile</a></li>
        <li><a href="#" class="active">Setting</a></li>
      </ul>
    </div>-->
    <div id="user-section">
    
    <div class="container">
      <div id="profile-content">
        <div class="up5 visible-xs"></div>
        <div class="row">
          <div class="col-xs-12 pad0-xs mobile-nav-section">
            <div class="profile-header hidden-xs">Setting Privacy</div>
            <div class="row visible-xs mobile-nav-pad">
                <div class="col-xs-4 mobile-nav-link text-center border-right-ccc"><a class="active" href="<?=$path['user-edit'];?>"><span class="glyphicon glyphicon-user"></span>&nbsp;<br class="visible-xs" />&nbsp;<?=strtoupper($lang['nav-profil']);?></a></div>
                  <div class="col-xs-4 mobile-nav-link text-center border-right-ccc"><a href="<?=$path['user-portfolio'];?>"><span class="glyphicon glyphicon-folder-open"></span>&nbsp;<br class="visible-xs" />&nbsp;<?=strtoupper($lang['nav-portfolio']);?></a></div>
                  <div class="col-xs-4 mobile-nav-link text-center"><a href="<?=$path['user-setting'];?>"><span class="glyphicon glyphicon-cog"></span>&nbsp;<br class="visible-xs" />&nbsp;<?=strtoupper($lang['nav-setting']);?></a></div>
            </div>
          </div>
        </div>
        <div class="row">
            <div class="up5 visible-xs"></div>
          <!-- profile sidebar -->
          <div class="col-sm-3 col-xs-12 hidden-xs">
            <!-- start sidebar -->
            <?php include("user_part-sidebar.php");?>
            <!-- end sidebar -->
          </div><!-- profile sidebar -->
          <div class="col-sm-9 col-xs-12 pad0-xs">
              <div id="personal-portfolio-section" class="job-sbox" style="padding-bottom:10px;">
                <div id="education-header" class="user-section-header row border-bottom-eee">
                  <div class="col-xs-12">
                    <div class="profile-title"><i class="glyphicon glyphicon-cog"></i>&nbsp;&nbsp;SETTING PRIVACY</div>
                  </div>
                </div>
                <div id="setting-section">
                <form name="setting" action="<?=$path['user-setting-privacy-action'];?>" method="post">
                  <div class="privacy-list-head">
                    <div class="row">
                      <div class="col-xs-12">
                        <?php if($message != null){ ?> 
                          <div id="message1" class="alert alert-success" role="alert"><?php echo $message;?></div>
                        <?php } ?>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-xs-8 pad0">
                        <div class="label-privacy-head">Profile Set To Public</div>
                      </div>
                      <div class="col-xs-4 pad0">
                        <div class="onoffswitch">
                            <input type="checkbox" name="profile" class="onoffswitch-checkbox" id="privacy_profile" onclick="checkPrivacy();" <?php if($O_profile == "Yes"){echo "checked";}?> value="Yes">
                            <label class="onoffswitch-label" for="privacy_profile">
                                <span class="onoffswitch-inner"></span>
                                <span class="onoffswitch-switch"></span>
                            </label>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div id="privacy_profile_list">

                  <div class="privacy-list">
                    <div class="row">
                      <div class="col-xs-8">
                        <div class="label-privacy">Display Age</div>
                      </div>
                      <div class="col-xs-4">
                        <div class="onoffswitch">
                            <input type="checkbox" name="age" class="onoffswitch-checkbox" id="privacy_age" <?php if($O_profile_age == "Yes"){echo "checked";}?> value="Yes">
                            <label class="onoffswitch-label" for="privacy_age">
                                <span class="onoffswitch-inner"></span>
                                <span class="onoffswitch-switch"></span>
                            </label>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="privacy-list">
                    <div class="row">
                      <div class="col-xs-8">
                        <div class="label-privacy">Display Phone & Email</div>
                      </div>
                      <div class="col-xs-4">
                        <div class="onoffswitch">
                            <input type="checkbox" name="phone_email" class="onoffswitch-checkbox" id="privacy_phone" <?php if($O_profile_phone_email == "Yes"){echo "checked";}?> value="Yes">
                            <label class="onoffswitch-label" for="privacy_phone">
                                <span class="onoffswitch-inner"></span>
                                <span class="onoffswitch-switch"></span>
                            </label>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="privacy-list">
                    <div class="row">
                      <div class="col-xs-8">
                        <div class="label-privacy">Display Summary</div>
                      </div>
                      <div class="col-xs-4">
                        <div class="onoffswitch">
                            <input type="checkbox" name="summary" class="onoffswitch-checkbox" id="privacy_summary" <?php if($O_profile_summary == "Yes"){echo "checked";}?> value="Yes">
                            <label class="onoffswitch-label" for="privacy_summary">
                                <span class="onoffswitch-inner"></span>
                                <span class="onoffswitch-switch"></span>
                            </label>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="privacy-list">
                    <div class="row">
                      <div class="col-xs-8">
                        <div class="label-privacy">Display Education</div>
                      </div>
                      <div class="col-xs-4">
                        <div class="onoffswitch">
                            <input type="checkbox" name="edu" class="onoffswitch-checkbox" id="privacy_education" <?php if($O_profile_edu == "Yes"){echo "checked";}?> value="Yes">
                            <label class="onoffswitch-label" for="privacy_education">
                                <span class="onoffswitch-inner"></span>
                                <span class="onoffswitch-switch"></span>
                            </label>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="privacy-list">
                    <div class="row">
                      <div class="col-xs-8">
                        <div class="label-privacy">Display Work</div>
                      </div>
                      <div class="col-xs-4">
                        <div class="onoffswitch">
                            <input type="checkbox" name="work" class="onoffswitch-checkbox" id="privacy_work" <?php if($O_profile_work == "Yes"){echo "checked";}?> value="Yes">
                            <label class="onoffswitch-label" for="privacy_work">
                                <span class="onoffswitch-inner"></span>
                                <span class="onoffswitch-switch"></span>
                            </label>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="privacy-list">
                    <div class="row">
                      <div class="col-xs-8">
                        <div class="label-privacy">Display Skill</div>
                      </div>
                      <div class="col-xs-4">
                        <div class="onoffswitch">
                            <input type="checkbox" name="skill" class="onoffswitch-checkbox" id="privacy_skill" <?php if($O_profile_skill == "Yes"){echo "checked";}?> value="Yes">
                            <label class="onoffswitch-label" for="privacy_skill">
                                <span class="onoffswitch-inner"></span>
                                <span class="onoffswitch-switch"></span>
                            </label>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="privacy-list">
                    <div class="row">
                      <div class="col-xs-8">
                        <div class="label-privacy">Display Certificate</div>
                      </div>
                      <div class="col-xs-4">
                        <div class="onoffswitch">
                            <input type="checkbox" name="cert" class="onoffswitch-checkbox" id="privacy_certificate" <?php if($O_profile_cert == "Yes"){echo "checked";}?> value="Yes">
                            <label class="onoffswitch-label" for="privacy_certificate">
                                <span class="onoffswitch-inner"></span>
                                <span class="onoffswitch-switch"></span>
                            </label>
                        </div>
                      </div>
                    </div>
                  </div>
                  </div> <!-- end privacy profile list -->

                  <div class="up2"></div>
                  <div class="privacy-list-head">
                    <div class="row">
                      <div class="col-xs-8 pad0">
                        <div class="label-privacy-head">Resume Set To Public</div>
                      </div>
                      <div class="col-xs-4 pad0">
                        <div class="onoffswitch">
                            <input type="checkbox" name="resume" class="onoffswitch-checkbox" id="privacy_resume" <?php if($O_resume == "Yes"){echo "checked";}?> value="Yes">
                            <label class="onoffswitch-label" for="privacy_resume">
                                <span class="onoffswitch-inner"></span>
                                <span class="onoffswitch-switch"></span>
                            </label>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="privacy-list-head">
                    <div class="row">
                      <div class="col-xs-8 pad0">
                        <div class="label-privacy-head">Portfolio Set To Public</div>
                      </div>
                      <div class="col-xs-4 pad0">
                        <div class="onoffswitch">
                            <input type="checkbox" name="portfolio" class="onoffswitch-checkbox" id="privacy_portfolio" <?php if($O_portfolio == "Yes"){echo "checked";}?> value="Yes">
                            <label class="onoffswitch-label" for="privacy_portfolio">
                                <span class="onoffswitch-inner"></span>
                                <span class="onoffswitch-switch"></span>
                            </label>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="savebutton-privacy">
                    <button class="btn btn-info" type="submit">Save Changes</button>
                  </div>
                </form>
                </div>
              </div>
          </div>
      </div>
    </div>
            
    </div>
    <!-- end center content -->
  </div>
    <!-- start footer section -->
    <?php include("section-footer.php");?>
    <!-- end footer section -->
  </div><!--  end all div -->
  <script src="<?php echo $global['absolute-url'];?>js/function.js"></script>
  <script type="text/javascript">
  $("#message1").fadeOut(8400);

  $(document).ready(function(){
    checkPrivacy();
  })
  function checkPrivacy() {
  var s = document.getElementById('privacy_profile');
    if (s.checked) {
      $("#privacy_profile_list").removeClass('hide');
    } else {
      $("#privacy_profile_list").addClass('hide');
    }
  }
  function profileNav(){
    $('#profile-arrow').toggleClass('nav-close');
  }
  </script>
</body>
</html>
