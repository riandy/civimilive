<?php 
//HOME
$lang['home-notice'] = "You are currently viewing our new design. If you are a civimi shop user, please login <a href='http://shop.civimi.com' class='alert-link'>here</a>";
$lang['home-header'] = "REINVENT YOURSELF";
$lang['home-note'] = "Let show your true abilities to potential people (i.e. employers, clients, headhunters, etc)";
$lang['home-pop_profil'] = "Popular Profiles";
$lang['home-pop_profil_note'] = "Here are the most popular profiles of the day.";
$lang['home-reinvent'] = "Reinvent Yours Now";
$lang['home-reinvent_note'] = "It's simple and free!";
$lang['home-reinvent_btn'] = "Sign Up Now";

//REGISTER 
$lang['register-title'] = "Join us to create an online CV/ Resume and portfolio for free. Express yourself here and be creative!!";
$lang['register-desc'] = "	<li>Professional online CV/ Resume</li>
 							<li>Online portfolio for design, web development, video, and essay documents</li>
                            <li>Slick CV/ Resume & portfolio design</li>
                            <li>Simple editing process</li>
                            <li>Get noticed by potential people</li>
                            <li>Apply for jobs with top employers*</li>
                            <li>Receive relevant job alerts by email*</li>
                            <li>No payment required</li>";
$lang['register-head'] = "It is simple & free!";


//PROFILE PAGE

//SUMMARY PART
$lang['sum-title'] = "SUMMARY";
$lang['sum-desc-placeholder'] = "start typing about yourself or your target.";

//SIDE NAV PROFILE
$lang['nav-profil'] = "Profile";
$lang['nav-personal'] = "Personal";
$lang['nav-summary'] = "Summary";
$lang['nav-education'] = "Education";
$lang['nav-work'] = "Work";
$lang['nav-skill'] = "Skill";
$lang['nav-portfolio'] = "Portfolio";
$lang['nav-image'] = "Image";
$lang['nav-video'] = "Video";
$lang['nav-document'] = "Document";
$lang['nav-web'] = "Web";

//EDU PART
$lang['edu-title'] = "EDUCATION";
$lang['edu-btn'] = "Add Education";
$lang['edu-school'] = "School/University";
$lang['edu-school-placeholder'] = "start typing your school/University name";
$lang['edu-country'] = "Country";
$lang['edu-country-placeholder'] = "country of school/University";
$lang['edu-city'] = "City";
$lang['edu-city-placeholder'] = "city of school/University";
$lang['edu-attending'] = " Currently Attending";
$lang['edu-from'] = "From";
$lang['edu-to'] = "To";
$lang['edu-degree'] = "Degree";
$lang['edu-degree-placeholder'] = "your degree i.e. Bachelor of Arts, Sarjana, Master";
$lang['edu-study'] = "Field of Study";
$lang['edu-study-placeholder'] = "your field of study i.e. Computer Science, Product Design, Kehutanan";
$lang['edu-grade'] = "Grade";
$lang['edu-grade-placeholder'] = "your grade, GPA, IPK i.e. 4.0, 3.59";
$lang['edu-desc'] = "Description";
$lang['edu-desc-placeholder'] = "describe your achievement during your study, i.e. Became Lecturer Assistant, Manage to achieve 100% attendance during Summer 2014, etc";

//WORK PART
$lang['work-title'] = "WORK";
$lang['work-btn'] = "Add Work";
$lang['work-job'] = "Job Title";
$lang['work-job-placeholder'] = "your job title. i.e. Software Engineer, Financial Analyst, Account Executive, etc.";
$lang['work-company'] = "Company Name";
$lang['work-company-placeholder'] = "start typing a company name i.e. Google, Facebook, Civimi, Uniqlo ,Eannovate, etc ";
$lang['work-country'] = "Country";
$lang['work-country-placeholder'] = "Country where you work at. i.e. USA, Indonesia, Japan, South Korea";
$lang['work-city'] = "City";
$lang['work-city-placeholder'] = "City where you work at i.e. Mountain View, Jakarta, Tokyo, Seoul";
$lang['work-attending'] = " Currently Attending";
$lang['work-from'] = "From";
$lang['work-to'] = "To";
$lang['work-desc'] = "Description";
$lang['work-desc-placeholder'] = "describe your duty, achievement, responsibility, and task. i.e. Managed to reduce operating cost by 10% in 2011, Responsible for maintaining relationship with existing customers";

//SKILL PART
$lang['skill-title'] = "SKILL";
$lang['skill-btn'] = "Add Skill";
$lang['skill-name'] = "Skill Name";
$lang['skill-name-placeholder'] = "start typing a skill name i.e. Photo Editing, HTML, Adobe Photoshop";
$lang['skill-proficiency'] = "Proficiency";
$lang['skill-proficiency-placeholder'] = "Select your skill experience";
$lang['skill-desc'] = "Description";
$lang['skill-desc-placeholder'] = "Describe how you learn, obtain, and use the skill in working environment i.e. I previously coded HTML from PSD or design files.";

//LANGUAGE PART
$lang['lang-title'] = "LANGUAGE";
$lang['lang-btn'] = "Add Language";
$lang['lang-name'] = "Language";
$lang['lang-name-placeholder'] = "start typing a language i.e. Chinese, English, Japanese, Spanish";
$lang['lang-listening'] = "Listening Proficiency";
$lang['lang-listening-placeholder'] = "Select your listening proficiency";
$lang['lang-speaking'] = "Speaking Proficiency";
$lang['lang-speaking-placeholder'] = "Select your speaking proficiency";
$lang['lang-reading'] = "Reading Proficiency";
$lang['lang-reading-placeholder'] = "Select your reading proficiency";
$lang['lang-writing'] = "Writing Proficiency";
$lang['lang-writing-placeholder'] = "Select your writing proficiency";
$lang['lang-desc'] = "Description";
$lang['lang-desc-placeholder'] = "Describe how you learn, obtain, use the language i.e. Lived in Hongkong for 4 years as an expat, Comfortable in conversational Cantonese.";

//CERTIFICATION PART
$lang['cert-title'] = "CERTIFICATION";
$lang['cert-btn'] = "Add Certification";
$lang['cert-name'] = "Certification Name";
$lang['cert-name-placeholder'] = "start typing a certificate name. ex. Certified Public Accountant (CPA)";
$lang['cert-number'] = "Certification Number";
$lang['cert-number-placeholder'] = "type a certificate number ex. 103889";
$lang['cert-url'] = "Certification URL";
$lang['cert-url-placeholder'] = "type a URL website of the Certificate Issue";
$lang['cert-expire'] = " do not expire";
$lang['cert-from'] = "From";
$lang['cert-to'] = "To";
$lang['cert-desc'] = "Description";
$lang['cert-desc-placeholder'] = "describe the detail of the certification, what does it allow you to do? i.e. Allowed to manage a restaurant in the State of California.";

//END PROFILE PAGE

//FOOTER
$lang['footer-note'] = "Reinvent Yourself";
?> 