<?php 
if($_sub == 'EN'){
	
	//section civ homepage
	$lang['home-head'] = "Show Yourself.";
	$lang['home-note'] = "Let the opportunities coming your way.";
	$lang['home-tips'] = "Not sure yet? here are some tips.";

	$lang['hello-teks'] = "Hello, ";

	$lang['btn-started'] = "Get Started";
	$lang['link-profile'] = "Your Profile";
	$lang['free-text'] = "It's Free";
	$lang['more-text'] = "more...";

	$lang['home-ps_head'] = "Beautiful online profile";
	$lang['home-ps_desc'] = "We believe the more people know you, the more opportunities you'll get.
		                    With us, you can present your profile anywhere and anytime to any potential people with no hesitation. 
		                    It is simple, easy, and free.
		                    <br/><br/>
		                    In addition, your profile will display beautifully on any devices (laptop, smartphone, and tablet). 
		                    This would be the beginning of your true opportunities.";
	$lang['home-ps_link'] = "See how others done it >";

	$lang['home-cvs_head'] = "Anywhere CV / Resume";
	$lang['home-cvs_desc'] = "In this connected world. you will meet people anywhere and anytime. 
                    		Having your CV/Resume accessible will make it much easier for opportunities to come to you.
                    		<br/><br/>
                    		It means \"You're always ready for opportunities\".";
	$lang['home-cvs_link'] = "See top rated CV / Resume >";

	$lang['home-ws_head'] = "Showcase your work";
	$lang['home-ws_desc'] = "Making your work available anywhere and anytime will be key to new opportunities. 
		                    Photography, logo design, branding, video, and any image related work can be shown on your profile for free and unlimited.
		                    <br/><br/>
		                    Things will only get better for you.";
	$lang['home-ws_link'] = "See editor's choice portfolio >";

	$lang['home-ds_head'] = "Come on Writers!";
	$lang['home-ds_desc'] = "Writing portfolio has always been undermined compare to images. 
		                    We believe that your writing works have to be shown as well, so that new opportunities are coming for writers. 
		                    Essays, thesis, books, novel, research and any document related works can be shown on your profile for free and unlimited.";
	$lang['home-ds_link'] = "See editor's choice portfolio >";

	//section civ footer
	$lang['footer-head'] = "What is there to wait? Start opening yourself to new opportunities.";
	$lang['footer-note'] = "It's free and quick";

	//section civ testimoni
	$lang['testimoni-head'] = "Testimonial";
	$lang['testimoni-note'] = "What our users says after using our online CV and portfolio service";
	$lang['testimoni-footer'] = "Have testimony regarding our service?";
	$lang['testimoni-link'] = "Let us know";
	$lang['testimoni-modal_note'] = "We're happy that you find our service to be helpful and useful for your career and life.";
	$lang['testimoni-modal_head'] = "Tell us your story";
	$lang['btn-send'] = "Send";
	$lang['notlog-testimoni'] = "Login First To Send Us Testimoni";

	//section civ tips
	$lang['tips-head'] = "Career Tips & Blogs";
	$lang['tips-note'] = "Here are the tips and advice that can help you succeed in your employment or enterpreneurial career.";

	//TOP NAV
	$lang['dropdown-hello'] = "Hello";
	$lang['dropdown-edit'] = "EDIT";
	$lang['dropdown-profile'] = "Profile";
	$lang['dropdown-portfolio'] = "Portfolio";
	$lang['dropdown-setting'] = "Setting";
	$lang['dropdown-view'] = "VIEW";
	$lang['dropdown-my-resume'] = "CV";
	$lang['dropdown-my-portfolio'] = "Portfolio";
	$lang['dropdown-my-profile'] = "Profile";
	$lang['dropdown-logout'] = "Logout";
	$lang['btn-profile'] = "View My Profile";
	$lang['btn-resume'] = "View My Resume";
	$lang['btn-pdf'] = "Print CV";

} else {

	//section civ homepage
	$lang['home-head'] = "Tunjukan dirimu.";
	$lang['home-note'] = "Biarkan kesempatan datang menuju anda.";
	$lang['home-tips'] = "Masih tidak yakin? ini beberapa tips.";

	$lang['hello-teks'] = "Halo, ";

	$lang['btn-started'] = "Memulai";
	$lang['link-profile'] = "Profil Anda";
	$lang['free-text'] = "Gratis";
	$lang['more-text'] = "lainnya...";

	$lang['home-ps_head'] = "Profil Online Yang Indah";
	$lang['home-ps_desc'] = "Kami percaya semakin banyak orang mengenal anda, semakin banyak peluang yang anda akan dapatkan.
							Bersama kami, Anda bisa menyajikan profil anda di mana saja dan kapan saja untuk setiap orang potensial tanpa ragu-ragu.
							Hal ini sederhana, mudah, dan gratis.
		                    <br/><br/>
		                    Selain itu, profil anda akan tampil indah pada setiap perangkat (laptop, smartphone, dan tablet).
							Ini akan menjadi awal dari peluang sejati Anda.";
	$lang['home-ps_link'] = "Lihat bagaimana orang lain melakukannya >";

	$lang['home-cvs_head'] = "CV / Resume Di Manapun";
	$lang['home-cvs_desc'] = "Dalam dunia yang terhubung ini. Anda akan bertemu orang-orang di mana saja dan kapan saja. 
							Memiliki CV / Resume yang mudah diakses akan membuat lebih mudah bagi kesempatan untuk datang kepada Anda.
		                    <br/><br/>
		                    Ini berarti \"Anda selalu siap menghadapi peluang yang datang\".";
	$lang['home-cvs_link'] = "Lihat CV / Resume terbaik >";

	$lang['home-ws_head'] = "Tunjukan Hasil Karyamu";
	$lang['home-ws_desc'] = "Membuat pekerjaan Anda tersedia di mana saja dan kapan saja akan menjadi kunci untuk peluang baru. 
							Fotografi, desain logo, branding, video, dan gambar kerja terkait dapat ditampilkan pada profil Anda secara gratis dan tidak terbatas.
		                    <br/><br/>
		                    Berbagai hal akan menjadi lebih baik untuk anda.";
	$lang['home-ws_link'] = "Lihat portofolio pilihan editor >";

	$lang['home-ds_head'] = "Ayo Penulis!";
	$lang['home-ds_desc'] = "Menulis portofolio selalu kurang dipikirkan dibandingkan dengan gambar.
							Kami percaya bahwa karya-karya tulisan Anda harus menunjukkan juga, sehingga peluang baru datang untuk penulis.
							Esai, tesis, buku, novel, penelitian dan dokumen terkait karya dapat ditampilkan di profil Anda secara gratis dan tidak terbatas.";
	$lang['home-ds_link'] = "Lihat portofolio pilihan editor >";

	//section civ footer
	$lang['footer-head'] = "Apa yang anda tunggu? Mulai membuka diri untuk peluang baru.";
	$lang['footer-note'] = "Gratis dan Cepat";

	//section civ testimoni
	$lang['testimoni-head'] = "Testimonial";
	$lang['testimoni-note'] = "Apa yang pengguna kami katakan setelah menggunakan CV dan portofolio online kami";
	$lang['testimoni-footer'] = "Memiliki pernyataan mengenai online servis kami?";
	$lang['testimoni-link'] = "Beritahu kami";
	$lang['testimoni-modal_note'] = "Kami senang bahwa layanan kami dapat membantu dan berguna untuk karir anda dan kehidupan.";
	$lang['testimoni-modal_head'] = "Ceritakan kisah Anda";
	$lang['btn-send'] = "Kirim";
	$lang['notlog-testimoni'] = "Login Dahulu Untuk Mengirim Pesan";

	//section civ tips
	$lang['tips-head'] = "Career Tips & Blogs";
	$lang['tips-note'] = "Berikut adalah tips dan saran yang dapat membantu anda sukses dalam pekerjaan dan karir kewirausahaan.";

	//TOP NAV
	$lang['dropdown-hello'] = "Halo";
	$lang['dropdown-edit'] = "UBAH";
	$lang['dropdown-profile'] = "Profil";
	$lang['dropdown-portfolio'] = "Portofolio";
	$lang['dropdown-setting'] = "Setting";
	$lang['dropdown-view'] = "LIHAT";
	$lang['dropdown-my-resume'] = "CV";
	$lang['dropdown-my-portfolio'] = "Portofolio";
	$lang['dropdown-my-profile'] = "Profil";
	$lang['dropdown-logout'] = "Keluar";
	$lang['btn-profile'] = "Lihat Profile Saya";
	$lang['btn-resume'] = "Lihat Resume / CV Saya";
	$lang['btn-pdf'] = "Cetak CV";
}
?>