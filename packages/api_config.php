<?php 

//COMPANY
$api['company-popular'] = $global['api']."company/popular/";

//FIELD / function
$api['field-popular'] = $global['api']."field/popular/";

// API TOPNAV SEARCH
$api['level-index'] = $global['api']."level/index/"; //Level 
$api['field-index'] = $global['api']."field/index/"; //Job Function
$api['city-index'] = $global['api']."city/index/"; //City

// API FOR JOBS RESULT
$api['job-result'] = $global['api']."api_jobs.php?action=get_result&page="; //get data result

// API FOR TAG
$api['tag-autocomplete'] = $global['api']."api_tags.php?action=autocomplete&term="; //autocomplete
$api['tag-update'] = $global['api']."api_tags.php?action=update_data";
$api['tag-data'] = $global['api']."api_tags.php?action=get_data_tags";

// API FOR REGISTER
$api['check-username'] = $global['api']."api_user.php?action=check_username&username="; //autocomplete

// API JOB DETAIL
//$api['job-detail'] = $global['api']."job/detail/"; //get detail

// API FOR JOBS RECENT VIEWED
$api['job-recent'] = $global['api']."api_jobs.php?action=get_recent_viewed"; //get data recent viewed

// API JOB IN SEARCH3.PHP
$api['job-favorite'] = $global['api']."api_user_fav.php?action=get_data"; //get favorite job
$api['job-update-fav'] = $global['api']."api_user_fav.php?action=update"; //update favorite job
$api['job-detail'] = $global['api']."jobs/detail/"; //job detail
$api['job-related'] = $global['api']."api_jobs.php?action=get_related_by_tag"; //get related job
$api['job-company-related'] = $global['api']."api_jobs.php?action=get_related_by_company"; //get related company

//API USER DETAIL
$api['user-update_summary'] = $global['api']."api_user.php?action=update_summary";
$api['user-get_personal'] = $global['api']."api_user.php?action=get_personal";
$api['user-update_personal'] = $global['api']."api_user.php?action=update_personal";
$api['user-update_background'] = $global['api']."api_user.php?action=update_background";

//API FOR VIEW HITS
$api['job-hits'] = $global['api']."api_hits.php?action=insert"; //insert view hits
$api['user-hits'] = $global['api']."api_hits_user.php?action=insert"; //insert hits user

//API FOR LIKE
$api['like-data'] = $global['api']."api_like.php?action=get_total_like"; //get total like
$api['like-update'] = $global['api']."api_like.php?action=update"; //update like
$api['like-check'] = $global['api']."api_like.php?action=check_like"; //check like

//API FOR USER SETTING
$api['user-sort'] = $global['api']."api_setting.php?action=save_sort"; //save sort user setting

//API EDUCATION
$api['edu-autocomplete'] = $global['api']."api_education.php?action=autocomplete&term=";//autocomplete university
$api['edu-data'] = $global['api']."api_education.php?action=get_data";
$api['edu-update'] = $global['api']."api_education.php?action=update";
$api['edu-delete'] = $global['api']."api_education.php?action=delete";

//API WORK
$api['work-autocomplete'] = $global['api']."api_work.php?action=autocomplete&term=";//autocomplete work
$api['work-data'] = $global['api']."api_work.php?action=get_data";
$api['work-update'] = $global['api']."api_work.php?action=update";
$api['work-delete'] = $global['api']."api_work.php?action=delete";

//API ORGANIZATION
$api['org-autocomplete'] = $global['api']."api_organization.php?action=autocomplete&term=";//autocomplete work
$api['org-data'] = $global['api']."api_organization.php?action=get_data";
$api['org-update'] = $global['api']."api_organization.php?action=update";
$api['org-delete'] = $global['api']."api_organization.php?action=delete";

//API ACHIEVEMENT
$api['achieve-autocomplete'] = $global['api']."api_achievement.php?action=autocomplete";//autocomplete achievement
$api['achieve-data'] = $global['api']."api_achievement.php?action=get_data";
$api['achieve-update'] = $global['api']."api_achievement.php?action=update";
$api['achieve-delete'] = $global['api']."api_achievement.php?action=delete";

//API SKILL
$api['skill-autocomplete'] = $global['api']."api_skill.php?action=autocomplete&term=";//autocomplete skill
$api['skill-data'] = $global['api']."api_skill.php?action=get_data";
$api['skill-update'] = $global['api']."api_skill.php?action=update";
$api['skill-delete'] = $global['api']."api_skill.php?action=delete";

//API LANGUAGE
$api['lang-autocomplete'] = $global['api']."api_lang.php?action=autocomplete&term=";//autocomplete language
$api['lang-data'] = $global['api']."api_lang.php?action=get_data";
$api['lang-update'] = $global['api']."api_lang.php?action=update";
$api['lang-delete'] = $global['api']."api_lang.php?action=delete";

//API CERTIFICATE
$api['cert-autocomplete'] = $global['api']."api_cert.php?action=autocomplete&term=";//autocomplete certificate
$api['cert-data'] = $global['api']."api_cert.php?action=get_data";
$api['cert-update'] = $global['api']."api_cert.php?action=update";
$api['cert-delete'] = $global['api']."api_cert.php?action=delete";

//API ALBUM
$api['album-data'] = $global['api']."api_images.php?action=get_album";
$api['album-insert'] = $global['api']."api_images.php?action=insert_album";
$api['album-update'] = $global['api']."api_images.php?action=update_album";
$api['album-delete'] = $global['api']."api_images.php?action=delete_album";

//API PHOTO
$api['photo-data'] = $global['api']."api_images.php?action=get_photo";
$api['photo-insert'] = $global['api']."api_images.php?action=insert_photo";
$api['photo-update'] = $global['api']."api_images.php?action=update_photo";
$api['photo-primary'] = $global['api']."api_images.php?action=set_primary";
$api['photo-delete'] = $global['api']."api_images.php?action=delete_photo";
$api['photo-detail'] = $global['api']."api_images.php?action=get_data_detail";

//API VIDEO CAT
$api['videocat-data'] = $global['api']."api_videos.php?action=get_video_cat";
$api['videocat-insert'] = $global['api']."api_videos.php?action=insert_video_cat";
$api['videocat-update'] = $global['api']."api_videos.php?action=update_video_cat";
$api['videocat-delete'] = $global['api']."api_videos.php?action=delete_video_cat";

//API VIDEO
$api['video-data'] = $global['api']."api_videos.php?action=get_video";
$api['video-insert'] = $global['api']."api_videos.php?action=insert_video";
$api['video-update'] = $global['api']."api_videos.php?action=update_video";
$api['video-delete'] = $global['api']."api_videos.php?action=delete_video";

//API DOC CAT
$api['doccat-data'] = $global['api']."api_documents.php?action=get_doc_cat";
$api['doccat-insert'] = $global['api']."api_documents.php?action=insert_doc_cat";
$api['doccat-update'] = $global['api']."api_documents.php?action=update_doc_cat";
$api['doccat-delete'] = $global['api']."api_documents.php?action=delete_doc_cat";

//API DOC
$api['doc-data'] = $global['api']."api_documents.php?action=get_doc";
$api['doc-insert'] = $global['api']."api_documents.php?action=insert_doc";
$api['doc-update'] = $global['api']."api_documents.php?action=update_doc";
$api['doc-delete'] = $global['api']."api_documents.php?action=delete_doc";

//API WEB CAT
$api['webcat-data'] = $global['api']."api_web.php?action=get_web_cat";
$api['webcat-insert'] = $global['api']."api_web.php?action=insert_web_cat";
$api['webcat-update'] = $global['api']."api_web.php?action=update_web_cat";
$api['webcat-delete'] = $global['api']."api_web.php?action=delete_web_cat";

//API WEB
$api['web-data'] = $global['api']."api_web.php?action=get_web";
$api['web-insert'] = $global['api']."api_web.php?action=insert_web";
$api['web-update'] = $global['api']."api_web.php?action=update_web";
$api['web-delete'] = $global['api']."api_web.php?action=delete_web";
?>