<?php 

//check session
if(!isset($_SESSION['userData']['id'])){
	// header("Location:".$global['absolute-url']."?message=please-login-first");
}

//logging out
if(isset($_GET['action'])){
	if($_GET['action']== "logout" && is_array($_SESSION['userData'])){
		//set end data session
		end_session();
		//set session data to an empty array
		$_SESSION = array();
		//expire their cookie files
		if(isset($_COOKIE['cookie_id'])){
			end_cookie();
		}
		//destroy the session variables
		session_destroy();
		//double check to see if thier sessions exists
		if(isset($_SESSION['userData']['id'])){
			header("Location:http://www.civimi.com/?msg=Error:_Logout_Failed");
		}else{
			header("Location:{$global['base']}");
			exit();
		}
	}
}

if(isset($_COOKIE['cookie_id']) && !isset($_SESSION['userData']['id'])){ //if login check me out
	//set cookie in session
	$_SESSION['userData']['id'] = $_COOKIE['cookie_id'];
   	$_SESSION['userData']['username'] = $_COOKIE['cookie_username'];
    $_SESSION['userData']['auth_code'] = $_COOKIE['cookie_auth_code'];
    $_SESSION['userData']['email'] = $_COOKIE['cookie_email'];
}

?>