<meta name="robots" content="<?=$seo['robot_yes'];?>">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0 maximum-scale=1.0, user-scalable=no">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<link rel="shortcut icon" href="<?php echo $global['favicon'];?>"/>
<link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,700,700italic,400italic,300,300italic' rel='stylesheet' type='text/css'>
<!-- BOOTSTRAP CSS-->
<link href="<?php echo $global['head-url'];?>css/bootstrap.css" rel="stylesheet"/>
<link rel="stylesheet" href="<?php echo $global['head-url'];?>stylesheets/civ-style.css"/>
<link rel="stylesheet" href="<?php echo $global['head-url'];?>stylesheets/libcvm.css"/>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<script src="<?php echo $global['head-url'];?>js/bootstrap.min.js"></script>