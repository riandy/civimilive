<?php 
$global['base'] = "http://www.civimi.com/";
$global['absolute-url'] = $global['base'];
$global['head-url'] = $global['base'];
$global['api'] = $global['absolute-url']."api/";
$global['absolute-url-captcha'] = $global['absolute-url']."packages/captcha/captcha.php";
$global['absolute-base-admin'] = "http://www.civimi.com/admin/";
$global['img-url'] = $global['base'];
$global['absolute-url-admin'] = $global['base']."admin/";
$global['path-head'] = "packages/head.php";
$global['path-config'] = "packages/front_config.php";
$global['favicon'] = $global['head-url']."img/icon/favicon.ico";
$global['logo-mobile'] = $global['head-url']."img/logo/logo-mobile.png";
$global['logo-desktop'] = $global['head-url']."img/logo/logo-desktop.png";
$global['logo-white'] = $global['head-url']."img/logo/logo-white.png";
$seo['company-name'] = "Civimi";
$global['copyright'] = "&copy;".date('Y')." PD. ".$seo['company-name'].". All Rights reserved.";
$global['engineered'] = "engineered by <a href='http://www.eannovate.com/' target='_blank'>Eannovate</a>";
$seo['robot_yes'] = "index, follow";
$seo['robot_no'] = "noindex, nofollow";

$path['home'] = $global['base'];
$path['login'] = $global['base']."login.html";
$path['register'] = $global['base']."register.html";
$path['tips'] = $global['base']."tips/";
$path['tips-detail'] = $global['base']."tips/";
$path['testimoni'] = $global['base']."testimonial.html";
$path['user-cv'] = $global['base']."profile/";

//----------------------------START USER PAGE LINKS----------------------------//

//user resume PDF
// $path['user-resume-pdf'] = "http://resume.civimi.com/";
$path['user-resume-pdf'] = $global['base']."resume/";


$path['user-profile'] = $global['base']."profile/";

$path['user-edit'] = $global['base']."edit/";
$path['user-setting'] = $global['base']."edit/setting/privacy/";
$path['user-setting-privacy'] = $global['base']."edit/setting/privacy/";
$path['user-setting-privacy-action'] = $global['base']."edit/setting/privacy/?action=set";
$path['user-setting-order'] = $global['base']."edit/setting/order/";
$path['user-setting-order-action'] = $global['base']."edit/setting/order/?action=set";
$path['user-jobs'] = $global['base']."";
//user profile portfolio
$path['user-portfolio'] = $path['user-edit']."portfolio/image/";
$path['user-portfolio-folder'] = $path['user-edit']."portfolio/image/";
$path['user-portfolio-image-detail'] = $path['user-edit']."portfolio/image/detail/";
$path['user-portfolio-detail'] = $path['user-edit']."portfolio/image/folder";
//user profile video
$path['user-video'] = $path['user-edit']."portfolio/video/";
$path['user-video-folder'] = $path['user-edit']."portfolio/video/";
$path['user-video-detail'] = $path['user-edit']."portfolio/video/folder/";
//user profile document
$path['user-doc'] = $path['user-edit']."portfolio/doc/";
$path['user-doc-folder'] = $path['user-edit']."portfolio/doc/";
$path['user-doc-detail'] = $path['user-edit']."portfolio/doc/folder/";
//user profile web
$path['user-web'] = $path['user-edit']."portfolio/web/";
$path['user-web-folder'] = $path['user-edit']."portfolio/web/";
$path['user-web-detail'] = $path['user-edit']."portfolio/web/folder/";
//user action
$path['user-login'] = $path['login']."?action=login";
$path['user-logout'] = $global['base']."?action=logout";
$path['user-login-failed'] = $global['base']."ERROR:_login-failed/";
$path['success-register'] = $global['base']."success-register.html";
//----------------------------END USER PAGE LINKS----------------------------//


//----------------START RESUME/PORTFOLIO PAGE LINKS----------------//
$path['user-resume'] = $global['base']."resume/";
$path['user-portfolio_image'] = $global['base']."portfolio/image/";
$path['user-portfolio_video'] = $global['base']."portfolio/video/";
$path['user-portfolio_doc'] = $global['base']."portfolio/doc/";
$path['user-portfolio_web'] = $global['base']."portfolio/web/";
//-----------------END RESUME/PORTFOLIO PAGE LINKS-----------------//

//----------------START PROFILE PAGE LINKS----------------//
$path['user-page_profile'] = $global['base']."profile/";
//-----------------END PROFILE PAGE LINKS-----------------//

//------------------------START INDEX PAGE LINKS------------------------//
$path['home'] = $global['absolute-url'];
//blog
$path['blog'] = $global['base']."blog/";
//job
$path['job-search'] = $global['absolute-url']."search3.php";
$path['job-detail'] = $global['base']."job/";
//register
$path['register'] = $global['base']."register.html";
$path['register-action'] = $global['base']."register/";
$path['success-register'] = $global['base']."success-register.html";
//form after register
$path['form-personal'] = $global['base']."form-personal.html";
$path['form-personal-action'] = $global['base']."form-personal/?action=update";
$path['form-goal'] = $global['base']."form-goal.html";
$path['form-goal-action'] = $global['base']."form-goal/?action=update";
$path['form-industry'] = $global['base']."form-industry.html";
$path['form-industry-action'] = $global['base']."form-industry/?action=add";
//unsubscribe
$path['unsubscribe'] = $global['base']."unsubscribe.html";
$path['unsubscribe-action'] = $global['base']."unsubscribe/?action=update";
//forgot password
$path['forgot-password'] = $global['base']."forgot-password.html";
$path['forgot-password-action'] = $global['base']."forgot-password/";
//reset password
$path['reset-password'] = $global['base']."reset-password.html";
$path['reset-password-action'] = $global['base']."reset-password/";
//third party
$path['login-with-facebook'] = $global['base']."login-with-facebook.html";
$path['login-with-twitter'] = $global['base']."login-with-twitter.html";
$path['login-with-google'] = $global['base']."login-with-google.html";
$path['login-with-linkedin'] = $global['base']."login-with-linkedin.html";
//blog
$path['404'] = $global['base']."404.html";
//portfolio card
$path['portfolio-card'] = $global['base']."portfolio/card/";
//discover
$path['discover'] = $global['base']."discover/";
//-------------------------END INDEX PAGE LINKS-------------------------//

//seo home
$seo['title-home'] = "Create Free Online CV, Professional Resume, and Portfolio in ".$seo['company-name'].". Create yours now!";
$seo['keyword-home'] = "Online CV Builder and Professional Resume and Beautiful Portfolio";
$seo['desc-home'] = "Civimi.com is a CV/ resume and portfolio hosting. We encourage people to create online resumes and portfolio. Visit us today!";

//seo login
$seo['title-login'] = $seo['company-name']." | Login";
$seo['keyword-login'] = "";
$seo['desc-login'] = "";

//seo register
$seo['title-register'] = $seo['company-name']." | Sign up";
$seo['keyword-register'] = "";
$seo['desc-register'] = "";

//seo tips
$seo['title-tips'] = "Tips and Advice for Job Interviews, Resume/ CV, and Career Development by ".$seo['company-name'];
$seo['keyword-tips'] = "career advice, career development, cover letter, resume templates, interview tips, interview questions";
$seo['desc-tips'] = "Career advice including: interview tips, resume examples, how to write a cover letter, networking and more. Civimi helps you get your next job and opportunities.";

//seo testimoni
$seo['title-testimoni'] = "Real Testimonies from our users in ".$seo['company-name'];
$seo['keyword-testimoni'] = "career advice, career development, cover letter, resume templates, interview tips, interview questions";
$seo['desc-testimoni'] = "";

//seo success register
$seo['title-success_register'] = $seo['company-name']." | Success Register";
$seo['keyword-success_register'] = "Success Register Civimi";
$seo['desc-success_register'] = "Success Register Civimi";
?>