<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0 maximum-scale=1.0, user-scalable=no">
<meta http-equiv="X-UA-Compatible" content="IE=edge">

<link rel="shortcut icon" href="<?php echo $global['favicon'];?>"/>
<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,700italic,400,700,300' rel='stylesheet' type='text/css'>
<!-- BOOTSTRAP CSS-->
<link href="<?php echo $global['absolute-url'];?>css/bootstrap.css" rel="stylesheet"/>
<link rel="stylesheet" href="<?php echo $global['absolute-url'];?>stylesheets/global-style.css"/>
<link rel="stylesheet" href="<?php echo $global['absolute-url'];?>stylesheets/libcvm.css"/>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="<?php echo $global['absolute-url'];?>js/bootstrap.min.js"></script>
<!-- for select2 js -->
<link rel="stylesheet" type="text/css" href="<?=$global['absolute-url'];?>css/select2.css" />
<script type="text/javascript" src="<?=$global['absolute-url'];?>js/select2.full.js"></script>
<script type="text/javascript" src="<?=$global['absolute-url'];?>js/select2.js"></script>