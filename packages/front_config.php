<?php 
$global['base'] = "http://www.civimi.com/";
$global['api'] = $global['base']."api/";
$global['client-page'] = $global['base'];
$global['absolute-url'] = $global['client-page'];
$global['absolute-url-captcha'] = $global['absolute-url']."packages/captcha/captcha.php";
$global['resume-page'] = $global['client-page']."resume/";
$global['img-url'] = "http://www.civimi.com/";
$global['absolute-url-admin'] = "http://www.civimi.com/admin/";
$global['path-head'] = "packages/head.php";
$global['path-config'] = "packages/front_config.php";
$global['favicon'] = $global['absolute-url']."img/icon/favicon.ico";
$global['logo-mobile'] = $global['absolute-url']."img/logo/logo-mobile.png";
$global['logo-desktop'] = $global['absolute-url']."img/logo/logo-desktop.png";
$global['copyright'] = "Copyright © ".date('Y')." Civimi. All rights reserved. Terms and Conditions";
$seo['robot_yes'] = "index, follow";
$seo['robot_no'] = "noindex, nofollow";


//----------------------------------------START WEBSITE LINKS----------------------------------------//
//header
$global['title-resume'] = "Resume - create professional online resume, portfolio, and CV (Curriculum Vitae) for (FREE)";


//JOB
$global['title-job'] = "Civimi Job | Cari Kerja";
$seo['keyword-job'] = "";
$seo['desc-job'] = "";

$global['title-job-result'] = "Civimi Job | Result";
$seo['keyword-job-result'] = "";
$seo['desc-job-result'] = "";

$global['title-job-detail'] = "Civimi Job Detail";
$seo['keyword-job-detail'] = "";
$seo['desc-job-detail'] = "";

//BLOG
$global['title-blog'] = "Civimi Blog | Info dan Tips membuat Resume, Curriculum Vitae CV dan Portfolio yang menarik";
$global['desc-blog'] = "Informasi dan Tips membuat Resume, Curriculum Vitae CV, dan Portfolio yang tepat dan menarik untuk mendapatkan pekerjaan.";
$global['title-blog_detail'] = $global['title-blog'];

//REGISTER
$global['title-register'] = "Register Page - Start using CIVIMI | Reinvent Yourself with Professional Profile, CV, and Portfolio ";
$seo['keyword-register'] = "free service to promote yourself, register on civimi, Professional Profile , CV, Resume, and Portfolio ";
$seo['desc-register'] = "Registration page of CIVIMI - Free Service to create Professional Profile , CV, Resume, and Portfolio, get discovered by people, headhunters, potential employer, business partners online ";

$global['title-form_personal'] = "Fill your Personal Information to get Started | CIVIMI | Reinvent Yourself with Professional Profile, CV, and Portfolio ";
$global['title-form_goal'] = "Choose your current Goal in career | CIVIMI | Reinvent Yourself with Professional Profile, CV, and Portfolio ";
$global['title-form_industry'] = "Select all Industries that you are interested in | CIVIMI | Reinvent Yourself with Professional Profile, CV, and Portfolio ";

//UNSUBSCRIBE
$global['title-unsubscribe'] = "Unsubscribe Newsletter | CIVIMI | Reinvent Yourself with Professional Profile, CV, and Portfolio ";

//MODULE PROFILE
$global['title-profile'] = " Profile | CIVIMI | Reinvent Yourself with Professional Profile, CV, and Portfolio ";
$global['title-portfolio_image'] = " Images Portfolio | CIVIMI | Reinvent Yourself with Professional Profile, CV, and Portfolio ";
$global['title-portfolio_video'] = " Videos Portfolio | CIVIMI | Reinvent Yourself with Professional Profile, CV, and Portfolio ";
$global['title-portfolio_doc'] = " Publications Portfolio | CIVIMI | Reinvent Yourself with Professional Profile, CV, and Portfolio ";
$global['title-portfolio_web'] = " Websites Portfolio | CIVIMI | Reinvent Yourself with Professional Profile, CV, and Portfolio ";
$global['title-setting_privacy'] = " Privacy Setting | CIVIMI | Reinvent Yourself with Professional Profile, CV, and Portfolio ";

$global['title-forgot_password'] = "Recover your Password | CIVIMI | Reinvent Yourself with Professional Profile, CV, and Portfolio ";

//footer
$footer['path-sketchbook'] = "http://sketchbook.civimi.com";
//helper
$helper['doc-upload'] = "<small class='text-primary'>For Best Reading Experience, please upload .pdf filetype</small>";

$global['title-faq'] = " Help & FAQs | CIVIMI | Reinvent Yourself with Professional Profile, CV, and Portfolio ";

//404
$global['title-404'] = "404 - Page not found";
//-----------------------------------------END WEBSITE LINKS-----------------------------------------//


//----------------------------START ICON LINKS----------------------------//
$global['facebook-footer'] = $global['client-page']."img/logo/facebook.png";
$global['twitter-footer'] = $global['client-page']."img/logo/twitter.png";
$global['google-footer'] = $global['client-page']."img/logo/google.png";
//-----------------------------END ICON LINKS-----------------------------//


//------------------------START INDEX PAGE LINKS------------------------//
$path['home'] = $global['absolute-url'];
//blog
$path['blog'] = $global['base']."blog/";
//job
$path['job-search'] = $global['absolute-url']."search3.php";
$path['job-detail'] = $global['base']."job/";
//register
$path['register'] = $global['base']."register.html";
$path['register-action'] = $global['base']."register/";
$path['success-register'] = $global['base']."success-register.html";
//form after register
$path['form-personal'] = $global['base']."form-personal.html";
$path['form-personal-action'] = $global['base']."form-personal/?action=update";
$path['form-goal'] = $global['base']."form-goal.html";
$path['form-goal-action'] = $global['base']."form-goal/?action=update";
$path['form-industry'] = $global['base']."form-industry.html";
$path['form-industry-action'] = $global['base']."form-industry/?action=add";
//unsubscribe
$path['unsubscribe'] = $global['base']."unsubscribe.html";
$path['unsubscribe-action'] = $global['base']."unsubscribe/?action=update";
//forgot password
$path['forgot-password'] = $global['base']."forgot-password.html";
$path['forgot-password-action'] = $global['base']."forgot-password/";
//reset password
$path['reset-password'] = $global['base']."reset-password.html";
$path['reset-password-action'] = $global['base']."reset-password/";
//third party
$path['login-with-facebook'] = $global['base']."login-with-facebook.html";
$path['login-with-twitter'] = $global['base']."login-with-twitter.html";
$path['login-with-google'] = $global['base']."login-with-google.html";
$path['login-with-linkedin'] = $global['base']."login-with-linkedin.html";
//blog
$path['404'] = $global['base']."404.html";
//portfolio card
$path['portfolio-card'] = $global['base']."portfolio/card/";
//discover
$path['discover'] = $global['base']."discover/";
//-------------------------END INDEX PAGE LINKS-------------------------//


//----------------------------START USER PAGE LINKS----------------------------//

//user resume PDF
// $path['user-resume-pdf'] = "http://resume.civimi.com/";
$path['user-resume-pdf'] = $global['base']."resume/";


$path['user-profile'] = $global['base']."profile/";

$path['user-edit'] = $global['base']."edit/";
$path['user-setting'] = $global['base']."edit/setting/privacy/";
$path['user-setting-privacy'] = $global['base']."edit/setting/privacy/";
$path['user-setting-privacy-action'] = $global['base']."edit/setting/privacy/?action=set";
$path['user-setting-order'] = $global['base']."edit/setting/order/";
$path['user-setting-order-action'] = $global['base']."edit/setting/order/?action=set";
$path['user-jobs'] = $global['base']."";
//user profile portfolio
$path['user-portfolio'] = $path['user-edit']."portfolio/image/";
$path['user-portfolio-folder'] = $path['user-edit']."portfolio/image/";
$path['user-portfolio-image-detail'] = $path['user-edit']."portfolio/image/detail/";
$path['user-portfolio-detail'] = $path['user-edit']."portfolio/image/folder";
//user profile video
$path['user-video'] = $path['user-edit']."portfolio/video/";
$path['user-video-folder'] = $path['user-edit']."portfolio/video/";
$path['user-video-detail'] = $path['user-edit']."portfolio/video/folder/";
//user profile document
$path['user-doc'] = $path['user-edit']."portfolio/doc/";
$path['user-doc-folder'] = $path['user-edit']."portfolio/doc/";
$path['user-doc-detail'] = $path['user-edit']."portfolio/doc/folder/";
//user profile web
$path['user-web'] = $path['user-edit']."portfolio/web/";
$path['user-web-folder'] = $path['user-edit']."portfolio/web/";
$path['user-web-detail'] = $path['user-edit']."portfolio/web/folder/";
//user action
$path['user-login'] = $global['base']."?action=login";
$path['user-logout'] = $global['base']."?action=logout";
$path['user-login-failed'] = $global['base']."ERROR:_login-failed/";
//----------------------------END USER PAGE LINKS----------------------------//


//----------------START RESUME/PORTFOLIO PAGE LINKS----------------//
$path['user-resume'] = $global['base']."resume/";
$path['user-portfolio_image'] = $global['base']."portfolio/image/";
$path['user-portfolio_video'] = $global['base']."portfolio/video/";
$path['user-portfolio_doc'] = $global['base']."portfolio/doc/";
$path['user-portfolio_web'] = $global['base']."portfolio/web/";
//-----------------END RESUME/PORTFOLIO PAGE LINKS-----------------//

//----------------START PROFILE PAGE LINKS----------------//
$path['user-page_profile'] = $global['base']."profile/";
//-----------------END PROFILE PAGE LINKS-----------------//

//--------------START SOCIAL MEDIA LINKS-------------//
$path['facebook'] = "https://www.facebook.com/civimi";
$path['twitter'] = "https://twitter.com/civimi";
$path['google'] = "https://plus.google.com/+Civimi";
//---------------END SOCIAL MEDIA LINKS--------------//


//-------------------START PLACEHOLDER IMAGE MODULE PROFILE----------------//
$placeholder['img-education'] = $global['absolute-url']."img/education.png";
$placeholder['img-work'] = $global['absolute-url']."img/work.png";
$placeholder['img-org'] = $global['absolute-url']."img/work.png";
$placeholder['img-language'] = $global['absolute-url']."img/work.png";
$placeholder['img-skill'] = $global['absolute-url']."img/work.png";
$placeholder['img-certificate'] = $global['absolute-url']."img/certificate.png";
$placeholder['img-achievement'] = $global['absolute-url']."img/education.png";
//-------------------END LACEHOLDER IMAGE MODULE PROFILE-------------------//

//----------------------------------------START LANGUAGE----------------------------------------//
if(!isset($_sub)){
	$_sub = "ID";
}
if($_sub == 'EN'){

	$lang['btn-profile'] = "View My Profile";
	$lang['btn-resume'] = "View My Resume";
	$lang['btn-pdf'] = "Print CV";

	$lang['personal-empty'] = "YOUR PROFESSION / TITLE HERE";
	//BLOG
	$lang['footer-profile'] = "Site that provides CV guidance and match you with Companies, Partners, and Freelance Job.";

	//HOME
	$global['title-home'] = "CIVIMI | Reinvent Yourself with Professional Profile, CV, and Portfolio ";
	$seo['keyword-home'] = "free service to promote yourself, Online Curriculum Vitae Portfolio Profile, Professional Profile , CV, Resume, and Portfolio ";
	$seo['desc-home'] = "CIVIMI is a free Service to create Professional Profile , CV, Resume, and Portfolio help user discover and get discovered by people, headhunters, potential employer, business partners online ";
	
	//TOP NAV
	$lang['dropdown-hello'] = "Hello";
	$lang['dropdown-edit'] = "EDIT";
	$lang['dropdown-profile'] = "Profile";
	$lang['dropdown-portfolio'] = "Portfolio";
	$lang['dropdown-setting'] = "Setting";
	$lang['dropdown-view'] = "VIEW";
	$lang['dropdown-my-resume'] = "CV";
	$lang['dropdown-my-portfolio'] = "Portfolio";
	$lang['dropdown-my-profile'] = "Profile";
	$lang['dropdown-logout'] = "Logout";
	
	//FORM PERSONAL
	$lang['fpersonal-welcome'] = "Welcome to Civimi";
	$lang['fpersonal-upload'] = "Upload your photo by clicking the image";
	$lang['fpersonal-info'] = "To continue please fill in the information";
	$lang['fpersonal-first'] = "First Name";
	$lang['fpersonal-last'] = "Last Name";
	$lang['fpersonal-email'] = "Email";
	$lang['fpersonal-username'] = "Username";
	$lang['fpersonal-dob'] = "Date of Birth";
	$lang['fpersonal-location'] = "Location";
	$lang['fpersonal-city'] = "City";
	$lang['fpersonal-city_placeholder'] = "City where you reside, i.e. Los Angeles, Depok, Amsterdam";
	$lang['fpersonal-state'] = "State / Province";
	$lang['fpersonal-state_placeholder'] = "State where you reside, i.e. California, West Java, North Holland";
	$lang['fpersonal-phone'] = "Phone";
	$lang['fpersonal-phone_placeholder'] = "phone number to get in touch";
	$lang['fpersonal-next'] = "NEXT";

	//FORM GOAL
	$lang['fgoal-welcome'] = "What is your current goal ?";
	$lang['fgoal-info'] = "Help us identity and give the best experience for you!";
	$lang['fgoal-opportunity'] = "Open for any opportunity";
	$lang['fgoal-freelance'] = "Available for freelance work";
	$lang['fgoal-fulltime'] = "Looking for fulltime job";
	$lang['fgoal-internship'] = "Looking for an internship";
	$lang['fgoal-company'] = "Starting up a company";
	$lang['fgoal-education'] = "Pursue higher education";
	$lang['fgoal-college'] = "Finishing college";
	$lang['fgoal-next'] = "NEXT";

	//FORM INDUSTRY
	$lang['findustry-welcome'] = "Just one more step !";
	$lang['findustry-info'] = "Select the industry you would like to work in, <br/>you can select more than one.";
	$lang['findustry-continue'] = "Continue to my profile";

	//UNSUBSCRIBE
	$lang['unsub-head'] = "Unsubscribe Request";
	$lang['unsub-info'] = "Please confirm your email to unsubscribe";
	$lang['unsub-email'] = "E-mail";
	$lang['unsub-submit'] = "Submit";

	//FEEDBACK
	$lang['feedback-link'] = "Give us Feedback";
	$lang['feedback-head'] = "Give Feedback";
	$lang['feedback-name'] = "your name...";
	$lang['feedback-email'] = "your email...";
	$lang['feedback-message'] = "your feedback message";
	$lang['feedback-captcha'] = "insert captcha";
	$lang['feedback-send'] = "Send Feedback";

	//HOME
	$lang['home-notice'] = "You are currently viewing our new design. If you are a <u>civimi shop user</u>, please login <a href='http://shop.civimi.com' class='alert-link'>here</a>";
	$lang['home-header'] = "REINVENT YOURSELF";
	$lang['home-note'] = "Let show your true abilities to potential people (i.e. employers, clients, headhunters, etc)";
	$lang['home-pop_profil'] = "Popular Profiles";
	$lang['home-pop_profil_note'] = "Here are the most popular profiles of the day.";
	$lang['home-pop_univ'] = "Popular University";
	$lang['home-pop_univ_note'] = "Here are the most popular university of the day.";
	$lang['home-reinvent'] = "Reinvent Yours Now";
	$lang['home-reinvent_note'] = "It's simple and free!";
	$lang['home-reinvent_btn'] = "Sign Up Now";
	$lang['home-news'] = "Recent News";
	$lang['home-news_note'] = "Here are the top recent news of the day.";
	$lang['home-news_more'] = "Read More";

	// home welcome modal
	$lang['home-modal_title'] = "We focus on making it easier to show your abilities and experience to the world. Join and try civimi:";
	$lang['home-modal_desc'] = "FREE for all<br/>
                    Faster Login (with Facebook / Google)<br/>
                    Mobile-optimized<br/>
                    Share and Get Featured!<br/>
                    Live Chat!<br/>
                    Indonesian Language <span class='badge badge-red'>NEW</span><br/>
                    Messaging Feature in Resume <span class='badge badge-red'>NEW</span><br/>
                    Print & Save to PDF <span class='badge badge-red'>NEW</span><br/>
                    Smart Job Board Coming Soon!<br/>
                    <em>More to come</em><br/>";
    $lang['home-modal_action'] = "Let's Start!";
    $lang['home-modal_shop'] =  "For Civimi Shop User, Login <a href='http://shop.civimi.com'>Here</a>";

	//REGISTER 
	$lang['register-title'] = "Join us to create an online CV/ Resume and portfolio for free. Express yourself here and be creative!!";
	$lang['register-desc'] = "	<li>Professional online CV/ Resume</li>
	 							<li>Online portfolio for design, web development, video, and essay documents</li>
	                            <li>Slick CV/ Resume & portfolio design</li>
	                            <li>Simple editing process</li>
	                            <li>Get noticed by potential people</li>
	                            <li>Apply for jobs with top employers*</li>
	                            <li>Receive relevant job alerts by email*</li>
	                            <li>No payment required</li>";
	$lang['register-head'] = "It is simple & free!";
	$lang['register-sosmed'] = "Faster with no email verification!";

	//PROFILE PAGE

	//SIDE NAV PROFILE
	$lang['nav-profil'] = "Home";
	$lang['nav-personal'] = "Personal";
	$lang['nav-summary'] = "Summary";
	$lang['nav-education'] = "Education";
	$lang['nav-work'] = "Work";
	$lang['nav-skill'] = "Skill";
	$lang['nav-language'] = "Language";
	$lang['nav-certificate'] = "Certificate";
	$lang['nav-organization'] = "Organization";
	$lang['nav-achievement'] = "Achievement";
	$lang['nav-portfolio'] = "Portfolio";
	$lang['nav-image'] = "Image";
	$lang['nav-video'] = "Video";
	$lang['nav-document'] = "Document";
	$lang['nav-web'] = "Web";
	$lang['nav-setting'] = "Settings";

	//PERSONAL PART
	$lang['personal-title'] = "PERSONAL";
	$lang['personal-fname'] = "First Name";
	$lang['personal-fname_placeholder'] = "First Name";
	$lang['personal-lname'] = "Last Name";
	$lang['personal-lname_placeholder'] = "Last Name";
	$lang['personal-date'] = "Date of Birth";
	$lang['personal-dob_show'] = "Show Date of Birth in Profile and Resume ";
	$lang['personal-interest'] = "Interest";
	$lang['personal-interest_select'] = "Choose Interest";
	$lang['personal-job'] = "Profession/Job Title";
	$lang['personal-job_placeholder'] = "Your Profession / Job, i.e. Mahasiswa, Akuntan, Dokter Spesialis Mata,Rocket iOS Developer";
	$lang['personal-city'] = "City";
	$lang['personal-city_placeholder'] = "City where you reside, i.e. Los Angeles, Depok, Amsterdam";
	$lang['personal-state'] = "State / Province";
	$lang['personal-state_placeholder'] = "State where you reside, i.e. California, West Java, North Holland";
	$lang['personal-country'] = "Country";
	$lang['personal-country_select'] = "Choose Country";
	$lang['personal-email'] = "E-Mail";
	$lang['personal-email_placeholder'] = "email to get in touch";
	$lang['personal-phone'] = "Phone";
	$lang['personal-phone_placeholder'] = "phone number to get in touch";
	$lang['personal-phone_show'] = "Show Phone Number in Profile and Resume ";
	$lang['personal-website'] = "Website";
	$lang['personal-website_placeholder'] = "Website";

	//SUMMARY PART
	$lang['sum-title'] = "SUMMARY";
	$lang['sum-desc_placeholder'] = "start typing about yourself or your target.";
	$lang['sum-empty'] = "Write a summary of yourself, passion, goals, and what inspires you.";

	//EDU PART
	$lang['edu-title'] = "EDUCATION";
	$lang['edu-btn'] = "Add Education";
	$lang['edu-school'] = "School/University";
	$lang['edu-school_placeholder'] = "start typing your school/University name";
	$lang['edu-country'] = "Country";
	$lang['edu-country_placeholder'] = "country of school/University";
	$lang['edu-city'] = "City";
	$lang['edu-city_placeholder'] = "city of school/University";
	$lang['edu-attending'] = " Currently Attending";
	$lang['edu-from'] = "From";
	$lang['edu-to'] = "To";
	$lang['edu-degree'] = "Degree";
	$lang['edu-degree_placeholder'] = "your degree i.e. Bachelor of Arts, Sarjana, Master";
	$lang['edu-study'] = "Field of Study";
	$lang['edu-study_placeholder'] = "your field of study i.e. Computer Science, Product Design, Kehutanan";
	$lang['edu-grade'] = "Grade";
	$lang['edu-grade_placeholder'] = "your grade, GPA, IPK i.e. 4.0, 3.59";
	$lang['edu-desc'] = "Description";
	$lang['edu-desc_placeholder'] = "describe your achievement during your study, i.e. Became Lecturer Assistant, Manage to achieve 100% attendance during Summer 2014, etc";
	$lang['edu-placeholder'] = "Insert your Education history with details of your achievement.";

	//WORK PART
	$lang['work-title'] = "WORK EXPERIENCE";
	$lang['work-btn'] = "Add Work";
	$lang['work-job'] = "Job Title";
	$lang['work-job_placeholder'] = "Your Profession / Job, i.e. Mahasiswa, Akuntan, Dokter Spesialis Mata,Rocket iOS Developer";
	$lang['work-company'] = "Company Name";
	$lang['work-company_placeholder'] = "start typing a company name i.e. Google, Facebook, Civimi, Uniqlo ,Eannovate, etc ";
	$lang['work-country'] = "Country";
	$lang['work-country_placeholder'] = "Country where you work at. i.e. USA, Indonesia, Japan, South Korea";
	$lang['work-city'] = "City";
	$lang['work-city_placeholder'] = "City where you work at i.e. Mountain View, Jakarta, Tokyo, Seoul";
	$lang['work-attending'] = " Currently Attending";
	$lang['work-from'] = "From";
	$lang['work-to'] = "To";
	$lang['work-desc'] = "Description";
	$lang['work-desc_placeholder'] = "describe your duty, achievement, responsibility, and task. i.e. Managed to reduce operating cost by 10% in 2011, Responsible for maintaining relationship with existing customers";
	$lang['work-placeholder'] = "Have any working and voluntary experience?<br/>Input your experience here.";

	//ORGANIZATION PART
	$lang['org-title'] = "ORGANIZATION";
	$lang['org-btn'] = "Add Organization";
	$lang['org-job'] = "Position in Organization";
	$lang['org-job_placeholder'] = "your position title. i.e. Member, Founder, Co-Founder, Team Leader, etc.";
	$lang['org-organization'] = "Organization Name";
	$lang['org-organization_placeholder'] = "start typing a organization name i.e. Indonesia Mengajar, UNESCO, International Development Association, WHO (World Health Organization), UNICEF (United Nations International Childrens Emergency Fund), etc ";
	$lang['org-country'] = "Country";
	$lang['org-country_placeholder'] = "Country where your organization at. i.e. USA, Indonesia, Japan, South Korea";
	$lang['org-city'] = "City";
	$lang['org-city_placeholder'] = "City where your organization at i.e. Mountain View, Jakarta, Tokyo, Seoul";
	$lang['org-attending'] = " Currently Organization";
	$lang['org-from'] = "From";
	$lang['org-to'] = "To";
	$lang['org-desc'] = "Description";
	$lang['org-desc_placeholder'] = "describe about your organization and your duty, achievement, responsibility, or task in your organization";
	$lang['org-placeholder'] = "Have join any organization?<br/>Input your organization here.";

	//SKILL PART
	$lang['skill-title'] = "SKILL";
	$lang['skill-btn'] = "Add Skill";
	$lang['skill-name'] = "Skill Name";
	$lang['skill-name_placeholder'] = "start typing a skill name i.e. Photo Editing, HTML, Adobe Photoshop";
	$lang['skill-proficiency'] = "Proficiency";
	$lang['skill-proficiency_select'] = "Select your skill experience";
	$lang['skill-proficiency_placeholder'] = "Choose how good your skill is";
	$lang['skill-desc'] = "Description";
	$lang['skill-desc_placeholder'] = "Describe how you learn, obtain, and use the skill in working environment i.e. I previously coded HTML from PSD or design files.";
	$lang['skill-placeholder'] = "Everyone is gifted!<br/>Input your skill with proficiency and describe how you manage to learn the skill.";

	//LANGUAGE PART
	$lang['lang-title'] = "LANGUAGE";
	$lang['lang-btn'] = "Add Language";
	$lang['lang-name'] = "Language";
	$lang['lang-name_placeholder'] = "start typing a language i.e. Chinese, English, Japanese, Spanish";
	$lang['lang-listening'] = "Listening Proficiency";
	$lang['lang-listening_select'] = "Select your listening proficiency";
	$lang['lang-listening_placeholder'] = "Choose how good in your listening";
	$lang['lang-speaking'] = "Speaking Proficiency";
	$lang['lang-speaking_select'] = "Select your speaking proficiency";
	$lang['lang-speaking_placeholder'] = "Choose how good in your speaking";
	$lang['lang-reading'] = "Reading Proficiency";
	$lang['lang-reading_select'] = "Select your reading proficiency";
	$lang['lang-reading_placeholder'] = "Choose how good in your reading";
	$lang['lang-writing'] = "Writing Proficiency";
	$lang['lang-writing_select'] = "Select your writing proficiency";
	$lang['lang-writing_placeholder'] = "Choose how good in your writing";
	$lang['lang-desc'] = "Description";
	$lang['lang-desc_placeholder'] = "Describe how you learn, obtain, use the language i.e. Lived in Hongkong for 4 years as an expat, Comfortable in conversational Cantonese.";
	$lang['lang-placeholder'] = "Do you know that Foreign language provide sharp edge in the job market?<br/>Input your language skill and its proficiency here.";

	//CERTIFICATION PART
	$lang['cert-title'] = "CERTIFICATION";
	$lang['cert-btn'] = "Add Certification";
	$lang['cert-name'] = "Certification Name";
	$lang['cert-name_placeholder'] = "start typing a certificate name. ex. Certified Public Accountant (CPA)";
	$lang['cert-number'] = "Certification Number";
	$lang['cert-number_placeholder'] = "type a certificate number ex. 103889";
	$lang['cert-url'] = "Certification URL";
	$lang['cert-url_placeholder'] = "type a URL website of the Certificate Issue";
	$lang['cert-expire'] = " do not expire";
	$lang['cert-from'] = "From";
	$lang['cert-to'] = "To";
	$lang['cert-desc'] = "Description";
	$lang['cert-desc_placeholder'] = "describe the detail of the certification, what does it allow you to do? i.e. Allowed to manage a restaurant in the State of California.";
	$lang['cert-placeholder'] = "Do you have Certificate?<br/>Input your Certificate info here.";

	//ACHIEVEMENT PART
	$lang['achieve-title'] = "ACHIEVEMENT";
	$lang['achieve-btn'] = "Add Achievement";
	$lang['achieve-name'] = "Achievement Name";
	$lang['achieve-name_placeholder'] = "typing your achievement name";
	$lang['achieve-source'] = "Achievement Source";
	$lang['achieve-source_placeholder'] = "Choose One Source";
	$lang['achieve-institute'] = "Achievement From Institution";
	$lang['achieve-institute_placeholder'] = "start typing a achievement institution.";
	$lang['achieve-attending'] = " Currently Attending";
	$lang['achieve-year'] = "Year";
	$lang['achieve-desc'] = "Description";
	$lang['achieve-desc_placeholder'] = "describe how you can get achievement and where you get.";
	$lang['achieve-placeholder'] = "Insert your Achievement you have.";


	// NO RESULT LIST TEXT
	$lang['entry-result'] = "There is no result.";
	$lang['entry-result_text'] = "will be inserted as new entry.";

	//END PROFILE PAGE

	//FOOTER
	$lang['footer-note'] = "Reinvent Yourself";

	//TOUR PROFILE

	//PERSONAL TOUR
	$tour['personal-1-title'] = "Welcome on <br/>Your Profile Page";
	$tour['personal-1-content'] = "Insert your personal data in here";
	$tour['personal-2-title'] = "Personal Part";
	$tour['personal-2-content'] = "<div class='text-center'>Insert Your Personal Data<br/>Biodata<br/>Profession<br/>City and Province</div>";
	$tour['personal-3-title'] = "Profile Picture";
	$tour['personal-3-content'] = "Upload your best picture click or drag & drop your file on top";

	//SUMMARY TOUR 
	$tour['summary-1-title'] = "Summary";
	$tour['summary-1-content'] = "Click to edit your summary data";
	$tour['summary-2-title'] = "Summary Edit";
	$tour['summary-2-content'] = "<div class='text-center'>Insert Your Summary/Describe About Yourself</div>";

	//EDUCATION TOUR 
	$tour['edu-1-title'] = "Education";
	$tour['edu-1-content'] = "This section describe your education background";
	$tour['edu-2-title'] = "Add Education";
	$tour['edu-2-content'] = "<div class='text-center'>Click This button to insert education</div>";
	$tour['edu-3-title'] = "Form Add Education";
	$tour['edu-3-content'] = "<div class='text-center'>In this form you can insert your education data</div>";

	//WORK TOUR 
	$tour['work-1-title'] = "Work Experience";
	$tour['work-1-content'] = "This section describe your work experience";
	$tour['work-2-title'] = "Add Work Experience";
	$tour['work-2-content'] = "<div class='text-center'>Click This button to insert work experience</div>";
	$tour['work-3-title'] = "Form Add Work Experience";
	$tour['work-3-content'] = "<div class='text-center'>In this form you can insert your work experience data</div>";

	//SKILL TOUR 
	$tour['skill-1-title'] = "Skill";
	$tour['skill-1-content'] = "This section describe what skill do you have";
	$tour['skill-2-title'] = "Add Skill";
	$tour['skill-2-content'] = "<div class='text-center'>Click This button to insert skill</div>";
	$tour['skill-3-title'] = "Form Add Skill";
	$tour['skill-3-content'] = "<div class='text-center'>In this form you can insert your skill data</div>";

	//LANGUAGE TOUR 
	$tour['lang-1-title'] = "Language";
	$tour['lang-1-content'] = "This section describe your language experience";
	$tour['lang-2-title'] = "Add Language";
	$tour['lang-2-content'] = "<div class='text-center'>Click This button to insert language</div>";
	$tour['lang-3-title'] = "Form Add Language";
	$tour['lang-3-content'] = "<div class='text-center'>In this form you can insert your language data</div>";

	//CERTIFICATE TOUR 
	$tour['cert-1-title'] = "Certificate";
	$tour['cert-1-content'] = "This section describe what certificate you have";
	$tour['cert-2-title'] = "Add Certificate";
	$tour['cert-2-content'] = "<div class='text-center'>Click This button to insert certificate</div>";
	$tour['cert-3-title'] = "Form Add Certificate";
	$tour['cert-3-content'] = "<div class='text-center'>In this form you can insert your certificate data</div>";

	//ORGANIZATION TOUR 
	$tour['org-1-title'] = "Organization";
	$tour['org-1-content'] = "This section describe what organization you join";
	$tour['org-2-title'] = "Add Organization";
	$tour['org-2-content'] = "<div class='text-center'>Click This button to insert organization</div>";
	$tour['org-3-title'] = "Form Add Organization";
	$tour['org-3-content'] = "<div class='text-center'>In this form you can insert your organization data</div>";

	//ACHIEVEMENT TOUR 
	$tour['achieve-1-title'] = "Achievement";
	$tour['achieve-1-content'] = "This section describe what achievement you have";
	$tour['achieve-2-title'] = "Add Achievement";
	$tour['achieve-2-content'] = "<div class='text-center'>Click This button to insert achievement</div>";
	$tour['achieve-3-title'] = "Form Add Achievement";
	$tour['achieve-3-content'] = "<div class='text-center'>In this form you can insert your achievement data</div>";

	//PROFIL WELCOME TOUR 
	$tour['profil-1-title'] = "Welcome in your profile page";
	$tour['profil-1-content'] = "in this page you can insert information about your profile and CV";
	$tour['profil-2-title'] = "Profile Menu";
	$tour['profil-2-content'] = "<div class='text-center'>This is a part you can insert in your profile.<br/>Click <span class='glyphicon glyphicon-info-sign info-sign'></span> if you need help</div>";
	$tour['profil-3-title'] = "Portfolio Menu";
	$tour['profil-3-content'] = "<div class='text-center'>This part where you can upload your work.</div>";

	//LIKE CARD NOTIFICATION 
	$lang['card-welcome'] = "Congratulation";
	$lang['card-welcome_text'] = "Someone from";
	$lang['card-welcome_text2'] = "liked your work";
	$lang['card-closing'] = "Show your passion<br/>Discover work and be discovered!";
	$lang['card-notification_link'] = "too many notifications? click here";

} else {

	$lang['btn-profile'] = "Lihat Profile Saya";
	$lang['btn-resume'] = "Lihat Resume / CV Saya";
	$lang['btn-pdf'] = "Cetak CV";

	$lang['personal-empty'] = "PROFESI / TITLE ANDA DISINI";

	//BLOG
	$lang['footer-profile'] = "Situs panduan untuk membuat CV dan mempertemukan kalian dengan Perusahaan, Partners, dan Pekerjaan Freelance.";

	//HOME
	$global['title-home'] = "CIVIMI | Tunjukan dirimu dengan membuat Profil Profesional ,CV, dan Portofolio Digital";
	$seo['keyword-home'] = "layanan gratis untuk mempromosikan Keahlian dan Pengalaman, Online Curriculum Vitae Portfolio Profile, Professional Profile , CV, Resume, and Portfolio ";
	$seo['desc-home'] = "CIVIMI adalah layanan GRATIS panduan membuat CV, curriculum vitae dan Portfolio. Menolong kalian untuk ditemukan oleh orang, headhunter, calon atasan, dan calon rekan bisnis.";

	//TOP NAV
	$lang['dropdown-hello'] = "Halo";
	$lang['dropdown-edit'] = "UBAH";
	$lang['dropdown-profile'] = "Profil";
	$lang['dropdown-portfolio'] = "Portofolio";
	$lang['dropdown-setting'] = "Setting";
	$lang['dropdown-view'] = "LIHAT";
	$lang['dropdown-my-resume'] = "CV";
	$lang['dropdown-my-portfolio'] = "Portofolio";
	$lang['dropdown-my-profile'] = "Profil";
	$lang['dropdown-logout'] = "Keluar";

	//FORM PERSONAL
	$lang['fpersonal-welcome'] = "Selamat Datang di Civimi";
	$lang['fpersonal-upload'] = "Upload photo anda dengan menekan gambar";
	$lang['fpersonal-info'] = "Untuk melanjutkan silakan input informasi";
	$lang['fpersonal-first'] = "Nama Depan";
	$lang['fpersonal-last'] = "Nama Belakang";
	$lang['fpersonal-email'] = "Email";
	$lang['fpersonal-username'] = "Username";
	$lang['fpersonal-dob'] = "Tanggal Lahir";
	$lang['fpersonal-location'] = "Negara";
	$lang['fpersonal-city'] = "Kota";
	$lang['fpersonal-city_placeholder'] = "Kota domisili, i.e. Jakarta, Depok, Bandung";
	$lang['fpersonal-state'] = "Provinsi";
	$lang['fpersonal-state_placeholder'] = "Provinsi domisili, i.e. DKI Jakarta, Sumatera Selatan, Jawa Barat";
	$lang['fpersonal-phone'] = "Telepon";
	$lang['fpersonal-phone_placeholder'] = "Telepon yang dapat dihubungi";
	$lang['fpersonal-next'] = "Selanjutnya";

	//FORM GOAL
	$lang['fgoal-welcome'] = "Apa tujuan utama anda saat ini ?";
	$lang['fgoal-info'] = "Bantu kita memberikan pengalaman terbaik untuk anda!";
	$lang['fgoal-opportunity'] = "Siap untuk semua kesempatan dan peluang";
	$lang['fgoal-freelance'] = "Mencari pekerjaan freelance";
	$lang['fgoal-fulltime'] = "Mencari pekerjaan fulltime";
	$lang['fgoal-internship'] = "Mencari tempat Magang";
	$lang['fgoal-company'] = "Memulai sebuah usaha";
	$lang['fgoal-education'] = "Melanjutkan pendidikan tinggi";
	$lang['fgoal-college'] = "Menyelesaikan Kuliah";
	$lang['fgoal-next'] = "Selanjutnya";

	//FORM INDUSTRY
	$lang['findustry-welcome'] = "Tinggal satu langkah lagi!";
	$lang['findustry-info'] = "Pilih industri yang membuat anda tertarik, <br/>anda dapat memilih lebih dari satu.";
	$lang['findustry-continue'] = "Lanjutkan ke profile saya";

	//UNSUBSCRIBE
	$lang['unsub-head'] = "Unsubscribe Request";
	$lang['unsub-info'] = "Silahkan konfirmasi untuk unsubscribe";
	$lang['unsub-email'] = "E-mail";
	$lang['unsub-submit'] = "Submit";

	//FEEDBACK
	$lang['feedback-link'] = "Beri Saran dan Masukan";
	$lang['feedback-head'] = "Beri Saran dan Masukan";
	$lang['feedback-name'] = "nama anda";
	$lang['feedback-email'] = "email anda";
	$lang['feedback-message'] = "Saran dan Masukkan bagi civimi";
	$lang['feedback-captcha'] = "ketikkan karakter di atas";
	$lang['feedback-send'] = "Kirim Feedback";

	//HOME
	$lang['home-notice'] = "Saat ini anda melihat design baru. Jika anda pengguna <u>civimi shop</u>, silahkan login <a href='http://shop.civimi.com' class='alert-link'>disini</a>";
	$lang['home-header'] = "Profil Professional dan Panduan membuat CV";
	$lang['home-note'] = "Tunjukan kemampuan anda ke orang berpotensi menjadi (i.e. Calon Atasan, Klien, headhunters, dll)";
	$lang['home-pop_profil'] = "Profil Popular";
	$lang['home-pop_profil_note'] = "ini adalah Profil Popular hari ini";
	$lang['home-pop_univ'] = "Universitas Popular";
	$lang['home-pop_univ_note'] = "ini adalah Profil Universitas hari ini";
	$lang['home-reinvent'] = "Tunjukkan diri anda";
	$lang['home-reinvent_note'] = "mudah dan gratis!";
	$lang['home-reinvent_btn'] = "Daftar Sekarang";
	$lang['home-news'] = "Berita Terbaru";
	$lang['home-news_note'] = "ini adalah berita terbaru hari ini";
	$lang['home-news_more'] = "Baca Lebih Lanjut";

		// home welcome modal
	$lang['home-modal_title'] = "Kami fokus untuk memudahkan kamu untuk menunjukkan Kemampuan dan Pengalamanmu kepada Dunia. Bergabung dan coba civimi:";
	$lang['home-modal_desc'] = "GRATIS untuk semua<br/>
                    Kemudahan Register (dengan Facebook & Google)<br/>
                    Tampilan Optimal di Smartphone dan Tablet<br/>
                    Sebarkan Resume Portfolio anda!<br/>
                    24 jam Customer Service via Live Chat!<br/><br/>
                    <b>Bahasa Indonesia</b> <span class='badge badge-red'>NEW</span><br/>
                   	Berkirim pesan di halaman Resume<span class='badge badge-red'>NEW</span><br/>
                    Cetak & Simpan ke PDF <span class='badge badge-red'>NEW</span><br/>
                    Halaman Lowongan Kerja segera!<br/>
                    <em>More to come</em><br/>";
    $lang['home-modal_action'] = "Ayo Mulai!";
    $lang['home-modal_shop'] =  "Pengguna Civimi Shop, Login <a href='http://shop.civimi.com'>disini</a>";

	//REGISTER 
	$lang['register-title'] = "Bergabung dengan kami untuk membuat online CV/Resume dan portofolio gratis. Ekspresikan dirimu dan menjadi kreatif!!";
	$lang['register-desc'] = "	<li>Profesional online CV/Resume</li>
	 							<li>Online portofolio untuk design, web development, video, dan dokumen essay</li>
	                            <li>Tampilan CV/Resume & portfolio design yang menarik</li>
	                            <li>Proses editing yang simpel</li>
	                            <li>Dicari-cari oleh orang yang berpotensi merekrut anda</li>
	                            <li>Melamar pekerjaan kepada Perusahaan Top*</li>
	                            <li>Menerima kabar perkerjaan melalui email*</li>
	                            <li>Tidak membutuhkan pembayaran, GRATIS!</li>";
	$lang['register-head'] = "Mudah & Gratis!";
	$lang['register-sosmed'] = "Cepat tanpa verifikasi email!";

	//PROFILE PAGE

	//SIDE NAV PROFILE
	$lang['nav-profil'] = "Beranda";
	$lang['nav-personal'] = "Personal";
	$lang['nav-summary'] = "Ringkasan";
	$lang['nav-education'] = "Pendidikan";
	$lang['nav-work'] = "Pekerjaan";
	$lang['nav-skill'] = "Keahlian";
	$lang['nav-language'] = "Bahasa";
	$lang['nav-certificate'] = "Sertifikat";
	$lang['nav-organization'] = "Organisasi";
	$lang['nav-achievement'] = "Penghargaan";
	$lang['nav-portfolio'] = "Portfolio";
	$lang['nav-image'] = "Gambar";
	$lang['nav-video'] = "Video";
	$lang['nav-document'] = "Dokumen";
	$lang['nav-web'] = "Web";
	$lang['nav-setting'] = "Pengaturan";

	//SUMMARY PART
	$lang['sum-title'] = "RINGKASAN";
	$lang['sum-desc_placeholder'] = "sekilas mengenai diri anda, hal yang ingin anda capai, mimpi anda.";
	$lang['sum-empty'] = "Tuliskan sekilas tentang diri anda, semangat, mimpi, dan inspirasi anda.";

	//PERSONAL PART
	$lang['personal-title'] = "DATA PRIBADI";
	$lang['personal-fname'] = "Nama Depan";
	$lang['personal-fname_placeholder'] = "Nama Depan";
	$lang['personal-lname'] = "Nama Belakang";
	$lang['personal-lname_placeholder'] = "Nama Belakang";
	$lang['personal-date'] = "Tanggal Lahir";
	$lang['personal-dob_show'] = "Tampilkan Tanggal Lahir di Profile dan Resume ";
	$lang['personal-interest'] = "Tujuan Sekarang";
	$lang['personal-interest_select'] = "Pilih Tujuan";
	$lang['personal-job'] = "Profesi/Pekerjaan";
	$lang['personal-job_placeholder'] = "Profesi / Pekerjaan anda, i.e. Mahasiswa, Akuntan, Dokter Spesialis Mata,Rocket iOS Developer";
	$lang['personal-city'] = "Kota";
	$lang['personal-city_placeholder'] = "Kota domisili, i.e. Los Angeles, Depok, Amsterdam";
	$lang['personal-state'] = "Provinsi";
	$lang['personal-state_placeholder'] = "Provinsi domisili, i.e. California, West Java, North Holland";
	$lang['personal-country'] = "Negara";
	$lang['personal-country_select'] = "Pilih Negara";
	$lang['personal-email'] = "E-mail";
	$lang['personal-email_placeholder'] = "email yang aktif digunakan";
	$lang['personal-phone'] = "Telepon";
	$lang['personal-phone_placeholder'] = "nomor telepon yang aktif digunakan";
	$lang['personal-phone_show'] = "Tampilkan No Telepon di Profile dan Resume ";
	$lang['personal-website'] = "Website";
	$lang['personal-website_placeholder'] = "Website";

	//EDU PART
	$lang['edu-title'] = "PENDIDIKAN";
	$lang['edu-btn'] = "Tambah Pendidikan";
	$lang['edu-school'] = "Sekolah/Universitas";
	$lang['edu-school_placeholder'] = "masukan nama sekolah/Universitas";
	$lang['edu-country'] = "Negara";
	$lang['edu-country_placeholder'] = "negara tempat sekolah/Universitas";
	$lang['edu-city'] = "Kota";
	$lang['edu-city_placeholder'] = "kota tempat sekolah/Universitas";
	$lang['edu-attending'] = "Masih Dalam Proses";
	$lang['edu-from'] = "Dari";
	$lang['edu-to'] = "Sampai";
	$lang['edu-degree'] = "Gelar/Degree";
	$lang['edu-degree_placeholder'] = "gelar/degree anda i.e. Bachelor of Arts, Sarjana, Master";
	$lang['edu-study'] = "Bidang Pembelajaran";
	$lang['edu-study_placeholder'] = "bidang pembelajaran i.e. Computer Science, Product Design, Kehutanan";
	$lang['edu-grade'] = "Nilai";
	$lang['edu-grade_placeholder'] = "nilai anda, GPA, IPK i.e. 4.0, 3.59";
	$lang['edu-desc'] = "Penjelasan";
	$lang['edu-desc_placeholder'] = "jelaskan apa yang anda raih selama proses pembelajaran, i.e. Menjadi Asisten Dosen, Kehadiran 100% selama beberapa semester, etc";
	$lang['edu-placeholder'] = "Masukkan sejarah pendidikan anda dengan rincian prestasi anda.";

	//WORK PART
	$lang['work-title'] = "PENGALAMAN KERJA";
	$lang['work-btn'] = "Tambah Pekerjaan";
	$lang['work-job'] = "Nama Pekerjaan";
	$lang['work-job_placeholder'] = "Profesi / Pekerjaan anda, i.e. Mahasiswa, Akuntan, Dokter Spesialis Mata,Rocket iOS Developer";
	$lang['work-company'] = "Nama Perusahaan";
	$lang['work-company_placeholder'] = "nama perusahaan i.e. Google, Facebook, Civimi, Uniqlo ,Eannovate, etc ";
	$lang['work-country'] = "Negara";
	$lang['work-country_placeholder'] = "Negara tempat anda bekerja i.e. USA, Indonesia, Japan, South Korea";
	$lang['work-city'] = "Kota";
	$lang['work-city_placeholder'] = "Kota tempat anda bekerja i.e. Mountain View, Jakarta, Tokyo, Seoul";
	$lang['work-attending'] = "Masih Dalam Proses";
	$lang['work-from'] = "Dari";
	$lang['work-to'] = "Sampai";
	$lang['work-desc'] = "Penjelasan";
	$lang['work-desc_placeholder'] = "jelaskan peranan anda, tanggung jawab, dan tugas anda. i.e. bertanggung jawab untuk manajemen hubungan dengan pelanggan.";
	$lang['work-placeholder'] = "Memiliki pengalaman bekerja dan pengalaman magang?<br/>Masukan pengalaman anda disini.";

	//ORGANIZATION PART
	$lang['org-title'] = "ORGANISASI";
	$lang['org-btn'] = "Tambah Organisasi";
	$lang['org-job'] = "Jabatan di Organisasi";
	$lang['org-job_placeholder'] = "jabatan anda di organisasi. i.e. Member, Ketua Organisasi, Bendahara, Sekretaris, etc.";
	$lang['org-organization'] = "Nama Organisasi";
	$lang['org-organization_placeholder'] = "nama organisasi i.e. Indonesia Mengajar, Ikatan Motor Indonesia, Persatuan Bola Basket Seluruh Indonesia, Taekwondo Indonesia, Indonesia Mengajar, etc ";
	$lang['org-country'] = "Negara";
	$lang['org-country_placeholder'] = "Negara tempat organisasi anda berada i.e. USA, Indonesia, Japan, South Korea";
	$lang['org-city'] = "Kota";
	$lang['org-city_placeholder'] = "Kota tempat organisasi anda berada i.e. Mountain View, Jakarta, Tokyo, Seoul";
	$lang['org-attending'] = "Masih di Organisasi";
	$lang['org-from'] = "Dari";
	$lang['org-to'] = "Sampai";
	$lang['org-desc'] = "Penjelasan";
	$lang['org-desc_placeholder'] = "jelaskan tentang organisasi dan peranan anda, tanggung jawab, atau tugas anda di dalam organisasi.";
	$lang['org-placeholder'] = "Pernah bergabung dalam organisasi?<br/>Masukan organisasi anda disini.";

	//SKILL PART
	$lang['skill-title'] = "SKILL";
	$lang['skill-btn'] = "Tambah Skill";
	$lang['skill-name'] = "Nama Skill";
	$lang['skill-name_placeholder'] = "nama keahlian i.e. Photo Editing, HTML, Adobe Photoshop";
	$lang['skill-proficiency'] = "Kemahiran";
	$lang['skill-proficiency_select'] = "Pilih pengalaman keahlian anda";
	$lang['skill-proficiency_placeholder'] = "Pilih seberapa mahir anda";
	$lang['skill-desc'] = "Penjelasan";
	$lang['skill-desc_placeholder'] = "jelaskan Bagaimana anda mempelajari, mendapatkan, dan menggunakan keahlian anda di lingkungan pekerjaan.";
	$lang['skill-placeholder'] = "Setiap orang berbakat!<br/>Masukan info mengenai bakat anda dan seberapa besar kemampuan anda disini.";

	//LANGUAGE PART
	$lang['lang-title'] = "BAHASA";
	$lang['lang-btn'] = "Tambah Bahasa";
	$lang['lang-name'] = "Bahasa";
	$lang['lang-name_placeholder'] = "bahasa i.e. Chinese, English, Japanese, Spanish";
	$lang['lang-listening'] = "Kemahiran Dalam Mendengar";
	$lang['lang-listening_select'] = "Pilih tingkat Kemahiran mendengar anda";
	$lang['lang-listening_placeholder'] = "Pilih Mahir Anda dalam Mendengar";
	$lang['lang-speaking'] = "Kemahiran Dalam Berbicara";
	$lang['lang-speaking_select'] = "Pilih tingkat Kemahiran berbicara anda";
	$lang['lang-speaking_placeholder'] = "Pilih Mahir Anda Dalam Berbicara";
	$lang['lang-reading'] = "Kemahiran Dalam Membaca";
	$lang['lang-reading_select'] = "Pilih tingkat Kemahiran membaca anda";
	$lang['lang-reading_placeholder'] = "Pilih Mahir Anda Dalam Membaca";
	$lang['lang-writing'] = "Kemahiran Dalam Menulis";
	$lang['lang-writing_select'] = "Pilih tingkat Kemahiran menulis anda";
	$lang['lang-writing_placeholder'] = "Pilih Mahir Anda Dalam Menulis";
	$lang['lang-desc'] = "Penjelasan";
	$lang['lang-desc_placeholder'] = "jelaskan Bagaimana anda mempelajari, mendapatkan, dan menggunakan bahasa.";
	$lang['lang-placeholder'] = "Apakah anda memiliki kemampuan berbahasa asing?<br/>Masukan kemampuan bahasa anda disini.";

	//CERTIFICATION PART
	$lang['cert-title'] = "SERTIFIKAT";
	$lang['cert-btn'] = "Tambah Sertifikat";
	$lang['cert-name'] = "Nama Sertifikat";
	$lang['cert-name_placeholder'] = "name sertifikat ex. Certified Public Accountant (CPA)";
	$lang['cert-number'] = "Nomor Sertifikat";
	$lang['cert-number_placeholder'] = "masukan nomor serifikat ex. 103889";
	$lang['cert-url'] = "Sertifikat URL";
	$lang['cert-url_placeholder'] = "masukan URL dimana sertikat itu berada";
	$lang['cert-expire'] = "masih berlaku";
	$lang['cert-from'] = "Dari";
	$lang['cert-to'] = "Sampai";
	$lang['cert-desc'] = "Penjelasan";
	$lang['cert-desc_placeholder'] = "jelaskan detail sertikat, apa yang dapat digunakan sertikat tersebut.";
	$lang['cert-placeholder'] = "Anda memiliki sertifikat?<br/>Masukian info sertifikat anda disini.";

	//ACHIEVEMENT PART
	$lang['achieve-title'] = "PENGHARGAAN";
	$lang['achieve-btn'] = "Tambah Penghargaan";
	$lang['achieve-name'] = "Nama Penghargaan";
	$lang['achieve-name_placeholder'] = "masukan nama penghargaan";
	$lang['achieve-source'] = "Sumber Penghargaan";
	$lang['achieve-source_placeholder'] = "Pilih Satu";
	$lang['achieve-institute'] = "Institusi yang memberikan";
	$lang['achieve-institute_placeholder'] = "masukan institusi yang memberikan";
	$lang['achieve-year'] = "Tahun";
	$lang['achieve-desc'] = "Keterangan";
	$lang['achieve-desc_placeholder'] = "jelaskan bagaimana anda mendapatkan penghargaan tersebut";
	$lang['achieve-placeholder'] = "Masukan penghargaan yang pernah anda raih atau anda miliki.";

	// NO RESULT LIST TEXT
	$lang['entry-result'] = "Tidak ada hasil.";
	$lang['entry-result_text'] = "akan dimasukan sebagai data baru.";
	//END PROFILE PAGE

	//FOOTER
	$lang['footer-note'] = "Tunjukkan Diri Anda";

	//TOUR PROFILE

	//PERSONAL TOUR
	$tour['personal-1-title'] = "Selamat Datang <br/>di Halaman Profile Anda";
	$tour['personal-1-content'] = "Isi data mengenai diri anda di halaman ini";
	$tour['personal-2-title'] = "Bagian Personal</div>";
	$tour['personal-2-content'] = "<div class='text-center'>Isilah Data Personal Anda <br/>Biodata<br/>Profesi<br/>Kota, Provinsi tinggal</div>";
	$tour['personal-3-title'] = "Foto Profile";
	$tour['personal-3-content'] = "Upload foto terbaik anda dengan klik atau drag & drop file foto anda ke kotak di atas";

	//SUMMARY TOUR 
	$tour['summary-1-title'] = "Ringkasan";
	$tour['summary-1-content'] = "Click untuk mengedit data";
	$tour['summary-2-title'] = "Ubah Ringkasan";
	$tour['summary-2-content'] = "<div class='text-center'>masukan ringkasn mengenai diri anda.</div>";

	//EDUCATION TOUR 
	$tour['edu-1-title'] = "Pendidikan";
	$tour['edu-1-content'] = "Bagian ini menjelaskan data pendidikan anda";
	$tour['edu-2-title'] = "Tambah Info Pendidikan";
	$tour['edu-2-content'] = "<div class='text-center'>Tekan tombol ini untuk menambahkan data pendidikan</div>";
	$tour['edu-3-title'] = "Form Tambah Pendidikan";
	$tour['edu-3-content'] = "<div class='text-center'>Disini anda dapat mengisi data pendidikan anda</div>";

	//WORK TOUR 
	$tour['work-1-title'] = "Pengalaman Kerja";
	$tour['work-1-content'] = "Bagian ini menjelaskan data pengalaman kerja anda";
	$tour['work-2-title'] = "Tambah Pengalaman Kerja";
	$tour['work-2-content'] = "<div class='text-center'>Tekan tombol ini untuk menambahkan data pengalaman kerja</div>";
	$tour['work-3-title'] = "Form Tambah Pengalaman Kerja";
	$tour['work-3-content'] = "<div class='text-center'>Disini anda dapat mengisi data pengalaman kerja anda</div>";

	//SKILL TOUR 
	$tour['skill-1-title'] = "Keahlian";
	$tour['skill-1-content'] = "Bagian ini menjelaskan data keahlian anda";
	$tour['skill-2-title'] = "Tambah Keahlian";
	$tour['skill-2-content'] = "<div class='text-center'>Tekan tombol ini untuk menambahkan data keahlian anda</div>";
	$tour['skill-3-title'] = "Form Tambah Keahlian";
	$tour['skill-3-content'] = "<div class='text-center'>Disini anda dapat mengisi data keahlian anda</div>";

	//LANGUAGE TOUR 
	$tour['lang-1-title'] = "Bahasa";
	$tour['lang-1-content'] = "Bagian ini menjelaskan data kemampuan berbahasa anda";
	$tour['lang-2-title'] = "Tambah Bahasa";
	$tour['lang-2-content'] = "<div class='text-center'>Tekan tombol ini untuk menambahkan data bahasa anda</div>";
	$tour['lang-3-title'] = "Form Tambah Bahasa";
	$tour['lang-3-content'] = "<div class='text-center'>Disini anda dapat mengisi data bahasa anda</div>";

	//CERTIFICATE TOUR 
	$tour['cert-1-title'] = "Sertifikat";
	$tour['cert-1-content'] = "Bagian ini menjelaskan data sertifikat yang anda miliki";
	$tour['cert-2-title'] = "Tambah Sertifikat";
	$tour['cert-2-content'] = "<div class='text-center'>Tekan tombol ini untuk menambahkan data sertifikat anda</div>";
	$tour['cert-3-title'] = "Form Tambah Sertifikat";
	$tour['cert-3-content'] = "<div class='text-center'>Disini anda dapat mengisi data sertifikat anda</div>";

	//ORGANIZATION TOUR 
	$tour['org-1-title'] = "Organisasi";
	$tour['org-1-content'] = "Bagian ini menjelaskan data organisasi yang anda ikuti";
	$tour['org-2-title'] = "Tambah Organisasi";
	$tour['org-2-content'] = "<div class='text-center'>Tekan tombol ini untuk menambahkan data organisasi anda</div>";
	$tour['org-3-title'] = "Form Tambah Organisasi";
	$tour['org-3-content'] = "<div class='text-center'>Disini anda dapat mengisi data organisasi anda</div>";

	//ACHIEVEMENT TOUR 
	$tour['achieve-1-title'] = "Penghargaan";
	$tour['achieve-1-content'] = "Bagian ini menjelaskan data penghargaan yang anda miliki";
	$tour['achieve-2-title'] = "Tambah Penghargaan";
	$tour['achieve-2-content'] = "<div class='text-center'>Tekan tombol ini untuk menambahkan data penghargaan anda</div>";
	$tour['achieve-3-title'] = "Form Tambah Penghargaan";
	$tour['achieve-3-content'] = "<div class='text-center'>Disini anda dapat mengisi data penghargaan anda</div>";

	//PROFIL WELCOME TOUR 
	$tour['profil-1-title'] = "Selamat datang di halaman profil anda";
	$tour['profil-1-content'] = "Di halaman ini anda dapat mengisi informasi untuk Profile dan CV anda";
	$tour['profil-2-title'] = "Profil";
	$tour['profil-2-content'] = "<div class='text-center'>Ini adalah bagian-bagian yang dapat anda isi di Profile anda.<br/>Klik <span class='glyphicon glyphicon-info-sign info-sign'></span> jika anda butuh bantuan</div>";
	$tour['profil-3-title'] = "Portofolio";
	$tour['profil-3-content'] = "<div class='text-center'>Bagian ini untuk meng-upload karya-karya anda.</div>";

	//LIKE CARD NOTIFICATION 
	$lang['card-welcome'] = "Halo";
	$lang['card-welcome_text'] = "Seseorang dari";
	$lang['card-welcome_text2'] = "menyukai karya anda!";
	$lang['card-closing'] = "Tunjukkan semangat anda<br/>Temukan pekerjaan dan dapat ditemukan!";
	$lang['card-notification_link'] = "terlalu banyak notifikasi?klik disini";
}
//----------------------------------------END LANGUAGE----------------------------------------//
?>