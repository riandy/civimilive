<?php 

//HOME
$lang['home-notice'] = "Saat ini anda melihat design baru. Jika anda pengguna civimi shop, silahkan login <a href='http://shop.civimi.com' class='alert-link'>disini</a>";
$lang['home-header'] = "Temukan Dirimu";
$lang['home-note'] = "Tunjukan kemampuan anda ke orang berpotensi (i.e. employers, clients, headhunters, etc)";
$lang['home-pop_profil'] = "Popular Profil";
$lang['home-pop_profil_note'] = "ini adalah popular profil hari ini";
$lang['home-reinvent'] = "Temukan Dirimu Sekarang";
$lang['home-reinvent_note'] = "Ini mudah dan gratis!";
$lang['home-reinvent_btn'] = "Daftar Sekarang";

//REGISTER 
$lang['register-title'] = "Bergabung dengan kami untuk membuat online CV/Resume dan portofolio gratis. Ekspresikan dirimu dan menjadi kreatif!!";
$lang['register-desc'] = "	<li>Profesional online CV/Resume</li>
 							<li>Online portofolio untuk design, web development, video, dan dokumen essay</li>
                            <li>Tampilan CV/Resume & portfolio design yang menarik</li>
                            <li>Proses editing yang simpel</li>
                            <li>Diperhatkan oleh orang berpotensi</li>
                            <li>Melamar pekerjaan dengan manajemen atas*</li>
                            <li>Menerima kabar perkerjaan dari email*</li>
                            <li>Tidak dibutuhkan pembayaran</li>";
$lang['register-head'] = "Ini Mudah & Gratis!";

//PROFILE PAGE

//SUMMARY PART
$lang['sum-title'] = "Rangkuman";
$lang['sum-desc-placeholder'] = "jelaskan rangkuman mengenai diri anda.";

//SIDE NAV PROFILE
$lang['nav-profil'] = "Profil";
$lang['nav-personal'] = "Pribadi";
$lang['nav-summary'] = "Rangkuman";
$lang['nav-education'] = "Pendidikan";
$lang['nav-work'] = "Pekerjaan";
$lang['nav-skill'] = "Keahlian";
$lang['nav-portfolio'] = "Portofolio";
$lang['nav-image'] = "Gambar";
$lang['nav-video'] = "Video";
$lang['nav-document'] = "Dokumen";
$lang['nav-web'] = "Web";

//EDU PART
$lang['edu-title'] = "PENDIDIKAN";
$lang['edu-btn'] = "Tambah Pendidikan";
$lang['edu-school'] = "Sekolah/Universitas";
$lang['edu-school-placeholder'] = "masukan nama sekolah/Universitas";
$lang['edu-country'] = "Negara";
$lang['edu-country-placeholder'] = "negara tempat sekolah/Universitas";
$lang['edu-city'] = "Kota";
$lang['edu-city-placeholder'] = "kota tempat sekolah/Universitas";
$lang['edu-attending'] = "Masih Dalam Proses";
$lang['edu-from'] = "Dari";
$lang['edu-to'] = "Sampai";
$lang['edu-degree'] = "Gelar/Degree";
$lang['edu-degree-placeholder'] = "gelar/degree anda i.e. Bachelor of Arts, Sarjana, Master";
$lang['edu-study'] = "Bidang Pembelajaran";
$lang['edu-study-placeholder'] = "bidang pembelajaran i.e. Computer Science, Product Design, Kehutanan";
$lang['edu-grade'] = "Nilai";
$lang['edu-grade-placeholder'] = "nilai anda, GPA, IPK i.e. 4.0, 3.59";
$lang['edu-desc'] = "Deskripsi";
$lang['edu-desc-placeholder'] = "jelaskan apa yang anda raih selama proses pembelajaran, i.e. Menjadi Asisten Dosen, Kehadiran 100% selama beberapa semester, etc";

//WORK PART
$lang['work-title'] = "PEKERJAAN";
$lang['work-btn'] = "Tambah Pekerjaan";
$lang['work-job'] = "Nama Pekerjaan";
$lang['work-job-placeholder'] = "nama perkerjaan anda. i.e. Software Engineer, Financial Analyst, Account Executive, etc.";
$lang['work-company'] = "Nama Perusahaan";
$lang['work-company-placeholder'] = "nama perusahaan i.e. Google, Facebook, Civimi, Uniqlo ,Eannovate, etc ";
$lang['work-country'] = "Negara";
$lang['work-country-placeholder'] = "Negara tempat anda bekerja i.e. USA, Indonesia, Japan, South Korea";
$lang['work-city'] = "Kota";
$lang['work-city-placeholder'] = "Negara tempat anda bekerja i.e. Mountain View, Jakarta, Tokyo, Seoul";
$lang['work-attending'] = "Masih Dalam Proses";
$lang['work-from'] = "Dari";
$lang['work-to'] = "Sampai";
$lang['work-desc'] = "Deskripsi";
$lang['work-desc-placeholder'] = "jelaskan peranan anda, tanggung jawab, dan tugas anda. i.e. bertanggung jawab untuk manajemen hubungan dengan pelanggan.";

//SKILL PART
$lang['skill-title'] = "KEAHLIAN";
$lang['skill-btn'] = "Tambah Keahlian";
$lang['skill-name'] = "Nama Keahlian";
$lang['skill-name-placeholder'] = "nama keahlian i.e. Photo Editing, HTML, Adobe Photoshop";
$lang['skill-proficiency'] = "Kecakapan";
$lang['skill-proficiency-placeholder'] = "Pilih pengalaman keahlian anda";
$lang['skill-desc'] = "Deskripsi";
$lang['skill-desc-placeholder'] = "jelaskan Bagaimana anda mempelajari, mendapatkan, dan menggunakan keahlian anda di lingkungan pekerjaan.";

//LANGUAGE PART
$lang['lang-title'] = "BAHASA";
$lang['lang-btn'] = "Tambah Bahasa";
$lang['lang-name'] = "Bahasa";
$lang['lang-name-placeholder'] = "bahasa i.e. Chinese, English, Japanese, Spanish";
$lang['lang-listening'] = "Kecakapan Dalam Mendengar";
$lang['lang-listening-placeholder'] = "Pilih tingkat kecakapan mendengar anda";
$lang['lang-speaking'] = "Kecakapan Dalam Berbicara";
$lang['lang-speaking-placeholder'] = "Pilih tingkat kecakapan berbicara anda";
$lang['lang-reading'] = "Kecakapan Dalam Membaca";
$lang['lang-reading-placeholder'] = "Pilih tingkat kecakapan membaca anda";
$lang['lang-writing'] = "Kecakapan Dalam Menulis";
$lang['lang-writing-placeholder'] = "Pilih tingkat kecakapan menulis anda";
$lang['lang-desc'] = "Deskripsi";
$lang['lang-desc-placeholder'] = "jelaskan Bagaimana anda mempelajari, mendapatkan, dan menggunakan bahasa.";

//CERTIFICATION PART
$lang['cert-title'] = "SERTIFIKAT";
$lang['cert-btn'] = "Tambah Sertifikat";
$lang['cert-name'] = "Nama Sertifikat";
$lang['cert-name-placeholder'] = "name sertifikat ex. Certified Public Accountant (CPA)";
$lang['cert-number'] = "Nomor Sertifikat";
$lang['cert-number-placeholder'] = "masukan nomor serifikat ex. 103889";
$lang['cert-url'] = "Sertifikat URL";
$lang['cert-url-placeholder'] = "masukan URL dimana sertikat itu berada";
$lang['cert-expire'] = "masih berlaku";
$lang['cert-from'] = "Dari";
$lang['cert-to'] = "Sampai";
$lang['cert-desc'] = "Deskripsi";
$lang['cert-desc-placeholder'] = "jelaskan detail sertikat, apa yang dapat digunakan sertikat tersebut.";

//END PROFILE PAGE

//FOOTER
$lang['footer-note'] = "Temukan Dirimu";
?> 