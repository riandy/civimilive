<div  id="personal-skill-section" class="job-sbox" data-order="4">
  <div id="skill-header" class="user-section-header row border-bottom-eee">
    <div class="col-xs-6">
      <div class="profile-title"><i class="glyphicon glyphicon-star"></i>&nbsp;&nbsp;<?=$lang['skill-title'];?></div>
    </div>
    <div class="col-xs-6">
        <div class="edit-button text-right">
            <span class="sort-icon"><i class="glyphicon glyphicon-sort"></i></span>
            <a href="Javascript:;" id="add-skill" onclick="hideEmptySkill();" data-toggle="collapse" data-target="#slide-skill" aria-expanded="true" aria-controls="slide-skill">
              <i class="glyphicon glyphicon-plus"></i> <span class="hidden-xs"><?=$lang['skill-btn'];?></span>
            </a>
        </div>
    </div>
  </div>
  <div id="skill-profile" class="profile-well">
    <!-- start add skill -->
    <div id="slide-skill" class="collapse panel-input">
      <div class="panel-input-body panel-input-skill">
        <div class="row up15">
          <div class="col-xs-12"> 
            <div class="form-label bold"><?=$lang['skill-name'];?> *</div>
          </div>
          <div class="col-sm-10 col-xs-12">
            <div id="sname-box" class="sname-box">
              <input id="skill-name" onfocus="$('#sname-list').show();" oninput="get_sname();$('#sname-list').show();" type="text" name="" class="form-control no-radius" placeholder="<?=$lang['skill-name_placeholder'];?>">
              <div id="sname-list" class="sname-list" onmouseover="$('#sname-list').show();">
                <ul id="sname-ul">

                </ul>
              </div>
            </div>
            <div id="skill-vname" class="valid-message"></div>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-10 col-xs-12 up1">
            <div class="form-label bold">
              <?=$lang['skill-proficiency'];?> *
              <a id="info_rate" href="javascript:;" class="skill-tips" type="button" data-container="body" data-toggle="popover" data-placement="top" data-content="<?=$lang['skill-proficiency_placeholder'];?>"><i class="glyphicon glyphicon-info-sign"></i></a>
            </div>
          </div>
          <div class="col-sm-10 col-xs-12" style="">
            <select id="skill-rate" name="" class="form-control no-radius">
              <option value="" class="skill-prof"><?=$lang['skill-proficiency_select'];?></option>
              <option value="1" class="skill-prof">Beginner - < 1 year</option>
              <option value="2" class="skill-prof">Intermediate -  1 - 3 year</option>
              <option value="3" class="skill-prof">Advanced - 3 - 5 year</option>
              <option value="4" class="skill-prof">Expert - > 5 year</option>
            </select>
          </div>
          <div class="col-xs-12">
            <div id="skill-vrate" class="valid-message"></div>
          </div>
        </div>
        <div class="row up1">
          <div class="col-sm-10 col-xs-12">
            <div class="form-label">
              <?=$lang['skill-desc'];?>
              <a id="info_sdesc" href="javascript:;" class="skill-tips" type="button" data-container="body" data-toggle="popover" data-placement="top" data-content="<?=$lang['skill-desc_placeholder'];?>"><i class="glyphicon glyphicon-info-sign"></i></a>
            </div>
          </div>
          <div class="col-sm-10 col-xs-12">
            <textarea id="skill-desc" name="desc" class="form-control no-radius" rows="4" placeholder="<?=$lang['skill-desc_placeholder'];?>"></textarea>
          </div>
        </div>
        <div class="row up2">
          <div class="col-xs-12">
            <a href="Javascript:;" class="btn btn-info" onclick="save_skill();">Save</a><span class="pad05"></span>
            <a href="Javascript:;" onclick="hideEmptySkill();" type="reset" class="btn btn-default" data-toggle="collapse" data-target="#slide-skill" aria-expanded="true" aria-controls="slide-skill">Cancel</a>
          </div>
        </div>
      </div>
    </div><!-- end panel add skill -->


    <!-- main skill -->
    <div id="main-skill" class="panel-group marg0" role="tablist" aria-multiselectable="true">
    </div> <!-- end main skill -->
    <div class="see-more"><a href="javascript:;" id="see-skill" class="hide">SEE <span id="seeSkill-num"></span> MORE</a></div>
    <div id='load-skill' class='text-center'></div>
    <input id="total-skill" type="hidden">

    <!-- start edit skill -->
    <div id="edit-skill-place" class="hide">
      <div id="edit-skill">
        <div class="panel-input-skill">
          <div class="row up15">
            <div class="col-xs-12">
              <div class="form-label bold"><?=$lang['skill-name'];?> *</div>
            </div>
            <div class="col-sm-10 col-xs-12">
              <div id="sname-boxEdit" class="sname-box">
                <input id="skill-ename" onfocus="$('#sname-listEdit').show();" oninput="get_snameEdit();$('#sname-listEdit').show();" type="text" name="" class="form-control no-radius" placeholder="<?=$lang['skill-name_placeholder'];?>">
                <div id="sname-listEdit" class="sname-list" onmouseover="$('#sname-listEdit').show();">
                  <ul id="sname-eul">

                  </ul>
                </div>
              </div>
              <div id="skill-evname" class="valid-message"></div>
            </div>
          </div>
          <div class="row">
            <div class="col-sm-10 col-xs-12 up1">
              <div class="form-label bold">
                <?=$lang['skill-proficiency'];?> *
                <a id="info_erate" href="javascript:;" class="skill-tips" type="button" data-container="body" data-toggle="popover" data-placement="top" data-content="<?=$lang['skill-proficiency_placeholder'];?>"><i class="glyphicon glyphicon-info-sign"></i></a>
              </div>
            </div>
            <div class="col-sm-10 col-xs-12" style="">
              <select id="skill-erate" name="" class="form-control no-radius">
                <option value="" class="skill-eprof"><?=$lang['skill-proficiency_select'];?></option>
                <option value="1" class="skill-eprof">Beginner - < 1 year</option>
                <option value="2" class="skill-eprof">Intermediate -  1 - 3 year</option>
                <option value="3" class="skill-eprof">Advanced - 3 - 5 year</option>
                <option value="4" class="skill-eprof">Expert - > 5 year</option>
              </select>
            </div>
            <div class="col-xs-12">
              <div id="skill-evrate" class="valid-message"></div>
            </div>
          </div>
          <div class="row up1">
            <div class="col-sm-10 col-xs-12">
              <div class="form-label">
                <?=$lang['skill-desc'];?>
                <a id="info_esdesc" href="javascript:;" class="skill-tips" type="button" data-container="body" data-toggle="popover" data-placement="top" data-content="<?=$lang['skill-desc_placeholder'];?>"><i class="glyphicon glyphicon-info-sign"></i></a>
              </div>
            </div>
            <div class="col-sm-10 col-xs-12">
              <textarea id="skill-edesc" name="desc" class="form-control no-radius" rows="4" placeholder="<?=$lang['skill-desc_placeholder'];?>"></textarea>
            </div>
          </div>
          <div class="row up1">
            <div class="col-xs-12">
              <input id="edit_skill_old_title" type="hidden">
              <input id="edit_skill_id" type="hidden">
              <a class="btn btn-info" onclick="update_skill();" >Save</a><span class="pad05"></span>
              <a id="reset_edit_skill" type="reset" class="btn btn-default" onclick="close_edit_skill()">Cancel</a>
              <a href="javascript:;" id="delete-skill" onclick="delete_skill();" class="delete-text">Delete</a>
            </div>
          </div>
        </div>
      </div>
    </div><!-- end edit skill -->
  </div> <!-- end skill profile -->

<script>
  $('#info_rate').popover({
    placement : 'top',
    trigger : 'click',
    delay: { 
       show: "100", 
       hide: "100"
    },
  });
  $('#info_rate').on('shown.bs.popover', function() {
    setTimeout(function() {
        $('#info_rate').popover('hide');
    }, 4000);
  });
  $('#info_sdesc').popover({
    placement : 'top',
    trigger : 'click',
    delay: { 
       show: "100", 
       hide: "100"
    },
  });
  $('#info_sdesc').on('shown.bs.popover', function() {
    setTimeout(function() {
        $('#info_wdesc').popover('hide');
    }, 4000);
  });
  $('#info_erate').popover({
    placement : 'top',
    trigger : 'click',
    delay: { 
       show: "100", 
       hide: "100"
    },
  });
  $('#info_erate').on('shown.bs.popover', function() {
    setTimeout(function() {
        $('#info_erate').popover('hide');
    }, 4000);
  });
  $('#info_esdesc').popover({
    placement : 'top',
    trigger : 'click',
    delay: { 
       show: "100", 
       hide: "100"
    },
  });
  $('#info_esdesc').on('shown.bs.popover', function() {
    setTimeout(function() {
        $('#info_ewdesc').popover('hide');
    }, 4000);
  });
  //get skill name
  $('html').click(function() {
    $('#sname-list').hide();
    $("#skill-name").removeClass("load-input");
  });
  $('#sname-box').click(function(event){
    event.stopPropagation();
    $("#skill-name").removeClass("load-input");
  });
  $('html').click(function() {
    $('#sname-listEdit').hide();
    $("#skill-ename").removeClass("load-input");
  });
  $('#sname-boxEdit').click(function(event){
    event.stopPropagation();
    $("#skill-ename").removeClass("load-input");
  });
  function get_sname(){
    $("#skill-name").addClass("load-input");
    var q = $("#skill-name").val();
    var url = "<?=$api['skill-autocomplete'];?>"+q;                            
    $.ajax({url: url,success:function(result){
      $("#skill-name").removeClass("load-input");
      $('#sname-ul').html(replace_sname(result.data,q));
    }}); 
  }
  function get_snameEdit(){
    $("#skill-ename").addClass("load-input");
    var q = $("#skill-ename").val();
    var url = "<?=$api['skill-autocomplete'];?>"+q;                            
    $.ajax({url: url,success:function(result){
      $("#skill-ename").removeClass("load-input");
      $('#sname-eul').html(replace_snameEdit(result.data,q));
    }}); 
  }
  function replace_sname(datas,text){
    var resultHTML='';

    if(datas != null){
      var obj = datas;    
      for(var i=0;i < obj.length;i++){
        if(obj[i].Skill_ID != '' && obj[i].Skill_name != ''){
          resultHTML += "<li class='sname-li' id='sname-"+obj[i].Skill_ID+"' onclick='selectSname("+obj[i].Skill_ID+")' data-sk_title='"+obj[i].Skill_name+"'>";
          if(obj[i].Skill_img != ""){
            resultHTML += "<div class='sname-image'><img src='<?=$global['img-url'];?>"+obj[i].Skill_img+"' alt=''></div>";
          }else {
            resultHTML += "<div class='sname-image'><img src='<?=$placeholder['img-skill'];?>' alt=''></div>";
          }
          resultHTML += "<div class='sname-text'>";
          resultHTML += "<div class='sname-title'>"+obj[i].Skill_name+"</div>";
          resultHTML += "</div>";
          resultHTML += "</li>";
        }
      }
    } else {
      resultHTML += "<li class='no-list'><div class='no-list-text'><?=$lang['entry-result'];?> \"<span>"+text+"</span>\" <?=$lang['entry-result_text'];?></div></li>";
    }
    return resultHTML;
  }
  function replace_snameEdit(datas,text){
    var resultHTML='';

    if(datas != null){
      var obj = datas;    
      for(var i=0;i < obj.length;i++){
        if(obj[i].Skill_ID != '' && obj[i].Skill_name != ''){
          resultHTML += "<li class='sname-li' id='esname-"+obj[i].Skill_ID+"' onclick='selectSnameEdit("+obj[i].Skill_ID+")' data-sk_etitle='"+obj[i].Skill_name+"'>";
          if(obj[i].Skill_img != ""){
            resultHTML += "<div class='sname-image'><img src='<?=$global['img-url'];?>"+obj[i].Skill_img+"' alt=''></div>";
          }else {
            resultHTML += "<div class='sname-image'><img src='<?=$placeholder['img-skill'];?>' alt=''></div>";
          }
          resultHTML += "<div class='sname-text'>";
          resultHTML += "<div class='sname-title'>"+obj[i].Skill_name+"</div>";
          resultHTML += "</div>";
          resultHTML += "</li>";
        }
      }
    } else {
      resultHTML += "<li class='no-list'><div class='no-list-text'><?=$lang['entry-result'];?> \"<span>"+text+"</span>\" <?=$lang['entry-result_text'];?></div></li>";
    }
    return resultHTML;
  }
  function selectSname(param){
    var title = $('#sname-'+param).data('sk_title');
    $("#skill-name").removeClass("load-input");
    $('#skill-name').val(title);
    $('#sname-list').hide();
  }
  function selectSnameEdit(param){
    var title = $('#esname-'+param).data('sk_etitle');
    $("#skill-ename").removeClass("load-input");
    $('#skill-ename').val(title);
    $('#sname-listEdit').hide();
  }
  //clear value add skill
  function emptyAddSkill(){
    $("#skill-name").val('');
    $('option[value=""].skill-prof').attr("selected", true);
    $("#skill-desc").val('');
  }
  //open slide edit
  function edit_box_skill(param){
    if ($('#slide-skill').hasClass('in')){
      $('#slide-skill').collapse('hide');
    }
    if($('.skill-box').hasClass('hide-skill')){
      $('.skill-box').removeClass('hide-skill');
      if($('.collapse-skill').hasClass('in')){
        $('.collapse-skill').removeClass('in')
      }
      $('#skill-'+param).addClass('hide-skill');
    } else {
      $('#skill-'+param).addClass('hide-skill');
    }
    $('#edit-skill').detach().appendTo('#scollapse-'+param+'');
    var id = $('#skill-'+param).data('skill_id');
    var title = $('#skill-'+param).data('skill_name');
    var rate = $('#skill-'+param).data('skill_rate');
    var desc = $('#skill-'+param).data('skill_desc');

    $('#reset_edit_skill').data("target") === "#scollapse";
    $('#skill-ename').val(title);
    $('#skill-edesc').val(desc);
    $('#edit_skill_old_title').val(title);
    $('#edit_skill_id').val(id);
    $('option[value="' + rate + '"].skill-eprof').attr("selected", true);
  }
  //close edit skill
  function close_edit_skill(){
    var id = $('#edit_skill_id').val();
    $('#close-skill-'+id+'').click();
    $('#skill-'+id).removeClass('hide-skill');
  }
  //hide empty word when add new
  function hideEmptySkill(){
    var myElem = document.getElementById('empty-skill');
    if(myElem != null){
      $('#empty-skill').toggle();
    }
    var id = $('#edit_skill_id').val();
    if (id != ''){
      if ($('#scollapse-'+id).hasClass('in')){
        $('#close-skill-'+id+'').click();
        $('#skill-'+id).removeClass('hide-skill');
      }
    }
    emptyAddSkill();
  }
  //update function
  function update_skill(){
    if(validSkillEdit()){
    $('#edit-skill').detach().appendTo('#edit-skill-place');
    var iskill = $('#edit_skill_id').val();
    $('#close-skill-'+iskill).click();
    $('html, body').animate({
      scrollTop: $('#skill-header').offset().top-80
    }, 'fast');
    //$('#load-skill-'+iskill).html('<img src="img/load-blue.gif" class="marg1" />');

      var url = "<?=$api['skill-update'];?>";
      var skill_id = $("#edit_skill_id").val();
      var skill_name = $("#skill-ename").val();
      var skill_rate = $("#skill-erate").val();
      var skill_desc = $("#skill-edesc").val();
      var data = {
        user_id : "<?=$_SESSION['userData']['id'];?>",
        auth_code : "<?=$_SESSION['userData']['auth_code'];?>",
        id : skill_id,
        name : skill_name,
        desc : skill_desc,
        rate : skill_rate
      }
      $.ajax({url: url,data : data, success:function(result){
        get_skill();
        console.log(result);
      }});
    }
  }
  //insert function
  function save_skill(){
    if(validSkill()){
      $('#edit-skill').detach().appendTo('#edit-skill-place');
      $('html, body').animate({
        scrollTop: $('#skill-header').offset().top-80
      }, 'fast');
      //$('#load-skill').html('<img src="img/load-blue.gif" style="margin:70px 0" />');

      var url = "<?=$api['skill-update'];?>";
      var skill_title = $("#skill-name").val();
      var skill_rate = $("#skill-rate").val();
      var skill_desc = $("#skill-desc").val();
      var data = {
        user_id : "<?=$_SESSION['userData']['id'];?>",
        auth_code : "<?=$_SESSION['userData']['auth_code'];?>",
        name : skill_title,
        desc : skill_desc,
        rate : skill_rate
      }
      $.ajax({url: url,data : data, success:function(result){
        $('#slide-skill').collapse('hide');
        get_skill();
        emptyAddSkill();
        console.log(result);
      }});
    }
  } 
  //delete function
  function delete_skill(){
    if (confirm("Are you sure to delete this information ?")) {
      $('#edit-skill').detach().appendTo('#edit-skill-place');
      $('html, body').animate({
        scrollTop: $('#skill-header').offset().top-80
      }, 'fast');
      
      var url = "<?=$api['skill-delete'];?>";
      var skill_id = $("#edit_skill_id").val();
      var data = {
        user_id : "<?=$_SESSION['userData']['id'];?>",
        auth_code : "<?=$_SESSION['userData']['auth_code'];?>",
        id : skill_id
      }
      $.ajax({url: url,data : data, success:function(result){
        get_skill();
        console.log(result);
      }});
    }
  }
  get_skill();
  function get_skill(){
    $('#see-skill').addClass('hide');
    $('#load-skill').html("<img src='<?=$global['absolute-url'];?>img/load-blue.gif' style='margin:70px 0' />");
    $('#main-skill').html('');
    var url = "<?=$api['skill-data'];?>&user_id=<?php echo $_SESSION['userData']['id'];?>&auth_code=<?php echo $_SESSION['userData']['auth_code'];?>";                            
    $.ajax({url: url,success:function(result){
      setTimeout(function() {
      $('#load-skill').html('');
      $('#main-skill').html(replace_skill(result.data));
      seeMoreSkill();
      }, 1000)
    }}); 
  }
  function replace_rate(param){
    var rate = "";
    if(param == 1){
      rate = "Beginner";
    } else if (param == 2){
      rate = "Intermediate";
    } else if (param == 3){
      rate = "Advanced";
    } else if (param == 4){
      rate = "Expert";
    }
    return rate;
  }
  function replace_skill(datas){
    var resultHTML='';resultNext = "";
    var total = 0;previous = "";user_name = "";link="";
    var d = new Date();strip_string ="";
    var current = d.getTime();
    // console.log(datas);
    if(datas != null){
      var obj = datas;    
      for(var i=0;i < obj.length;i++){
        if(obj[i].Skill_ID != '' && obj[i].Skill_name != ''){
          resultHTML += "<div class='row row-skill hide hm-skill' role='tab'>";
          resultHTML += "<div class='col-xs-12'>";
          resultHTML += "<a href='#scollapse-"+obj[i].Skill_ID+"' aria-controls='scollapse-"+obj[i].Skill_ID+"' data-parent='#main-skill' onclick='edit_box_skill("+obj[i].Skill_ID+");' data-toggle='collapse' aria-expanded='false' id='skill-"+obj[i].Skill_ID+"' data-skill_id='"+obj[i].Skill_ID+"' data-skill_name='"+obj[i].Skill_name+"' data-skill_rate='"+obj[i].ATskill_rate+"' data-skill_desc='"+obj[i].ATskill_desc+"' class='skill-box'>";
          resultHTML += "<div class='row'>";
          resultHTML += "<div class='col-md-10 col-xs-9 pad0'>";
          //resultHTML += "<a href='#scollapse-"+obj[i].Skill_ID+"' aria-controls='scollapse-"+obj[i].Skill_ID+"' data-parent='#main-skill' onclick='edit_box_skill("+obj[i].Skill_ID+");' data-toggle='collapse' aria-expanded='false'>";
          resultHTML += "<div class='skill-name'>"+obj[i].Skill_name+" <span class='skill-etext edit-button'><i class='glyphicon glyphicon-edit'></i> <span class='hidden-xs'>Edit</span></span></div>";
          //resultHTML += "</a>";
          resultHTML += "<div class='skill-rate'>"+replace_rate(obj[i].ATskill_rate)+"</div>";
          //resultHTML += "<div class='skill-rate'>"+replace_rate(obj[i].ATskill_rate)+" <a href='javascript:;' onclick='timetips("+i+")' id='info_rate-"+i+"' class='skill-tooltips' type='button' data-container='body' data-toggle='popover' data-placement='top' data-content='"+encodesummary(obj[i].ATskill_desc)+"'><i class='glyphicon glyphicon-info-sign'></i></a></div>";
          resultHTML += "</div>";
          resultHTML += "<div class='col-md-2 col-xs-3'></div>";
          resultHTML += "</div>";
          resultHTML += "</a>";
          resultHTML += "<div id='load-skill-"+obj[i].Skill_ID+"' class='text-center'></div>";
          resultHTML += "<a id='close-skill-"+obj[i].Skill_ID+"' href='#scollapse-"+obj[i].Skill_ID+"' aria-controls='scollapse-"+obj[i].Skill_ID+"' data-parent='#main-skill' data-toggle='collapse' aria-expanded='false' class='hide'></a>";
          resultHTML += "</div>";
          resultHTML += "</div>";
          resultHTML += "<div id='scollapse-"+obj[i].Skill_ID+"' aria-labelledby='skill-"+obj[i].Skill_ID+"' class='panel-collapse collapse collapse-skill pad0' role='tabpanel'></div>";

          //$("#total-skill").val(obj.length);
        }
      }
    } else {
      resultHTML += "<div id='empty-skill' class='empty-file'><?=$lang['skill-placeholder'];?></div>";
    }
    return resultHTML;
  }
  function seeMoreSkill(){
    size = $(".row-skill").size();
    x=2;

    $('.row-skill:lt('+x+')').removeClass('hide');
    $('.row-skill:lt('+x+')').removeClass('hm-skill'); 
    size_left= $(".hm-skill").size();
    if(size > 2){
      $('#see-skill').removeClass('hide');
      $("#seeSkill-num").text('('+size_left+')');
    } else {
      $('#see-skill').addClass('hide');
    }

    $('#see-skill').on('click', function(){
        $('.row-skill').removeClass('hide');
        $('.row-skill').removeClass('hm-skill');
        $('#see-skill').addClass('hide');
    });
};
  function validSkill(){
    var skill_name = $("#skill-name").val();
    var skill_rate = $("#skill-rate").val();

    if(skill_name != ""){
      if(skill_name.length >= 3){
        message = "";
        $("#skill-name").removeClass('input-error');
        $("#skill-vname").text(message);
      } else {
        message = "Skill Name has to be at least 3 characters!";
        $("#skill-name").addClass('input-error');
        $("#skill-name").focus();
        $("#skill-vname").text(message);
        return false;
      }
    } else {
      message = "Please input or select your Skill Name!";
      $("#skill-name").addClass('input-error');
      $("#skill-name").focus();
      $("#skill-vname").text(message);
      return false;
    }
    if(skill_rate != ""){
      message = "";
      $("#skill-rate").removeClass('input-error');
      $("#skill-vrate").text(message);
    } else {
      message = "Please select your skill rate!";
      $("#skill-rate").addClass('input-error');
      $("#skill-rate").focus();
      $("#skill-vrate").text(message);
      return false;
    }
    return true;
  }
  function validSkillEdit(){
    var skill_name = $("#skill-ename").val();
    var skill_rate = $("#skill-erate").val();

    if(skill_name != ""){
      if(skill_name.length >= 3){
        message = "";
        $("#skill-ename").removeClass('input-error');
        $("#skill-evname").text(message);
      } else {
        message = "Skill Name has to be at least 3 characters!";
        $("#skill-ename").addClass('input-error');
        $("#skill-ename").focus();
        $("#skill-evname").text(message);
        return false;
      }
    } else {
      message = "Please input or select your Skill Name!";
      $("#skill-ename").addClass('input-error');
      $("#skill-ename").focus();
      $("#skill-evname").text(message);
      return false;
    }
    if(skill_rate != ""){
      message = "";
      $("#skill-erate").removeClass('input-error');
      $("#skill-evrate").text(message);
    } else {
      message = "Please select your skill rate!";
      $("#skill-erate").addClass('input-error');
      $("#skill-erate").focus();
      $("#skill-evrate").text(message);
      return false;
    }
    return true;
  }

  function calltips(){
  var total =  $("#total-skill").val();
  for(var j=0;j < total;j++){
    $('#info_rate-'+j).popover({ placement : 'top', trigger : 'click', delay: { show: '100', hide: '100'}, });
    $('#info_rate-'+j).toggleClass('active');
  }
  }
  function timetips(param){
    $('#info_rate-'+param).on('shown.bs.popover', function() { setTimeout(function() { $('#info_rate-'+param).popover('hide'); }, 3000); });
  }
</script>
</div><!-- end personal skill section -->