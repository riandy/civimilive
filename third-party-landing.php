<?php

require_once('../admin/packages/back_config.php'); 
include("packages/require.php");
include("controller/controller_landing.php");

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html id="bg-admin" xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Landing third-party</title>
    <link rel="shortcut icon" href="<?php echo $global['absolute-url'];?>/img/favicon.ico"/>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="robots" content="noindex, nofollow" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
    <link href="../admin/css/bootstrap.min.css" rel="stylesheet" media="screen"/>
    <link href="../admin/css/bootstrap-responsive.min.css" rel="stylesheet" media="screen"/>
    <link href="../admin/stylesheets/dashboard.css" rel="stylesheet" type="text/css" />
    <link href="../admin/stylesheets/admin-global.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="<?php echo $global['absolute-url'];?>/stylesheets/libcvm.css"/>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
    <script src="../admin/js/bootstrap.min.js"></script>
    <script src="../admin/js/scroll_civimiDB.js" type="text/javascript"></script>
    <script src="../admin/js/getDate.js" type="text/javascript"></script>
    <style type="text/css">
        .bg-img{border-radius:100em;opacity:1.5;border-top:2px solid #cf2031;border-right:2px solid #0f7dc8;
            border-bottom:2px solid #2eb31a;border-left:2px solid #eab823;
        }
    </style>
</head>
<body id="admin-login" class="container">
    <?php if ($_SESSION['ID']): ?> <!-- After user sign/login -->
    <div class="row-fluid">
        <div class="span12">
            <h3>Create Account CIVIMI</h3>
        </div>
    </div>
    <div class="row-fluid">
        <div id="admin-logo" class="span12">
            <img class="bg-img" style="width:15%;" id="admin-logo-area" src="<?php echo $_SESSION['PROFILE_PICTURE'];?>" border="0" />
        </div>
    </div>
    <div class="row-fluid">
        <div class="span12">
            <h4>Hi <?php echo $_SESSION['FULLNAME'];?>!</h4>
            <?php if($_SESSION['PROVIDER'] == "Twitter"){echo "@".$_SESSION['USERNAME'];}
                    else if($_SESSION['PROVIDER'] == "Facebook" && $_SESSION['EMAIL'] == NULL){echo "E-mail is empty";}
                        else{echo $_SESSION['EMAIL'];}?><br>
            <?php echo $_SESSION['CITY'].", ".$_SESSION['COUNTRY'];?>
        </div>
    </div>
    <div class="row-fluid up2">
        <div class="span12">
            This account will let you connect CIVIMI<br> 
            to services login via <?php echo $_SESSION['PROVIDER'];?>.
        </div>
    </div>
    <div class="row-fluid up2">
        <div class="span12" align="center">
            <form action="third-party-landing.php?action=finish" method="post">
             <table>
              <tr>
                <td>
                  <input name="fname" type="hidden" value="<?php echo $_SESSION['FIRSTNAME'];?>"/>
                  <input name="lname" type="hidden" value="<?php echo $_SESSION['LASTNAME'];?>"/>
                  <input name="birthday1" type="hidden" value="<?php echo date('d', strtotime($_SESSION['BIRTHDAY']));?>"/>
                  <input name="birthday2" type="hidden" value="<?php echo date('m', strtotime($_SESSION['BIRTHDAY']));?>"/>
                  <input name="birthday3" type="hidden" value="<?php echo date('Y', strtotime($_SESSION['BIRTHDAY']));?>"/>
                  <input name="city" type="hidden" value="<?php echo $_SESSION['CITY'];?>"/>
                  <input name="country" type="hidden" value="<?php echo $_SESSION['COUNTRY'];?>"/>
                  <input name="provider" type="hidden" value="<?php echo $_SESSION['PROVIDER'];?>"/>
                  <input name="picture" type="hidden" value="<?php echo $_SESSION['PROFILE_PICTURE'];?>"/>
                  
                  <!-- condition email !-->
                  <?php if($_SESSION['EMAIL'] == NULL){$type_email="email"; $cond_email="required"; $br_email="<br>";}
                        else{$type_email="hidden"; $cond_email=""; $br_email = "";} ?>
                        <input name="email" type="<?php echo $type_email;?>" class="text" maxlength="30" placeholder="Enter E-mail" <?php echo $cond_email;?> value="<?php echo $_SESSION['EMAIL'];?>"/><?php echo $br_email;?>
                  
                  <!-- condition username !-->
                  <?php if($_SESSION['USERNAME'] == NULL){$type_username="text"; $cond_username="required"; $br_username="<br>";}
                        else{$type_username="hidden"; $cond_username=""; $br_username="";} ?>
                        <input name="username" type="<?php echo $type_username;?>" class="text" maxlength="30" placeholder="Enter Username" <?php echo $cond_username;?> value="<?php echo $_SESSION['USERNAME'];?>"/><?php echo $br_username;?>
        
                  <input name="password" type="password" class="text" maxlength="30" placeholder="Enter password" required="required" />
                  <button class="btn btn-info btn-block" type="submit">Finish</button>
                </td>
              </tr>
             </table>
            </form>
        </div>
    </div>
    <?php else: ?>     <!-- Before sign/login --> 
    <div class="row-fluid">
        <div class="span12">
            <h3>Not Connected</h3>
        </div>
    </div>
    <?php endif ?>
</body>
</html>