<?php 
include("packages/civ_require.php");
include("controller/controller_civ_global.php");
include("controller/controller_civ_login.php");
$curpage='login';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title><?=$seo['title-login'];?></title>
    <meta name="keywords" content="<?=$seo['keyword-login'];?>">
    <meta name="description" content="<?=$seo['desc-login'];?>">
    <?php include("packages/civ_head.php");?>
</head>
<body class="login-body">
    <a href="<?=$global['absolute-url'];?>" class="log-back">< back</a>
    <div class="login-content">
        <div class="logo-pos">
            <a href="<?=$global['absolute-url'];?>">
                <img src="<?=$global['logo-white'];?>" alt="logo">
            </a>
            <span>your anywhere CV & portfolio</span>
        </div>
        <div class="social-warp">
            <a href="http://register.facebook" onclick="this.href='<?=$path['login-with-facebook'];?>'" class="login-facebook">Login With Facebook</a>
            <a href="http://register.google" onclick="this.href='<?=$path['login-with-google'];?>'" class="login-google">Login With Google</a>
            <a href="http://register.linkedin" onclick="this.href='<?=$path['login-with-linkedin'];?>'" class="login-linked">Login With LinkedIn</a>
        </div>
        <div class="space-or">
            <span class="space-border"></span>
            <span class="space-text">OR</span>
            <span class="space-border"></span>
        </div>
        <div class="form-login">
            <form name="login" action="<?=$path['login'];?>?action=login" method="POST" enctype="multipart/form-data" onsubmit="return validateLog();">
                <input id="input-email" type="text" name="email" autocomplete="off" class="form-control input-log marg0" placeholder="email address">
                <div id="error-email" class="is-error"></div>
                <input id="input-password" type="password" name="password" autocomplete="off" class="form-control input-log up1" placeholder="password">
                <div id="error-password" class="is-error"></div>
                <div class="text-center up15">
                    <button type="submit" class="btn btn-log">LOGIN</button>
                </div>
            </form>
            <div class="forgot-link"><a href="<?=$path['forgot-password'];?>">forgot password?</a></div>
        </div>
        <div class="log-signup">
            <div class="log-signup-text">Don't have an account? <a href="<?=$path['register'];?>">Sign Up</a></div>
        </div>
    </div>
    <div id="alert-login" class="register-alert" <?php if($login_status != null || $login_status != ""){ ?> style="display: block;"<?php } ?>>
        <div class="register-card">
            <i class="glyphicon glyphicon-exclamation-sign"></i>
            <?=$login_status;?>
        </div>
    </div>

    <?php if($login_status != null || $login_status != ""){ ?>
    <script type="text/javascript">
      $(document).ready(function() {
          $("#alert-login").delay(5000).fadeOut();
      });
    </script>
    <?php } ?>
    <script type="text/javascript">
        function validateLog() {
            var email = $("#input-email").val();
            var password = $("#input-password").val();
            var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

            if(email != ""){
                if(email.match(mailformat)){
                    $("#error-email").html("");
                    $("#error-email").hide();
                    $("#input-email").removeClass("input-error");
                } else {
                    $("#error-email").show();
                    $("#error-email").html("<i class='glyphicon glyphicon-warning-sign'></i> Your email address must be in the format of name@domain.com");
                    $("#input-email").addClass("input-error");
                    $("#input-email").focus();
                    return false;
                }
            } else {
                $("#error-email").show();
                $("#error-email").html("<i class='glyphicon glyphicon-warning-sign'></i> This field is required.");
                $("#input-email").addClass("input-error");
                $("#input-email").focus();
                return false;
            }
            if(password != ""){
                $("#error-password").html("");
                $("#error-password").hide();
                $("#input-password").removeClass("input-error");
            } else {
                $("#error-password").show();
                $("#error-password").html("<i class='fa fa-warning'></i> This field is required.");
                $("#input-password").addClass("input-error");
                $("#input-password").focus();
                return false;
            }
        }
    </script>
</body>
</html>

