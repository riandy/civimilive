<div id="profile-sidebar" class="hidden-xs job-sidebar up1">
  <div id="top-sidebar">
  <a data-toggle="collapse" onclick="profileNav();" data-parent="#accordion" href="#profile-bar" aria-expanded="true" aria-controls="profile-bar" class="profile-side">
    <img src="<?=$global['absolute-url'];?>img/user.png" alt="user"> <?=$lang['nav-profil'];?> <span id="profile-arrow" class="arrow <?php if ($curpage != "user_profile"){?>nav-close<?php } ?>"></span>
  </a>
  <div id="profile-bar" class="panel-collapse collapse <?php if ($curpage == "user_profile"){?>in<?php } ?>" role="tabpanel">
    <div class="profile-nav">
      <ul>
        <?php if($curpage == 'user_profile'){?>
        <li>
          <a href="#personal-profile" class="profile-link move"><?=$lang['nav-personal'];?></a> 
          <!-- <span id="personal-progress"><?php echo $progress_personal;?></span> -->
          <a href="javascript:;" onclick="tourPersonal();" class="tour-info hidden-sm hidden-md"><span class='glyphicon glyphicon-info-sign info-sign'></span></a>
        </li>
        <li>
          <a href="#summary-profile" class="profile-link move"><?=$lang['nav-summary'];?></a>
          <a href="javascript:;" onclick="tourSummary();" class="tour-info hidden-sm hidden-md"><span class='glyphicon glyphicon-info-sign info-sign'></span></a>
        </li>
        <li>
          <a href="#education-profile" class="profile-link move"><?=$lang['nav-education'];?></a>
          <a href="javascript:;" onclick="tourEducation();" class="tour-info hidden-sm hidden-md"><span class='glyphicon glyphicon-info-sign info-sign'></span></a>
        </li>
        <li>
          <a href="#work-profile" class="profile-link move"><?=$lang['nav-work'];?></a>
          <a href="javascript:;" onclick="tourWork();" class="tour-info hidden-sm hidden-md"><span class='glyphicon glyphicon-info-sign info-sign'></span></a>
        </li>
        <li>
          <a href="#skill-profile" class="profile-link move"><?=$lang['nav-skill'];?></a>
          <a href="javascript:;" onclick="tourSkill();" class="tour-info hidden-sm hidden-md"><span class='glyphicon glyphicon-info-sign info-sign'></span></a>
        </li>
        <li>
          <a href="#language-profile" class="profile-link move"><?=$lang['nav-language'];?></a>
          <a href="javascript:;" onclick="tourLanguage();" class="tour-info hidden-sm hidden-md"><span class='glyphicon glyphicon-info-sign info-sign'></span></a>
        </li>
        <li>
          <a href="#cert-profile" class="profile-link move"><?=$lang['nav-certificate'];?></a>
          <a href="javascript:;" onclick="tourCertificate();" class="tour-info hidden-sm hidden-md"><span class='glyphicon glyphicon-info-sign info-sign'></span></a>
        </li>
        <li>
          <a href="#org-profile" class="profile-link move"><?=$lang['nav-organization'];?> <span class="badge badge-red">NEW</span></a>
          <a href="javascript:;" onclick="tourOrganization();" class="tour-info hidden-sm hidden-md"><span class='glyphicon glyphicon-info-sign info-sign'></span></a>
        </li>
        <li>
          <a href="#org-profile" class="profile-link move"><?=$lang['nav-achievement'];?> <span class="badge badge-red">NEW</span></a>
          <a href="javascript:;" onclick="tourAchievement();" class="tour-info hidden-sm hidden-md"><span class='glyphicon glyphicon-info-sign info-sign'></span></a>
        </li>
        <?php } else { ?>
        <li><a href="<?=$path['user-edit'];?>#personal-profile" class="profile-link move"><?=$lang['nav-personal'];?></a></li>
        <li><a href="<?=$path['user-edit'];?>#summary-profile" class="profile-link move"><?=$lang['nav-summary'];?></a></li>
        <li><a href="<?=$path['user-edit'];?>#education-profile" class="profile-link move"><?=$lang['nav-education'];?></a></li>
        <li><a href="<?=$path['user-edit'];?>#work-profile" class="profile-link move"><?=$lang['nav-work'];?></a></li>
        <li><a href="<?=$path['user-edit'];?>#skill-profile" class="profile-link move"><?=$lang['nav-skill'];?></a></li>
        <li><a href="<?=$path['user-edit'];?>#language-profile" class="profile-link move"><?=$lang['nav-language'];?></a></li>
        <li><a href="<?=$path['user-edit'];?>#cert-profile" class="profile-link move"><?=$lang['nav-certificate'];?></a></li>
        <li><a href="<?=$path['user-edit'];?>#org-profile" class="profile-link move"><?=$lang['nav-organization'];?> <span class="badge badge-red">NEW</span></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  </div>
  <div id="bottom-sidebar">
  <a data-toggle="collapse" onclick="profileNav2();" data-parent="#accordion" href="#profile-bar2" aria-expanded="true" aria-controls="profile-bar2" class="profile-side">
    <img src="<?=$global['absolute-url'];?>img/portfolio.png" alt="user"> <?=$lang['nav-portfolio'];?> <span id="profile-arrow2" class="arrow"></span>
  </a>
  <div id="profile-bar2" class="panel-collapse collapse in" role="tabpanel">
    <div class="profile-nav">
      <ul>
        <li>
          <a href="<?=$path['user-portfolio'];?>" class="profile-link"><?=$lang['nav-image'];?></a>
          <?php if($curpage == 'user_portfolio'){?>
          <!-- <a href="javascript:;" onclick="tourPortfolio();" class="tour-info hidden-sm hidden-md"><span class='glyphicon glyphicon-info-sign info-sign'></span></a> -->
          <?php } ?>
        </li>
        <li>
          <a href="<?=$path['user-video'];?>" class="profile-link"><?=$lang['nav-video'];?></a>
          <?php if($curpage == 'user_video'){?>
          <!-- <a href="javascript:;" onclick="tourVideo();" class="tour-info hidden-sm hidden-md"><span class='glyphicon glyphicon-info-sign info-sign'></span></a> -->
          <?php } ?>
        </li>
        <li>
          <a href="<?=$path['user-doc'];?>" class="profile-link"><?=$lang['nav-document'];?></a>
          <?php if($curpage == 'user_doc'){?>
          <!-- <a href="javascript:;" onclick="tourDoc();" class="tour-info hidden-sm hidden-md"><span class='glyphicon glyphicon-info-sign info-sign'></span></a> -->
          <?php } ?>
        </li>
        <li>
          <a href="<?=$path['user-web'];?>" class="profile-link"><?=$lang['nav-web'];?></a>
          <?php if($curpage == 'user_web'){?>
          <!-- <a href="javascript:;" onclick="tourWeb();" class="tour-info hidden-sm hidden-md"><span class='glyphicon glyphicon-info-sign info-sign'></span></a> -->
          <?php } ?>
        </li>
      </ul>
    </div>
  </div>
  </div>
  <!--<a data-toggle="collapse" onclick="profileNav3();" data-parent="#accordion" href="#profile-bar3" aria-expanded="true" aria-controls="profile-bar3" class="profile-side">
    <img src="<?=$global['absolute-url'];?>img/portfolio.png" alt="user"> Setting <span id="profile-arrow3" class="arrow <?php if ($curpage != "user_setting"){?>nav-close<?php } ?>"></span>
  </a>
  <div id="profile-bar3" class="panel-collapse collapse <?php if ($curpage == "user_setting"){?>in<?php } ?>" role="tabpanel">
    <div class="profile-nav">
      <ul>
        <li><a href="<?=$path['user-setting-privacy'];?>" class="profile-link">Privacy</a></li>
        <li><a href="<?=$path['user-setting-order'];?>" class="profile-link">Order</a></li>
      </ul>
    </div>
  </div>!-->
</div>

<script type="text/javascript">
  function profileNav(){
    $('#profile-arrow').toggleClass('nav-close');
  }
  function profileNav2(){
    $('#profile-arrow2').toggleClass('nav-close');
  }
  function profileNav3(){
    $('#profile-arrow3').toggleClass('nav-close');
  }
  $('.move').click(function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {

        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
        if (target.length) {
            $('html,body').animate({
                scrollTop: target.offset().top-42
            }, 1000);
            return false;
        }
    }
  });
</script>