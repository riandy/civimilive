<?php 

//check session
if(!isset($_SESSION['userData']['id'])){
	// header("Location:".$global['absolute-url']."?message=please-login-first");
}

//logging out
if(isset($_GET['action'])){
	if($_GET['action']== "logout" && is_array($_SESSION['userData'])){
		//set end data session
		end_session();
		//set session data to an empty array
		$_SESSION = array();
		//expire their cookie files
		if(isset($_COOKIE['cookie_id'])){
			end_cookie();
		}
		//destroy the session variables
		session_destroy();
		//double check to see if thier sessions exists
		if(isset($_SESSION['userData']['id'])){
			$location = $global['absolute-url']."?msg=Error:_Logout_Failed";
			header("Location:".$location);
		}else{
			header("Location:{$global['absolute-url']}");
			exit();
		}
	}
	else if($_GET['action']== "testimoni"){
		function send_email($tujuan, $testimoni, $username, $email){
        //mail
        $to = $tujuan; 
        $subject = "Testimonial From User ".$username."";

        $message = "
        <!DOCTYPE html>
        <html>
        <head>
        <title></title>
        </head>
        <body>
        <span style='font-size:14px;'>Testimonial From User $username</span>
        <br/><br/>
        <b>Email User</b> : $email
        <br/><br/>
        <b>Testimoni</b> : ".str_replace('\r\n','<br/>',$testimoni)."
        <br/><br/><br/>
        <em>*Do not reply to this e-mail.<br/><br/>Thank you!</em>
        </body>
        </html>
        ";


        // Always set content-type when sending HTML email
        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

        // More headers
        $headers .= "From: <donotreply@civimi.com>" . "\r\n";

        $result = mail($to,$subject,$message,$headers);
        return $result; 
        }

        $O_testimoni = $_POST['testimoni'];
        $O_username = $_POST['username'];
        $O_userEmail = $_POST['email'];
        $O_page = $_POST['url'];
        $O_to = "support@civimi.com";

        $result = send_email( $O_to, $O_testimoni, $O_username, $O_userEmail);
        $alert_status = "success";

        $_SESSION['testimonistatus'] = $alert_status;
        header("Location:$O_page");
	}
}

if(isset($_COOKIE['cookie_id']) && !isset($_SESSION['userData']['id'])){ //if login check me out
	//set cookie in session
	$_SESSION['userData']['id'] = $_COOKIE['cookie_id'];
   	$_SESSION['userData']['username'] = $_COOKIE['cookie_username'];
    $_SESSION['userData']['auth_code'] = $_COOKIE['cookie_auth_code'];
    $_SESSION['userData']['email'] = $_COOKIE['cookie_email'];
}

?>