<?php 
require_once("model/Connection.php");
$obj_con = new Connection();

require_once("model/Ref_Field.php");
$obj_rfield = new Ref_Field();

require_once("model/Ref_Level.php");
$obj_rlvl = new Ref_Level();

require_once("model/Ref_City.php");
$obj_rcity = new Ref_City();

require_once("model/User.php");
$obj_user = new User();

require_once("model/Edu.php");
$obj_edu = new Edu();

require_once("model/News.php");
$obj_news = new News();

if(!isset($_GET['action'])){
	$obj_con->up();

	$sub = "id";
	if(isset($_SESSION['sub'])){
		$sub = $_SESSION['sub'];
	}

	$data_fields = $obj_rfield->get_index();
	$data_levels = $obj_rlvl->get_index();
	$data_citys = $obj_rcity->get_index();
	$data_feats = $obj_user->get_featured_user();
	$data_pops = $obj_user->get_popular_user();
	$data_edus = $obj_edu->get_popular_univ($sub);
	$data_news = $obj_news->get_index_news();
	//var_dump($data_feats);
	//var_dump($data_pops);

	if(!isset($_GET['msg'])){unset($_SESSION['statusLogin']);} //remove session
	$obj_con->down();
} else {
	if($_GET['action'] == 'feedback' && isset($_POST['feedback_name']) && $_POST['feedback_email'] != '' && isset($_POST['feedback_message']) && $_POST['feedback_captcha'] != '' && $_POST['feedback_captcha'] == $_SESSION['captcha_session'] ){
		$N_name = mysql_real_escape_string(check_input($_POST['feedback_name']));
		$N_email = mysql_real_escape_string(check_input($_POST['feedback_email']));
		$N_message = mysql_real_escape_string(($_POST['feedback_message']));
		$N_captcha = mysql_real_escape_string(check_input($_POST['feedback_captcha']));
		$N_url = mysql_real_escape_string(check_input($_POST['feedback_url']));
		$N_to = "support@civimi.com";

		function send_feedback($tujuan, $name, $email, $url, $pesan){
	    //mail
		$result = 0;
	    $to = $tujuan; 
	    $subject = "Feedback Message from $email";

	    $message = "
	    <!DOCTYPE html>
	    <html>
	    <head>
	    <title></title>
	    </head>
	    <body>
	    <b>From Page</b> : $url
		<br/><br/>
	    <b>Name</b> : $name
		<br/><br/>
		<b>E-mail</b> : $email
		<br/><br/>
		<b>Message</b> : 
		<br/>
		".str_replace('\r\n','<br/>',$pesan)."

		<br/><br/>
		<span style='font-size:12px;'>Please Follow up the message.</span><br/>
		<span style='font-size:12px;font-style: italic;'><em>*This message is auto generated. Please do not reply this email</em></span>
	    </body>
	    </html>
	    ";


	    // Always set content-type when sending HTML email
	    $headers = "MIME-Version: 1.0" . "\r\n";
	    $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

	    // More headers
	    $headers .= "From: <support@civimi.com>" . "\r\n";

	    $result = mail($to,$subject,$message,$headers);
	    return $result;	
	    }

		if($N_captcha == $_SESSION['captcha_session']){
			
			if(isset($_SESSION['feedback_name'])){ unset($_SESSION['feedback_name']); }
	        if(isset($_SESSION['feedback_email'])){ unset($_SESSION['feedback_email']); }
	        if(isset($_SESSION['feedback_message'])){ unset($_SESSION['feedback_message']); }

	        $result = send_feedback($N_to, $N_name, $N_email, $N_url, $N_message);
			if($result != 0){
				$alert = "Thank you for your feedback<br/>We will get back to you as soon as possible!";
				$_SESSION['feedback_alert'] = $alert;
			}else{
				$alert = "Send Feedback Failed, please try again!";
				$_SESSION['feedback_alert'] = $alert;
			}
		}else{
			$_SESSION['feedback_name'] = $N_name;
	        $_SESSION['feedback_email'] = $N_email;
	        $_SESSION['feedback_message'] = $N_message;
			$alert = "Failed to send feedback! Please insert the correct captcha!";
			$_SESSION['feedback_alert'] = $alert;
		}

	
	header("Location:".$N_url);
	$obj_con->down();
	}
}
?>