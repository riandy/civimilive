<?php 
require_once("model/Connection.php");
$obj_con = new Connection();

require_once("model/News.php");
$obj_news = new News();

require_once("model/News_Category.php");
$obj_nc = new News_Category();

if(isset($_GET['id']) && $_GET['id'] != "" && isset($_GET['title']) && $_GET['title'] != ""){

	$obj_con->up();

	$O_id = mysql_real_escape_string(check_input($_GET['id'])); //id
    $datas = $obj_news->get_by_id($O_id); //GET DATA
    $data_photos = $obj_news->get_image_id($O_id); //GET DATA PHOTO
    $cat_id = $datas[0]['Nc_ID'];
    $data_relateds = $obj_news->get_by_related($O_id,$cat_id);

	$obj_con->down();

} else { 
    header("Location:{$path['home']}");
}
?>