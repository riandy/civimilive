<?php 

require_once("model/Connection.php");
$obj_con = new Connection();

require_once("model/Setting.php");
$obj_set = new Setting();

if(!isset($_GET['action'])){
	error_reporting(E_ALL^E_NOTICE); //remove notice
	$obj_con->up();

	$message = $_SESSION['status'];
	unset($_SESSION['status']);
	$datas = $obj_set->get_data_edit($_SESSION['userData']['id']);
	//start user setting
	if(is_array($datas)){
		$O_obj = $datas[0]['Setting_objSort'];
		$O_edu = $datas[0]['Setting_eduSort'];
		$O_exp = $datas[0]['Setting_expSort'];
		$O_skill = $datas[0]['Setting_skillSort'];
		$O_work = $datas[0]['Setting_workSort'];
		$O_resumeCode = $datas[0]['Setting_resumeCode'];
		$O_resume = $datas[0]['Setting_resume'];
		$O_portfolio = $datas[0]['Setting_portfolio'];
		$O_portfolioPage = $datas[0]['Setting_portfolioPage'];
	}else{
		$O_obj = 1;
		$O_edu = 2;
		$O_exp = 3;
		$O_skill = 4;
		$O_work = 5;
		$O_resumeCode = "";
		$O_resume = 0; //publish
		$O_portfolio = 0; //publish
		$O_portfolioPage = "Photo";
	}
	//end user setting

	$obj_con->down();
}
else if($_GET['action'] == 'set'){
	$obj_con->up();

	$N_user_id = $_SESSION['userData']['id'];
	$N_obj = mysql_real_escape_string(check_input($_POST['objective']));
	$N_edu = mysql_real_escape_string(check_input($_POST['education']));
	$N_exp = mysql_real_escape_string(check_input($_POST['experience']));
	$N_skill = mysql_real_escape_string(check_input($_POST['skill']));
	$N_work = mysql_real_escape_string(check_input($_POST['work']));
	$N_resumeCode = mysql_real_escape_string(check_input($_POST['resume_code']));
	$N_portfolioPage = mysql_real_escape_string(check_input($_POST['portfolio_page']));
	if(isset($_POST['resume'])){
		$N_resume = mysql_real_escape_string(check_input($_POST['resume']));
	}else{
		$N_resume = "1";
	}
	if(isset($_POST['portfolio'])){
		$N_portfolio = mysql_real_escape_string(check_input($_POST['portfolio']));
	}else{
		$N_portfolio = "1";
	}

	$result = $obj_set->process_data($N_user_id, $N_obj, $N_edu, $N_exp, $N_skill, $N_work, $N_resume, $N_resumeCode, $N_portfolio, $N_portfolioPage);
	if($result == 1){
		$message = "Setting order success";
	}else{
		$message = "Setting order failed";
	}

	$_SESSION['status'] = $message;
	header("Location:{$path['user-setting-order']}");
	$obj_con->down();
}
?>