<?php
//include("packages/front_config.php");
require_once("model/Connection.php");
$obj_con = new Connection();

require_once("model/User.php");
$obj_user = new User();

require_once("model/Setting.php");
$obj_set = new Setting();

require_once("model/AT_Progress.php");
$obj_atpro = new AT_Progress();

require_once("model/Ref_Country.php");
$obj_rcountry = new Ref_Country();

error_reporting(E_ALL^E_NOTICE); //remove notice
//for reinvent_email
$reinvent_email = "";
if(isset($_POST['email'])){
	$reinvent_email = $_POST['email'];
}

if(!isset($_GET['action'])){
	$obj_con->up();
	$message = "";
	//$data_country = $obj_rcountry->get_index();
	//var_dump($data_country);
	$obj_con->down();
}else{
	$dbh_con = $obj_con->up();
	
	if($_GET['action'] == 'reg'){

		$N_username = mysql_real_escape_string(check_input($_POST['username']));
		$lower_username = strtolower($N_username);
		$N_email = mysql_real_escape_string(check_input($_POST['email']));
		$N_password = mysql_real_escape_string(check_input($_POST['password']));
		$N_re_password = mysql_real_escape_string(check_input($_POST['re_password']));
		$success = 0;

		//start for activation number
		$length = 64; //determine the length
		$random = crypt(uniqid(rand(),1)); //generate a random id. Encript it and set it as $random
		$random_no_slashes = strip_tags(stripslashes($random)); //if you want to remove any slashes
		$random_id = str_replace(".", "", $random_no_slashes); //remove any .
		$random_final = strrev(str_replace("/", "", $random_id)); //and or reversing the string
		$random_code = substr($random_final, 0, $length); //take the first characters from $random_final
		//end set for activation number

		$check_email = $obj_user->check_email($N_email);
		if($check_email == 1){
			$result = $obj_user->insert_data($lower_username, $N_email, $N_password, $random_code);
			// var_dump($result);
			if($result == 1){
				//create session for send email
				$_SESSION['statusEmail'] = $N_email;
				$_SESSION['statusUsername'] = $lower_username;
				$_SESSION['statusCode'] = $random_code;
				header("Location:{$path['success-register']}");
			}else{
				$message = "Register account failed.";
			}
		}else if($check_email == 0 && $N_email != ""){
			$message = "Email already exists, Please use another email.";
		}else{
			$message = "Register account failed.";
		}
	}
	else if($_GET['action'] == 'activate' && $_GET['username'] != '' && $_GET['actNum'] != ''){
		$O_username = mysql_real_escape_string(check_input($_GET['username']));
		$O_act_num = mysql_real_escape_string(check_input($_GET['actNum']));

		$result = $obj_user->update_act_status($O_username, $O_act_num);
		//var_dump($result);
		if($result == 1){
			$check = $obj_user->check_primary_username($O_username); // check username
	        //var_dump($check);
			if(is_array($check)){
				$users = $obj_user->user_login($check[0]['User_email'], $check[0]['User_password']);
				if(is_array($users)){
	                create_session($users[0]['User_ID'], $users[0]['User_username'], $users[0]['User_auth_code'], $users[0]['User_email']);
					$obj_set->insert_data($users[0]['User_ID'], 1, 2, 3, 4, 5, 6, 7); //insert setting sort
					$obj_atpro->insert_data($users[0]['User_ID'], 0, 0, 0, 0, 0, 0, 0, 0); //insert progress
				}
				$message = "Account activate";
				header("Location:{$path['form-goal']}");
				//remove session if send email & active account success
				unset($_SESSION['statusEmail']);
				unset($_SESSION['statusUsername']);
				unset($_SESSION['statusCode']);
			}else{
				$message = "Account cannot login";
			}
		}else{
			$message = "Activation account has been failed";
		}
	}
	$obj_con->down($dbh_con);
}
?>