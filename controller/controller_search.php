<?php 

require_once("model/Connection.php");
$obj_con = new Connection();

require_once("model/Jobs.php");
$obj_job = new Jobs();

require_once("model/Query_job.php");
$obj_qjob = new Query_job();

require_once("model/Ref_City.php");
$obj_rcity = new Ref_City();

require_once("model/Ref_Level.php");
$obj_rlvl = new Ref_Level();

require_once("model/Ref_Field.php");
$obj_rfield = new Ref_Field();

require_once("model/Industry.php");
$obj_industry = new Industry();

require_once("model/Company.php");
$obj_comp = new Company();

if(!isset($_GET['action'])){
	$obj_con->up();
	
	$data_citys = $obj_rcity->get_index();
	$data_levels = $obj_rlvl->get_index();
	$data_fields = $obj_rfield->get_index();
	$data_industrys = $obj_industry->get_index();

	$O_userID = 0;
	if(isset($_SESSION['userData']['id'])){
		$O_userID = $_SESSION['userData']['id'];
	}

	$O_page = 1;
	if(isset($_GET['page'])){
		$O_page = mysql_real_escape_string(check_input($_GET['page']));
	}

	$O_keyword = "";
	if(isset($_GET['q'])){
		$O_keyword = mysql_real_escape_string(check_input($_GET['q']));
	}

	$O_company = "";
	if(isset($_GET['company'])){
		$O_company = mysql_real_escape_string(check_input($_GET['company']));
	}

	$O_location = "";
	if(isset($_GET['location'])){
		$O_location = mysql_real_escape_string(check_input($_GET['location']));
	}

	$O_expertise = "";
	if(isset($_GET['expertise'])){
		$O_expertise = mysql_real_escape_string(check_input($_GET['expertise']));
	}

	$O_function = "";
	if(isset($_GET['function'])){
		$O_function = mysql_real_escape_string(check_input($_GET['function']));
	}

	$O_industry = "";
	if(isset($_GET['industry'])){
		$O_industry = mysql_real_escape_string(check_input($_GET['industry']));
	}

	$O_exp = "";
	if(isset($_GET['experience'])){
		$O_exp = mysql_real_escape_string(check_input($_GET['experience']));
	}

	$sort = 'weight';
	if(isset($_GET['sort'])){
		$sort = mysql_real_escape_string(check_input($_GET['sort']));
	}

	//REQUEST API WIKIPEDIA
	if($O_keyword != ''){
		$filename = "http://en.wikipedia.org/w/api.php?action=query&prop=extracts&format=json&exintro=&exsectionformat=wiki&indexpageids=&iwurl=&titles={$O_keyword}";
		$json = json_decode(file_get_contents($filename), TRUE);
		if($json){
			$pageids = $json['query']['pageids'][0];
			if($pageids == "-1"){
				$J_content = "<h5><strong>Keyword not found</strong></h5>";
			}else{
				$J_content = $json['query']['pages'][$pageids]['extract'];
			}
		}else{
			$J_content = "<h3><strong>Keyword is empty</strong></h3>";
		}
	}

	//GET RESULT
	if($O_exp != ''){$exp = $O_exp." years";}else{$exp = "";} //for insert query job
	if($O_keyword != '' || $O_company != '' || $O_location != '' || $O_expertise != '' || $O_function != '' || $O_industry != '' || $O_exp != ''){
		//$data_results = $obj_job->get_result($O_keyword, $O_company, $O_location, $O_expertise, $O_function, $O_industry, $O_exp, $sort);
		$data_results = $obj_job->get_result($O_page, $O_keyword, $O_company, $O_location, $O_expertise, $O_function, $O_industry, $O_exp, $sort);
		$data_companys = $obj_comp->get_data_edit($O_company); //for company content
		$data_field_contents = $obj_rfield->get_data_edit($O_function); //for field content
		//var_dump($data_results);
		$total_result = $obj_job->get_total_result($O_keyword, $O_company, $O_location, $O_expertise, $O_function, $O_industry, $O_exp);
		//var_dump($total_result);
		//var_dump($data_results);
	}else{
		$data_results = ""; //for job result null
		$data_companys = ""; //for company content null
		$data_field_contents = ""; //for field content null
	}

	//for insert query job
	if(is_array($data_results)){
		if($O_location != ''){$location = $data_results[0]['City_title'];}else{$location = "";}
		if($O_function != ''){$field = $data_results[0]['Field_title'];}else{$field = "";}
		if($O_expertise != ''){$level = $data_results[0]['Level_title'];}else{$level = "";}
	}else{
		$location = "";
		$field = "";
		$level = "";
	}

	$data_recents = $obj_qjob->get_recent(); //get recent search

	$ip = $_SERVER['REMOTE_ADDR']; //check ip address
		//$ip = "216.239.51.99"; //ip google
		$filename = "http://freegeoip.net/json/".$ip; //request API in freegoip
		$json = json_decode(file_get_contents($filename), TRUE);
		$J_city = $json['city'];
		$J_country = $json['country_name'];

	if($O_keyword != ''){ //if keyowd not null
		//SAVE QUERY JOB
		$save_query = $obj_qjob->insert_data($O_keyword, $ip, $J_city, $J_country, $location, $field, $level, $exp, $O_userID);
	}

	$obj_con->down();
}

?>