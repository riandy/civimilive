<?php 

require_once("model/Connection.php");
$obj_con = new Connection();

require_once("model/User.php");
$obj_user = new User();

require_once("model/Third_party.php");
$obj_tp = new Third_party();

require_once("model/Ref_Country.php");
$obj_rcountry = new Ref_Country();

if(!isset($_GET['action'])){
	error_reporting(E_ALL^E_NOTICE); //remove notice
	$obj_con->up();

	$message = $_SESSION['message'];
	unset($_SESSION['message']);

	if(isset($_SESSION['userData'])){
		//get data by user id
		if(isset($_SESSION['userData']['id'])){
			$datas = $obj_user->get_data_detail($_SESSION['userData']['id']);
			//var_dump($datas);
		}
	}
	$data_country = $obj_rcountry->get_index();
	//var_dump($data_country);

	$obj_con->down();
}else if($_GET['action'] == 'update'){
	$obj_con->up();

	$N_id = mysql_real_escape_string(check_input($_POST['id']));
	$N_auth_code = mysql_real_escape_string(check_input($_POST['auth_code']));
	$N_fname = mysql_real_escape_string(check_input($_POST['fname']));
	$N_lname = mysql_real_escape_string(check_input($_POST['lname']));
	$N_username = mysql_real_escape_string(check_input($_POST['username']));
	$N_email = mysql_real_escape_string(check_input($_POST['email']));
	$N_birthday1 = mysql_real_escape_string(check_input($_POST['birthday1']));
	$N_birthday2 = mysql_real_escape_string(check_input($_POST['birthday2']));
	$N_birthday3 = mysql_real_escape_string(check_input($_POST['birthday3']));
	$N_location = mysql_real_escape_string(check_input($_POST['location']));
	$N_city = mysql_real_escape_string(check_input($_POST['city']));
	$N_state = mysql_real_escape_string(check_input($_POST['state']));
	$N_phone = mysql_real_escape_string(check_input($_POST['phone']));

	$message = "";
	if($_POST['via'] != ""){ //update user via third party
		$check_username = $obj_tp->check_username($N_username);	
		if($check_username == 1){
			$result = $obj_tp->update_form_personal($N_id, $N_fname, $N_lname, $N_username, $N_email, $N_birthday1, $N_birthday2, $N_birthday3, $N_location, $N_city, $N_state, $N_phone);
			//var_dump($result);
			if($result == 1){
		        if(!isset($_SESSION['userData']['auth_code'])){
		          $_SESSION['userData']['auth_code'] = $N_auth_code; //session auth_code 
		        }
		        if($_POST['via'] != "Twitter"){
		        	if(!isset($_SESSION['userData']['username'])){
		          		$_SESSION['userData']['username'] = $N_username; //session username  
		        	}
		        }else{
		        	if(!isset($_SESSION['userData']['email'])){
		          	    $_SESSION['userData']['email'] = $N_email; //session username  
		        	}
		        }
				$location = $path['form-industry'];
			}else{
				$location = $path['form-personal'];
				$message = "Update form personal account failed.";
			}
		}else if($check_username == 0){
			$location = $path['form-personal'];
			$message = "Username already exists, Please use another username.";
		}else{
			$location = $path['form-personal'];
			$message = "Update form personal account failed.";
		}
	}else{ //update user via normal
		$result = $obj_user->update_form_personal($N_id, $N_fname, $N_lname, $N_birthday1, $N_birthday2, $N_birthday3, $N_location, $N_city, $N_state, $N_phone);
		//var_dump($result);
		if($result == 1){
			$location = $path['form-industry'];
		}else{
			$location = $path['form-personal'];
			$message = "Update form personal account failed.";
		}
	}
	
	if($message != ""){
		$_SESSION['message'] = $message;
	}
	header("Location:".$location);
	$obj_con->down();
}
?>