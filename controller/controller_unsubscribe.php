<?php 

require_once("model/Connection.php");
$obj_con = new Connection();

require_once("model/User.php");
$obj_user = new User();

if(!isset($_GET['action'])){
	error_reporting(E_ALL^E_NOTICE); //remove notice
	
	$message = $_SESSION['message'];
	$email = $_SESSION['email'];
	$color = $_SESSION['color'];
	unset($_SESSION['message']);
	unset($_SESSION['email']);
	unset($_SESSION['color']);

}else if($_GET['action'] == 'update'){
	$obj_con->up();

	$N_email = mysql_real_escape_string(check_input($_POST['email']));
	$message = "";

	$check_email = $obj_user->check_primary_email($N_email);
	if(is_array($check_email)){ //update user via third party
		$result = $obj_user->update_unsubscribe($check_email[0]['User_ID']);	
		if($result == 1){
			$email = "";
			$color = "#2be94a";
		    $message = "Unsubscribe Success, you will no longer receive newsletter email from us!";
		}else{
			$email = $N_email;
			$color = "#a94442";
			$message = "Unsubscribe failed, try again!";
		}
	}else{
		$email = $N_email;
		$color = "#a94442";
		$message = "Something is wrong. Maybe your e-mail does not exist in our system.";
	}
	
	$location = $path['unsubscribe'];
	$_SESSION['message'] = $message;
	$_SESSION['email'] = $email;
	$_SESSION['color'] = $color;

	header("Location:".$location);
	$obj_con->down();
}
?>