<?php
error_reporting(E_ALL^E_NOTICE); //remove notice

require_once("model/Connection.php");
$obj_con = new Connection();

require_once("model/User.php");
$obj_user = new User();

if(!isset($_GET['action'])){
	$obj_con->up();

	$message = "";
	//$_SESSION['message'] = null;
	//$message = $_SESSION['message'];
    
	$obj_con->down();
}
else if($_GET['action'] == 'reset' && $_GET['email'] != '' && $_GET['code'] != ''){
	$obj_con->up();
	$message = "";

	$O_email = mysql_real_escape_string(check_input($_GET['email']));
	$O_code = mysql_real_escape_string(check_input($_GET['code']));

	$result = $obj_user->check_reset_password($O_email, $O_code);
	if($result != 1){
		echo "<script>alert('Incorrect E-mail or code is expired')</script>";
		echo "<meta http-equiv='refresh' content='0;url={$path['reset-password']}'>";
	}

	$obj_con->down();
}
else if($_GET['action'] == 'newpass'){
	$obj_con->up();

	$N_email = mysql_real_escape_string(check_input($_POST['email']));
	$N_code = mysql_real_escape_string(check_input($_POST['code']));
	$N_password = mysql_real_escape_string(check_input($_POST['password']));
	$N_re_password = mysql_real_escape_string(check_input($_POST['re_password']));

	if($N_password == $N_re_password){
		$result = $obj_user->reset_password($N_email, $N_password, $N_code);
		if($result == 1){
			echo "<script>alert('Password successfully updated! Please login using the new password!')</script>";
			echo "<meta http-equiv='refresh' content='0;url={$global['base']}'>";
		}else{
			echo "<script>alert('Password failed to be updated. Something is wrong')</script>";
			echo "<meta http-equiv='refresh' content='0;url={$path['reset-password-action']}/$N_email/$N_code'>";
		}
	}else{
		echo "<script>alert('Password does not match')</script>";
		echo "<meta http-equiv='refresh' content='0;url={$path['reset-password-action']}/$N_email/$N_code'>";
	}	

	$obj_con->down();
}

?>