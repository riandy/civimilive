<?php

require_once("model/Connection.php");
$obj_con = new Connection();

require_once("model/Jobs.php");
$obj_job = new Jobs();

require_once("model/Query_job.php");
$obj_qjob = new Query_job();

require_once("model/Tagging.php");
$obj_tagging = new Tagging();

require_once("model/Hits.php");
$obj_hits = new Hits();

require_once("model/News.php");
$obj_news = new News();

require_once("model/AT_Jobs_Application.php");
$obj_japp = new AT_Jobs_Application();

if(!isset($_GET['action'])){
	$obj_con->up();

	error_reporting(E_ERROR | E_WARNING | E_PARSE);
	//error_reporting(E_ALL^E_NOTICE);

    $obj_con->down();
}
else if($_GET['action'] == 'job_detail' && $_GET['jobs_id'] != ''){
	$obj_con->up();

    $O_userID = 0;
    if(isset($_SESSION['userData']['id'])){
        $O_userID = $_SESSION['userData']['id'];
    }
	// GET DATA DETAIL
	$O_id = mysql_real_escape_string(check_input($_GET['jobs_id']));
	$data_jobss = $obj_job->get_data_detail($O_id);

	// GET RELATED JOB BY COMPANY
	if(is_array($data_jobss)){
		$O_companyID = $data_jobss[0]['Jobs_ref_companyID'];
		$data_related_companys = $obj_job->get_related_by_company($O_id, $O_companyID);
	}
	$data_recents = $obj_qjob->get_recent(); //get recent search
	// GET RELATED JOB BY TAG
	$N_table = "jobs";
	$data_tags = $obj_tagging->get_tags($N_table, $O_id); 
	$tag = "";
	if(is_array($data_tags)){
		if(count($data_tags) > 0){
			for($i = 0; $i < count($data_tags); $i++){
				if($i >= 1){
					$tag .= ",";
				}
				$tag .= $data_tags[$i]['Tag_ID'];
			}
		}
		$data_related_tags = $obj_job->get_related_by_tag($O_id, $tag);
        $data_related_news = $obj_news->get_related_by_tag($tag);
	}else{
	    $data_related_tags = "";
        $data_related_news = "";
	}

	$ip = $_SERVER['REMOTE_ADDR']; //check ip address
	$filename = "http://freegeoip.net/json/".$ip; //request API in freegoip
	$json = json_decode(file_get_contents($filename), TRUE);
	$J_city = $json['city'];
	$J_country = $json['country_name'];

	$obj_hits->insert($ip, $J_city, $J_country, $O_id, $data_jobss[0]['Jobs_ref_companyID'],$data_jobss[0]['Company_ref_industryID'],$data_jobss[0]['Jobs_ref_fieldID'],$data_jobss[0]['Jobs_ref_levelID'], $O_userID);	
	$data_hits = $obj_hits->get_total('jobs', $O_id);
	
	$obj_con->down();
}
else if($_GET['action'] == 'apply'){ 

    $obj_con->up();

    $N_jobID = mysql_real_escape_string(check_input($_POST['job_id']));
    $N_userID = mysql_real_escape_string(check_input($_POST['user_id']));
    $N_jobTitle = mysql_real_escape_string(check_input($_POST['job_title']));
    $N_companyTitle = mysql_real_escape_string(check_input($_POST['company_title']));
    $N_name = mysql_real_escape_string(check_input($_POST['name']));
    $N_email = mysql_real_escape_string(check_input($_POST['email']));
    $N_phone = mysql_real_escape_string(check_input($_POST['phone']));
    $N_message = mysql_real_escape_string(check_input($_POST['message']));
    $N_captcha = $_POST['captcha_text'];
   
    $mpath = $path['job-detail'].encode($N_jobTitle)."_".$N_jobID.".html"; //link for redirect

    if($N_captcha == $_SESSION['captcha_session']){

        $ran = rand();
        $timestamp = time();
        //$fileLink = "../admin/uploads/applicant/" . $timestamp . $ran . cleanSpace($_FILES["cv"]["name"]);
        $fileLink = "../admin/uploads/applicant/" . $timestamp . $ran . $_FILES["cv"]["name"];
        $fileLink1 = "admin/uploads/applicant/" . $timestamp . $ran . $_FILES["cv"]["name"]; //for save link in db
        $fullFileLink = $global['base'].$fileLink1;

        if (($_FILES["cv"]["name"] != null && $_FILES["cv"]["name"] != '')) {
            if ($_FILES["cv"]["error"] > 0) {
                //echo "Return Code: " . $_FILES["file"]["error"][$k] . "<br />";
            } else {
                $file = $_FILES["cv"]["tmp_name"];
                if (move_uploaded_file($file, $fileLink)) {
                    $result = $obj_japp->insert($N_jobID, $N_userID, $N_email, $N_phone, $fileLink1, $N_message);
                    if($result){                      
                        $result_email = $obj_japp->send_email($N_jobTitle, $N_companyTitle, $N_name, $N_email, $N_phone, $N_message, $fullFileLink);
                        echo "<script>window.alert('Job has been Apply!Please give us a moment to get back to you!')
                            window.location='$mpath';</script>";
                    } else {
                        echo "<script>window.alert('Job failed to Apply!Please check your input!')
                            window.location='$mpath';</script>";
                    }
                } else {
                    echo "<script>window.alert('Job failed to Apply!Please check your file input!')
                            window.location='$mpath';</script>";
                }
            }
        }
    }  else {
        echo "<script>window.alert('Failed to apply job!Please insert the correct captcha!')
                window.location='$mpath';</script>";
    }
    $obj_con->down();
}

?>