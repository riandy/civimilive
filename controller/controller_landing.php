<?php

require_once("model/Connection.php");
$obj_con = new Connection();

require_once("model/Third_party.php");
$obj_tp = new Third_party();

if(!isset($_GET['action'])){
	$obj_con->up();

	error_reporting(E_ERROR | E_WARNING | E_PARSE);
	
	$obj_con->down();
}
else if($_GET['action'] == 'finish'){
	$obj_con->up();

	$N_fname = mysql_real_escape_string(check_input($_POST['fname']));
	$N_lname = mysql_real_escape_string(check_input($_POST['lname']));
	$N_city = mysql_real_escape_string(check_input($_POST['city']));
	$N_country = mysql_real_escape_string(check_input($_POST['country']));
	$N_provider = mysql_real_escape_string(check_input($_POST['provider']));
	$N_picture = mysql_real_escape_string(check_input($_POST['picture']));
	$N_username = mysql_real_escape_string(check_input($_POST['username']));
	$N_email = mysql_real_escape_string(check_input($_POST['email']));
	$N_password = mysql_real_escape_string(check_input(md5($_POST['password'])));
	$N_birthday1 = mysql_real_escape_string(check_input($_POST['birthday1']));
	$N_birthday2 = mysql_real_escape_string(check_input($_POST['birthday2']));
	$N_birthday3 = mysql_real_escape_string(check_input($_POST['birthday3']));

	$result_create = $obj_tp->insert_data($N_fname, $N_lname, $N_city, $N_country, $N_username, $N_email, $N_password, $N_birthday1, $N_birthday2, $N_birthday3, $N_provider, $N_picture, $N_picture);
	if($result_create == 1){
		$result = $obj_tp->user_login($N_email, $N_password);
	    if(is_array($result)){
	    	$obj_tp->remove_session(); //for session in landing.php
	    	//create cookie for login
	        create_cookie($result[0]['User_ID'], $result[0]['User_username'], $result[0]['User_auth_code'], $result[0]['User_email']);
			$location = "user_profile.php";
	    } 
	}else{
		$location = "register.php";
	}

	header("Location:".$location);
	$obj_con->down();
}
?>