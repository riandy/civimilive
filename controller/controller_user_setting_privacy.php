<?php 
require_once("model/Connection.php");
$obj_con = new Connection();

require_once("model/User_Setting.php");
$obj_set = new User_Setting();

if(!isset($_GET['action'])){
	error_reporting(E_ALL^E_NOTICE); //remove notice
	$obj_con->up();

	$message = $_SESSION['status'];
	unset($_SESSION['status']);
	$datas = $obj_set->get_data_edit($_SESSION['userData']['id']);
	
	//start user setting
	if(is_array($datas)){
		$O_profile = $datas[0]['Profile_set'];
		$O_profile_age = $datas[0]['Profile_age'];
		$O_profile_phone_email = $datas[0]['Profile_phone_email'];
		$O_profile_summary = $datas[0]['Profile_summary'];
		$O_profile_edu = $datas[0]['Profile_edu'];
		$O_profile_work = $datas[0]['Profile_work'];
		$O_profile_skill = $datas[0]['Profile_skill'];
		$O_profile_cert = $datas[0]['Profile_cert'];
		$O_resume = $datas[0]['Resume_set'];
		$O_portfolio = $datas[0]['Portfolio_set'];
	}else{
		$O_profile = "Yes";
		$O_profile_age = "Yes";
		$O_profile_phone_email = "Yes";
		$O_profile_summary = "Yes";
		$O_profile_edu = "Yes";
		$O_profile_work = "Yes";
		$O_profile_skill = "Yes";
		$O_profile_cert = "Yes";
		$O_resume = "Yes";
		$O_portfolio = "Yes";
	}
	//end user setting

	$obj_con->down();
}
else if($_GET['action'] == 'set'){
	$obj_con->up();

	$N_user_id = $_SESSION['userData']['id'];
	if(isset($_POST['profile'])){
		$N_profile = mysql_real_escape_string(check_input($_POST['profile']));

		$N_age = "No";
		if(isset($_POST['age'])){
			$N_age = mysql_real_escape_string(check_input($_POST['age']));
		}

		$N_phone_email = "No";
		if(isset($_POST['phone_email'])){
			$N_phone_email = mysql_real_escape_string(check_input($_POST['phone_email']));
		}

		$N_summary = "No";
		if(isset($_POST['summary'])){
			$N_summary = mysql_real_escape_string(check_input($_POST['summary']));
		}

		$N_edu = "No";
		if(isset($_POST['edu'])){
			$N_edu = mysql_real_escape_string(check_input($_POST['edu']));
		}

		$N_work = "No";
		if(isset($_POST['work'])){
			$N_work = mysql_real_escape_string(check_input($_POST['work']));
		}

		$N_skill = "No";
		if(isset($_POST['skill'])){
			$N_skill = mysql_real_escape_string(check_input($_POST['skill']));
		}
		
		$N_cert = "No";
		if(isset($_POST['cert'])){
			$N_cert = mysql_real_escape_string(check_input($_POST['cert']));
		}
	}
	else{
		$N_profile = "No";
		$N_age = "No";
		$N_phone_email = "No";
		$N_summary = "No";
		$N_edu = "No";
		$N_work = "No";
		$N_skill = "No";
		$N_cert = "No";
	} 
	
	$N_resume = "No";
	if(isset($_POST['resume'])){
		$N_resume = mysql_real_escape_string(check_input($_POST['resume']));
	}
	
	$N_portfolio = "No";
	if(isset($_POST['portfolio'])){
		$N_portfolio = mysql_real_escape_string(check_input($_POST['portfolio']));
	}

	$result = $obj_set->process_data($N_user_id, $N_profile, $N_age, $N_phone_email, $N_summary, $N_edu, $N_work, $N_skill, $N_cert, $N_resume, $N_portfolio);
	if($result == 1){
		$message = "Setting privacy success";
	}else{
		$message = "Setting privacy failed";
	}

	$_SESSION['status'] = $message;
	header("Location: {$path['user-setting-privacy']}");
	$obj_con->down();
}
?>