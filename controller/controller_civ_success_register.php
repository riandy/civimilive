<?php
require_once("model/Connection.php");
$obj_con = new Connection();

require_once("model/User.php");
$obj_user = new User();


if(!isset($_GET['action'])){
    error_reporting(E_ALL^E_NOTICE); //remove notice
    
    $obj_con->up();
    if(isset($_SESSION['statusEmail'])){
        $userEmail = $_SESSION['statusEmail'];
    }
    if(isset($_SESSION['statusUsername'])){
        $userUsername = $_SESSION['statusUsername'];
    }
    if(isset($_SESSION['statusCode'])){
        $userCode = $_SESSION['statusCode'];
    }
    //send email or resend confirmation
    if($userEmail != "" && $userUsername != "" && $userCode != ""){
        $emails = $obj_user->send_email($userEmail, $userUsername, $userCode);
    }
    $obj_con->down();
}
?>