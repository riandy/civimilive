<?php 

require_once("model/Connection.php");
$obj_con = new Connection();

require_once("model/AT_User_Industry.php");
$obj_atui = new AT_User_Industry();

require_once("model/User.php");
$obj_user = new User();

if(!isset($_GET['action'])){
	error_reporting(E_ALL^E_NOTICE); //remove notice in checkbox offset
	$obj_con->up();

	$datas = $obj_atui->get_data_by_user($_SESSION['userData']['id']);
	$data_user = $obj_user->get_data_detail($_SESSION['userData']['id']);
	//var_dump($datas);
	$industrys = $obj_atui->get_index_industry();
	
	$obj_con->down();
}else if($_GET['action'] == 'add'){
	$obj_con->up();

	$N_id = mysql_real_escape_string(check_input($_POST['id']));
	$N_industry = mysql_real_escape_string(check_input($_POST['industry']));

	//start insert AT_User_Industry
	$obj_atui->delete_data($N_id); //remove data in AT_User_Industry
    $data = explode(",",$N_industry);
    if(isset($data)){
        for($i=0; $i < count($data); $i++){
        	if($data[$i] != 0){
        		$result_industry = $obj_atui->insert_data($N_id, $data[$i]);
        	}
        }
        
        $result = $obj_user->update_form_industry($N_id);
        if($result == 1){
			header("Location:{$path['user-edit']}");
		}else{
			header("Location:{$path['form-industry']}");
		}
    }
    //end insert AT_User_Industry

	$obj_con->down();
}
?>