<?php

require_once("model/Connection.php");
$obj_con = new Connection();

require_once("model/User.php");
$obj_user = new User();

if(!isset($_GET['action'])){
//if($_GET['action'] == ''){
	$obj_con->up();

	//$message = null;
	$_SESSION['message'] = null;
	$message = $_SESSION['message'];
    
	$obj_con->down();
}
else if($_GET['action'] == 'forgot'){
	$obj_con->up();

	$N_email = mysql_real_escape_string(check_input($_POST['email']));
	
	$check = $obj_user->check_primary_email($N_email);
	if($check[0]['User_login_via'] != ""){
		echo "<script>alert('Something is wrong. Your account using {$check[0]['User_login_via']}')</script>";
		echo "<meta http-equiv='refresh' content='0;url={$path['forgot-password']}'>";
	}else{
		$check_email = $obj_user->check_email($N_email);
		//var_dump($check_email);
		if($check_email == 0){
			$result = $obj_user->update_reset_code($N_email);
			$N_code = $obj_user->get_reset_code($N_email); //get reset code
		
			$to = $N_email;
			$subject = "Forget Password - civimi.com";
			$from = "From: no-reply@civimi.com \n";
			$from .= "Content-type: text/html \r\n";
			$information = "<html><head></head><body>";
			//$information .= "<a target='_blank' href='".$global['base']."'><img src='".$global['absolute-url']."img/civimi-beta.png' alt='civimi'></a><br/><br/>";
			$information .= "Hi ".$N_email.",<br/>";
			$information .= "<h2>You're almost there!</h2>";
			$information .= "Thank you for using civimi, to change your password, please click the link below.<br/><br/>";
			$information .= "<a target='_blank' href='".$global['base']."reset-password/".$N_email."/".$N_code."/'><strong>Change my password</strong></a><br/><br/><br/>";
			$information .= "(If clicking on the link doesn't work, try copying and pasting the below address into your browser.)<br/><br/>";
			$information .= "<a target='_blank' href='".$global['base']."reset-password/".$N_email."/".$N_code."/'>".$global['base']."reset-password/".$N_email."/".$N_code."/</a><br/><br/><br/>";
			$information .= "We appreciate your support and please contact us if you have any questions or comments.<br/><br/>";
			$information .= "Kind Regards,<br/><br/>";
			$information .= "The civimi team";
			$information .= "</body></html>";

			$sent = mail($to, $subject, $information, $from);
			//var_dump($sent);

			if($sent && $result == 1){
				//$message = "We'll send a link to your email, and please click on the link to change your password";
				echo "<script>alert('We will send a link to your email, and please click on the link to change your password')</script>";
				echo "<meta http-equiv='refresh' content='0;url={$global['base']}'>";
			}else{
				//$message = "Reset password failed";
				echo "<script>alert('Reset password failed')</script>";
				echo "<meta http-equiv='refresh' content='0;url={$path['forgot-password']}'>";
			}
		}else{
			//$message = "Something is wrong<br>";
			echo "<script>alert('Something is wrong. Maybe your e-mail not exist in our system.')</script>";
			echo "<meta http-equiv='refresh' content='0;url={$path['forgot-password']}'>";
		}	    		
	}

	$_SESSION['message'] = null;
	$message = $_SESSION['message'];
	//$_SESSION['status'] = $message;
	//header("Location: http://www.eannovate.com/mix/civimi/index.php");
	$obj_con->down();
}

?>