<?php 

require_once("model/Connection.php");
$obj_con = new Connection();

require_once("model/User.php");
$obj_user = new User();

if(!isset($_GET['action'])){
	$obj_con->up();

	$datas = $obj_user->get_data_detail($_SESSION['userData']['id']);

	$obj_con->down();
}else if($_GET['action'] == 'update'){
	$obj_con->up();

	$N_id = mysql_real_escape_string(check_input($_POST['id']));
	$N_goal = mysql_real_escape_string(check_input($_POST['goal']));

	$result = $obj_user->update_form_goal($N_id, $N_goal);
	if($result == 1){
		header("Location:{$path['form-personal']}");
	}else{
		header("Location:{$path['form-goal']}");
	}

	$obj_con->down();
}
?>