<?php 
require_once("model/Connection.php");
$obj_con = new Connection();

require_once("model/News.php");
$obj_news = new News();

require_once("model/Hits_User.php");
$obj_hu = new Hits_User();

if(!isset($_GET['action']) && $_GET['id'] != null && $_GET['title'] != null ){
    $obj_con->up();
    error_reporting(E_ALL^E_NOTICE); //remove notice

    $id = mysql_real_escape_string(check_input($_GET['id']));
    $title = mysql_real_escape_string(check_input($_GET['title']));

    $datas = $obj_news->get_by_id($id);//GET DATA
    $cat_id = $datas[0]['News_ref_ncID'];
    $data_photos = $obj_news->get_image_id($id);
    $data_relateds = $obj_news->get_by_related($id, $cat_id);
    //var_dump($datas);
    //var_dump($data_photos);

    //start for save hits user
    $N_view_userID = 0;
    if(isset($_SESSION['userData']['id'])){
        $N_view_userID = $_SESSION['userData']['id'];
    }
    $N_ip = getIP(); //get ip
    $userGeoData = getGeoIP($N_ip); //get data by ip address
    $N_city = $userGeoData->city; //ip city
    $N_country = $userGeoData->country; //ip country
                
    if($N_ip != "" && $N_city != "" && $N_country != ""){
        $obj_hu->insert_data("", "", "web", "news", $id, $N_view_userID, $N_ip, $N_city, $N_country);  
    }
    //end for save hits user

    $obj_con->down();
}
else {
    header("Location: index.php");
}
?>