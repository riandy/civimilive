<?php 

if(!isset($_GET['action'])){
	require_once("model/Connection.php");
	$obj_con = new Connection();

	require_once("model/Query_job.php");
	$obj_qjob = new Query_job();

	require_once("model/Ref_Field.php");
	$obj_rfield = new Ref_Field();

	require_once("model/Ref_Level.php");
	$obj_rlvl = new Ref_Level();

	require_once("model/Ref_City.php");
	$obj_rcity = new Ref_City();

	require_once("model/Ref_Country.php");
	$obj_rcountry = new Ref_Country();

	require_once("model/User.php");
	$obj_user = new User();

	require_once("model/Setting.php");
	$obj_set = new Setting();

	if(isset($_SESSION['userData']['id'])){
		$obj_con->up();

		$data_user = $obj_user->get_data_detail($_SESSION['userData']['id']);
		if(is_array($data_user)){
			if(!isset($_SESSION['userData']['username']) || $data_user[0]['User_username'] == ""){
				header("Location:{$path['form-goal']}");
			}else if($data_user[0]['User_step_goal'] == "No" || $data_user[0]['User_step_goal'] == ""){
				header("Location:{$path['form-goal']}");
			}else if($data_user[0]['User_step_personal'] == "No" || $data_user[0]['User_step_personal'] == ""){
				header("Location:{$path['form-personal']}");
			}else if($data_user[0]['User_step_industry'] == "No" || $data_user[0]['User_step_industry'] == ""){
				header("Location:{$path['form-industry']}");
			}
		}

		//start sort order page profile
		$datas = $obj_set->get_data_by_user($_SESSION['userData']['id']);
		$data_sort = array_sort($datas[0]['Setting_objSort'], $datas[0]['Setting_eduSort'], $datas[0]['Setting_expSort'], $datas[0]['Setting_skillSort'], $datas[0]['Setting_certSort'], $datas[0]['Setting_langSort'], $datas[0]['Setting_orgSort'], $datas[0]['Setting_achSort'], SORT_ASC);
		//end sort order
		
		$data_countrys = $obj_rcountry->get_index(); //GET INDEX COUNTRY
		$data_recents = $obj_qjob->get_recent(); //GET RESENT SEARCH
		
		//check progress personal
		if(is_array($data_user)){
			$N_array = array(
	            array('module' => 'image', 'field' => $data_user[0]['User_proPhotoThmb']),
	            array('module' => 'profession', 'field' => $data_user[0]['User_interest']),
	            array('module' => 'city', 'field' => $data_user[0]['User_city']),
	            array('module' => 'state', 'field' => $data_user[0]['User_state']),
	            array('module' => 'country', 'field' => $data_user[0]['User_country']),
	            array('module' => 'phone', 'field' => $data_user[0]['User_phone']),
	            array('module' => 'website', 'field' => $data_user[0]['User_website'])
	        );
	       	$progress_personal = check_progress("personal", $N_array)."%";
		}//end check progress personal

		$obj_con->down();
	}else{
		$message = "You must login first!";
		$_SESSION['statusLogin'] = $message;
		header("Location:{$path['user-login-failed']}");
	}
}
?>