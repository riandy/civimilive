<?php 
require_once("model/Connection.php");
$obj_con = new Connection();

require_once("model/User.php");
$obj_user = new User();

require_once("model/Setting.php");
$obj_set = new Setting();

require_once("model/AT_Progress.php");
$obj_atpro = new AT_Progress();

if(isset($_GET['action'])) {

	if($_GET['action'] == 'login'){

		$obj_con->up();

		if($_POST['email'] != "" && $_POST['password'] != "") {
			
			$O_email = mysql_real_escape_string(check_input($_POST['email']));
			$O_password = mysql_real_escape_string(check_input($_POST['password']));

			if($O_email != null && $O_password != null){
				$N_salt = $obj_user->get_salt($O_email);

				$N_password = substr(doHash($O_password, $N_salt), 0, 64);
				
				$result = $obj_user->user_login($O_email, $N_password);
				// var_dump($result);
				if(is_array($result)){
					if($result[0]['User_actStatus'] == 1){ //if email confirmation success
						$obj_user->update_action_login($result[0]['User_ID']); //update for last login and num login
						//if auth_code null, update
						if($result[0]['User_auth_code'] == "" || $result[0]['User_auth_code'] == NULL){
							$obj_user->update_auth_code($result[0]['User_ID']);
							$result = $obj_user->user_login($O_email, $N_password); //check login again with a new auth code
						}
						create_session($result[0]['User_ID'], $result[0]['User_username'], $result[0]['User_auth_code'], $result[0]['User_email']);
	        			$obj_set->insert_data($result[0]['User_ID'], 1, 2, 3, 4, 5, 6, 7); //insert setting sort
	        			$obj_atpro->insert_data($result[0]['User_ID'], 0, 0, 0, 0, 0, 0, 0, 0); //insert progress
	        			$location = $path['user-edit'];
					}else{ 
						$location = $path['success-register']; //not yet to confirm email
					}
			    }else{
			    	$check = $obj_user->check_primary_email($O_email);
			    	//var_dump($check);
			    	if($check[0]['User_login_via'] != ""){
			    		//messa error for user third party
			    		$message = "Incorrect login information! Please login using {$check[0]['User_login_via']}";
			    		$_SESSION['statusLogin'] = $message;
			    		$location = $path['user-login-failed'];
			    	}else{
			    		//message error invalid e-mail and password
			    		$message = "Incorrect login!";
			    		$_SESSION['statusLogin'] = $message;
			    		$location = $path['user-login-failed'];
					}
			    }
			}

		} else {
			$message = "Incorrect login!";
		    $_SESSION['statusLogin'] = $message;
		    $location = $path['user-login-failed'];
		}
		
		header("Location:".$location);
		$obj_con->down();
	}
} else {
	if(isset($_SESSION['statusLogin'])){
	    $login_status = $_SESSION['statusLogin'];
	    unset($_SESSION['statusLogin']);
	} else {
	    $login_status = "";
	}
}
?>