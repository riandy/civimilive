<?php 
include("packages/require.php");
$curpage='home';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title><?=$global['title-website'];?></title>
    <meta name="keywords" content="">
    <meta name="description" content="">
    <?php include("packages/head.php");?>
    <link rel="stylesheet" href="<?php echo $global['absolute-url'];?>stylesheets/global-style-main.css"/>
    <link rel="stylesheet" href="<?php echo $global['absolute-url'];?>css/swiper.min.css"/>
    <script src="<?php echo $global['absolute-url'];?>js/swiper.min.js"></script>
</head>
<body>
    <div id="all">
        <div class="top-content">
            <div class="top-wrapper">
                <div id="index-top-nav" class="hidden-xs">
                    <a href="<?=$global['absolute-url'];?>" class="nav-logo"><img src="<?=$global['logo-desktop'];?>" alt="logo"></a>
                    <div class="desktop-nav">
                        <ul>
                            <li class="nav-link"><a href="#">Sign up</a> / <a href="#">Login</a></li>
                            <li class="nav-link-job"><a href="#">Look for a Job</a></li>
                        </ul>
                    </div>
                </div>
                <div id="mobile-top-nav" class="visible-xs">
                    <div class="fixed-nav-mobile">
                        <ul>
                            <li><a href="#" class="nav-mob">Sign up</a></li>
                            <li><a href="#" class="nav-mob">Login</a></li>
                            <li><a href="#" class="nav-mob">Look for a job</a></li>
                        </ul>
                    </div>
                    <div class="row top-nav-mobile">
                        <div class="col-xs-2 pad0">
                            <a id="nav-logo-mobile" href="#" >
                                <img src="<?=$global['logo-mobile'];?>" alt="logo">
                            </a>
                        </div>
                        <div class="col-xs-10 pad0">
                            <div class="text-right btn-nav-mobile"><a  id="btn-nav-mobile" href="#" onclick="" class="btn"><i class="glyphicon glyphicon-align-justify up05"></i></a></div>
                        </div>
                    </div>
                </div>
                <div class="container container-header-cvm">
                    <div class="index-top-header">REINVENT YOURSELF</div>
                    <div class="index-top-header-child">We simply just want to help you demonstrate and showyour true abilities to potential people (i.e. employers, clients, headhunters, etc)</div>
                    <div class="up3"></div>
                </div>
                <!-- start comment section -->
                <div id="swiper-comment" class="swiper-container container container-comment" style="padding: 0 20px !important;">
                <div class="swiper-wrapper height-auto">
                    <div class="swiper-slide height-auto">
                        <div class="index-content-comment">
                            <div class="comment-profile">
                                <span class="angle"></span>
                                <img src="<?=$global['absolute-url'];?>img/basic-img/user.jpg" alt="" class="comment-avatar">
                                <div class="comment-name">Steve</div>
                            </div>
                            <div class="comment-message">
                                <div class="comment-text">I'm a professional lawyer and currentlyinterested in finding new clients.</div>
                                <div class="comment-see"><a href="">see details</a></div>
                            </div>
                        </div>
                    </div>
                    <div class="swiper-slide height-auto">
                        <div class="index-content-comment-right" style="top:60px;">
                            <div class="comment-message">
                                <div class="comment-text">I'm a professional lawyer and currentlyinterested in finding new clients.</div>
                                <div class="comment-see"><a href="">see details</a></div>
                            </div>
                            <div class="comment-profile">
                                <span class="angle-right"></span>
                                <img src="<?=$global['absolute-url'];?>img/basic-img/user.jpg" alt="" class="comment-avatar">
                                <div class="comment-name">Steve</div>
                            </div>
                        </div>
                    </div>
                    <div class="swiper-slide height-auto">
                        <div class="index-content-comment" style="top:120px;">
                            <div class="comment-profile">
                                <span class="angle"></span>
                                <img src="<?=$global['absolute-url'];?>img/basic-img/user.jpg" alt="" class="comment-avatar">
                                <div class="comment-name">Steve</div>
                            </div>
                            <div class="comment-message">
                                <div class="comment-text">I'm a professional lawyer and currentlyinterested in finding new clients.</div>
                                <div class="comment-see"><a href="">see details</a></div>
                            </div>
                        </div>
                    </div>
                    <div class="swiper-slide height-auto">
                        <div class="index-content-comment-right" style="top:180px;">
                            <div class="comment-message">
                                <div class="comment-text">I'm a professional lawyer and currentlyinterested in finding new clients.</div>
                                <div class="comment-see"><a href="">see details</a></div>
                            </div>
                            <div class="comment-profile">
                                <span class="angle-right"></span>
                                <img src="<?=$global['absolute-url'];?>img/basic-img/user.jpg" alt="" class="comment-avatar">
                                <div class="comment-name">Steve</div>
                            </div>
                        </div>
                    </div>
                    <div class="swiper-slide height-auto">
                        <div class="index-content-comment" style="top:250px;">
                            <div class="comment-profile">
                                <span class="angle"></span>
                                <img src="<?=$global['absolute-url'];?>img/basic-img/user.jpg" alt="" class="comment-avatar">
                                <div class="comment-name">Steve</div>
                            </div>
                            <div class="comment-message">
                                <div class="comment-text">I'm a professional lawyer and currentlyinterested in finding new clients.</div>
                                <div class="comment-see"><a href="">see details</a></div>
                            </div>
                        </div>
                    </div>
                    <div class="swiper-slide height-auto">
                        <div class="index-content-comment-right" style="top:310px;">
                            <div class="comment-message">
                                <div class="comment-text">I'm a professional lawyer and currentlyinterested in finding new clients.</div>
                                <div class="comment-see"><a href="">see details</a></div>
                            </div>
                            <div class="comment-profile">
                                <span class="angle-right"></span>
                                <img src="<?=$global['absolute-url'];?>img/basic-img/user.jpg" alt="" class="comment-avatar">
                                <div class="comment-name">Steve</div>
                            </div>
                        </div>
                    </div>
                    <div class="swiper-slide height-auto">
                        <div class="index-content-comment" style="top:370px;">
                            <div class="comment-profile">
                                <span class="angle"></span>
                                <img src="<?=$global['absolute-url'];?>img/basic-img/user.jpg" alt="" class="comment-avatar">
                                <div class="comment-name">Steve</div>
                            </div>
                            <div class="comment-message">
                                <div class="comment-text">I'm a professional lawyer and currentlyinterested in finding new clients.</div>
                                <div class="comment-see"><a href="">see details</a></div>
                            </div>
                        </div>
                    </div>
                </div>
                </div><!-- end comment section -->
            </div><!--  end wrapper -->
        </div><!--  end top content -->
        <div id="center-content">
            <!-- start popular profile -->
            <div id="popular-profile-content">
                <div class="container container-max">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="popular-profile-header">Today's Popular Profiles</div>
                            <div class="popular-profile-header-child">Here are the most popular profiles of the day.</div>
                        </div>
                    </div>
                    <div class="row row-popular-profile hidden-xs">
                        <?php for($m=1;$m<=6;$m++){?>
                        <div class="col-xs-2 col-popular-profile">
                            <a href="" class="popular-profile-box">
                                <div class="popular-profile-img"><img src="<?=$global['absolute-url'];?>img/basic-img/user.jpg" alt=""></div>
                                <div class="popular-profile-name">John Doe</div>
                                <div class="popular-profile-job">UX Designer</div>
                            </a>
                        </div>
                        <?php } ?>
                    </div>
                    <div id="swiper-profile" class="swiper-container height-auto visible-xs">
                        <div class="swiper-wrapper height-auto">
                            <?php for($p=1;$p<=6;$p++) { ?>
                            <div class="swiper-slide swiper-slide-desktop height-auto">
                                <div class=" col-xs-12 col-popular-profile">
                                    <a href="" class="popular-profile-box">
                                        <div class="popular-profile-img"><img src="<?=$global['absolute-url'];?>img/basic-img/user.jpg" alt=""></div>
                                        <div class="popular-profile-name">John Doe</div>
                                        <div class="popular-profile-job">UX Designer</div>
                                    </a>
                                </div>
                            </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div><!-- end popular profile -->
            <!-- start signup content -->
            <div id="index-signup-content">
                <div class="container container-max">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="index-signup-header">Reinvent Yours Now, it's very simple!</div>
                        </div>
                    </div>
                    <div class="row up1">
                        <div class="col-xs-10 col-xs-offset-1 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4">
                            <input type="email" class="form-control" name="email" placeholder="email address" required>
                        </div>
                    </div>
                    <div class="row up1">
                        <div class="col-xs-12 text-center">
                            <button class="btn btn-info btn-signup" type="submit">Sign up</button>
                        </div>
                    </div>
                </div>
            </div><!-- end signup content -->
            <!-- start popular job -->
            <!-- <div id="popular-job-content">
                <div class="container container-job">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="popular-job-header">Most Popular Jobs</div>
                            <div class="popular-job-header-child">Here are the current most popular jobs.</div>
                        </div>
                    </div>
                    <div class="row row-popular-job hidden-xs">
                        <?php for($j=1;$j<=5;$j++){?>
                        <div class="col-xs-12 col-popular-job">
                            <div class="popular-job-box" style="background-image:url('img/jobs.jpg')">
                                <div class="popular-job-text">
                                    <div class="popular-job-title">IT Infrastructure Developer</div>
                                    <a href="" class="popular-job-link">see openings</a>
                                </div>
                            </div>
                        </div>
                        <?php } ?>
                    </div>
                    <div id="swiper-job" class="swiper-container height-auto visible-xs">
                        <div class="swiper-wrapper height-auto">
                            <?php for($p=1;$p<=6;$p++) { ?>
                            <div class="swiper-slide swiper-slide-desktop height-auto">
                                <div class="col-xs-12 col-popular-job">
                                    <div class="popular-job-box" style="background-image:url('img/jobs.jpg')">
                                        <div class="popular-job-text">
                                            <div class="popular-job-title">IT Infrastructure Developer</div>
                                            <a href="" class="popular-job-link">see openings</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="row up3">
                        <div class="col-xs-12">
                            <a href="" class="popular-job-all-link">see all job openings</a>
                        </div>
                    </div>
                </div>
            </div> -->
            <!-- end popular job -->
        </div><!-- end center content -->
        <!-- start footer section -->
        <div id="footer-content">
            <div class="container">
                <div class="desktop-footer">
                    <div class="row">
                        <div class="col-sm-3 col-md-2 hidden-xs">
                            <div class="nav-footer">
                                <ul>
                                    <li><a href="">About us</a></li>
                                    <li><a href="">Jobs</a></li>
                                    <li><a href="">Design</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-sm-3 col-md-2 hidden-xs">
                            <div class="nav-footer">
                                <ul>
                                    <li><a href="">Blog</a></li>
                                    <li><a href="">Sketchbook</a></li>
                                </ul>
                            </div>
                            <div class="footer-social">
                                <a href="" class="social-logo"><img src="<?=$global['absolute-url'];?>img/logo/facebook.png" alt=""></a>
                                <a href="" class="social-logo"><img src="<?=$global['absolute-url'];?>img/logo/google.png" alt=""></a>
                                <a href="" class="social-logo"><img src="<?=$global['absolute-url'];?>img/logo/twitter.png" alt=""></a>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-8 col-xs-12">
                            <div class="footer-slogan">"Reinvent Yourself"</div>
                        </div>
                    </div>
                    <div class="row visible-xs">
                        <div class="col-xs-12 text-center">
                            <button class="btn btn-info btn-morelink" data-toggle="modal" data-target="#modalFooter">More Links</button>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="footer-copyright"><?=$global['copyright'];?></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div><!--  end all div -->
<!-- modal footer -->
<div class="modal fade" id="modalFooter" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content" style="border-radius:0;">
            <div class="modal-header modal-header-footer">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h1 class="modal-title-footer">More Links</h1>
            </div>
            <div class="modal-body modal-body-footer">
                <div class="row up1">
                    <div class="col-xs-12 text-center">
                        <div class="modal-nav-footer">
                            <ul>
                                <li><a href="">About us</a></li>
                                <li><a href="">Jobs</a></li>
                                <li><a href="">Design</a></li>
                                <li><a href="">Blog</a></li>
                                <li><a href="">Sketchbook</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer" style="background-color: #03A1E4;">
                <div class="modal-footer-social">
                    <a href="" class="social-logo"><img src="<?=$global['absolute-url'];?>img/logo/facebook.png" alt=""></a>
                    <a href="" class="social-logo"><img src="<?=$global['absolute-url'];?>img/logo/google.png" alt=""></a>
                    <a href="" class="social-logo"><img src="<?=$global['absolute-url'];?>img/logo/twitter.png" alt=""></a>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $('#btn-nav-mobile').click(function() {
        $('.fixed-nav-mobile').slideToggle('fast');
        return false;
    });
    $('.nav-mob').click(function() {
        $('.fixed-nav-mobile').slideToggle('fast');
        return false;
    });
</script>
<script type="text/javascript">
    var contentSwiper = new Swiper('#swiper-profile',{
    loop: true,
    autoResize: true,
    paginationClickable: true,
    autoplay:6000,
    slidesPerView: 2,
    });
    var contentSwiperJob = new Swiper('#swiper-job',{
    loop: true,
    autoResize: true,
    paginationClickable: true,
    autoplay:3000,
    slidesPerView: 2,
    });
    var contentSwiperComment = new Swiper('#swiper-comment',{
    speed : 1000,
    autoplay:3000,
    slidesPerView: 5,
    direction: 'vertical'
    });
</script>
</body>
</html>
