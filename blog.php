<?php 
include("packages/require.php");
include("controller/controller_global.php");
include("controller/controller_blog.php");

$curpage='blog';
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title><?=$global['title-blog'];?></title>
  <meta name="keywords" content="">
  <meta name="description" content="<?=$global['desc-blog'];?>">
  <meta name="robots" content="<?=$seo['robot_yes'];?>">
  <meta property="fb:pages" content="321442411240957" />
  <?php include("packages/head.php");?>
  </head>
  <body>
    <div id="all">
      <!-- start top nav -->
      <?php include("section-top-nav.php");?>
      <!-- end top nav -->

      <!-- start blog part -->
      <div id="blog-section" class="down3">
        <div class="visible-xs up6"></div>
        <div class="blog-head">
          <div class="container">
            <div style="position:relative;">
              <div style="position:absolute;"><h1 class="blog-breadcrumb">Blogs</h1></div>
            </div>
          </div>
        </div>
        <div class="container">
          <div class="row">
            <div class="col-md-9 col-sm-8 col-xs-12">
              <?php if(is_array($datas)){ foreach($datas as $data){?>
              <div class="bs-box">
                <div class="bs-list-top" style="background: url('<?php echo $global['base'].'admin/'.$data['Np_img'];?>') center center;">
                  <div class="bs-date-comment">
                    <span class="bs-date"><?php echo date("d F Y", strtotime($data['News_create_date']));?></span>
                  </div>
                </div>
                <div class="bs-list-bottom">
                  <h3 class="bs-title"><a href="<?php echo $path['blog'].$data['News_ID']."_".encode($data['News_title']).".html";?>" ><?php echo correctDisplay($data['News_title']);?></a></h3>
                  <?php if($data['News_author'] != ""){ ?>
                  <div class="bs-author">by <a href="<?php if($data['News_author_link'] != ""){ echo $data['News_author_link']; } else { echo "#"; }?>" <?php if($data['News_author_link'] != ""){ ?>target="_blank"<?php } ?>><?php echo correctDisplay($data['News_author']);?></a></div>
                  <?php } ?>
                  <div class="bs-desc">
                    <p class="" style="margin:20px 0 10px 0;"><?php echo charLength(correctDisplay($data['News_content']),300);?></p>
                  </div><br/>
                  <a href="<?php echo $path['blog'].$data['News_ID']."_".encode($data['News_title']).".html";?>" class="bs-read">Read More</a>
                </div>
              </div>
              <?php } ?>
              <!-- START PAGINATION -->
              <div class="up2"></div>
              <?php include("part-pagination.php");?>
              <!-- END PAGINATION -->
              <?php }else{?>
              <div class="bs-box">
                <h3>Oops.. there is no article at the moment.</h3>
              </div>
              <?php } ?>
            </div>
            <div class="col-md-3 col-sm-4 col-xs-12">
              <div class="subscribe-news">
                <form name="subscribe" action="" method="post">
                  <span>Keep me updated</span> 
                  <input type="email" class="form-control" name="email" placeholder="enter email">
                  <button type="submit" class="btn btn-info btn-block">Give Me Update</button>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- end blog part -->

      <!-- start footer section -->
      <?php include("section-footer.php");?>
      <!-- end footer section -->
    </div><!--  end all div -->
  </body>
  </html>
