<?php 
class Ref_Country{

	private $table = "T_Ref_Country";
	private $publish = "Country_publish IN('Publish') "; 
	private $order = "ORDER BY Country_title ASC";
	private $itemPerPage = 20;

	//FUNCTION TO GET AMOUNT OF ALL PUBLISHED Disease IN DATABASE
	public function get_total($letter = null, $publish = 1){
		$result = 0;
		$cond = '';
		if($letter != null){
			$cond .="AND Country_title LIKE '$letter%' ";
		}
		if(isset($publish)){
			if($publish != 1){$this->publish = "Country_publish IN('Publish', 'Not Publish') ";}
		}
		$text = "SELECT count(Country_ID) AS count FROM $this->table WHERE $this->publish $cond ";
		$query = mysql_query($text);
		if(mysql_num_rows($query) >= 1){
			$row = mysql_fetch_array($query,MYSQL_ASSOC);
			$result =  $row['count'];
		}
		return $result;
	}

	public function get_total_page($total_data){
		$num_page = ceil($total_data / $this->itemPerPage);
		return $num_page;
	}	

	//FUNCTION TO GET data OF ALL PUBLISHED Country IN DATABASE
	//1 to set publish, 0 to get publish and not published data
	public function get_data_by_page($page=0, $letter=null, $publish=1){
		$data = "There is no result.";
		if($page <= 1 || $page == null){
			$limitBefore = 0;
		}else{
			$limitBefore = ($page-1) * $this->itemPerPage;
		}
		$cond = '';
		if($letter != null){
			$cond .="AND Country_title LIKE '$letter%' ";
		}
		if($publish != 1){
			$this->publish = "Country_publish IN('Publish', 'Not Publish') ";
		}
 		$text = "SELECT * FROM $this->table WHERE $this->publish $cond $this->order LIMIT $limitBefore, $this->itemPerPage";
 		$query = mysql_query($text);
		if(mysql_num_rows($query) >= 1){
			$data = array();
			while($row = mysql_fetch_array($query,MYSQL_ASSOC)){
				$data[]=$row;
			}
		}
		return $data;
	}

	public function get_index(){
		$result = 0;

		$text = "SELECT Country_ID, Country_title, Country_abbr, Country_flag 
			FROM $this->table ORDER BY Country_title ASC";
		$query = mysql_query($text);
		if(mysql_num_rows($query) >= 1){
			$result = array();
			while($row = mysql_fetch_assoc($query)){
				$result[] = $row;
			}
		}
		return $result;
	}

	public function get_data_edit($id){
		$result = 0;

		$text = "SELECT * FROM $this->table WHERE Country_ID = '$id'";
		$query = mysql_query($text);
		if(mysql_num_rows($query) >= 1){
			$result = array();
			while($row = mysql_fetch_assoc($query)){
				$result[] = $row;
			}
		}
		return $result;
	}

	public function insert_data($title, $abbr, $flag, $admin_id, $publish){
		$result = 0;

		$text = "INSERT INTO $this->table (Country_title, Country_abbr, Country_flag, Country_create_adminID, Country_publish, Country_create_date) VALUES ('$title', '$abbr', '$flag', '$admin_id', '$publish', NOW())";
		$query = mysql_query($text);
		if($query){
			$result = mysql_insert_id(); 
		}
		return $result;
	}

	public function update_data($id, $title, $abbr, $admin_id, $publish){
		$result = 0;

		$text = "UPDATE $this->table SET Country_title = '$title', Country_abbr = '$abbr', Country_modify_adminID = '$admin_id', Country_publish = '$publish' WHERE Country_ID = '$id'";
		$query = mysql_query($text);
		if(mysql_affected_rows() == 1){
			$result = 1;
		}
		return $result;
	}

	public function update_data_image($id, $title, $abbr, $flag, $admin_id, $publish){
		$result = 0;

		$this->remove_image($id); //remove image before

		$text = "UPDATE $this->table SET Country_title = '$title', Country_abbr = '$abbr', Country_flag = '$flag', Country_modify_adminID = '$admin_id', Country_publish = '$publish' WHERE Country_ID = '$id'";
		$query = mysql_query($text);
		if(mysql_affected_rows() == 1){
			$result = 1;
		}
		return $result;
	}

	public function remove_image($id){
		$result = 0;
		$flag_img = 0;
		$text = "SELECT Country_ID, Country_flag FROM $this->table WHERE Country_ID = '$id' ";
		$query = mysql_query($text);
		if(mysql_num_rows($query) == 1){
			$row = mysql_fetch_assoc($query);
			$deleteImg = $_SERVER['DOCUMENT_ROOT']."/".$row['Country_flag'];
            if (file_exists($deleteImg)) {
                unlink($deleteImg);
                $flag_img = 1;
            }
           
            if($flag_img == 1){
            	$result = 1;
            }
		}
		return $result;
	}

	public function delete_data($id){
		$result = 0;

		$this->remove_image($id); //remove image before

		$text = "DELETE FROM $this->table WHERE Country_ID = '$id'";
		$query = mysql_query($text);
		if(mysql_affected_rows() == 1){
			$result = 1;
		}
		return $result;
	}

	public function publish_data($id, $publish){
		$result = 0;

		$text = "UPDATE $this->table SET Country_publish = '$publish' WHERE Country_ID = '$id'";
		$query = mysql_query($text);
		if(mysql_affected_rows() == 1){
			$result = 1;
		}
		return $result;
	}

	public function unpublish_data($id){
		$result = 0;
		$not_publish = "Not Publish";

		$text = "UPDATE $this->table SET Country_publish = '$not_publish'  WHERE Country_ID = '$id'";
		$query = mysql_query($text);
		if(mysql_affected_rows() == 1){
			$result = 1;
		}
		return $result;
	}

	//for api check country
	public function check_country($title){//return 0 if not exist
		$data = 0;

		$text = "SELECT Country_ID FROM $this->table WHERE Country_title = '$title'";
		$query = mysql_query($text);
		if(mysql_num_rows($query) == 1){
			$row = mysql_fetch_array($query, MYSQL_ASSOC);
			$data = $row['Country_ID'];
		}
		return $data;
	}//END CHECK-EXIST

}

?>