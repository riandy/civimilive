<?php 
class Growth{

    private $table = "T_User";
    private $itemPerPage = 20;

// START FUNCTION FOR ADMIN PAGE
    //function for get data graph in adminMgr-Growth.php
    public function get_data_graph($start_date, $end_date){
        $result = 0;
        //create time zone
        date_default_timezone_set('Asia/Jakarta');
        $nowDate = date("Y-m-d");
        
        if($start_date != $end_date){
            $condPeriod = "DATE(User_create_date) BETWEEN '$start_date' AND '$end_date' "; //range date
        }else{
            if($start_date != ""){
                $condPeriod = "DATE(User_create_date) = '$start_date' "; //data user perhari/kemarin
            }else{
                $condPeriod = "DATE(User_create_date) = '$nowDate' "; //data user hari ini
            }
        }

        $text = "SELECT COUNT(User_ID) AS counter_user, DATE(User_create_date) AS User_create_date FROM $this->table WHERE $condPeriod GROUP BY DATE(User_create_date)";
        $query = mysql_query($text);
        if(mysql_num_rows($query) >= 1){
            $result = array();
            while($row = mysql_fetch_assoc($query)){
                $result[] = $row;
            }
        }
        //$result = $text;
        return $result;
    }

    //function for get data in adminMgr-Growth.php
    public function get_data($page=0, $letter=null, $start_date, $end_date){
        $data = "There is no result.";
        //create time zone
        date_default_timezone_set('Asia/Jakarta');
        $nowDate = date("Y-m-d");

        if($page <= 1 || $page == null){
            $limitBefore = 0;
        }else{
            $limitBefore = ($page-1) * $this->itemPerPage;
        }

        if($start_date != $end_date){
            $condPeriod = "DATE(growth.User_create_date) BETWEEN '$start_date' AND '$end_date' "; //range date
        }else{
            if($start_date != ""){
                $condPeriod = "DATE(growth.User_create_date) = '$start_date' "; //data user perhari/kemarin
            }else{
                $condPeriod = "DATE(growth.User_create_date) = '$nowDate' "; //data user hari ini
            }
        }

        $condLetter = '';
        if($letter != null){$condLetter .="AND growth.User_fname LIKE '$letter%' ";}

        $text = "SELECT (SELECT COUNT(atedu.ATedu_ID) FROM AT_Edu atedu WHERE atedu.User_ID = growth.User_ID) AS edu_counter,
            (SELECT COUNT(atework.ATwork_ID) FROM AT_Work atework WHERE atework.ATwork_ref_userID = growth.User_ID) AS work_counter,
            growth.User_ID, growth.User_username, growth.User_fname, growth.User_lname, growth.User_email, growth.User_login_via, 
            growth.User_create_date, growth.User_proPhoto, growth.User_proPhotoThmb, growth.User_actStatus, growth.User_actNum, 
            growth.User_deactivate, growth.User_interest, growth.User_city, growth.User_state, growth.User_country, growth.User_phone, 
            growth.User_website FROM $this->table growth WHERE $condPeriod $condLetter ORDER BY growth.User_create_date DESC LIMIT 
            $limitBefore, $this->itemPerPage";
        $query = mysql_query($text);
        if(mysql_num_rows($query) >= 1){
            $data = array();
            while($row = mysql_fetch_array($query,MYSQL_ASSOC)){
                $data[] = $row;
            }
        }
        //$data = $text;
        return $data;
    }

    //function get total in adminMgr-Growth.php
    public function get_total($letter=null, $start_date, $end_date){
        $result = 0;
        //create time zone
        date_default_timezone_set('Asia/Jakarta');
        $nowDate = date("Y-m-d");
       
        if($start_date != $end_date){
            $condPeriod = "User_create_date BETWEEN '$start_date' AND '$end_date' "; //range date
        }else{
            if($start_date != ""){
                $condPeriod = "DATE(User_create_date) = '$start_date' "; //data user perhari/kemarin
            }else{
                $condPeriod = "DATE(User_create_date) = '$nowDate' "; //data user hari ini
            }
        }

        $condLetter = '';
        if($letter != null){$condLetter .="AND User_fname LIKE '$letter%' ";}

        $text = "SELECT count(User_ID) AS count FROM $this->table WHERE $condPeriod $condLetter";
        $query = mysql_query($text);
        if(mysql_num_rows($query) >= 1){
            $row = mysql_fetch_array($query,MYSQL_ASSOC);
            $result =  $row['count'];
        }
        //$result = $text;
        return $result;
    }

    //function for get total page in adminMgr-Growth.php
    public function get_total_page($total_data){
        $num_page = ceil($total_data / $this->itemPerPage);
        return $num_page;
    }
// END FUNCTION FOR ADMIN PAGE  
}
?>