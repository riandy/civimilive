<?php
class DocCat{

	private $table = "T_DocCat";
	private $tableDoc = "T_Doc";

// START FUNCTION FOR API
	//FUNCTION TO GET all document category by the user
    public function get_doc_cat($user_id, $sort){
        $result = 0;

        if($sort == "name"){
            $sql = "ORDER BY DocCat_name ASC";
        }else if($sort == "recent_add"){
            $sql = "ORDER BY DocCat_create_date DESC";
        }else if($sort == "recent_modify"){
            $sql = "ORDER BY DocCat_modify_date DESC";
        }else{
            $sql = "";
        }
        
        $text = "SELECT User_ID, DocCat_ID, DocCat_name, DocCat_desc, DocCat_create_date,
            DocCat_modify_date FROM $this->table WHERE User_ID = '$user_id' $sql";
        $query = mysql_query($text);
        if(mysql_num_rows($query) >= 1){
            $result = array();
            while($row = mysql_fetch_array($query,MYSQL_ASSOC)){
                $result[]=$row;
            }
        }
        //$result = $text;
        return $result;
    }

    public function get_data_detail($id){
		$result = 0;

		$text = "SELECT DocCat_ID, DocCat_name, DocCat_desc FROM $this->table WHERE DocCat_ID = '$id' ";
		$query = mysql_query($text);
		if(mysql_num_rows($query) >= 1){
			$result = array();
			while($row = mysql_fetch_assoc($query)){
				$result[] = $row;
			}
		}
		return $result;
	}

	/* Start insert function */
	public function insert_data($user_id, $name, $desc, $publish){
		$result = 0;

		$text = "INSERT INTO $this->table (User_ID, DocCat_name, DocCat_desc, DocCat_publish, DocCat_create_date)VALUES('$user_id', '$name', '$desc', '$publish', NOW())";
		$query = mysql_query($text);
		if($query){
			$result = mysql_insert_id();
		}
		return $result;
	}
	/* End function insert */

	/* Fungsi Update */
	public function update_data($id, $name, $desc){
		$result = 0;

		$text = "UPDATE $this->table SET DocCat_name = '$name', DocCat_desc = '$desc' WHERE DocCat_ID = '$id' ";
		$query = mysql_query($text);
		if(mysql_affected_rows() == 1){
			$result = 1;
		}
		return $result;
	}
	/* End Fungsi Update */

	/* 	Fungsi Delete */
	public function delete_data($id){
		$result = 0;

		$this->remove_doc_related($id); //return doc related
        $this->delete_data_related($id); //delete data related

		$text = "DELETE FROM $this->table WHERE DocCat_ID = '$id' ";
		$query = mysql_query($text);
		if(mysql_affected_rows() == 1){
			$result = 1;
		}
		return $result;
	}
	/* 	End Delete */

	 /*  Fungsi Delete */
    public function delete_data_related($id){
        $result = 0;

        $text = "DELETE FROM $this->tableDoc WHERE DocCat_ID = '$id' ";
        $query = mysql_query($text);
        if(mysql_affected_rows() == 1){
            $result = 1;
        }
        return $result;
    }
    /*  End Delete */

    //start function REMOVE doc
    public function remove_doc_related($id){
        $result = 0;
        $flag_doc = 0;
        $text = "SELECT Doc_fileLoc FROM $this->tableDoc WHERE DocCat_ID = '$id'";
        $query = mysql_query($text);
        while ($row = mysql_fetch_array($query, MYSQL_ASSOC)) 
        {
            $deleteDoc = $_SERVER['DOCUMENT_ROOT']."/".$row['Doc_fileLoc'];
            if (file_exists($deleteDoc)) {
                unlink($deleteDoc);
                $flag_doc = 1;
            }

            if($flag_doc == 1){
                $result = 1;
            }
        }
        return $result;
    }
    //end function remove doc
}
?>