<?php 
class Like{ 

	private $table = "T_Like";

	//for api get total like
	public function get_total_like($type, $type_id){
		$result = 0;

		$text = "SELECT COUNT(Like_ID) AS total_like FROM T_Like WHERE Like_type = '$type' AND Like_ref_typeID = '$type_id' ";
		$query = mysql_query($text);
		if(mysql_num_rows($query) >= 1){
			$result = array();
			while($row = mysql_fetch_assoc($query)){
				$result[] = $row;
			}
		}
		//$result = $text;
		return $result;
	}

    //INSERT LIKE TO TABLE
	public function insert_data($user_id, $username, $page, $type, $type_id, $view_user, $ip, $ip_city, $ip_country){
		$result = 0;

		$text = "INSERT INTO $this->table (Like_ref_userID, Like_username, Like_page, Like_type, Like_ref_typeID, Like_viewer_userID, Like_ip, Like_ip_city, Like_ip_country, Like_create_date) VALUES('$user_id', '$username', '$page', '$type', '$type_id', '$view_user', '$ip', '$ip_city', '$ip_country', NOW())";
		$query = mysql_query($text);
		if($query){
			$result = mysql_insert_id();
		}
		//$result = $text;
	    return $result;
	}

	public function delete_data($type, $type_id, $view_user){
		$result = 0;

		$text = "DELETE FROM $this->table WHERE Like_type = '$type' AND Like_ref_typeID = '$type_id' AND Like_viewer_userID = '$view_user' ";
		$query = mysql_query($text);
		if(mysql_affected_rows() == 1){
			$result = 1;
		}
		return $result;
	}

	//for api check_exist
	public function check_exist($type, $type_id, $view_user){
		$result = 0;

		$text = "SELECT Like_viewer_userID FROM $this->table WHERE Like_type = '$type' AND Like_ref_typeID = '$type_id' AND Like_viewer_userID = '$view_user' ";
		$query = mysql_query($text);
		if(mysql_num_rows($query) == 1){
			$row = mysql_fetch_array($query, MYSQL_ASSOC);
			$result = $row['Like_viewer_userID'];
		}
		//$result = $text;
		return $result;
	}

	//function to get info photo user for send mail 
    public function get_info_photo_user($id){
        $result = 0;
    
        $text = "SELECT user.User_fname AS User_fname, user.User_lname AS User_lname, user.User_email AS User_email, user.User_bioDesc AS User_bioDesc,
	        user.User_proPhotoThmb AS User_proPhotoThmb, user.User_city AS User_city, user.User_country AS User_country, userview.User_fname AS User_fnameview, 
	        userview.User_country AS User_countryview, Photo_ID, Photo_title, Photo_imgLink FROM ($this->table LEFT JOIN T_Photo ON Like_ref_typeID = Photo_ID AND Like_type = 'image') LEFT JOIN T_User user 
	        ON Like_ref_userID = user.User_ID LEFT JOIN T_User userview ON Like_viewer_userID = userview.User_ID WHERE Like_ID = '$id' ";
        $query = mysql_query($text);
		if(mysql_num_rows($query) >= 1){
			$result = array();
			while($row = mysql_fetch_assoc($query)){
				$result[] = $row;
			}
		}
		//$result = $text;
        return $result;
    }   
}
?>