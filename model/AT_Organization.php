<?php
class AT_Organization{

	private $table = "AT_Organization";
	private $joinOrg = "LEFT JOIN T_Organization ON Org_ID = ATorg_ref_orgID ";
	private $joinUser = "LEFT JOIN T_User ON User_ID = ATorg_ref_userID ";
	private $itemPerPage = 20;

// START FUNCTION FOR ADMIN PAGE
	/*//FUNCTION TO GET AMOUNT OF ALL PUBLISHED Employee IN DATABASE
	public function get_total($Org_ID){
		$result = 0;
		$cond = '';
		if($letter != null){
			$cond .="AND Org_title LIKE '$letter%' ";
		}
		if(isset($publish)){
			if($publish != 1){$this->publish = "Org_publish IN('Publish', 'Not Publish') ";}
		}
		$text = "SELECT COUNT(ATorg_ID) AS count FROM $this->table $this->joinOrg $this->joinUser 
 			WHERE Org_ID = '$Org_ID'";
		$query = mysql_query($text);
		if(mysql_num_rows($query) >= 1){
			$row = mysql_fetch_array($query,MYSQL_ASSOC);
			$result =  $row['count'];
		}
		return $result;
	}

	public function get_total_page($total_data){
		$num_page = ceil($total_data / $this->itemPerPage);
		return $num_page;
	}	

	//FUNCTION TO GET data OF ALL PUBLISHED Employee IN DATABASE
	//1 to set publish, 0 to get publish and not published data
	public function get_data_by_Org($page=0, $Org_ID){
		$data = "There is no result.";
		if($page <= 1 || $page == null){
			$limitBefore = 0;
		}else{
			$limitBefore = ($page-1) * $this->itemPerPage;
		}
 		
 		$text = "SELECT Org_ID, Org_title, User_ID, User_email, User_proPhoto, User_proPhotoThmb,
 			User_fname, User_lname, User_username, ATorg_ID, 
 			CASE WHEN ATorg_from_month = 1 THEN 'Jan'
 				WHEN ATorg_from_month = 2 THEN 'Feb'
 				WHEN ATorg_from_month = 3 THEN 'Mar'
 				WHEN ATorg_from_month = 4 THEN 'Apr'
 				WHEN ATorg_from_month = 5 THEN 'May'
 				WHEN ATorg_from_month = 6 THEN 'Jun'
 				WHEN ATorg_from_month = 7 THEN 'Jul'
 				WHEN ATorg_from_month = 8 THEN 'Aug'
 				WHEN ATorg_from_month = 9 THEN 'Sep'
 				WHEN ATorg_from_month = 10 THEN 'Oct'
 				WHEN ATorg_from_month = 11 THEN 'Nov'
 				WHEN ATorg_from_month = 12 THEN 'Dec'
 			END AS ATorg_from_month,
 			ATorg_from_year, 
 			CASE WHEN ATorg_to_month = 1 THEN 'Jan'
 				WHEN ATorg_to_month = 2 THEN 'Feb'
 				WHEN ATorg_to_month = 3 THEN 'Mar'
 				WHEN ATorg_to_month = 4 THEN 'Apr'
 				WHEN ATorg_to_month = 5 THEN 'May'
 				WHEN ATorg_to_month = 6 THEN 'Jun'
 				WHEN ATorg_to_month = 7 THEN 'Jul'
 				WHEN ATorg_to_month = 8 THEN 'Aug'
 				WHEN ATorg_to_month = 9 THEN 'Sep'
 				WHEN ATorg_to_month = 10 THEN 'Oct'
 				WHEN ATorg_to_month = 11 THEN 'Nov'
 				WHEN ATorg_to_month = 12 THEN 'Dec'
 			END AS ATorg_to_month,
 			ATorg_to_year, ATorg_title FROM $this->table $this->joinOrg $this->joinUser
 			WHERE Org_ID = '$Org_ID' ORDER BY User_fname LIMIT $limitBefore, $this->itemPerPage";
 		$query = mysql_query($text);
		if(mysql_num_rows($query) >= 1){
			$data = array();
			while($row = mysql_fetch_array($query,MYSQL_ASSOC)){
				$data[]=$row;
			}
		}
		//$data = $text;
		return $data;
	}

	// function index
	public function get_index($Org_ID){
		$result = 0;

		$text = "SELECT User_username, User_email, User_fname, User_lname, Org_ID, ATorg_ID, ATorg_title,
			ATorg_from_month, ATorg_from_year, ATorg_to_month, ATorg_to_year, ATorg_desc
			FROM $this->table $this->joinOrg $this->joinUser WHERE Org_ID = '$Org_ID'";
		$query = mysql_query($text);
		if(mysql_num_rows($query) >= 1){
			$result = array();
			while($row = mysql_fetch_assoc($query)){
				$result[] = $row;
			}
		}
		return $result;
	}

	public function get_data_detail($id){
		$result = 0;
		
 		$text = "SELECT Org_ID, Org_title, User_ID, User_email, User_fname, User_lname, ATorg_ID, 
 			ATorg_desc, ATorg_from_month, ATorg_from_year, ATorg_to_month, ATorg_to_year, ATorg_title, 
 			ATorg_current FROM $this->table $this->joinOrg $this->joinUser WHERE ATorg_ID = '$id'";
 		$query = mysql_query($text);
		if(mysql_num_rows($query) >= 1){
			$result = array();
			while($row = mysql_fetch_array($query,MYSQL_ASSOC)){
				$result[]=$row;
			}
		}
		//$result = $text;
		return $result;
	}*/
// END FUNCTION FOR ADMIN PAGE

// START OPERATION CRUD
	/* Start insert function */
	public function insert_data($org_id, $user_id, $title, $fromMonth, $fromYear, $toMonth, $toYear, $current, $desc){
		$result = 0;

		$text = "INSERT INTO $this->table (ATorg_ref_orgID, ATorg_ref_userID, ATorg_title, ATorg_from_month, ATorg_from_year, ATorg_to_month, ATorg_to_year, ATorg_current, ATorg_desc, ATorg_create_date) VALUES('$org_id', '$user_id', '$title', '$fromMonth', '$fromYear', '$toMonth', '$toYear', '$current', '$desc', NOW())";
		$query = mysql_query($text);
		if($query){
			//$result = 1;
			$result = mysql_insert_id();
		}
		return $result;
	}
	/* End function insert */

	/* Fungsi Update */
	public function update_data($id, $org_id, $title, $fromMonth, $fromYear, $toMonth, $toYear, $current, $desc){
		$result = 0;

		$text = "UPDATE $this->table SET ATorg_ref_orgID = '$org_id', ATorg_title = '$title', ATorg_from_month = '$fromMonth', ATorg_from_year = '$fromYear', ATorg_to_month = '$toMonth', ATorg_to_year = '$toYear', ATorg_current = '$current', ATorg_desc = '$desc' WHERE ATorg_ID = '$id' ";
		$query = mysql_query($text);
		if(mysql_affected_rows() == 1){
			$result = 1;
		}
		//$result = $text;
		return $result;
	}
	/* End Fungsi Update */

	/* 	Fungsi Delete */
	public function delete_data($id){
		$result = 0;

		$text = "DELETE FROM $this->table WHERE ATorg_ID = '$id' ";
		$query = mysql_query($text);
		if(mysql_affected_rows() == 1){
			$result = 1;
		}
		return $result;
	}
	/* 	End Delete */
// END OPERATION CRUD

// START FUNCTION FOR CLIENT PAGE
	//for api get data
	public function get_data($user_id){
		$result = 0;

		$text = "SELECT ATorg_ID, Org_title, Org_img, ATorg_title, ATorg_from_month, ATorg_from_year,
			ATorg_to_month, ATorg_to_year, ATorg_current, ATorg_desc, City_title, Country_title FROM T_Organization 
			LEFT JOIN $this->table ON ATorg_ref_orgID = Org_ID LEFT JOIN T_Ref_City ON Org_ref_cityID = City_ID
			LEFT JOIN T_Ref_Country ON Org_ref_countryID = Country_ID WHERE ATorg_ref_userID = '$user_id' ORDER BY 
			ATorg_current DESC, ATorg_from_year DESC, ATorg_from_month DESC, ATorg_to_year DESC, ATorg_to_month DESC";
		$query = mysql_query($text);
		if(mysql_num_rows($query) >= 1){
			$result = array();
			while($row = mysql_fetch_assoc($query)){
				$result[] = $row;
			}
		}
		//$result = $text;
		return $result;
	}

	/*//for api check user
	public function check_user($user_id){//return 0 if not exist
		$data = 0;

		$text = "SELECT ATorg_ID FROM $this->table WHERE ATorg_ref_userID = '$user_id'";
		$query = mysql_query($text);
		if(mysql_num_rows($query) == 1){
			$row = mysql_fetch_array($query, MYSQL_ASSOC);
			$data = $row['ATorg_ID'];
		}
		return $data;
	}//END CHECK-EXIST*/
// END FUNCTION FOR CLIENT PAGE
}
?>