<?php 
class Certificate{

	private $table = "T_Certificate";
	private $publish = "Cert_publish IN('Publish') "; 
	private $order = "ORDER BY Cert_title ASC";
	private $itemPerPage = 20;

	//FUNCTION TO GET AMOUNT OF ALL PUBLISHED Certificate IN DATABASE
	public function get_total($letter = null, $publish = 1){
		$result = 0;
		$cond = '';
		if($letter != null){
			$cond .="AND Cert_title LIKE '$letter%' ";
		}
		if(isset($publish)){
			if($publish != 1){$this->publish = "Cert_publish IN('Publish', 'Not Publish') ";}
		}
		$text = "SELECT count(Cert_ID) AS count FROM $this->table WHERE $this->publish $cond ";
		$query = mysql_query($text);
		if(mysql_num_rows($query) >= 1){
			$row = mysql_fetch_array($query,MYSQL_ASSOC);
			$result =  $row['count'];
		}
		return $result;
	}

	public function get_total_page($total_data){
		$num_page = ceil($total_data / $this->itemPerPage);
		return $num_page;
	}	

	//FUNCTION TO GET data OF ALL PUBLISHED Certificate IN DATABASE
	//1 to set publish, 0 to get publish and not published data
	public function get_data_by_page($page=0, $letter=null){
		$data = "There is no result.";
		if($page <= 1 || $page == null){
			$limitBefore = 0;
		}else{
			$limitBefore = ($page-1) * $this->itemPerPage;
		}

		$cond = '';
		if($letter != null){
			$cond .="AND t.Cert_title LIKE '$letter%' ";
		}
		
		/*$condType = "";
		if($type == 'publish'){
			$condType .= "Cert_publish = 'Publish' ";
		}else if($type == 'notpublish'){
			$condType .= "Cert_publish = 'Not Publish' ";
		}*/
 		
 		$text = "SELECT COUNT(at.ATcert_ID) AS cert_counter, t.Cert_ID AS Cert_ID, t.Cert_title AS Cert_title, 
 			t.Cert_url AS Cert_url, t.Cert_img AS Cert_img, t.Cert_img_thmb AS Cert_img_thmb, t.Cert_publish AS Cert_publish 
 			FROM $this->table t LEFT JOIN AT_Certificate at ON at.Cert_ID = t.Cert_ID WHERE $this->publish 
 			$cond GROUP BY t.Cert_ID ORDER BY t.Cert_title ASC LIMIT $limitBefore, $this->itemPerPage";
 		$query = mysql_query($text);
		if(mysql_num_rows($query) >= 1){
			$data = array();
			while($row = mysql_fetch_array($query,MYSQL_ASSOC)){
				$data[]=$row;
			}
		}
		//$data = $text;
		return $data;
	}

	public function get_index(){
		$result = 0;

		$text = "SELECT Cert_ID, Cert_title FROM $this->table WHERE $this->publish $this->order";
		$query = mysql_query($text);
		if(mysql_num_rows($query) >= 1){
			$result = array();
			while($row = mysql_fetch_assoc($query)){
				$result[] = $row;
			}
		}
		return $result;
	}

	public function get_data_edit($id){
		$result = 0;

		$text = "SELECT * FROM $this->table WHERE Cert_ID = '$id'";
		$query = mysql_query($text);
		if(mysql_num_rows($query) >= 1){
			$result = array();
			while($row = mysql_fetch_assoc($query)){
				$result[] = $row;
			}
		}
		return $result;
	}

	public function insert_data($title, $url, $img, $img_thmb, $publish, $admin_id){
		$result = 0;

		$text = "INSERT INTO $this->table (Cert_title, Cert_url, Cert_img, Cert_img_thmb, Cert_publish, Cert_create_adminID, Cert_create_date) VALUES ('$title', '$url', '$img', '$img_thmb', '$publish', '$admin_id', NOW())";
		$query = mysql_query($text);
		if($query){
			$result = mysql_insert_id(); 
		}
		//$result = $text;
		return $result;
	}

	public function update_data($id, $title, $url, $publish, $admin_id){
		$result = 0;

		$text = "UPDATE $this->table SET Cert_title = '$title', Cert_url = '$url', Cert_publish = '$publish', Cert_modify_adminID = '$admin_id' WHERE Cert_ID = '$id'";
		$query = mysql_query($text);
		if(mysql_affected_rows() == 1){
			$result = 1;
		}
		return $result;
	}

	public function update_data_image($id, $title, $url, $img, $img_thmb, $publish, $admin_id){
		$result = 0;

		$this->remove_image($id); //remove image before

		$text = "UPDATE $this->table SET Cert_title = '$title', Cert_url = '$url', Cert_img = '$img', Cert_img_thmb = '$img_thmb', Cert_publish = '$publish', Cert_modify_adminID = '$admin_id' WHERE Cert_ID = '$id'";
		$query = mysql_query($text);
		if(mysql_affected_rows() == 1){
			$result = 1;
		}
		return $result;
	}

	public function remove_image($id){
		$result = 0;
		$flag_img = 0;
		$flag_img_thmb = 0;
		$text = "SELECT Cert_ID, Cert_img, Cert_img_thmb FROM $this->table WHERE Cert_ID = '$id' ";
		$query = mysql_query($text);
		if(mysql_num_rows($query) == 1){
			$row = mysql_fetch_assoc($query);
			$deleteImg = $_SERVER['DOCUMENT_ROOT']."/".$row['Cert_img'];
            if (file_exists($deleteImg)) {
                unlink($deleteImg);
                $flag_img = 1;
            }
            $deleteImgThmb = $_SERVER['DOCUMENT_ROOT']."/".$row['Cert_img_thmb'];
            if (file_exists($deleteImgThmb)) {
                unlink($deleteImgThmb);
                $flag_img_thmb = 1;
            }

            if($flag_img == 1 && $flag_img_thmb ==1){
            	$result = 1;
            }
		}
		return $result;
	}

	public function delete_data($id){
		$result = 0;

		$this->remove_image($id); //remove image before

		$text = "DELETE FROM $this->table WHERE Cert_ID = '$id'";
		$query = mysql_query($text);
		if(mysql_affected_rows() == 1){
			$result = 1;
		}
		return $result;
	}

	public function publish_data($id){
		$result = 0;
		$publish = "Publish";

		$text = "UPDATE $this->table SET Cert_publish = '$publish' WHERE Cert_ID = '$id'";
		$query = mysql_query($text);
		if(mysql_affected_rows() == 1){
			$result = 1;
		}
		return $result;
	}

	public function unpublish_data($id){
		$result = 0;
		$not_publish = "Not Publish";

		$text = "UPDATE $this->table SET Cert_publish = '$not_publish'  WHERE Cert_ID = '$id'";
		$query = mysql_query($text);
		if(mysql_affected_rows() == 1){
			$result = 1;
		}
		return $result;
	}

	//for api get list
	public function get_list($title){
		$result = 0;

		$text = "SELECT Cert_ID, Cert_title, Cert_url, Cert_img, Cert_img_thmb,
			IF(Cert_title LIKE '$title%', 20, IF(Cert_title LIKE '%$title%', 15, 
				IF(Cert_title LIKE '%$title', 10, 0))) AS weight
			FROM $this->table WHERE Cert_publish = 'Publish' AND Cert_title LIKE '$title%' OR 
			Cert_title LIKE '%$title' OR Cert_title LIKE '%$title%' 
			ORDER BY weight DESC, Cert_title ASC";
		$query = mysql_query($text);
		if(mysql_num_rows($query) >= 1){
			$result = array();
			while($row = mysql_fetch_assoc($query)){
				$result[] = $row;
			}
		}
		//$result = $text;
		return $result;
	}

	//for api check certificate
	public function check_certificate($title){//return 0 if not exist
		$data = 0;

		$text = "SELECT Cert_ID FROM $this->table WHERE Cert_title = '$title' ORDER BY Cert_ID ASC LIMIT 0,1";
		$query = mysql_query($text);
		if(mysql_num_rows($query) == 1){
			$row = mysql_fetch_array($query, MYSQL_ASSOC);
			$data = $row['Cert_ID'];
		}
		return $data;
	}//END CHECK-EXIST

}

?>