<?php 
class Setting{
	
	private $table = "T_Setting";

	/*public function get_data_edit($user_id){
		$result = 0;
		
 		$text = "SELECT * FROM $this->table WHERE User_ID = '$user_id'";
 		$query = mysql_query($text);
		if(mysql_num_rows($query) >= 1){
			$result = array();
			while($row = mysql_fetch_array($query,MYSQL_ASSOC)){
				$result[]=$row;
			}
		}
		//$result = $text;
		return $result;
	}*/

	//function for sort order profile by user id
	public function get_data_by_user($user_id){
		$result = 0;
		
 		$text = "SELECT setting.Setting_ID AS Setting_ID, setting.User_ID AS User_ID, setting.Setting_objSort AS Setting_objSort, 
 			setting.Setting_eduSort AS Setting_eduSort, setting.Setting_expSort AS Setting_expSort, setting.Setting_skillSort AS
 			Setting_skillSort, setting.Setting_certSort AS Setting_certSort, setting.Setting_langSort AS Setting_langSort,
 			setting.Setting_workSort AS Setting_workSort, setting.Setting_orgSort AS Setting_orgSort, setting.Setting_achSort AS Setting_achSort 
 			FROM $this->table setting LEFT JOIN T_User user ON user.User_ID = setting.User_ID WHERE setting.User_ID = '$user_id'";
 		$query = mysql_query($text);
		if(mysql_num_rows($query) >= 1){
			$result = array();
			while($row = mysql_fetch_array($query,MYSQL_ASSOC)){
				$result[]=$row;
			}
		}
		//$result = $text;
		return $result;
	}

	//function for sort order resume/portfolio by username
	public function get_data_by_username($username){
		$result = 0;
		
 		$text = "SELECT setting.Setting_ID AS Setting_ID, setting.User_ID AS User_ID, setting.Setting_objSort AS Setting_objSort, 
 			setting.Setting_eduSort AS Setting_eduSort, setting.Setting_expSort AS Setting_expSort, setting.Setting_skillSort AS
 			Setting_skillSort, setting.Setting_certSort AS Setting_certSort, setting.Setting_langSort AS Setting_langSort,
 			setting.Setting_workSort AS Setting_workSort, setting.Setting_orgSort AS Setting_orgSort, setting.Setting_achSort AS Setting_achSort 
 			FROM $this->table setting LEFT JOIN T_User user ON user.User_ID = setting.User_ID WHERE user.User_username = '$username'";
 		$query = mysql_query($text);
		if(mysql_num_rows($query) >= 1){
			$result = array();
			while($row = mysql_fetch_array($query,MYSQL_ASSOC)){
				$result[]=$row;
			}
		}
		//$result = $text;
		return $result;
	}

	// check user
	public function check_user($user_id){//return 0 if not exist
		$data = 0;

		$text = "SELECT User_ID FROM $this->table WHERE User_ID = '$user_id' ";
		$query = mysql_query($text);
		if(mysql_num_rows($query) == 1){
			$row = mysql_fetch_array($query, MYSQL_ASSOC);
			$data = $row['User_ID'];
		}
		return $data;
	}//END CHECK-EXIST

	public function insert_data($user_id, $obj, $edu, $exp, $skill, $cert, $lang, $work){
		$result = 0;
		$text = "";

		$check_exist = $this->check_user($user_id);
		if($check_exist == 0){
			$text = "INSERT INTO $this->table (User_ID, Setting_objSort, Setting_eduSort, Setting_expSort, Setting_skillSort, Setting_certSort, Setting_langSort, Setting_workSort) VALUES('$user_id', '$obj', '$edu', '$exp', '$skill', '$cert', '$lang', '$work')";
		}
		$query = mysql_query($text);
		if($query){
			$result = 1;
		}
		//$result = $text;
		return $result;
	}

	/* Start process data function */
	/*public function process_data($user_id, $obj, $edu, $exp, $skill, $work, $resume, $resume_code, $portfolio, $portfolio_page){
		$result = 0;

		$check_exist = $this->check_user($user_id);
		if($check_exist == 0){
			$text = "INSERT INTO $this->table (User_ID, Setting_objSort, Setting_eduSort, Setting_expSort, Setting_skillSort, Setting_workSort, Setting_resume, Setting_resumeCode, Setting_portfolio, Setting_portfolioPage) VALUES('$user_id', '$obj', '$edu', '$exp', '$skill', '$work', '$resume', '$resume_code', '$portfolio', '$portfolio_page')";
		}else{
			$text = "UPDATE $this->table SET Setting_objSort = '$obj', Setting_eduSort = '$edu', Setting_expSort = '$exp', Setting_skillSort = '$skill', Setting_workSort = '$work', Setting_resume = '$resume', Setting_resumeCode = '$resume_code', Setting_portfolio = '$portfolio', Setting_portfolioPage = '$portfolio_page' WHERE User_ID = '$user_id'";
		}

		$query = mysql_query($text);
		if($query){
			$result = 1;
		}
		//$result = $text;
		return $result;
	}*/
	/* End function process data */

	/* Start save sort function */
	public function save_sort($user_id, $field, $order){
		$result = 0;

		$cond = "";
		if($field == "obj"){
			$cond = "Setting_objSort = '$order'";
		}else if($field == "edu"){
			$cond = "Setting_eduSort = '$order'";
		}else if($field == "exp"){
			$cond = "Setting_expSort = '$order'";
		}else if($field == "ski"){
			$cond = "Setting_skillSort = '$order'";
		}else if($field == "wor"){
			$cond = "Setting_workSort = '$order'";
		}else if($field == "cert"){
			$cond = "Setting_certSort = '$order'";
		}else if($field == "lang"){
			$cond = "Setting_langSort = '$order'";
		}else if($field == "org"){
			$cond = "Setting_orgSort = '$order'";
		}else if($field == "ach"){
			$cond = "Setting_achSort = '$order'";
		}
		   
		$text = "UPDATE $this->table SET $cond WHERE User_ID = '$user_id'";
		$query = mysql_query($text);
		if(mysql_affected_rows() >= 1){
			$result = 1;
		}
  		//$result = $text;
		return $result;
	}		
	/* End save sort function */

}
?>