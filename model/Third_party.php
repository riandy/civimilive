<?php 
require_once(dirname(__FILE__)."/../packages/check_input.php");
//start class
class Third_party{ 

    private $table = "T_User";

    public function check_username($username){//return 1 can be used
        $result = 0;//cannot use already exist
    
        $text = "SELECT User_username FROM $this->table WHERE User_username = '$username' ";
        $query = mysql_query($text);
        if(mysql_num_rows($query) < 1){
            $result = 1;//can be used
        }
        return $result;
    }

    public function check_primary_email($email){//return 1 can be used, for login facebook/google
        $result = 0;//cannot use already exist
    
        $text = "SELECT User_email FROM $this->table WHERE User_email = '$email' LIMIT 0,1";
        $query = mysql_query($text);
        if(mysql_num_rows($query) == 1){//HAS TO BE EXACT 1 RESULT
            $result = array();
            $row = mysql_fetch_array($query,MYSQL_ASSOC);
            $result[] = $row;
        }
        //$result = $text;
        return $result;
    }

    public function check_primary_username($username){//return 1 can be used, for login with twitter
        $result = 0;//cannot use already exist
    
        $text = "SELECT User_email, User_username FROM $this->table WHERE User_username = '$username' LIMIT 0,1";
        $query = mysql_query($text);
        if(mysql_num_rows($query) == 1){//HAS TO BE EXACT 1 RESULT
            $result = array();
            while($row = mysql_fetch_assoc($query)){
                $result[] = $row;
            }
        }
        //$result = $text;
        return $result;
    }

    public function user_login($email){
        $result = 0;//FAILED
        
        if($email != null){
            $text = "SELECT User_ID, User_username, User_email, User_auth_code FROM $this->table WHERE User_email = '$email' LIMIT 0,1";
            $query = mysql_query($text);
            if(mysql_num_rows($query) == 1){//HAS TO BE EXACT 1 RESULT
                $result = array();
                $row = mysql_fetch_array($query,MYSQL_ASSOC);
                $result[] = $row;
            }
        }
        return $result;
    }

    public function update_action_login($id){
        $result = 0;
       
        $text = "UPDATE $this->table SET User_lastLogin = NOW(), User_num_login = User_num_login + 1 WHERE User_ID = '$id'";
        $query = mysql_query($text);
        if(mysql_affected_rows() >= 1){
            $result = 1;
        }
        return $result;
    }

    //function insert for facebook/google
    public function insert_data($fname, $lname, $email, $via, $photo, $photo_thmb){
        $result = 0;

        $code = generate_code(32);
        $text = "INSERT INTO $this->table (User_fname, User_lname, User_email, User_login_via, User_proPhoto, User_proPhotoThmb, User_actStatus, User_auth_code, User_create_date) 
            VALUES ('$fname', '$lname', '$email', '$via', '$photo', '$photo_thmb', '1', '$code', NOW())";
        $query = mysql_query($text);
        if($query){
            $result = mysql_insert_id();
        }
        //$result = $text;
        return $result;
    }

    //function insert for twitter
    public function insert_data_username($fname, $lname, $city, $state, $country, $username, $via, $photo, $photo_thmb){
        $result = 0;

        $code = generate_code(32);
        $text = "INSERT INTO $this->table (User_fname, User_lname, User_city, User_state, User_country, User_username, User_login_via, User_proPhoto, User_proPhotoThmb, User_actStatus, User_auth_code, User_create_date) 
            VALUES ('$fname', '$lname', '$city', '$state', '$country', '$username', '$via', '$photo', '$photo_thmb', '1', '$code', NOW())";
        $query = mysql_query($text);
        if($query){
            $result = mysql_insert_id();
        }
        //$result = $text;
        return $result;
    }

    public function update_form_personal($id, $fname, $lname, $username, $email, $day, $month, $year, $country, $city, $state, $phone){
        $result = 0;

        $text = "UPDATE $this->table SET User_fname= '$fname', User_lname = '$lname', User_username = '$username', User_email = '$email', User_DOBday = '$day', User_DOBmonth = '$month', User_DOB = '$year', User_country = '$country', User_city = '$city', User_state = '$state', User_phone = '$phone', User_step_personal = 'Yes' WHERE User_ID = '$id'";
        $query = mysql_query($text);
        if(mysql_affected_rows() == 1){
            $result = 1;
        }
        //$result = $text;
        return $result;
    }

    public function update_auth_code($id){
        $result = 0;
        $code = generate_code(32);
        
        $text = "UPDATE $this->table SET User_auth_code = '$code' WHERE User_ID = '$id'";
        $query = mysql_query($text);
        if(mysql_affected_rows() >= 1){
            $result = 1;
        }
        return $code;
    }
}//end class
?>