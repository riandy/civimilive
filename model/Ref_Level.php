<?php 
class Ref_Level{

	private $table = "T_Ref_Level";
	private $publish = "Level_publish IN('Publish') "; 
	private $order = "ORDER BY Level_title ASC";
	private $itemPerPage = 20;

	//FUNCTION TO GET AMOUNT OF ALL PUBLISHED LEVEL IN DATABASE
	public function get_total($letter = null, $publish = 1){
		$result = 0;
		$cond = '';
		if($letter != null){
			$cond .="AND Level_title LIKE '$letter%' ";
		}
		if(isset($publish)){
			if($publish != 1){$this->publish = "Level_publish IN('Publish', 'Not Publish') ";}
		}
		$text = "SELECT count(Level_ID) AS count FROM $this->table WHERE $this->publish $cond ";
		$query = mysql_query($text);
		if(mysql_num_rows($query) >= 1){
			$row = mysql_fetch_array($query,MYSQL_ASSOC);
			$result =  $row['count'];
		}
		return $result;
	}

	public function get_total_page($total_data){
		$num_page = ceil($total_data / $this->itemPerPage);
		return $num_page;
	}	

	//FUNCTION TO GET data OF ALL PUBLISHED Level IN DATABASE
	//1 to set publish, 0 to get publish and not published data
	public function get_data_by_page($page=0, $letter=null, $publish=1){
		$data = "There is no result.";
		if($page <= 1 || $page == null){
			$limitBefore = 0;
		}else{
			$limitBefore = ($page-1) * $this->itemPerPage;
		}
		$cond = '';
		if($letter != null){
			$cond .="AND Level_title LIKE '$letter%' ";
		}
		if($publish != 1){
			$this->publish = "Level_publish IN('Publish', 'Not Publish') ";
		}
 		$text = "SELECT * FROM $this->table WHERE $this->publish $cond $this->order LIMIT $limitBefore, $this->itemPerPage";
 		$query = mysql_query($text);
		if(mysql_num_rows($query) >= 1){
			$data = array();
			while($row = mysql_fetch_array($query,MYSQL_ASSOC)){
				$data[]=$row;
			}
		}
		return $data;
	}

	//for api level
	public function get_list($title){
		$result = 0;

		$text = "SELECT Level_title AS label, Level_title AS value FROM $this->table WHERE $this->publish AND Level_title LIKE '$title%' OR Level_title LIKE '%$title' OR Level_title LIKE '%$title%' $this->order";
		$query = mysql_query($text);
		if(mysql_num_rows($query) >= 1){
			$result = array();
			while($row = mysql_fetch_assoc($query)){
				$result[] = $row;
			}
		}
		return $result;
	}

	// function for api and admin page
	public function get_index(){
		$result = 0;

		$text = "SELECT Level_ID, Level_title FROM $this->table ORDER BY Level_title DESC";
		$query = mysql_query($text);
		if(mysql_num_rows($query) >= 1){
			$result = array();
			while($row = mysql_fetch_assoc($query)){
				$result[] = $row;
			}
		}
		return $result;
	}

	public function get_data_edit($id){
		$result = 0;

		$text = "SELECT * FROM $this->table WHERE Level_ID = '$id'";
		$query = mysql_query($text);
		if(mysql_num_rows($query) >= 1){
			$result = array();
			while($row = mysql_fetch_assoc($query)){
				$result[] = $row;
			}
		}
		return $result;
	}

	public function insert_data($title, $content, $admin_id, $publish){
		$result = 0;

		$text = "INSERT INTO $this->table (Level_title, Level_content, Level_create_adminID, Level_publish, Level_create_date) VALUES ('$title', '$content', '$admin_id', '$publish', NOW())";
		$query = mysql_query($text);
		if($query){
			$result = 1; 
		}
		return $result;
	}

	public function update_data($id, $title, $content, $admin_id, $publish){
		$result = 0;

		$text = "UPDATE $this->table SET Level_title = '$title', Level_content = '$content', Level_modify_adminID = '$admin_id', Level_publish = '$publish' WHERE Level_ID = '$id'";
		$query = mysql_query($text);
		if(mysql_affected_rows() == 1){
			$result = 1;
		}
		return $result;
	}

	public function delete_data($id){
		$result = 0;

		$text = "DELETE FROM $this->table WHERE Level_ID = '$id'";
		$query = mysql_query($text);
		if(mysql_affected_rows() == 1){
			$result = 1;
		}
		return $result;
	}

	public function publish_data($id, $publish){
		$result = 0;

		$text = "UPDATE $this->table SET Level_publish = '$publish' WHERE Level_ID = '$id'";
		$query = mysql_query($text);
		if(mysql_affected_rows() == 1){
			$result = 1;
		}
		return $result;
	}

	public function unpublish_data($id){
		$result = 0;
		$not_publish = "Not Publish";

		$text = "UPDATE $this->table SET Level_publish = '$not_publish'  WHERE Level_ID = '$id'";
		$query = mysql_query($text);
		if(mysql_affected_rows() == 1){
			$result = 1;
		}
		return $result;
	}

}

?>