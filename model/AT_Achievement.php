<?php
class AT_Achievement{

	private $table = "AT_Achievement";
	//private $itemPerPage = 20;

// START FUNCTION FOR ADMIN PAGE
	//FUNCTION TO GET AMOUNT OF ALL PUBLISHED Student/Alumni IN DATABASE
	/*public function get_total($edu_id){
		$result = 0;
		$cond = '';
		if($letter != null){
			$cond .="AND Edu_schoolName LIKE '$letter%' ";
		}
		if(isset($publish)){
			if($publish != 1){$this->publish = "Edu_publish IN('Publish', 'Not Publish') ";}
		}
		$text = "SELECT COUNT(atedu.ATedu_ID) AS count FROM $this->table atedu LEFT JOIN T_Edu edu 
 			ON edu.Edu_ID = atedu.Edu_ID LEFT JOIN T_User user ON user.User_ID = atedu.User_ID 
 			WHERE edu.Edu_ID = '$edu_id'";
		$query = mysql_query($text);
		if(mysql_num_rows($query) >= 1){
			$row = mysql_fetch_array($query,MYSQL_ASSOC);
			$result =  $row['count'];
		}
		return $result;
	}

	public function get_total_page($total_data){
		$num_page = ceil($total_data / $this->itemPerPage);
		return $num_page;
	}	

	//FUNCTION TO GET data OF ALL PUBLISHED Student/Alumni IN DATABASE
	//1 to set publish, 0 to get publish and not published data
	public function get_data_by_edu($page=0, $edu_id){
		$data = "There is no result.";
		if($page <= 1 || $page == null){
			$limitBefore = 0;
		}else{
			$limitBefore = ($page-1) * $this->itemPerPage;
		}
 		
 		$text = "SELECT edu.Edu_ID AS Edu_ID, edu.Edu_schoolName AS Edu_schoolName, 
 			user.User_username AS User_username, user.User_ID AS User_ID, user.User_email AS User_email, 
 			user.User_proPhoto AS User_pic, user.User_proPhotoThmb AS User_pic_thmb, user.User_fname AS User_fname, 
 			user.User_lname AS User_lname, atedu.ATedu_ID AS ATedu_ID, 
 			CASE WHEN atedu.ATedu_fromMonth = 1 THEN 'Jan'
 				WHEN atedu.ATedu_fromMonth = 2 THEN 'Feb'
 				WHEN atedu.ATedu_fromMonth = 3 THEN 'Mar'
 				WHEN atedu.ATedu_fromMonth = 4 THEN 'Apr'
 				WHEN atedu.ATedu_fromMonth = 5 THEN 'May'
 				WHEN atedu.ATedu_fromMonth = 6 THEN 'Jun'
 				WHEN atedu.ATedu_fromMonth = 7 THEN 'Jul'
 				WHEN atedu.ATedu_fromMonth = 8 THEN 'Aug'
 				WHEN atedu.ATedu_fromMonth = 9 THEN 'Sep'
 				WHEN atedu.ATedu_fromMonth = 10 THEN 'Oct'
 				WHEN atedu.ATedu_fromMonth = 11 THEN 'Nov'
 				WHEN atedu.ATedu_fromMonth = 12 THEN 'Dec'
 			END AS ATedu_fromMonth,
 			atedu.ATedu_fromYear AS ATedu_fromYear, 
 			CASE WHEN atedu.ATedu_toMonth = 1 THEN 'Jan'
 				WHEN atedu.ATedu_toMonth = 2 THEN 'Feb'
 				WHEN atedu.ATedu_toMonth = 3 THEN 'Mar'
 				WHEN atedu.ATedu_toMonth = 4 THEN 'Apr'
 				WHEN atedu.ATedu_toMonth = 5 THEN 'May'
 				WHEN atedu.ATedu_toMonth = 6 THEN 'Jun'
 				WHEN atedu.ATedu_toMonth = 7 THEN 'Jul'
 				WHEN atedu.ATedu_toMonth = 8 THEN 'Aug'
 				WHEN atedu.ATedu_toMonth = 9 THEN 'Sep'
 				WHEN atedu.ATedu_toMonth = 10 THEN 'Oct'
 				WHEN atedu.ATedu_toMonth = 11 THEN 'Nov'
 				WHEN atedu.ATedu_toMonth = 12 THEN 'Dec'
 			END AS ATedu_toMonth,
 			atedu.ATedu_toYear AS ATedu_toYear, 
 			atedu.ATedu_degree AS ATedu_degree, atedu.ATedu_major AS ATedu_major, 
 			atedu.ATedu_GPA AS ATedu_GPA FROM $this->table atedu LEFT JOIN T_Edu edu 
 			ON edu.Edu_ID = atedu.Edu_ID LEFT JOIN T_User user ON user.User_ID = atedu.User_ID 
 			WHERE edu.Edu_ID = '$edu_id' ORDER BY user.User_fname LIMIT $limitBefore, $this->itemPerPage";
 		$query = mysql_query($text);
		if(mysql_num_rows($query) >= 1){
			$data = array();
			while($row = mysql_fetch_array($query,MYSQL_ASSOC)){
				$data[]=$row;
			}
		}
		//$data = $text;
		return $data;
	}

	// function index
	public function get_index($edu_id){
		$result = 0;

		$text = "SELECT user.User_email, user.User_fname AS User_fname, user.User_lname AS User_lname, atedu.Edu_ID AS Edu_ID,
			atedu.ATedu_ID AS ATedu_ID, atedu.ATedu_fromMonth AS ATedu_fromMonth,atedu.ATedu_fromYear AS ATedu_fromYear,atedu.ATedu_toMonth AS ATedu_toMonth,atedu.ATedu_toYear AS ATedu_toYear, atedu.ATedu_major AS ATedu_major, atedu.ATedu_degree AS ATedu_degree, atedu.ATedu_GPA AS ATedu_GPA, atedu.ATedu_status AS ATedu_status, atedu.ATedu_desc AS ATedu_desc FROM $this->table atedu LEFT JOIN T_Edu edu 
 			ON edu.Edu_ID = atedu.Edu_ID LEFT JOIN T_User user ON user.User_ID = atedu.User_ID 
 			WHERE edu.Edu_ID = '$edu_id'";
		$query = mysql_query($text);
		if(mysql_num_rows($query) >= 1){
			$result = array();
			while($row = mysql_fetch_assoc($query)){
				$result[] = $row;
			}
		}
		return $result;
	}

	public function get_data_detail($id){
		$result = 0;
		
 		$text = "SELECT edu.Edu_ID AS Edu_ID, edu.Edu_schoolName AS Edu_schoolName, 
 			edu.Edu_schoolCountry AS Edu_schoolCountry, edu.Edu_schoolCity AS Edu_schoolCity,
 			user.User_ID AS User_ID, user.User_proPhoto AS User_pic, user.User_email,
 			user.User_proPhotoThmb AS User_pic_thmb, user.User_fname AS User_fname, 
 			user.User_lname AS User_lname, atedu.ATedu_ID AS ATedu_ID, atedu.ATedu_desc AS ATedu_desc, 
 			atedu.ATedu_fromMonth AS ATedu_fromMonth, atedu.ATedu_fromYear AS ATedu_fromYear, 
 			atedu.ATedu_toMonth AS ATedu_toMonth, atedu.ATedu_toYear AS ATedu_toYear, 
 			atedu.ATedu_degree AS ATedu_degree, atedu.ATedu_major AS ATedu_major, 
 			atedu.ATedu_GPA AS ATedu_GPA FROM $this->table atedu LEFT JOIN T_Edu edu 
 			ON edu.Edu_ID = atedu.Edu_ID LEFT JOIN T_User user ON user.User_ID = atedu.User_ID 
 			WHERE atedu.ATedu_ID = '$id'";
 		$query = mysql_query($text);
		if(mysql_num_rows($query) >= 1){
			$result = array();
			while($row = mysql_fetch_array($query,MYSQL_ASSOC)){
				$result[]=$row;
			}
		}
		//$result = $text;
		return $result;
	}*/
// END FUNCTION FOR ADMIN PAGE

// START FUNCTION FOR CLIENT PAGE
	/* Start insert function */
	public function insert_data($user_id, $title, $source, $source_id, $institute, $year, $desc, $publish){
		$result = 0;

		$text = "INSERT INTO $this->table (User_ID, ATachieve_title, ATachieve_source, ATachieve_sourceID, ATachieve_institute, ATachieve_year, ATachieve_desc, ATachieve_publish, ATachieve_create_date) VALUES ('$user_id', '$title', '$source', '$source_id', '$institute', '$year', '$desc', '$publish', NOW())";
		$query = mysql_query($text);
		if($query){
			$result = mysql_insert_id();
		}
		return $result;
	}
	/* End function insert */

	/* Fungsi Update */
	public function update_data($id, $title, $source, $source_id, $institute, $year, $desc, $publish){
		$result = 0;

		$text = "UPDATE $this->table SET ATachieve_title = '$title', ATachieve_source = '$source', ATachieve_sourceID = '$source_id', ATachieve_institute = '$institute', ATachieve_year = '$year', ATachieve_desc = '$desc', ATachieve_publish = '$publish' WHERE ATachieve_ID = '$id' ";
		$query = mysql_query($text);
		if(mysql_affected_rows() == 1){
			$result = 1;
		}
		//$result = $text;
		return $result;
	}
	/* End Fungsi Update */

	/* 	Fungsi Delete */
	public function delete_data($id){
		$result = 0;

		$text = "DELETE FROM $this->table WHERE ATachieve_ID = '$id' ";
		$query = mysql_query($text);
		if(mysql_affected_rows() == 1){
			$result = 1;
		}
		return $result;
	}
	/* 	End Delete */

	//for api get data
	public function get_data($user_id){
		$result = 0;

		$text = "SELECT ATachieve_ID, ATachieve_title, ATachieve_year, ATachieve_desc, 
			ATachieve_create_date, ATachieve_source, ATachieve_sourceID, ATachieve_publish, 
			(CASE WHEN ATachieve_source = 'education' THEN (SELECT Edu_schoolName FROM T_Edu WHERE Edu_ID = ATachieve_sourceID) 
			WHEN ATachieve_source = 'company' THEN (SELECT Company_title FROM T_Company WHERE Company_ID = ATachieve_sourceID) 
			WHEN ATachieve_source = 'organization' THEN (SELECT Org_title FROM T_Organization WHERE Org_ID = ATachieve_sourceID) 
			ELSE ATachieve_institute END) AS institute_name FROM $this->table WHERE User_ID = '$user_id' AND 
			ATachieve_publish = 'Publish' ORDER BY ATachieve_year DESC, ATachieve_create_date DESC";
		$query = mysql_query($text);
		if(mysql_num_rows($query) >= 1){
			$result = array();
			while($row = mysql_fetch_assoc($query)){
				$result[] = $row;
			}
		}
		//$result = $text;
		return $result;
	}

	//for api get list
	public function get_list($source, $title){
		$result = 0;

		switch ($source) {
			case 'education':
				$text = "SELECT Edu_id AS Achieve_ID, Edu_schoolName AS Achieve_name, Edu_schoolCity AS Achieve_city, 
					Edu_schoolCountry AS Achieve_country, Edu_img AS Achieve_img,
					IF(Edu_schoolName LIKE '$title%', 20, IF(Edu_schoolName LIKE '%$title%', 15, 
						IF(Edu_schoolName LIKE '%$title', 10, 0))) AS weight
					FROM T_Edu WHERE Edu_publish = 'Publish' AND Edu_schoolName LIKE '$title%' OR 
					Edu_schoolName LIKE '%$title' OR Edu_schoolName LIKE '%$title%' 
					ORDER BY weight DESC, Edu_schoolName ASC";
			break;

			case 'company':
				$joinCity = "LEFT JOIN T_Ref_City ON Company_ref_cityID = City_ID ";
				$joinCountry = "LEFT JOIN T_Ref_Country ON Company_ref_countryID = Country_ID ";
				$text = "SELECT Company_ID AS Achieve_ID, Company_title AS Achieve_name, Company_img AS Achieve_img, 
					City_title AS Achieve_city, 
					Country_title AS Achieve_country,
					IF(Company_title LIKE '$title%', 20, IF(Company_title LIKE '%$title%', 15, 
						IF(Company_title LIKE '%$title', 10, 0))) AS weight
					FROM T_Company $joinCity $joinCountry
					WHERE Company_publish = 'Publish' AND Company_title LIKE '$title%' OR 
					Company_title LIKE '%$title' OR Company_title LIKE '%$title%' 
					ORDER BY weight DESC, Company_title ASC";
			break;

			case 'organization':
				$joinCity = "LEFT JOIN T_Ref_City ON Org_ref_cityID = City_ID ";
				$joinCountry = "LEFT JOIN T_Ref_Country ON Org_ref_countryID = Country_ID ";
				$text = "SELECT Org_ID AS Achieve_ID, Org_title AS Achieve_name, Org_img AS Achieve_img, 
					City_title AS Achieve_city, Country_title AS Achieve_country,
					IF(Org_title LIKE '$title%', 20, IF(Org_title LIKE '%$title%', 15, 
						IF(Org_title LIKE '%$title', 10, 0))) AS weight
					FROM T_Organization $joinCity $joinCountry
					WHERE Org_publish = 'Publish' AND Org_title LIKE '$title%' OR 
					Org_title LIKE '%$title' OR Org_title LIKE '%$title%' 
					ORDER BY weight DESC, Org_title ASC";
			break;
			
			default:
				/*$text = "SELECT ATachieve_ID, ATachieve_institute,
					IF(ATachieve_institute LIKE '$title%', 20, IF(ATachieve_institute LIKE '%$title%', 15, 
						IF(ATachieve_institute LIKE '%$title', 10, 0))) AS weight
					FROM $this->table WHERE ATachieve_publish = 'Publish' AND ATachieve_institute LIKE '$title%' OR 
					ATachieve_institute LIKE '%$title' OR ATachieve_institute LIKE '%$title%' 
					ORDER BY weight DESC, ATachieve_title ASC";*/
			break;
		}
		
		$query = mysql_query($text);
		if(mysql_num_rows($query) >= 1){
			$result = array();
			while($row = mysql_fetch_assoc($query)){
				$result[] = $row;
			}
		}
		//$result = $text;
		return $result;
	}

	//for api check source company/organization/education
	public function check_source($source, $institute){//return 0 if not exist
		$data = 0;

		if($source == "education"){
			$text = "SELECT Edu_ID FROM T_Edu WHERE Edu_schoolName = '$institute' ORDER BY Edu_ID ASC LIMIT 0,1";
		}else if($source == "company"){
			$text = "SELECT Company_ID FROM T_Company WHERE Company_title = '$institute' ORDER BY Company_ID ASC LIMIT 0,1";
		}else if($source == "organization"){
			$text = "SELECT Org_ID FROM T_Organization WHERE Org_title = '$institute' ORDER BY Org_ID ASC LIMIT 0,1";
		}

		$query = mysql_query($text);
		if(mysql_num_rows($query) == 1){
			$row = mysql_fetch_array($query, MYSQL_ASSOC);
			if($source == "education"){
				$data = $row['Edu_ID'];
			}else if($source == "company"){
				$data = $row['Company_ID'];
			}else if($source == "organization"){
				$data = $row['Org_ID'];
			}
		}
		return $data;
	}//END CHECK-EXIST

	//function insert for source company/organization/education
	public function insert_source($source, $institute){
		$result = 0;

		if($source == "education"){
			$text = "INSERT INTO T_Edu (Edu_schoolName, Edu_publish, Edu_create_date) VALUES ('$institute', 'Publish', NOW())";
		}else if($source == "company"){
			$text = "INSERT INTO T_Company (Company_title, Company_publish, Company_create_date) VALUES ('$institute', 'Publish', NOW())";
		}else if($source == "organization"){
			$text = "INSERT INTO T_Organization (Org_title, Org_publish, Org_create_date) VALUES ('$institute', 'Publish', NOW())";
		}

		$query = mysql_query($text);
		if($query){
			$result = mysql_insert_id();
		}
		//$result = $text;
		return $result;
	}
// END FUNCTION FOR CLIENT PAGE
}
?>