<?php
class Web{

	private $table = "T_Web";
	private $tableWc = "T_WebCat";

// START FUNCTION FOR API
	/* Start insert function */
	public function insert_data($link, $title, $desc, $webcat_id, $user_id, $publish){
		$result = 0;

		$text = "INSERT INTO $this->table (Web_link, Web_title, Web_desc, WebCat_ID, User_ID, Web_publish, Web_create_date)VALUES('$link', '$title', '$desc', '$webcat_id', '$user_id', '$publish', NOW())";
		$query = mysql_query($text);
		if($query){
			$result = mysql_insert_id();
		}
		return $result;
	}
	/* End function insert */

	/* Fungsi Update */
	public function update_data($id, $link, $title, $desc, $webcat_id){
		$result = 0;

		$text = "UPDATE $this->table SET Web_link = '$link', Web_title = '$title', Web_desc = '$desc', WebCat_ID = '$webcat_id' WHERE Web_ID = '$id' ";
		$query = mysql_query($text);
		if(mysql_affected_rows() == 1){
			$result = 1;
		}
		return $result;
	}
	/* End Fungsi Update */

	/* 	Fungsi GET DATA */
	public function get_data_recent($user_id){
		$result = 0;

		$text = "SELECT Web_ID, Web_desc, Web_title, Web_link, WebCat_name, Web_create_date, Web_modify_date 
			FROM $this->table web LEFT JOIN $this->tableWc wc ON wc.WebCat_ID = web.WebCat_ID 
			WHERE web.User_ID = '$user_id' AND Web_publish = 'Publish' AND WebCat_publish = 'Publish' 
			ORDER BY Web_modify_date DESC LIMIT 0,12";
		$query = mysql_query($text);
		if(mysql_num_rows($query) >= 1){
			$result = array();
			while($row = mysql_fetch_assoc($query)){
				$result[] = $row;
			}
		}
		return $result;
	}

	public function get_data_by_web_cat($webcat_id){
		$result = 0;

		$text = "SELECT Web_ID, Web_link, Web_title, Web_desc FROM $this->table WHERE WebCat_ID = '$webcat_id' 
			AND Web_publish = 'Publish' ORDER BY Web_modify_date DESC";
		$query = mysql_query($text);
		if(mysql_num_rows($query) >= 1){
			$result = array();
			while($row = mysql_fetch_assoc($query)){
				$result[] = $row;
			}
		}
		return $result;
	}

	public function get_data_detail($id){
        $result = 0;
        
        $text = "SELECT wc.WebCat_ID AS WebCat_ID, wc.WebCat_name AS WebCat_name, web.Web_ID AS Web_ID, 
        	web.Web_title AS Web_title, web.Web_desc AS Web_desc, web.Web_link AS Web_link, 
        	web.User_ID AS User_ID, web.Web_publish AS Web_publish FROM $this->table web LEFT JOIN 
        	$this->tableWc wc ON wc.WebCat_ID = web.WebCat_ID WHERE web.Web_ID = '$id'";  
        $query = mysql_query($text);
        if(mysql_num_rows($query) >= 1){
            $result = array();
            while($row = mysql_fetch_array($query,MYSQL_ASSOC)){
                $result[]=$row;
            }
        }
        //$result = $text;
        return $result;
    }
	/* 	End Fungsi GET DATA */

	/* 	Fungsi Delete */
	public function delete_data($id){
		$result = 0;

		$text = "DELETE FROM $this->table WHERE Web_ID = '$id' ";
		$query = mysql_query($text);
		if(mysql_affected_rows() == 1){
			$result = 1;
		}
		return $result;
	}
	/* 	End Delete */

	//function get data by user for resume
	public function get_data_resume($user_id){
        $result = 0;
        
        $text = "SELECT wc.WebCat_ID AS WebCat_ID, wc.WebCat_name AS WebCat_name, web.Web_ID AS Web_ID, 
        	web.Web_title AS Web_title, web.Web_desc AS Web_desc, web.Web_link AS Web_link, 
        	web.User_ID AS User_ID, web.Web_publish AS Web_publish FROM $this->table web LEFT JOIN 
        	$this->tableWc wc ON wc.WebCat_ID = web.WebCat_ID WHERE web.User_ID = '$user_id' AND
        	web.Web_publish = 'Publish' ORDER BY Web_create_date DESC LIMIT 0,6";  
        $query = mysql_query($text);
        if(mysql_num_rows($query) >= 1){
            $result = array();
            while($row = mysql_fetch_array($query,MYSQL_ASSOC)){
                $result[]=$row;
            }
        }
        //$result = $text;
        return $result;
    }
// END FUNCTION API
}
?>