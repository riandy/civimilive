<?php 
class Ref_Tag{

	private $table = "T_Ref_Tag";
	private $publish = "Tag_publish IN('Publish') "; 
	private $order = "ORDER BY Tag_title ASC";
	private $itemPerPage = 20;

	//FUNCTION TO GET AMOUNT OF ALL PUBLISHED Disease IN DATABASE
	public function get_total($letter = null, $publish = 1){
		$result = 0;
		$cond = '';
		if($letter != null){
			$cond .="AND Tag_title LIKE '$letter%' ";
		}
		if(isset($publish)){
			if($publish != 1){$this->publish = "Tag_publish IN('Publish', 'Not Publish') ";}
		}
		$text = "SELECT count(Tag_ID) AS count FROM $this->table WHERE $this->publish $cond ";
		$query = mysql_query($text);
		if(mysql_num_rows($query) >= 1){
			$row = mysql_fetch_array($query,MYSQL_ASSOC);
			$result =  $row['count'];
		}
		return $result;
	}

	public function get_total_page($total_data){
		$num_page = ceil($total_data / $this->itemPerPage);
		return $num_page;
	}	

	//FUNCTION TO GET data OF ALL PUBLISHED Country IN DATABASE
	//1 to set publish, 0 to get publish and not published data
	public function get_data_by_page($page=0, $letter=null, $publish=1){
		$data = "There is no result.";
		if($page <= 1 || $page == null){
			$limitBefore = 0;
		}else{
			$limitBefore = ($page-1) * $this->itemPerPage;
		}
		$cond = '';
		if($letter != null){
			$cond .="AND Tag_title LIKE '$letter%' ";
		}
		if($publish != 1){
			$this->publish = "Tag_publish IN('Publish', 'Not Publish') ";
		}
 		$text = "SELECT * FROM $this->table WHERE $this->publish $cond $this->order LIMIT $limitBefore, $this->itemPerPage";
 		$query = mysql_query($text);
		if(mysql_num_rows($query) >= 1){
			$data = array();
			while($row = mysql_fetch_array($query,MYSQL_ASSOC)){
				$data[]=$row;
			}
		}
		return $data;
	}

	public function get_data_edit($id){
		$result = 0;

		$text = "SELECT * FROM $this->table WHERE Tag_ID = '$id'";
		$query = mysql_query($text);
		if(mysql_num_rows($query) >= 1){
			$result = array();
			while($row = mysql_fetch_assoc($query)){
				$result[] = $row;
			}
		}
		return $result;
	}

	/*public function insert_data($title, $publish){
		$result = 0;

		$text = "INSERT INTO $this->table (Tag_title, Tag_publish, Tag_create_date) VALUES ('$title', '$publish', NOW())";
		$query = mysql_query($text);
		if($query){
			$result = 1; 
		}
		return $result;
	}*/
	public function insert_data($title){
		$data = 0;
		$text = "INSERT INTO $this->table (Tag_title, Tag_publish, Tag_create_date) VALUES ('$title', 'Publish', NOW())";
		$query = mysql_query($text);
		if($query){
			$data = mysql_insert_id();
		}
		return $data;
	}

	public function update_data($id, $title, $publish){
		$result = 0;

		$text = "UPDATE $this->table SET Tag_title = '$title', Tag_publish = '$publish' WHERE Tag_ID = '$id'";
		$query = mysql_query($text);
		if(mysql_affected_rows() == 1){
			$result = 1;
		}
		return $result;
	}

	public function delete_data($id){
		$result = 0;

		$text = "DELETE FROM $this->table WHERE Tag_ID = '$id'";
		$query = mysql_query($text);
		if(mysql_affected_rows() == 1){
			$result = 1;
		}
		return $result;
	}

	public function publish_data($id, $publish){
		$result = 0;

		$text = "UPDATE $this->table SET Tag_publish = '$publish' WHERE Tag_ID = '$id'";
		$query = mysql_query($text);
		if(mysql_affected_rows() == 1){
			$result = 1;
		}
		return $result;
	}

	public function unpublish_data($id){
		$result = 0;
		$not_publish = "Not Publish";

		$text = "UPDATE $this->table SET Tag_publish = '$not_publish'  WHERE Tag_ID = '$id'";
		$query = mysql_query($text);
		if(mysql_affected_rows() == 1){
			$result = 1;
		}
		return $result;
	}

	public function get_list($title){//START GET_LIST
		$data = 0;
		$text = "SELECT Tag_title AS label, Tag_title AS value FROM $this->table WHERE $this->publish AND Tag_title LIKE '$title%' OR Tag_title LIKE '%$title' OR Tag_title LIKE '%$title%' ORDER BY Tag_title ASC";
		$query = mysql_query($text);
		if(mysql_num_rows($query) >= 1){
			$data = array();
			while($row = mysql_fetch_array($query,MYSQL_ASSOC)){
				$data[] = $row;
			}
		}
		return $data;
	}//END GET_LIST

}

?>