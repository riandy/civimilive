<?php 
class Tagging{

	private $table = "AT_Tagging";

	//function for get tags in job_detail.php
	public function get_tags($tablename, $table_id){
		$result = 0;

		$text = "SELECT Tag_ID, Tag_title, Tagging_ref_tableID FROM AT_Tagging LEFT JOIN T_Ref_Tag ON Tagging_ref_tagID = Tag_ID WHERE Tagging_ref_table = '$tablename' AND Tagging_ref_tableID = '$table_id' ORDER BY Tag_title ASC";
		$query = mysql_query($text);
		if(mysql_num_rows($query) >= 1){
			$result = array();
			while($row = mysql_fetch_assoc($query)){
				$result[] = $row;
			}
		}
		return $result;
	}

	public function get_data_detail($tablename, $table_id){
		$result = "";

		$text = "SELECT Tag_title FROM $this->table LEFT JOIN T_Ref_Tag ON Tagging_ref_tagID = Tag_ID WHERE Tagging_ref_table = '$tablename' AND Tagging_ref_tableID = '$table_id' ORDER BY Tag_title ASC";
		$query = mysql_query($text);
		if(mysql_num_rows($query) >= 1){
			while($row = mysql_fetch_assoc($query)){
				if($result != null){$result .= ",";}
				$result .= $row['Tag_title'];
			}
		}
		return $result;
	}

	public function get_data($tablename, $table_id){//START GET DATA
		$data = "";

		if(isset($table_id) AND isset($tablename)){//VALIDATE ID AND TABLE
			$text = "SELECT Tagging_ref_tableID, Tagging_ref_table, Tag_ID, Tag_title FROM $this->table LEFT JOIN T_Ref_Tag ON Tagging_ref_tagID = Tag_ID WHERE Tagging_ref_table = '$tablename' AND Tagging_ref_tableID = '$table_id' ORDER BY Tag_title ASC";
			//$text = "SELECT rtag.title AS 'rtag_title', rtag.id AS 'rtag_id' FROM $this->join_table WHERE $this->publish AND tag.target_id = '$id' AND tag.tables = '$table' ORDER BY rtag.title ASC";
			$query = mysql_query($text);
			if(mysql_num_rows($query) >= 1){
				// $data = array();
				// while($row = mysql_fetch_array($query,MYSQL_ASSOC)){
				// 	$data[] = $row;
				// }
				while($row = mysql_fetch_array($query,MYSQL_ASSOC)){
					$data .= $row['Tag_title']." ,";
				}
			}
		}//END IF VALIDATE
		return $data;
	}//END FUNCTION GET DATA

	public function check_exist($title){//return 0 if not exist
		$data = 0;
			$text = "SELECT Tag_ID FROM T_Ref_Tag WHERE Tag_title = '$title' ";
			$query = mysql_query($text);
			if(mysql_num_rows($query) == 1){
				$row = mysql_fetch_array($query, MYSQL_ASSOC);
				$data = $row['Tag_ID'];
			}
		return $data;
	}//END CHECK-EXIST

	public function insert_data($table, $table_id, $tag_id){
		$data = 0;

		$text = "INSERT INTO $this->table (Tagging_ref_table, Tagging_ref_tableID, Tagging_ref_tagID, Tagging_create_date) VALUES ('$table', '$table_id', '$tag_id', NOW())";
		$query = mysql_query($text);
		if($query){
			$data = 1; 
		}
		return $data;
	}

	public function delete_data($table_id, $table_name){//DELETE TAGS FIRST
		$data = 0;
			$text = "DELETE FROM $this->table WHERE Tagging_ref_tableID = '$table_id' AND Tagging_ref_table = '$table_name'";
			$query = mysql_query($text);
			if(mysql_affected_rows() >= 1){
				$data = 1;
			}
		return $data;
	}//END delete_data

	//function for api get data tags
	public function get_data_tags($tablename, $table_id){//START GET DATA TAGS
		$result = 0;

		$text = "SELECT Tagging_ref_tableID, Tagging_ref_table, Tag_ID, Tag_title FROM $this->table LEFT JOIN T_Ref_Tag ON 
			Tagging_ref_tagID = Tag_ID WHERE Tagging_ref_table = '$tablename' AND Tagging_ref_tableID = '$table_id' ORDER BY 
			Tag_title ASC";
		$query = mysql_query($text);
		if(mysql_num_rows($query) >= 1){
			$result = array();
			while($row = mysql_fetch_array($query,MYSQL_ASSOC)){
				$result[] = $row;
			}
		}
		
		//$result = $text;
		return $result;
	}//END FUNCTION GET DATA TAGS
}

?>