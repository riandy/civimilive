<?php
class WebCat{

	private $table = "T_WebCat";
	private $tableWeb = "T_Web";

// START FUNCTION FOR API
	//FUNCTION TO GET all web category by the user
    public function get_web_cat($user_id){
        $result = 0;
        
        $text = "SELECT User_ID, WebCat_ID, WebCat_name, WebCat_desc FROM $this->table 
        	WHERE User_ID = '$user_id'";
        $query = mysql_query($text);
        if(mysql_num_rows($query) >= 1){
            $result = array();
            while($row = mysql_fetch_array($query,MYSQL_ASSOC)){
                $result[]=$row;
            }
        }
        //$result = $text;
        return $result;
    }

    public function get_data_detail($id){
		$result = 0;

		$text = "SELECT WebCat_ID, WebCat_name, WebCat_desc FROM $this->table WHERE WebCat_ID = '$id' ";
		$query = mysql_query($text);
		if(mysql_num_rows($query) >= 1){
			$result = array();
			while($row = mysql_fetch_assoc($query)){
				$result[] = $row;
			}
		}
		return $result;
	}

	/* Start insert function */
	public function insert_data($user_id, $name, $desc, $publish){
		$result = 0;

		$text = "INSERT INTO $this->table (User_ID, WebCat_name, WebCat_desc, WebCat_publish, WebCat_create_date)VALUES('$user_id', '$name', '$desc', '$publish', NOW())";
		$query = mysql_query($text);
		if($query){
			$result = mysql_insert_id();
		}
		return $result;
	}
	/* End function insert */

	/* Fungsi Update */
	public function update_data($id, $name, $desc){
		$result = 0;

		$text = "UPDATE $this->table SET WebCat_name = '$name', WebCat_desc = '$desc' WHERE WebCat_ID = '$id' ";
		$query = mysql_query($text);
		if(mysql_affected_rows() == 1){
			$result = 1;
		}
		return $result;
	}
	/* End Fungsi Update */

	/* 	Fungsi Delete */
	public function delete_data($id){
		$result = 0;

		$this->delete_data_related($id);

		$text = "DELETE FROM $this->table WHERE WebCat_ID = '$id' ";
		$query = mysql_query($text);
		if(mysql_affected_rows() == 1){
			$result = 1;
		}
		return $result;
	}
	/* 	End Delete */

	 /*  Fungsi Delete */
    public function delete_data_related($id){
        $result = 0;

        $text = "DELETE FROM $this->tableWeb WHERE WebCat_ID = '$id' ";
        $query = mysql_query($text);
        if(mysql_affected_rows() == 1){
            $result = 1;
        }
        return $result;
    }
    /*  End Delete */
// END FUNCTION API
}
?>