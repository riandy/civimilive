<?php 
class Industry{

	private $table = "T_Industry";
	private $publish = "Industry_publish IN('Publish') "; 
	private $order = "ORDER BY Industry_title ASC";
	private $itemPerPage = 20;

	//FUNCTION TO GET AMOUNT OF ALL PUBLISHED INDUSTRY IN DATABASE
	public function get_total($letter = null, $publish = 1){
		$result = 0;
		$cond = '';
		if($letter != null){
			$cond .="AND Industry_title LIKE '$letter%' ";
		}
		if(isset($publish)){
			if($publish != 1){$this->publish = "Industry_publish IN('Publish', 'Not Publish') ";}
		}
		$text = "SELECT count(Industry_ID) AS count FROM $this->table WHERE $this->publish $cond ";
		$query = mysql_query($text);
		if(mysql_num_rows($query) >= 1){
			$row = mysql_fetch_array($query,MYSQL_ASSOC);
			$result =  $row['count'];
		}
		return $result;
	}

	public function get_total_page($total_data){
		$num_page = ceil($total_data / $this->itemPerPage);
		return $num_page;
	}	

	//FUNCTION TO GET data OF ALL PUBLISHED Industry IN DATABASE
	//1 to set publish, 0 to get publish and not published data
	public function get_data_by_page($page=0, $letter=null, $publish=1){
		$data = "There is no result.";
		if($page <= 1 || $page == null){
			$limitBefore = 0;
		}else{
			$limitBefore = ($page-1) * $this->itemPerPage;
		}
		$cond = '';
		if($letter != null){
			$cond .="AND Industry_title LIKE '$letter%' ";
		}
		if($publish != 1){
			$this->publish = "Industry_publish IN('Publish', 'Not Publish') ";
		}
 		$text = "SELECT * FROM $this->table WHERE $this->publish $cond $this->order LIMIT $limitBefore, $this->itemPerPage";
 		$query = mysql_query($text);
		if(mysql_num_rows($query) >= 1){
			$data = array();
			while($row = mysql_fetch_array($query,MYSQL_ASSOC)){
				$data[]=$row;
			}
		}
		return $data;
	}

	// function for api and admin page
	public function get_index(){
		$result = 0;

		$text = "SELECT Industry_ID, Industry_title FROM $this->table ORDER BY Industry_ID DESC";
		$query = mysql_query($text);
		if(mysql_num_rows($query) >= 1){
			$result = array();
			while($row = mysql_fetch_assoc($query)){
				$result[] = $row;
			}
		}
		return $result;
	}

	//for api industry
	public function get_list($title){
		$result = 0;

		$text = "SELECT Industry_title AS label, Industry_title AS value FROM $this->table WHERE $this->publish AND Industry_title LIKE '$title%' OR Industry_title LIKE '%$title' OR Industry_title LIKE '%$title%' $this->order";
		$query = mysql_query($text);
		if(mysql_num_rows($query) >= 1){
			$result = array();
			while($row = mysql_fetch_assoc($query)){
				$result[] = $row;
			}
		}
		return $result;
	}

	/*public function get_data($page = 1){
		$result = 0;
		//get total data			
		$text_total = "SELECT * FROM $this->table";
		$query_total = mysql_query($text_total);
		$total_data = mysql_num_rows($query_total);

		if($total_data < 1){
			$total_data = 0;
		}

		//get total page
		$total_page = ceil($total_data / $this->itemPerPage);

		if($page <= 1 || $page == null){
			$page_request = 0;
		}else{
			$page_request = ($page-1) * $this->itemPerPage;
		}

		$text = "SELECT * FROM $this->table ORDER BY Industry_ID DESC LIMIT $page_request, $this->itemPerPage";
		$query = mysql_query($text);
		if(mysql_num_rows($query) >= 1){
			$result = array();
			while($row = mysql_fetch_assoc($query)){
				$result[] = $row;
			}
		}
		if(is_array($result)){	
			$result[0]['total_page'] = $total_page;					
			$result[0]['total_data_all'] = $total_data;
			$result[0]['total_data'] = count($result);
		}
		return $result;
	}*/

	public function get_data_edit($id){
		$result = 0;

		$text = "SELECT * FROM $this->table WHERE Industry_ID = '$id'";
		$query = mysql_query($text);
		if(mysql_num_rows($query) >= 1){
			$result = array();
			while($row = mysql_fetch_assoc($query)){
				$result[] = $row;
			}
		}
		return $result;
	}

	public function insert_data($title, $content, $admin_id, $publish){
		$result = 0;

		$text = "INSERT INTO $this->table (Industry_title, Industry_content, Industry_create_adminID, Industry_publish, Industry_create_date) VALUES ('$title', '$content', '$admin_id', '$publish', NOW())";
		$query = mysql_query($text);
		if($query){
			$result = 1; 
		}
		return $result;
	}

	public function update_data($id, $title, $content, $admin_id, $publish){
		$result = 0;

		$text = "UPDATE $this->table SET Industry_title = '$title', Industry_content = '$content', Industry_modify_adminID = '$admin_id', Industry_publish = '$publish' WHERE Industry_ID = '$id'";
		$query = mysql_query($text);
		if(mysql_affected_rows() == 1){
			$result = 1;
		}
		return $result;
	}

	public function delete_data($id){
		$result = 0;

		$text = "DELETE FROM $this->table WHERE Industry_ID = '$id'";
		$query = mysql_query($text);
		if(mysql_affected_rows() == 1){
			$result = 1;
		}
		return $result;
	}

	public function publish_data($id, $publish){
		$result = 0;

		$text = "UPDATE $this->table SET Industry_publish = '$publish' WHERE Industry_ID = '$id'";
		$query = mysql_query($text);
		if(mysql_affected_rows() == 1){
			$result = 1;
		}
		return $result;
	}

	public function unpublish_data($id){
		$result = 0;
		$not_publish = "Not Publish";

		$text = "UPDATE $this->table SET Industry_publish = '$not_publish'  WHERE Indsutry_ID = '$id'";
		$query = mysql_query($text);
		if(mysql_affected_rows() == 1){
			$result = 1;
		}
		return $result;
	}

}

?>