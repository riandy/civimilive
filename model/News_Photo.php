<?php
class News_Photo{

	private $table = "T_News_Photo";

    public function insert_data($img, $img_thmb, $news_id, $main){
        $result = 0;

        $text = "INSERT INTO $this->table (Np_img, Np_img_thmb, Np_ref_newsID, Np_main, Np_create_date) VALUES ('$img', '$img_thmb' ,'$news_id', '$main', NOW())";
        $query = mysql_query($text);
        if($query){
            $result = 1;
        }
        return $result;
    }

    public function insert_sub_data($img, $img_thmb, $news_id){
        $result = 0;

        $text = "INSERT INTO $this->table (Np_img, Np_img_thmb, Np_ref_newsID, Np_create_date) VALUES ('$img', '$img_thmb' ,'$news_id', NOW())";
        $query = mysql_query($text);
        if($query){
            $result = 1;
        }
        return $result;
    }

    public function delete_data($news_id, $id){
        $result = 0;

        $this->remove_photo($news_id, $id); //remove photo by news 

        $text = "DELETE FROM $this->table WHERE Np_ref_newsID = '$news_id' AND Np_ID = '$id'";
        $query = mysql_query($text);
        if(mysql_affected_rows() == 1){
            $result = 1;
        }
        //$result = $text;
        return $result;
    }

    public function remove_photo($news_id, $id){
        $result = 0;
        $flag_img = 0;
        $flag_img_thmb = 0;

        $text = "SELECT Np_img, Np_img_thmb FROM $this->table WHERE Np_ref_newsID = '$news_id' AND Np_ID = '$id'";
        $query = mysql_query($text);
        if(mysql_num_rows($query) == 1){
            $row = mysql_fetch_assoc($query);
            $deleteImg = $row['Np_img'];
            if (file_exists($deleteImg)) {
                unlink($deleteImg);
                $flag_img = 1;
            }
            $deleteImgThmb = $row['Np_img_thmb'];
            if (file_exists($deleteImgThmb)) {
                unlink($deleteImgThmb);
                $flag_img_thmb = 1;
            }

            if($flag_img == 1 && $flag_img_thmb ==1){
                $result = 1;
            }
        }
        return $result;
    }

    public function remove_photo_primary($news_id){
        $result = 0;
        $flag_img = 0;
        $flag_img_thmb = 0;

        $text = "SELECT Np_img, Np_img_thmb FROM $this->table WHERE Np_ref_newsID = '$news_id' AND Np_main = '1'";
        $query = mysql_query($text);
        if(mysql_num_rows($query) == 1){
            $row = mysql_fetch_assoc($query);
            $deleteImg = $row['Np_img'];
            if (file_exists($deleteImg)) {
                unlink($deleteImg);
                $flag_img = 1;
            }
            $deleteImgThmb = $row['Np_img_thmb'];
            if (file_exists($deleteImgThmb)) {
                unlink($deleteImgThmb);
                $flag_img_thmb = 1;
            }

            if($flag_img == 1 && $flag_img_thmb ==1){
                $result = 1;
            }
        }
        return $result;
    }

    public function update_data_primary($news_id, $img, $img_thmb){
        $result = 0;

        $text = "UPDATE $this->table SET Np_img = '$img', Np_img_thmb = '$img_thmb' WHERE Np_ref_newsID = '$news_id' AND Np_main = '1'";
        $query = mysql_query($text);
        if(mysql_affected_rows() == 1){
            $result = 1;
        }
        return $result;
    }

    public function set_title($id, $title){
        $result = 0;

        $text = "UPDATE $this->table SET Np_title = '$title' WHERE Np_ID = '$id'";
        $query = mysql_query($text);
        if(mysql_affected_rows() == 1){
            $result = 1;
        }
        return $result;
    }

    public function set_main($news_id, $main){
        $result = 0;

        $text = "UPDATE $this->table SET Np_main = 1 WHERE Np_ref_newsID = '$news_id' AND Np_ID = '$main'";
        $query = mysql_query($text);
        if(mysql_affected_rows() == 1){
            $result = 1;
        }
        return $result;
    }

    public function unset_main($news_id){
        $result = 0;

        $text = "UPDATE $this->table SET Np_main = 0 WHERE Np_ref_newsID = '$news_id'";
        $query = mysql_query($text);
        if(mysql_affected_rows() == 1){
            $result = 1;
        }
        return $result;
    }
}
?>