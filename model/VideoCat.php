<?php
class VideoCat{

	private $table = "T_VideoCat";
	private $tableVideo = "T_Video";

// START FUNCTION FOR API
	//FUNCTION TO GET all video category by the user
    public function get_video_cat($user_id, $sort){
        $result = 0;

        if($sort == "name"){
            $sql = "ORDER BY VideoCat_name ASC";
        }else if($sort == "recent_add"){
            $sql = "ORDER BY VideoCat_create_date DESC";
        }else if($sort == "recent_modify"){
            $sql = "ORDER BY VideoCat_modify_date DESC";
        }else{
            $sql = "";
        }
        
        $text = "SELECT User_ID, VideoCat_ID, VideoCat_name, VideoCat_desc, VideoCat_create_date,
            VideoCat_modify_date FROM $this->table WHERE User_ID = '$user_id' $sql";
        $query = mysql_query($text);
        if(mysql_num_rows($query) >= 1){
            $result = array();
            while($row = mysql_fetch_array($query,MYSQL_ASSOC)){
                $result[]=$row;
            }
        }
        //$result = $text;
        return $result;
    }

    public function get_data_detail($id){
        $result = 0;
        
        $text = "SELECT VideoCat_ID, VideoCat_name, VideoCat_desc FROM $this->table WHERE VideoCat_ID = '$id'";  
        $query = mysql_query($text);
        if(mysql_num_rows($query) >= 1){
            $result = array();
            while($row = mysql_fetch_array($query,MYSQL_ASSOC)){
                $result[]=$row;
            }
        }
        //$result = $text;
        return $result;
    }

	/* Start insert function */
	public function insert_data($user_id, $name, $desc, $publish){
		$result = 0;
		$text = "INSERT INTO $this->table (User_ID, VideoCat_name, VideoCat_desc, VideoCat_publish, VideoCat_create_date)VALUES('$user_id', '$name', '$desc', '$publish', NOW())";
		$query = mysql_query($text);
		if($query){
			$result = mysql_insert_id();
		}
		return $result;
	}
	/* End function insert */

	/* Fungsi Update */
	public function update_data($id, $user_id, $name, $desc, $publish){
		$result = 0;

		$text = "UPDATE $this->table SET User_ID = '$user_id', VideoCat_name = '$name', VideoCat_desc = '$desc', VideoCat_publish = '$publish' WHERE VideoCat_ID = '$id' ";
		$query = mysql_query($text);
		if(mysql_affected_rows() == 1){
			$result = 1;
		}
		return $result;
	}
	/* End Fungsi Update */

	/* 	Fungsi Delete */
	public function delete_data($id){
		$result = 0;

		$text = "DELETE FROM $this->table WHERE VideoCat_ID = '$id' ";
		$query = mysql_query($text);
		if(mysql_affected_rows() == 1){
			$result = 1;
		}

		$this->delete_data_related($id); //return delete data related

		return $result;
	}
	/* 	End Delete */

	/*  Fungsi Delete */
    public function delete_data_related($id){
        $result = 0;

        $text = "DELETE FROM $this->tableVideo WHERE VideoCat_ID = '$id' ";
        $query = mysql_query($text);
        if(mysql_affected_rows() == 1){
            $result = 1;
        }
        return $result;
    }
    /*  End Delete */
// END FUNCTION API
}
?>