<?php 
class Skill{

	private $table = "T_Skill";
	private $publish = "Skill_publish IN('Publish') "; 
	private $order = "ORDER BY Skill_name ASC";
	private $itemPerPage = 20;

	//FUNCTION TO GET AMOUNT OF ALL PUBLISHED Skill IN DATABASE
	public function get_total($letter = null, $publish = 1){
		$result = 0;
		$cond = '';
		if($letter != null){
			$cond .="AND Skill_name LIKE '$letter%' ";
		}
		if(isset($publish)){
			if($publish != 1){$this->publish = "Skill_publish IN('Publish', 'Not Publish') ";}
		}
		$text = "SELECT count(Skill_ID) AS count FROM $this->table WHERE $this->publish $cond ";
		$query = mysql_query($text);
		if(mysql_num_rows($query) >= 1){
			$row = mysql_fetch_array($query,MYSQL_ASSOC);
			$result =  $row['count'];
		}
		return $result;
	}

	public function get_total_page($total_data){
		$num_page = ceil($total_data / $this->itemPerPage);
		return $num_page;
	}	

	//FUNCTION TO GET data OF ALL PUBLISHED Skill IN DATABASE
	//1 to set publish, 0 to get publish and not published data
	public function get_data_by_page($page=0, $letter=null, $publish=1){
		$data = "There is no result.";
		if($page <= 1 || $page == null){
			$limitBefore = 0;
		}else{
			$limitBefore = ($page-1) * $this->itemPerPage;
		}

		$cond = '';
		if($letter != null){
			$cond .="AND t.Skill_name LIKE '$letter%' ";
		}
		
		if($publish != 1){
			$this->publish = "t.Skill_publish IN('Publish', 'Not Publish') ";
		}
		/*$condType = "";
		if($type == 'publish'){
			$condType .= "Cert_publish = 'Publish' ";
		}else if($type == 'notpublish'){
			$condType .= "Cert_publish = 'Not Publish' ";
		}*/
 		
 		$text = "SELECT COUNT(at.Skill_ID) AS skill_counter, t.Skill_ID AS Skill_ID, t.Skill_name AS Skill_name, 
 			t.Skill_img AS Skill_img, t.Skill_img_thmb AS Skill_img_thmb, t.Skill_publish AS Skill_publish 
 			FROM $this->table t LEFT JOIN AT_Skill at ON at.Skill_ID = t.Skill_ID WHERE $this->publish 
 			$cond GROUP BY t.Skill_ID ORDER BY t.Skill_name ASC LIMIT $limitBefore, $this->itemPerPage";
 		$query = mysql_query($text);
		if(mysql_num_rows($query) >= 1){
			$data = array();
			while($row = mysql_fetch_array($query,MYSQL_ASSOC)){
				$data[]=$row;
			}
		}
		//$data = $text;
		return $data;
	}

	public function get_index(){
		$result = 0;

		$text = "SELECT Skill_ID, Skill_name FROM $this->table WHERE $this->publish $this->order";
		$query = mysql_query($text);
		if(mysql_num_rows($query) >= 1){
			$result = array();
			while($row = mysql_fetch_assoc($query)){
				$result[] = $row;
			}
		}
		return $result;
	}

	public function get_data_edit($id){
		$result = 0;

		$text = "SELECT * FROM $this->table WHERE Skill_ID = '$id'";
		$query = mysql_query($text);
		if(mysql_num_rows($query) >= 1){
			$result = array();
			while($row = mysql_fetch_assoc($query)){
				$result[] = $row;
			}
		}
		return $result;
	}

	public function insert_data($name, $publish, $admin_id){
		$result = 0;

		$text = "INSERT INTO $this->table (Skill_name, Skill_publish, Skill_create_adminID, Skill_create_date) VALUES ('$name', '$publish', '$admin_id', NOW())";
		$query = mysql_query($text);
		if($query){
			$result = mysql_insert_id(); 
		}
		//$result = $text;
		return $result;
	}

	public function insert_data_image($name, $img, $img_thmb, $publish, $admin_id){
		$result = 0;

		$text = "INSERT INTO $this->table (Skill_name, Skill_img, Skill_img_thmb, Skill_publish, Skill_create_adminID, Skill_create_date) VALUES ('$name', '$img', '$img_thmb', '$publish', '$admin_id', NOW())";
		$query = mysql_query($text);
		if($query){
			$result = mysql_insert_id(); 
		}
		//$result = $text;
		return $result;
	}

	public function update_data($id, $name, $publish, $admin_id){
		$result = 0;

		$text = "UPDATE $this->table SET Skill_name = '$name', Skill_publish = '$publish', Skill_modify_adminID = '$admin_id' WHERE Skill_ID = '$id'";
		$query = mysql_query($text);
		if(mysql_affected_rows() == 1){
			$result = 1;
		}
		return $result;
	}

	public function update_data_image($id, $name, $img, $img_thmb, $publish, $admin_id){
		$result = 0;

		$this->remove_image($id); //remove image before

		$text = "UPDATE $this->table SET Skill_name = '$name', Skill_img = '$img', Skill_img_thmb = '$img_thmb', Skill_publish = '$publish', Skill_modify_adminID = '$admin_id' WHERE Skill_ID = '$id'";
		$query = mysql_query($text);
		if(mysql_affected_rows() == 1){
			$result = 1;
		}
		return $result;
	}

	public function remove_image($id){
		$result = 0;
		$flag_img = 0;
		$flag_img_thmb = 0;
		$text = "SELECT Skill_ID, Skill_img, Skill_img_thmb FROM $this->table WHERE Skill_ID = '$id' ";
		$query = mysql_query($text);
		if(mysql_num_rows($query) == 1){
			$row = mysql_fetch_assoc($query);
			$deleteImg = $_SERVER['DOCUMENT_ROOT']."/".$row['Skill_img'];
            if (file_exists($deleteImg)) {
                unlink($deleteImg);
                $flag_img = 1;
            }
            $deleteImgThmb = $_SERVER['DOCUMENT_ROOT']."/".$row['Skill_img_thmb'];
            if (file_exists($deleteImgThmb)) {
                unlink($deleteImgThmb);
                $flag_img_thmb = 1;
            }

            if($flag_img == 1 && $flag_img_thmb ==1){
            	$result = 1;
            }
		}
		return $result;
	}

	public function delete_data($id){
		$result = 0;

		$this->remove_image($id); //remove image before

		$text = "DELETE FROM $this->table WHERE Skill_ID = '$id'";
		$query = mysql_query($text);
		if(mysql_affected_rows() == 1){
			$result = 1;
		}
		return $result;
	}

	public function publish_data($id){
		$result = 0;
		$publish = "Publish";

		$text = "UPDATE $this->table SET Skill_publish = '$publish' WHERE Skill_ID = '$id'";
		$query = mysql_query($text);
		if(mysql_affected_rows() == 1){
			$result = 1;
		}
		return $result;
	}

	public function unpublish_data($id){
		$result = 0;
		$not_publish = "Not Publish";

		$text = "UPDATE $this->table SET Skill_publish = '$not_publish'  WHERE Skill_ID = '$id'";
		$query = mysql_query($text);
		if(mysql_affected_rows() == 1){
			$result = 1;
		}
		return $result;
	}

	//for api get list
	public function get_list($name){
		$result = 0;

		$text = "SELECT Skill_ID, Skill_name, Skill_img, Skill_img_thmb,
			IF(Skill_name LIKE '$name%', 20, IF(Skill_name LIKE '%$name%', 15, 
				IF(Skill_name LIKE '%$name', 10, 0))) AS weight
			FROM $this->table WHERE Skill_publish = 'Publish' AND Skill_name LIKE '$name%' OR 
			Skill_name LIKE '%$name' OR Skill_name LIKE '%$name%' 
			ORDER BY weight DESC, Skill_name ASC";
		$query = mysql_query($text);
		if(mysql_num_rows($query) >= 1){
			$result = array();
			while($row = mysql_fetch_assoc($query)){
				$result[] = $row;
			}
		}
		//$result = $text;
		return $result;
	}

	//for api check skill
	public function check_skill($name){//return 0 if not exist
		$data = 0;

		$text = "SELECT Skill_ID FROM $this->table WHERE Skill_name = '$name' ORDER BY Skill_ID ASC LIMIT 0,1";
		$query = mysql_query($text);
		if(mysql_num_rows($query) == 1){
			$row = mysql_fetch_array($query, MYSQL_ASSOC);
			$data = $row['Skill_ID'];
		}
		return $data;
	}//END CHECK-EXIST

}

?>