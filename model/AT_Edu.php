<?php
class AT_Edu{

	private $table = "AT_Edu";
	private $itemPerPage = 20;

// START FUNCTION FOR ADMIN PAGE
	//FUNCTION TO GET AMOUNT OF ALL PUBLISHED Student/Alumni IN DATABASE
	public function get_total($edu_id){
		$result = 0;
		$cond = '';
		if($letter != null){
			$cond .="AND Edu_schoolName LIKE '$letter%' ";
		}
		if(isset($publish)){
			if($publish != 1){$this->publish = "Edu_publish IN('Publish', 'Not Publish') ";}
		}
		$text = "SELECT COUNT(atedu.ATedu_ID) AS count FROM $this->table atedu LEFT JOIN T_Edu edu 
 			ON edu.Edu_ID = atedu.Edu_ID LEFT JOIN T_User user ON user.User_ID = atedu.User_ID 
 			WHERE edu.Edu_ID = '$edu_id'";
		$query = mysql_query($text);
		if(mysql_num_rows($query) >= 1){
			$row = mysql_fetch_array($query,MYSQL_ASSOC);
			$result =  $row['count'];
		}
		return $result;
	}

	public function get_total_page($total_data){
		$num_page = ceil($total_data / $this->itemPerPage);
		return $num_page;
	}	

	//FUNCTION TO GET data OF ALL PUBLISHED Student/Alumni IN DATABASE
	//1 to set publish, 0 to get publish and not published data
	public function get_data_by_edu($page=0, $edu_id){
		$data = "There is no result.";
		if($page <= 1 || $page == null){
			$limitBefore = 0;
		}else{
			$limitBefore = ($page-1) * $this->itemPerPage;
		}
 		
 		$text = "SELECT edu.Edu_ID AS Edu_ID, edu.Edu_schoolName AS Edu_schoolName, 
 			user.User_username AS User_username, user.User_ID AS User_ID, user.User_email AS User_email, 
 			user.User_proPhoto AS User_pic, user.User_proPhotoThmb AS User_pic_thmb, user.User_fname AS User_fname, 
 			user.User_lname AS User_lname, atedu.ATedu_ID AS ATedu_ID, 
 			CASE WHEN atedu.ATedu_fromMonth = 1 THEN 'Jan'
 				WHEN atedu.ATedu_fromMonth = 2 THEN 'Feb'
 				WHEN atedu.ATedu_fromMonth = 3 THEN 'Mar'
 				WHEN atedu.ATedu_fromMonth = 4 THEN 'Apr'
 				WHEN atedu.ATedu_fromMonth = 5 THEN 'May'
 				WHEN atedu.ATedu_fromMonth = 6 THEN 'Jun'
 				WHEN atedu.ATedu_fromMonth = 7 THEN 'Jul'
 				WHEN atedu.ATedu_fromMonth = 8 THEN 'Aug'
 				WHEN atedu.ATedu_fromMonth = 9 THEN 'Sep'
 				WHEN atedu.ATedu_fromMonth = 10 THEN 'Oct'
 				WHEN atedu.ATedu_fromMonth = 11 THEN 'Nov'
 				WHEN atedu.ATedu_fromMonth = 12 THEN 'Dec'
 			END AS ATedu_fromMonth,
 			atedu.ATedu_fromYear AS ATedu_fromYear, 
 			CASE WHEN atedu.ATedu_toMonth = 1 THEN 'Jan'
 				WHEN atedu.ATedu_toMonth = 2 THEN 'Feb'
 				WHEN atedu.ATedu_toMonth = 3 THEN 'Mar'
 				WHEN atedu.ATedu_toMonth = 4 THEN 'Apr'
 				WHEN atedu.ATedu_toMonth = 5 THEN 'May'
 				WHEN atedu.ATedu_toMonth = 6 THEN 'Jun'
 				WHEN atedu.ATedu_toMonth = 7 THEN 'Jul'
 				WHEN atedu.ATedu_toMonth = 8 THEN 'Aug'
 				WHEN atedu.ATedu_toMonth = 9 THEN 'Sep'
 				WHEN atedu.ATedu_toMonth = 10 THEN 'Oct'
 				WHEN atedu.ATedu_toMonth = 11 THEN 'Nov'
 				WHEN atedu.ATedu_toMonth = 12 THEN 'Dec'
 			END AS ATedu_toMonth,
 			atedu.ATedu_toYear AS ATedu_toYear, 
 			atedu.ATedu_degree AS ATedu_degree, atedu.ATedu_major AS ATedu_major, 
 			atedu.ATedu_GPA AS ATedu_GPA FROM $this->table atedu LEFT JOIN T_Edu edu 
 			ON edu.Edu_ID = atedu.Edu_ID LEFT JOIN T_User user ON user.User_ID = atedu.User_ID 
 			WHERE edu.Edu_ID = '$edu_id' ORDER BY user.User_fname LIMIT $limitBefore, $this->itemPerPage";
 		$query = mysql_query($text);
		if(mysql_num_rows($query) >= 1){
			$data = array();
			while($row = mysql_fetch_array($query,MYSQL_ASSOC)){
				$data[]=$row;
			}
		}
		//$data = $text;
		return $data;
	}

	// function index
	public function get_index($edu_id){
		$result = 0;

		$text = "SELECT user.User_email, user.User_fname AS User_fname, user.User_lname AS User_lname, atedu.Edu_ID AS Edu_ID,
			atedu.ATedu_ID AS ATedu_ID, atedu.ATedu_fromMonth AS ATedu_fromMonth,atedu.ATedu_fromYear AS ATedu_fromYear,atedu.ATedu_toMonth AS ATedu_toMonth,atedu.ATedu_toYear AS ATedu_toYear, atedu.ATedu_major AS ATedu_major, atedu.ATedu_degree AS ATedu_degree, atedu.ATedu_GPA AS ATedu_GPA, atedu.ATedu_status AS ATedu_status, atedu.ATedu_desc AS ATedu_desc FROM $this->table atedu LEFT JOIN T_Edu edu 
 			ON edu.Edu_ID = atedu.Edu_ID LEFT JOIN T_User user ON user.User_ID = atedu.User_ID 
 			WHERE edu.Edu_ID = '$edu_id'";
		$query = mysql_query($text);
		if(mysql_num_rows($query) >= 1){
			$result = array();
			while($row = mysql_fetch_assoc($query)){
				$result[] = $row;
			}
		}
		return $result;
	}

	public function get_data_detail($id){
		$result = 0;
		
 		$text = "SELECT edu.Edu_ID AS Edu_ID, edu.Edu_schoolName AS Edu_schoolName, 
 			edu.Edu_schoolCountry AS Edu_schoolCountry, edu.Edu_schoolCity AS Edu_schoolCity,
 			user.User_ID AS User_ID, user.User_proPhoto AS User_pic, user.User_email,
 			user.User_proPhotoThmb AS User_pic_thmb, user.User_fname AS User_fname, 
 			user.User_lname AS User_lname, atedu.ATedu_ID AS ATedu_ID, atedu.ATedu_desc AS ATedu_desc, 
 			atedu.ATedu_fromMonth AS ATedu_fromMonth, atedu.ATedu_fromYear AS ATedu_fromYear, 
 			atedu.ATedu_toMonth AS ATedu_toMonth, atedu.ATedu_toYear AS ATedu_toYear, 
 			atedu.ATedu_degree AS ATedu_degree, atedu.ATedu_major AS ATedu_major, 
 			atedu.ATedu_GPA AS ATedu_GPA FROM $this->table atedu LEFT JOIN T_Edu edu 
 			ON edu.Edu_ID = atedu.Edu_ID LEFT JOIN T_User user ON user.User_ID = atedu.User_ID 
 			WHERE atedu.ATedu_ID = '$id'";
 		$query = mysql_query($text);
		if(mysql_num_rows($query) >= 1){
			$result = array();
			while($row = mysql_fetch_array($query,MYSQL_ASSOC)){
				$result[]=$row;
			}
		}
		//$result = $text;
		return $result;
	}
// END FUNCTION FOR ADMIN PAGE

// START FUNCTION FOR CLIENT PAGE
	/* Start insert function */
	public function insert_data($edu_id, $user_id, $degree, $major, $honors, $fromMonth, $fromYear, $toMonth, $toYear, $gpa, $desc, $progress){
		$result = 0;
		$text = "INSERT INTO $this->table (Edu_ID, User_ID, ATedu_degree, ATedu_major, ATedu_honors, ATedu_fromMonth, ATedu_fromYear, ATedu_toMonth, ATedu_toYear, ATedu_GPA, ATedu_desc, ATedu_create_date, ATedu_status)VALUES('$edu_id', '$user_id', '$degree', '$major', '$honors', '$fromMonth', '$fromYear', '$toMonth', '$toYear', '$gpa', '$desc', NOW(), '$progress')";
		$query = mysql_query($text);
		if($query){
			$result = mysql_insert_id();
		}
		return $result;
	}
	/* End function insert */

	/* Fungsi Update */
	public function update_data($id, $edu_id, $degree, $major, $honors, $fromMonth, $fromYear, $toMonth, $toYear, $gpa, $desc, $progress){
		$result = 0;

		$text = "UPDATE $this->table SET Edu_ID = '$edu_id', ATedu_degree = '$degree', ATedu_major = '$major', ATedu_honors = '$honors', ATedu_fromMonth = '$fromMonth', ATedu_fromYear = '$fromYear', ATedu_toMonth = '$toMonth', ATedu_toYear = '$toYear', ATedu_GPA = '$gpa', ATedu_desc = '$desc', ATedu_status = '$progress' WHERE ATedu_ID = '$id' ";
		$query = mysql_query($text);
		if(mysql_affected_rows() == 1){
			$result = 1;
		}
		//$result = $text;
		return $result;
	}
	/* End Fungsi Update */

	/* 	Fungsi Delete */
	public function delete_data($id){
		$result = 0;
		$text = "DELETE FROM $this->table WHERE ATedu_ID = '$id' ";
		$query = mysql_query($text);
		if(mysql_affected_rows() == 1){
			$result = 1;
		}
		return $result;
	}
	/* 	End Delete */

	//for api get data
	public function get_data($user_id){
		$result = 0;

		$text = "SELECT edu.Edu_ID AS Edu_ID, edu.Edu_schoolName AS Edu_schoolName, edu.Edu_schoolCity AS Edu_schoolCity, 
			edu.Edu_schoolCountry AS Edu_schoolCountry, edu.Edu_img AS Edu_img, edu.Edu_img_thmb AS Edu_img_thmb, 
			atedu.ATedu_ID AS ATedu_ID, atedu.User_ID AS User_ID, atedu.ATedu_degree AS ATedu_degree, atedu.ATedu_major AS ATedu_major, 
			atedu.ATedu_honors AS ATedu_honors, atedu.ATedu_fromMonth AS ATedu_fromMonth, atedu.ATedu_fromYear AS ATedu_fromYear, 
			atedu.ATedu_toMonth AS ATedu_toMonth, atedu.ATedu_toYear AS ATedu_toYear, atedu.ATedu_status AS ATedu_status,
			atedu.ATedu_GPA AS ATedu_GPA, atedu.ATedu_desc AS ATedu_desc FROM $this->table atedu LEFT JOIN T_Edu edu 
			ON edu.Edu_ID = atedu.Edu_ID WHERE atedu.User_ID = '$user_id' ORDER BY ATedu_status DESC, ATedu_fromYear DESC, 
			ATedu_fromMonth DESC, ATedu_toYear DESC, ATedu_toMonth DESC";
		$query = mysql_query($text);
		if(mysql_num_rows($query) >= 1){
			$result = array();
			while($row = mysql_fetch_assoc($query)){
				$result[] = $row;
			}
		}
		//$result = $text;
		return $result;
	}
// END FUNCTION FOR CLIENT PAGE
}
?>