<?php 
class AT_Admin_Setting{
	
	private $table = "AT_Admin_Setting";
	private $tableAdmin = "T_Admin";
	private $tableRole = "T_Admin_Role";
	private $joinAdmin = "LEFT JOIN T_Admin admin ON admin.id = setting.Admin_ID ";
	private $joinRole = "LEFT JOIN T_Admin_Role role ON role.Role_ID = setting.Role_ID ";

	public function get_data(){
		$result = 0;
 		
 		$text = "SELECT admin.id AS Admin_ID, admin.username AS Admin_username, 
 			role.Role_ID AS Role_ID, role.Role_name AS Admin_role,
 			setting.User_show AS User_show, setting.Admin_show AS Admin_show, 
 			setting.Certificate_show AS Certificate_show, setting.Skill_show AS Skill_show, 
 			setting.Country_show AS Country_show, setting.City_show AS City_show, 
 			setting.Tag_show AS Tag_show, setting.Setting_show AS Setting_show 
 			FROM $this->table setting $this->joinAdmin $this->joinRole 
 			ORDER BY admin.username ASC";
 		$query = mysql_query($text);
		if(mysql_num_rows($query) >= 1){
			$result = array();
			while($row = mysql_fetch_array($query,MYSQL_ASSOC)){
				$result[]=$row;
			}
		}
		//$result = $text;
		return $result;
	}

	public function get_data_edit($id){
		$result = 0;
		
 		$text = "SELECT admin.id AS Admin_ID, admin.username AS Admin_username, 
 			role.Role_ID AS Role_ID, role.Role_name AS Admin_role,
 			setting.Education_show AS Education_show, setting.Company_show AS Company_show, 
 			setting.Industry_show AS Industry_show, setting.Field_show AS Field_show,
 			setting.Jobs_show AS Jobs_show, setting.User_show AS User_show,
 			setting.Admin_show AS Admin_show, setting.Certificate_show AS Certificate_show,
 			setting.Skill_show AS Skill_show, setting.Country_show AS Country_show,
 			setting.City_show AS City_show, setting.Tag_show AS Tag_show,
 			setting.Level_show AS Level_show, setting.Setting_show AS Setting_show 
 			FROM $this->table setting $this->joinAdmin $this->joinRole WHERE admin.id = '$id'";
 		$query = mysql_query($text);
		if(mysql_num_rows($query) >= 1){
			$result = array();
			while($row = mysql_fetch_array($query,MYSQL_ASSOC)){
				$result[]=$row;
			}
		}
		//$result = $text;
		return $result;
	}

	// function index admin
	public function get_index_admin(){
		$result = 0;

		$text = "SELECT id, username FROM $this->tableAdmin ORDER BY username ASC";
		$query = mysql_query($text);
		if(mysql_num_rows($query) >= 1){
			$result = array();
			while($row = mysql_fetch_assoc($query)){
				$result[] = $row;
			}
		}
		return $result;
	}

	// function index role
	public function get_index_role(){
		$result = 0;

		$text = "SELECT Role_ID, Role_name FROM $this->tableRole ORDER BY Role_name ASC";
		$query = mysql_query($text);
		if(mysql_num_rows($query) >= 1){
			$result = array();
			while($row = mysql_fetch_assoc($query)){
				$result[] = $row;
			}
		}
		return $result;
	}

	// check admin
	public function check_admin($admin_id){//return 0 if not exist
		$data = 0;

		$text = "SELECT Admin_ID FROM $this->table WHERE Admin_ID = '$admin_id' ";
		$query = mysql_query($text);
		if(mysql_num_rows($query) == 1){
			$row = mysql_fetch_array($query, MYSQL_ASSOC);
			$data = $row['Admin_ID'];
		}
		return $data;
	}//END CHECK-EXIST

	/* Start process data function */
	public function process_data($admin_id, $role_id, $edu, $company, $industry, $field, $jobs, $user, $admin, $certificate, $skill, $country, $city, $level, $tag, $setting, $admin_login){
		$result = 0;

		$check_exist = $this->check_admin($admin_id);
		if($check_exist == 0){
			$text = "INSERT INTO $this->table (Admin_ID, Role_ID, Education_show, Company_show, Industry_show, Field_show, Jobs_show, User_show, Admin_show, Certificate_show, Skill_show, Country_show, City_show, Level_show, Tag_show, Setting_show, create_adminID, create_date) VALUES('$admin_id', '$role_id', '$edu', '$company', '$industry', '$field', '$jobs', '$user', '$admin', '$certificate', '$skill', '$country', '$city', '$level', '$tag', '$setting', '$admin_login', NOW())";
		}else{
			$text = "UPDATE $this->table SET Admin_ID = '$admin_id', Role_ID = '$role_id', Education_show = '$edu', Company_show = '$company', Industry_show = '$industry', Field_show = '$field', Jobs_show = '$jobs', User_show = '$user', Admin_show = '$admin', Certificate_show = '$certificate', Skill_show = '$skill', Country_show = '$country', City_show = '$city', Level_show = '$level', Tag_show = '$tag', Setting_show = '$setting', modify_adminID = '$admin_login' WHERE Admin_ID = '$admin_id'";
		}

		$query = mysql_query($text);
		if($query){
			$result = 1;
		}
		//$result = $text;
		return $result;
	}
	/* End function process data */


	/* 	Fungsi Delete */
	public function delete_data($admin_id){
		$result = 0;

		$text = "DELETE FROM $this->table WHERE Admin_ID = '$admin_id' ";
		$query = mysql_query($text);
		if(mysql_affected_rows() == 1){
			$result = 1;
		}
		return $result;
	}
	/* 	End Delete */

}
?>