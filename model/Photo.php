<?php
class Photo{

	private $table = "T_Photo";
	private $tableAlbum = "T_Album";
	private $publish = "Photo_publish IN('Publish') ";
	private $itemPerPage = 20;
	private $itemPerPageDiscover = 12; // for discover page

//START FUNCTION FOR ADMIN PAGE
	//FUNCTION TO GET AMOUNT OF ALL PUBLISHED image IN DATABASE
	public function get_total($publish = 1){
		$result = 0;
		
		if(isset($publish)){
			if($publish != 1){$this->publish = "Photo_publish IN('Publish', 'Not Publish') ";}
		}	
		$text = "SELECT count(Photo_ID) AS count FROM $this->table WHERE $this->publish ";
		$query = mysql_query($text);
		if(mysql_num_rows($query) >= 1){
			$row = mysql_fetch_array($query,MYSQL_ASSOC);
			$result =  $row['count'];
		}
		return $result;
	}

	public function get_total_page($total_data){
		$num_page = ceil($total_data / $this->itemPerPage);
		return $num_page;
	}	

	//function for get data in adminMgr-Image.php
	public function get_data_by_page($page=0, $sort="newest"){
		$data = "There is no result.";
		if($page <= 1 || $page == null){
			$limitBefore = 0;
		}else{
			$limitBefore = ($page-1) * $this->itemPerPage;
		}

		$cond = "";
		if($sort == "newest"){
			$cond .="ORDER BY photo.Photo_create_date DESC ";
		}else if($sort == "viewed"){
			$cond .="ORDER BY hits_image DESC ";
		}
 		
 		/*$text = "SELECT photo.Photo_ID, photo.Photo_title, photo.Photo_imgLink, photo.Photo_popular,
 			photo.Photo_ThmbImgLink, photo.Photo_publish, photo.Album_ID, photo.Photo_create_date, 
 			(SELECT album.User_ID FROM $this->tableAlbum album WHERE album.Album_ID = photo.Album_ID) AS id_user,
 			(SELECT user.User_proPhotoThmb FROM T_User user WHERE user.User_ID = id_user) AS thmb_user,
 			(SELECT user.User_username FROM T_User user WHERE user.User_ID = id_user) AS username_user,
 			(SELECT user.User_fname FROM T_User user WHERE user.User_ID = id_user) AS fname_user,
 			(SELECT user.User_lname FROM T_User user WHERE user.User_ID = id_user) AS lname_user,
 			(SELECT COUNT(hu_ID) FROM T_Hits_User WHERE hu_ref_typeID = photo.Photo_ID AND hu_type = 'image') AS hits_image,
 			(SELECT COUNT(Like_ID) FROM T_Like WHERE Like_ref_typeID = photo.Photo_ID AND Like_type = 'image') AS hits_like
 			FROM $this->table photo $cond LIMIT $limitBefore, $this->itemPerPage";*/
 		$text = "SELECT photo.Photo_ID AS Photo_ID, photo.Photo_title AS Photo_title, photo.Photo_imgLink AS Photo_imgLink, 
 			photo.Photo_popular AS Photo_popular, photo.Photo_ThmbImgLink AS Photo_ThmbImgLink, photo.Photo_publish AS Photo_publish, 
 			photo.Photo_create_date AS Photo_create_date, album.Album_ID AS Album_ID, album.User_ID AS User_ID, user.User_username AS User_username, 
 			user.User_proPhotoThmb AS User_proPhotoThmb, user.User_fname AS User_fname, user.User_lname AS User_lname,
 			(SELECT COUNT(hu_ID) FROM T_Hits_User WHERE hu_ref_typeID = photo.Photo_ID AND hu_type = 'image') AS hits_image,
 			(SELECT COUNT(Like_ID) FROM T_Like WHERE Like_ref_typeID = photo.Photo_ID AND Like_type = 'image') AS hits_like
 			FROM ($this->table photo LEFT JOIN $this->tableAlbum album ON photo.Album_ID = album.Album_ID) 
 			LEFT JOIN T_User user ON album.User_ID = user.User_ID $cond LIMIT $limitBefore, $this->itemPerPage";
 		$query = mysql_query($text);
		if(mysql_num_rows($query) >= 1){
			$data = array();
			while($row = mysql_fetch_array($query,MYSQL_ASSOC)){
				$data[]=$row;
			}
		}
		//$data = $text;
		return $data;
	}

	//function for get data in adminMgr-Image_detail.php
	public function get_data_edit($id){
        $result = 0;
        
        /*$text = "SELECT photo.Photo_ID, photo.Photo_title, photo.Photo_imgLink, 
 			photo.Photo_ThmbImgLink, photo.Photo_publish, photo.Album_ID, photo.Photo_create_date, 
 			(SELECT album.User_ID FROM T_Album album WHERE album.Album_ID = photo.Album_ID) AS id_user,
 			(SELECT user.User_email FROM T_User user WHERE user.User_ID = id_user) AS email_user,
 			(SELECT user.User_fname FROM T_User user WHERE user.User_ID = id_user) AS fname_user,
 			(SELECT user.User_lname FROM T_User user WHERE user.User_ID = id_user) AS lname_user
 			FROM $this->table photo WHERE photo.Photo_ID = '$id'";*/
 		$text = "SELECT photo.Photo_ID AS Photo_ID, photo.Photo_title AS Photo_title, photo.Photo_imgLink AS Photo_imgLink, 
 			photo.Photo_ThmbImgLink AS Photo_ThmbImgLink, photo.Photo_publish AS Photo_publish, photo.Photo_create_date AS Photo_create_date, 
 			album.Album_ID AS Album_ID, album.User_ID AS User_ID, user.User_email AS User_email, user.User_fname AS User_fname, 
 			user.User_lname AS User_lname FROM ($this->table photo LEFT JOIN $this->tableAlbum album ON photo.Album_ID = album.Album_ID) 
 			LEFT JOIN T_User user ON album.User_ID = user.User_ID WHERE photo.Photo_ID = '$id'";
        $query = mysql_query($text);
        if(mysql_num_rows($query) >= 1){
            $result = array();
            while($row = mysql_fetch_array($query,MYSQL_ASSOC)){
                $result[] = $row;
            }
        }
        //$result = $text;
        return $result;
    }

    //function get count tags in adminMgr-Image.php
    public function get_count_tags($tablename, $table_id){
		$result = 0;

		$text = "SELECT COUNT(Tagging_ID) AS counter FROM AT_Tagging WHERE Tagging_ref_table = '$tablename' AND Tagging_ref_tableID = '$table_id'";
		$query = mysql_query($text);
		if(mysql_num_rows($query) >= 1){
			while($row = mysql_fetch_assoc($query)){
				$result = $row['counter'];
			}
		}
		return $result;
	}

    /* 	Fungsi Publish */
	public function publish_data($id){
		$result = 0;

		$text = "UPDATE $this->table SET Photo_publish = 'Publish' WHERE Photo_ID = '$id' ";
		$query = mysql_query($text);
		if(mysql_affected_rows() == 1){
			$result = 1;
		}

		return $result;
	}
	/* 	End Publish */

	 /* Fungsi Not Publish */
	public function unpublish_data($id){
		$result = 0;

		$text = "UPDATE $this->table SET Photo_publish = 'Not Publish' WHERE Photo_ID = '$id' ";
		$query = mysql_query($text);
		if(mysql_affected_rows() == 1){
			$result = 1;
		}

		return $result;
	}
	/* 	End Not Publish */

	/* 	Fungsi Popular */
	public function popular_data($id){
		$result = 0;

		$text = "UPDATE $this->table SET Photo_popular = 'yes' WHERE Photo_ID = '$id' ";
		$query = mysql_query($text);
		if(mysql_affected_rows() == 1){
			$result = 1;
		}

		return $result;
	}
	/* 	End Popular */

	 /* Fungsi Not Unpopular */
	public function unpopular_data($id){
		$result = 0;

		$text = "UPDATE $this->table SET Photo_popular = 'no' WHERE Photo_ID = '$id' ";
		$query = mysql_query($text);
		if(mysql_affected_rows() == 1){
			$result = 1;
		}

		return $result;
	}
	/* 	End Not Unpopular */
//END FUNCTION FOR ADMIN PAGE

//START FUNCTION FOR CLIENT PAGE
	/* Start insert photo function */
	public function insert_data_photo($imglink, $thmb_imglink, $album_id, $publish){
		$result = 0;

		$text = "INSERT INTO $this->table (Photo_imgLink, Photo_ThmbImgLink, Album_ID, Photo_publish, Photo_create_date) VALUES('$imglink', '$thmb_imglink', '$album_id', '$publish', NOW())";
		$query = mysql_query($text);
		if($query){
			$result = mysql_insert_id();
		}
		return $result;
	}
	/* End function insert photo */

	/* Fungsi Update */
	public function update_data($id, $title, $publish){
		$result = 0;

		$text = "UPDATE $this->table SET Photo_title = '$title', Photo_publish = '$publish' WHERE Photo_ID = '$id' ";
		$query = mysql_query($text);
		if(mysql_affected_rows() == 1){
			$result = 1;
		}
		return $result;
	}
	/* End Fungsi Update */

	/* Fungsi Set Primary */
	public function set_primary($id, $album_id, $primary){
		$result = 0;
		$result1 = 0;

		$text1 = "UPDATE $this->table SET Photo_primary = 'no' WHERE Album_ID = '$album_id' ";
		$query1 = mysql_query($text1);
		if(mysql_affected_rows() >= 1){
			$result1 = 1;
		}

		$text = "UPDATE $this->table SET Photo_primary = '$primary' WHERE Photo_ID = '$id' ";
		$query = mysql_query($text);
		if(mysql_affected_rows() == 1){
			$result = 1;
		}
		return $result;
	}
	/* End Fungsi Set Primary */

	/* 	Fungsi GET DATA */
	public function get_data_by_album($album_id){
		$result = 0;

		$text = "SELECT * FROM $this->table WHERE Album_ID = '$album_id' AND Photo_publish = 'Publish' ORDER BY Photo_modify_date DESC";
		$query = mysql_query($text);
		if(mysql_num_rows($query) >= 1){
            $result = array();
            while($row = mysql_fetch_assoc($query)){
                $result[] = $row;
            }
        }
		// $result = $text;
		return $result;
	}

	//function for get data image detail in user_portfolio_image_detail.php
	public function get_data_detail($id){
        $result = 0;
        
        $text = "SELECT Photo_ID, Photo_imgLink, Photo_ThmbImgLink, Photo_title, Photo_primary, ph.Album_ID AS Album_ID, Album_name, Photo_create_date, Photo_modify_date FROM $this->table ph LEFT JOIN T_Album al ON ph.Album_ID = al.Album_ID WHERE ph.Photo_ID = '$id'";
		$query = mysql_query($text);
        if(mysql_num_rows($query) >= 1){
            $result = array();
            while($row = mysql_fetch_array($query,MYSQL_ASSOC)){
                $result[] = $row;
            }
        }
        //$result = $text;
        return $result;
    }

	public function get_data_recent($user_id){
		$result = 0;

		$text = "SELECT Photo_ID, Photo_imgLink, Photo_ThmbImgLink, Photo_title, Album_name, Photo_create_date, Photo_modify_date FROM $this->table ph LEFT JOIN T_Album al ON ph.Album_ID = al.Album_ID WHERE al.User_ID = '$user_id' AND Photo_publish = 'Publish' AND Album_publish = 'Publish' ORDER BY Photo_modify_date DESC LIMIT 0,12";
		$query = mysql_query($text);
		if(mysql_num_rows($query) >= 1){
			$result = array();
			while($row = mysql_fetch_assoc($query)){
				$result[] = $row;
			}
		}
		return $result;
	}
	/* 	End Fungsi GET DATA */

	/* 	Fungsi Delete */
	public function delete_data($id){
		$result = 0;

		$this->remove_image($id); //return remove image

		$text = "DELETE FROM $this->table WHERE Photo_ID = '$id' ";
		$query = mysql_query($text);
		if(mysql_affected_rows() == 1){
			$result = 1;
		}

		return $result;
	}
	/* 	End Delete */

	//start function REMOVE photo
    public function remove_image($id){
        $result = 0;
        $flag_img = 0;
        $flag_img_thmb = 0;
        $text = "SELECT Photo_imgLink, Photo_ThmbImgLink FROM $this->table WHERE Photo_ID = '$id' ";
        $query = mysql_query($text);
        if(mysql_num_rows($query) == 1){
            $row = mysql_fetch_assoc($query);
            $deleteImg = $_SERVER['DOCUMENT_ROOT']."/".$row['Photo_imgLink'];
            if (file_exists($deleteImg)) {
                unlink($deleteImg);
                $flag_img = 1;
            }
            $deleteImgThmb = $_SERVER['DOCUMENT_ROOT']."/".$row['Photo_ThmbImgLink'];
            if (file_exists($deleteImgThmb)) {
                unlink($deleteImgThmb);
                $flag_img_thmb = 1;
            }

            if($flag_img == 1 && $flag_img_thmb ==1){
                $result = 1;
            }
        }
        return $result;
    }//end function remove photo

    //function get data by user for resume
    public function get_data_resume($user_id){
        $result = 0;
        
        $text = "SELECT album.Album_ID AS Album_ID, album.Album_name AS Album_name, album.User_ID AS User_ID,
        	photo.Photo_ID AS Photo_ID, photo.Photo_title AS Photo_title, photo.Photo_imgLink AS Photo_imgLink, 
        	photo.Photo_ThmbImgLink AS Photo_ThmbImgLink FROM $this->table photo LEFT JOIN 
        	$this->tableAlbum album ON album.Album_ID = photo.Album_ID WHERE album.User_ID = '$user_id' 
        	AND photo.Photo_publish = 'Publish' ORDER BY photo.Photo_create_date DESC LIMIT 0,6";  
        $query = mysql_query($text);
        if(mysql_num_rows($query) >= 1){
            $result = array();
            while($row = mysql_fetch_array($query,MYSQL_ASSOC)){
                $result[]=$row;
            }
        }
        //$result = $text;
        return $result;
    }

    //function api for get data detail in profile
	public function api_get_data_detail($id){
        $result = 0;

        $text = "SELECT photo.Photo_ID, photo.Photo_title, photo.Photo_imgLink, photo.Photo_popular,
 			photo.Photo_ThmbImgLink, photo.Photo_publish, photo.Album_ID, photo.Photo_create_date, 
 			(SELECT album.User_ID FROM $this->tableAlbum album WHERE album.Album_ID = photo.Album_ID) AS id_user,
 			(SELECT user.User_proPhotoThmb FROM T_User user WHERE user.User_ID = id_user) AS User_proPhotoThmb,
 			(SELECT user.User_username FROM T_User user WHERE user.User_ID = id_user) AS User_username,
 			(SELECT user.User_fname FROM T_User user WHERE user.User_ID = id_user) AS User_fname,
 			(SELECT user.User_lname FROM T_User user WHERE user.User_ID = id_user) AS User_lname,
 			(SELECT user.User_bioDesc FROM T_User user WHERE user.User_ID = id_user) AS User_bioDesc,
 			(SELECT user.User_city FROM T_User user WHERE user.User_ID = id_user) AS User_city,
 			(SELECT user.User_country FROM T_User user WHERE user.User_ID = id_user) AS User_country,
 			(SELECT COUNT(hu_ID) FROM T_Hits_User WHERE hu_ref_typeID = photo.Photo_ID AND hu_type = 'image') AS hits_image,
 			(SELECT COUNT(Like_ID) FROM T_Like WHERE Like_ref_typeID = photo.Photo_ID AND Like_type = 'image') AS hits_like
 			FROM $this->table photo WHERE photo.Photo_ID = '$id' AND photo.Photo_publish = 'Publish' ";
 		/*$text = "SELECT photo.Photo_ID AS Photo_ID, photo.Photo_title AS Photo_title, photo.Photo_imgLink AS Photo_imgLink, 
 			photo.Photo_popular AS Photo_popular, photo.Photo_ThmbImgLink AS Photo_ThmbImgLink, photo.Photo_publish AS Photo_publish, 
 			photo.Photo_create_date AS Photo_create_date, album.Album_ID AS Album_ID, album.User_ID AS User_ID, user.User_username AS User_username, 
 			user.User_proPhotoThmb AS User_proPhotoThmb, user.User_fname AS User_fname, user.User_lname AS User_lname,
 			user.User_bioDesc AS User_bioDesc, user.User_city AS User_city, user.User_country AS User_country,
 			(SELECT COUNT(hu_ID) FROM T_Hits_User WHERE hu_ref_typeID = photo.Photo_ID AND hu_type = 'image') AS hits_image,
 			(SELECT COUNT(Like_ID) FROM T_Like WHERE Like_ref_typeID = photo.Photo_ID AND Like_type = 'image') AS hits_like
 			FROM ($this->table photo LEFT JOIN $this->tableAlbum album ON photo.Album_ID = album.Album_ID) 
 			LEFT JOIN T_User user ON album.User_ID = user.User_ID WHERE photo.Photo_ID = '$id' AND photo.Photo_publish = 'Publish' ";*/
        $query = mysql_query($text);
        if(mysql_num_rows($query) >= 1){
            $result = array();
            while ($row=mysql_fetch_assoc($query)) {
            	$result[] = $row;
            }
            // $loop = 0;
            // while($row = mysql_fetch_assoc($query)){
            //     $result[$loop] = $row;

            //     $text_tag = "SELECT Tagging_ref_tableID, Tagging_ref_table, Tag_ID, Tag_title FROM AT_Tagging LEFT JOIN 
            //     	T_Ref_Tag ON Tagging_ref_tagID = Tag_ID WHERE Tagging_ref_table = 'photo' AND Tagging_ref_tableID 
            //     	= '{$row['Photo_ID']}' ORDER BY Tag_title ASC";
            //     $query_tag = mysql_query($text_tag);
            //     if(mysql_num_rows($query_tag) >= 1){
            //         while($row_tag = mysql_fetch_array($query_tag,MYSQL_ASSOC)){
            //             $result[$loop]['Tag_title'] .= $row_tag['Tag_title'].", ";
            //         }
            //     }
            //     $loop++;
            // }
        }

        //$result = $text;
        return $result;
    }
    //function for get data in discover page
	public function get_data_discover($page=0){
		$data = "";

		//get total data
		$text_total = "SELECT photo.Photo_ID AS Photo_ID, photo.Photo_title AS Photo_title, photo.Photo_imgLink AS Photo_imgLink, 
 			photo.Photo_popular AS Photo_popular, photo.Photo_ThmbImgLink AS Photo_ThmbImgLink, photo.Photo_publish AS Photo_publish, 
 			photo.Photo_create_date AS Photo_create_date, album.Album_ID AS Album_ID, album.User_ID AS User_ID
 			FROM $this->table photo LEFT JOIN $this->tableAlbum album ON photo.Album_ID = album.Album_ID WHERE photo.Photo_publish = 'Publish'";
		$query_total = mysql_query($text_total);
		$total_data = mysql_num_rows($query_total);
		if($total_data < 1){$total_data = 0;}

		//get total page
		$total_page = ceil($total_data / $this->itemPerPageDiscover);

		if($page <= 1 || $page == null){
			$limitBefore = 0;
		}else{
			$limitBefore = ($page-1) * $this->itemPerPageDiscover;
		}

		
		$cond ="ORDER BY photo.Photo_create_date DESC ";
 		
 		$text = "SELECT photo.Photo_ID AS Photo_ID, photo.Photo_title AS Photo_title, photo.Photo_imgLink AS Photo_imgLink, 
 			photo.Photo_popular AS Photo_popular, photo.Photo_ThmbImgLink AS Photo_ThmbImgLink, photo.Photo_publish AS Photo_publish, 
 			photo.Photo_create_date AS Photo_create_date, album.Album_ID AS Album_ID, album.User_ID AS User_ID
 			FROM $this->table photo LEFT JOIN $this->tableAlbum album ON photo.Album_ID = album.Album_ID WHERE photo.Photo_publish = 'Publish' $cond LIMIT $limitBefore, $this->itemPerPageDiscover";
 		$query = mysql_query($text);
		if(mysql_num_rows($query) >= 1){
			$data = array();
			while($row = mysql_fetch_array($query,MYSQL_ASSOC)){
				$data[]=$row;
			}
		}
		if(is_array($data)){
			$data[0]['total_page'] = $total_page;
			$data[0]['total_data_all'] = $total_data;
			$data[0]['total_data'] = count($data);
		}
		//$data = $text;
		return $data;
	}
//END FUNCTION FOR CLIENT PAGE
}
?>