<?php
class Edu{

	private $table = "T_Edu";
	private $publish = "Edu_publish IN('Publish') "; 
	private $order = "ORDER BY Edu_schoolName ASC";
	private $itemPerPage = 20;

	// function get total search for admin page
	public function get_total_search($page=0, $letter=null, $type, $name=null, $city=null, $country=null, $select=null){
		$result = 0;
		
		$condLetter = '';
		if($letter != null){
			$condLetter .="AND Edu_schoolName LIKE '$letter%' ";
		}

		$condType = "";
		if($type == 'publish'){
			$condType = "Edu_publish = 'Publish' AND Edu_img != '' AND Edu_img_thmb != ''";
		}else if($type == 'notpublish'){
			$condType = "Edu_publish = 'Not Publish' ";
		}else if($type == 'new'){
			$condType = "Edu_img = '' AND Edu_img_thmb = '' ";
		}

		$condName = '';
		if($name != null){
			$condName .="AND Edu_schoolName LIKE '%$name%' ";
		}

		$condCity = '';
		if($city != null){
			$condCity .="AND Edu_schoolCity LIKE '%$city%' ";
		}

		$condCountry = '';
		if($country != null){
			$condCountry .="AND Edu_schoolCountry LIKE '%$country%' ";
		}

		$condSelect = '';
		if($select != null){
			if($select == "other"){
				$condSelect .="AND Edu_schoolCountry != 'Indonesia' ";	
			}else if($select == "indo"){
				$condSelect .="AND Edu_schoolCountry = 'Indonesia' ";	
			}
		}

 		$text = "SELECT COUNT(Edu_ID) AS count FROM $this->table WHERE $condType 
 			$condLetter $condName $condCity $condCountry $condSelect";
 		$query = mysql_query($text);
		if(mysql_num_rows($query) >= 1){
			$row = mysql_fetch_array($query,MYSQL_ASSOC);
			$result =  $row['count'];
		}
		//$result = $text;
		return $result;
	}

	// function get data search for admin page
	public function get_data_search($page=0, $letter=null, $type, $name=null, $city=null, $country=null, $select=null, $sort=null){
		$data = "There is no result.";
		if($page <= 1 || $page == null){
			$limitBefore = 0;
		}else{
			$limitBefore = ($page-1) * $this->itemPerPage;
		}

		$condType = "";
		$order = "";
		if($type == "publish"){
			$condType = "t.Edu_publish = 'Publish' AND Edu_img != '' AND Edu_img_thmb != '' ";
			if($sort != ""){
				if($sort == "asc"){
					$order = "ORDER BY edu_counter ASC, t.Edu_schoolName ASC";	
				}else if($sort == "desc"){
					$order = "ORDER BY edu_counter DESC, t.Edu_schoolName ASC";
				}
			}else{
				$order = "ORDER BY t.Edu_schoolName ASC";
			}
		}else if($type == "notpublish"){
			$condType = "t.Edu_publish = 'Not Publish' ";
			if($sort != ""){
				if($sort == "asc"){
					$order = "ORDER BY edu_counter ASC, t.Edu_schoolName ASC";	
				}else if($sort == "desc"){
					$order = "ORDER BY edu_counter DESC, t.Edu_schoolName ASC";
				}
			}else{
				$order = "ORDER BY t.Edu_schoolName ASC";
			}
		}else if($type == "new"){
			$condType = "t.Edu_img = '' AND t.Edu_img_thmb = '' ";
			if($sort != ""){
				if($sort == "asc"){
					$order = "ORDER BY edu_counter ASC, t.Edu_schoolName ASC";	
				}else if($sort == "desc"){
					$order = "ORDER BY edu_counter DESC, t.Edu_schoolName ASC";
				}
			}else{
				$order = "ORDER BY t.Edu_create_date DESC";
			}
		}

		$condLetter = '';
		if($letter != null){
			$condLetter .="AND t.Edu_schoolName LIKE '$letter%' ";
		}

		$condName = '';
		if($name != null){
			$condName .="AND t.Edu_schoolName LIKE '%$name%' ";
		}

		$condCity = '';
		if($city != null){
			$condCity .="AND t.Edu_schoolCity LIKE '%$city%' ";
		}

		$condCountry = '';
		if($country != null){
			$condCountry .="AND t.Edu_schoolCountry LIKE '%$country%' ";
		}

		$condSelect = '';
		if($select != null){
			if($select == "other"){
				$condSelect .= "AND t.Edu_schoolCountry != 'Indonesia' ";	
			}else if($select == "indo"){
				$condSelect .= "AND t.Edu_schoolCountry = 'Indonesia' ";	
			}else if($select == "all"){
				$condSelect .= "";	
			}
		}

 		$text = "SELECT COUNT(at.ATedu_ID) AS edu_counter, t.Edu_ID AS Edu_ID, t.Edu_schoolName AS Edu_schoolName, 
 			t.Edu_schoolCity AS Edu_schoolCity, t.Edu_schoolCountry Edu_schoolCountry, t.Edu_img AS Edu_img, 
 			t.Edu_img_thmb AS Edu_img_thmb, t.Edu_publish AS Edu_publish, t.Edu_create_date AS Edu_create_date 
 			FROM $this->table t LEFT JOIN AT_Edu at ON at.Edu_ID = t.Edu_ID WHERE $condType $condLetter $condName 
 			$condCity $condCountry $condSelect GROUP BY t.Edu_ID $order LIMIT $limitBefore, $this->itemPerPage";
 		$query = mysql_query($text);
		if(mysql_num_rows($query) >= 1){
			$data = array();
			while($row = mysql_fetch_array($query,MYSQL_ASSOC)){
				$data[]=$row;
			}
		}
		//$data = $text;
		return $data;
	}

	//FUNCTION TO GET AMOUNT OF ALL PUBLISHED Disease IN DATABASE
	public function get_total($letter = null, $type){
		$result = 0;

		$cond = '';
		if($letter != null){
			$cond .="AND Edu_schoolName LIKE '$letter%' ";
		}
		
		$condType = "";
		if($type == 'publish'){
			$condType = "Edu_publish = 'Publish' AND Edu_img != '' AND Edu_img_thmb != ''";
		}else if($type == 'notpublish'){
			$condType = "Edu_publish = 'Not Publish' ";
		}else if($type == 'new'){
			$condType = "Edu_img = '' AND Edu_img_thmb = '' ";
		}

		$text = "SELECT COUNT(Edu_ID) AS count FROM $this->table WHERE $condType $cond ";
		$query = mysql_query($text);
		if(mysql_num_rows($query) >= 1){
			$row = mysql_fetch_array($query,MYSQL_ASSOC);
			$result =  $row['count'];
		}
		return $result;
	}

	public function get_total_page($total_data){
		$num_page = ceil($total_data / $this->itemPerPage);
		return $num_page;
	}	

	//FUNCTION TO GET data OF ALL PUBLISHED Company IN DATABASE
	//1 to set publish, 0 to get publish and not published data
	public function get_data_by_page($page=0, $letter=null, $type, $sort=null){
		$data = "There is no result.";
		if($page <= 1 || $page == null){
			$limitBefore = 0;
		}else{
			$limitBefore = ($page-1) * $this->itemPerPage;
		}

		$cond = '';
		if($letter != null){
			$cond .="AND t.Edu_schoolName LIKE '$letter%' ";
		}

		$condType = "";
		$order = "";
		if($type == "publish"){
			$condType = "t.Edu_publish = 'Publish' AND Edu_img != '' AND Edu_img_thmb != '' ";
			if($sort != ""){
				if($sort == "asc"){
					$order = "ORDER BY edu_counter ASC, t.Edu_schoolName ASC";	
				}else if($sort == "desc"){
					$order = "ORDER BY edu_counter DESC, t.Edu_schoolName ASC";
				}
			}else{
				$order = "ORDER BY t.Edu_schoolName ASC";
			}
		}else if($type == "notpublish"){
			$condType = "t.Edu_publish = 'Not Publish' ";
			if($sort != ""){
				if($sort == "asc"){
					$order = "ORDER BY edu_counter ASC, t.Edu_schoolName ASC";	
				}else if($sort == "desc"){
					$order = "ORDER BY edu_counter DESC, t.Edu_schoolName ASC";
				}
			}else{
				$order = "ORDER BY t.Edu_schoolName ASC";
			}
		}else if($type == "new"){
			$condType = "t.Edu_img = '' AND t.Edu_img_thmb = '' ";
			if($sort != ""){
				if($sort == "asc"){
					$order = "ORDER BY edu_counter ASC, t.Edu_schoolName ASC";	
				}else if($sort == "desc"){
					$order = "ORDER BY edu_counter DESC, t.Edu_schoolName ASC";
				}
			}else{
				$order = "ORDER BY t.Edu_create_date DESC";
			}
		}
		
		/*$condType = "";
		$order = "";
		if($type == 'publish'){
			$condType = "t.Edu_publish = 'Publish' AND Edu_img != '' AND Edu_img_thmb != '' ";
			$order = "ORDER BY t.Edu_schoolName ASC";
		}else if($type == 'notpublish'){
			$condType = "t.Edu_publish = 'Not Publish' ";
			$order = "ORDER BY t.Edu_schoolName ASC";
		}else if($type == 'new'){
			$condType = "t.Edu_img = '' AND t.Edu_img_thmb = '' ";
			$order = "ORDER BY t.Edu_create_date DESC";
		}*/
 		
 		$text = "SELECT COUNT(at.ATedu_ID) AS edu_counter, t.Edu_ID AS Edu_ID, t.Edu_schoolName AS Edu_schoolName, 
 			t.Edu_schoolCity AS Edu_schoolCity, t.Edu_schoolCountry Edu_schoolCountry, t.Edu_img AS Edu_img, 
 			t.Edu_img_thmb AS Edu_img_thmb, t.Edu_publish AS Edu_publish, t.Edu_create_date AS Edu_create_date 
 			FROM $this->table t LEFT JOIN AT_Edu at ON at.Edu_ID = t.Edu_ID WHERE $condType $cond GROUP BY t.Edu_ID 
 			$order LIMIT $limitBefore, $this->itemPerPage";
 		$query = mysql_query($text);
		if(mysql_num_rows($query) >= 1){
			$data = array();
			while($row = mysql_fetch_array($query,MYSQL_ASSOC)){
				$data[]=$row;
			}
		}
		//$data = $text;
		return $data;
	}

	// function index
	public function get_index(){
		$result = 0;

		$text = "SELECT ed.Edu_ID, Edu_schoolName, Edu_schoolCity, Edu_schoolCountry, Edu_img, count(ATedu_ID) as num_student FROM T_Edu ed LEFT JOIN AT_Edu ate ON ate.Edu_ID = ed.Edu_ID GROUP BY ed.Edu_ID ORDER BY ed.Edu_schoolName ASC";
		$query = mysql_query($text);
		if(mysql_num_rows($query) >= 1){
			$result = array();
			while($row = mysql_fetch_assoc($query)){
				$result[] = $row;
			}
		}
		return $result;
	}

	public function remove_image($id){
		$result = 0;
		$flag_img = 0;
		$flag_img_thmb = 0;
		$text = "SELECT Edu_ID, Edu_img, Edu_img_thmb FROM $this->table WHERE Edu_ID = '$id' ";
		$query = mysql_query($text);
		if(mysql_num_rows($query) == 1){
			$row = mysql_fetch_assoc($query);
			$deleteImg = $_SERVER['DOCUMENT_ROOT']."/".$row['Edu_img'];
            if (file_exists($deleteImg)) {
                unlink($deleteImg);
                $flag_img = 1;
            }
            $deleteImgThmb = $_SERVER['DOCUMENT_ROOT']."/".$row['Edu_img_thmb'];
            if (file_exists($deleteImgThmb)) {
                unlink($deleteImgThmb);
                $flag_img_thmb = 1;
            }

            if($flag_img == 1 && $flag_img_thmb ==1){
            	$result = 1;
            }
		}
		return $result;
	}

	public function get_data_edit($id){
		$result = 0;

		$text = "SELECT * FROM $this->table WHERE Edu_ID = '$id'";
		$query = mysql_query($text);
		if(mysql_num_rows($query) >= 1){
			$result = array();
			while($row = mysql_fetch_assoc($query)){
				$result[] = $row;
			}
		}
		return $result;
	}

/* Start insert function */
public function insert_data($name, $city, $country, $descEN, $descIN, $img, $img_thmb, $admin_id, $publish){
	$result = 0;

	$text = "INSERT INTO $this->table (Edu_schoolName, Edu_schoolCity, Edu_schoolCountry, Edu_desc_EN, Edu_desc_IN, Edu_img, Edu_img_thmb, Edu_create_adminID, Edu_publish, Edu_create_date) VALUES('$name', '$city', '$country', '$descEN', '$descIN', '$img', '$img_thmb', '$admin_id', '$publish', NOW())";
	$query = mysql_query($text);
	if($query){
		$result = mysql_insert_id();
	}
	return $result;
}
/* End function insert */

/* Fungsi Update */
public function update_data($id, $name, $city, $country, $descEN, $descIN, $admin_id, $publish){
	$result = 0;

	$text = "UPDATE $this->table SET Edu_schoolName = '$name', Edu_schoolCity = '$city', Edu_schoolCountry = '$country', Edu_desc_EN = '$descEN', Edu_desc_IN = '$descIN', Edu_modify_adminID = '$admin_id', Edu_publish = '$publish' WHERE Edu_ID = '$id' ";
	$query = mysql_query($text);
	if(mysql_affected_rows() == 1){
		$result = 1;
	}
	return $result;
}
/* End Fungsi Update */

/* Fungsi Update Image */
public function update_data_image($id, $name, $city, $country, $descEN, $descIN, $img, $img_thmb, $admin_id, $publish){
	$result = 0;

	$this->remove_image($id); //remove image before

	$text = "UPDATE $this->table SET Edu_schoolName = '$name', Edu_schoolCity = '$city', Edu_schoolCountry = '$country', Edu_desc_EN = '$descEN', Edu_desc_IN = '$descIN', Edu_img = '$img', Edu_img_thmb = '$img_thmb', Edu_modify_adminID = '$admin_id', Edu_publish = '$publish' WHERE Edu_ID = '$id' ";
	$query = mysql_query($text);
	if(mysql_affected_rows() == 1){
		$result = 1;
	}
	return $result;
}
/* End Fungsi Update Image */

/* 	Fungsi Publish */
public function publish($id){
	$result = 0;

	$text = "UPDATE $this->table SET Edu_publish = 'Publish' WHERE Edu_ID = '$id' ";
	$query = mysql_query($text);
	if(mysql_affected_rows() == 1){
		$result = 1;
	}
	return $result;
}
/* 	End Publish */

/* 	Fungsi Unpublish */
public function unpublish($id){
	$result = 0;

	$text = "UPDATE $this->table SET Edu_publish = 'Not Publish' WHERE Edu_ID = '$id' ";
	$query = mysql_query($text);
	if(mysql_affected_rows() == 1){
		$result = 1;
	}
	return $result;
}
/* 	End Unpublish */

/* 	Fungsi Delete */
public function delete_data($id){
	$result = 0;

	$this->remove_image($id); //remove image before

	$text = "DELETE FROM $this->table WHERE Edu_ID = '$id' ";
	$query = mysql_query($text);
	if(mysql_affected_rows() == 1){
		$result = 1;
	}
	return $result;
}
/* 	End Delete */

	//for api get list
	public function get_list($title){
		$result = 0;

		$text = "SELECT Edu_id, Edu_schoolName, Edu_schoolCity, Edu_schoolCountry, Edu_img,
			IF(Edu_schoolName LIKE '$title%', 20, IF(Edu_schoolName LIKE '%$title%', 15, 
				IF(Edu_schoolName LIKE '%$title', 10, 0))) AS weight
			FROM $this->table WHERE Edu_publish = 'Publish' AND Edu_schoolName LIKE '$title%' OR 
			Edu_schoolName LIKE '%$title' OR Edu_schoolName LIKE '%$title%' 
			ORDER BY weight DESC, Edu_schoolName ASC, Edu_schoolCountry ASC, Edu_schoolCity ASC";
		$query = mysql_query($text);
		if(mysql_num_rows($query) >= 1){
			$result = array();
			while($row = mysql_fetch_assoc($query)){
				$result[] = $row;
			}
		}
		//$result = $text;
		return $result;
	}

	//for api check school
	public function check_school($name, $city, $country){//return 0 if not exist
		$data = 0;

		$text = "SELECT Edu_ID FROM $this->table WHERE Edu_schoolName = '$name' AND Edu_schoolCity = '$city' AND Edu_schoolCountry = '$country' ORDER BY Edu_ID ASC LIMIT 0,1";
		$query = mysql_query($text);
		if(mysql_num_rows($query) == 1){
			$row = mysql_fetch_array($query, MYSQL_ASSOC);
			$data = $row['Edu_ID'];
		}
		return $data;
	}//END CHECK-EXIST

	//function for get popular university
	public function get_popular_univ($country){
        $result = 0;

        $cond = "";
        if($country == "id"){
        	$cond = "WHERE edu.Edu_schoolCountry = 'Indonesia' ";
        }else{
        	$cond = "WHERE edu.Edu_schoolCountry != 'Indonesia' ";
        }

        $text = "SELECT COUNT(atedu.ATedu_ID) AS counter_edu, edu.Edu_ID AS Edu_ID, edu.Edu_schoolName AS Edu_schoolName, 
        	edu.Edu_schoolCountry AS Edu_schoolCountry, edu.Edu_schoolCity AS Edu_schoolCity, edu.Edu_img AS Edu_img, 
        	edu.Edu_img_thmb AS Edu_img_thmb FROM $this->table edu LEFT JOIN AT_Edu atedu ON edu.Edu_ID = atedu.Edu_ID 
        	$cond GROUP BY edu.Edu_ID ORDER BY counter_edu DESC LIMIT 0,8";
        $query = mysql_query($text);
        if(mysql_num_rows($query) >= 1){//HAS TO BE EXACT 1 RESULT
            $result = array();
            while($row = mysql_fetch_assoc($query)){
                $result[] = $row;
            }
        }
        //$result = $text;
        return $result;
    }
    //FOR ADMIN EDU USER
    public function get_data_all($title){
		$result = 0;

		$text = "SELECT Edu_id, Edu_schoolName, Edu_schoolCity, Edu_schoolCountry, Edu_img,
			IF(Edu_schoolName LIKE '$title%', 20, IF(Edu_schoolName LIKE '%$title%', 15, 
				IF(Edu_schoolName LIKE '%$title', 10, 0))) AS weight
			FROM $this->table WHERE Edu_publish = 'Publish' AND Edu_schoolName LIKE '$title%' OR 
			Edu_schoolName LIKE '%$title' OR Edu_schoolName LIKE '%$title%' 
			ORDER BY weight DESC, Edu_schoolName ASC, Edu_schoolCountry ASC, Edu_schoolCity ASC";
		$query = mysql_query($text);
		if(mysql_num_rows($query) >= 1){
			$result = array();
			while($row = mysql_fetch_assoc($query)){
				$result[] = $row;
			}
		}
		//$result = $text;
		return $result;
	}
}
?>