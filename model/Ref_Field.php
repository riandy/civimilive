<?php 
class Ref_Field{

	private $table = "T_Ref_Field";
	private $publish = "Field_publish IN('Publish') "; 
	private $order = "ORDER BY Field_title ASC";
	private $itemPerPage = 20;
	//private $joinIndustry = " LEFT JOIN T_Industry ON Field_ref_industryID = Industry_ID ";
	private $joinJob = " LEFT JOIN T_Jobs ON Jobs_ref_fieldID = A.Field_ID ";
	private $joinHits = " LEFT JOIN T_Hits ON Hits_ref_fieldID = A.Field_ID ";
	private $joinApply = " LEFT JOIN AT_Job_Application ON Atja_ref_JobsID = Jobs_ID AND Jobs_ref_fieldID = A.Field_ID ";
	
	//function in 6 POPULAR COMPANIES
	public function get_popular($page =1){
		$result = 0;
		$itemPerPage = 10;//SET HERE FOR POPULAR FIELD
		if($page <= 1 || $page == null){
			$limitBefore = 0;
		}else{
			$limitBefore = ($page-1) * $itemPerPage;
		}

		$text = "SELECT A.Field_ID AS Field_ID, A.Field_title AS Field_title, A.Field_img AS Field_img, 
			A.Field_publish AS Field_publish, DATE_FORMAT(A.Field_modify_date, '%e %b %Y, %h:%i %p') AS Field_modify_date, 
			DATE_FORMAT(A.Field_create_date, '%e %b %Y, %h:%i %p') AS Field_create_date, B.Field_title AS Field_parent,
			(SELECT COUNT(Atja_ID) FROM AT_Job_Application WHERE Atja_ref_jobsID = Jobs_ID AND Jobs_ref_fieldID = A.Field_ID) AS num_applicant,
			(SELECT COUNT(Hits_ID) FROM T_Hits WHERE Hits_ref_fieldID = A.Field_ID) AS view,
			count(Jobs_ID) AS no_jobs FROM $this->table A LEFT JOIN $this->table B ON B.Field_ID = A.Field_parentID
			$this->joinJob $this->joinHits $this->joinApply WHERE A.Field_parentID != 0 
			GROUP BY A.Field_ID ORDER BY num_applicant DESC, view DESC LIMIT $limitBefore, $itemPerPage";
		//ORDER BY no_jobs DESC, Field_modify_date DESC LIMIT $limitBefore, $itemPerPage
		//$text = "SELECT Field_ID, Field_title, Field_create_date, Field_img, Field_publish, DATE_FORMAT(Field_modify_date, '%e %b %Y, %h:%i %p') AS Field_modify_date, DATE_FORMAT(Field_create_date, '%e %b %Y, %h:%i %p') AS Field_create_date, count(Jobs_ID) AS no_jobs FROM $this->table LEFT JOIN T_JOBS ON Jobs_ref_fieldID = Field_ID $cond GROUP BY Field_ID ORDER BY no_jobs DESC, Field_modify_date DESC LIMIT 0,12";
		$query = mysql_query($text);
		if(mysql_num_rows($query) >= 1){
			$result = array();
			while($row = mysql_fetch_assoc($query)){
				$result[] = $row;
			}
		}
		//$result = $text;
		return $result;
	}
	
	//FUNCTION TO GET AMOUNT OF ALL PUBLISHED FIELD IN DATABASE
	public function get_total($letter = null, $publish = 1){
		$result = 0;
		$cond = '';
		if($letter != null){
			$cond .="AND Field_title LIKE '$letter%' ";
		}
		if(isset($publish)){
			if($publish != 1){$this->publish = "Field_publish IN('Publish', 'Not Publish') ";}
		}
		$text = "SELECT count(Field_ID) AS count FROM $this->table WHERE Field_parentID != 0 AND $this->publish $cond ";
		$query = mysql_query($text);
		if(mysql_num_rows($query) >= 1){
			$row = mysql_fetch_array($query,MYSQL_ASSOC);
			$result =  $row['count'];
		}
		return $result;
	}

	//for api field
	public function get_list($title){
		$result = 0;

		$text = "SELECT Field_title AS label, Field_title AS value FROM $this->table WHERE $this->publish AND Field_title LIKE '$title%' OR Field_title LIKE '%$title' OR Field_title LIKE '%$title%' $this->order";
		$query = mysql_query($text);
		if(mysql_num_rows($query) >= 1){
			$result = array();
			while($row = mysql_fetch_assoc($query)){
				$result[] = $row;
			}
		}
		return $result;
	}

	public function get_total_page($total_data){
		$num_page = ceil($total_data / $this->itemPerPage);
		return $num_page;
	}	

	//FUNCTION TO GET data OF ALL PUBLISHED Field IN DATABASE
	//1 to set publish, 0 to get publish and not published data
	public function get_data_by_page($page=0, $letter=null, $publish=1){
		$data = "There is no result.";
		if($page <= 1 || $page == null){
			$limitBefore = 0;
		}else{
			$limitBefore = ($page-1) * $this->itemPerPage;
		}
		$cond = '';
		if($letter != null){
			$cond .="AND A.Field_title LIKE '$letter%' ";
		}
		if($publish != 1){
			$this->publish = "A.Field_publish IN('Publish', 'Not Publish') ";
		}
 		/*$text = "SELECT Field_ID, Field_title, Field_create_date, Field_img_thmb, Field_publish, 
 			DATE_FORMAT(Field_modify_date, '%e %b %Y, %h:%i %p') AS Field_modify_date, 
 			DATE_FORMAT(Field_create_date, '%e %b %Y, %h:%i %p') AS Field_create_date, 
 			count(Jobs_ID) AS no_jobs FROM $this->table LEFT JOIN T_JOBS ON Jobs_ref_fieldID = Field_ID
 			WHERE Field_parentID != 0 $cond GROUP BY Field_ID ORDER BY no_jobs DESC, 
 			Field_modify_date DESC LIMIT $limitBefore, $this->itemPerPage";
 		*/
 		$text = "SELECT A.Field_ID AS Field_ID, A.Field_title AS Field_title, 
 			A.Field_img AS Field_img, A.Field_img_thmb AS Field_img_thmb, 
 			A.Field_publish AS Field_publish, B.Field_title AS Field_parent, 
 			count(Jobs_ID) AS no_jobs,
 			DATE_FORMAT(A.Field_modify_date, '%e %b %Y, %h:%i %p') AS Field_modify_date, 
 			DATE_FORMAT(A.Field_create_date, '%e %b %Y, %h:%i %p') AS Field_create_date 
 			FROM $this->table A LEFT JOIN T_Jobs ON Jobs_ref_fieldID = A.Field_ID 
 			LEFT JOIN $this->table B ON B.Field_ID = A.Field_parentID WHERE A.Field_parentID != 0 
 			$cond GROUP BY A.Field_ID ORDER BY no_jobs DESC, A.Field_modify_date DESC 
 			LIMIT $limitBefore, $this->itemPerPage";
 		$query = mysql_query($text);
		if(mysql_num_rows($query) >= 1){
			$data = array();
			while($row = mysql_fetch_array($query,MYSQL_ASSOC)){
				$data[]=$row;
			}
		}
		return $data;
	}

	// function for api and admin page
	public function get_index(){
		$result = 0;

		$text = "SELECT A.Field_ID AS Field_ID, A.Field_title AS Field_title,
			B.Field_title AS Field_parent FROM $this->table A LEFT JOIN $this->table B 
			ON B.Field_ID = A.Field_parentID WHERE A.Field_parentID != 0 ORDER BY B.Field_title ASC";
		$query = mysql_query($text);
		if(mysql_num_rows($query) >= 1){
			$result = array();
			while($row = mysql_fetch_assoc($query)){
				$result[] = $row;
			}
		}
		return $result;
	}

	public function get_parent(){
		$result = 0;

		$text = "SELECT * FROM $this->table WHERE Field_parentID = 0 ORDER BY Field_title ASC";
		$query = mysql_query($text);
		if(mysql_num_rows($query) >= 1){
			$result = array();
			while($row = mysql_fetch_assoc($query)){
				$result[] = $row;
			}
		}
		return $result;
	}

	public function get_data_edit($id){
		$result = 0;

		$text = "SELECT * FROM $this->table WHERE Field_ID = '$id'";
		$query = mysql_query($text);
		if(mysql_num_rows($query) >= 1){
			$result = array();
			while($row = mysql_fetch_assoc($query)){
				$result[] = $row;
			}
		}
		return $result;
	}

	// public function insert_data($industry_id, $title, $content, $img, $img_thmb, $publish){
	// 	$result = 0;

	// 	$text = "INSERT INTO $this->table (Field_ref_industryID, Field_title, Field_content, Field_img, Field_img_thmb, Field_publish, Field_create_date) VALUES ('$industry_id', '$title', '$content', '$img', '$img_thmb', '$publish', NOW())";
	// 	$query = mysql_query($text);
	// 	if($query){
	// 		$result = 1; 
	// 	}
	// 	return $result;
	// }

	public function insert_data($parent_id, $title, $content, $img, $img_thmb, $admin_id, $publish){
		$result = 0;

		$text = "INSERT INTO $this->table (Field_parentID, Field_title, Field_content, Field_img, Field_img_thmb, Field_create_adminID, Field_publish, Field_create_date) VALUES ('$parent_id', '$title', '$content', '$img', '$img_thmb', '$admin_id', '$publish', NOW())";
		$query = mysql_query($text);
		if($query){
			$result = 1; 
		}
		return $result;
	}

	// public function update_data($id, $industry_id, $title, $content, $publish){
	// 	$result = 0;

	// 	$text = "UPDATE $this->table SET Field_ref_industryID = '$industry_id', Field_title = '$title', Field_content = '$content', Field_publish = '$publish' WHERE Field_ID = '$id'";
	// 	$query = mysql_query($text);
	// 	if(mysql_affected_rows() == 1){
	// 		$result = 1;
	// 	}
	// 	return $result;
	// }
	public function update_data($id, $parent_id, $title, $content, $admin_id, $publish){
		$result = 0;

		$text = "UPDATE $this->table SET Field_parentID = '$parent_id', Field_title = '$title', Field_content = '$content', Field_modify_adminID = '$admin_id', Field_publish = '$publish' WHERE Field_ID = '$id'";
		$query = mysql_query($text);
		if(mysql_affected_rows() == 1){
			$result = 1;
		}
		return $result;
	}

	// public function update_data_image($id, $industry_id, $title, $content, $img, $img_thmb, $publish){
	// 	$result = 0;

	// 	$text = "UPDATE $this->table SET Field_ref_industryID = '$industry_id', Field_title = '$title', Field_content = '$content', Field_img = '$img', Field_img_thmb = '$img_thmb', Field_publish = '$publish' WHERE Field_ID = '$id'";
	// 	$query = mysql_query($text);
	// 	if(mysql_affected_rows() == 1){
	// 		$result = 1;
	// 	}
	// 	return $result;
	// }
	public function update_data_image($id, $parent_id, $title, $content, $img, $img_thmb, $admin_id, $publish){
		$result = 0;

		$this->remove_image($id); //remove image before

		$text = "UPDATE $this->table SET Field_parentID = '$parent_id', Field_title = '$title', Field_content = '$content', Field_img = '$img', Field_img_thmb = '$img_thmb', Field_modify_adminID = '$admin_id', Field_publish = '$publish' WHERE Field_ID = '$id'";
		$query = mysql_query($text);
		if(mysql_affected_rows() == 1){
			$result = 1;
		}
		return $result;
	}

	public function remove_image($id){
		$result = 0;
		$flag_img = 0;
		$flag_img_thmb = 0;
		$text = "SELECT Field_ID, Field_img, Field_img_thmb FROM $this->table WHERE Field_ID = '$id' ";
		$query = mysql_query($text);
		if(mysql_num_rows($query) == 1){
			$row = mysql_fetch_assoc($query);
			$deleteImg = $_SERVER['DOCUMENT_ROOT']."/".$row['Field_img'];
            if (file_exists($deleteImg)) {
                unlink($deleteImg);
                $flag_img = 1;
            }
            $deleteImgThmb = $_SERVER['DOCUMENT_ROOT']."/".$row['Field_img_thmb'];
            if (file_exists($deleteImgThmb)) {
                unlink($deleteImgThmb);
                $flag_img_thmb = 1;
            }

            if($flag_img == 1 && $flag_img_thmb ==1){
            	$result = 1;
            }
		}
		return $result;
	}

	public function delete_data($id){
		$result = 0;

		$this->remove_image($id); //remove image before

		$text = "DELETE FROM $this->table WHERE Field_ID = '$id'";
		$query = mysql_query($text);
		if(mysql_affected_rows() == 1){
			$result = 1;
		}
		return $result;
	}

	public function publish_data($id, $publish){
		$result = 0;

		$text = "UPDATE $this->table SET Field_publish = '$publish' WHERE Field_ID = '$id'";
		$query = mysql_query($text);
		if(mysql_affected_rows() == 1){
			$result = 1;
		}
		return $result;
	}

	public function unpublish_data($id){
		$result = 0;
		$not_publish = "Not Publish";

		$text = "UPDATE $this->table SET Field_publish = '$not_publish' WHERE Field_ID = '$id'";
		$query = mysql_query($text);
		if(mysql_affected_rows() == 1){
			$result = 1;
		}
		return $result;
	}

}

?>