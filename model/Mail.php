<?php
class Mail{
	
	private $table = "T_User";

	//Bagi user yang sudah LAMA tidak LOGIN
	public function get_user_inactive(){
		$result = 0;
		
 		$text = "SELECT COUNT(at.ATedu_ID) AS counter_edu, user.User_ID, user.User_fname, user.User_username, user.User_email, user.User_actStatus,
 			user.User_step_personal, user.User_step_industry, user.User_step_goal, user.User_lastLogin FROM $this->table user LEFT JOIN AT_Edu at ON user.User_ID = at.User_ID 
 			WHERE user.User_actStatus = '1' AND user.User_deactivate = '0' AND user.User_unsub = '0' AND user.User_step_personal = 'Yes' AND user.User_step_goal = 'Yes' AND 
 			user.User_step_industry = 'Yes' AND YEAR(user.User_lastLogin) != YEAR(NOW()) GROUP BY user.User_ID HAVING counter_edu != '0' ";
 		$query = mysql_query($text);
		if(mysql_num_rows($query) >= 1){
			$result = array();
			while($row = mysql_fetch_array($query,MYSQL_ASSOC)){
				$result[] = $row;
			}
		}
		//$result = $text;
		return $result;
	}

	//Bagi User yang BELUM AKTIVASI ACCOUNT
	public function get_user_yet_active(){
		$result = 0;
		
 		$text = "SELECT User_ID, User_fname, User_username, User_email, User_actStatus FROM $this->table WHERE User_actStatus = 0 AND User_deactivate = 0 AND User_unsub = 0";
 		$query = mysql_query($text);
		if(mysql_num_rows($query) >= 1){
			$result = array();
			while($row = mysql_fetch_array($query,MYSQL_ASSOC)){
				$result[] = $row;
			}
		}
		//$result = $text;
		return $result;
	}

	// Bagi user yang BELUM isi data personal, step form2 ada yang belum di isi
	public function get_user_yet_personal(){
		$result = 0;
		
 		$text = "SELECT User_ID, User_fname, User_username, User_email, User_actStatus, User_step_personal FROM $this->table 
 			WHERE User_actStatus = 1 AND User_deactivate = 0 AND User_unsub = 0 AND User_step_personal != '' AND User_step_personal = 'No' ";
 		$query = mysql_query($text);
		if(mysql_num_rows($query) >= 1){
			$result = array();
			while($row = mysql_fetch_array($query,MYSQL_ASSOC)){
				$result[] = $row;
			}
		}
		//$result = $text;
		return $result;
	}

	//Bagi user yang BELUM punya data pendidikan
	public function get_user_yet_edu(){
		$result = 0;
		
 		$text = "SELECT COUNT(at.ATedu_ID) AS counter_edu, user.User_ID, user.User_fname, user.User_username, user.User_email, user.User_actStatus,
 			user.User_step_personal, user.User_step_industry, user.User_step_goal FROM $this->table user LEFT JOIN AT_Edu at ON user.User_ID = at.User_ID 
 			WHERE user.User_actStatus = '1' AND user.User_deactivate = '0' AND user.User_unsub = '0' AND user.User_step_personal = 'Yes' AND user.User_step_goal = 'Yes' 
 			AND user.User_step_industry = 'Yes' GROUP BY user.User_ID HAVING counter_edu = '0' ";
 		$query = mysql_query($text);
		if(mysql_num_rows($query) >= 1){
			$result = array();
			while($row = mysql_fetch_array($query,MYSQL_ASSOC)){
				$result[] = $row;
			}
		}
		//$result = $text;
		return $result;
	}
}
?>