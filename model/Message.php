<?php 
class Message{ 

	private $table = "T_Message";
    private $tableUser = "T_User";
	private $itemPerPage = 20;

//START FUNCTION FOR FRONT PAGE
    //insert contact user to table
	public function insert_data($to_user, $from_user, $to_email, $from_email, $from_name, $purpose, $content, $publish){
		$result = 0;

		$text = "INSERT INTO $this->table (Message_to_userID, Message_from_userID, Message_to_email, Message_from_email, Message_from_name, Message_purpose, Message_content, Message_publish, Message_create_date) VALUES('$to_user', '$from_user', '$to_email', '$from_email', '$from_name', '$purpose', '$content', '$publish', NOW())";
		$query = mysql_query($text);
		if($query){
			$result = mysql_insert_id();
		}
		//$result = $text;
	    return $result;
	}

    //function for send email to support@civimi.com
    function send_support($id, $tujuan, $user, $from, $purpose, $name, $content){
        include("../../civimi/packages/front_config.php");
        //create session for login admin
        $_SESSION['admin_username'] = "administrator";
        $_SESSION['admin_email'] = "testing@civimi.com";
        $_SESSION['admin_id'] = 10;
        $_SESSION['admin_role'] = "Admin";
        $_SESSION['admin_role_id'] = 3;
        $_SESSION['education'] = "Yes";
        $_SESSION['company'] = "Yes";
        $_SESSION['industry'] = "Yes";
        $_SESSION['field'] = "Yes";
        $_SESSION['jobs'] = "Yes";
        $_SESSION['user'] = "Yes";
        $_SESSION['admin'] = "Yes";
        $_SESSION['certificate'] = "Yes";
        $_SESSION['skill'] = "Yes";
        $_SESSION['country'] = "Yes";
        $_SESSION['city'] = "Yes";
        $_SESSION['level'] = "Yes";
        $_SESSION['tag'] = "Yes";
        $_SESSION['setting'] = "Yes";
        
        //mail
        $to = "support@civimi.com";
        $subject = $purpose." message from ".$from." to ".$user;
        $content_message = str_replace('\r\n','<br/>', $content);

        $message = 
        "<!DOCTYPE html><html><head></head><body>
        <div id='all' style='width: 100%;max-width: 800px;margin: 0 auto;'>
        <div id='header' style='padding: 20px;border-bottom: solid 2px #B5B5B5;margin-bottom: 30px;'>
        <div>
        <img src='".$global['client-page']."img/logo/logo-desktop.png' alt='logo' style='width: 200px;'>
        </div>
        </div>
        <div id='main' style='font-size:16px;'>
        Dikirim dari halaman <a target='_blank' href='".$path['user-resume'].$user.".cvm'>".$path['user-resume'].$user.".cvm</a><br/><br/>
        To Name : ".$user."<br>
        To Email : ".$tujuan."<br><br>
        Sender : ".$from."<br>
        Sender Name : ".$name."<br><br>
        Purpose : ".$purpose."<br><br>
        Message : <br><br>".$content_message."<br><br>
        <div style='text-align:center;margin-top:30px;'>Please click the button below forward to recipient.</div>
        <div style='text-align:center;'>
        <a target='_blank' href='http://www.civimi.com/beta/admin/adminMgr-Message.php?action=send_recipient&page=1&msg_ID=".$id."&to_email=".$tujuan."&from_email=".$from."&username=".$user."&name=".$name."&purpose=".$purpose."&message=".urlencode($content)."' 
        type='button' style='padding: 12px 28px;display: inline-block;background-color: #8CCFEC;color: #fff;font-weight: bold;text-decoration: none;font-size: 20px;border-radius: 3px;border: solid 2px #ddd;margin: 50px 0;'>Forward to Recipient</a>
        </div>
        *This message is auto generated, do not reply to this message.
        </div>
        <div id='footer' style='background-color: #666;padding: 30px 0 20px 0;width: 100%;'>
        <div style='display: block;height: 50px;padding: 0 20px;position: relative;'>
        <div style='font-size: 28px;font-style: italic;color: #FFFFFF;font-weight: bold;display: block;width: 50%;float: left;'>'Reinvent Yourself'</div>
        <div style='display: block;float: left;width: 50%;position: relative;text-align: right;'>
        <a target='_blank' href='".$path['facebook']."' class='social-logo'><img src='".$global['facebook-footer']."' alt='facebook' style='width: 20px;margin-right: 10px;'></a>
        <a target='_blank' href='".$path['google']."' class='social-logo'><img src='".$global['google-footer']."' alt='google' style='width: 20px;margin-right:10px;'></a>
        <a target='_blank' href='".$path['twitter']."' class='social-logo'><img src='".$global['twitter-footer']."' alt='twitter' style='width: 20px;'></a>
        </div>
        </div>
        <div style='text-align:center;'>
        <img src='".$global['client-page']."img/logo/logo-desktop.png' alt='logo' style='width: 200px;'>
        </div>
        <div style='text-align:center;font-size: 13px;color: #FFFFFF;margin:20px 0 0 0;'>© 2015. Created by Passion in Jakarta</div>
        </div>
        </div>
        </body></html>";

        // Always set content-type when sending HTML email
        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

        // More headers
        $headers .= "From: donotreply@civimi.com" . "\r\n";
        $sent = mail($to, $subject, $message, $headers);
        return $sent;   
    }
//END FUNCTION FOR FRONT PAGE

//START FUNCTION FOR ADMIN PAGE
	//function for send forward
    function send_email($tujuan, $user, $from, $purpose, $name, $content){
        include("../civimi/packages/front_config.php");
        //mail
        $to = $tujuan; 
        $subject = $purpose;
        $content_message = str_replace('\r\n','<br/>', $content);

        $message = 
        "<!DOCTYPE html><html><head></head><body>
        <div id='all' style='width: 100%;max-width: 800px;margin: 0 auto;'>
        <div id='header' style='padding: 20px;border-bottom: solid 2px #B5B5B5;margin-bottom: 30px;'>
        <div>
        <img src='".$global['client-page']."img/logo/logo-desktop.png' alt='logo' style='width: 200px;'>
        </div>
        </div>
        <div id='main' style='font-size:16px;'>
        Dear ".$user.",<br/><br/>".$content_message."<br><br><br>
        Kind Regards,<br/><br/><br/>".$name."<br/><br/>
        </div>
        <div id='footer' style='background-color: #666;padding: 30px 0 20px 0;width: 100%;'>
        <div style='display: block;height: 50px;padding: 0 20px;position: relative;'>
        <div style='font-size: 28px;font-style: italic;color: #FFFFFF;font-weight: bold;display: block;width: 50%;float: left;'>'Reinvent Yourself'</div>
        <div style='display: block;float: left;width: 50%;position: relative;text-align: right;'>
        <a target='_blank' href='".$path['facebook']."' class='social-logo'><img src='".$global['facebook-footer']."' alt='facebook' style='width: 20px;margin-right: 10px;'></a>
        <a target='_blank' href='".$path['google']."' class='social-logo'><img src='".$global['google-footer']."' alt='google' style='width: 20px;margin-right:10px;'></a>
        <a target='_blank' href='".$path['twitter']."' class='social-logo'><img src='".$global['twitter-footer']."' alt='twitter' style='width: 20px;'></a>
        </div>
        </div>
        <div style='text-align:center;'>
        <img src='".$global['client-page']."img/logo/logo-desktop.png' alt='logo' style='width: 200px;'>
        </div>
        <div style='text-align:center;font-size: 13px;color: #FFFFFF;margin:20px 0 0 0;'>© 2015. Created by Passion in Jakarta</div>
        </div>
        </div>
        </body></html>";

        // Always set content-type when sending HTML email
        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

        // More headers
        $headers .= "From: ". $from . "\r\n";
        $sent = mail($to, $subject, $message, $headers);
        return $sent;   
    }

	public function get_total(){
		$result = 0;
		
		$text = "SELECT count(Message_ID) AS count FROM $this->table";
		$query = mysql_query($text);
		if(mysql_num_rows($query) >= 1){
			$row = mysql_fetch_array($query,MYSQL_ASSOC);
			$result =  $row['count'];
		}
		//$result = $text;
		return $result;
	}

	public function get_total_page($total_data){
		$num_page = ceil($total_data / $this->itemPerPage);
		return $num_page;
	}

	public function get_data_by_page($page=0){
		$data = "There is no result.";
		if($page <= 1 || $page == null){
			$limitBefore = 0;
		}else{
			$limitBefore = ($page-1) * $this->itemPerPage;
		}
		
 		$text = "SELECT Message_ID, Message_purpose, Message_content, Message_to_email, Message_from_name, 
            Message_from_email, Message_purpose, Message_forwarded, Message_create_date, 
 			(SELECT User_username FROM $this->tableUser WHERE User_ID = Message_to_userID) AS Message_to_user,
            (SELECT User_username FROM $this->tableUser WHERE User_ID = Message_from_userID) AS Message_from_user
            FROM $this->table $this->join ORDER BY Message_create_date DESC LIMIT $limitBefore, $this->itemPerPage";
 		$query = mysql_query($text);
		if(mysql_num_rows($query) >= 1){
			$data = array();
			while($row = mysql_fetch_array($query,MYSQL_ASSOC)){
				$data[]=$row;
			}
		}
		//$data = $text;
		return $data;
	}

	public function delete_data($id){
		$result = 0;

		$text = "DELETE FROM $this->table WHERE Message_ID = '$id'";
		$query = mysql_query($text);
		if(mysql_affected_rows() == 1){
			$result = 1;
		}
		return $result;
	}

	public function update_message($id, $content){
		$result = 0;

		$text = "UPDATE $this->table SET Message_content = '$content' WHERE Message_ID = '$id'";
		$query = mysql_query($text);
		if(mysql_affected_rows() == 1){
			$result = 1;
		}
		return $result;
	}

	public function update_forwarded($id){
		$result = 0;

		$text = "UPDATE $this->table SET Message_forwarded = 'yes' WHERE Message_ID = '$id'";
		$query = mysql_query($text);
		if(mysql_affected_rows() == 1){
			$result = 1;
		}
		return $result;
	}
//END FUNCTION FOR ADMIN PAGE
}
?>