<?php
class News_Category{

	private $table = "T_News_Category";
    private $join = "LEFT JOIN T_News ON Nc_ID = News_ref_ncID";

    public function get_index(){
        $result = 0;

        $text = "SELECT Nc_ID, Nc_title FROM $this->table WHERE Nc_publish = 'Publish' ORDER BY Nc_sort ASC";
        $query = mysql_query($text);
        if(mysql_num_rows($query) >= 1){
            $result = array();
            while($row = mysql_fetch_assoc($query)){
                $result[] = $row;
            }
        }
        return $result;
    }

	public function get_data(){
		$result = 0;

		$text = "SELECT COUNT(News_ID) AS counter_news, Nc_ID, Nc_title, Nc_publish FROM $this->table $this->join GROUP BY Nc_ID ORDER BY Nc_sort ASC";
    	$query = mysql_query($text);
    	if(mysql_num_rows($query) >= 1){
    		$result = array();
    		while($row = mysql_fetch_assoc($query)){
    			$result[] = $row;
    		}
    	}
		return $result;
	}

    public function get_data_edit($id){
        $result = 0;

        $text = "SELECT * FROM $this->table WHERE Nc_ID = '$id'";
        $query = mysql_query($text);
        if(mysql_num_rows($query) >= 1){
            $result = array();
            while($row = mysql_fetch_assoc($query)){
                $result[] = $row;
            }
        }
        return $result;
    }

    public function insert_data($title, $publish, $sort){
        $result = 0;

        $text = "INSERT INTO $this->table (Nc_title, Nc_publish, Nc_sort, Nc_create_date) VALUES ('$title', '$publish', '$sort', NOW())";
        $query = mysql_query($text);
        if($query){
            $result = 1;
        }
        return $result;
    }

    public function update_data($id, $title, $publish, $sort){
        $result = 0;

        $text = "UPDATE $this->table SET Nc_title = '$title', Nc_publish = '$publish', Nc_sort = '$sort' WHERE Nc_ID = '$id'";
        $query = mysql_query($text);
        if(mysql_affected_rows() == 1){
            $result = 1;
        }
        return $result;
    }

    public function delete_data($id){
        $result = 0;

        $text = "DELETE FROM $this->table WHERE Nc_ID = '$id'";
        $query = mysql_query($text);
        if(mysql_affected_rows() == 1){
            $result = 1;
        }
        return $result;
    }
    
}
?>