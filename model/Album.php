<?php 
class Album{ 
//start class

    private $table = "T_Album";
    private $tablePhoto = "T_Photo";
   
// START FUNCTION FOR API
    //FUNCTION TO GET all Album by the user
    public function get_album($user_id, $sort){
        $result = 0;

        if($sort == "name"){
            $sql = "ORDER BY album.Album_name ASC, photo.Photo_primary DESC";
        }else if($sort == "recent_add"){
            $sql = "ORDER BY album.Album_create_date DESC, AND photo.Photo_primary DESC";
        }else if($sort == "recent_modify"){
            $sql = "ORDER BY album.Album_modify_date DESC, AND photo.Photo_primary DESC";
        }else{
            $sql = "ORDER BY photo.Photo_primary DESC";
        }
        
        $text = "SELECT album.User_ID AS User_ID, album.Album_ID AS Album_ID, album.Album_name AS Album_name, 
            album.Album_desc AS Album_desc, album.Album_create_date AS Album_create_date, album.Album_modify_date AS Album_modify_date, 
            photo.Photo_ThmbImgLink, photo.Photo_imgLink, photo.Photo_primary FROM $this->table album LEFT JOIN $this->tablePhoto photo ON album.Album_ID = photo.Album_ID 
            AND Photo_primary = 'yes' WHERE User_ID = '$user_id' $sql";
        $query = mysql_query($text);
        if(mysql_num_rows($query) >= 1){
            $result = array();
            while($row = mysql_fetch_array($query,MYSQL_ASSOC)){
                $result[]=$row;
            }
        }
        //$result = $text;
        return $result;
    }

    public function get_data_detail($id){
        $result = 0;
        
        $text = "SELECT Album_ID, Album_name, Album_desc FROM $this->table WHERE Album_ID = '$id'";  
        $query = mysql_query($text);
        if(mysql_num_rows($query) >= 1){
            $result = array();
            while($row = mysql_fetch_array($query,MYSQL_ASSOC)){
                $result[]=$row;
            }
        }
        //$result = $text;
        return $result;
    }

    /* Start insert function */
    public function insert_data($user_id, $name, $desc, $publish){
        $result = 0;

        $text = "INSERT INTO $this->table (User_ID, Album_name, Album_desc, Album_publish, Album_create_date)VALUES('$user_id', '$name', '$desc', '$publish', NOW())";
        $query = mysql_query($text);
        if($query){
            $result = mysql_insert_id();
        }
        return $result;
    }
    /* End function insert */

    /* Fungsi Update */
    public function update_data($id, $name, $desc, $publish){
        $result = 0;

        $text = "UPDATE $this->table SET Album_name = '$name', Album_desc = '$desc', Album_publish = '$publish' WHERE Album_ID = '$id' ";
        $query = mysql_query($text);
        if(mysql_affected_rows() == 1){
            $result = 1;
        }
        return $result;
    }
    /* End Fungsi Update */

    /*  Fungsi Delete */
    public function delete_data($id){
        $result = 0;

        $this->remove_image_related($id); //remove image related
        $this->delete_data_related($id); //delete data related

        $text = "DELETE FROM $this->table WHERE Album_ID = '$id' ";
        $query = mysql_query($text);
        if(mysql_affected_rows() == 1){
            $result = 1;
        }
        
        return $result;
    }
    /*  End Delete */

    /*  Fungsi Delete */
    public function delete_data_related($id){
        $result = 0;

        $text = "DELETE FROM $this->tablePhoto WHERE Album_ID = '$id' ";
        $query = mysql_query($text);
        if(mysql_affected_rows() == 1){
            $result = 1;
        }
        return $result;
    }
    /*  End Delete */

    //start function REMOVE photo
    public function remove_image_related($id){
        $result = 0;
        $flag_img = 0;
        $flag_img_thmb = 0;
        $text = "SELECT Photo_imgLink, Photo_ThmbImgLink FROM $this->tablePhoto WHERE Album_ID = '$id' ";
        $query = mysql_query($text);
        while ($row = mysql_fetch_array($query, MYSQL_ASSOC)) 
        {
            $deleteImg = $_SERVER['DOCUMENT_ROOT']."/".$row['Photo_imgLink'];
            if (file_exists($deleteImg)) {
                unlink($deleteImg);
                $flag_img = 1;
            }
            $deleteImgThmb = $_SERVER['DOCUMENT_ROOT']."/".$row['Photo_ThmbImgLink'];
            if (file_exists($deleteImgThmb)) {
                unlink($deleteImgThmb);
                $flag_img_thmb = 1;
            }

            if($flag_img == 1 && $flag_img_thmb == 1){
                $result = 1;
            }
        }
        return $result;
    }//end function remove photo
// END FUNCTION API

}//end class
?>