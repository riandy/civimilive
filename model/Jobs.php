<?php 
class Jobs{

	private $table = "T_Jobs";
	private $publish = "Jobs_publish IN('Publish') "; 
	private $order = "ORDER BY Jobs_create_date DESC, Jobs_modify_date DESC, Jobs_title ASC";
	private $itemPerPage = 20;
	// JOIN PART 1, FOR ALL
	private $joinCom = " LEFT JOIN T_Company ON Jobs_ref_companyID = Company_ID ";
	private $joinField = " LEFT JOIN T_Ref_Field ON Jobs_ref_fieldID = Field_ID ";
	private $joinLvl = " LEFT JOIN T_Ref_Level ON Jobs_ref_levelID = Level_ID ";
	private $joinCity = " LEFT JOIN T_Ref_City ON Jobs_ref_cityID = City_ID ";
	private $joinInd = " LEFT JOIN T_Industry ON Jobs_ref_industryID = Industry_ID ";
	private $joinHits = " LEFT JOIN T_Hits ON Hits_ref_jobsID = Jobs_ID ";
	private $joinApply = " LEFT JOIN AT_Job_Application ON Atja_ref_jobsID = Jobs_ID ";
	// JOIN PART 2, FOR GET RESULT (USE TABLE ALIAS)
	private $joinCom1 = " LEFT JOIN T_Company comp ON jobs.Jobs_ref_companyID = comp.Company_ID ";
	private $joinField1 = " LEFT JOIN T_Ref_Field field ON jobs.Jobs_ref_fieldID = field.Field_ID ";
	private $joinLvl1 = " LEFT JOIN T_Ref_Level lvl ON jobs.Jobs_ref_levelID = lvl.Level_ID ";
	private $joinCity1 = " LEFT JOIN T_Ref_City city ON jobs.Jobs_ref_cityID = city.City_ID ";
	private $joinInd1 = " LEFT JOIN T_Industry ind ON jobs.Jobs_ref_industryID = ind.Industry_ID ";
	private $joinHits1 = " LEFT JOIN T_Hits hits ON hits.Hits_ref_jobsID = jobs.Jobs_ID ";
	private $joinApply1 = " LEFT JOIN AT_Job_Application atja ON atja.Atja_ref_jobsID = jobs.Jobs_ID ";

/* START FUNCTION FOR ADMIN PAGE */
	// function get total search for admin page
	public function get_total_search($page=0, $letter=null, $publish=1, $field_id=0, $city_id=0, $level_id=0, $intern=0, $fulltime=0, $parttime=0, $freelance=0, $keyword=null){
		$result = 0;
		
		$condLetter = '';
		if($letter != null){
			$condLetter .="AND Jobs_title LIKE '$letter%' ";
		}

		if($publish != 1){
			$this->publish = "Jobs_publish IN('Publish', 'Not Publish') ";
		}

		$condField = '';
		if($field_id != 0){
			$condField .="AND Jobs_ref_fieldID = '$field_id' ";
		}

		$condCity = '';
		if($city_id != 0){
			$condCity .=" AND Jobs_ref_cityID = '$city_id' ";
		}

		$condLevel = '';
		if($level_id != 0){
			$condLevel .="AND Jobs_ref_levelID = '$level_id' ";
		}

		$condIntern = '';
		if($intern != 0){
			$condIntern .="AND Jobs_intern = '$intern' ";
		}

		$condFulltime = '';
		if($fulltime != 0){
			$condFulltime .="AND Jobs_fulltime = '$fulltime' ";
		}

		$condParttime = '';
		if($parttime != 0){
			$condParttime .="AND Jobs_parttime = '$parttime' ";
		}

		$condFreelance = '';
		if($freelance != 0){
			$condFreelance .="AND Jobs_freelance = '$freelance' ";
		}

		$condKeyword = '';
		if($keyword != null){
			$condKeyword .="AND Jobs_title LIKE '%$keyword%' OR Jobs_content LIKE '%$keyword%' OR Jobs_qualification_info LIKE '%$keyword%' OR Jobs_apply_info LIKE '%$keyword%' ";
		}

 		$text = "SELECT COUNT(Jobs_ID) AS count FROM $this->table $this->joinCom $this->joinField $this->joinLvl $this->joinCity WHERE $this->publish $condLetter $condField $condCity $condLevel $condIntern $condFulltime $condParttime $condFreelance $condKeyword $this->order";
 		$query = mysql_query($text);
		if(mysql_num_rows($query) >= 1){
			$row = mysql_fetch_array($query,MYSQL_ASSOC);
			$result =  $row['count'];
		}
		//$result = $text;
		return $result;
	}

	// function get data search for admin page
	public function get_data_search($page=0, $letter=null, $publish=1, $field_id=0, $city_id=0, $level_id=0, $intern=0, $fulltime=0, $parttime=0, $freelance=0, $keyword=null){
		$data = "There is no result.";
		if($page <= 1 || $page == null){
			$limitBefore = 0;
		}else{
			$limitBefore = ($page-1) * $this->itemPerPage;
		}

		if($publish != 1){
			$this->publish = "Jobs_publish IN('Publish', 'Not Publish') ";
		}

		$condLetter = '';
		if($letter != null){$condLetter .="AND Jobs_title LIKE '$letter%' ";}

		$condField = '';
		if($field_id != 0){$condField .="AND Jobs_ref_fieldID = '$field_id' ";}

		$condCity = '';
		if($city_id != 0){$condCity .=" AND Jobs_ref_cityID = '$city_id' ";}

		$condLevel = '';
		if($level_id != 0){$condLevel .="AND Jobs_ref_levelID = '$level_id' ";}

		$condIntern = '';
		if($intern != 0){$condIntern .="AND Jobs_intern = '$intern' ";}

		$condFulltime = '';
		if($fulltime != 0){$condFulltime .="AND Jobs_fulltime = '$fulltime' ";}

		$condParttime = '';
		if($parttime != 0){$condParttime .="AND Jobs_parttime = '$parttime' ";}

		$condFreelance = '';
		if($freelance != 0){$condFreelance .="AND Jobs_freelance = '$freelance' ";}

		$condKeyword = '';
		if($keyword != ''){$condKeyword .="AND Jobs_title LIKE '%$keyword%' OR Jobs_content LIKE '%$keyword%' OR Jobs_qualification_info LIKE '%$keyword%' OR Jobs_apply_info LIKE '%$keyword%' ";}

 		$text = "SELECT * FROM $this->table $this->joinCom $this->joinField $this->joinLvl $this->joinCity WHERE $this->publish $condLetter $condField $condCity $condLevel $condIntern $condFulltime $condParttime $condFreelance $condKeyword $this->order LIMIT $limitBefore, $this->itemPerPage";
 		$query = mysql_query($text);
		if(mysql_num_rows($query) >= 1){
			$data = array();
			while($row = mysql_fetch_array($query,MYSQL_ASSOC)){
				$data[]=$row;
			}
		}
		//$data = $text;
		return $data;
	}

	//FUNCTION TO GET AMOUNT OF ALL PUBLISHED ADMIN JOBS IN DATABASE
	public function get_total($letter = null, $publish = 1){
		$result = 0;
		$cond = '';
		if($letter != null){
			$cond .="AND Jobs_title LIKE '$letter%' ";
		}
		if(isset($publish)){
			if($publish != 1){$this->publish = "Jobs_publish IN('Publish', 'Not Publish') ";}
		}
		$text = "SELECT count(Jobs_ID) AS count FROM $this->table WHERE $this->publish $cond ";
		$query = mysql_query($text);
		if(mysql_num_rows($query) >= 1){
			$row = mysql_fetch_array($query,MYSQL_ASSOC);
			$result =  $row['count'];
		}
		return $result;
	}

	public function get_total_page($total_data){
		$num_page = ceil($total_data / $this->itemPerPage);
		return $num_page;
	}	

	//FUNCTION TO GET data OF ALL PUBLISHED ADMIN JOBS IN DATABASE
	//1 to set publish, 0 to get publish and not published data
	public function get_data_by_page($page=0, $letter=null, $publish=1){
		$data = "There is no result.";
		if($page <= 1 || $page == null){
			$limitBefore = 0;
		}else{
			$limitBefore = ($page-1) * $this->itemPerPage;
		}
		$cond = '';
		if($letter != null){
			$cond .="WHERE Jobs_title LIKE '$letter%' ";
		}
		if($publish != 1){
			$this->publish = "Jobs_publish IN('Publish', 'Not Publish') ";
		}
 		$text = "SELECT * FROM $this->table $this->joinCom $this->joinField $this->joinLvl $this->joinCity $cond $this->order LIMIT $limitBefore, $this->itemPerPage";
 		$query = mysql_query($text);
		if(mysql_num_rows($query) >= 1){
			$data = array();
			while($row = mysql_fetch_array($query,MYSQL_ASSOC)){
				$data[]=$row;
			}
		}
		return $data;
	}

	// function edit data jobs in admin page
	public function get_data_edit($id){
		$result = 0;

		$text = "SELECT * FROM $this->table WHERE Jobs_ID = '$id'";
		$query = mysql_query($text);
		if(mysql_num_rows($query) >= 1){
			$result = array();
			while($row = mysql_fetch_assoc($query)){
				$result[] = $row;
			}
		}
		return $result;
	}

	// function insert data jobs in admin page
	public function insert_data($title, $content, $company_id, $field_id, $level_id, $city_id, $exp, $start, $end, $qualification, $apply, $intern, $fulltime, $parttime, $freelance, $salary_type, $salary_period, $salary_min, $salary_max, $admin_id, $publish, $status){
		$result = 0;

		$text = "INSERT INTO $this->table (Jobs_title, Jobs_content, Jobs_ref_companyID, Jobs_ref_fieldID, Jobs_ref_levelID, Jobs_ref_cityID, 
			Jobs_exp_year, Jobs_post_start, Jobs_post_end, Jobs_qualification_info, Jobs_apply_info, Jobs_intern, Jobs_fulltime, Jobs_parttime, 
			Jobs_freelance, Jobs_salary_type, Jobs_salary_period, Jobs_salary_min, Jobs_salary_max, Jobs_create_adminID, Jobs_publish, Jobs_status, 
			Jobs_create_date) VALUES ('$title', '$content', '$company_id', '$field_id', '$level_id', '$city_id', '$exp', '$start', '$end', '$qualification', 
			'$apply', '$intern', '$fulltime', '$parttime', '$freelance', '$salary_type', '$salary_period', '$salary_min', '$salary_max', 
			'$admin_id' ,'$publish', '$status', NOW())";
		$query = mysql_query($text);
		if($query){
			$result = mysql_insert_id(); 
		}
		return $result;
	}

	// function update data jobs in admin page
	public function update_data($id, $title, $content, $company_id, $field_id, $level_id, $city_id, $exp, $start, $end, $qualification, $apply, $intern, $fulltime, $parttime, $freelance, $salary_type, $salary_period, $salary_min, $salary_max, $admin_id, $publish, $status){
		$result = 0;

		$text = "UPDATE $this->table SET Jobs_title = '$title', Jobs_content = '$content', Jobs_ref_companyID = '$company_id', 
			Jobs_ref_fieldID = '$field_id', Jobs_ref_levelID = '$level_id', Jobs_ref_cityID = '$city_id', Jobs_exp_year = '$exp', 
			Jobs_post_start = '$start', Jobs_post_end = '$end', Jobs_qualification_info = '$qualification', Jobs_apply_info = '$apply', 
			Jobs_intern = '$intern', Jobs_fulltime = '$fulltime', Jobs_parttime = '$parttime', Jobs_freelance = '$freelance', 
			Jobs_salary_type = '$salary_type', Jobs_salary_period = '$salary_period', Jobs_salary_min = '$salary_min',
			Jobs_salary_max = '$salary_max', Jobs_modify_adminID = '$admin_id', Jobs_publish = '$publish', Jobs_status = '$status' 
			WHERE Jobs_ID = '$id'";
		$query = mysql_query($text);
		if(mysql_affected_rows() == 1){
			$result = 1;
		}
		//$result = $text;
		return $result;
	}

	// function delete data jobs in admin page
	public function delete_data($id){
		$result = 0;

		$text = "DELETE FROM $this->table WHERE Jobs_ID = '$id'";
		$query = mysql_query($text);
		if(mysql_affected_rows() == 1){
			$result = 1;
		}
		return $result;
	}

	// function set publish data jobs in admin page
	public function publish_data($id, $publish){
		$result = 0;

		$text = "UPDATE $this->table SET Jobs_publish = '$publish' WHERE Jobs_ID = '$id'";
		$query = mysql_query($text);
		if(mysql_affected_rows() == 1){
			$result = 1;
		}
		return $result;
	}

	// function set unpublish data jobs in admin page
	public function unpublish_data($id){
		$result = 0;
		$not_publish = "Not Publish";

		$text = "UPDATE $this->table SET Jobs_publish = '$not_publish'  WHERE Jobs_ID = '$id'";
		$query = mysql_query($text);
		if(mysql_affected_rows() == 1){
			$result = 1;
		}
		return $result;
	}

	//GET ALL RELEVANT DATA WITH KEYWORD in search page
	public function get_result($page=1, $keyword=null, $company_id=0, $city_id=0, $level_id=0, $field_id=0, $industry_id=0, $exp=0, $sort='weight'){
		$result = 0;
		$itemPerPage = 5;//SET HERE FOR JOB RESULT
		if($page <= 1 || $page == null){
			$limitBefore = 0;
		}else{
			$limitBefore = ($page-1) * $itemPerPage;
		}
		
		$condCompany = '';
		if($company_id != 0){$condCompany .= "AND jobs.Jobs_ref_companyID = '$company_id' ";}

		$condCity = '';
		if($city_id != 0){$condCity .= "AND jobs.Jobs_ref_cityID = '$city_id' ";}

		$condLevel = '';
		if($level_id != 0){$condLevel .= "AND jobs.Jobs_ref_levelID = '$level_id' ";}

		$condField = '';
		if($field_id != 0){$condField .= "AND jobs.Jobs_ref_fieldID = '$field_id' ";}

		$condIndustry = '';
		if($industry_id != 0){$condIndustry .= "AND jobs.Jobs_ref_industryID = '$industry_id' ";}

		$condExp = '';
		if($exp != 0){$condExp .= "AND jobs.Jobs_exp_year = '$exp' ";}

		$condSort = '';
		if($sort == 'recent'){$condSort = "ORDER BY Jobs_create_date DESC";}
		else if($sort == 'weight'){$condSort = "ORDER BY weight DESC";}
		else if($sort == 'mostview'){$condSort = "ORDER BY counter DESC";}
		else if($sort == 'mostpopular'){$condSort = "ORDER BY popular DESC, counter DESC";}
		else if($sort == 'leastview'){$condSort = "ORDER BY counter ASC";}
		else if($sort == 'leastpopular'){$condSort = "ORDER BY popular ASC, counter ASC";}
				
		$text = "SELECT DISTINCT 
			jobs.Jobs_ID AS Jobs_ID, jobs.Jobs_title AS Jobs_title, jobs.Jobs_content AS Jobs_content, 
			comp.Company_ID AS Company_ID, comp.Company_title AS Company_title, city.City_title AS City_title, 
			city.City_ID, lvl.Level_ID AS Level_ID, lvl.Level_title AS Level_title, field.Field_ID AS Field_ID, 
			field.Field_title AS Field_title, ind.Industry_title AS Industry_title, ind.Industry_ID AS Industry_ID,
			DATE_FORMAT(jobs.Jobs_create_date, '%Y/%m/%d %h:%i:%s') AS Jobs_create_date, 
			(SELECT COUNT(atja.Atja_ID) FROM AT_Job_Application atja WHERE atja.Atja_ref_jobsID = jobs.Jobs_ID) AS popular,
			IF(jobs.Jobs_title LIKE '$keyword%', 20, IF(jobs.Jobs_title LIKE '%$keyword%', 10, 0))+
				IF(jobs.Jobs_content LIKE '%$keyword%', 6, 0) AS weight, 
			(SELECT COUNT(hits.Hits_ID) FROM T_Hits hits WHERE hits.Hits_ref_jobsID = jobs.Jobs_ID) AS counter,
			(SELECT country.Country_title FROM T_Ref_City rcity LEFT JOIN T_Ref_Country country 
				ON rcity.City_ref_countryID = country.Country_ID WHERE rcity.City_ID = city.City_ID) AS Country_title
			FROM $this->table jobs $this->joinCom1 $this->joinCity1 $this->joinLvl1 $this->joinField1 $this->joinInd1 
			$this->joinHits1 $this->joinApply1 WHERE (jobs.Jobs_title LIKE '%$keyword%' OR jobs.Jobs_content LIKE '%$keyword%') 
			$condCompany $condCity $condLevel $condField $condIndustry $condExp $condSort 
			LIMIT $limitBefore, $itemPerPage";
		$query = mysql_query($text);
		if(mysql_num_rows($query) >= 1){
			$result = array();
			while($row = mysql_fetch_array($query,MYSQL_ASSOC)){
				$result[]=$row;
			}
		}
		//$result = $text;
		return $result;
	}

	//GET REMAINING
	public function get_remaining($keyword=null, $company_id=0, $city_id=0, $level_id=0, $field_id=0, $industry_id=0, $exp=0){
		$result = 0;
		
		$condCompany = '';
		if($company_id != 0){$condCompany .= "AND jobs.Jobs_ref_companyID = '$company_id' ";}

		$condCity = '';
		if($city_id != 0){$condCity .= "AND jobs.Jobs_ref_cityID = '$city_id' ";}

		$condLevel = '';
		if($level_id != 0){$condLevel .= "AND jobs.Jobs_ref_levelID = '$level_id' ";}

		$condField = '';
		if($field_id != 0){$condField .= "AND jobs.Jobs_ref_fieldID = '$field_id' ";}

		$condIndustry = '';
		if($industry_id != 0){$condIndustry .= "AND jobs.Jobs_ref_industryID = '$industry_id' ";}

		$condExp = '';
		if($exp != 0){$condExp .= "AND jobs.Jobs_exp_year = '$exp' ";}
				
		$text = "SELECT COUNT(jobs.Jobs_ID) AS count FROM $this->table jobs $this->joinCom1 
			$this->joinCity1 $this->joinLvl1 $this->joinField1 $this->joinInd1 
			WHERE (jobs.Jobs_title LIKE '%$keyword%' OR jobs.Jobs_content LIKE '%$keyword%') 
			$condCompany $condCity $condLevel $condField $condIndustry $condExp";
		$query = mysql_query($text);
		if(mysql_num_rows($query) >= 1){
			$row = mysql_fetch_array($query,MYSQL_ASSOC);
			$result =  $row['count'];
		}
		//$result = $text;
		return $result;
	}

	//GET TOTAL RELEVANT DATA WITH KEYWORD in search page
	public function get_total_result($keyword=null, $company_id=0, $city_id=0, $level_id=0, $field_id=0, $industry_id=0, $exp=0){
		$result = 0;
		
		$condCompany = '';
		if($company_id != 0){$condCompany .= "AND Jobs_ref_companyID = '$company_id' ";}

		$condCity = '';
		if($city_id != 0){$condCity .= "AND Jobs_ref_cityID = '$city_id' ";}

		$condLevel = '';
		if($level_id != 0){$condLevel .= "AND Jobs_ref_levelID = '$level_id' ";}

		$condField = '';
		if($field_id != 0){$condField .= "AND Jobs_ref_fieldID = '$field_id' ";}

		$condIndustry = '';
		if($industry_id != 0){$condIndustry .= "AND Jobs_ref_industryID = '$industry_id' ";}

		$condExp = '';
		if($exp != 0){$condExp .= "AND Jobs_exp_year = '$exp' ";}

		$text = "SELECT COUNT(Jobs_ID) AS count FROM $this->table $this->joinCom 
			$this->joinCity $this->joinLvl $this->joinField $this->joinInd
			WHERE (Jobs_title LIKE '%$keyword%' OR Jobs_content LIKE '%$keyword%') 
			$condCompany $condCity $condLevel $condField $condIndustry $condExp";
		$query = mysql_query($text);
		if(mysql_num_rows($query) >= 1){
			$row = mysql_fetch_array($query,MYSQL_ASSOC);
			$result =  $row['count'];
		}
		//$result = $text;
		return $result;
	}
/* END FUNCTION FOR ADMIN PAGE */

/* START FUNCTION FOR API */
	// function api get data
	public function get_data($publish=1, $keyword=null, $level_id=0, $field_id=0, $city_id=0, $intern=0, $fulltime=0, $parttime=0, $freelance=0){
		$data = "There is no data.";
		
		if($publish != 1){
			$this->publish = "Jobs_publish IN('Publish', 'Not Publish') ";
		}

		$condKeyword = '';
		if($keyword != ''){$condKeyword .="AND Jobs_title LIKE '%$keyword%' OR Jobs_content LIKE '%$keyword%' OR Jobs_qualification_info LIKE '%$keyword%' OR Jobs_apply_info LIKE '%$keyword%' ";}

		$condLevel = '';
		if($level_id != 0){$condLevel .="AND Level_ID = '$level_id' ";}
		
		$condField = '';
		if($field_id != 0){$condField .="AND Field_ID = '$field_id' ";}

		$condCity = '';
		if($city_id != 0){$condCity .=" AND City_ID = '$city_id' ";}

		$condIntern = '';
		if($intern != 0){$condIntern .="AND Jobs_intern = '$intern' ";}

		$condFulltime = '';
		if($fulltime != 0){$condFulltime .="AND Jobs_fulltime = '$fulltime' ";}

		$condParttime = '';
		if($parttime != 0){$condParttime .="AND Jobs_parttime = '$parttime' ";}

		$condFreelance = '';
		if($freelance != 0){$condFreelance .="AND Jobs_freelance = '$freelance' ";}
	
		$text = "SELECT Jobs_ID, Jobs_title, Jobs_content, Jobs_ref_companyID, Jobs_ref_industryID, Jobs_ref_fieldID, Jobs_ref_levelID, 
			Jobs_ref_cityID, Jobs_exp_year, Jobs_post_start, Jobs_post_end, Jobs_qualification_info, Jobs_apply_info, Jobs_intern, Jobs_fulltime, 
			Jobs_parttime, Jobs_freelance, Jobs_publish, Jobs_status, Jobs_modify_date, Jobs_create_date, Company_title, Level_title, Field_title,
			City_title, IF(Jobs_title LIKE '$keyword%', 20, IF('Jobs_title' LIKE '%$keyword%', 10, 0))+IF(Jobs_content LIKE '%$keyword%', 4, 0)+
			IF('Jobs_qualification_info' LIKE '%$keyword%', 1, 0)+IF('Jobs_apply_info' LIKE '%$keyword%', 1, 0) AS weight FROM $this->table 
			$this->joinCom $this->joinLvl $this->joinField $this->joinCity WHERE $this->publish $condKeyword $condLevel $condField $condCity 
			$condIntern $condFulltime $condParttime $condFreelance $this->order, weight DESC";
 		$query = mysql_query($text);
		if(mysql_num_rows($query) >= 1){
			$data = array();
			while($row = mysql_fetch_array($query,MYSQL_ASSOC)){
				$data[]=$row;
			}
		}
		return $data;
	}

	// function api for related by tag
	public function get_related_by_tag($id, $array_tag){
		$result = 0;

		$cond = '';
		if($array_tag != ''){
			$cond .="IN($array_tag)";
		}

		$text = "SELECT Jobs_ID, Jobs_title, Jobs_intern, Jobs_fulltime, Jobs_parttime, Jobs_freelance, Tagging_ref_tagID, Field_title, 
			Company_title, City_title, Country_title FROM AT_Tagging LEFT JOIN T_Jobs ON Tagging_ref_table = 'jobs' AND Tagging_ref_tableID = Jobs_ID 
			$this->joinField $this->joinCom $this->joinCity JOIN T_Ref_Country ON Country_ID = City_ref_countryID WHERE Jobs_ID != '$id' AND Tagging_ref_tagID $cond GROUP BY Jobs_title 
			ORDER BY Jobs_create_date DESC LIMIT 0,5";
		$query = mysql_query($text);
		if(mysql_num_rows($query) >= 1){
			$result = array();
			while($row = mysql_fetch_assoc($query)){
				$result[] = $row;
			}
		}
		//$result = $text;
		return $result;
	}

	// function api get data detail
	public function get_data_detail($id){
		$result = 0;

		$text = "SELECT Jobs_ID, Jobs_title, Jobs_content, Jobs_ref_companyID, Jobs_ref_industryID, Jobs_ref_fieldID, Jobs_ref_levelID, 
			Jobs_ref_cityID, Jobs_exp_year, Jobs_post_start, Jobs_post_end, Jobs_qualification_info, Jobs_apply_info, Jobs_intern, Jobs_fulltime, 
			Jobs_parttime, Jobs_freelance, Jobs_publish, Jobs_status, Jobs_modify_date, Jobs_create_date, Company_ref_industryID, Company_title, 
			Company_motto, Company_content, Company_email, Company_img, Company_img_thmb, Level_title, Field_title, City_ref_countryID, City_title,
			(SELECT Country_title FROM T_Ref_Country WHERE Country_ID = City_ref_countryID) AS Country_title
			FROM $this->table $this->joinCom $this->joinLvl 
			$this->joinField $this->joinCity WHERE Jobs_ID = '$id'";
		$query = mysql_query($text);
		if(mysql_num_rows($query) >= 1){
			$result = array();
			while($row = mysql_fetch_assoc($query)){
				$result[] = $row;
			}
		}
		//$result = $text;
		return $result;
	}

	//function api get related by company
	public function get_related_by_company($id, $company_id){
		$result = 0;

		$text = "SELECT Jobs_ID, Jobs_title, Jobs_intern, Jobs_fulltime, Jobs_parttime, Jobs_freelance, City_title, City_ref_countryID, Country_ID, Country_title
			FROM $this->table $this->joinCom $this->joinCity JOIN T_Ref_Country ON Country_ID = City_ref_countryID WHERE Jobs_ID != '$id' AND Jobs_ref_companyID = '$company_id'";
		$query = mysql_query($text);
		if(mysql_num_rows($query) >= 1){
			$result = array();
			while($row = mysql_fetch_assoc($query)){
				$result[] = $row;
			}
		}
		//$result = $text;
		return $result;
	}

	//function api get recent_viewed
	public function get_recent_viewed($user_id, $ip){
		$result = 0;

		if($user_id != ''){ //if user login, check user id
			$cond = "WHERE Hits_ref_userID = '$user_id'";
		}else{ //if not login, check ip address
			$cond = "WHERE Hits_ref_userID = 0 AND Hits_ip = '$ip'";
		}

		$text = "SELECT Jobs_ID, Jobs_title, Jobs_intern, Jobs_fulltime, Jobs_parttime, Jobs_freelance, Field_title, 
			Company_title, City_title, Country_title FROM $this->table $this->joinHits	$this->joinField $this->joinCom $this->joinCity JOIN T_Ref_Country ON Country_ID = City_ref_countryID
			$cond ORDER BY Hits_create_date DESC LIMIT 0,20";
		$query = mysql_query($text);
		if(mysql_num_rows($query) >= 1){
			$result = array();
			while($row = mysql_fetch_assoc($query)){
				$result[] = $row;
			}
		}
		//$result = $text;
		return $result;
	}
/* END FUNCTION FOR API */
}

?>