<?php
class Doc{

	private $table = "T_Doc";
	private $tableDc = "T_DocCat";

// START FUNCTION FOR API
	/* 	Fungsi GET DATA */
	public function get_data_recent($user_id){
		$result = 0;

		$text = "SELECT Doc_ID, Doc_fileLoc, Doc_title, Doc_desc, DocCat_name, Doc_create_date, 
			Doc_modify_date FROM $this->table doc LEFT JOIN $this->tableDc dc ON dc.DocCat_ID = doc.DocCat_ID 
			WHERE doc.User_ID = '$user_id' AND Doc_publish = 'Publish' AND DocCat_publish = 'Publish' 
			ORDER BY Doc_modify_date DESC LIMIT 0,12";
		$query = mysql_query($text);
		if(mysql_num_rows($query) >= 1){
			$result = array();
			while($row = mysql_fetch_assoc($query)){
				$result[] = $row;
			}
		}
		return $result;
	}

	public function get_data_by_doc_cat($doccat_id){
		$result = 0;

		$text = "SELECT * FROM $this->table WHERE DocCat_ID = '$doccat_id' AND Doc_publish = 'Publish' ORDER BY Doc_modify_date DESC";
		$query = mysql_query($text);
		if(mysql_num_rows($query) >= 1){
			$result = array();
			while($row = mysql_fetch_assoc($query)){
				$result[] = $row;
			}
		}
		return $result;
	}

	public function get_data_detail($id){
        $result = 0;
        
        $text = "SELECT dc.DocCat_ID AS DocCat_ID, dc.DocCat_name AS DocCat_name, doc.Doc_ID AS Doc_ID, 
        	doc.Doc_title AS Doc_title, doc.Doc_fileLoc AS Doc_fileLoc, doc.Doc_desc AS Doc_desc, 
        	doc.Doc_cover_img AS Doc_cover_img, doc.User_ID AS User_ID, doc.Doc_type AS Doc_type,
        	doc.Doc_fileSize AS Doc_fileSize, doc.Doc_feature AS Doc_feature, 
        	doc.Doc_Publish AS Doc_publish, doc.Doc_create_date AS Doc_create_date 
        	FROM $this->table doc LEFT JOIN $this->tableDc dc ON dc.DocCat_ID = doc.DocCat_ID WHERE 
        	doc.Doc_ID = '$id'";  
        $query = mysql_query($text);
        if(mysql_num_rows($query) >= 1){
            $result = array();
            while($row = mysql_fetch_array($query,MYSQL_ASSOC)){
                $result[]=$row;
            }
        }
        //$result = $text;
        return $result;
    }
	/* 	End Fungsi GET DATA */

	/* Start insert function */
	public function insert_data($fileloc, $title, $desc, $cover, $doccat_id, $user_id, $type, $filesize, $feature, $publish){
		$result = 0;

		$text = "INSERT INTO $this->table (Doc_fileLoc, Doc_title, Doc_desc, Doc_cover_img, DocCat_ID, User_ID, Doc_type, Doc_fileSize, Doc_feature, Doc_publish, Doc_create_date)VALUES('$fileloc', '$title', '$desc', '$cover', '$doccat_id', '$user_id', '$type', '$filesize', '$feature', '$publish', NOW())";
		$query = mysql_query($text);
		if($query){
			$result = mysql_insert_id();
		}
		return $result;
	}
	/* End function insert */

	/* Fungsi Update */
	public function update_data_doc($id, $fileloc, $title, $desc, $cover, $doccat_id, $type, $filesize, $feature, $publish){
		$result = 0;

		$this->remove_doc($id); //remove document before

		$text = "UPDATE $this->table SET Doc_fileLoc = '$fileloc', Doc_title = '$title', Doc_desc = '$desc', Doc_cover_img = '$cover', DocCat_ID = '$doccat_id', Doc_type = '$type', Doc_fileSize = '$filesize', Doc_feature = '$feature', Doc_publish = '$publish' WHERE Doc_ID = '$id' ";
		$query = mysql_query($text);
		if(mysql_affected_rows() == 1){
			$result = 1;
		}
		return $result;
	}

	public function update_data($id, $title, $desc, $cover, $doccat_id, $feature, $publish){
		$result = 0;

		$text = "UPDATE $this->table SET Doc_title = '$title', Doc_desc = '$desc', DocCat_ID = '$doccat_id', Doc_feature = '$feature', Doc_publish = '$publish' WHERE Doc_ID = '$id' ";
		$query = mysql_query($text);
		if(mysql_affected_rows() == 1){
			$result = 1;
		}
		return $result;
	}
	/* End Fungsi Update */

	/* 	Fungsi Delete */
	public function delete_data($id){
		$result = 0;

		$this->remove_doc($id); //remove document before

		$text = "DELETE FROM $this->table WHERE Doc_ID = '$id' ";
		$query = mysql_query($text);
		if(mysql_affected_rows() == 1){
			$result = 1;
		}
		return $result;
	}
	/* 	End Delete */

	//start function REMOVE doc
    public function remove_doc($id){
        $result = 0;
        $flag_doc = 0;
        $text = "SELECT Doc_fileLoc FROM $this->table WHERE Doc_ID = '$id' ";
        $query = mysql_query($text);
        if(mysql_num_rows($query) == 1){
            $row = mysql_fetch_assoc($query);
            $deleteDoc = $_SERVER['DOCUMENT_ROOT']."/".$row['Doc_fileLoc'];
            if (file_exists($deleteDoc)) {
                unlink($deleteDoc);
                $flag_doc = 1;
            }
            
            if($flag_doc == 1){
                $result = 1;
            }
        }
        return $result;
    }//end function remove doc

    //function get data by user for resume
    public function get_data_resume($user_id){
        $result = 0;
        
        $text = "SELECT dc.DocCat_ID AS DocCat_ID, dc.DocCat_name AS DocCat_name, doc.Doc_ID AS Doc_ID, 
        	doc.Doc_title AS Doc_title, doc.Doc_fileLoc AS Doc_fileLoc, doc.Doc_desc AS Doc_desc, 
        	doc.Doc_cover_img AS Doc_cover_img, doc.User_ID AS User_ID, doc.Doc_Publish AS Doc_publish, 
        	doc.Doc_create_date AS Doc_create_date FROM $this->table doc LEFT JOIN $this->tableDc dc 
        	ON dc.DocCat_ID = doc.DocCat_ID WHERE doc.User_ID = '$user_id' AND doc.Doc_Publish = 'Publish' 
        	ORDER BY doc.Doc_create_date DESC LIMIT 0,6";  
        $query = mysql_query($text);
        if(mysql_num_rows($query) >= 1){
            $result = array();
            while($row = mysql_fetch_array($query,MYSQL_ASSOC)){
                $result[]=$row;
            }
        }
        //$result = $text;
        return $result;
    }
// END FUNCTION API
}
?>