<?php 
class Admin{
    
    private $table = "T_Admin";
    private $tableSetting = "AT_Admin_Setting";
    private $joinAdmin = "LEFT JOIN T_Admin admin ON admin.id = setting.Admin_ID ";
    private $joinRole = "LEFT JOIN T_Admin_Role role ON role.Role_ID = setting.Role_ID ";
    
    public function checkEmail($email){
        $result = 0; //NOT GOOD
        if($email != null){
            $text = "SELECT id FROM $this->table WHERE email = '$email' ";
            $query = mysql_query($text);
            if(mysql_num_rows($query) < 1){
                $result = 1;
            }
        }
        return $result;
    }
    
    public function admin_create($username, $email, $password, $admin_id){
        $result = 0;//FAILED EMAIL ALREADY EXIST
        if($username != null && $email != null && $password != null){
            if($this->checkEmail($email) == 1){
                $text = "INSERT INTO $this->table (username, email, password, user_id)VALUES('$username', '$email', '$password', '$admin_id')";
                $query = mysql_query($text);
                if(mysql_affected_rows() >= 1){
                    $result = 1;
                }
            }
        }
        return $result;
    }
    
    public function admin_login($username, $password){
        $result = 0;//FAILED
        
        if($username != null && $password != null){
            //$text = "SELECT id,email FROM $this->table WHERE username = '$username' AND password = '$password' LIMIT 0,1";
            $text = "SELECT admin.id AS Admin_ID, admin.email AS Admin_email, 
                role.Role_ID AS Role_ID, role.Role_name AS Admin_role,
                setting.Education_show AS Education_show, setting.Company_show AS Company_show, 
                setting.Industry_show AS Industry_show, setting.Field_show AS Field_show,
                setting.Jobs_show AS Jobs_show, setting.User_show AS User_show,
                setting.Admin_show AS Admin_show, setting.Certificate_show AS Certificate_show,
                setting.Skill_show AS Skill_show, setting.Country_show AS Country_show,
                setting.City_show AS City_show, setting.Tag_show AS Tag_show, setting.Level_show AS Level_show,
                setting.Setting_show AS Setting_show FROM $this->tableSetting setting $this->joinAdmin 
                $this->joinRole WHERE admin.username = '$username' AND admin.password = '$password' LIMIT 0,1 ";
            $query = mysql_query($text);
            if(mysql_num_rows($query) == 1){//HAS TO BE EXACT 1 RESULT
                $row = mysql_fetch_array($query,MYSQL_ASSOC);
                $_SESSION['admin_username'] = $username;
                $_SESSION['admin_email'] = $row['Admin_email'];
                $_SESSION['admin_id'] = $row['Admin_ID'];
                $_SESSION['admin_role'] = $row['Admin_role'];
                $_SESSION['admin_role_id'] = $row['Role_ID'];
                //for admin sidebar
                $_SESSION['education'] = $row['Education_show'];
                $_SESSION['company'] = $row['Company_show'];
                $_SESSION['industry'] = $row['Industry_show'];
                $_SESSION['field'] = $row['Field_show'];
                $_SESSION['jobs'] = $row['Jobs_show'];
                $_SESSION['user'] = $row['User_show'];
                $_SESSION['admin'] = $row['Admin_show'];
                $_SESSION['certificate'] = $row['Certificate_show'];
                $_SESSION['skill'] = $row['Skill_show'];
                $_SESSION['country'] = $row['Country_show'];
                $_SESSION['city'] = $row['City_show'];
                $_SESSION['level'] = $row['Level_show'];
                $_SESSION['tag'] = $row['Tag_show'];
                $_SESSION['setting'] = $row['Setting_show'];
                $result = 1;
            }
        }
        return $result;
    }
    
    
    public function admin_check_login(){
        $result = 0;
        if(isset($_SESSION['admin_id'])){
            $result = 1;
        }
        return $result;
    }
    
    public function admin_logout(){
        unset($_SESSION['admin_username']);
        unset($_SESSION['admin_email']);
        unset($_SESSION['admin_id']);
        unset($_SESSION['admin_role']);
        unset($_SESSION['admin_role_id']);
        //for admin sidebar
        unset($_SESSION['education']);
        unset($_SESSION['company']);
        unset($_SESSION['industry']);
        unset($_SESSION['field']);
        unset($_SESSION['jobs']);
        unset($_SESSION['user']);
        unset($_SESSION['admin']);
        unset($_SESSION['certificate']);
        unset($_SESSION['skill']);
        unset($_SESSION['country']);
        unset($_SESSION['city']);
        unset($_SESSION['level']);
        unset($_SESSION['tag']);
        unset($_SESSION['setting']);
        return true;
    }
    
    public function admin_delete($id){
        $result = 0;//FAILED EMAIL ALREADY EXIST
        if($id != null){
            $text = "DELETE FROM $this->table WHERE id = '$id'";
            $query = mysql_query($text);
            if(mysql_affected_rows() >= 1){
                $result = 1;
            }
        }
        return $result;
    }
    
        //START FUNCTION IN adminMgr-User.php
    public function get_admin($id = null){
        $data = 0;
        $cond = '';
        if($id != null){
            $cond .= "WHERE id = '$id' ";
        }
        $text = "SELECT id AS admin_id, email AS admin_email, username AS admin_username, password AS admin_password FROM $this->table $cond";
        $query = mysql_query($text);
        if(mysql_num_rows($query) >= 1){
            $data = array();
            while($row = mysql_fetch_array($query,MYSQL_ASSOC)){
                $data[] = $row;
            }
        }
        return $data;
    }
        //END FUNCTION IN adminMgr-User.php
    
       //START FUNCTION IN adminMgr-UserDetail.php
    public function admin_update($id, $username,$email, $admin_id){
            $result = 0;//failed
            if($id != null){
                $text = "UPDATE $this->table SET email = '$email', username = '$username', modify_user_id = '$admin_id' WHERE id = '$id' ";
                $query = mysql_query($text);
                if(mysql_affected_rows() == 1){
                    $result = 1;
                }
            }
            return $result;
        }
        
            //FUNCTION TO UPDATE PASSWORD
public function admin_update_password($id, $old, $new){
    $result = 0;
    if($id != null && $old != null){
        $text_check = "SELECT password FROM $this->table WHERE id = '$id'";
        $query_check = mysql_query($text_check);
        if(mysql_num_rows($query_check) == 1){
            $row = mysql_fetch_array($query_check,MYSQL_ASSOC);
            $real_old = $row['password'];
	    
            if($real_old == $old){
                if($old != $new){//OLD PASSWORD AND NEW IS NOT SAME
                    $text = "UPDATE $this->table SET password = '$new' WHERE id = '$id' ";
                    $query = mysql_query($text);
                    if(mysql_affected_rows() == 1){
                        $result = 1;
                    }
                }   
            }
        }
    }
    return $result;
}
        //END FUNCTION IN adminMgr-UserDetail.php
}
?>