<?php 
class Ref_City{

	private $table = "T_Ref_City";
	private $publish = "City_publish IN('Publish') "; 
	private $order = "ORDER BY City_title ASC";
	private $itemPerPage = 20;
	private $joinCountry = " LEFT JOIN T_Ref_Country ON City_ref_countryID = Country_ID ";

	//FUNCTION TO GET AMOUNT OF ALL PUBLISHED CITY IN DATABASE
	public function get_total($letter = null, $publish = 1){
		$result = 0;
		$cond = '';
		if($letter != null){
			$cond .="AND City_title LIKE '$letter%' ";
		}
		if(isset($publish)){
			if($publish != 1){$this->publish = "City_publish IN('Publish', 'Not Publish') ";}
		}
		$text = "SELECT count(City_ID) AS count FROM $this->table WHERE $this->publish $cond ";
		$query = mysql_query($text);
		if(mysql_num_rows($query) >= 1){
			$row = mysql_fetch_array($query,MYSQL_ASSOC);
			$result =  $row['count'];
		}
		return $result;
	}

	public function get_total_page($total_data){
		$num_page = ceil($total_data / $this->itemPerPage);
		return $num_page;
	}	

	//FUNCTION TO GET data OF ALL PUBLISHED City IN DATABASE
	//1 to set publish, 0 to get publish and not published data
	public function get_data_by_page($page=0, $letter=null, $publish=1){
		$data = "There is no result.";
		if($page <= 1 || $page == null){
			$limitBefore = 0;
		}else{
			$limitBefore = ($page-1) * $this->itemPerPage;
		}
		$cond = '';
		if($letter != null){
			$cond .="WHERE City_title LIKE '$letter%' ";
		}
		if($publish != 1){
			$this->publish = "City_publish IN('Publish', 'Not Publish') ";
		}
 		//$text = "SELECT * FROM $this->table company WHERE $this->publish $cond $this->order LIMIT $limitBefore, $this->itemPerPage";
 		$text = "SELECT * FROM $this->table $this->joinCountry $cond $this->order LIMIT $limitBefore, $this->itemPerPage";
 		$query = mysql_query($text);
		if(mysql_num_rows($query) >= 1){
			$data = array();
			while($row = mysql_fetch_array($query,MYSQL_ASSOC)){
				$data[]=$row;
			}
		}
		return $data;
	}

	// function for api and admin page
	public function get_index(){
		$result = 0;

		$text = "SELECT City_ID, City_title, Country_abbr FROM $this->table $this->joinCountry ORDER BY City_title ASC";
		$query = mysql_query($text);
		if(mysql_num_rows($query) >= 1){
			$result = array();
			while($row = mysql_fetch_assoc($query)){
				$result[] = $row;
			}
		}
		return $result;
	}

	//for api city
	public function get_list($title){
		$result = 0;

		$text = "SELECT City_title AS label, City_title AS value FROM $this->table WHERE $this->publish AND City_title LIKE '$title%' OR City_title LIKE '%$title' OR City_title LIKE '%$title%' $this->order";
		$query = mysql_query($text);
		if(mysql_num_rows($query) >= 1){
			$result = array();
			while($row = mysql_fetch_assoc($query)){
				$result[] = $row;
			}
		}
		return $result;
	}

	public function get_data_edit($id){
		$result = 0;

		$text = "SELECT * FROM $this->table WHERE City_ID = '$id'";
		$query = mysql_query($text);
		if(mysql_num_rows($query) >= 1){
			$result = array();
			while($row = mysql_fetch_assoc($query)){
				$result[] = $row;
			}
		}
		return $result;
	}

	public function insert_data($country_id, $title, $admin_id, $publish){
		$result = 0;

		$text = "INSERT INTO $this->table (City_ref_countryID, City_title, City_create_adminID, City_publish, City_create_date) VALUES ('$country_id', '$title', '$admin_id', '$publish', NOW())";
		$query = mysql_query($text);
		if($query){
			$result = mysql_insert_id(); 
		}
		return $result;
	}

	public function update_data($id, $country_id, $title, $admin_id, $publish){
		$result = 0;

		$text = "UPDATE $this->table SET City_ref_countryID = '$country_id', City_title = '$title', City_modify_adminID = '$admin_id', City_publish = '$publish' WHERE City_ID = '$id'";
		$query = mysql_query($text);
		if(mysql_affected_rows() == 1){
			$result = 1;
		}
		return $result;
	}

	public function delete_data($id){
		$result = 0;

		$text = "DELETE FROM $this->table WHERE City_ID = '$id'";
		$query = mysql_query($text);
		if(mysql_affected_rows() == 1){
			$result = 1;
		}
		return $result;
	}

	public function publish_data($id, $publish){
		$result = 0;

		$text = "UPDATE $this->table SET City_publish = '$publish' WHERE City_ID = '$id'";
		$query = mysql_query($text);
		if(mysql_affected_rows() == 1){
			$result = 1;
		}
		return $result;
	}

	public function unpublish_data($id){
		$result = 0;
		$not_publish = "Not Publish";

		$text = "UPDATE $this->table SET City_publish = '$not_publish'  WHERE City_ID = '$id'";
		$query = mysql_query($text);
		if(mysql_affected_rows() == 1){
			$result = 1;
		}
		return $result;
	}

	//for api check city
	public function check_city($title){//return 0 if not exist
		$data = 0;

		$text = "SELECT City_ID FROM $this->table WHERE City_title = '$title'";
		$query = mysql_query($text);
		if(mysql_num_rows($query) == 1){
			$row = mysql_fetch_array($query, MYSQL_ASSOC);
			$data = $row['City_ID'];
		}
		return $data;
	}//END CHECK-EXIST

}

?>