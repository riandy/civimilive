<?php 
class Organization{

	private $table = "T_Organization";
	private $publish = "Org_publish IN('Publish') "; 
	private $order = "ORDER BY Org_title ASC";
	private $itemPerPage = 20;
	private $joinCity = "LEFT JOIN T_Ref_City ON Org_ref_cityID = City_ID ";
	private $joinCountry = "LEFT JOIN T_Ref_Country ON Org_ref_countryID = Country_ID ";
	private $joinAT = "LEFT JOIN AT_Organization ON ATorg_ref_orgID = Org_ID ";
	/*private $joinHits = " LEFT JOIN T_Hits ON Hits_ref_OrgID = Org_ID ";
	private $joinApply = " LEFT JOIN AT_Job_Application ON Atja_ref_JobsID = Jobs_ID AND Jobs_ref_orgID = Org_ID ";

	//function in 6 POPULAR ORGANIZATION
	public function get_popular(){
		$result = 0;

		$text = "SELECT Org_ID , Org_title, Org_img, Org_motto, 
			(SELECT COUNT(Atja_ID) FROM AT_Job_Application WHERE Atja_ref_jobsID = Jobs_ID AND Jobs_ref_orgID = Org_ID) AS num_applicant,
			(SELECT COUNT(Hits_ID) FROM T_Hits WHERE Hits_ref_orgID = Org_ID) AS view,
			count(Jobs_ID) AS no_jobs FROM $this->table 
			$this->joinJob $this->joinHits $this->joinApply
			WHERE $this->publish AND Org_img IS NOT NULL GROUP BY Org_ID 
			ORDER BY num_applicant DESC, view DESC LIMIT 0,6";
		//ORDER BY no_jobs DESC
		$query = mysql_query($text);
		if(mysql_num_rows($query) >= 1){
			$result = array();
			while($row = mysql_fetch_assoc($query)){
				$result[] = $row;
			}
		}
		//$result = $text;
		return $result;
	}*/

//START FUNCTION FOR ADMIN PAGE
	public function get_index(){
		$result = 0;

		$text = "SELECT Org_ID, Org_title, Org_img, Org_motto, Org_img, count(ATorg_ID) as num_employee 
			FROM $this->table $this->joinAT GROUP BY Org_ID ORDER BY Org_title ASC";
		$query = mysql_query($text);
		if(mysql_num_rows($query) >= 1){
			$result = array();
			while($row = mysql_fetch_assoc($query)){
				$result[] = $row;
			}
		}
		return $result;
	}

	//FUNCTION TO GET data OF ALL PUBLISHED Organization IN DATABASE
	//1 to set publish, 0 to get publish and not published data
	public function get_data_by_page($page=0, $letter=null, $type){
		$data = "There is no result.";
		if($page <= 1 || $page == null){
			$limitBefore = 0;
		}else{
			$limitBefore = ($page-1) * $this->itemPerPage;
		}

		$cond = '';
		if($letter != null){
			$cond .="AND org.Org_title LIKE '$letter%' ";
		}
		
		$condType = "";
		if($type == 'publish'){
			$condType .= "org.Org_publish = 'Publish' ";
		}else if($type == 'notpublish'){
			$condType .= "org.Org_publish = 'Not Publish' ";
		}
 		
 		$text = "SELECT org.Org_ID AS Org_ID, org.Org_title AS Org_title, ind.Industry_ID AS Industry_ID,
 			(SELECT COUNT(atorg.ATorg_ID) FROM AT_Organization atorg WHERE atorg.ATorg_ref_orgID = org.Org_ID) AS work_counter,
 			ind.Industry_title AS Industry_title, org.Org_content AS Org_content, org.Org_img AS Org_img, 
 			org.Org_img_thmb AS Org_img_thmb, org.Org_publish AS Org_publish FROM $this->table org LEFT JOIN 
 			T_Industry ind ON org.Org_ref_industryID = ind.Industry_ID WHERE $condType $cond GROUP BY org.Org_ID 
 			ORDER BY org.Org_title ASC LIMIT $limitBefore, $this->itemPerPage";		
 		$query = mysql_query($text);
		if(mysql_num_rows($query) >= 1){
			$data = array();
			while($row = mysql_fetch_array($query,MYSQL_ASSOC)){
				$data[]=$row;
			}
		}
		//$data = $text;
		return $data;
	}

	//FUNCTION TO GET AMOUNT OF ALL PUBLISHED Organazitation IN DATABASE
	public function get_total($letter = null, $type){
		$result = 0;
		$cond = '';
		if($letter != null){
			$cond .="AND Org_title LIKE '$letter%' ";
		}
		
		$condType = "";
		if($type == 'publish'){
			$condType .= "Org_publish = 'Publish' ";
		}else if($type == 'notpublish'){
			$condType .= "Org_publish = 'Not Publish' ";
		}

		$text = "SELECT count(Org_ID) AS count FROM $this->table WHERE $condType $cond ";
		$query = mysql_query($text);
		if(mysql_num_rows($query) >= 1){
			$row = mysql_fetch_array($query,MYSQL_ASSOC);
			$result =  $row['count'];
		}
		return $result;
	}

	public function get_total_page($total_data){
		$num_page = ceil($total_data / $this->itemPerPage);
		return $num_page;
	}	

	public function get_data_edit($id){
		$result = 0;

		$text = "SELECT * FROM $this->table WHERE Org_ID = '$id'";
		$query = mysql_query($text);
		if(mysql_num_rows($query) >= 1){
			$result = array();
			while($row = mysql_fetch_assoc($query)){
				$result[] = $row;
			}
		}
		return $result;
	}

	public function insert_data($industry_id, $city_id, $country_id, $title, $motto, $content, $address, $email, $phone, $img, $img_thmb, $admin_id, $publish){
		$result = 0;

		$text = "INSERT INTO $this->table (Org_ref_industryID, Org_ref_cityID, Org_ref_countryID, Org_title, Org_motto, Org_content, Org_address, Org_email, Org_phone, Org_img, Org_img_thmb, Org_create_adminID, Org_publish, Org_create_date) VALUES ('$industry_id', '$city_id', '$country_id', '$title', '$motto', '$content', '$address', '$email', '$phone', '$img', '$img_thmb', '$admin_id', '$publish', NOW())";
		$query = mysql_query($text);
		if($query){
			$result = mysql_insert_id(); 
		}
		return $result;
	}

	public function update_data($id, $industry_id, $city_id, $country_id, $title, $motto, $content, $address, $email, $phone, $admin_id, $publish){
		$result = 0;

		$text = "UPDATE $this->table SET Org_ref_industryID = '$industry_id', Org_ref_cityID = '$city_id', Org_ref_countryID = '$country_id', Org_title = '$title', Org_motto = '$motto', Org_content = '$content', Org_address = '$address', Org_email = '$email', Org_phone = '$phone', Org_modify_adminID = '$admin_id', Org_publish = '$publish' WHERE Org_ID = '$id'";
		$query = mysql_query($text);
		if(mysql_affected_rows() == 1){
			$result = 1;
		}
		return $result;
	}

	public function update_data_image($id, $industry_id, $city_id, $country_id, $title, $motto, $content, $address, $email, $phone, $img, $img_thmb, $admin_id, $publish){
		$result = 0;
		$this->remove_image($id); //remove image before

		$text = "UPDATE $this->table SET Org_ref_industryID = '$industry_id', Org_ref_cityID = '$city_id', Org_ref_countryID = '$country_id', Org_title = '$title', Org_motto = '$motto', Org_content = '$content', Org_address = '$address', Org_email = '$email', Org_phone = '$phone', Org_img = '$img', Org_img_thmb = '$img_thmb', Org_modify_adminID = '$admin_id', Org_publish = '$publish' WHERE Org_ID = '$id'";
		$query = mysql_query($text);
		if(mysql_affected_rows() == 1){
			$result = 1;
		}
		return $result;
	}

	public function remove_image($id){
		$result = 0;
		$flag_img = 0;
		$flag_img_thmb = 0;

		$text = "SELECT Org_ID, Org_img, Org_img_thmb FROM $this->table WHERE Org_ID = '$id' ";
		$query = mysql_query($text);
		if(mysql_num_rows($query) == 1){
			$row = mysql_fetch_assoc($query);
			$deleteImg = $_SERVER['DOCUMENT_ROOT']."/".$row['Org_img'];
            if (file_exists($deleteImg)) {
                unlink($deleteImg);
                $flag_img = 1;
            }
            $deleteImgThmb = $_SERVER['DOCUMENT_ROOT']."/".$row['Org_img_thmb'];
            if (file_exists($deleteImgThmb)) {
                unlink($deleteImgThmb);
                $flag_img_thmb = 1;
            }

            if($flag_img == 1 && $flag_img_thmb ==1){
            	$result = 1;
            }
		}
		return $result;
	}

	public function delete_data($id){
		$result = 0;
		$this->delete_tags($id); //delete data tags
		$this->remove_image($id); //remove image before

		$text = "DELETE FROM $this->table WHERE Org_ID = '$id'";
		$query = mysql_query($text);
		if(mysql_affected_rows() == 1){
			$result = 1;
		}
		return $result;
	}

	//function for delete data in table AT_Tagging
    public function delete_tags($id){
        $result = 0;

        $text = "DELETE FROM AT_Tagging WHERE Tagging_ref_table = 'organization' AND Tagging_ref_tableID = '$id'";
        $query = mysql_query($text);
        if(mysql_affected_rows() >= 1){
            $result = 1;
        }
        return $result;
    }

	public function publish_data($id, $publish){
		$result = 0;

		$text = "UPDATE $this->table SET Org_publish = '$publish' WHERE Org_ID = '$id'";
		$query = mysql_query($text);
		if(mysql_affected_rows() == 1){
			$result = 1;
		}
		return $result;
	}

	public function unpublish_data($id){
		$result = 0;
		$not_publish = "Not Publish";

		$text = "UPDATE $this->table SET Org_publish = '$not_publish'  WHERE Org_ID = '$id'";
		$query = mysql_query($text);
		if(mysql_affected_rows() == 1){
			$result = 1;
		}
		return $result;
	}
//END FUNCTION FOR ADMIN PAGE

	//for api check organization
	public function check_organization($org, $city, $country){//return 0 if not exist
		$data = 0;

		$text = "SELECT Org_ID FROM $this->table $this->joinCity $this->joinCountry 
			WHERE Org_title = '$org' AND City_title = '$city' AND Country_title = '$country' 
			ORDER BY Org_ID ASC LIMIT 0,1";
		$query = mysql_query($text);
		if(mysql_num_rows($query) == 1){
			$row = mysql_fetch_array($query, MYSQL_ASSOC);
			$data = $row['Org_ID'];
		}
		//$data = $text;
		return $data;
	}//END CHECK-EXIST

	//for api get list
	public function get_list($title){
		$result = 0;

		$text = "SELECT Org_id, Org_title, Org_img, City_title, Country_title,
			IF(Org_title LIKE '$title%', 20, IF(Org_title LIKE '%$title%', 15, 
				IF(Org_title LIKE '%$title', 10, 0))) AS weight
			FROM $this->table $this->joinCity $this->joinCountry 
			WHERE Org_publish = 'Publish' AND Org_title LIKE '$title%' OR 
			Org_title LIKE '%$title' OR Org_title LIKE '%$title%' 
			ORDER BY weight DESC, Org_title ASC";
		$query = mysql_query($text);
		if(mysql_num_rows($query) >= 1){
			$result = array();
			while($row = mysql_fetch_assoc($query)){
				$result[] = $row;
			}
		}
		//$result = $text;
		return $result;
	}
}

?>