<?php 
class Testimonial{
		
	private $table = "T_Testimonial";
	private $itemPerPage = 4;

//START FUNCTION FOR CLIENT PAGE
	public function insert_data($user_id, $img, $img_thmb, $content){
		$result = 0;

		$text = "INSERT INTO $this->table (Testimonial_ref_userID, Testimonial_img, Testimonial_img_thmb, Testimonial_content, Testimonial_create_date) VALUES('$user_id', '$img', '$img_thmb', '$content', NOW())";
		$query = mysql_query($text);
		if($query){
			$result = 1;
		}

		return $result;
	}

	public function get_data_index(){
		$result = 0;

		$text = "SELECT Testimonial_ID, Testimonial_featured, Testimonial_img, Testimonial_img_thmb, Testimonial_content,
        	Testimonial_create_date, User_ID, User_username, User_email, User_fname, User_lname FROM $this->table LEFT JOIN 
        	T_User ON Testimonial_ref_userID = User_ID WHERE Testimonial_publish = 'Publish' AND Testimonial_featured = 'yes' 
        	ORDER BY Testimonial_create_date DESC LIMIT 0,4";
        $query = mysql_query($text);
        if(mysql_num_rows($query) >= 1){
        	$result = array();
        	while($row = mysql_fetch_assoc($query)){
        		$result[] = $row;
        	}
        }

        return $result;
	}
//END FUNCTION FOR CLIENT PAGE

//START FUNCTION FOR ADMIN PAGE
	public function get_data_by_page($page=1, $type="new"){
        $result = 0;    

        $cond = "";
        if($type == "approved"){
        	$cond = "WHERE Testimonial_publish = 'Publish'";
        }else{
        	$cond = "WHERE Testimonial_publish != 'Publish'";
        }
        
        //get total data
        $text_total = "SELECT Testimonial_ID FROM $this->table $cond";
        $query_total = mysql_query($text_total);
        $total_data = mysql_num_rows($query_total);
        if($total_data < 1){$total_data = 0;}

        //get total page
        $total_page = ceil($total_data / $this->itemPerPage);

        if($page <= 1 || $page == null){
            $limitBefore = 0;
        }else{
            $limitBefore = ($page-1) * $this->itemPerPage;
        }

        $text = "SELECT Testimonial_ID, Testimonial_featured, Testimonial_img, Testimonial_img_thmb, Testimonial_content,
        	Testimonial_create_date, Testimonial_publish, User_ID, User_username, User_email, User_fname, User_lname 
        	FROM $this->table LEFT JOIN T_User ON Testimonial_ref_userID = User_ID $cond ORDER BY Testimonial_create_date 
        	DESC LIMIT $limitBefore, $this->itemPerPage";
        $query = mysql_query($text);
        if(mysql_num_rows($query) >= 1){
            $result = array();
            while($row = mysql_fetch_assoc($query)){
                $result[] = $row;
            }
        }
        
        if(is_array($result)){
            $result[0]['total_page'] = $total_page;
            $result[0]['total_data_all'] = $total_data;
            $result[0]['total_data'] = count($result);
        }
        //$result = $text;
        return $result;
    }

    public function update_testimonial($id, $featured, $publish, $content){
		$result = 0;

		$text = "UPDATE $this->table SET Testimonial_featured = '$featured', Testimonial_publish = '$publish', Testimonial_content = '$content' WHERE Testimonial_ID = '$id'";
		$query = mysql_query($text);
		if(mysql_affected_rows() == 1){
			$result = 1;
		}
		return $result;
	}

	public function delete_data($id){
		$result = 0;

		$text = "DELETE FROM $this->table WHERE Testimonial_ID = '$id'";
		$query = mysql_query($text);
		if(mysql_affected_rows() == 1){
			$result = 1;
		}
		return $result;
	}
//END FUNCTION FOR ADMIN PAGE
}
?>