<?php
class AT_Work{

	private $table = "AT_Work";
	private $joinCompany = "LEFT JOIN T_Company ON Company_ID = ATwork_ref_companyID ";
	private $joinUser = "LEFT JOIN T_User ON User_ID = ATwork_ref_userID ";
	private $itemPerPage = 20;

// START FUNCTION FOR ADMIN PAGE
	//FUNCTION TO GET AMOUNT OF ALL PUBLISHED Employee IN DATABASE
	public function get_total($company_id){
		$result = 0;
		$cond = '';
		if($letter != null){
			$cond .="AND Company_title LIKE '$letter%' ";
		}
		if(isset($publish)){
			if($publish != 1){$this->publish = "Company_publish IN('Publish', 'Not Publish') ";}
		}
		$text = "SELECT COUNT(ATwork_ID) AS count FROM $this->table $this->joinCompany $this->joinUser 
 			WHERE Company_ID = '$company_id'";
		$query = mysql_query($text);
		if(mysql_num_rows($query) >= 1){
			$row = mysql_fetch_array($query,MYSQL_ASSOC);
			$result =  $row['count'];
		}
		return $result;
	}

	public function get_total_page($total_data){
		$num_page = ceil($total_data / $this->itemPerPage);
		return $num_page;
	}	

	//FUNCTION TO GET data OF ALL PUBLISHED Employee IN DATABASE
	//1 to set publish, 0 to get publish and not published data
	public function get_data_by_company($page=0, $company_id){
		$data = "There is no result.";
		if($page <= 1 || $page == null){
			$limitBefore = 0;
		}else{
			$limitBefore = ($page-1) * $this->itemPerPage;
		}
 		
 		$text = "SELECT Company_ID, Company_title, User_ID, User_email, User_proPhoto, User_proPhotoThmb,
 			User_fname, User_lname, User_username, ATwork_ID, 
 			CASE WHEN ATwork_from_month = 1 THEN 'Jan'
 				WHEN ATwork_from_month = 2 THEN 'Feb'
 				WHEN ATwork_from_month = 3 THEN 'Mar'
 				WHEN ATwork_from_month = 4 THEN 'Apr'
 				WHEN ATwork_from_month = 5 THEN 'May'
 				WHEN ATwork_from_month = 6 THEN 'Jun'
 				WHEN ATwork_from_month = 7 THEN 'Jul'
 				WHEN ATwork_from_month = 8 THEN 'Aug'
 				WHEN ATwork_from_month = 9 THEN 'Sep'
 				WHEN ATwork_from_month = 10 THEN 'Oct'
 				WHEN ATwork_from_month = 11 THEN 'Nov'
 				WHEN ATwork_from_month = 12 THEN 'Dec'
 			END AS ATwork_from_month,
 			ATwork_from_year, 
 			CASE WHEN ATwork_to_month = 1 THEN 'Jan'
 				WHEN ATwork_to_month = 2 THEN 'Feb'
 				WHEN ATwork_to_month = 3 THEN 'Mar'
 				WHEN ATwork_to_month = 4 THEN 'Apr'
 				WHEN ATwork_to_month = 5 THEN 'May'
 				WHEN ATwork_to_month = 6 THEN 'Jun'
 				WHEN ATwork_to_month = 7 THEN 'Jul'
 				WHEN ATwork_to_month = 8 THEN 'Aug'
 				WHEN ATwork_to_month = 9 THEN 'Sep'
 				WHEN ATwork_to_month = 10 THEN 'Oct'
 				WHEN ATwork_to_month = 11 THEN 'Nov'
 				WHEN ATwork_to_month = 12 THEN 'Dec'
 			END AS ATwork_to_month,
 			ATwork_to_year, ATwork_title FROM $this->table $this->joinCompany $this->joinUser
 			WHERE Company_ID = '$company_id' ORDER BY User_fname LIMIT $limitBefore, $this->itemPerPage";
 		$query = mysql_query($text);
		if(mysql_num_rows($query) >= 1){
			$data = array();
			while($row = mysql_fetch_array($query,MYSQL_ASSOC)){
				$data[]=$row;
			}
		}
		//$data = $text;
		return $data;
	}

	// function index
	public function get_index($company_id){
		$result = 0;

		$text = "SELECT User_username, User_email, User_fname, User_lname, Company_ID, ATwork_ID, ATwork_title,
			ATwork_from_month, ATwork_from_year, ATwork_to_month, ATwork_to_year, ATwork_desc
			FROM $this->table $this->joinCompany $this->joinUser WHERE Company_ID = '$company_id'";
		$query = mysql_query($text);
		if(mysql_num_rows($query) >= 1){
			$result = array();
			while($row = mysql_fetch_assoc($query)){
				$result[] = $row;
			}
		}
		return $result;
	}

	public function get_data_detail($id){
		$result = 0;
		
 		$text = "SELECT Company_ID, Company_title, User_ID, User_email, User_fname, User_lname, ATwork_ID, 
 			ATwork_desc, ATwork_from_month, ATwork_from_year, ATwork_to_month, ATwork_to_year, ATwork_title, 
 			ATwork_current FROM $this->table $this->joinCompany $this->joinUser WHERE ATwork_ID = '$id'";
 		$query = mysql_query($text);
		if(mysql_num_rows($query) >= 1){
			$result = array();
			while($row = mysql_fetch_array($query,MYSQL_ASSOC)){
				$result[]=$row;
			}
		}
		//$result = $text;
		return $result;
	}
// END FUNCTION FOR ADMIN PAGE

// START OPERATION CRUD
	/* Start insert function */
	public function insert_data($company_id, $user_id, $title, $fromMonth, $fromYear, $toMonth, $toYear, $current, $desc){
		$result = 0;

		$text = "INSERT INTO $this->table (ATwork_ref_companyID, ATwork_ref_userID, ATwork_title, ATwork_from_month, ATwork_from_year, ATwork_to_month, ATwork_to_year, ATwork_current, ATwork_desc, ATwork_create_date) VALUES('$company_id', '$user_id', '$title', '$fromMonth', '$fromYear', '$toMonth', '$toYear', '$current', '$desc', NOW())";
		$query = mysql_query($text);
		if($query){
			//$result = 1;
			$result = mysql_insert_id();
		}
		return $result;
	}
	/* End function insert */

	/* Fungsi Update */
	public function update_data($id, $company_id, $title, $fromMonth, $fromYear, $toMonth, $toYear, $current, $desc){
		$result = 0;

		$text = "UPDATE $this->table SET ATwork_ref_companyID = '$company_id', ATwork_title = '$title', ATwork_from_month = '$fromMonth', ATwork_from_year = '$fromYear', ATwork_to_month = '$toMonth', ATwork_to_year = '$toYear', ATwork_current = '$current', ATwork_desc = '$desc' WHERE ATwork_ID = '$id' ";
		$query = mysql_query($text);
		if(mysql_affected_rows() == 1){
			$result = 1;
		}
		//$result = $text;
		return $result;
	}
	/* End Fungsi Update */

	/* 	Fungsi Delete */
	public function delete_data($id){
		$result = 0;
		$text = "DELETE FROM $this->table WHERE ATwork_ID = '$id' ";
		$query = mysql_query($text);
		if(mysql_affected_rows() == 1){
			$result = 1;
		}
		return $result;
	}
	/* 	End Delete */
// END OPERATION CRUD

// START FUNCTION FOR CLIENT PAGE
	//for api get data
	public function get_data($user_id){
		$result = 0;

		$text = "SELECT ATwork_ID, Company_title, Company_img, ATwork_title, ATwork_from_month, ATwork_from_year,
			ATwork_to_month, ATwork_to_year, ATwork_current, ATwork_desc, City_title, Country_title FROM T_Company 
			LEFT JOIN $this->table ON ATwork_ref_companyID = Company_ID LEFT JOIN T_Ref_City ON Company_ref_cityID = City_ID
			LEFT JOIN T_Ref_Country ON Company_ref_countryID = Country_ID WHERE ATwork_ref_userID = '$user_id' ORDER BY 
			ATwork_current DESC, ATwork_from_year DESC, ATwork_from_month DESC, ATwork_to_year DESC, ATwork_to_month DESC";
		$query = mysql_query($text);
		if(mysql_num_rows($query) >= 1){
			$result = array();
			while($row = mysql_fetch_assoc($query)){
				$result[] = $row;
			}
		}
		//$result = $text;
		return $result;
	}

	//for api check user
	public function check_user($user_id){//return 0 if not exist
		$data = 0;

		$text = "SELECT ATwork_ID FROM $this->table WHERE ATwork_ref_userID = '$user_id'";
		$query = mysql_query($text);
		if(mysql_num_rows($query) == 1){
			$row = mysql_fetch_array($query, MYSQL_ASSOC);
			$data = $row['ATwork_ID'];
		}
		return $data;
	}//END CHECK-EXIST
// END FUNCTION FOR CLIENT PAGE
}
?>