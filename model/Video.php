<?php
class Video{

	private $table = "T_Video";
	private $tableVc = "T_VideoCat";

// START FUNCTION FOR API
	/* Start insert function */
	public function insert_data($link, $title, $desc, $videocat_id, $user_id, $publish){
		$result = 0;

		$text = "INSERT INTO $this->table (Video_link, Video_title, Video_desc, VideoCat_ID, User_ID, Video_publish, Video_create_date)VALUES('$link', '$title', '$desc', '$videocat_id', '$user_id', '$publish', NOW())";
		$query = mysql_query($text);
		if($query){
			$result = mysql_insert_id();
		}
		return $result;
	}
	/* End function insert */

	/* Fungsi Update */
	public function update_data($id, $link, $title, $desc, $videocat_id, $user_id, $publish){
		$result = 0;

		$text = "UPDATE $this->table SET Video_link = '$link', Video_title = '$title', Video_desc = '$desc', VideoCat_ID = '$videocat_id', User_ID = '$user_id', Video_publish = '$publish' WHERE Video_ID = '$id' ";
		$query = mysql_query($text);
		if(mysql_affected_rows() == 1){
			$result = 1;
		}
		return $result;
	}
	/* End Fungsi Update */

	/* 	Fungsi GET DATA */
	public function get_data_by_video_cat($videocat_id){
		$result = 0;

		$text = "SELECT * FROM $this->table WHERE VideoCat_ID = '$videocat_id' AND Video_publish = 'Publish' ORDER BY Video_modify_date DESC";
		$query = mysql_query($text);
		if(mysql_num_rows($query) >= 1){
			$result = array();
			while($row = mysql_fetch_assoc($query)){
				$result[] = $row;
			}
		}
		return $result;
	}

	public function get_data_recent($user_id){
		$result = 0;

		$text = "SELECT Video_ID, Video_link, Video_title, Video_desc, VideoCat_name, Video_modify_date, Video_create_date FROM $this->table v LEFT JOIN T_VideoCat vc ON v.VideoCat_ID = vc.VideoCat_ID WHERE vc.User_ID = '$user_id' AND Video_publish = 'Publish' ORDER BY Video_modify_date DESC";
		$query = mysql_query($text);
		if(mysql_num_rows($query) >= 1){
			$result = array();
			while($row = mysql_fetch_assoc($query)){
				$result[] = $row;
			}
		}
		return $result;
	}

	public function get_data_detail($id){
        $result = 0;
        
        $text = "SELECT vc.VideoCat_ID AS VideoCat_ID, vc.VideoCat_name AS VideoCat_name, video.Video_ID AS Video_ID, 
        	video.Video_title AS Video_title, video.Video_desc AS Video_desc, video.Video_link AS Video_link, 
        	video.User_ID AS User_ID, video.Video_publish AS Video_publish FROM $this->table video LEFT JOIN 
        	$this->tableVc vc ON vc.VideoCat_ID = video.VideoCat_ID WHERE video.Video_ID = '$id'";  
        $query = mysql_query($text);
        if(mysql_num_rows($query) >= 1){
            $result = array();
            while($row = mysql_fetch_array($query,MYSQL_ASSOC)){
                $result[]=$row;
            }
        }
        //$result = $text;
        return $result;
    }
	/* 	End Fungsi GET DATA */

	/* 	Fungsi Delete */
	public function delete_data($id){
		$result = 0;
		$text = "DELETE FROM $this->table WHERE Video_ID = '$id' ";
		$query = mysql_query($text);
		if(mysql_affected_rows() == 1){
			$result = 1;
		}
		return $result;
	}
	/* 	End Delete */

	//function get data by user for resume
	public function get_data_resume($user_id){
        $result = 0;
        
        $text = "SELECT vc.VideoCat_ID AS VideoCat_ID, vc.VideoCat_name AS VideoCat_name, video.Video_ID AS Video_ID, 
        	video.Video_title AS Video_title, video.Video_desc AS Video_desc, video.Video_link AS Video_link, 
        	video.User_ID AS User_ID, video.Video_publish AS Video_publish FROM $this->table video LEFT JOIN 
        	$this->tableVc vc ON vc.VideoCat_ID = video.VideoCat_ID WHERE video.User_ID = '$user_id'
        	AND video.Video_publish = 'Publish' ORDER BY Video_create_date DESC LIMIT 0,6";  
        $query = mysql_query($text);
        if(mysql_num_rows($query) >= 1){
            $result = array();
            while($row = mysql_fetch_array($query,MYSQL_ASSOC)){
                $result[]=$row;
            }
        }
        //$result = $text;
        return $result;
    }
// END FUNCTION API
}
?>