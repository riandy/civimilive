<?php 
class News{
	
	private $table = "T_News";
    private $tablePhoto = "T_News_Photo";
	private $main_cond = "News_publish IN('Publish') ";
	private $joinCat = "JOIN T_News_Category ON Nc_ID = News_ref_ncID ";
	private $joinPhoto = "JOIN T_News_Photo ON Np_ref_newsID = News_ID ";
	private $order = "ORDER BY News_modify_date DESC ";
	private $itemPerPage = 6;
	private $itemPerPageAdmin = 12; //for admin page

//START FUNCTION FOR ADMIN PAGE
	public function get_data_by_page($page=1){
        $result = 0;    
        
        //get total data
        $text_total = "SELECT * FROM $this->table";
        $query_total = mysql_query($text_total);
        $total_data = mysql_num_rows($query_total);
        if($total_data < 1){$total_data = 0;}

        //get total page
        $total_page = ceil($total_data / $this->itemPerPageAdmin);

        if($page <= 1 || $page == null){
            $limitBefore = 0;
        }else{
            $limitBefore = ($page-1) * $this->itemPerPageAdmin;
        }

        $text = "SELECT News_ID, News_title, Nc_title, News_featured, News_author, News_publish, 
            News_create_date, Np_img, Np_img_thmb FROM $this->table $this->joinCat $this->joinPhoto 
            WHERE Np_main = '1' ORDER BY News_featured DESC, News_create_date DESC, News_publish DESC, 
            News_title ASC LIMIT $limitBefore, $this->itemPerPageAdmin";
        $query = mysql_query($text);
        if(mysql_num_rows($query) >= 1){
            $result = array();
            while($row = mysql_fetch_assoc($query)){
                $result[] = $row;
            }
        }
        
        if(is_array($result)){
            $result[0]['total_page'] = $total_page;
            $result[0]['total_data_all'] = $total_data;
            $result[0]['total_data'] = count($result);
        }
        //$result = $text;
        return $result;
    }

    public function get_data_edit($id){
        $result = 0;

        $text = "SELECT News_ID, News_title, News_featured, News_content, News_tag, News_photographer, News_photographer_link,
        	News_author, News_author_link, News_url_video, News_publish, News_ref_ncID, Np_img, Np_img_thmb
        	FROM $this->table $this->joinCat $this->joinPhoto WHERE News_ID = '$id' AND Np_main = '1'";
        $query = mysql_query($text);
        if(mysql_num_rows($query) >= 1){
            $result = array();
            while($row = mysql_fetch_assoc($query)){
                $result[] = $row;
            }
        }
        return $result;
    }

    public function get_data_edit_photo($id){
        $result = 0;

        $text = "SELECT Np_ID, Np_ref_newsID, Np_title,	Np_main, Np_img, Np_img_thmb FROM $this->table 
        	$this->joinPhoto WHERE News_ID = '$id' ORDER BY Np_main DESC, Np_modify_date DESC";
        $query = mysql_query($text);
        if(mysql_num_rows($query) >= 1){
            $result = array();
            while($row = mysql_fetch_assoc($query)){
                $result[] = $row;
            }
        }
        //$result = $text;
        return $result;
    }

    public function insert_data($category, $title, $content, $tag, $featured, $photographer, $photographerLink, $author, $authorLink, $videoLink, $publish){
        $result = 0;

        $text = "INSERT INTO $this->table (News_ref_ncID, News_title, News_content, News_tag, News_featured, News_photographer, News_photographer_link, News_author, News_author_link, News_url_video, News_publish, News_create_date) VALUES 
        	('$category', '$title', '$content', '$tag', '$featured', '$photographer', '$photographerLink', '$author', '$authorLink', '$videoLink', '$publish', NOW())";
        $query = mysql_query($text);
        if($query){
            $result = mysql_insert_id();
        }
        return $result;
    }

    public function delete_data($id){
        $result = 0;

        $this->delete_tags($id); //delete tag
        $this->remove_photo($id); //remove photo news related
        $this->delete_data_photo($id); //delete data photo news related

        $text = "DELETE FROM $this->table WHERE News_ID = '$id'";
        $query = mysql_query($text);
        if(mysql_affected_rows() == 1){
            $result = 1;
        }
        return $result;
    }

    //function for delete data tagging
    public function delete_tags($id){
        $result = 0;

        $text = "DELETE FROM AT_Tagging WHERE Tagging_ref_table = 'news' AND Tagging_ref_tableID = '$id'";
        $query = mysql_query($text);
        if(mysql_affected_rows() >= 1){
            $result = 1;
        }
        return $result;
    }

    public function delete_data_photo($id){
        $result = 0;

        $text = "DELETE FROM $this->tablePhoto WHERE Np_ref_newsID = '$id'";
        $query = mysql_query($text);
        if(mysql_affected_rows() == 1){
            $result = 1;
        }
        return $result;
    }

    public function remove_photo($id){
        $result = 0;
        $flag_img = 0;
        $flag_img_thmb = 0;

        $text = "SELECT Np_img, Np_img_thmb FROM $this->tablePhoto WHERE Np_ref_newsID = '$id'";
        $query = mysql_query($text);
        while ($row = mysql_fetch_array($query, MYSQL_ASSOC)) 
    	{
    		$deleteImg = $row['Np_img'];
	        if (file_exists($deleteImg)) {
	            unlink($deleteImg);
	            $flag_img = 1;
	        }
	        $deleteImgThmb = $row['Np_img_thmb'];
	        if (file_exists($deleteImgThmb)) {
	            unlink($deleteImgThmb);
	            $flag_img_thmb = 1;
	        }

            if($flag_img == 1 && $flag_img_thmb ==1){
	 	        $result = 1;
	        }
    	}
        return $result;
    }

    public function update_data($id, $category_id, $title, $featured, $tag, $photographer, $photographerLink, $author, $authorLink, $videoLink, $content, $publish){
        $result = 0;

        $text = "UPDATE $this->table SET News_ref_ncID = '$category_id', News_title = '$title', News_featured = '$featured', News_tag = '$tag', News_photographer = '$photographer', News_photographer_link = '$photographerLink', News_author = '$author', News_author_link = '$authorLink', News_url_video = '$videoLink', News_content = '$content', News_publish = '$publish' WHERE News_ID = '$id'";
        $query = mysql_query($text);
        if(mysql_affected_rows() == 1){
            $result = 1;
        }
        return $result;
    }
//END FUNCTION FOR ADMIN PAGE

//START FUNCTION FOR FRONT PAGE
    public function get_index(){
        $result = 0;

        $text = "SELECT News_ID, News_title, News_create_date FROM $this->table WHERE News_publish = 'Publish' ORDER BY News_create_date DESC LIMIT 0,2";
            $query = mysql_query($text);
            if(mysql_num_rows($query) >= 1){
                $result = array();
                while($row = mysql_fetch_assoc($query)){
                    $result[] = $row;
                }
            }
        return $result;
    }
    public function get_index_news(){
        $result = 0;

        $text = "SELECT News_ID, News_ref_ncID, News_title, News_content, News_author, News_author_link,
            News_tag, Nc_ID, Nc_title, News_create_date, Np_img, Np_img_thmb FROM $this->table $this->joinCat $this->joinPhoto WHERE News_publish = 'Publish' AND Np_main = '1' 
            ORDER BY News_featured DESC, News_create_date DESC, News_title ASC LIMIT 0,4";
            $query = mysql_query($text);
            if(mysql_num_rows($query) >= 1){
                $result = array();
                while($row = mysql_fetch_assoc($query)){
                    $result[] = $row;
                }
            }
        return $result;
    }

	public function get_news($page=1, $catID = 0){
		$result = 0;	
		
		$cond_cat = "";
		if($catID > 0){
			$cond_cat = " AND News_ref_ncID = '$catID' ";
		}

		//get total data
		$text_total = "SELECT News_ID FROM $this->table $this->joinCat $this->joinPhoto WHERE News_publish = 'Publish' AND Np_main = '1' $cond_cat";
		$query_total = mysql_query($text_total);
		$total_data = mysql_num_rows($query_total);
		if($total_data < 1){$total_data = 0;}

		//get total page
		$total_page = ceil($total_data / $this->itemPerPage);

		if($page <= 1 || $page == null){
            $limitBefore = 0;
	    }else{
	        $limitBefore = ($page-1) * $this->itemPerPage;
	    }

        $text = "SELECT News_ID, News_title, News_content, News_author, News_author_link, Nc_title, Np_img, Np_title, 
            News_create_date FROM $this->table $this->joinCat $this->joinPhoto WHERE News_publish = 'Publish' AND Np_main = '1' 
            $cond_cat ORDER BY News_featured DESC, News_create_date DESC, News_title ASC LIMIT $limitBefore, $this->itemPerPage";
		$query = mysql_query($text);
		if(mysql_num_rows($query) >= 1){
			$result = array();
			while($row = mysql_fetch_assoc($query)){
				$result[] = $row;
			}
		}
		if(is_array($result)){
		$result[0]['total_page'] = $total_page;
		$result[0]['total_data_all'] = $total_data;
		$result[0]['total_data'] = count($result);
		}
		// $result = $text;
		return $result;
	}

	public function get_by_id($id){
		$result = 0;

		$text = "SELECT News_ID, News_ref_ncID, News_title, News_content, News_author, News_author_link,
            News_tag, Nc_ID, Nc_title, DATE_FORMAT(News_create_date, '%d %M %Y') AS News_create_date FROM 
            $this->table $this->joinCat WHERE News_publish = 'Publish' AND News_ID = '$id' ";
			$query = mysql_query($text);
			if(mysql_num_rows($query) >= 1){
				$result = array();
				while($row = mysql_fetch_assoc($query)){
					$result[] = $row;
				}
			}
		return $result;
	}

	public function get_image_id($id){
		$result = 0;

		$text = "SELECT Np_ID, Np_title, Np_img, Np_img_thmb FROM $this->tablePhoto WHERE Np_ref_newsID = '$id' ORDER BY Np_main DESC";
		$query = mysql_query($text);
		if(mysql_num_rows($query) >= 1){
			$result = array();
			while($row = mysql_fetch_assoc($query)){
				$result[] = $row;
			}
		}
		return $result;
	}

	public function get_by_related($id, $catid){
		$result = 0;

		$text = "SELECT News_ID, News_title, Np_img, Np_title FROM $this->table $this->joinPhoto WHERE News_publish = 'Publish' AND Np_main = '1' AND News_ref_ncID = '$catid' AND News_ID != '$id' ORDER BY News_create_date DESC LIMIT 0,3 ";
		$query = mysql_query($text);
		if(mysql_num_rows($query) >= 1){
			$result = array();
			while($row = mysql_fetch_assoc($query)){
				$result[] = $row;
			}
		}
		return $result;
	}

    //function for get data related article in job_detail.php
    public function get_related_by_tag($id){
        $result = 0;

        $text = "SELECT News_ID, News_title, News_create_date, Nc_title, Np_img, Np_title FROM $this->table $this->joinCat $this->joinPhoto LEFT JOIN AT_Tagging ON Tagging_ref_table = 'news' AND Tagging_ref_tableID = News_ID
            WHERE News_publish = 'Publish' AND Np_main = '1' AND Tagging_ref_tagID IN($id) ORDER BY News_create_date DESC LIMIT 0,3";
        $query = mysql_query($text);
        if(mysql_num_rows($query) >= 1){
            $result = array();
            while($row = mysql_fetch_assoc($query)){
                $result[] = $row;
            }
        }
        return $result;
    }
	
    //function for get data news in android/ios
	public function get_news_mobile($page=1, $category_id=0, $sort="new"){
        $result = 0;    

        $cond = "";
        if($category_id != 0){
            $cond = "AND Nc_ID = '$category_id'";
        }

        if($sort == "popular"){
            $condPopular = "(SELECT COUNT(hu_ID) FROM T_Hits_User WHERE hu_type = 'news' AND hu_ref_typeID = News_ID) AS popular,"; 
            $condSort = "ORDER BY popular DESC";
        }else{
            $condPopular = "";
            $condSort = "ORDER BY News_create_date DESC";
        }
        
        //get total data
        $text_total = "SELECT * FROM $this->table";
        $query_total = mysql_query($text_total);
        $total_data = mysql_num_rows($query_total);
        if($total_data < 1){$total_data = 0;}

        //get total page
        $total_page = ceil($total_data / $this->itemPerPageAdmin);

        if($page <= 1 || $page == null){
            $limitBefore = 0;
        }else{
            $limitBefore = ($page-1) * $this->itemPerPageAdmin;
        }

        $text = "SELECT News_ID, News_title, Nc_title, News_featured, News_author, News_publish, News_create_date, Np_img,
            $condPopular Np_img_thmb FROM $this->table $this->joinCat $this->joinPhoto WHERE Np_main = '1' AND 
            News_publish = 'Publish' $cond $condSort LIMIT $limitBefore, $this->itemPerPageAdmin";
        $query = mysql_query($text);
        if(mysql_num_rows($query) >= 1){
            $result = array();
            while($row = mysql_fetch_assoc($query)){
                $result[] = $row;
            }
        }
        
        if(is_array($result)){
            $result[0]['total_page'] = $total_page;
            $result[0]['total_data_all'] = $total_data;
            $result[0]['total_data'] = count($result);
        }
        //$result = $text;
        return $result;
    }

    public function get_news_detail_mobile($id){
        $result = 0;

        $text = "SELECT News_ID, News_title, News_content, Nc_title, News_author, DATE_FORMAT(News_create_date, '%d %M %Y') AS News_create_date, 
            Np_img, Np_img_thmb FROM $this->table $this->joinCat $this->joinPhoto WHERE News_publish = 'Publish' AND News_ID = '$id' ";
            $query = mysql_query($text);
            if(mysql_num_rows($query) >= 1){
                $result = array();
                while($row = mysql_fetch_assoc($query)){
                    $result[] = $row;
                }
            }
        return $result;
    }
//END FUNCTION FOR FRONT PAGE
}
?>