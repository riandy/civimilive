<?php 
class Company{

	private $table = "T_Company";
	private $publish = "Company_publish IN('Publish') "; 
	private $order = "ORDER BY Company_title ASC";
	private $itemPerPage = 20;
	private $joinCity = "LEFT JOIN T_Ref_City ON Company_ref_cityID = City_ID";
	private $joinCountry = "LEFT JOIN T_Ref_Country ON Company_ref_countryID = Country_ID";
	private $joinJob = " LEFT JOIN T_Jobs ON Jobs_ref_companyID = Company_ID ";
	private $joinHits = " LEFT JOIN T_Hits ON Hits_ref_companyID = Company_ID ";
	private $joinApply = " LEFT JOIN AT_Job_Application ON Atja_ref_JobsID = Jobs_ID AND Jobs_ref_companyID = Company_ID ";

	//function in 6 POPULAR COMPANIES
	public function get_popular(){
		$result = 0;

		$text = "SELECT Company_ID , Company_title, Company_img, Company_motto, 
			(SELECT COUNT(Atja_ID) FROM AT_Job_Application WHERE Atja_ref_jobsID = Jobs_ID AND Jobs_ref_companyID = Company_ID) AS num_applicant,
			(SELECT COUNT(Hits_ID) FROM T_Hits WHERE Hits_ref_companyID = Company_ID) AS view,
			count(Jobs_ID) AS no_jobs FROM $this->table 
			$this->joinJob $this->joinHits $this->joinApply
			WHERE $this->publish AND Company_img IS NOT NULL GROUP BY Company_ID 
			ORDER BY num_applicant DESC, view DESC LIMIT 0,6";
		//ORDER BY no_jobs DESC
		$query = mysql_query($text);
		if(mysql_num_rows($query) >= 1){
			$result = array();
			while($row = mysql_fetch_assoc($query)){
				$result[] = $row;
			}
		}
		//$result = $text;
		return $result;
	}

	//FUNCTION TO GET AMOUNT OF ALL PUBLISHED Disease IN DATABASE
	public function get_total($letter = null, $type){
		$result = 0;
		$cond = '';
		if($letter != null){
			$cond .="AND Company_title LIKE '$letter%' ";
		}
		
		$condType = "";
		if($type == 'publish'){
			$condType .= "Company_publish = 'Publish' ";
		}else if($type == 'notpublish'){
			$condType .= "Company_publish = 'Not Publish' ";
		}

		$text = "SELECT count(Company_ID) AS count FROM $this->table WHERE $condType $cond ";
		$query = mysql_query($text);
		if(mysql_num_rows($query) >= 1){
			$row = mysql_fetch_array($query,MYSQL_ASSOC);
			$result =  $row['count'];
		}
		return $result;
	}

	public function get_total_page($total_data){
		$num_page = ceil($total_data / $this->itemPerPage);
		return $num_page;
	}	

	//FUNCTION TO GET data OF ALL PUBLISHED Company IN DATABASE
	//1 to set publish, 0 to get publish and not published data
	public function get_data_by_page($page=0, $letter=null, $type, $sort_type, $sort_order){
		$data = "There is no result.";
		if($page <= 1 || $page == null){
			$limitBefore = 0;
		}else{
			$limitBefore = ($page-1) * $this->itemPerPage;
		}

		$cond = '';
		if($letter != null){
			$cond .="AND company.Company_title LIKE '$letter%' ";
		}
		
		$condType = "";
		if($type == 'publish'){
			$condType .= "company.Company_publish = 'Publish' ";
		}else if($type == 'notpublish'){
			$condType .= "company.Company_publish = 'Not Publish' ";
		}

		$condSort = "";
		if($sort_type == 'employee' && $sort_order == 'asc'){
			$condSort .= "work_counter ASC";
		}else if($sort_type == 'employee' && $sort_order == 'desc'){
			$condSort .= "work_counter DESC";
		}else if($sort_type == 'jobs' && $sort_order == 'asc'){
			$condSort .= "jobs_counter ASC";
		}else if($sort_type == 'jobs' && $sort_order == 'desc'){
			$condSort .= "jobs_counter DESC";
		}else if($sort_type == 'other' && $sort_order == 'asc'){
			$condSort .= "jobs_counter ASC, work_counter ASC";
		}else if($sort_type == 'other' && $sort_order == 'desc'){
			$condSort .= "jobs_counter DESC, work_counter DESC";
		}
 		
 		$text = "SELECT company.Company_ID AS Company_ID, company.Company_title AS Company_title, ind.Industry_ID AS Industry_ID, 
 			ind.Industry_title AS Industry_title, company.Company_content AS Company_content, company.Company_img AS Company_img, 
 			company.Company_img_thmb AS Company_img_thmb, company.Company_publish AS Company_publish,
 			(SELECT COUNT(atework.ATwork_ID) FROM AT_Work atework WHERE atework.ATwork_ref_companyID = company.Company_ID) AS work_counter, 
 			(SELECT COUNT(jobs.Jobs_ID) FROM T_Jobs jobs WHERE jobs.Jobs_ref_companyID = company.Company_ID) AS jobs_counter 
 			FROM $this->table company LEFT JOIN T_Industry ind ON company.Company_ref_industryID = ind.Industry_ID WHERE $condType $cond 
 			GROUP BY company.Company_ID ORDER BY $condSort, company.Company_title ASC LIMIT $limitBefore, $this->itemPerPage";		
 		$query = mysql_query($text);
		if(mysql_num_rows($query) >= 1){
			$data = array();
			while($row = mysql_fetch_array($query,MYSQL_ASSOC)){
				$data[]=$row;
			}
		}
		//$data = $text;
		return $data;
	}

	public function get_index(){
		$result = 0;

		$text = "SELECT Company_ID, Company_title, Company_img, Company_motto, Company_img, count(ATwork_ID) 
			as num_employee FROM T_Company LEFT JOIN AT_Work ON ATwork_ref_companyID = Company_ID 
			GROUP BY Company_ID ORDER BY Company_title ASC";
		$query = mysql_query($text);
		if(mysql_num_rows($query) >= 1){
			$result = array();
			while($row = mysql_fetch_assoc($query)){
				$result[] = $row;
			}
		}
		return $result;
	}

	public function get_data_edit($id){
		$result = 0;

		$text = "SELECT * FROM $this->table WHERE Company_ID = '$id'";
		$query = mysql_query($text);
		if(mysql_num_rows($query) >= 1){
			$result = array();
			while($row = mysql_fetch_assoc($query)){
				$result[] = $row;
			}
		}
		return $result;
	}

	public function insert_data($industry_id, $city_id, $country_id, $title, $motto, $content, $address, $email, $phone, $img, $img_thmb, $admin_id, $publish){
		$result = 0;

		$text = "INSERT INTO $this->table (Company_ref_industryID, Company_ref_cityID, Company_ref_countryID, Company_title, Company_motto, Company_content, Company_address, Company_email, Company_phone, Company_img, Company_img_thmb, Company_create_adminID, Company_publish, Company_create_date) VALUES ('$industry_id', '$city_id', '$country_id', '$title', '$motto', '$content', '$address', '$email', '$phone', '$img', '$img_thmb', '$admin_id', '$publish', NOW())";
		$query = mysql_query($text);
		if($query){
			$result = mysql_insert_id(); 
		}
		return $result;
	}

	public function update_data($id, $industry_id, $city_id, $country_id, $title, $motto, $content, $address, $email, $phone, $admin_id, $publish){
		$result = 0;

		$text = "UPDATE $this->table SET Company_ref_industryID = '$industry_id', Company_ref_cityID = '$city_id', Company_ref_countryID = '$country_id', Company_title = '$title', Company_motto = '$motto', Company_content = '$content', Company_address = '$address', Company_email = '$email', Company_phone = '$phone', Company_modify_adminID = '$admin_id', Company_publish = '$publish' WHERE Company_ID = '$id'";
		$query = mysql_query($text);
		if(mysql_affected_rows() == 1){
			$result = 1;
		}
		return $result;
	}

	public function update_data_image($id, $industry_id, $city_id, $country_id, $title, $motto, $content, $address, $email, $phone, $img, $img_thmb, $admin_id, $publish){
		$result = 0;

		$this->remove_image($id); //remove image before

		$text = "UPDATE $this->table SET Company_ref_industryID = '$industry_id', Company_ref_cityID = '$city_id', Company_ref_countryID = '$country_id', Company_title = '$title', Company_motto = '$motto', Company_content = '$content', Company_address = '$address', Company_email = '$email', Company_phone = '$phone', Company_img = '$img', Company_img_thmb = '$img_thmb', Company_modify_adminID = '$admin_id', Company_publish = '$publish' WHERE Company_ID = '$id'";
		$query = mysql_query($text);
		if(mysql_affected_rows() == 1){
			$result = 1;
		}
		return $result;
	}

	public function remove_image($id){
		$result = 0;
		$flag_img = 0;
		$flag_img_thmb = 0;
		$text = "SELECT Company_ID, Company_img, Company_img_thmb FROM $this->table WHERE Company_ID = '$id' ";
		$query = mysql_query($text);
		if(mysql_num_rows($query) == 1){
			$row = mysql_fetch_assoc($query);
			$deleteImg = $_SERVER['DOCUMENT_ROOT']."/".$row['Company_img'];
            if (file_exists($deleteImg)) {
                unlink($deleteImg);
                $flag_img = 1;
            }
            $deleteImgThmb = $_SERVER['DOCUMENT_ROOT']."/".$row['Company_img_thmb'];
            if (file_exists($deleteImgThmb)) {
                unlink($deleteImgThmb);
                $flag_img_thmb = 1;
            }

            if($flag_img == 1 && $flag_img_thmb ==1){
            	$result = 1;
            }
		}
		return $result;
	}

	public function delete_data($id){
		$result = 0;

		$this->remove_image($id); //remove image before

		$text = "DELETE FROM $this->table WHERE Company_ID = '$id'";
		$query = mysql_query($text);
		if(mysql_affected_rows() == 1){
			$result = 1;
		}
		return $result;
	}

	public function publish_data($id, $publish){
		$result = 0;

		$text = "UPDATE $this->table SET Company_publish = '$publish' WHERE Company_ID = '$id'";
		$query = mysql_query($text);
		if(mysql_affected_rows() == 1){
			$result = 1;
		}
		return $result;
	}

	public function unpublish_data($id){
		$result = 0;
		$not_publish = "Not Publish";

		$text = "UPDATE $this->table SET Company_publish = '$not_publish'  WHERE Company_ID = '$id'";
		$query = mysql_query($text);
		if(mysql_affected_rows() == 1){
			$result = 1;
		}
		return $result;
	}

	//for api check company
	public function check_company($company, $city, $country){//return 0 if not exist
		$data = 0;

		$text = "SELECT Company_ID FROM $this->table $this->joinCity $this->joinCountry 
			WHERE Company_title = '$company' AND City_title = '$city' AND Country_title = '$country' 
			ORDER BY Company_ID ASC LIMIT 0,1";
		$query = mysql_query($text);
		if(mysql_num_rows($query) == 1){
			$row = mysql_fetch_array($query, MYSQL_ASSOC);
			$data = $row['Company_ID'];
		}
		//$data = $text;
		return $data;
	}//END CHECK-EXIST

	//for api get list
	public function get_list($title){
		$result = 0;

		$text = "SELECT Company_id, Company_title, Company_img, City_title, Country_title,
			IF(Company_title LIKE '$title%', 20, IF(Company_title LIKE '%$title%', 15, 
				IF(Company_title LIKE '%$title', 10, 0))) AS weight
			FROM $this->table $this->joinCity $this->joinCountry 
			WHERE Company_publish = 'Publish' AND Company_title LIKE '$title%' OR 
			Company_title LIKE '%$title' OR Company_title LIKE '%$title%' 
			ORDER BY weight DESC, Company_title ASC";
		$query = mysql_query($text);
		if(mysql_num_rows($query) >= 1){
			$result = array();
			while($row = mysql_fetch_assoc($query)){
				$result[] = $row;
			}
		}
		//$result = $text;
		return $result;
	}
}

?>