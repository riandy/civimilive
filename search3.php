<?php 
include("packages/require.php");
include("controller/controller_search.php");
include("controller/controller_global.php");
$curpage='search';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title><?=$global['title-job-result'];?></title>
    <meta name="keywords" content="<?=$seo['keyword-job-result'];?>">
    <meta name="description" content="<?=$seo['desc-job-result'];?>">
    <?php include("packages/head.php");?>
</head>
<body>
    <div id="all">
        <!-- start top nav -->

        <?php include("section-top-nav.php");?>
        <!-- end top nav -->

        <div id="search-header-section">
            <div class="up6 visible-xs"></div>
            <div class="search-header-layer">
                <form action="<?=$path['job-search'];?>?action=search" method="GET">
                    <div class="row hide">
                        <div class="col-xs-12 col-hsearch">
                            <input id="input-qsearch" type="text" name="q" class="form-control input-nav-search" placeholder="keyword" value="<?php echo $O_keyword;?>">
                        </div>
                        <div class="col-xs-12 col-hsearch">
                            <button id="submit-search" type="submit" class="btn btn-info btn-nav-search">Search</button>
                        </div>
                    </div>
                    <div class="row hidden-xs">
                        <div class="col-xs-12 col-hsearch">
                            <div class="hsearch-title">Location <a id="clear-location" href="javascript:;" class="clear-select"><i class="glyphicon glyphicon-remove"></i></a></div>
                            <select id="qlocation" type="text" class="js-example-basic-single form-control hsearch-input tags-location" name="location">
                               <option value="">All Location</option>
                               <?php foreach($data_citys as $data_city){ ?>
                               <option <?php if($O_location == $data_city['City_ID']){ echo "selected=selected"; }?> value="<?php echo $data_city['City_ID'];?>"><?php echo $data_city['City_title'];?></option>
                               <?php } ?>
                           </select>
                       </div>
                       <!-- <div class="col-xs-12 col-hsearch">
                        <div class="hsearch-title">Expertise <a id="clear-expertise" href="javascript:;" class="clear-select"><i class="glyphicon glyphicon-remove"></i></a></div>
                        <select id="qexpertise" type="text" class="js-example-basic-single form-control hsearch-input tags-expertise" name="expertise">
                            <option value="">All Expertise</option>
                            <?php foreach($data_levels as $data_level){ ?>
                            <option <?php if($O_expertise == $data_level['Level_ID']){ echo "selected=selected"; }?> value="<?php echo $data_level['Level_ID'];?>"><?php echo $data_level['Level_title'];?></option>
                            <?php } ?>
                        </select>
                    </div> -->
                    <div class="col-xs-12 col-hsearch">
                        <div class="hsearch-title">Job Function <a id="clear-function" href="javascript:;" class="clear-select"><i class="glyphicon glyphicon-remove"></i></a></div>
                        <select id="qfunction" type="text" class="js-example-basic-single form-control hsearch-input tags-function" name="function">
                            <option value="">All Function</option>
                            <?php foreach($data_fields as $data_field){ ?>
                            <option <?php if($O_function == $data_field['Field_ID']){ echo "selected=selected"; }?> value="<?php echo $data_field['Field_ID'];?>"><?php echo $data_field['Field_parent']." - ".$data_field['Field_title'];?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="col-xs-12 col-hsearch">
                        <div class="hsearch-title">Industry <a id="clear-industry" href="javascript:;" class="clear-select"><i class="glyphicon glyphicon-remove"></i></a></div>
                        <select id="qindustry" type="text" class="js-example-basic-single form-control hsearch-input tags-industry" name="industry">
                            <option value="">All Industry</option>
                            <?php foreach($data_industrys as $data_industry){ ?>
                            <option <?php if($O_industry == $data_industry['Industry_ID']){ echo "selected=selected"; }?> value="<?php echo $data_industry['Industry_ID'];?>"><?php echo $data_industry['Industry_title'];?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="col-xs-12 col-hsearch">
                        <div class="hsearch-title">Experience <a id="clear-experience" href="javascript:;" class="clear-select"><i class="glyphicon glyphicon-remove"></i></a></div>
                        <select id="qexperience" type="text" class="js-example-basic-single form-control hsearch-input tags-experience" name="experience">
                            <option value="">All Experience</option>
                            <option <?php if ($O_exp == "1"){echo "selected=selected";}?> value="1">1 years</option>
                            <option <?php if ($O_exp == "2"){echo "selected=selected";}?> value="2">2 years</option>
                            <option <?php if ($O_exp == "3"){echo "selected=selected";}?> value="3">3 years</option>
                            <option <?php if ($O_exp == "4"){echo "selected=selected";}?> value="4">4 years</option>
                            <option <?php if ($O_exp == "5"){echo "selected=selected";}?> value="5">5 years</option>
                        </select>
                    </div>
                </div>
            </form>
            <div class="row hidden-xs">
                <div class="col-sm-9 col-xs-12">
                        <!-- <div class="recent-list">
                            <span class="hrecent-list">Recent Searches</span>
                            <?php if(is_array($data_recents)){
                                foreach($data_recents AS $data_recent){?>
                                <a href="<?=$path['job-search'];?>?q=<?php echo $data_recent['Qj_keyword'];?>"><?php echo $data_recent['Qj_keyword'];?></a>
                            <?php }};?>
                        </div> -->
                    </div>
                    <div class="col-sm-3 col-xs-12">
                        <!-- <div class="search-options">
                            <span class="drop-setting">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">More Options <span class="caret"></span></a>
                                <ul class="dropdown-menu dropdown-setting" role="menu">
                                    <li><a href="#">Setting</a></li>
                                </ul>
                            </span>
                        </div> -->
                    </div>
                </div>
                <div class="row hide">
                    <div class="col-xs-12">
                        <div class="mobile-btn-content down1">
                            <a href="#" class="btn-mobile-search" onclick="searchSlideTag();">
                                <?php if($O_keyword != ""){ ?>
                                <div class="input-mobile-search" style="font-size:13px;color:#333;"><?php echo $O_keyword; ?></div>
                                <?php } else { ?>
                                <div class="input-mobile-search">search for job title, skills, or keywords</div>
                                <?php } ?>
                                <div class="icon-search"><span class="glyphicon glyphicon-search"></span></div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="pad1"></div>
        </div><!--  end header section -->

        <!--<div id="result"></div>!-->       
        <div id="search-section">
            <div class="row">
                <div class="col-xs-12">
                    <?php 
                    if(is_array($data_results)){
                        ?>
                        <div class="search-result">
                            Showing <strong><?php echo $total_result;?> jobs</strong> 
                            <?php if($O_company != ""){echo "for <strong>\"".$data_results[0]['Company_title']."\"</strong>";}?> 
                            <?php if($O_keyword != "" || $O_location != "" || $O_expertise != "" || $O_function != "" || $O_industry != "" || $O_exp != ""){echo "for";}?> 
                            <?php if($O_keyword != ""){echo "<strong>\"".$O_keyword."\"</strong>";}?>
                            <?php if($O_keyword != "" && $O_location != ""){echo "in";}?>
                            <?php if($O_location != ""){echo "<strong>\"".$data_results[0]['City_title']."\"</strong>";}?>
                            <?php if(($O_location != "" && $O_expertise != "" && $O_function != "" && $O_industry != "")
                            OR ($O_expertise != "" || $O_function != "" || $O_industry != "" || $O_exp != "")){echo "with";}?> 
                            <?php if($O_expertise != ""){echo "<strong>\"".$data_results[0]['Level_title']."\"</strong> job expertise";}?> 
                            <?php if($O_expertise != "" && $O_function != ""){echo ",";}?>
                            <?php if($O_function != ""){echo "<strong>\"".$data_results[0]['Field_title']."\"</strong> job function";}?> 
                            <!-- for industry !-->
                            <?php if($O_expertise != "" && $O_function != "" && $O_industry != ""){echo ",";}?>
                            <?php if($O_keyword != "" && $O_location != "" && $O_expertise == "" && $O_function != "" && $O_industry != "" && $O_exp == ""){echo ",";}?>
                            <?php if($O_industry != ""){echo "<strong>\"".$data_results[0]['Industry_title']."\"</strong> industry";}?>                            
                            <!-- for experience !-->
                            <?php if(($O_keyword == "" && $O_location == "" && $O_expertise == "" && $O_function == "" && $O_industry == "" && $O_exp != "")
                                OR ($O_keyword != "" || $O_location != "" || $O_expertise != "" || $O_function != "" || $O_industry != "")){echo "";}
                                else if($O_keyword != "" || $O_location != "" || $O_expertise != "" || $O_function != "" || $O_industry != "" || $O_exp != ""){echo ",";}?> 
                                <?php if($O_exp != ""){echo "<strong>\"".$O_exp."\" years </strong> experience";}?>
                            </div>
                            <?php 
                        }else{?>
                        <div class="search-result">
                            <strong><?php if($O_keyword != "" && $O_location == "" && $O_expertise == "" && $O_function == "" && $O_industry == "" && $O_exp == ""){
                                echo "No search for a \"".$O_keyword."\"";}else{echo "No search for a jobs";}
                                ?></strong>
                            </div>
                            <?php };?>
                            <div class="drop-recent">
                                <select id="relevance" name="sort" class="text" onchange="get_bysort(1);">
                                 <option value="weight">Relevance</option>
                                 <option value="recent">Recently Added</option>
                                 <option value="mostview">Most Viewed Jobs</option>
                                 <option value="mostpopular">Most Popular Jobs</option>
                                 <option value="leastview" >Least Viewed Jobs</option>
                                 <option value="leastpopular">Least Popular Jobs</option>
                             </select>
                         </div>
                     </div>
                 </div>
                 <div class="row">
                    <div class="col-sm-3 col-xs-12">
                        <div class="job-slist">
                            <div id="job-list"></div>
                            <div id="sbox-loading" class="text-center"></div>
                            <button id="load-latest" class="btn btn-load-more"><i class='glyphicon glyphicon-repeat'></i> Load More</button>
                        </div>      
                    </div>
                    <div class="col-sm-7 hidden-xs">
                        <div class="search-content">
                            <div class="row">
                                <div class="col-xs-12">   
                                    <div id="s-content">
                                        <!-- SHOW KEYWORD !-->
                                        <?php if($O_keyword != ''){?>
                                        <div class="job-stitle"><?php echo strtoupper($O_keyword) ?></div>
                                        <?php if($J_content != ""){ echo "<div align='justify'>$J_content</div>";}?>
                                        <?php }?>

                                        <!-- SHOW COMPANY CONTENT !-->
                                        <?php if(is_array($data_companys)){
                                            foreach ($data_companys as $data_company) {?>
                                            <div class="job-stitle"><?php echo strtoupper($data_company['Company_title']);?></div>
                                            <div align='justify'><?php echo correctDisplay($data_company['Company_content']);?></div>
                                            <?php }}?>

                                            <!-- SHOW FIELD CONTENT !-->
                                            <?php if(is_array($data_field_contents)){
                                                foreach ($data_field_contents as $data_field_content) {?>
                                                <div class="job-stitle"><?php echo strtoupper($data_field_content['Field_title']);?></div>
                                                <div align='justify'><?php echo correctDisplay($data_field_content['Field_content']);?></div>
                                                <?php }}?>

                                            </div>
                                            <div id="s-loading"></div>
                                        </div>

                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <input type="hidden" id="openjobs">
                                            <input type="hidden" id="opencompany">
                                            <a href="#" class="banner-search"><img src="<?=$global['absolute-url'];?>img/stanford banner.jpg" alt="banner"></a>  
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-2 hidden-xs">
                                <div id="related-job-col" class="row hide job-sbox line13 hide">
                                    <div class="col-xs-12 no-padding">  
                                        <div class="job-related-header">
                                            Related Jobs
                                        </div>
                                        <div id="related-job">
                                        </div>
                                    </div>
                                </div>
                                <div id="related-company-col" class="row hide job-sbox line13 hide">
                                    <div class="col-xs-12 no-padding">  
                                        <div class="job-related-header">
                                            Other Jobs by <span id="company-side"></span>
                                        </div>
                                        <div id="related-company">
                                        </div>
                                    </div>
                                </div>
                                <div id="recent-job-col" class="row hide job-sbox line13 hide">
                                    <div class="col-xs-12 no-padding">  
                                        <div class="job-related-header">
                                            Recently Viewed Jobs
                                        </div>
                                        <div id="recent-job">
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <!-- start footer section -->
                    <?php include("section-footer.php");?>
                    <!-- end footer section -->
                </div><!--  end all div -->
                <!-- search modal -->
                <div id="search-modal-tag">
                    <div class="ss-header">
                        <div class="ss-htext">Search</div>
                        <a href="javascript:;" class="ss-close" onclick="searchSlideTag();"><span class="glyphicon glyphicon-remove"></span></a>
                    </div>
                    <form action="<?=$path['job-search'];?>?action=search" method="GET">
                        <div class="ss-layer">
                            <div class="row">
                                <div class="col-xs-12 col-hsearch">
                                    <div class="hsearch-title">Keyword</div>
                                    <input type="text" name="q" class="form-control hsearch-input" value="<?php echo $O_keyword;?>" placeholder="keyword">
                                </div>
                                <div class="col-xs-12 col-hsearch">
                                    <div class="hsearch-title">Location <a id="mclear-location" href="javascript:;" class="clear-select"><i class="glyphicon glyphicon-remove"></i></a></div>
                                    <select id="mqlocation" type="text" class="js-example-basic-single form-control hsearch-input tags-location" name="location">
                                        <option value="">All Location</option>
                                        <?php foreach($data_citys as $data_city){ ?>
                                        <option <?php if($O_location == $data_city['City_ID']){ echo "selected=selected"; }?> value="<?php echo $data_city['City_ID'];?>"><?php echo $data_city['City_title'];?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <!-- <div class="col-xs-12 col-hsearch">
                                    <div class="hsearch-title">Expertise <a id="mclear-expertise" href="javascript:;" class="clear-select"><i class="glyphicon glyphicon-remove"></i></a></div>
                                    <select id="mqexpertise" type="text" class="js-example-basic-single form-control hsearch-input tags-expertise" name="expertise">
                                        <option value="">All Expertise</option>
                                        <?php foreach($data_levels as $data_level){ ?>
                                        <option <?php if($O_expertise == $data_level['Level_ID']){ echo "selected=selected"; }?> value="<?php echo $data_level['Level_ID'];?>"><?php echo $data_level['Level_title'];?></option>
                                        <?php } ?>
                                    </select>
                                </div> -->
                                <div class="col-xs-12 col-hsearch">
                                    <div class="hsearch-title">Job Function <a id="mclear-function" href="javascript:;" class="clear-select"><i class="glyphicon glyphicon-remove"></i></a></div>
                                    <select id="mqfunction" type="text" class="js-example-basic-single form-control hsearch-input tags-function" name="function">
                                        <option value="">All Function</option>
                                        <?php foreach($data_fields as $data_field){ ?>
                                        <option <?php if($O_function == $data_field['Field_ID']){ echo "selected=selected"; }?> value="<?php echo $data_field['Field_ID'];?>"><?php echo $data_field['Field_parent']." - ".$data_field['Field_title'];?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="col-xs-12 col-hsearch">
                                    <div class="hsearch-title">Industry <a id="mclear-industry" href="javascript:;" class="clear-select"><i class="glyphicon glyphicon-remove"></i></a></div>
                                    <select id="mqindustry" type="text" class="js-example-basic-single form-control hsearch-input tags-industry" name="industry">
                                        <option value="">All Industry</option>
                                        <?php foreach($data_industrys as $data_industry){ ?>
                                        <option <?php if($O_industry == $data_industry['Industry_ID']){ echo "selected=selected"; }?> value="<?php echo $data_industry['Industry_ID'];?>"><?php echo $data_industry['Industry_title'];?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="col-xs-12 col-hsearch">
                                    <div class="hsearch-title">Experience <a id="mclear-experience" href="javascript:;" class="clear-select"><i class="glyphicon glyphicon-remove"></i></a></div>
                                    <select id="mqexperience" type="text" class="js-example-basic-single form-control hsearch-input tags-experience" name="experience">
                                        <option value="">All Experience</option>
                                        <option <?php if ($O_exp == "1"){echo "selected=selected";}?> value="1">1 years</option>
                                        <option <?php if ($O_exp == "2"){echo "selected=selected";}?> value="2">2 years</option>
                                        <option <?php if ($O_exp == "3"){echo "selected=selected";}?> value="3">3 years</option>
                                        <option <?php if ($O_exp == "4"){echo "selected=selected";}?> value="4">4 years</option>
                                        <option <?php if ($O_exp == "5"){echo "selected=selected";}?> value="5">5 years</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row up2">
                                <div class="col-xs-12">
                                    <button name="submit" type="submit" class="btn btn-block btn-info btn-search" style="width:100%;">Search</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div><!--  end search modal -->

                <!-- modal apply career -->
                <div class="modal fade" id="career-apply" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header modal-header-footer">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h1 class="modal-title-footer">Apply Job in Civimi</h1>
                            </div>
                            <div class="modal-body modal-body-footer">
                                <div class="row up1">
                                    <div class="col-xs-12 pad0">
                                        <form name="applyCareer" action="<?=$global['client-page'];?>job_detail.php?action=apply" method="post" enctype="multipart/form-data">
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-3">
                                                    <div class="career-input-header">Job *</div>
                                                </div>
                                                <div class="col-xs-12 col-sm-9">
                                                    <!--<input id="job-title-modal" type="text" name="job" class="form-control" placeholder="choosen job" value="" disabled readonly>!-->
                                                    <div id="job-title"></div>
                                                </div>
                                            </div>
                                            <div class="row up1">
                                                <div class="col-xs-12 col-sm-3">
                                                    <div class="career-input-header">Email *</div>
                                                </div>
                                                <div class="col-xs-12 col-sm-9">
                                                    <input type="email" name="email" class="form-control" placeholder="email address" required>
                                                </div>
                                            </div>
                                            <div class="row up1">
                                                <div class="col-xs-12 col-sm-3">
                                                    <div class="career-input-header">Full Name *</div>
                                                </div>
                                                <div class="col-xs-12 col-sm-9">
                                                    <input type="text" name="name" class="form-control" placeholder="full name" required>
                                                </div>
                                            </div>
                                            <div class="row up1">
                                                <div class="col-xs-12 col-sm-3">
                                                    <div class="career-input-header">Phone Number *</div>
                                                </div>
                                                <div class="col-xs-12 col-sm-9">
                                                    <input type="text" name="phone" class="form-control" placeholder="phone number" required>
                                                </div>
                                            </div>
                                            <div class="row up1">
                                                <div class="col-xs-12 col-sm-3">
                                                    <div class="career-input-header">CV File *</div>
                                                </div>
                                                <div class="col-xs-12 col-sm-9">
                                                    <input type="file" name="cv" class="form-control" required>
                                                </div>
                                            </div>
                                            <div class="row up1">
                                                <div class="col-xs-12 col-sm-3">
                                                    <div class="career-input-header">Message</div>
                                                </div>
                                                <div class="col-xs-12 col-sm-9">
                                                    <textarea class="form-control" rows="6" name="message"></textarea>
                                                </div>
                                            </div>
                                            <div class="row up1">
                                                <div class="col-xs-12 col-sm-3"></div>
                                                <div class="col-xs-12 col-sm-9">
                                                    <img src="<?php echo $global['absolute-url-captcha'];?>" alt="captcha" >
                                                    <input type="text" name="captcha_text" class="form-control up05" placeholder="input captcha" required>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-12">
                                        <!--<input type="hidden" name="job_id" class="form-control" value="">
                                        <input type="hidden" name="job_title" class="form-control" value="">
                                        <input type="hidden" name="company_title" class="form-control" value="">!-->
                                        <div id="job-apply"></div>
                                        <input type="hidden" name="user_id" class="form-control" value="<?=$_SESSION['userData']['id'];?>">
                                        <button class="btn btn-info btn-apply up2" type="submit" style="width:100%;">Apply Now</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- popup alert -->
    <div id="alert-popup" class="popup">
      <div id="popup-text" class="popup-text">Your favorite admin cibitung</div>
      <a id="popup-close" href="javascript:;" class="popup-close"><i class="glyphicon glyphicon-remove"></i></a>
  </div>
  <script type="text/javascript">
  function popup(message){
    $("#alert-popup").fadeIn();
    $("#popup-text").text(message);
    setTimeout(function(){
        $("#alert-popup").fadeOut();
    }, 3000);
}
$("#popup-close").click(function(){
    $("#alert-popup").fadeOut();
})
</script>
<!--end  popup alert -->
<script src="<?php echo $global['absolute-url'];?>js/function.js"></script>
<script type="text/javascript">
$('.hsearch-input').on('change', function () {
    checkSelect();
});
$(document).ready(function(){
    checkSelect();
});
function checkSelect(){
    var vlocation = $("#qlocation").val();
    var vexpertise = $("#qexpertise").val();
    var vfunction = $("#qfunction").val();
    var vindustry = $("#qindustry").val();
    var vexperience = $("#qexperience").val();

    var mvlocation = $("#mqlocation").val();
    var mvexpertise = $("#mqexpertise").val();
    var mvfunction = $("#mqfunction").val();
    var mvindustry = $("#mqindustry").val();
    var mvexperience = $("#mqexperience").val();
    
    var tlocation = $('#qlocation option:selected').text();
    var texpertise = $('#qexpertise option:selected').text();
    var tfunction = $('#qfunction option:selected').text();
    var tindustry = $('#qindustry option:selected').text();
    var texperience = $('#qexperience option:selected').text();

    if(vlocation != ""){ 
        $('#slocation').attr('data-value',vlocation);
        $('#slocation').text(tlocation);
        $('#sugest-1').show(); 
    } else { 
        $('#sugest-1').hide();
    }
    if(vexpertise != ""){ 
        $('#sexpertise').attr('data-value',vexpertise);
        $('#sexpertise').text(texpertise);
        $('#sugest-2').show(); 
    } else { 
        $('#sugest-2').hide();
    }
    if(vfunction != ""){ 
        $('#sfunction').attr('data-value',vfunction);
        $('#sfunction').text(tfunction);
        $('#sugest-3').show(); 
    } else { 
        $('#sugest-3').hide();
    }
    if(vindustry != ""){ 
        $('#sindustry').attr('data-value',vindustry);
        $('#sindustry').text(tindustry);
        $('#sugest-4').show(); 
    } else { 
        $('#sugest-4').hide();
    }
    if(vexperience != ""){ 
        $('#sexperience').attr('data-value',vexperience);
        $('#sexperience').text(texperience);
        $('#sugest-5').show(); 
    } else { 
        $('#sugest-5').hide();
    }

    if(vlocation != ""){ $("#clear-location").show(); } else { $("#clear-location").hide();}
    if(vexpertise != ""){ $("#clear-expertise").show(); } else { $("#clear-expertise").hide();}
    if(vfunction != ""){ $("#clear-function").show(); } else { $("#clear-function").hide();}
    if(vindustry != ""){ $("#clear-industry").show(); } else { $("#clear-industry").hide();}
    if(vexperience != ""){ $("#clear-experience").show(); } else { $("#clear-experience").hide();}

    if(mvlocation != ""){ $("#mclear-location").show(); } else { $("#mclear-location").hide();}
    if(mvexpertise != ""){ $("#mclear-expertise").show(); } else { $("#mclear-expertise").hide();}
    if(mvfunction != ""){ $("#mclear-function").show(); } else { $("#mclear-function").hide();}
    if(mvindustry != ""){ $("#mclear-industry").show(); } else { $("#mclear-industry").hide();}
    if(mvexperience != ""){ $("#mclear-experience").show(); } else { $("#mclear-experience").hide();}
}
$("#clear-location").on("click", function () {$("#qlocation").val("").trigger("change");$("#clear-location").hide();});
$("#clear-expertise").on("click", function () {$("#qexpertise").val("").trigger("change");$("#clear-expertise").hide();});
$("#clear-function").on("click", function () {$("#qfunction").val("").trigger("change");$("#clear-function").hide();});
$("#clear-industry").on("click", function () {$("#qindustry").val("").trigger("change");$("#clear-industry").hide();});
$("#clear-experience").on("click", function () {$("#qexperience").val("").trigger("change");$("#clear-experience").hide();});

$("#mclear-location").on("click", function () {$("#mqlocation").val("").trigger("change");$("#mclear-location").hide();});
$("#mclear-expertise").on("click", function () {$("#mqexpertise").val("").trigger("change");$("#mclear-expertise").hide();});
$("#mclear-function").on("click", function () {$("#mqfunction").val("").trigger("change");$("#mclear-function").hide();});
$("#mclear-industry").on("click", function () {$("#mqindustry").val("").trigger("change");$("#mclear-industry").hide();});
$("#mclear-experience").on("click", function () {$("#mqexperience").val("").trigger("change");$("#mclear-experience").hide();});

<?php if(isset($_SESSION['userData']['id']) && isset($_SESSION['userData']['auth_code'])){?>
    get_recent_jobs();
    function get_recent_jobs(){
        var url = "<?=$api['job-recent'];?>&user_id=<?php echo $_SESSION['userData']['id'];?>&auth_code=<?php echo $_SESSION['userData']['auth_code'];?>";
        $.ajax({url: url,success:function(result){
            $('#recent-job').html(replace_recent(result.data));
        }});  
    }
    <?php } ?>

    function replace_recent(datas){
        var resultHTML='';resultNext = "";
        var total = 0;previous = "";user_name = "";link="";
        var d = new Date();strip_string ="";
        var current = d.getTime();
        // console.log(current);
        if(datas != null){
          $('#recent-job-col').removeClass('hide');
          var obj = datas;    

          var ctr = 0;
          var cur_recent_id = 0;
          for(var i=1;i < obj.length;i++){

            if(obj[i].Jobs_ID != '' && obj[i].Jobs_title != '' && cur_recent_id != obj[i].Jobs_ID){

              var jobTitle = obj[i].Jobs_title;
              var regex = /\\/g;
              var replaceTitle = jobTitle.replace(regex, "").replace(/&quot;/g, "");
              var lowercase = replaceTitle.toLowerCase();
              var encodeTitle = lowercase.replace(/\s+/g, '-');

              link = "<?=$path['job-detail'];?>"+encodeTitle+"_"+obj[i].Jobs_ID+".html";

              resultHTML += "<a href='"+link+"' class='job-related-box'>";
              resultHTML += "<div class='job-related-title'>"+simpleDecodeHtml(obj[i].Jobs_title)+"</div>";
              resultHTML += "<div class='job-related-location-time'>";
              resultHTML += "<div class='job-related-location'>"+simpleDecodeHtml(obj[i].Company_title)+"</div>";
              resultHTML += "<div class='job-related-location'>"+obj[i].City_title+", "+obj[i].Country_title+"</div>";
                // resultHTML += "<span class='job-related-time'>"+obj[i].Jobs_ID+"</span>";
                resultHTML += "</div>";
                resultHTML += "</a>";
                console.log(cur_recent_id+" beda "+obj[i].Jobs_ID);
                cur_recent_id = obj[i].Jobs_ID;//assign the displayed recent view into the current pointer
                ctr++;
            }else{
                console.log(cur_recent_id+" sama "+obj[i].Jobs_ID);
            }

            if(ctr == 5){break;}
        }
    } else {
        $('#recent-job-col').addClass('hide');
    }
    return resultHTML;
};

  // start favorite js
    <?php if (isset($_SESSION['userData']['id'])){ ?> //if user login
        function get_fav_job(){
            var url = "<?=$api['job-favorite'];?>&user_id=<?php echo $_SESSION['userData']['id'];?>&table_name=jobs";                            
            $.ajax({url: url,success:function(result){
            // $('#fav').append(replace_fav(result.data));
            var element = $(".job-sbox");
            var total_jobs = element.length;
            // console.log(total_jobs);
            // console.log(result.data[0].ATuf_ref_table_ID);
            for(var m = 0; m < result.data.length; m++){
                $(".job-sbox").each(function(){
                    var job_id = $(this).attr("data-job");
                    if(result.data[m].ATuf_ref_table_ID == job_id){
                        $("#jfav-"+job_id).addClass("bookmark-active");
                        console.log("active - "+job_id+" - "+result.data[m].ATuf_ref_table_ID);
                        // break;
                    }
                })
            }
        }}); 
        }
        <?php } ?>

        function update_fav(table_id){
            <?php if (isset($_SESSION['userData']['id'])){ ?>

                var jobTitle = $('#jfav-'+table_id).data('jfav_title');
                var companyTitle = $('#jfav-'+table_id).data('jfav_company');

                if($("#jfav-"+table_id).hasClass("bookmark-active")){
                    $("#jfav-"+table_id).removeClass("bookmark-active");
                    var message = "Removed "+jobTitle+", "+companyTitle+" from Favourite List!";
                    popup(message);
                }else{
                    $("#jfav-"+table_id).addClass("bookmark-active");
                    var message = "Added "+jobTitle+", "+companyTitle+" to Favourite List!";
                    popup(message);
                }
                var url = "<?=$api['job-update-fav'];?>&user_id=<?=$_SESSION['userData']['id'];?>&table_name=jobs&table_id="+table_id;                            
                $.ajax({url: url,success:function(result){
                    console.log(result.message);
                }});
                <?php } else {  ?>
                    $('#modal-login').modal('show');
                    var message = "'To Bookmark Job, please login first'";
                    popup(message);
                    <?php } ?>
                }
    // end favorite js
    <?php if (isset($_GET['job_id'])){ ?>
        openjob(<?=$_GET['job_id'];?>);
        <?php } ?>
        function openjob(id){
            setTimeout(function() {
                $("#sbox-"+id).toggleClass('sbox-active');
            }, 1500)
            var idOpen = $('#openjobs').val();
            if(idOpen != ''){
                if( idOpen != id ){
                    setTimeout(function() {
                        $("#sbox-"+idOpen).toggleClass('sbox-active');
                    }, 1500)
                    $('#openjobs').val(id);
            // $("#opencompany").val(cid);
        }
    } else {
        $('#openjobs').val(id);
        // $("#opencompany").val(cid);
    }
    $('#s-content').html('');
    $('#job-title').html('');
    $('#job-apply').html('');

    if(window.location.href.indexOf("job_id") > -1){
        var newlinks = "<?php echo $global['absolute-url'];?>search3.php?q=<?=$O_keyword;?>&location=<?=$O_location;?>&expertise=<?=$O_expertise;?>&function=<?=$O_function;?>&industry=<?=$O_industry;?>&experience=<?=$O_exp;?>&job_id="+id;
        window.history.pushState('civimi', 'civimi', newlinks);
    } else { 
        var newlinks = document.location+"&job_id="+id;
        window.history.pushState('civimi', 'civimi', newlinks);
    } 

    $('#s-loading').html("<img src='img/loader.gif' alt='loading'>");
    setTimeout(function() {
        var url = "<?=$api['job-detail'];?>"+id+"/";
        $.ajax({url: url,success:function(result){
            $('#s-loading').html('');
            $('#s-content').append(replace_search(result.data));
            setTimeout(function() {
                get_related_jobs();
                get_related_company();
                <?php if(isset($_SESSION['userData']['id']) && isset($_SESSION['userData']['auth_code'])){?>
                    get_recent_jobs();
                <?php } ?>
            },500)

            $('#job-title').append(replace_jobTitle(result.data));
            $('#job-apply').append(replace_apply(result.data));
            store_view(id);//STORE VIEW to T_HITS
        }});
    }, 1500)
}

function store_view(id){
    var company = $("#sbox-"+id).attr("data-company");
    var industry = $("#sbox-"+id).attr("data-industry");
    var level = $("#sbox-"+id).attr("data-level");
    var field = $("#sbox-"+id).attr("data-field");

    var data = {
        ip: "<?=$ip;?>",
        ip_city: "<?=$J_city;?>",
        ip_country: "<?=$J_country;?>",
        job_id: id,
        company_id: company,
        industry_id: industry,
        level_id: level,
        field_id: field,
        user_id: "<?=$O_userID;?>"
    }

    var url = "<?=$api['job-hits'];?>";
    $.ajax({url: url, data:data,type:"POST",success:function(result){
        console.log(result.message);
    }});

}

function replace_jobTitle(datas){
    var resultHTML='';resultNext = "";
    var total = 0;previous = "";user_name = "";link="";
    var d = new Date();strip_string ="";
    var current = d.getTime();
             // console.log(datas);
             if(datas != null){
              var obj = datas;    
              for(var i=0;i < obj.length;i++){
                if(obj[i].Jobs_ID != '' && obj[i].Jobs_title != ''){
                    resultHTML += "<input id='job-title-modal' type='text' name='job' class='form-control' placeholder='choosen job' value='"+obj[i].Jobs_title+"' disabled readonly>";
                }
            }
        }
        return resultHTML;
    };
    function replace_apply(datas){
        var resultHTML='';resultNext = "";
        var total = 0;previous = "";user_name = "";link="";
        var d = new Date();strip_string ="";
        var current = d.getTime();
             // console.log(datas);
             if(datas != null){
              var obj = datas;    
              for(var i=0;i < obj.length;i++){
                if(obj[i].Jobs_ID != '' && obj[i].Jobs_title != ''){
                    resultHTML += "<input type='hidden' name='job_id' class='form-control' value='"+obj[i].Jobs_ID+"'>";
                    resultHTML += "<input type='hidden' name='job_title' class='form-control' value='"+obj[i].Jobs_title+"'>";
                    resultHTML += "<input type='hidden' name='company_title' class='form-control' value='"+obj[i].Company_title+"'>";                 
                }
            }
        }
        return resultHTML;
    };
    function replace_search(datas){
        var resultHTML='';resultNext = "";
        var total = 0;previous = "";user_name = "";link="";
        var d = new Date();strip_string ="";
        var current = d.getTime();
        console.log(datas);
        if(datas != null){
          var obj = datas;    

          for(var i=0;i < obj.length;i++){
            $("#company-side").text(simpleDecodeHtml(obj[i].Company_title));
            var jobsTitle = obj[i].Jobs_title;
            var regex = /\\/g;
            var replaceTitle = jobsTitle.replace(regex, "").replace(/&quot;/g, "");
            var lowercase = replaceTitle.toLowerCase();
            var encodeTitle = lowercase.replace(/\s+/g, '-');
            link = "<?=$path['job-detail'];?>"+encodeTitle+"_"+obj[i].Jobs_ID+".html";

            if(obj[i].Jobs_ID != '' && obj[i].Jobs_title != ''){
                resultHTML += "<div class='row s-content-title'>"; 
                resultHTML += "<div class='col-xs-12 col-sm-10 no-padding'>";
                resultHTML += "<div id='openjob' class='job-stitle' data-job_id='"+obj[i].Jobs_ID+"'>"+simpleDecodeHtml(obj[i].Jobs_title)+"</div>";  
                // resultHTML += "<div class=''>by <a href='#company-url-here'>"+simpleDecodeHtml(obj[i].Company_title)+"</a></div>";  
                resultHTML += "<div class='list-saddress'><img src='<?php echo $global['absolute-url'];?>img/map.png' alt='map'>"+obj[i].City_title+", "+obj[i].Country_title+"</div>"; 
                resultHTML += "</div>";              
                resultHTML += "<div class='col-sm-2 hidden-xs no-padding'>";   
                if(obj[i].Company_img != ''){
                    resultHTML += "<a href='#company-url-here'><img src='<?=$global['absolute-url-admin'];?>"+obj[i].Company_img+"' class='scontent-img'></a>";
                }
                resultHTML += "</div>";
                resultHTML += "</div>";
                resultHTML += "<div class='row'><div class='col-xs-12'>";
                if(obj[i].Jobs_content != ''){
                    resultHTML += "<div class='job-shead up15'>Job Description</div><div class='job-sdesc'>"+simpleDecodeHtml(obj[i].Jobs_content)+"</div>";
                }
                if(obj[i].Jobs_qualification_info != ''){
                    resultHTML += "<div class='job-shead up15'>Job Qualification</div><div class='job-sdesc'>"+simpleDecodeHtml(obj[i].Jobs_qualification_info)+"</div>";
                }
                if(obj[i].Jobs_apply_info != ''){
                    resultHTML += "<div class='job-shead up15'>How to Apply</div><div class='job-sdesc'>"+simpleDecodeHtml(obj[i].Jobs_apply_info)+"</div>";
                }
                resultHTML += "</div></div>";
                <?php 
                if(isset($_SESSION['userData']['id'])){$data_target = "data-target='#career-apply'";} //if user login
                else{$data_target = "data-target='#modal-login'";} //not login
                ?>
                resultHTML += "<div class='apply-btn-place'><button class='btn btn-info btn-apply xs-100' data-toggle='modal' <?=$data_target;?>>Apply Now</button></div>";
                //resultHTML += "<a href='"+link+"' type='button' class='btn btn-default xs-100' style='float: right;'>View Detail <span class='glyphicon glyphicon-circle-arrow-right' style='margin-left: 3px;'></span></a></div>";
            }
            $("#opencompany").val(obj[i].Jobs_ref_companyID);
        }
    }
    return resultHTML;
};
var count = 1;
get_latests(1);
$('#load-latest').click(function(){
  $('#load-latest').html("");
  $('#load-latest').html("<img src='<?php echo $global['absolute-url'];?>img/loadmores.gif' alt='loading' style='width:20px;''> Loading....");
  setTimeout(function() { 
      get_latests(count); 
  }, 300)
  count++;
});
function get_bysort(count){
    $('#job-list').html("");
    // $('#sbox-loading').html("<img src='img/loader.gif' alt='loading' style='width:150px;height:150px;margin:100px 0;'>");
    var sortby = document.getElementById("relevance").value;
    setTimeout(function() {
        var url = "<?=$api['job-result'];?>"+count+"&q=<?=$O_keyword;?>&company=<?=$O_company;?>&location=<?=$O_location;?>&expertise=<?=$O_expertise;?>&function=<?=$O_function;?>&industry=<?=$O_industry;?>&expertise=<?=$O_exp;?>&sort="+sortby;                            
        $.ajax({url: url,success:function(result){
            // $('#sbox-loading').html("");
            $('#job-list').html(replace_data(result.data));
            <?php if(isset($_SESSION['userData']['id'])){?>
                get_fav_job();
                <?php } ?>
                $('#load-latest').html("<i class='glyphicon glyphicon-repeat'></i> Load More");
                var remainings = result.remaining;
                if(remainings <= 0){
                    $('#load-latest').hide();
                }else {
                    $('#load-latest').show();
                }
            }});  
    }, 1000)
}
function get_latests(count){
    var sortby = document.getElementById("relevance").value;
    // $('#sbox-loading').html("<img src='img/loader.gif' alt='loading' style='width:150px;height:150px;margin:100px 0;'>");
    setTimeout(function() {
        var url = "<?=$api['job-result'];?>"+count+"&q=<?=$O_keyword;?>&company=<?=$O_company;?>&location=<?=$O_location;?>&expertise=<?=$O_expertise;?>&function=<?=$O_function;?>&industry=<?=$O_industry;?>&experience=<?=$O_exp;?>&sort="+sortby;                            
        $.ajax({url: url,success:function(result){
            // $('#sbox-loading').html("");
            $('#job-list').append(replace_data(result.data));
            <?php if(isset($_SESSION['userData']['id'])){?>
                get_fav_job();
                <?php } ?>
                $('#load-latest').html("<i class='glyphicon glyphicon-repeat'></i> Load More");
                var remainings = result.remaining;
                if(remainings <= 0){
                    $('#load-latest').hide();
                }else {
                    $('#load-latest').show();
                }
            }});  
    }, 1000)
}
function replace_data(datas){
    var resultHTML='';resultNext = "";
    var total = 0;previous = "";user_name = "";link="";
    var d = new Date();strip_string ="";
    var current = d.getTime();
        // console.log(current);
        if(datas != null){
          var obj = datas;    

          for(var i=0;i < obj.length;i++){


            if(obj[i].Jobs_ID != '' && obj[i].Jobs_title != ''){
                previous = new Date(obj[i].Jobs_create_date).getTime();
                var jobsTitle = simpleDecodeHtml(obj[i].Jobs_title);
                var regex = /\\/g;
                var replaceTitle = jobsTitle.replace(regex, "").replace(/&quot;/g, "");
                var lowercase = replaceTitle.toLowerCase();
                var encodeTitle = lowercase.replace(/\s+/g, '-');

                link = "<?=$path['job-detail'];?>"+encodeTitle+"_"+obj[i].Jobs_ID+".html";
                resultHTML += "<div class='job-sbox' data-job='"+obj[i].Jobs_ID+"' >";
                resultHTML += "<a id='sbox-"+obj[i].Jobs_ID+"' href='javascript:;' onclick='openjob("+obj[i].Jobs_ID+");' data-company='"+obj[i].Company_ID+"' data-industry='"+obj[i].Industry_ID+"' data-level='"+obj[i].Level_ID+"' data-field='"+obj[i].Field_ID+"' class='job-sbox-link hidden-xs'>";
                resultHTML += "    <div class='list-stitle'>"+jobsTitle+"</div>";
                resultHTML += "    <div class='list-scompany'>"+simpleDecodeHtml(obj[i].Company_title)+"</div>";
                resultHTML += "    <div class='list-saddress'><img src='<?=$global['absolute-url'];?>img/map.png' alt='map'>"+obj[i].City_title+", "+obj[i].Country_title+"</div>";
                resultHTML += "    <div class='row'><div class='list-stime col-sm-9 no-padding'><img src='<?=$global['absolute-url'];?>img/clock.png' alt='map'>"+timeDifference(current,previous)+"</div>";
                resultHTML += "    <div align='right' class='list-stime col-sm-3 no-padding'><span class='glyphicon glyphicon-eye-open'></span> "+obj[i].counter+" views</div></div>";
                resultHTML += "</a>";
                resultHTML += "<a id='sbox-"+obj[i].Jobs_ID+"' href='"+link+"' class='job-sbox-link visible-xs'>";
                resultHTML += "    <div class='list-stitle'>"+jobsTitle+"</div>";
                resultHTML += "    <div class='list-scompany'>"+simpleDecodeHtml(obj[i].Company_title)+"</div>";
                resultHTML += "    <div class='list-saddress'><img src='<?=$global['absolute-url'];?>img/map.png' alt='map'>"+obj[i].City_title+", "+obj[i].Country_title+"</div>";
                resultHTML += "    <div class='row'><div class='list-stime col-sm-9 no-padding'><img src='<?=$global['absolute-url'];?>img/clock.png' alt='map'>"+timeDifference(current,previous)+"</div>";
                resultHTML += "    <div align='right' class='list-stime col-sm-3 no-padding'><span class='glyphicon glyphicon-eye-open'></span> "+obj[i].counter+" views</div></div>";
                resultHTML += "</a>";
                resultHTML += "<a href='javascript:;' id='jfav-"+obj[i].Jobs_ID+"' data-jfav_title='"+jobsTitle+"' data-jfav_company='"+obj[i].Company_title+"' onclick='update_fav("+obj[i].Jobs_ID+")' class='search-bookmark '><span class='glyphicon glyphicon-star'></span></a>";
                resultHTML += "</div>";
            }

        }
    }
    else{
        $('#load-latest').addClass('hidden');
                /*resultHTML += "<div class='job-sbox'>";
                resultHTML += "<div class='list-scompany'>No jobs title</div>";
                resultHTML += "<div class='list-saddress'><img src='<?php echo $global['absolute-url'];?>img/map.png' alt='map'>No city</div>";
                resultHTML += "<div class='list-stime'><img src='<?php echo $global['absolute-url'];?>img/clock.png' alt='map'>No time</div>";
                resultHTML += "<a href='#' class='search-bookmark'><span class='glyphicon glyphicon-star'></span></a>";
                resultHTML += "<div align='right' class='list-stime'><span class='glyphicon glyphicon-eye-open'></span> No view</div>"
                resultHTML += "</div>";*/
            }
            return resultHTML;
        };


        function get_related_jobs(){
            var idOpen = $('#openjobs').val();
            var url = "<?=$api['job-related'];?>&jobs_id="+idOpen;
            $.ajax({url: url,success:function(result){

                $('#related-job').html(replace_related(result.data));
            }});  
        }
        function replace_related(datas){
            var resultHTML='';resultNext = "";
            var total = 0;previous = "";user_name = "";link="";
            var d = new Date();strip_string ="";
            var current = d.getTime();
        // console.log(current);
        if(datas != null){
          $('#related-job-col').removeClass('hide');
          var obj = datas;    

          for(var i=0;i < obj.length;i++){

            if(obj[i].Jobs_ID != '' && obj[i].Jobs_title != ''){

              var jobTitle = obj[i].Jobs_title;
              var regex = /\\/g;
              var replaceTitle = jobTitle.replace(regex, "").replace(/&quot;/g, "");
              var lowercase = replaceTitle.toLowerCase();
              var encodeTitle = lowercase.replace(/\s+/g, '-');

              link = "<?=$path['job-detail'];?>"+encodeTitle+"_"+obj[i].Jobs_ID+".html";

              resultHTML += "<a href='"+link+"' class='job-related-box'>";
              resultHTML += "<div class='job-related-title'>"+simpleDecodeHtml(obj[i].Jobs_title)+"</div>";
              resultHTML += "<div class='job-related-location-time'>";
              resultHTML += "<span class='job-related-location'>"+simpleDecodeHtml(obj[i].Company_title)+"</span>";
                // resultHTML += "<span class='job-related-time'>"+obj[i].Jobs_ID+"</span>";
                resultHTML += "</div>";
                resultHTML += "<div class='job-related-location-time'>";
              resultHTML += "<span class='job-related-location'>"+obj[i].City_title+", "+obj[i].Country_title+"</span>";
                resultHTML += "</div>";
                resultHTML += "</a>";
            }
        }
    } else {
        $('#related-job-col').addClass('hide');
    }
    return resultHTML;
};
function get_related_company(){
    var idOpen = $('#openjobs').val();
    var companyOpen = $('#opencompany').val();
    var url = "<?=$api['job-company-related'];?>&jobs_id="+idOpen+"&company_id="+companyOpen;
    $.ajax({url: url,success:function(result){

        $('#related-company').html(replace_related_company(result.data));
    }});  
}
function replace_related_company(datas){
    var resultHTML='';resultNext = "";
    var total = 0;previous = "";user_name = "";link="";
    var d = new Date();strip_string ="";
    var current = d.getTime();
        // console.log(current);
        if(datas != null){
          $('#related-company-col').removeClass('hide');
          var obj = datas;    

          for(var i=0;i < obj.length;i++){

            if(obj[i].Jobs_ID != '' && obj[i].Jobs_title != ''){

              var jobTitle = obj[i].Jobs_title;
              var regex = /\\/g;
              var replaceTitle = jobTitle.replace(regex, "").replace(/&quot;/g, "");
              var lowercase = replaceTitle.toLowerCase();
              var encodeTitle = lowercase.replace(/\s+/g, '-');

              link = "<?=$path['job-detail'];?>"+encodeTitle+"_"+obj[i].Jobs_ID+".html";

              resultHTML += "<a href='"+link+"' class='job-related-box'>";
              resultHTML += "<div class='job-related-title'>"+simpleDecodeHtml(obj[i].Jobs_title)+"</div>";
              resultHTML += "<div class='job-related-location-time'>";
              resultHTML += "<span class='job-related-location'>"+obj[i].City_title+"</span>";
                // resultHTML += "<span class='job-related-time'>"+obj[i].Jobs_ID+"</span>";
                resultHTML += "</div>";
                resultHTML += "</a>";
            }
        }
    } else {
        $('#related-company-col').addClass('hide');
    }
    return resultHTML;
};
function keywordValue(){
    var key = $('#input-nav-qsearch').val();
    $('#input-qsearch').val(key);
}
$('#input-nav-qsearch').keypress(function(e) {
    if(e.which == 13) {
        jQuery(this).blur();
        jQuery('#submit-search').focus().click();
    }
});
$('.btn-nav-search').keypress(function(e) {
    if(e.which == 13) {
        jQuery(this).blur();
        jQuery('#submit-search').focus().click();
    }
});
function searchSlideTag() {
    $('#search-modal-tag').fadeToggle(300);
};
$(".tags-location").select2({
    // placeholder: "all location"
});
$(".tags-expertise").select2({
    // placeholder: "all expertise"
});
$(".tags-function").select2({
    // placeholder: "all job function"
});
$(".tags-industry").select2({
    // placeholder: "all industry"
});
$(".tags-experience").select2({
    // placeholder: "all experience"
});
</script>
</body>
</html>
