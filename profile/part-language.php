<?php if(is_array($data_langs)){ ?>
<h2 class="title"><span>Language</span></h2>
<!-- language -->
<div class="row">
  <?php $num_lang=1; foreach($data_langs as $data_lang){ ?>
  <div class="language-list hide hm-language col-md-6">
    <h3 class="title-2"><?=$data_lang['Lang_name'];?></h3>
    <ul class="list-unstyled list-progress hide">
      <li>
        <h4>Listening  <?php if($data_lang['ATlang_listen']!="0"){ ?><small class="label label-cvm"><?=replace_rate($data_lang['ATlang_listen']);?></small> <?php } ?></h4>
        <!-- <div class="progress-line">
          <div class="line" data-holdwidth="100%" ></div>
        </div> -->
      </li>
      <li>
        <h4>Speaking  <?php if($data_lang['ATlang_speak']!="0"){ ?><small class="label label-cvm"><?=replace_rate($data_lang['ATlang_speak']);?></small> <?php } ?></h4>
        <!-- <div class="progress-line">
          <div class="line" data-holdwidth="100%" ></div>
        </div> -->
      </li>
      <li>
        <h4>Reading  <?php if($data_lang['ATlang_read']!="0"){ ?><small class="label label-cvm"><?=replace_rate($data_lang['ATlang_read']);?></small> <?php } ?></h4>
        <!-- <div class="progress-line">
          <div class="line" data-holdwidth="100%" ></div>
        </div> -->
      </li>
      <li>
        <h4>Writing  <?php if($data_lang['ATlang_write']!="0"){ ?><small class="label label-cvm"><?=replace_rate($data_lang['ATlang_write']);?></small> <?php } ?></h4>
        <!-- <div class="progress-line">
          <div class="line" data-holdwidth="100%" ></div>
        </div> -->
      </li>
    </ul>
    <hr>
  </div>
  <?php $num_lang++; } ?>
</div><!-- language -->
<a id="more-language" href="javascript:;" class="see-list hide">SEE <span id="language-num"></span> MORE</a>
<?php } ?> 