<section id="aboutme">
          <div class="section-padd bg2" >
            <div class="container-body clearfix">
              &nbsp;
              <!-- <div class="big-qoute">
                <h3>Start where you are. Use what you have. Do what you can. <small>&rarr; <?=$data_user[0]['User_fname']." ".$data_user[0]['User_lname'];?></small></h3>
              </div> -->
            </div>
          </div>
          <div class="section-padd top-bold-border" >
            <div class="container-body clearfix">
              <div class="row">
                <div class="col-md-12">
                  <div class="img-pr">
                    <a href="<?=check_image_url($data_user[0]['User_proPhoto']);?>" class="image-popup" title="This is Me.">
                      <img src="<?php echo check_image_url($data_user[0]['User_proPhoto']);?>" alt="Resume picture of <?=$data_user[0]['User_fname']." ".$data_user[0]['User_lname'];?> in CIVIMI" >
                    </a>
                  </div>
                  <h2 class="title"><span><?=$data_user[0]['User_fname']." ".$data_user[0]['User_lname'];?></span></h2>
                  <?php if($data_user[0]['User_obj'] != ""){ ?>
                    <?=correctText($data_user[0]['User_obj']);?>
                  <?php } ?>
                </div>
              </div>
            </div>
          </div>
          <div class="clearfix br-t" style="display:none;" >
            <div class="col-md-6 col-sm-6 no-padding">
              <div class="desc-mini no-br-l">
                <div class="mid-desc-mini">
                  <h3>Web Designer</h3>
                </div>
              </div>
            </div>
            <div class="col-md-6 col-sm-6 no-padding">
              <div class="desc-mini">
                <div class="mid-desc-mini">
                  <h3>Web Developer</h3>
                </div>
              </div>
            </div>
          </div>
          <div class="bg2 p30 br-b">
            <div class="container-body clearfix">
              <div class="clearfix br-t br-r " >
                <!-- <div class="col-lg-4 col-md-6 col-sm-6 no-padding">
                  <div class="desc-mini">
                    <div class="mid-desc-mini">
                      <h4>Birthdate</h4>
                      <p><?=$data_user[0]['User_DOBday']."/".$data_user[0]['User_DOBmonth']."/".$data_user[0]['User_DOB'];?></p>
                    </div>
                  </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6 no-padding">
                  <div class="desc-mini">
                    <div class="mid-desc-mini">
                      <h4>Skype</h4>
                      <p>-</p>
                    </div>
                  </div>
                </div> -->
                <!-- <div class="col-lg-6 col-md-6 col-sm-6 no-padding">
                  <div class="desc-mini">
                    <div class="mid-desc-mini">
                      <h4>Phone </h4>
                      <?php if ($data_user[0]['User_phone'] != ""){ ?>
                      <p><?=$data_user[0]['User_phone'];?></p>
                      <?php } else { echo "<p>-</p>"; } ?>
                    </div>
                  </div>
                </div> -->
                <!-- <div class="col-lg-6 col-md-6 col-sm-6 no-padding">
                  <div class="desc-mini">
                    <div class="mid-desc-mini">
                      <h4>Email</h4>
                      <?php if ($data_user[0]['User_email'] != ""){ ?>
                      <p><?=$data_user[0]['User_email'];?></p>
                      <?php } else { echo "<p>-</p>"; } ?>
                    </div>
                  </div>
                </div> -->
                <div class="col-lg-6 col-md-6 col-sm-6 no-padding">
                  <div class="desc-mini" >
                    <div class="mid-desc-mini">
                      <h4>Website</h4>
                      <?php if ($data_user[0]['User_website'] != ""){ ?>
                      <p><?=$data_user[0]['User_website'];?></p>
                      <?php } else { echo "<p>-</p>"; } ?>
                    </div>
                  </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 no-padding">
                  <div class="desc-mini">
                    <div class="mid-desc-mini">
                      <h4>Address </h4>
                        <p><?=$data_user[0]['User_city'].", ".$data_user[0]['User_state'].", ".$data_user[0]['User_country'];?></p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>