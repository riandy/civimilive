<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title><?=$data_user[0]['User_fname']." ".$data_user[0]['User_lname'];?>'s Professional Profile Page - <?=$data_user[0]['User_bioDesc'];?> in <?=$data_user[0]['User_city'].", ".$data_user[0]['User_country'];?> | civimi.com</title>
    <meta name="desc" content="<?=$data_user[0]['User_fname']." ".$data_user[0]['User_lname'];?>'s Professional Profile Page. A <?=$data_user[0]['User_bioDesc'];?> in <?=$data_user[0]['User_city'].", ".$data_user[0]['User_country'];?>.Page About <?=$data_user[0]['User_fname']." ".$data_user[0]['User_lname'];?>, Education, Employment, and Skills">
    <!-- Bootstrap -->
    <link href="<?php echo $global['absolute-url'];?>profile/assets/external/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!--favicon-->
    <link rel="apple-touch-icon" href="<?php echo $global['favicon'];?>">
    <link rel="shortcut icon" href="<?php echo $global['favicon'];?>" type="image/x-icon">

    <!-- Google Fonts -->
    <link href='http://fonts.googleapis.com/css?family=Sarala:400,700%7COpen+Sans:400,300' rel='stylesheet' type='text/css'>

    <!-- Theme Style -->
    <link href="<?php echo $global['absolute-url'];?>profile/assets/theme/css/style.css" rel="stylesheet" type="text/css">
    <!-- Your custom css -->
    <link href="<?php echo $global['absolute-url'];?>profile/assets/theme/css/theme-custom.css" rel="stylesheet">

    <!-- Font Icons -->
    <link href="<?php echo $global['absolute-url'];?>profile/assets/external/font-awesome-4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <!-- Captcha -->
    <link href="<?php echo $global['absolute-url'];?>profile/assets/external/simpleCaptcha/jquery.simpleCaptcha.css" rel="stylesheet">
    <!-- lightbox -->
    <link href="<?php echo $global['absolute-url'];?>profile/assets/external/magnific-popup/magnific-popup.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->