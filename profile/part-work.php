<?php if(is_array($data_works)){ ?>
<h2 class="title"><span>Employment</span></h2>
<div class="white-space-10" ></div>
<!-- resume list-->

<ul class="resume-list"> 
  <?php foreach($data_works as $data_work){ ?>
  <li class="work-list hide hm-work"> 
    <h4><i class="fa fa-calendar ic-re" ></i> <?=$data_work['ATwork_from_year'];?> - <?php if($data_work['ATwork_to_month'] != "0"){ echo $data_work['ATwork_to_year']; } else { echo "Present";}?></h4>
    <i><?=$data_work['ATwork_title'];?></i>
    <h3>
    	<?php if($data_work['Company_img']== ""){ ?>
	    <i class="fa fa-building-o ic-re" ></i>
	    <?php } else { ?>
	    <img src="<?=$global['img-url'].$data_work['Company_img'];?>" alt="logo <?=correctDisplay($data_work['Company_title']);?>" style="width:25px;height:25px;">
	    <?php } ?> 
	    <?=correctDisplay($data_work['Company_title']);?>
    </h3>
    <p><?php echo correctText($data_work['ATwork_desc']);?></p>
  </li>
  <?php } ?>
  <a id="more-work" href="javascript:;" class="see-list hide">SEE <span id="work-num"></span> MORE</a>
</ul><!-- end resume list-->
<?php } ?>