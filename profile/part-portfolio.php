<section id="portfolio" >
  <?php if(is_array($data_photos)){ ?>

  <!-- filter portfolio -->
  <div class="section-padd br-t bg2" >
    <div class="container-body clearfix">
      <h2 class="title dark"><span>Photography</span></h2>
    </div>
  </div>

  <!-- grid portfolio -->
  <div class="galery-box clearfix bg2">
    <?php for($p=0;$p<=3;$p++){?>
    <?php if(isset($data_photos[$p]['Photo_imgLink'])){ ?>
    <div class="col-sm-3 col-xs-6 item-box img">
      <div class="hover-area">
        <div class="text-vcenter-area">
          <div class="text-vcenter">
          <h3><a href="#" id="pp-<?=$data_photos[$p]['Photo_ID'];?>" onclick="openCard('<?=$data_photos[$p]['Photo_ID'];?>');" data-toggle="modal" data-target="#modal-card" class="portfolio-url"><?php echo $data_photos[$p]['Photo_title'];?><i class="fa fa-link"></i></a></h3>
          </div>  
        </div>
      </div>
      <img src="<?php echo $global['img-url'].$data_photos[$p]['Photo_ThmbImgLink'];?>" alt="<?php echo $data_photos[$p]['Photo_title'];?>">
    </div>
    <?php } } ?>

  </div><!-- end grid portfolio -->
  <?php } ?>
</section> 