<div class="page-loader">
      <div class="v-align-center">
        <div class="middle-content">
          <div class="img-p-area"> <img src="<?php echo check_image_url($data_user[0]['User_proPhoto']);?>" alt="Resume picture of <?=$data_user[0]['User_fname']." ".$data_user[0]['User_lname'];?> in CIVIMI" class="img-thumbnail no-radius"></div>
          <span class="itsme" >it's me</span>
          <h4><?=$data_user[0]['User_fname']." ".$data_user[0]['User_lname'];?></h4>
          <p>please wait</p>
          <div class="anim-pg">
            <span ></span>
          </div>
          <div class="force-pg"><button type="button" id='force-close-pg' class="btn">Skip This &rarr;</button></div>
        </div>
      </div>
    </div>