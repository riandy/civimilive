<?php 
session_start();
require_once("../model/Connection.php");
$obj_con = new Connection();

if(!isset($_GET['action'])){
	require_once("../model/User.php");
	$obj_user = new User();

	require_once("../model/User_Setting.php");
	$obj_uset = new User_Setting();

	require_once("../model/Setting.php");
	$obj_set = new Setting();

	require_once("../model/AT_Edu.php");
	$obj_edu = new AT_Edu();

	require_once("../model/AT_Work.php");
	$obj_work = new AT_Work();

	require_once("../model/AT_Skill.php");
	$obj_skill = new AT_Skill();

	require_once("../model/AT_Lang.php");
	$obj_lang = new AT_Lang();

	require_once("../model/Photo.php");
	$obj_photo = new Photo();

	require_once("../model/Doc.php");
	$obj_doc = new Doc();

	require_once("../model/Video.php");
	$obj_video = new Video();

	require_once("../model/Web.php");
	$obj_web = new Web();

	require_once("../model/Hits_User.php");
	$obj_hu = new Hits_User();

	if( isset($_GET['username']) && $_GET['username'] != ''){
		error_reporting(E_ALL^E_NOTICE); //remove notice
		$obj_con->up();

		$message = $_SESSION['message'];
		unset($_SESSION['message']);

		$O_username = mysql_real_escape_string(check_input($_GET['username']));
		$data_setting = $obj_uset->get_data_by_username($O_username);
		if(is_array($data_setting)){
			$O_resume = $data_setting[0]['Resume_set'];
		}else{
			$O_resume = "Yes";
		}

		if($O_resume == "Yes"){
			$data_user = $obj_user->get_data_by_username($O_username); //for data user
			$data_user_setting = $obj_set->get_data_by_username($O_username); //for sort order
			if(is_array($data_user)){
				$O_id = $data_user[0]['User_ID'];
				$O_email = $data_user[0]['User_email'];
			}
			
			$data_edus = $obj_edu->get_data($O_id); //for many data education user
			$data_works = $obj_work->get_data($O_id); //for many data work user
			$data_skills = $obj_skill->get_data($O_id); //for many data skill user
			$data_langs = $obj_lang->get_data($O_id); //for many data language user
			$data_photos = $obj_photo->get_data_resume($O_id); //for many data photo user
			$data_docs = $obj_doc->get_data_resume($O_id); //for many data document user
			$data_videos = $obj_video->get_data_resume($O_id); //for many data video user
			$data_webs = $obj_web->get_data_resume($O_id); //for many data web user

			//start for save hits user
			$N_ip = getIP(); //get ip
			$userGeoData = getGeoIP($N_ip); //get data by ip address
			$N_view_userID = $_SESSION['userData']['id'];
			$N_page = "resume";
			$N_type = "";
			$N_typeID = "";
			$N_city = $userGeoData->city; //ip city
			$N_country = $userGeoData->country_name; //ip country
			
			if($N_ip != "" && $N_city != "" && $N_country != ""){
				$obj_hu->insert_data($O_id, $O_username, $N_page, $N_type, $N_typeID, $N_view_userID, $N_ip, $N_city, $N_country);	
			}
			//end for save hits user
		}else{
			header("Location: {$global['base']}");
		}

		$obj_con->down();
	}
	else{
		header("Location: {$global['base']}");
	}
}else if($_GET['username'] != '' && $_GET['action'] == 'contact_me'){
	//print_r($_POST);
	require_once("../../model/Message.php");
	$obj_msg = new Message();
	$obj_con->up();

	$N_username = mysql_real_escape_string(check_input($_POST['contact_username']));
	$N_toUser = mysql_real_escape_string(check_input($_POST['contact_to_user']));
	$N_fromUser = mysql_real_escape_string(check_input($_POST['contact_from_user']));
	// $N_toEmail = mysql_real_escape_string(check_input($_POST['contact_to_email']));
	$N_toEmail = "support@civimi.com";
	$N_fromEmail = mysql_real_escape_string(check_input($_POST['contact_from_email']));
	$N_fromName = mysql_real_escape_string(check_input($_POST['contact_from_name']));
	$N_purpose = mysql_real_escape_string(check_input($_POST['contact_purpose']));
	$N_message = mysql_real_escape_string(check_input(str_replace("'", "’", $_POST['contact_message'])));
	$N_captcha = $_POST['contact_captcha'];
	$N_url = $_POST['contact_url'];

	if($N_captcha == $_SESSION['captcha_session']){
		
		if(isset($_SESSION['contact_name'])){ unset($_SESSION['contact_name']); }
        if(isset($_SESSION['contact_purpose'])){ unset($_SESSION['contact_purpose']); }
        if(isset($_SESSION['contact_message'])){ unset($_SESSION['contact_message']); }

		$result = $obj_msg->insert_data($N_toUser, $N_fromUser, $N_toEmail, $N_fromEmail, $N_fromName, $N_purpose, $N_message, "Publish");
		if($result > 0){
			$obj_msg->send_support($result, $N_toEmail, $N_username, $N_fromEmail, $N_purpose, $N_fromName, $N_message);
			$message = "Send message success!";
		}else{
			$message = "Send message failed!";
		}
	}else{
		$_SESSION['contact_name'] = $N_fromName;
        $_SESSION['contact_purpose'] = $N_purpose;
        $_SESSION['contact_message'] = $N_message;
		$message = "Failed to send message! Please insert the correct captcha!";
	}

	$_SESSION['message'] = $message;
	header("Location:".$N_url);
	$obj_con->down();	
}else{
	echo "error";
}
?> 