<?php 
session_start();

if(!isset($_GET['action'])){
	require_once("../model/Connection.php");
	$obj_con = new Connection();

	require_once("../model/User.php");
	$obj_user = new User();

	require_once("../model/User_Setting.php");
	$obj_set = new User_Setting();

	require_once("../model/WebCat.php");
	$obj_web_cat = new WebCat();

	require_once("../model/Web.php");
	$obj_web = new Web();

	require_once("../model/Hits_User.php");
	$obj_hu = new Hits_User();

	if($_GET['username'] != ''){
		error_reporting(E_ALL^E_NOTICE); //remove notice
		$obj_con->up();

		$O_username = mysql_real_escape_string(check_input($_GET['username']));
		$data_setting = $obj_set->get_data_by_username($O_username);
		if(is_array($data_setting)){
			$O_portolio = $data_setting[0]['Portfolio_set'];
		}else{
			$O_portolio = "Yes";
		}

		if($O_portolio == "Yes"){
			$data_user = $obj_user->get_data_by_username($O_username); //for data user
			if(is_array($data_user)){
				$O_id = $data_user[0]['User_ID'];
			}

			$data_web_cats = $obj_web_cat->get_web_cat($O_id, "name");//get list of album names by a user
			$O_cat_id = null;
			if(isset($_GET['cat_id'])){
				$O_cat_id = mysql_real_escape_string(check_input($_GET['cat_id']));
			}

			if($O_cat_id != "" || $O_cat_id != null){
				$data_webs = $obj_web->get_data_by_web_cat($O_cat_id); //for doc cat
			}else{
				$data_webs = $obj_web->get_data_recent($O_id); //for recent doc
			}

			//save in hits user
			$N_ip = getIP(); //get ip
			$userGeoData = getGeoIP($N_ip); //get data by ip address
			$N_view_userID = $_SESSION['userData']['id'];
			$N_page = "portfolio";
			$N_type = "web";
			$N_city = $userGeoData->city; //ip city
			$N_country = $userGeoData->country; //ip country
		}else{
			header("Location: {$global['base']}");
		}
		
		$obj_con->down();
	}
}
?>