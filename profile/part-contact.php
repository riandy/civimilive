<div class="section-padd">
            <div class="container-body clearfix">
              <h2 class="title dark">
                <span>
                  Let's Talk 
                  <!-- About <small id='show-subject'>...</small> <small><a href="index.html#" class="fire-toogle-subject mini-arrow"><i class="fa fa-angle-down"></i></a></small> -->
                </span>
              </h2>
              <!-- <div class="dropdown top-min">
                <a href="index.html#" data-toggle="dropdown" id='toogle-subject'></a>
                <ul class="dropdown-menu" id="option_subject" >
                  <li><a href="index.html#">The Next Project</a></li>
                  <li><a href="index.html#">The Next Design</a></li>
                  <li><a href="index.html#">The Next Mobile Apps</a></li>
                  <li><a href="index.html#">Say Hi</a></li>
                </ul>
              </div> -->
              <div class="white-space-10"></div>
              <form id="ContactForm" name="ContactForm" class="validate-form" action="<?=$global['absolute-url'];?>profile1/index.php?username=<?=$O_username;?>&action=contact_me" method="post" enctype="multipart/form-data">
                <input type="hidden" name="subject" value="" id='subject_contact'>
                <input type="hidden" name="file" id="file-att" value="">
                <div class="form-group">
                  <label>Your Name (*)</label>
                  <input name="contact_from_name" type="text" class="form-control form-flat" placeholder="name" value="<?php if(isset($_SESSION['contact_name'])){ echo $_SESSION['contact_name']; }?>" required>
                </div>
                <div class="form-group">
                  <label>Email (*)</label>
                  <input name="contact_from_email" type="email" class="form-control form-flat" placeholder="email" value="<?=$_SESSION['userData']['email'];?>" required>
                  <input name="contact_to_email" type="hidden" value="<?=$O_email;?>" required>
                </div>
                <div class="form-group">
                  <label>Purpose (*)</label>
                  <select class="form-control form-flat" name="contact_purpose" required>
                    <option value="">Purpose</option>
                    <?php if(isset($_SESSION['contact_purpose'])){ ?>
                    <option <?php if($_SESSION['contact_purpose'] == "Freelance"){echo "selected=selected";} ?> value="Freelance">Freelance</option>
                    <option <?php if($_SESSION['contact_purpose'] == "Network"){echo "selected=selected";} ?> value="Network">Network</option>
                    <option <?php if($_SESSION['contact_purpose'] == "Recruitment"){echo "selected=selected";} ?> value="Recruitment">Recruitment</option>
                    <option <?php if($_SESSION['contact_purpose'] == "Partnership"){echo "selected=selected";} ?> value="Partnership">Partnership</option>
                    <?php } else { ?>
                    <option value="Freelance">Freelance</option>
                    <option value="Network">Network</option>
                    <option value="Recruitment">Recruitment</option>
                    <option value="Partnership">Partnership</option>
                    <?php } ?>
                  </select>
                </div>
                <div class="form-group">
                  <label>Your Message (*)</label>
                  <textarea class="form-control form-flat" name="contact_message" rows="8" placeholder="message" required><?php if(isset($_SESSION['contact_message'])){ echo $_SESSION['contact_message']; }?></textarea>
                </div>
                <div class="form-group">
                  <img src="<?php echo $global['absolute-url-captcha'];?>" alt="captcha" >
                  <input style="margin:5px 0 0 0; " type="text" class="form-control form-flat" name="contact_captcha" placeholder="input captcha" required>
                </div>

                <div class="hold-feature uploader-hold" style="display:none;">
                  <div class="form-group">
                    <label>Attach Your Document (Optional) <span class="display-block ">(only .pdf  allowed , max size 2Mb)</span></label>
                    <div class="clearfix">            
                      <input type="button" id="upload-btn" class="btn  btn-file btn-xs btn-default clearfix" value="Choose file">
                      <div id="errormsg" class="clearfix error"></div>                
                      <div id="pic-progress-wrap" class="progress-wrap"></div>  
                      <div id="picbox" class="attbox "></div>
                    </div>
                  </div>  
                </div>

                <div class="hold-feature captcha-hold">
                  <div class="form-group">
                    <!-- generate captcha -->
                    <div id="mycaptcha-wrap" class="mycaptcha1">
                      <div id="mycaptcha" class="mycaptcha1"></div>
                    </div>
                  </div>  
                </div>


                <div class="form-group">
                  <input name="contact_url" type="hidden" value="<?php echo 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];?>">
                  <input name="contact_to_user" type="hidden" value="<?=$O_id;?>">
                  <input name="contact_username" type="hidden" value="<?=$O_username;?>">
                  <input name="contact_from_user" type="hidden" value="<?=$_SESSION['userData']['id'];?>">
                  <button type="submit" class="btn btnc2 with-br "><span>Send This Message</span></button>
                </div>

                <div class="form-group">
                  <!-- aja msg -->
                  <div class="preload-submit hidden"><hr/> <i class="fa fa-spinner fa-spin"></i> Please Wait ...</div>
                  <div class="message-submit error hidden"></div>
                </div>
              </form>
            </div>
          </div>
          <!-- popup alert -->
<div id="alert-popup" class="popup">
  <div id="popup-text" class="popup-text"></div>
  <a id="popup-close" href="javascript:;" class="popup-close"><i class="glyphicon glyphicon-remove"></i></a>
</div>