<section id="video" >
<?php if(is_array($data_videos)){ ?>

          <!-- filter portfolio -->
          <div class="section-padd br-t bg2" >
            <div class="container-body clearfix">
              <h2 class="title dark"><span>Video</span></h2>
              <!-- <ul class="list-inline list-filter-galery">
                <li class="active" data-filter="*"><a href="index.html#">All</a></li>
                <li data-filter=".img"><a href="index.html#" >Image</a></li>
                <li data-filter=".gal"><a href="index.html#">Gallery</a></li>
                <li data-filter=".vid"><a href="index.html#">Video</a></li>
              </ul> -->
            </div>
          </div>

          <!-- grid portfolio -->
          <div class="galery-box clearfix bg2">
            <?php for($v=0;$v<=3;$v++){?>
            <?php if(isset($data_videos[$v]['Video_link'])){ ?>
            <script type="text/javascript">
            $(document).ready(function() {
                videoGenerate(<?=$v;?>,"<?=$data_videos[$v]['Video_link'];?>");
            })
            </script>
            <div class="col-sm-3 col-xs-6 item-box img">
              <div class="hover-area">
                <div class="text-vcenter-area">
                  <div class="text-vcenter">
                    <h3><a id="videoEmbed-<?=$v;?>" href="<?php echo $data_videos[$v]['Video_link'];?>" class="video-url"><?php echo $data_videos[$v]['Video_title'];?><i class="fa fa-link"></i></a></h3>
                  </div>  
                </div>
              </div>
              <img id="video-<?=$v;?>" src="<?php echo $data_videos[$v]['Video_link'];?>" alt="<?php echo $data_videos[$v]['Video_title'];?>">
            </div>
            <?php } } ?>

          </div><!-- end grid portfolio -->

<?php } ?>
        </section>
        <script type="text/javascript">
    
    $(document).ready(function() {
        $(".video-url").fancybox({
            openEffect  : 'none',
            closeEffect : 'none',
            autoSize: true,
            type: 'iframe',
            iframe: {
            preload: false // fixes issue with iframe and IE
            }
        });
    });
    </script>