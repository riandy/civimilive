<section id="video" > 
<?php if(is_array($data_docs)){ ?>

          <!-- filter portfolio -->
          <div class="section-padd br-t bg2" >
            <div class="container-body clearfix">
              <h2 class="title dark"><span>Document</span></h2>
            </div>
          </div>

          <!-- grid portfolio -->
          <!-- <div class="galery-box clearfix bg2">
            <?php for($d=0;$d<=3;$d++){?>
            <?php if(isset($data_docs[$d]['Doc_fileLoc'])){ ?>
            <div class="col-sm-3 col-xs-6 item-box img">
              <div class="hover-area">
                <div class="text-vcenter-area">
                  <div class="text-vcenter">
                    <h3><a href="<?php echo $global['img-url'].$data_docs[$d]['Doc_fileLoc'];?>" class="link-work"><?php echo $data_docs[$d]['Doc_title'];?><i class="fa fa-link"></i></a></h3>
                  </div>  
                </div>
              </div>
              <img src="<?php echo $global['absolute-url'];?>img/doc-<?=$d;?>.png" alt="<?php echo $data_docs[$d]['Doc_title'];?>">
            </div>
            <?php } } ?>
          </div> -->
          <div class="portfolio-list" style="margin:20px 0 0 0;">
              <div class="row">
          <?php for($d=0;$d<=3;$d++){?>
          <?php if(isset($data_docs[$d]['Doc_fileLoc'])){ ?>
                  <div class="col-sm-6 col-xs-6 col-portfolio-doc">
                      <a href="<?php echo $global['img-url'].$data_docs[$d]["Doc_fileLoc"];?>" title="<?php echo $data_docs[$d]["Doc_title"];?>" class="portfolio-url-doc url-doc" >
                          <?php echo charLength(correctDisplay($data_docs[$d]["Doc_title"]),32);?>
                      </a>
                      <div class="document-desc">
                          &nbsp;&nbsp;<?php echo charLength(correctDisplay($data_docs[$d]["Doc_desc"]),180);?>
                      </div>
                      <div class="document-read">
                          <a href="<?php echo $global['img-url'].$data_docs[$d]["Doc_fileLoc"];?>" title="<?php echo $data_docs[$d]["Doc_title"];?>" class="read-doc open-doc">Read more</a>
                      </div>
                  </div> 
          <?php } } ?>
              </div>
          </div>
          <!-- end grid portfolio -->

<?php } ?>
        </section>
        <script type="text/javascript">
    
    $(document).ready(function() {
        $(".url-doc").fancybox({
            openEffect  : 'none',
            closeEffect : 'none',
            autoSize: true,
            type: 'iframe',
            iframe: {
            preload: false // fixes issue with iframe and IE
            }
        });
    });
    $(document).ready(function() {
        $(".open-doc").fancybox({
            openEffect  : 'none',
            closeEffect : 'none',
            autoSize: true,
            type: 'iframe',
            iframe: {
            preload: false // fixes issue with iframe and IE
            }
        });
    });
    </script>