<header class="main-header">
        <div class="nav-main">
          <a href="<?=$global['base'];?>" class="profil-logo"><img src="<?=$global['logo-desktop'];?>" alt="logo civimi"></a>
          <a href="#menu-ovefly" class="toogle-overfly"><i class="fa fa-bars open-t"></i> <i class="fa fa-times close-t"></i></a>
          <!-- <a href="#search-ovefly" class="toogle-overfly"><i class="fa fa-search open-t"></i> <i class="fa fa-times close-t"></i></a> -->
          <a href="#share-ovefly" class="toogle-overfly"><i class="fa fa-share-alt open-t"></i> <i class="fa fa-times close-t"></i></a>
        </div>

        <div class="over-fly-area" id="menu-ovefly">
          <div class="inner-overfly">
            <div class="middle-overfly">
              <h2 class="title-over">Menu</h2>
              <nav class="main-nav" id="menu">
                <ul class="nav">
                  <!-- <li class="active"><a href="index.html#aboutme" class="inner-link" data-text="About Me"><span>About Me</span></a></li>
                  <li><a href="index.html#resume" class="inner-link" data-text="My Resume"><span>Resume</span></a></li>
                  <li><a href="index.html#portfolio" class="inner-link" data-text="My Portfolio"><span>Portfolio</span></a></li>
                  <li><a href="index.html#"><span>Blog</span></a>
                    <ul>
                      <li><a href="blog-list-posts.html"  class="open-link" >List Posts</a></li>
                      <li><a href="blog-single-post.html"  class="open-link">Single Post</a></li>
                      <li><a href="index.html#modal-example" data-toggle="modal" >Extra Pages</a></li>
                    </ul>
                  </li>
                  <li><a href="index.html#contact" class="inner-link"  data-text="Contact Me"><span>Contact</span></a></li> -->
                  <?php if(isset($_SESSION['userData']['id'])){?>
                  <li><a href="<?=$path['user-edit'];?>" ><?=$lang['dropdown-profile'];?></a></li>
                  <li><a href="<?=$path['user-portfolio'];?>" ><?=$lang['dropdown-portfolio'];?></a></li>
                  <li class="hide"><a href="<?=$path['user-jobs'];?>" >Saved Jobs</a></li>
                  <li><a href="<?=$path['user-setting'];?>" ><?=$lang['dropdown-setting'];?></a></li>
                  <li><a href="<?=$path['user-resume'].$_SESSION['userData']['username'].".cvm";?>" target="_blank"><?=$lang['dropdown-my-resume'];?></a></li>
                  <li><a href="<?=$path['user-portfolio_image'].$_SESSION['userData']['username'].".cvm";?>" target="_blank"><?=$lang['dropdown-my-portfolio'];?></a></li>
                  <li><a href="<?=$path['user-logout'];?>"><?=$lang['dropdown-logout'];?></a></li>
                  <?php } else { ?>
                  <li><a href="#" data-toggle="modal" data-target="#modal-login">Login</a></li>
                  <li><a href="<?=$path['register'];?>">Sign Up</a></li>
                  <?php } ?>
                </ul>
              </nav>
            </div>
          </div>
        </div>

        <div class="over-fly-area" id="search-ovefly">
          <div class="inner-overfly">
            <div class="middle-overfly">
              <h2 class="title-over">Find My Articles</h2>
              <form class="from-search">
                <div class="form-group">
                  <input type="search" name="s" class="form-control input-lg input-search" placeholder="Input Text Here !" >
                </div>
                <div class="form-group">
                  <button  class="btn btnc2"><span>Search</span></button>
                </div>
              </form>
            </div>
          </div>
        </div>

        <div class="over-fly-area" id="share-ovefly">
          <div class="inner-overfly">
            <div class="middle-overfly">
              <h2 class="title-over">Share This Page</h2>
              <!-- You MUST change the URL definition in these links to share YOUR page - simply change the URL -->
              <div class="social-share">
                <a onclick="centeredPopup('https://www.facebook.com/sharer/sharer.php?u=<?php echo $path["user-page_profile"];?><?php echo $O_username;?>.cvm','myWindow','500','300','yes');return false" href="#" data-toggle="tooltip" data-placement="bottom" title="Facebook"><i class="fa fa-facebook"></i></a>
                <a onclick="centeredPopup('https://twitter.com/intent/tweet?url=<?php echo $path["user-page_profile"];?><?php echo $O_username;?>.cvm&via=civimi');return false" href="#" data-toggle="tooltip" data-placement="bottom" title="Twitter"><i class="fa fa-twitter"></i></a>
                <a onclick="centeredPopup('https://plus.google.com/share?url=<?php echo $path["user-page_profile"];?><?php echo $O_username;?>.cvm','myWindow','500','300','yes');return false" href="#" data-toggle="tooltip" data-placement="bottom" title="Google Plus"><i class="fa fa-google-plus"></i></a>
                <a onclick="centeredPopup('http://www.linkedin.com/shareArticle?mini=true&url=<?php echo $path["user-page_profile"];?><?php echo $O_username;?>.cvm&source=civimi','myWindow','500','300','yes');return false" href="#" data-toggle="tooltip" data-placement="bottom" title="Linkedin"><i class="fa fa-linkedin"></i></a>

           <!-- <a href="https://plus.google.com/share?url=http://www.yourwebsite.com" target="_blank"  data-toggle="tooltip" data-placement="bottom" title="Google Plus"><i class="fa fa-google-plus"></i></a>
                <a href="http://pinterest.com/pin/create/button/?url=http://www.yourwebsite.com" target="_blank"  data-toggle="tooltip" data-placement="bottom" title="Pinterest"><i class="fa fa-pinterest"></i></a>
                <a href="http://www.facebook.com/share.php?u=http://www.yourwebsite.com" target="_blank"  data-toggle="tooltip" data-placement="bottom" title="Facebook"><i class="fa fa-facebook"></i></a>
                <a href="http://twitter.com/home?status=http://www.yourwebsite.com" target="_blank"  data-toggle="tooltip" data-placement="bottom" title="Twitter"><i class="fa fa-twitter"></i></a> -->
              </div>
            </div>
          </div>
        </div>

        <!-- hero  -->
        <?php if($data_user[0]['User_bg_img'] != "") { ?>
        <div class="hero-01" style="background-image:url(<?=$data_user[0]['User_bg_img'];?>) !important;">
        <?php } else { ?>
        <div class="hero-01">
        <?php } ?>
          <div class="hero-border">
            <div class="top"></div>
            <div class="bottom"></div>
            <div class="left"></div>
            <div class="right">
              <div class="v-area">
                <div class="v-middle  show-span" >
                  <div class="p5" id="label-menu">
                    <span>A</span>
                    <span>B</span>
                    <span>O</span>
                    <span>U</span>
                    <span>T</span>
                    <span></span>
                    <span>M</span>
                    <span>E</span>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <!-- <div class="my-thumb">
            <div class="thumb-top"><a href="assets/theme/img/pic/pic1.jpg" class="image-popup" title="This is Me."><img src="assets/theme/img/profile-thumb.jpg" class="img-thumbnail no-radius"  alt=""></a></div>
          </div> -->

          <div class="content-hero">
            <div class="v-content">
              <!-- <h4 class="font-normal">My Name is</h4> -->
              <h1 class="myname"><?=$data_user[0]['User_fname']." ".$data_user[0]['User_lname'];?></h1>
              <h3 class="font-normal with-line"><?=$data_user[0]['User_bioDesc'];?></h3>
              <p ><a href="#contact" class="btn btnc1 contact-me"><span>Contact Me</span></a> <a href="<?=$path['user-resume'].$O_username.".cvm";?>" target="_blank" class="btn btnc1"><span>View My Resume</span></a></p>
            </div>
          </div>

        </div><!-- hero  -->
      </header>
      <!-- Modal Login-->
      <div class="modal fade" id="modal-login" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog mlogin-size">
          <div class="modal-content mlogin-content">
            <div class="modal-body mlogin-body">
              <div class="row">
                  <div class="col-xs-6"><div class="mlogin-header">CIVIMI Login</div></div>
                  <!--<div class="col-xs-6"><div class="mlogin-rheader">Employer Login <span style="color:#D54E55;">&gt;&gt;</span></div></div>-->
                  <div class="col-xs-12"><hr/></div>
              </div>
              <div class="row">
                  <div class="col-xs-12 col-modal-left">
                      <form name="register" action="<?=$path['user-login'];?>" method="post" enctype="multipart/form-data" >
                          <?php if(isset($_SESSION['statusLogin'])){ ?>
                              <div class="col-xs-12">
                                  <div style="color: #a94442;font-weight: bold;font-size: 13px;margin: 3px 0;"><?php echo $_SESSION['statusLogin'];?></div>
                              </div>
                          <?php } ?>
                          <input type="email" class="form-control up15" name="email" placeholder="Email (name@example.com)" required>
                          <input type="password" class="form-control up15" name="password" placeholder="Password" required>
                          <div class="forget-pass">
                              <a href="<?=$path['forgot-password'];?>">Forget password?</a>
                          </div>
                          <button class="btn btn-info btn-block up15" type="submit">Login</button>
                      </form>
                  </div>
                  <div class="col-xs-1 col-modal-center hidden-xs"><img src="<?php echo $global['absolute-url'];?>img/or.png" alt="border"></div>
                  <div class="col-xs-12 col-modal-right">
                      <a href="http://login.facebook" onclick="this.href='<?=$path['login-with-facebook'];?>'" type="button" class="btn btn-info btn-facebook btn-block"><img src="<?php echo $global['absolute-url'];?>img/btn-facebook.png" alt="facebook">Login with Facebook</a>
                      <a href="http://login.google" onclick="this.href='<?=$path['login-with-google'];?>'" type="button" class="btn btn-info btn-google btn-block"><img src="<?php echo $global['absolute-url'];?>img/btn-google.png" alt="google">Login with Google+</a>
                      <!--<a href="http://login.twitter" onclick="this.href='<?=$path['login-with-twitter'];?>'" type="button" class="btn btn-info btn-twitter btn-block"><img src="<?php echo $global['absolute-url'];?>img/btn-twitter.png" alt="twitter">Login with Twitter</a>-->
                      <br />
                      <div class="mlogin-register">Don't have an account? <a href="<?=$path['register'];?>">Join now</a></div>
                  </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <!-- Modal Login 2-->
      <div class="modal fade" id="modalLogin" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog mlog-size">
          <div class="modal-content mlogin-content">
            <div class="modal-body mlog-body pad0">
              <div class="mlog-header">
                <div class="mlog-img">
                  <img src="<?php echo $global['absolute-url'];?>img/background/cvm-bg4.jpg" alt="image">
                </div>
                <div class="mlog-header-text">Discover people around the world Who share your passions</div>
              </div>
              <div class="row">
                <div class="col-xs-12">
                  <div class="mlog-title">Create your own resume in civimi!</div>
                </div>
              </div>
              <div class="row marg0">
                <div class="col-xs-12">
                  <div class="text-center">
                    <a href="<?=$path['register'];?>" class="btn btn-info btn-mlog">Sign Up With Email</a>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-6 col-xs-12 up2">
                  <a href="http://login.facebook" onclick="this.href='<?=$path['login-with-facebook'];?>'" class="btn btn-info btn-mlog-fb"><img src="<?php echo $global['absolute-url'];?>img/btn-facebook.png" alt="facebook">Sign Up With Facebook</a>
                </div>
                <div class="col-sm-6 col-xs-12 up2">
                  <a href="http://login.google" onclick="this.href='<?=$path['login-with-google'];?>'" class="btn btn-info btn-mlog-google"><img src="<?php echo $global['absolute-url'];?>img/btn-google.png" alt="google">Sign Up With Google</a>
                </div>
              </div>

              <div class="row up2">
                <div class="col-xs-6 pad0">
                  <a href="#" class="mlog-link" data-dismiss="modal">No Thanks</a>
                </div>
                <div class="col-xs-6 pad0">
                  <a href="#" class="mlog-link" data-dismiss="modal" data-toggle="modal" data-target="#modal-login">Login</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>