<?php if(is_array($data_skills)){ ?>
<h2 class="title"><span>Skills</span></h2>
<!-- skills -->
<div class="row">
  <?php foreach($data_skills as $data_skill){ ?>
  <div class="skill-list hide hm-skill col-md-6">
    <ul class="list-unstyled list-progress">
      <li>
        <h4><?=$data_skill['Skill_name'];?>  <?php if($data_skill['ATskill_rate']!="0"){ ?><small class="label label-cvm"><?=replace_rate($data_skill['ATskill_rate']);?></small> <?php } ?></h4>
        <!-- <div class="progress-line">
          <div class="line" data-holdwidth="100%" ></div>
        </div> -->
      </li>
    </ul>
    <hr>
  </div>
  <?php } ?>
</div><!-- skills -->
<a id="more-skill" href="javascript:;" class="see-list hide">SEE <span id="skill-num"></span> MORE</a>
<?php } ?>