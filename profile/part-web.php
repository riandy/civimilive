<section id="video" > 
<?php if(is_array($data_webs)){ ?>

          <!-- filter portfolio -->
          <div class="section-padd br-t bg2" >
            <div class="container-body clearfix">
              <h2 class="title dark"><span>Website</span></h2>
            </div>
          </div>

          <!-- grid portfolio -->
          <!-- <div class="galery-box clearfix bg2">
            <?php for($w=0;$w<=3;$w++){?>
            <?php if(isset($data_webs[$w]['Web_link'])){ ?>
            <div class="col-sm-3 col-xs-6 item-box img">
              <div class="hover-area">
                <div class="text-vcenter-area">
                  <div class="text-vcenter">
                    <h3><a href="<?php echo correctDisplay($data_webs[$w]['Web_link']);?>" class="link-work"><?php echo $data_webs[$w]['Web_title'];?><i class="fa fa-link"></i></a></h3>
                  </div>  
                </div>
              </div>
              <img src="<?php echo $global['absolute-url'];?>img/placeholder-web.png" alt="<?php echo $data_webs[$w]['Web_title'];?>">
            </div>
            <?php } } ?> 
          </div> -->
          <div class="portfolio-list" style="margin:20px 0 0 0;">
              <div class="row">
          <?php for($w=0;$w<=3;$w++){?>
          <?php if(isset($data_webs[$w]['Web_link'])){ ?>
                  <div class="col-sm-6 col-xs-6 col-portfolio-doc">
                      <a href="<?php echo $data_webs[$w]["Web_link"];?>" title="<?php echo $data_webs[$w]["Web_title"];?>" class="portfolio-url-doc url-web">
                          <?php echo charLength(correctDisplay($data_webs[$w]["Web_title"]),32);?>
                      </a>
                      <div class="document-desc">
                          &nbsp;&nbsp;<?php echo charLength(correctDisplay($data_webs[$w]["Web_desc"]),180);?>
                      </div>
                      <div class="document-read">
                          <a href="<?php echo $data_webs[$w]["Web_link"];?>" title="<?php echo $data_webs[$w]["Web_title"];?>" class="read-doc open-web">Read more</a>
                      </div>
                  </div> 
          <?php } } ?>
              </div>
          </div>
          <!-- end grid portfolio -->

<?php } ?>
        </section>
        <script type="text/javascript">
    
    $(document).ready(function() {
        $(".url-web").fancybox({
            openEffect: 'elastic',
            closeEffect: 'elastic',
            autoSize: true,
            type: 'iframe',
            iframe: {
            preload: false // fixes issue with iframe and IE
            }
        });
    });
    $(document).ready(function() {
        $(".open-web").fancybox({
            openEffect  : 'elastic',
            closeEffect : 'elastic',
            autoSize: true,
            type: 'iframe',
            iframe: {
            preload: false // fixes issue with iframe and IE
            }
        });
    });
    </script>