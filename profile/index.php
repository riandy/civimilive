<?php 
include("packages/require.php");
include("controller/controller_profil_page.php");
$curpage='profile';
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <?php include("head.php");?>
    <meta name="keywords" content="<?=$data_user[0]['User_bioDesc'].' | '.$data_user[0]['User_fname'].' '.$data_user[0]['User_lname'];?>, resume, curricullum vitae, portfolio, experience, education, certificate, skills">
    <meta name="description" content="<?=$data_user[0]['User_obj'];?>">
    <meta itemprop="name" content="<?=$data_user[0]['User_fname']." ".$data_user[0]['User_lname'];?>">
    <meta itemprop="description" content="<?=$data_user[0]['User_obj'];?>">
    <meta itemprop="image" content="<?php echo check_image_url($data_user[0]['User_proPhoto']);?>">
    
    <!-- facebook and linkedin -->
    <meta prefix="og: http://ogp.me/ns#" property="og:description" content="<?=$data_user[0]['User_obj'];?>">
    <meta prefix="og: http://ogp.me/ns#" property="og:image" content="<?php echo check_image_url($data_user[0]['User_proPhoto']);?>">
    <meta prefix="og: http://ogp.me/ns#" property="og:site_name" content="CIVIMI">
    <meta prefix="og: http://ogp.me/ns#" property="og:title" content="<?=$data_user[0]['User_fname']." ".$data_user[0]['User_lname']."'s CV and Portfolio - ".$data_user[0]['User_bioDesc']." in ".$data_user[0]['User_city'].", ".$data_user[0]['User_country'];?>">
    <meta prefix="og: http://ogp.me/ns#" property="og:type" content="website">
    <meta prefix="og: http://ogp.me/ns#" property="og:url" content="<?=$path['user-resume'].$data_user[0]['User_username'].".cvm";?>">

    <!-- twitter meta -->
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:description" content="<?=$data_user[0]['User_obj'];?>">
    <meta name="twitter:title" content="<?=$data_user[0]['User_fname']." ".$data_user[0]['User_lname']."'s CV and Portfolio - ".$data_user[0]['User_bioDesc']." in ".$data_user[0]['User_city'].", ".$data_user[0]['User_country'];?>">
    <meta name="twitter:site" content="@CIVIMI">
    <meta name="twitter:creator" content="@CIVIMI">
    <meta name="twitter:image" content="<?php echo check_image_url($data_user[0]['User_proPhoto']);?>">
    <meta name="twitter:image:src" content="<?php echo check_image_url($data_user[0]['User_proPhoto']);?>">
    <link rel="stylesheet" href="<?php echo $global['absolute-url'];?>stylesheets/global-style.css"/>
    <link rel="stylesheet" href="<?php echo $global['absolute-url'];?>stylesheets/libcvm.css"/>
    <script src="<?php echo $global['absolute-url'];?>profile/assets/external/jquery.min.js"></script>
    <!-- Add fancyBox main JS and CSS files -->
    <script type="text/javascript" src="<?php echo $global['resume-page'];?>js/fbox/source/jquery.fancybox.js?v=2.1.5"></script>
    <link rel="stylesheet" type="text/css" href="<?php echo $global['resume-page'];?>js/fbox/source/jquery.fancybox.css?v=2.1.5" media="screen" />
  </head>
  <body >
    <!-- Page Loader -->
    <?php //include("part-loader.php");?>
    <!-- End Page Loader -->

    <!-- body-area -->
    <div class="body-area">
      <!-- Header -->
      <?php include("part-header.php");?>
      <!-- end HEader -->
      <!-- Content body -->
      <div class="content-body">

        <!-- start about me section -->
        <?php include("part-about.php");?>
        <!-- end about me section -->

        <!-- start resume section -->
        <section id="resume">
          <div class="section-padd" >
            <div class="container-body clearfix">

              <?php include("part-edu.php");?>

              <?php include("part-work.php");?>
              
              <?php include("part-skill.php");?>

              <?php include("part-language.php");?>
              
            </div>
          </div>
        </section><!-- end resume section-->

        <!-- start portfolio section-->
        <?php include("part-portfolio.php");?>
        <!-- end portfolio section -->

        <!-- start video section-->
        <?php include("part-video.php");?>
        <!-- end video section -->

        <!-- start document section-->
        <?php include("part-document.php");?>
        <!-- end document section -->

        <!-- start web section-->
        <?php include("part-web.php");?>
        <!-- end web section -->
        <div class="section-padd br-t bg2" ></div>
        <!-- start contact section -->
        <section id='contact'>
 
          <!-- contact form -->
          <?php include("part-contact.php");?>
          <!-- end contact form -->

          <!-- social links -->
          <?php //include("part-social.php");?>
          <!-- end social links -->

        </section><!-- end contact section -->

        <a href="#" class="back-top" style="bottom:12%;right:25px;"><img src="<?php echo $global['absolute-url'];?>img/back-top.png" alt="back-top"></a>
      </div><!-- end Content body -->

      <?php include_once("../packages/analytic.php");?>
      <!-- footer -->
      <footer class="main-footer">
        <?=$global['copyright'];?>
      </footer><!-- footer -->
    </div><!-- body-area -->


    <!-- portfolio details ajax -->
    <div class="over-fly-area" id="load-works">
      <div class="inner-overfly" id="work-wait-msg">
        <div class="middle-overfly">
          <h2 class="title-over">PLEASE WAIT...</h2>
        </div>
      </div>
      <div class="work-close"><a href="index.html#" class="close-panel-work btn btn-xs btn-default" >Close</a></div>
      <div id="load-work-html" ></div>
    </div>

    <!-- Modal Example-->
    <div class="modal fade" id="modal-example" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">Extra Pages</h4>
          </div>
          <div class="modal-body">
            <ul class="list-boxpage">
              <li><a href="blog-list-posts.html" class="open-link">Blog List Post</a></li>
              <li><a href="blog-single-post.html" class="open-link">Blog Single Post</a></li>
              <li><a href="404.html" class="open-link">404 error</a></li>
              <li><a href="cooming-soon.html" class="open-link">Cooming Soon</a></li>
              <li><a href="theme-element.html" class="open-link">Theme Element</a></li>
            </ul>
          </div>
          <div class="modal-footer">

            <!-- <a href="index.html#" class="btn btnc2 with-br " ><span>Buy This Theme</span></a> -->
          </div>
        </div>
      </div>
    </div><!-- end  Modal Example-->

    

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="<?php echo $global['absolute-url'];?>profile/assets/external/jquery.easing-1.3.pack.js"></script>

    <!-- Include all compiled plugins (below)-->
    <script src="<?php echo $global['absolute-url'];?>profile/assets/external/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?php echo $global['absolute-url'];?>profile/assets/external/jquery.easypiechart.min.js"></script>
    <script src="<?php echo $global['absolute-url'];?>profile/assets/external/isotope.pkgd.min.js"></script>
    <script src="<?php echo $global['absolute-url'];?>profile/assets/external/validator/jquery.validate.min.js"></script>
    <script src="<?php echo $global['absolute-url'];?>profile/assets/external/simpleCaptcha/jquery.simpleCaptcha.js"></script>
    <script src="<?php echo $global['absolute-url'];?>profile/assets/external/Simple-Ajax-Uploader/SimpleAjaxUploader.min.js"></script>
    <script src="<?php echo $global['absolute-url'];?>profile/assets/external/magnific-popup/jquery.magnific-popup.min.js"></script>
    <script src="<?php echo $global['absolute-url'];?>profile/assets/external/metisMenu/metisMenu.js"></script>

    <!-- theme config --> 
    <script src="<?php echo $global['absolute-url'];?>profile/assets/theme/js/theme.js"></script>

    <!-- map --> 
    <script src="http://maps.googleapis.com/maps/api/js?sensor=false" type="text/javascript"></script> 
    <script src="<?php echo $global['absolute-url'];?>profile/assets/external/gmap3.min.js"></script>
    <script src="<?php echo $global['absolute-url'];?>profile/assets/theme/js/map.js"></script>
    <script src="<?php echo $global['absolute-url'];?>js/function.js"></script>

    <!-- START PORTFOLIO CARD-->
    <?php include("../part-portfolio_card.php");?>
    <!-- END PORTFOLIO CARD-->
    <script language="javascript">
    var popupWindow = null;
    function centeredPopup(url,winName,w,h,scroll){
      LeftPosition = (screen.width) ? (screen.width-w)/2 : 0;
      TopPosition = (screen.height) ? (screen.height-h)/2 : 0;
      settings ='height='+h+',width='+w+',top='+TopPosition+',left='+LeftPosition+',scrollbars='+scroll+',resizable'
      popupWindow = window.open(url,winName,settings)
    }
    </script>

    <script>
    $(document).ready(function () {
      //edu see more
      size_edu = $(".edu-list").size();
      x=2;
      $('.edu-list:lt('+x+')').removeClass('hide');
      $('.edu-list:lt('+x+')').removeClass('hm-edu'); 
      size_edu_left= $(".hm-edu").size();
      if(size_edu > 2){
        $('#more-edu').removeClass('hide');
        $("#edu-num").text('('+size_edu_left+')');
      } else {
        $('#more-edu').addClass('hide');
      }
      $('#more-edu').on('click', function(){
          $('.edu-list').removeClass('hide');
          $('.edu-list').removeClass('hm-edu');
          $('#more-edu').addClass('hide');
      });// end edu see more

      //work see more
      size_work = $(".work-list").size();
      w=2;
      $('.work-list:lt('+w+')').removeClass('hide');
      $('.work-list:lt('+w+')').removeClass('hm-work'); 
      size_work_left= $(".hm-work").size();
      if(size_work > 2){
        $('#more-work').removeClass('hide');
        $("#work-num").text('('+size_work_left+')');
      } else {
        $('#more-work').addClass('hide');
      }
      $('#more-work').on('click', function(){
          $('.work-list').removeClass('hide');
          $('.work-list').removeClass('hm-work');
          $('#more-work').addClass('hide');
      });// end work see more

      //skill see more
      size_skill = $(".skill-list").size();
      s=4;
      $('.skill-list:lt('+s+')').removeClass('hide');
      $('.skill-list:lt('+s+')').removeClass('hm-skill'); 
      size_skill_left= $(".hm-skill").size();
      if(size_skill > 4){
        $('#more-skill').removeClass('hide');
        $("#skill-num").text('('+size_skill_left+')');
      } else {
        $('#more-skill').addClass('hide');
      }
      $('#more-skill').on('click', function(){
          $('.skill-list').removeClass('hide');
          $('.skill-list').removeClass('hm-skill');
          $('#more-skill').addClass('hide');
      });// end skill see more

      //language see more
      size_language = $(".language-list").size();
      l=4;
      $('.language-list:lt('+l+')').removeClass('hide');
      $('.language-list:lt('+l+')').removeClass('hm-language'); 
      size_language_left= $(".hm-language").size();
      if(size_language > 4){
        $('#more-language').removeClass('hide');
        $("#language-num").text('('+size_language_left+')');
      } else {
        $('#more-language').addClass('hide');
      }
      $('#more-language').on('click', function(){
          $('.language-list').removeClass('hide');
          $('.language-list').removeClass('hm-language');
          $('#more-language').addClass('hide');
      });// end language see more
    });           
    jQuery(document).ready(function() {
      var offset = 220;
      var duration = 500;
      jQuery('.content-body').scroll(function() {
        if (jQuery(this).scrollTop() > offset) {
          jQuery('.back-top').fadeIn(duration);
        } else {
          jQuery('.back-top').fadeOut(duration);
        }
      });
   
      jQuery('.back-top').click(function(event) {
        event.preventDefault();
        jQuery('.content-body').animate({scrollTop: 0}, duration);
        return false;
      })
    });
  </script>
<script language="javascript">
    var popupWindow = null;
    function centeredPopup(url,winName,w,h,scroll){
        LeftPosition = (screen.width) ? (screen.width-w)/2 : 0;
        TopPosition = (screen.height) ? (screen.height-h)/2 : 0;
        settings ='height='+h+',width='+w+',top='+TopPosition+',left='+LeftPosition+',scrollbars='+scroll+',resizable'
        popupWindow = window.open(url,winName,settings)
    }
    </script>
<script type="text/javascript">
<?php if(!isset($_SESSION['userData']['id'])){?>
$(document).ready(function(){
    $("#modalLogin").modal('show');
});
<?php } ?>
//generate thumbnail
function videoGenerate(id,url){
    var thumbnail = "";
    var iframe_src       = url;
    var youtube_video_id = parseVideoID(iframe_src);
    var thumbnail = "<?=$global['absolute-url'];?>img/placeholder-video.png";
    if (youtube_video_id.length == 11) {
        var thumbnail = "//img.youtube.com/vi/"+youtube_video_id+"/0.jpg";
        var embedURL = "https://www.youtube.com/embed/"+youtube_video_id;
    }
    $("#video-"+id).attr('src', thumbnail);
    $("#videoEmbed-"+id).attr('href', embedURL);
}
function changeSub(sub){
        var link = "<?php echo 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];?>";
        jQuery.ajax({
        url: "<?php echo $global['absolute-url'];?>packages/setlanguage.php",
        type: "POST",
        data: {
            lang: sub,
        },
        dataType : 'json',
        success: function(data, textStatus, xhr) {
            console.log(data); // do with data e.g success message
        }
        });
        window.location.href = link;
    }
function popup(message){
    $("#alert-popup").fadeIn();
    $("#popup-text").text(message);
    setTimeout(function(){
        $("#alert-popup").fadeOut();
    }, 5000);
}
$("#popup-close").click(function(){
    $("#alert-popup").fadeOut();
})
<?php if($message != ""){?>
        $(document).ready(function(){
            var message = "<?php echo $message;?>";
            popup(message);
        });
        <?php } ?>
</script>
  </body>
</html>