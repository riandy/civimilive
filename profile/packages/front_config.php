<?php 
$global['civimi-page'] = "http://www.civimi.com/";
$global['main-page'] = "http://www.civimi.com/resume/";
$global['absolute-url'] = "http://www.civimi.com/resume/";
$global['img-url'] = $global['absolute-url']."img/";
$global['absolute-url-admin'] = $global['absolute-url']."admin/";
$global['path-head'] = "packages/head.php";
$global['path-config'] = "packages/front_config.php";
$global['favicon'] = $global['absolute-url']."img/icon/favicon.ico";
$global['logo-mobile'] = $global['absolute-url']."img/logo/logo-mobile.png";
$global['logo-desktop'] = $global['absolute-url']."img/logo/logo-desktop.png";
$global['copyright'] = "Copyright © ".date('Y')." Civimi. All rights reserved. Terms and Conditions";
$seo['robot_yes'] = "index, follow";
$seo['robot_no'] = "noindex, nofollow";

$global['title-website'] = "Civimi ";



?>