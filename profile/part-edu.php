<?php if(is_array($data_edus)){ ?>
<h2 class="title"><span>Education</span></h2>
<div class="white-space-10" ></div>
<!-- resume list-->

<ul class="resume-list"> 
  <?php foreach($data_edus as $data_edu){ ?>
  <li class="edu-list hide hm-edu"> 
    <h4><i class="fa fa-calendar ic-re"></i> <?=$data_edu['ATedu_fromYear'];?> - <?php if($data_edu['ATedu_toMonth'] != "0"){ echo $data_edu['ATedu_toYear']; } else { echo "Present";}?></h4>
    <i><?=$data_edu["ATedu_degree"]."&nbsp;in ".$data_edu["ATedu_major"];?></i>
    <h3>
	    <?php if($data_edu['Edu_img']== ""){ ?>
	    <i class="fa fa-building-o ic-re" ></i>
	    <?php } else { ?>
	    <img src="<?=$global['img-url'].$data_edu['Edu_img'];?>" alt="logo <?=correctDisplay($data_edu['Edu_schoolName']);?>" style="width:25px;height:25px;">
	    <?php } ?> 
	    <?=correctDisplay($data_edu['Edu_schoolName']);?>
    </h3>
    <?=correctDisplay($data_edu['ATedu_desc']);?>
  </li>
  <?php } ?>
  <a id="more-edu" href="javascript:;" class="see-list hide">SEE <span id="edu-num"></span> MORE</a>
</ul><!-- end resume list-->
<?php } ?>