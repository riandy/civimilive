<div id="personal-education-section" class="job-sbox" data-order="2">
  <div id="education-header" class="user-section-header row border-bottom-eee">
    <div class="col-xs-6">
      <div class="profile-title"><i class="glyphicon glyphicon-education"></i>&nbsp;&nbsp;<?=$lang['edu-title'];?></div>
    </div>
    <div class="col-xs-6">
        <div class="edit-button text-right">
          <span class="sort-icon"><i class="glyphicon glyphicon-sort"></i></span>
            <a href="Javascript:;" id="add-education" onclick="hideEmpty();" data-toggle="collapse" data-target="#slide-education" aria-expanded="true" aria-controls="slide-education">
        <i class="glyphicon glyphicon-plus"></i> <span class="hidden-xs"><?=$lang['edu-btn'];?></span>
      </a>
        </div>
      
    </div>
  </div>
  <div id="education-profile">
    <!-- start add edu -->
    <div id="slide-education" class="collapse panel-input">
      <div class="panel-input-body panel-input-edu">
        <div id="listed-place">
          <div id="listed-school">
            <div class="row ">
              <div class="col-xs-6 up1">
                <div class="form-label bold"><?=$lang['edu-school'];?> *</div>
              </div>
              <div class="col-xs-6 up15 not-listed">
                <!-- <a id="link-listed" href="#slide-education" onclick="openSchool();">not listed here?</a> -->
              </div>
            </div>
            <!-- start input school -->
            <div class="row">
              <div class="col-xs-12">
                <div id="school-box" class="school-box">
                  <input id="edu-title" onfocus="$('#school-list').show();" oninput="get_univ();$('#school-list').show();" type="text" class="form-control no-radius" placeholder="<?=$lang['edu-school_placeholder'];?>">
                  <div id="school-list" class="school-list" onmouseover="$('#school-list').show();">
                    <ul id="school-ul">

                    </ul>
                  </div>
                </div>
                <div id="edu-vtitle" class="valid-message"></div>
              </div>
            </div><!-- end input school -->
          </div>
        </div>
        <div class="row up1">
          <div class="col-xs-6">
            <div class="form-label bold"><?=$lang['edu-country'];?> *</div>
          </div>
          <div class="col-xs-6">
            <div class="form-label bold"><?=$lang['edu-city'];?> *</div>
          </div>
        </div>
        <div class="row">
          <div class="col-xs-6">
            <input id="edu-country" type="text" class="form-control no-radius" name="country" placeholder="<?=$lang['edu-country_placeholder'];?>">
          </div>
          <div class="col-xs-6">
            <input id="edu-city" type="text" class="form-control no-radius" name="city" placeholder="<?=$lang['edu-city_placeholder'];?>">
          </div>
          <div class="col-xs-12">
            <div id="edu-vcountry" class="valid-message"></div>
          </div>
        </div>
          <div class="row up1">
            <!-- <div class="col-xs-12">
              <div class="form-label">Education Status</div>
            </div> -->
            <div class="col-sm-6 col-xs-12">
              <div class="checkbox">
                <label>
                  <input id="edu-status" type="checkbox" onclick="checkStatEdu();" value="yes"> <?=$lang['edu-attending'];?>
                </label>
              </div>
            </div>
          </div>
        <div class="row">
          <div class="col-xs-6 up1">
            <div class="form-label bold"><?=$lang['edu-from'];?> *</div>
            <div class="row">
              <div class="col-xs-6" style="padding: 0 5px 0 0;">
                <select id="edu-fmonth" name="" class="form-control no-radius">
                  <option value="">Month</option>
                  <?php $month = '12';
                  for ($months = '1'; $months <= $month; $months++) { ?>
                  <option value="<?php echo $months; ?>"><?php echo date("F", strtotime("$months/12/10")); ?></option>
                  <?php } ?>
                </select>
              </div>
              <div class="col-xs-6" style="padding: 0 0 0 5px;">
                <select id="edu-fyear" name="" class="form-control no-radius">
                  <option value="">--</option>
                  <?php $year = '1950';
                  for ($years = date("Y"); $years >= $year; $years--) { ?>
                  <option value="<?php echo $years; ?>"><?php echo $years; ?></option>
                  <?php } ?>
                </select>
              </div>
            </div>
          </div>
          <div id="to-edustat" class="col-xs-6 up1">
            <div class="form-label bold"><?=$lang['edu-to'];?> *</div>
            <div class="row">
              <div class="col-xs-6" style="padding: 0 5px 0 0;">
                <select id="edu-tmonth" name="" class="form-control no-radius">
                  <option value="" class="edu-tmonth">Month</option>
                  <?php $month = '12';
                  for ($months = '1'; $months <= $month; $months++) { ?>
                  <option value="<?php echo $months; ?>" class="edu-tmonth"><?php echo date("F", strtotime("$months/12/10")); ?></option>
                  <?php } ?>
                </select>
              </div>
              <div class="col-xs-6" style="padding: 0 0 0 5px;">
                <select id="edu-tyear" name="" class="form-control no-radius">
                  <option value="" class="edu-tyear">--</option>
                  <?php $year = '1950';
                  for ($years = date("Y"); $years >= $year; $years--) { ?>
                  <option value="<?php echo $years; ?>" class="edu-tyear"><?php echo $years; ?></option>
                  <?php } ?>
                </select>
              </div>
            </div>
          </div>
          <div class="col-xs-12">
            <div id="edu-vdate" class="valid-message"></div>
          </div>
        </div>
        <div class="row">
          <div class="col-xs-12 up1">
            <div class="form-label bold"><?=$lang['edu-degree'];?> *</div>
          </div>
          <div class="col-sm-10 col-xs-12">
            <input id="edu-degree" type="text" name="" class="form-control no-radius" placeholder="<?=$lang['edu-degree_placeholder'];?>">
            <div id="edu-vdegree" class="valid-message"></div>
          </div>
        </div>
        <div class="row">
          <div class="col-xs-12 up1">
            <div class="form-label bold"><?=$lang['edu-study'];?> *</div>
          </div>
          <div class="col-sm-10 col-xs-12">
            <input id="edu-field" type="text" name="" class="form-control no-radius" placeholder="<?=$lang['edu-study_placeholder'];?>">
            <div id="edu-vfield" class="valid-message"></div>
          </div>
        </div>
        <div class="row">
          <div class="col-xs-12 up1">
            <div class="form-label"><?=$lang['edu-grade'];?></div>
          </div>
          <div class="col-sm-6 col-xs-12">
            <input id="edu-grade" type="text" name="" class="form-control no-radius" placeholder="<?=$lang['edu-grade_placeholder'];?>">
            <div id="edu-vgrade" class="valid-message"></div>
          </div>
        </div>
        <div class="row">
          <div class="col-xs-12 up1">
            <div class="form-label"><?=$lang['edu-desc'];?></div>
          </div>
          <div class="col-sm-12 col-xs-12">
            <textarea id="edu-desc" name="desc" class="form-control no-radius" rows="3" placeholder="<?=$lang['edu-desc_placeholder'];?>"></textarea>
          </div>
        </div>
        <div class="row">
          <div class="col-xs-12 up2">
            <div id="edu-message" class="valid-message"></div>
            <a href="Javascript:;" class="btn btn-info" onclick="save_edu();">Save</a><span class="pad05"></span>
            <a href="Javascript:;" onclick="hideEmpty();" type="reset" class="btn btn-default" data-toggle="collapse" data-target="#slide-education" aria-expanded="true" aria-controls="slide-education">Cancel</a>
          </div>
        </div>
      </div>
    </div><!-- end panel add edu -->

    <!-- start not listed place -->
    <div id="not-listed" class="hide">
      <!-- start not listed school -->
      <div id="not-listed-school">
        <div class="row">
          <div class="col-sm-10 col-xs-12">
            <div class="alert-edu">
              Please input the school name with the city and country. Our team will validate your input
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-12 col-xs-12">
            <div class="form-label"><?=$lang['edu-school'];?></div>
            <input id="edu-title2" type="text" name="" class="form-control no-radius" placeholder="<?=$lang['edu-school_placeholder'];?>">
          </div>
        </div>
      </div><!-- end not listed school -->
    </div>

    <!-- main edu -->
    <div class="panel-group marg0" id="main-edu" role="tablist" aria-multiselectable="true">
    </div> <!-- end main edu -->
    <div class="see-more"><a href="javascript:;" id="see-edu" class="hide">SEE <span id="seeEdu-num"></span> MORE</a></div>
    <div id='load-edu' class='text-center'></div>
    <!-- start edit edu -->
    <div id="edit-edu-place" class="hide">
      <div id="edit-edu">
        <div class="panel-input-edu">
          <div class="row">
            <div class="col-sm-12 col-xs-12">
              <div class="form-label bold"><?=$lang['edu-school'];?> *</div>
            </div>
          </div>
          <!-- start input school -->
          <div class="row">
            <div class="col-xs-12">
              <div id="school-boxEdit" class="school-box">
                <input id="edu-etitle" onfocus="$('#school-listEdit').show();" oninput="get_univ2();$('#school-listEdit').show();" type="text" class="form-control no-radius" placeholder="<?=$lang['edu-school_placeholder'];?>">
                <div id="school-listEdit" class="school-list" onmouseover="$('#school-listEdit').show();">
                  <ul id="school-eul">

                  </ul>
                </div>
              </div>
              <div id="edu-evtitle" class="valid-message"></div>
            </div>
          </div><!-- end input school -->
          <div class="row up1">
            <div class="col-xs-6">
              <div class="form-label bold"><?=$lang['edu-country'];?> *</div>
            </div>
            <div class="col-xs-6">
              <div class="form-label bold"><?=$lang['edu-city'];?> *</div>
            </div>
          </div>
          <div class="row">
            <div class="col-xs-6">
              <input id="edu-ecountry" type="text" class="form-control no-radius" name="country" placeholder="<?=$lang['edu-country_placeholder'];?>">
            </div>
            <div class="col-xs-6">
              <input id="edu-ecity" type="text" class="form-control no-radius" name="city" placeholder="<?=$lang['edu-city_placeholder'];?>">
            </div>
            <div class="col-xs-12">
              <div id="edu-evcountry" class="valid-message"></div>
            </div>
          </div>
          <div class="row up1">
            <div class="col-sm-6 col-xs-12">
              <div class="checkbox">
                <label>
                  <input id="edu-estatus" type="checkbox" onclick="checkStatEdu2();" value="yes"> <?=$lang['edu-attending'];?>
                </label>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-xs-6 up1">
              <div class="form-label bold"><?=$lang['edu-from'];?> *</div>
              <div class="row">
                <div class="col-xs-6" style="padding: 0 5px 0 0;">
                  <select id="edu-efmonth" name="" class="form-control no-radius">
                    <option value="">Month</option>
                    <?php $month = '12';
                    for ($months = '1'; $months <= $month; $months++) { ?>
                    <option value="<?php echo $months; ?>" class="edu-efmonth"><?php echo date("F", strtotime("$months/12/10")); ?></option>
                    <?php } ?>
                  </select>
                </div>
                <div class="col-xs-6" style="padding: 0 0 0 5px;">
                  <select id="edu-efyear" name="" class="form-control no-radius">
                    <option value="" >--</option>
                    <?php $year = '1950';
                    for ($years = date("Y"); $years >= $year; $years--) { ?>
                    <option value="<?php echo $years; ?>" class="edu-efyear"><?php echo $years; ?></option>
                    <?php } ?>
                  </select>
                </div>
              </div>
            </div>
            <div id="to-edustat2" class="col-xs-6 up1">
              <div class="form-label bold"><?=$lang['edu-to'];?> *</div>
              <div class="row">
                <div class="col-xs-6" style="padding: 0 5px 0 0;">
                  <select id="edu-etmonth" name="" class="form-control no-radius">
                    <option value="" class="edu-etmonth">Month</option>
                    <?php $month = '12';
                    for ($months = '1'; $months <= $month; $months++) { ?>
                    <option value="<?php echo $months; ?>" class="edu-etmonth"><?php echo date("F", strtotime("$months/12/10")); ?></option>
                    <?php } ?>
                  </select>
                </div>
                <div class="col-xs-6" style="padding: 0 0 0 5px;">
                  <select id="edu-etyear" name="" class="form-control no-radius">
                    <option value="" class="edu-etyear">--</option>
                    <?php $year = '1950';
                    for ($years = date("Y"); $years >= $year; $years--) { ?>
                    <option value="<?php echo $years; ?>" class="edu-etyear"><?php echo $years; ?></option>
                    <?php } ?>
                  </select>
                </div>
              </div>
            </div>
            <div class="col-xs-12">
              <div id="edu-evdate" class="valid-message"></div>
            </div>
          </div>
          <div class="row">
            <div class="col-xs-12 up1">
              <div class="form-label bold"><?=$lang['edu-degree'];?> *</div>
            </div>
            <div class="col-sm-10 col-xs-12">
              <input id="edu-edegree" type="text" name="" class="form-control no-radius" placeholder="<?=$lang['edu-degree_placeholder'];?>">
              <div id="edu-evdegree" class="valid-message"></div>
            </div>
          </div>
          <div class="row">
            <div class="col-xs-12 up1">
              <div class="form-label bold"><?=$lang['edu-study'];?> *</div>
            </div>
            <div class="col-sm-10 col-xs-12">
              <input id="edu-efield" type="text" name="" class="form-control no-radius" placeholder="<?=$lang['edu-study_placeholder'];?>">
              <div id="edu-evfield" class="valid-message"></div>
            </div>
          </div>
          <div class="row">
            <div class="col-xs-12 up1">
              <div class="form-label"><?=$lang['edu-grade'];?></div>
            </div>
            <div class="col-sm-6 col-xs-12">
              <input id="edu-egrade" type="text" name="" class="form-control no-radius" placeholder="<?=$lang['edu-grade_placeholder'];?>">
              <div id="edu-evgrade" class="valid-message"></div>
            </div>
          </div>
          <div class="row">
            <div class="col-xs-12 up1">
              <div class="form-label"><?=$lang['edu-desc'];?></div>
            </div>
            <div class="col-sm-12 col-xs-12">
              <textarea id="edu-edesc" name="desc" class="form-control no-radius" rows="3" placeholder="<?=$lang['edu-desc_placeholder'];?>"></textarea>
            </div>
          </div>
          <div class="row up1">
            <div class="col-xs-12">
              <input id="edit_edu_old_title" type="hidden">
              <input id="edit_edu_id" type="hidden">
              <input id="edit_edu_ATid" type="hidden">
              <a class="btn btn-info" onclick="update_edu();" >Save</a><span class="pad05"></span>
              <a id="reset_edit_edu" type="reset" class="btn btn-default" onclick="close_edit()">Cancel</a>
              <a href="javascript:;" id="delete-edu" onclick="delete_edu();" class="delete-text">Delete</a>
            </div>
          </div>
        </div>
      </div> 
    </div><!-- end edit edu -->
  </div>

<script type="text/javascript">
// FUNCTION AUTOCOMPLETE SCHOOL 
function get_univ(){
  var q = $("#edu-title").val();
  $("#edu-title").addClass("load-input");
  var url = "<?=$api['edu-autocomplete'];?>"+q;                            
  $.ajax({url: url,success:function(result){
    $("#edu-title").removeClass("load-input");
    $('#school-ul').html(replace_univ(result.data,q));
  }}); 
}
function replace_univ(datas,text){
  var resultHTML='';
  if(datas != null){
    var obj = datas;    
    for(var i=0;i < obj.length;i++){
      if(obj[i].Edu_id != '' && obj[i].Edu_schoolName != ''){
        resultHTML += "<li class='school-li' id='univ-"+obj[i].Edu_id+"' onclick='selectUniv("+obj[i].Edu_id+")' data-sc_image='"+obj[i].Edu_img+"' data-sc_title='"+obj[i].Edu_schoolName+"' data-sc_country='"+obj[i].Edu_schoolCountry+"' data-sc_city='"+obj[i].Edu_schoolCity+"'>";
        if(obj[i].Edu_img != ""){
          resultHTML += "<div class='school-image'><img src='<?=$global['img-url'];?>"+obj[i].Edu_img+"' alt=''></div>";
        }else {
          resultHTML += "<div class='school-image'><img src='<?=$placeholder['img-education'];?>' alt='image'></div>";
        }
        resultHTML += "<div class='school-text'>";
        resultHTML += "<div class='school-title'>"+obj[i].Edu_schoolName+"</div>";
        resultHTML += "<div class='school-place'>"+obj[i].Edu_schoolCountry+", "+obj[i].Edu_schoolCity+"</div>";
        resultHTML += "</div>";
        resultHTML += "</li>";
      }
    }
  } else {
    resultHTML += "<li class='no-list'><div class='no-list-text'><?=$lang['entry-result'];?> \"<span>"+text+"</span>\" <?=$lang['entry-result_text'];?></div></li>";
  }
  return resultHTML;
}
function get_univ2(){
  var q = $("#edu-etitle").val();
  $("#edu-etitle").addClass("load-input");
  var url = "<?=$api['edu-autocomplete'];?>"+q;                            
  $.ajax({url: url,success:function(result){
    $("#edu-etitle").removeClass("load-input");
    $('#school-eul').html(replace_univ2(result.data,q));
  }}); 
}
function replace_univ2(datas,text){
  var resultHTML='';
  if(datas != null){
    var obj = datas;    
    for(var i=0;i < obj.length;i++){
      if(obj[i].Edu_id != '' && obj[i].Edu_schoolName != ''){
        resultHTML += "<li class='school-li' id='unive-"+obj[i].Edu_id+"' onclick='selectUnivEdit("+obj[i].Edu_id+")' data-sc_eimage='"+obj[i].Edu_img+"' data-sc_etitle='"+obj[i].Edu_schoolName+"' data-sc_ecountry='"+obj[i].Edu_schoolCountry+"' data-sc_ecity='"+obj[i].Edu_schoolCity+"'>";
        if(obj[i].Edu_img != ""){
          resultHTML += "<div class='school-image'><img src='<?=$global['img-url'];?>"+obj[i].Edu_img+"' alt=''></div>";
        }else {
          resultHTML += "<div class='school-image'><img src='<?=$placeholder['img-education'];?>' alt='image'></div>";
        }
        resultHTML += "<div class='school-text'>";
        resultHTML += "<div class='school-title'>"+obj[i].Edu_schoolName+"</div>";
        resultHTML += "<div class='school-place'>"+obj[i].Edu_schoolCity+", "+obj[i].Edu_schoolCountry+"</div>";
        resultHTML += "</div>";
        resultHTML += "</li>";
      }
    }
  } else {
    resultHTML += "<li class='no-list'><div class='no-list-text'><?=$lang['entry-result'];?> \"<span>"+text+"</span>\" <?=$lang['entry-result_text'];?></div></li>";
  }
  return resultHTML;
}
function selectUniv(param){
  var title = $('#univ-'+param).data('sc_title');
  var country = $('#univ-'+param).data('sc_country');
  var city = $('#univ-'+param).data('sc_city');

  $("#edu-title").removeClass("load-input");
  $('#edu-title').val(title);
  $('#edu-country').val(country);
  $('#edu-city').val(city);
  $('#school-list').hide();
}
function selectUnivEdit(param){
  var title = $('#unive-'+param).data('sc_etitle');
  var country = $('#unive-'+param).data('sc_ecountry');
  var city = $('#unive-'+param).data('sc_ecity');

  $("#edu-etitle").removeClass("load-input");
  $('#edu-etitle').val(title);
  $('#edu-ecountry').val(country);
  $('#edu-ecity').val(city);
  $('#school-listEdit').hide();
}
$('html').click(function() {
  $('#school-list').hide();
  $("#edu-title").removeClass("load-input");
});
$('#school-box').click(function(event){
  event.stopPropagation();
  $("#edu-title").removeClass("load-input");
});
$('html').click(function() {
  $('#school-listEdit').hide();
  $("#edu-etitle").removeClass("load-input");
});
$('#school-boxEdit').click(function(event){
  event.stopPropagation();
  $("#edu-etitle").removeClass("load-input");
});
// END FUNCTION AUTOCOMPLETE SCHOOL 

// FUNCTION GET DATA EDUCATION
get_edu();
function get_edu(){
  $('#see-edu').addClass('hide');
  $('#load-edu').html("<img src='<?=$global['absolute-url'];?>img/load-blue.gif' style='margin:70px 0' />");
  $('#main-edu').html('');
  var url = "<?=$api['edu-data'];?>&user_id=<?php echo $_SESSION['userData']['id'];?>&auth_code=<?php echo $_SESSION['userData']['auth_code'];?>";                            
  $.ajax({url: url,success:function(result){
    setTimeout(function() {
      $('#load-edu').html('');
      $('#main-edu').html(replace_edu(result.data));
      seeMoreEdu();
    }, 1000)
  }}); 
}
function replace_edu(datas){
  var resultHTML='';resultNext = "";
  var total = 0;previous = "";user_name = "";link="";
  var d = new Date();strip_string ="";
  var current = d.getTime();
  // console.log(datas);
  if(datas != null){
    var obj = datas;    
    for(var i=0;i < obj.length;i++){
      if(obj[i].Edu_ID != '' && obj[i].Edu_schoolName != ''){
        if(obj[i].ATedu_GPA != 0){
          var GPA = obj[i].ATedu_GPA;
        } else {
          var GPA = "";
        }
        resultHTML += "<div class='row row-edu hide hm-edu' role='tab'>";
        resultHTML += "<div class='col-xs-12'>";
        resultHTML += "<a href='#collapse-"+obj[i].Edu_ID+"' aria-controls='collapse-"+obj[i].Edu_ID+"' data-parent='#main-edu' onclick='edit_box_edu("+obj[i].Edu_ID+");' id='edu-"+obj[i].Edu_ID+"' data-edu_atid='"+obj[i].ATedu_ID+"' data-edu_id='"+obj[i].Edu_ID+"'  data-edu_status='"+obj[i].ATedu_status+"' data-edu_country='"+obj[i].Edu_schoolCountry+"' data-edu_city='"+obj[i].Edu_schoolCity+"' data-edu_title='"+obj[i].Edu_schoolName+"' data-edu_fmonth='"+obj[i].ATedu_fromMonth+"' data-edu_fyear='"+obj[i].ATedu_fromYear+"' data-edu_tmonth='"+obj[i].ATedu_toMonth+"' data-edu_tyear='"+obj[i].ATedu_toYear+"' data-edu_degree='"+obj[i].ATedu_degree+"' data-edu_grade='"+GPA+"' data-edu_field='"+obj[i].ATedu_major+"' data-edu_desc='"+obj[i].ATedu_desc+"' data-toggle='collapse' aria-expanded='false' class='education-box'>";
        resultHTML += "<div class='row'>";
        resultHTML += "<div class='col-md-10 col-xs-9 pad0'>";
        resultHTML += "<div class='education-school'>";                 
        resultHTML += obj[i].Edu_schoolName+" ";
        resultHTML += "<span class='edu-etext edit-button'><i class='glyphicon glyphicon-edit'></i> <span class='hidden-xs'>Edit</span></span>";
        resultHTML += "</div>";
        resultHTML += "<div class='education-location hidden-xs'>"+obj[i].Edu_schoolCity+", "+obj[i].Edu_schoolCountry+"</div>";
        resultHTML += "<div class='education-location'>"+obj[i].ATedu_degree+" in "+obj[i].ATedu_major+"</div>";
        if(obj[i].ATedu_GPA != '' && obj[i].ATedu_GPA != 0){
          resultHTML += "<div class='education-location'>GPA: "+obj[i].ATedu_GPA+"</div>";
        }
        if(obj[i].ATedu_toYear == "" || obj[i].ATedu_toYear == 0){
          var toYear = "Present";
        } else {
          var toYear = obj[i].ATedu_toYear;
        }
        resultHTML += "<div class='education-year-mobile visible-xs'>"+obj[i].ATedu_fromYear+" - "+toYear+"</div>";
        resultHTML += "</div>";
        resultHTML += "<div class='col-md-2 col-xs-3'>";
        if(obj[i].Edu_img != ''){
          resultHTML += "<div class='education-image'><img src='<?=$global['img-url'];?>"+obj[i].Edu_img+"' alt='"+obj[i].Edu_schoolName+"'></div>";
        }else {
          resultHTML += "<div class='education-image'><img src='<?=$placeholder['img-education'];?>' alt='image'></div>";
        }
        resultHTML += "<div class='education-year hidden-xs'>"+obj[i].ATedu_fromYear+" - "+toYear+"</div>";
        resultHTML += "</div>";
        resultHTML += "</div>";
        resultHTML += "</a>";
        resultHTML += "<div id='load-edu-"+obj[i].Edu_ID+"' class='text-center'></div>";
        resultHTML += "<a id='close-edu-"+obj[i].Edu_ID+"' href='#collapse-"+obj[i].Edu_ID+"' aria-controls='collapse-"+obj[i].Edu_ID+"' data-parent='#main-edu' data-toggle='collapse' aria-expanded='false' class='hide'></a>";
        resultHTML += "</div>";
        resultHTML += "</div>";
        resultHTML += "<div id='collapse-"+obj[i].Edu_ID+"' aria-labelledby='edu-"+obj[i].Edu_ID+"' class='panel-collapse collapse collapse-education pad0' role='tabpanel'>";
        resultHTML += "</div>";
      }
    }
  } else {
    resultHTML += "<div id='empty-edu' class='empty-file'><?=$lang['edu-placeholder'];?></div>";
  }
  return resultHTML;
};
function seeMoreEdu(){
    size = $(".row-edu").size();
    x=2;

    $('.row-edu:lt('+x+')').removeClass('hide');
    $('.row-edu:lt('+x+')').removeClass('hm-edu'); 
    size_left= $(".hm-edu").size();
    if(size > 2){
      $('#see-edu').removeClass('hide');
      $("#seeEdu-num").text('('+size_left+')');
    } else {
      $('#see-edu').addClass('hide');
    }

    $('#see-edu').on('click', function(){
        $('.row-edu').removeClass('hide');
        $('.row-edu').removeClass('hm-edu');
        $('#see-edu').addClass('hide');
    });
};
// END FUNCTION GET DATA EDUCATION

// FUNCTION VALIDATION SAVE AND UPDATE
function validEdu(){
  var edu_title = $("#edu-title").val();
  var edu_degree = $("#edu-degree").val();
  var edu_grade = $("#edu-grade").val();
  var edu_field = $("#edu-field").val();
  var edu_country = $("#edu-country").val();
  var edu_city = $("#edu-city").val();
  var edu_desc = $("#edu-desc").val();
  var edu_fmonth = $("#edu-fmonth").val();
  var edu_fyear = $("#edu-fyear").val();
  var edu_tmonth = $("#edu-tmonth").val();
  var edu_tyear = $("#edu-tyear").val();
  var grade_format = /^(\d+(?:[\.\,]\d{1,2})?)$/;
  var edu_status = document.getElementById('edu-status');
  var message = "";

  if(edu_title != ""){
    if(edu_title.length >= 3){
      message = "";
      $("#edu-title").removeClass('input-error');
      $("#edu-vtitle").text(message);
    } else {
      message = "School Name has to be at least 3 characters!";
      $("#edu-title").addClass('input-error');
      $("#edu-title").focus();
      $("#edu-vtitle").text(message);
      return false;
    }
  } else {
    message = "Please input or select your school name!";
    $("#edu-title").addClass('input-error');
    $("#edu-title").focus();
    $("#edu-vtitle").text(message);
    return false;
  }
  if(edu_country != ""){
    message = "";
    $("#edu-country").removeClass('input-error');
    $("#edu-vcountry").text(message);
  } else {
    message = "Please input Country of School!";
    $("#edu-country").addClass('input-error');
    $("#edu-country").focus();
    $("#edu-vcountry").text(message);
    return false;
  }
  if(edu_city != ""){
    message = "";
    $("#edu-city").removeClass('input-error');
    $("#edu-vcountry").text(message);
  } else {
    message = "Please input City of School!";
    $("#edu-city").addClass('input-error');
    $("#edu-city").focus();
    $("#edu-vcountry").text(message);
    return false;
  }
  if (edu_status.checked) {
    if(edu_fmonth != ""){
      message = "";
      $("#edu-fmonth").removeClass('input-error');
      $("#edu-vdate").text(message);
    } else {
      message = "Start Month cannot be empty!";
      $("#edu-fmonth").addClass('input-error');
      $("#edu-fmonth").focus();
      $("#edu-vdate").text(message);
      return false;
    }
    if(edu_fyear != ""){
      message = "";
      $("#edu-fyear").removeClass('input-error');
      $("#edu-vdate").text(message);
    } else {
      message = "Start Year cannot be empty!";
      $("#edu-fyear").addClass('input-error');
      $("#edu-fyear").focus();
      $("#edu-vdate").text(message);
      return false;
    }
  } else {
    if(edu_fmonth != ""){
      message = "";
      $("#edu-fmonth").removeClass('input-error');
      $("#edu-vdate").text(message);
    } else {
      message = "Start Month cannot be empty!";
      $("#edu-fmonth").addClass('input-error');
      $("#edu-fmonth").focus();
      $("#edu-vdate").text(message);
      return false;
    }
    if(edu_fyear != ""){
      message = "";
      $("#edu-fyear").removeClass('input-error');
      $("#edu-vdate").text(message);
    } else {
      message = "Start Year cannot be empty!";
      $("#edu-fyear").addClass('input-error');
      $("#edu-fyear").focus();
      $("#edu-vdate").text(message);
      return false;
    }
    if(edu_tmonth != ""){
      if(edu_tyear == edu_fyear){
        if(eval(edu_tmonth) >= eval(edu_fmonth)){
          message = "";
          $("#edu-tmonth").removeClass('input-error');
          $("#edu-vdate").text(message);
        } else {
          message = "End Date has to be later than Start Date!";
          $("#edu-tmonth").addClass('input-error');
          $("#edu-tmonth").focus();
          $("#edu-vdate").text(message);
          return false;
        }
      } else {
        message = "";
        $("#edu-tmonth").removeClass('input-error');
        $("#edu-vdate").text(message);
      }
    } else {
      message = "End Month cannot be empty!";
      $("#edu-tmonth").addClass('input-error');
      $("#edu-tmonth").focus();
      $("#edu-vdate").text(message);
      return false;
    }
    if(edu_tyear != ""){
      if(edu_tyear >= edu_fyear){
        message = "";
        $("#edu-tyear").removeClass('input-error');
        $("#edu-vdate").text(message);
      } else {
        message = "End Date has to be later than Start Date!";
        $("#edu-tyear").addClass('input-error');
        $("#edu-tyear").focus();
        $("#edu-vdate").text(message);
        return false;
      } 
    } else {
      message = "End Year cannot be empty!";
      $("#edu-tyear").addClass('input-error');
      $("#edu-tyear").focus();
      $("#edu-vdate").text(message);
      return false;
    }
  }
  if(edu_degree != ""){
    message = "";
    $("#edu-degree").removeClass('input-error');
    $("#edu-vdegree").text(message);
  } else {
    message = "Please input the degree!";
    $("#edu-degree").addClass('input-error');
    $("#edu-degree").focus();
    $("#edu-vdegree").text(message);
    return false;
  }
  if(edu_field != ""){
    message = "";
    $("#edu-field").removeClass('input-error');
    $("#edu-vfield").text(message);
  } else {
    message = "Please input Field of Study!";
    $("#edu-field").addClass('input-error');
    $("#edu-field").focus();
    $("#edu-vfield").text(message);
    return false;
  }
  if(edu_grade != ""){
    if(edu_grade.match(grade_format)) {
      if(edu_grade != 0){
        message = "";
        $("#edu-grade").removeClass('input-error');
        $("#edu-vgrade").text(message);
      } else {
        message = "Grade has to be more than 0!";
        $("#edu-grade").addClass('input-error');
        $("#edu-grade").focus();
        $("#edu-vgrade").text(message);
        return false;
      }
    } else {
      message = "Grade has to be in number!";
      $("#edu-grade").addClass('input-error');
      $("#edu-grade").focus();
      $("#edu-vgrade").text(message);
      return false;
    }
  }
  return true;
}
function validEduEdit(){
  var edu_title = $("#edu-etitle").val();
  var edu_degree = $("#edu-edegree").val();
  var edu_grade = $("#edu-egrade").val();
  var edu_field = $("#edu-efield").val();
  var edu_country = $("#edu-ecountry").val();
  var edu_city = $("#edu-ecity").val();
  var edu_desc = $("#edu-edesc").val();
  var edu_fmonth = $("#edu-efmonth").val();
  var edu_fyear = $("#edu-efyear").val();
  var edu_tmonth = $("#edu-etmonth").val();
  var edu_tyear = $("#edu-etyear").val();
  var grade_format = /^(\d+(?:[\.\,]\d{1,2})?)$/;
  var edu_status = document.getElementById('edu-estatus');
  var message = "";

  if(edu_title != ""){
    if(edu_title.length >= 3){
      message = "";
      $("#edu-etitle").removeClass('input-error');
      $("#edu-evtitle").text(message);
    } else {
      message = "School Name has to be at least 3 characters!";
      $("#edu-etitle").addClass('input-error');
      $("#edu-etitle").focus();
      $("#edu-evtitle").text(message);
      return false;
    }
  } else {
    message = "Please input or select your school name!";
    $("#edu-etitle").addClass('input-error');
    $("#edu-etitle").focus();
    $("#edu-evtitle").text(message);
    return false;
  }
  if(edu_country != ""){
    message = "";
    $("#edu-ecountry").removeClass('input-error');
    $("#edu-evcountry").text(message);
  } else {
    message = "Please input Country of School!";
    $("#edu-ecountry").addClass('input-error');
    $("#edu-ecountry").focus();
    $("#edu-evcountry").text(message);
    return false;
  }
  if(edu_city != ""){
    message = "";
    $("#edu-ecity").removeClass('input-error');
    $("#edu-evcountry").text(message);
  } else {
    message = "Please input City of School!";
    $("#edu-ecity").addClass('input-error');
    $("#edu-ecity").focus();
    $("#edu-evcountry").text(message);
    return false;
  }
  if (edu_status.checked) {
    if(edu_fmonth != ""){
      message = "";
      $("#edu-efmonth").removeClass('input-error');
      $("#edu-evdate").text(message);
    } else {
      message = "Start Month cannot be empty!";
      $("#edu-efmonth").addClass('input-error');
      $("#edu-efmonth").focus();
      $("#edu-evdate").text(message);
      return false;
    }
    if(edu_fyear != ""){
      message = "";
      $("#edu-efyear").removeClass('input-error');
      $("#edu-evdate").text(message);
    } else {
      message = "Start Year cannot be empty!";
      $("#edu-efyear").addClass('input-error');
      $("#edu-efyear").focus();
      $("#edu-evdate").text(message);
      return false;
    }
  } else {
    if(edu_fmonth != ""){
      message = "";
      $("#edu-efmonth").removeClass('input-error');
      $("#edu-evdate").text(message);
    } else {
      message = "Start Month cannot be empty!";
      $("#edu-efmonth").addClass('input-error');
      $("#edu-efmonth").focus();
      $("#edu-evdate").text(message);
      return false;
    }
    if(edu_fyear != ""){
      message = "";
      $("#edu-efyear").removeClass('input-error');
      $("#edu-evdate").text(message);
    } else {
      message = "Start Year cannot be empty!";
      $("#edu-efyear").addClass('input-error');
      $("#edu-efyear").focus();
      $("#edu-evdate").text(message);
      return false;
    }
    if(edu_tmonth != ""){
      if(edu_tyear == edu_fyear){
        if(eval(edu_tmonth) >= eval(edu_fmonth)){
          message = "";
          $("#edu-etmonth").removeClass('input-error');
          $("#edu-evdate").text(message);
        } else {
          message = "End Date has to be later than Start Date!";
          $("#edu-etmonth").addClass('input-error');
          $("#edu-etmonth").focus();
          $("#edu-evdate").text(message);
          return false;
        }
      } else {
        message = "";
        $("#edu-etmonth").removeClass('input-error');
        $("#edu-evdate").text(message);
      }
    } else {
      message = "End Month cannot be empty!";
      $("#edu-etmonth").addClass('input-error');
      $("#edu-etmonth").focus();
      $("#edu-evdate").text(message);
      return false;
    }
    if(edu_tyear != ""){
      if(edu_tyear >= edu_fyear){
        message = "";
        $("#edu-etyear").removeClass('input-error');
        $("#edu-evdate").text(message);
      } else {
        message = "End Date has to be later than Start Date!";
        $("#edu-etyear").addClass('input-error');
        $("#edu-etyear").focus();
        $("#edu-evdate").text(message);
        return false;
      } 
    } else {
      message = "End Year cannot be empty!";
      $("#edu-etyear").addClass('input-error');
      $("#edu-etyear").focus();
      $("#edu-evdate").text(message);
      return false;
    }
  }
  if(edu_degree != ""){
    message = "";
    $("#edu-edegree").removeClass('input-error');
    $("#edu-evdegree").text(message);
  } else {
    message = "Please input the degree!";
    $("#edu-edegree").addClass('input-error');
    $("#edu-edegree").focus();
    $("#edu-evdegree").text(message);
    return false;
  }
  if(edu_field != ""){
    message = "";
    $("#edu-efield").removeClass('input-error');
    $("#edu-evfield").text(message);
  } else {
    message = "Please input Field of Study!";
    $("#edu-efield").addClass('input-error');
    $("#edu-efield").focus();
    $("#edu-evfield").text(message);
    return false;
  }
  if(edu_grade != ""){
    if(edu_grade.match(grade_format)) {
      if(edu_grade != 0){
        message = "";
        $("#edu-egrade").removeClass('input-error');
        $("#edu-evgrade").text(message);
      } else {
        message = "Grade has to be more than 0!";
        $("#edu-egrade").addClass('input-error');
        $("#edu-egrade").focus();
        $("#edu-evgrade").text(message);
        return false;
      }
    } else {
      message = "Grade has to be in number!";
      $("#edu-egrade").addClass('input-error');
      $("#edu-egrade").focus();
      $("#edu-evgrade").text(message);
      return false;
    }
  }
  return true;
}
//END VALIDATION SAVE AND UPDATE

// FUNCTION SAVE, UPDATE, DELETE
function save_edu(){
  if ( validEdu() ){
    $('#edit-edu').detach().appendTo('#edit-edu-place');
    $('html, body').animate({
      scrollTop: $('#education-header').offset().top-80
    }, 'fast');
    //$('#load-edu').html('<img src="img/load-blue.gif" style="margin:70px 0" />');

    var url = "<?=$api['edu-update'];?>";
    var edu_title = $("#edu-title").val();
    var edu_degree = $("#edu-degree").val();
    var edu_grade = $("#edu-grade").val();
    var edu_field = $("#edu-field").val();
    var edu_country = $("#edu-country").val();
    var edu_city = $("#edu-city").val();
    var edu_desc = $("#edu-desc").val();
    var edu_fmonth = $("#edu-fmonth").val();
    var edu_fyear = $("#edu-fyear").val();
    var edu_tmonth = $("#edu-tmonth").val();
    var edu_tyear = $("#edu-tyear").val();
    var edu_status = document.getElementById('edu-status');
    if (edu_status.checked) {
      edu_status = 'yes';
    } else {
      edu_status = 'no';
    }
    var data = {
      user_id : "<?=$_SESSION['userData']['id'];?>",
      auth_code : "<?=$_SESSION['userData']['auth_code'];?>",
      name : edu_title,
      city : edu_city,
      country : edu_country,
      degree : edu_degree,
      major : edu_field,
      from_month : edu_fmonth,
      from_year : edu_fyear,
      to_month : edu_tmonth,
      to_year : edu_tyear,
      gpa : edu_grade,
      desc : edu_desc,
      status : edu_status
    }
    $.ajax({url: url,data : data, success:function(result){
      $('#slide-education').collapse('hide');
      emptyAddEdu();
      get_edu();
      console.log(result);
    }});
  }
}
function update_edu(){
  if( validEduEdit()){
    $('#edit-edu').detach().appendTo('#edit-edu-place');
    var idu = $('#edit_edu_id').val();
    $('#close-edu-'+idu).click();
    $('html, body').animate({
    scrollTop: $('#education-header').offset().top-80
    }, 'fast');
    //$('#load-edu-'+idu).html('<img src="img/load-blue.gif" class="marg1" />');

    var url = "<?=$api['edu-update'];?>";
    var edu_id = $("#edit_edu_ATid").val();
    var edu_title = $("#edu-etitle").val();
    var edu_degree = $("#edu-edegree").val();
    var edu_grade = $("#edu-egrade").val();
    var edu_field = $("#edu-efield").val();
    var edu_country = $("#edu-ecountry").val();
    var edu_city = $("#edu-ecity").val();
    var edu_desc = $("#edu-edesc").val();
    var edu_fmonth = $("#edu-efmonth").val();
    var edu_fyear = $("#edu-efyear").val();
    var edu_tmonth = $("#edu-etmonth").val();
    var edu_tyear = $("#edu-etyear").val();
    var edu_status = document.getElementById('edu-estatus');
    if (edu_status.checked) {
      edu_status = 'yes';
    } else {
      edu_status = 'no';
    }
    var data = {
      user_id : "<?=$_SESSION['userData']['id'];?>",
      auth_code : "<?=$_SESSION['userData']['auth_code'];?>",
      id : edu_id,
      name : edu_title,
      city : edu_city,
      country : edu_country,
      degree : edu_degree,
      major : edu_field,
      from_month : edu_fmonth,
      from_year : edu_fyear,
      to_month : edu_tmonth,
      to_year : edu_tyear,
      gpa : edu_grade,
      desc : edu_desc,
      status : edu_status
    }
    $.ajax({url: url,data : data, success:function(result){
      get_edu();
      console.log(result);
    }});
  }
}
function delete_edu(){
  if (confirm("Are you sure to delete this information ?")) {
    $('#edit-edu').detach().appendTo('#edit-edu-place');
    $('html, body').animate({
      scrollTop: $('#education-header').offset().top-80
    }, 'fast');
    
    var url = "<?=$api['edu-delete'];?>";
    var edu_id = $("#edit_edu_ATid").val();
    var data = {
      user_id : "<?=$_SESSION['userData']['id'];?>",
      auth_code : "<?=$_SESSION['userData']['auth_code'];?>",
      id : edu_id
    }
    $.ajax({url: url,data : data, success:function(result){
      get_edu();
      console.log(result);
    }});
  }
}
// END FUNCTION SAVE, UPDATE, DELETE

// OTHER FUNCTION
function checkStatEdu() {
  var s = document.getElementById('edu-status');
  if (s.checked) {
    if($("#to-edustat").hasClass("hide")){
      //nothing happen
    } else {
      $("#to-edustat").addClass("hide");
      $('option[value=""].edu-tmonth').attr("selected", true);
      $('option[value=""].edu-tyear').attr("selected", true);
    }
  } else {
    if($("#to-edustat").hasClass("hide")){
      $("#to-edustat").removeClass("hide");
    }
  }
}
function checkStatEdu2() {
  var s = document.getElementById('edu-estatus');
  if (s.checked) {
    if($("#to-edustat2").hasClass("hide")){
      //nothing happen
    } else {
      $("#to-edustat2").addClass("hide");
      $('option[value=""].edu-etmonth').attr("selected", true);
      $('option[value=""].edu-etyear').attr("selected", true);
    }
  } else {
    if($("#to-edustat2").hasClass("hide")){
      $("#to-edustat2").removeClass("hide");
    }
  }
}
function close_edit(){
  var id = $('#edit_edu_id').val();
  $('#close-edu-'+id+'').click();
  $('#edu-'+id).removeClass('hide-edu');
}
function hideEmpty(){
  var myElem = document.getElementById('empty-edu');
  if(myElem != null){
    $('#empty-edu').toggle();
  }
  var id = $('#edit_edu_id').val();
  if (id != ''){
    if ($('#collapse-'+id).hasClass('in')){
      $('#close-edu-'+id+'').click();
      $('#edu-'+id).removeClass('hide-edu');
    }
  }
  emptyAddEdu();
}
function emptyAddEdu(){
  $("#edu-title").val('');
  $("#edu-degree").val('');
  $("#edu-grade").val('');
  $("#edu-field").val('');
  $("#edu-country").val('');
  $("#edu-city").val('');
  $("#edu-desc").val('');
  $("#edu-fmonth").val('');
  $("#edu-fyear").val('');
  $("#edu-tmonth").val('');
  $("#edu-tyear").val('');
  document.getElementById("edu-status").checked = false;
  $("#to-edustat").removeClass("hide");
}
function edit_box_edu(param){
  if($('#slide-education').hasClass('in')){
    $('#slide-education').collapse('hide');
  }
  if($('.education-box').hasClass('hide-edu')){
    $('.education-box').removeClass('hide-edu');
    if($('.collapse-education').hasClass('in')){
      $('.collapse-education').removeClass('in')
    }
    $('#edu-'+param).addClass('hide-edu');
  } else {
    $('#edu-'+param).addClass('hide-edu');
  }
  $('#edit-edu').detach().appendTo('#collapse-'+param+'');
  var id = $('#edu-'+param).data('edu_id');
  var ATid = $('#edu-'+param).data('edu_atid');
  var title = $('#edu-'+param).data('edu_title');
  var degree = $('#edu-'+param).data('edu_degree');
  var field = $('#edu-'+param).data('edu_field');
  var grade = $('#edu-'+param).data('edu_grade');
  var country = $('#edu-'+param).data('edu_country');
  var city = $('#edu-'+param).data('edu_city');
  var desc = $('#edu-'+param).data('edu_desc');
  var fmonth = $('#edu-'+param).data('edu_fmonth');
  var fyear = $('#edu-'+param).data('edu_fyear');
  var tmonth = $('#edu-'+param).data('edu_tmonth');
  var tyear = $('#edu-'+param).data('edu_tyear');
  var status = $('#edu-'+param).data('edu_status');

  $('#reset_edit_edu').data("target") === "#collapse";
  $('#edu-etitle').val(title);
  $('#edu-edegree').val(degree);
  $('#edu-efield').val(field);
  $('#edu-egrade').val(grade);
  $('#edu-ecountry').val(country);
  $('#edu-ecity').val(city);
  $('#edu-edesc').val(desc);
  $('#edit_edu_old_title').val(title);
  $('#edit_edu_id').val(id);
  $('#edit_edu_ATid').val(ATid);
  $('option[value="' + fmonth + '"].edu-efmonth').attr("selected", true);
  $('option[value="' + fyear + '"].edu-efyear').attr("selected", true);
  if(status == 'yes') {
    document.getElementById("edu-estatus").checked = true;
  } else {
    document.getElementById("edu-estatus").checked = false;
    $('option[value="' + tmonth + '"].edu-etmonth').attr("selected", true);
    $('option[value="' + tyear + '"].edu-etyear').attr("selected", true);
  }
  checkStatEdu2();
}
function openSchool(){
  $('#listed-school').detach().appendTo('#not-listed');
  $('#not-listed-school').detach().appendTo('#listed-place')
}
// END OTHER FUNCTION
</script>
</div> <!-- end personal edu section -->