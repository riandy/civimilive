<?php 
include("packages/require.php");
include("controller/controller_unsubscribe.php");
$curpage='unsubscribe';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title><?=$global['title-unsubscribe'];?></title>
    <meta name="keywords" content="">
    <meta name="description" content="">
    <?php include("packages/head.php");?>
</head>
<body>
    <div id="all">
        <!-- start top nav -->
        <?php include("section-top-nav.php");?>
        <!-- end top nav -->
        <div class="register-section" style="background: url('<?=$global['base'];?>admin/img/pattern/pattern.png');">
            <div class="register-wrapper" style="background-color: transparent;">
                <div class="container container-max">
                    <div style="margin-top: 10%;">
                        <div class="visible-xs" style="margin-top: 45%;"></div>
                        <div class="row up5">
                            <div class="col-xs-12">
                                <div class="fpersonal-header"><?=$lang['unsub-head'];?></div>
                                <div align="center" style="color: <?=$color;?>;font-weight: bold;font-size: 13px;margin: 3px 0;"><?php echo $message;?></div>
                                <div class="fpersonal-info"><?=$lang['unsub-info'];?></div>
                            </div>
                        </div>
                        <form name="formPersonal" action="<?=$path['unsubscribe-action'];?>" method="post" enctype="multipart/form-data" onsubmit="return personalValid()" style="max-width: 500px;margin: 0 auto;">
                            <div class="row up15">
                                <div class="col-sm-12 col-xs-12"><input id="email" type="email" name="email" class="form-control no-radius" placeholder="<?=$lang['unsub-email'];?>" value="<?php echo $email;?>" /></div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12"><div id="alert-personal" class="control-alert text-center up1"></div></div>
                            </div>
                            <div class="row up2">
                                <div class="col-xs-12 text-center">
                                    <button type="submit" class="btn btn-info btn-fpersonal"><?=$lang['unsub-submit'];?></button>
                                </div>
                            </div>
                        </form>
                        <div class="up3"></div>
                    </div>
                </div>
            </div>
        </div>
        <!-- start footer section -->
        <?php include("section-footer.php");?>
        <!-- end footer section -->
    </div><!--  end all div -->
    <script type="text/javascript">
        function add_zindex(){
            $('.ui-autocomplete').css("z-index", 1055);
        }

        function personalValid(){
            var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
            var email = $("#email").val();
    
            // validation email
            if(email != ""){
                if(email.match(mailformat)) {
                    $("#location").removeClass('input-error');
                    $("#alert-personal").text("");
                    $("#alert-personal").hide();
                } else {  
                    $("#location").addClass('input-error');
                    $("#email").focus();
                    $("#alert-personal").text("your email is not correct");
                    $("#alert-personal").show();
                    return false;  
                }
            } else {
                $("#location").addClass('input-error');
                $("#email").focus();
                $("#alert-personal").text("insert your email address");
                $("#alert-personal").show();
                return false;
            } //end validation email
            return true;
        }
    </script>
  <script src="<?php echo $global['absolute-url'];?>js/function.js"></script>
<script src="<?=$global['absolute-url-admin'];?>js/globalJS.js" type="text/javascript"></script>
</body>
</html>
