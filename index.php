<?php 
include("packages/civ_require.php");
include("controller/controller_civ_global.php");
include("controller/controller_civ_home.php");
$curpage='home';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title><?=$seo['title-home'];?></title>
    <meta name="keywords" content="<?=$seo['keyword-home'];?>">
    <meta name="description" content="<?=$seo['desc-home'];?>">
    <?php include("packages/civ_head.php");?>
    <link rel="stylesheet" href="<?=$global['head-url'];?>css/swiper.min.css"/>
    <script src="<?=$global['head-url'];?>js/swiper.min.js"></script>
</head>
<body>
    <!-- start section header -->
    <?php include("parts/part-home_header.php");?>
    <!-- end section header -->

    <div class="section-thumb hidden-xs">
        <div class="thumb-list">
            <a href="#profile-section" class="thumb-link">
                <div class="thumb-img">
                    <img src="<?=$global['img-url'];?>img/profile-icon.svg" alt="image">
                </div>
                <div class="thumb-text">Profile</div>
            </a>
            <a href="#cv-section" class="thumb-link">
                <div class="thumb-img">
                    <img src="<?=$global['img-url'];?>img/cv-icon.svg" alt="image">
                </div>
                <div class="thumb-text">CV/Resume</div>
            </a>
            <a href="#work-section" class="thumb-link">
                <div class="thumb-img">
                    <img src="<?=$global['img-url'];?>img/image-icon.svg" alt="image">
                </div>
                <div class="thumb-text">Image & Video</div>
            </a>
            <a href="#document-section" class="thumb-link">
                <div class="thumb-img">
                    <img src="<?=$global['img-url'];?>img/doc-icon.svg" alt="image">
                </div>
                <div class="thumb-text">Document</div>
            </a>
        </div>
    </div>

    <div class="section-content">
        <div id="profile-section" class="first-part">
            <div class="fp-content">
                <div class="head-part"><?=$lang['home-ps_head'];?></div>
                <div class="head-image visible-xs" style="padding:0 30px;"><img src="<?=$global['img-url'];?>img/imagexs-2.png" alt="Beautiful online profile"></div>
                <div class="fp-desc">
                    <?=$lang['home-ps_desc'];?>
                </div>
                <a href="http://www.civimi.com/profile/kelvingani.cvm" class="fp-link"><?=$lang['home-ps_link'];?></a>
            </div>
        </div>
        <div id="cv-section" class="second-part">
            <div class="second-content">
                <div class="head-part"><?=$lang['home-cvs_head'];?></div>
                <div class="head-image visible-xs"><img src="<?=$global['img-url'];?>img/imagexs-3.png" alt="Anywhere CV / Resume"></div>
                <div class="sp-desc">
                    <?=$lang['home-cvs_desc'];?>
                </div>
                <a href="http://www.civimi.com/resume/billygani.cvm" class="fp-link"><?=$lang['home-cvs_link'];?></a>
            </div>
        </div>
        <div id="work-section" class="third-part">
            <div class="third-content">
                <div class="head-part"><?=$lang['home-ws_head'];?></div>
                <div class="head-image visible-xs"><img src="<?=$global['img-url'];?>img/imagexs-4.png" alt="Showcase your work"></div>
                <div class="sp-desc">
                    <?=$lang['home-ws_desc'];?>
                </div>
                <a href="http://www.civimi.com/portfolio/image/kelvingani.cvm" class="fp-link"><?=$lang['home-ws_link'];?></a>
            </div>
        </div>
        <div id="document-section" class="fourth-part">
            <div class="fourth-content">
                <div class="head-part"><?=$lang['home-ds_head'];?></div>
                <div class="head-image visible-xs"><img src="<?=$global['img-url'];?>img/imagexs-5.png" alt="Come on writers!"></div>
                <div class="sp-desc">
                    <?=$lang['home-ds_desc'];?>
                </div>
                <a href="http://www.civimi.com/portfolio/doc/kelvingani.cvm" class="fp-link"><?=$lang['home-ds_link'];?></a>
            </div>
        </div>
    </div>

    <!-- START PART FOOTER -->
    <?php include("parts/part-home_footer.php");?>
    <!-- END PART FOOTER -->

    
    <script type="text/javascript">
        function changeLang(){
            var sub = $("#select-lang").val();
            var link = "<?php echo 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];?>";
            jQuery.ajax({
            url: "<?php echo $global['head-url'];?>packages/setlanguage.php",
            type: "POST",
            data: {
                lang: sub,
            },
            dataType : 'json',
            success: function(data, textStatus, xhr) {
                console.log(data); // do with data e.g success message
                window.location.href = link;
            }
            });
        }
        function changeMobLang(){
            var sub = $("#mob-lang").val();
            var link = "<?php echo 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];?>";
            jQuery.ajax({
                url: "<?php echo $global['head-url'];?>packages/setlanguage.php",
                type: "POST",
                data: {
                    lang: sub,
                },
                dataType : 'json',
                success: function(data, textStatus, xhr) {
                    console.log(data); // do with data e.g success message
                    window.location.href = link;
                }
            });
        }
        $("#bar-toggle").click(function(){
            $("#bar-menu").slideToggle();
            if($("#bar-toggle").hasClass("in")){
                $("#bar-toggle").removeClass("in");
                $("#bar-img").attr("src","<?=$global['img-url'];?>img/cancel-icon.png");
            } else {
                $("#bar-toggle").addClass("in");
                $("#bar-img").attr("src","<?=$global['img-url'];?>img/bar-icon.png");
            }
        });
        $('.thumb-link').click(function(){
            $('html, body').animate({
                scrollTop: $( $.attr(this, 'href') ).offset().top
            }, 500);
            return false;
        });
        var swiper = new Swiper('#slide-screen', {
            autoplay:5000,
            speed : 1000,
            loop:true,
            variableWidth: true,
            grabCursor: false,
            scrollbarDraggable: false,
            simulateTouch : false,
            effect: 'fade'
        });
    </script>
</body>
</html>
