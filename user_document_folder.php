<?php 
include("packages/require.php");
include("controller/controller_global.php");
include("controller/controller_user_document.php");

$curpage='user_document';
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title><?=$_SESSION['userData']['username']."'s ".$data_document[0]['DocCat_name']." - ".$global['title-portfolio_doc'];?></title>
  <meta name="keywords" content="">
  <meta name="description" content="">
  <meta name="robots" content="<?=$seo['robot_no'];?>">
  <?php include("packages/head.php");?>
  <link rel="stylesheet" href="<?php echo $global['absolute-url'];?>css/dropzone.css"/>
  <script src="<?php echo $global['absolute-url'];?>js/dropzone.js"></script>
  <!-- Add fancyBox main JS and CSS files -->
  <link rel="stylesheet" type="text/css" href="<?php echo $global['absolute-url'];?>js/fbox/source/jquery.fancybox.css?v=2.1.5" media="screen" />
</head>
<body>
  <div id="all">
    <!-- start top nav -->
    <?php include("section-top-nav.php");?>
    <!-- end top nav -->
    <div id="user-section" class="container-section">
    
    <div class="container">
      <div id="profile-content">
        <div class="up5 visible-xs"></div>
        <!-- start fixed mobile nav -->
        <?php $header_content = "Document";  include("user_part-mobile-nav.php");?>
        <!-- end fixed mobile nav-->
        <div class="row">
        <div class="up5 visible-xs"></div>
          <!-- profile sidebar -->
          <div class="col-sm-3 col-xs-12">
            <!-- start sidebar -->
            <?php include("user_part-sidebar.php");?>
            <!-- end sidebar -->
          </div><!-- profile sidebar -->
          <div class="col-sm-9 col-xs-12 xs-pad0">
              <div id="personal-portfolio-section" class="job-sbox">
                <div id="education-header" class="user-section-header row border-bottom-eee">
                  <div class="col-xs-8">
                    <div class="profile-title hidden-xs" style="font-size:14px;"><i class="glyphicon glyphicon-file"></i>&nbsp;&nbsp;
                      <a href="<?=$path['user-doc'];?>" class="module-breadcrumb">Document</a> >
                      <span id="folder-nav" class="module-breadcrumb"><?=$data_document[0]['DocCat_name'];?></span>
                    </div>
                    <div class="profile-title visible-xs" style="font-size:14px;"><i class="glyphicon glyphicon-file"></i>&nbsp;&nbsp;
                      <a id="folder-back" href="<?=$path['user-document'];?>" class="module-breadcrumb">< Back</a>
                    </div>
                  </div>
                  <div class="col-xs-4">
                      <div class="edit-button text-right">
                        <a href="javascript:;" class="edit-portfolio" title="show edit folder" data-toggle='modal' data-target='#modal-folder'><i class="glyphicon glyphicon-pencil"></i></a>
                      </div>
                  </div>
                </div>
                <!-- <div class="add-photo-bar">
                  <a href="#modal-doc" class="add-photo-portfolio" data-toggle='modal' data-target='#modal-doc'><i class="glyphicon glyphicon-file"></i> Add Document</a>
                </div> -->
                <div id="portfolio-profile">
                  <div id="doc-list" class="row" style="padding-bottom: 20px;">
                  </div>
                </div>
              </div>
          </div>
      </div>
    </div>
            
    </div>
    <!-- end center content -->
  </div>
    <!-- start footer section -->
    <?php include("section-footer.php");?>
    <!-- end footer section -->
  </div><!--  end all div -->
  <!-- Modal Edit Folder -->
  <div class="modal fade" id="modal-folder" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content" style="border-radius:0;">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="font-size: 35px;position: absolute;right: 10px;top: 3px;"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel" style="text-align: center;font-size: 20px;font-weight: bold;color: #717171;">Edit Folder</h4>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-sm-4 col-xs-12">
              <div class="portfolio-label bold">Title *</div>
            </div>
            <div class="col-sm-8 col-xs-12">
              <input id="folder-title" type="text" class="form-control no-radius" placeholder="name of the document folder" value="<?=$data_document[0]['DocCat_name'];?>">
            </div>
          </div>
          <div class="row up1">
            <div class="col-sm-4 col-xs-12">
              <div class="portfolio-label">Description</div>
            </div>
            <div class="col-sm-8 col-xs-12">
              <textarea  id="folder-desc" class="form-control" rows="3" placeholder="tell story about this document folder"><?=$data_document[0]['DocCat_desc'];?></textarea>
            </div>
          </div>
        </div>
        <div class="modal-footer" style="text-align:left;">
          <input id="folder-id" type="hidden" value="<?=$data_document[0]['DocCat_ID'];?>">
          <div class="btn-group">
            <button id="save-folder" type="button" class="btn btn-primary btn-sfolder">Save</button>
            <button id="cancel-doc" type="button" class="btn btn-default btn-sfolder" data-dismiss="modal">Cancel</button>
          </div>
          <div id="mload-folder" class="edit-state hide">
            <img src="<?=$global['absolute-url'];?>img/loadmores.gif" alt="loading">
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- Modal Add Document -->
  <div class="modal fade" id="modal-doc" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <form method="post" id="myForm" action="#" enctype="multipart/form-data" onsubmit="return save_doc()">
      <div class="modal-content" style="border-radius:0;">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="font-size: 35px;position: absolute;right: 10px;top: 3px;"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel" style="text-align: center;font-size: 20px;font-weight: bold;color: #717171;">Add New Document</h4>
        </div>
        <div class="modal-body">
          <!--<div class="row">
            <div class="col-sm-4 col-xs-12">
              <div class="portfolio-label bold">Video Folder *</div>
            </div>
            <div class="col-sm-8 col-xs-12">
              <select id="video-folder" class="form-control no-radius" disabled>
                <option value="">Choose folder</option>
                <option value="<?=$data_video[0]['VideoCat_ID'];?>" selected="selected"><?=$data_video[0]['VideoCat_name'];?></option>
              </select>
            </div>
          </div>!-->
          <div class="row">
            <div class="col-sm-4 col-xs-12">
              <div class="portfolio-label">File Document</div>
            </div>
            <div class="col-sm-8 col-xs-12">
              <input id="doc-file" type="file" name="file" class="form-control no-radius" placeholder="file location">
              <br/><?=$helper['doc-upload'];?>
            </div>

          </div>
          <div class="row up1">
            <div class="col-sm-4 col-xs-12">
              <div class="portfolio-label bold">Document Name *</div>
            </div>
            <div class="col-sm-8 col-xs-12">
              <input id="doc-title" type="text" name="name" class="form-control no-radius" placeholder="name of the document">
            </div>
          </div>
          <div class="row up1">
            <div class="col-sm-4 col-xs-12">
              <div class="portfolio-label">Description</div>
            </div>
            <div class="col-sm-8 col-xs-12">
              <textarea id="doc-desc" name="desc" class="form-control no-radius" rows="3" placeholder="tell story about this document"></textarea>
            </div>
          </div>
         <!--  <div class="row up1">
            <div class="col-sm-4 col-xs-12">
              <div class="portfolio-label">Feature</div>
            </div>
            <div class="col-sm-8 col-xs-12">
                <input id="doc-feature" name="feature" type="checkbox" value="1">
            </div>
          </div> -->
        </div>
        <div class="modal-footer" style="text-align:left;">
          <div class="btn-group">
            <input id="doc-folder" name="doccat_id" type="hidden" class="form-control no-radius" placeholder="doccat id" value="<?php echo $data_document[0]['DocCat_ID'];?>">
            <button id="save-doc" type="button" class="btn btn-success btn-doc">Save</button>
            <button id="cancel-doc" type="button" class="btn btn-default btn-doc" data-dismiss="modal">Cancel</button>
          </div>
          <div id="load-doc" class="edit-state hide">
            <img src="<?=$global['absolute-url'];?>img/loadmores.gif" alt="loading">
          </div>
      </div>
      </form>
    </div>
  </div>
  <script src="<?php echo $global['absolute-url'];?>js/function.js"></script>
  <script type="text/javascript" src="<?php echo $global['absolute-url'];?>js/fbox/source/jquery.fancybox.js?v=2.1.5"></script>
  <script type="text/javascript">
  $(document).ready(function() {
    $(".docpdf").fancybox({
    openEffect: 'elastic',
    closeEffect: 'elastic',
    autoSize: true,
    type: 'iframe',
    iframe: {
    preload: false // fixes issue with iframe and IE
    }
    });
  });
  get_doc();
  function get_doc(){
    var url = "<?=$api['doc-data'];?>&user_id=<?php echo $_SESSION['userData']['id'];?>&auth_code=<?php echo $_SESSION['userData']['auth_code'];?>&doccat_id=<?=$data_document[0]['DocCat_ID'];?>";                            
    $.ajax({url: url,success:function(result){
    $('#doc-list').html(replace_doc(result.data));
    }});
  }
  function date_format(date){
    var formatDate = "";
    var month = new Array();
    month[0] = "January";
    month[1] = "February";
    month[2] = "March";
    month[3] = "April";
    month[4] = "May";
    month[5] = "June";
    month[6] = "July";
    month[7] = "August";
    month[8] = "September";
    month[9] = "October";
    month[10] = "November";
    month[11] = "December";

    var d = new Date(date);
    var day = d.getDate();
    var months = month[d.getMonth()];
    var year = d.getFullYear();

    var formatDate = months+" "+day+", "+year;
    return formatDate;
  }
  function replace_doc(datas){
    var resultHTML = '';
    resultHTML += "<div class='col-sm-3 col-xs-6 col-potfolio'>";
    resultHTML += "<div class='portfolio-content up2'>";
    resultHTML += "<a href='#modal-doc' class='add-photo-portfolio' data-toggle='modal' data-target='#modal-doc'>";
    resultHTML += "<img class='portfolio-img' src='<?=$global['absolute-url'];?>img/add-document.png' title='add file' alt='add file' />";
    resultHTML += "<div class='portfolio-name'>Add File</div>";
    resultHTML += "</a>";
    resultHTML += "</div>";
    resultHTML += "</div>";

    if(datas != null){

    var obj = datas;    
    for(var i=0;i < obj.length;i++){
    var name = obj[i].Doc_title;
    var regex = /\\/g;
    var replaceName = name.replace(regex, "").replace(/&quot;/g, "");
    var lowercase = replaceName.toLowerCase();
    var encodeName = lowercase.replace(/\s+/g, '-');
    resultHTML += "<div id='col-vthmb-"+obj[i].Doc_ID+"' class='col-sm-3 col-xs-6 col-portfolio'>";
    resultHTML += "<div class='portfolio-content up2 hidden-xs'>";
    resultHTML += "<div class='doc-thmb'>";
    resultHTML += "<div class='hover-doc'>";
    resultHTML += "<div class='Docdate'>Modified : <br/>"+date_format(obj[i].Doc_modify_date)+"</div>";
    resultHTML += "<div class='Docdate'>Created : <br/>"+date_format(obj[i].Doc_create_date)+"</div>";
    resultHTML += "<div class='hover-read'><a href='<?=$global['img-url'];?>"+obj[i].Doc_fileLoc+"' class='btn btn-info no-radius docpdf' target='_blank'><i class='glyphicon glyphicon-search' style='margin-right:3px;'></i>Read</a></div>";
    resultHTML += "</div>";
    resultHTML += "<img src='<?=$global['absolute-url'];?>img/placeholder-document.png' alt='"+obj[i].Doc_title+"' class='portfolio-img'>";
    resultHTML += "</div>";
    resultHTML += "<a href='<?=$path['user-doc-detail'];?>"+obj[i].Doc_ID+"_"+encodeName+".html' class='portfolio-link' title='"+obj[i].Doc_title+"'>";
    resultHTML += "<div class='portfolio-name hidden-xs hidden-sm'>"+charString(obj[i].Doc_title,12)+"</div>";
    resultHTML += "</a>";
    resultHTML += "</div>";
    resultHTML += "<div class='portfolio-content up2 visible-xs'>";
    resultHTML += "<a href='<?=$path['user-doc-detail'];?>"+obj[i].Doc_ID+"_"+encodeName+".html' class='portfolio-link' title='"+obj[i].Doc_title+"'>";
    resultHTML += "<div class='doc-thmb'>";
    resultHTML += "<img src='<?=$global['absolute-url'];?>img/placeholder-document.png' alt='"+obj[i].Doc_title+"' class='portfolio-img'>";
    resultHTML += "</div>";
    resultHTML += "<div class='portfolio-name'>"+charString(obj[i].Doc_title,5)+"</div>";
    resultHTML += "</a>";
    resultHTML += "</div>";
    resultHTML += "</div>";
 
    } 
  }

    return resultHTML;
  }
  $('#save-folder').on('click', function(){
    update_folder();
  })
  $('#save-doc').on('click', function(){
    save_doc();
  })
  // update function folder
  function update_folder(){

    var url = "<?=$api['doccat-update'];?>";
    var folder_title = $("#folder-title").val();
    var folder_desc = $("#folder-desc").val();
    var folder_id = $("#folder-id").val();
    $(".btn-sfolder").addClass("disabled");
    $("#mload-folder").removeClass("hide");

    var data = {
      user_id : "<?=$_SESSION['userData']['id'];?>",
      auth_code : "<?=$_SESSION['userData']['auth_code'];?>",
      id : folder_id,
      name : folder_title,
      desc : folder_desc
    }
    $.ajax({url: url,data : data, success:function(result){
      console.log(result);
      setTimeout(function() {
        $("#modal-folder").modal('hide');
        $(".btn-sfolder").removeClass("disabled");
        $("#mload-folder").addClass("hide");
      }, 1500)
      $("#folder-title").val(folder_title);
      $("#folder-desc").val(folder_desc);
      $("#folder-nav").text(folder_title);
    }});
  }
  function profileNav(){
    $('#profile-arrow').toggleClass('nav-close');
  }

  function save_doc(){ 
    var formdata = new FormData();      
    var file = $('#doc-file')[0].files[0];
    formdata.append('FILE', file);
    $.each($('#myForm').serializeArray(), function(a, b){
      formdata.append(b.name, b.value);
    });
    $.ajax({url: '<?=$global['absolute-url'];?>uploadDocument.php?action=insert_doc',
      data: formdata,
      processData: false,
      contentType: false,
      type: 'POST',
      beforeSend: function(){
        // add event or loading animation
        $(".btn-doc").addClass("disabled");
        $("#load-doc").removeClass("hide");
      },
      success: function(ret) {
        console.log(ret); // get return (if success) here
        setTimeout(function() {
          get_doc();
          $("#modal-doc").modal('hide');
          $(".btn-doc").removeClass("disabled");
          $("#load-doc").addClass("hide");
          $("#doc-file").val("");
          $("#doc-title").val("");
          $("#doc-desc").val("");
          document.getElementById("doc-feature").checked = false;
        }, 1500)
      }
    });
    return false;
  }
  </script>
</body>
</html>
