<?php 
include("packages/civ_require.php");
include("controller/controller_civ_global.php");
include("controller/controller_civ_tips.php");
$curpage='tips';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title><?=$seo['title-tips'];?></title>
    <meta name="keywords" content="<?=$seo['keyword-tips'];?>">
    <meta name="description" content="<?=$seo['desc-tips'];?>">
    <?php include("packages/civ_head.php");?>
</head>
<body>
    <!-- HEADER -->
    <?php include("parts/part-header.php");?>
    <!-- END HEADER -->
    
    <div class="tips-part">
        <div class="container container-max">
            <div class="row">
                <div class="col-md-9 col-sm-8 col-xs-12">
                    <div class="content-head"><?=$lang['tips-head'];?></div>
                    <div class="content-desc">
                        <?=$lang['tips-note'];?>
                    </div>
                    <?php if(is_array($datas)){ ?>

                    <?php foreach ($datas as $data) { ?>
                    <div class="tips-warp">
                        <div class="tips-cat">
                            <!-- <img src="<?=$global['absolute-url'];?>img/basic-img/200X.png" alt="image"> -->
                            <span><?=relative_time($data['News_create_date']);?> </span>
                            <span class="cat-title"><?=correctDisplay($data['Nc_title']);?></span>
                        </div>
                        <?php if($data['Np_img'] != ""){ ?>
                        <a href="<?=$path['tips'].encode($data['News_title']).'_'.$data['News_ID'].'.html';?>" class="tips-image" style="background-image: url('<?=$global['absolute-base-admin'].$data['Np_img'];?>')"></a>
                        <?php } ?>
                        <div class="tips-container">
                            <div class="tips-content">
                                <h1 class="tips-title">
                                    <a href="<?=$path['tips'].encode($data['News_title']).'_'.$data['News_ID'].'.html';?>"><?=$data['News_title'];?></a>
                                </h1>
                                <?php if($data['News_author'] != ""){ ?>
                                <div class="tips-author">by 
                                    <?php if($data['News_author_link'] != ""){ ?>
                                    <a href="<?=$data['News_author_link'];?>" target="_blank"><?=correctDisplay($data['News_author']);?></a>
                                    <?php } else { ?>
                                    <a href="#"><?=correctDisplay($data['News_author']);?></a>
                                    <?php } ?>
                                </div>
                                <?php } ?>
                                <div class="tips-desc">
                                    <?=charLength(correctDisplay($data['News_content']),300);?>
                                </div>
                                <a href="<?=$path['tips'].encode($data['News_title']).'_'.$data['News_ID'].'.html';?>" class="tips-read">Read More</a>
                            </div>
                        </div>
                    </div>
                    <?php } ?>

                    <?php } ?>

                    <?php include("parts/part-pagination.php");?> 

            </div>
            <div class="col-md-3 col-sm-4 col-xs-12">

            </div>
        </div>

        <!-- START PART TESTIMONI -->
        <?php include("parts/part-testimoni.php");?>
        <!-- END PART TESTIMONI -->
        
    </div>
</div>
<!-- START PART FOOTER -->
<?php include("parts/part-footer.php");?>
<!-- END PART FOOTER --> 
</body>
</html>