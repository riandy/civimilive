<?php 
include("packages/require.php");
include("controller/controller_global.php");
include("controller/controller_discover.php");

$curpage='discover';
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Discover Page | Civimi</title>
  <meta name="author" content="CIVIMI">
  <meta name="keywords" content="<?=$datas[0]['Photo_title'];?>">
  <meta name="description" content="<?=$datas[0]['Photo_title'];?>">
  <meta name="robots" content="<?=$seo['robot_yes'];?>">
  <?php include("packages/head.php");?>
  <script language="javascript">
  var popupWindow = null;
  function centeredPopup(url,winName,w,h,scroll){
    LeftPosition = (screen.width) ? (screen.width-w)/2 : 0;
    TopPosition = (screen.height) ? (screen.height-h)/2 : 0;
    settings ='height='+h+',width='+w+',top='+TopPosition+',left='+LeftPosition+',scrollbars='+scroll+',resizable'
    popupWindow = window.open(url,winName,settings)
  }
  </script>
</head> 
<body>
  <div id="all">
    <!-- start top nav -->
    <?php include("section-top-nav.php");?>
    <!-- end top nav -->
    <div id="discover-section">
      <div class="discover-header">
        <div class="discover-header-content">
          <div class="discover-header-text">Show your work to the world</div>
          <div class="discover-header-link">
            <ul>
              <li><a href="#" class="dh-link">Graphic</a></li>
              <li><a href="#" class="dh-link">Document</a></li>
              <li><a href="#" class="dh-link">Video</a></li>
              <li><a href="#" class="dh-link">Website</a></li>
            </ul>
          </div>
          <div class="discover-header-note">Want to get your work posted?</div>
        </div>
      </div>
      <div class="discover-content">
        <div class="discover-container">
          <div class="row">
            <?php foreach ($datas as $data) { ?> 
            <div class="col-sm-3 col-xs-4 col-discover">
              <a href="#" onclick="openCard('<?=$data['Photo_ID']?>');" data-toggle="modal" data-target="#modal-card" class="discover-grid" style="background-image:url('<?php echo $global['img-url'].$data['Photo_ThmbImgLink'];?>');">
                <div class="discover-title">
                  <h3><?=correctDisplay($data['Photo_title']);?></h3>
                </div>
              </a>
            </div>
            <?php } ?>
          </div>

          <?php
            $cur_page = $path['discover'];
            $q_page = '';
          ?>
          <div class="part-discover-pagination hidden-xs">
            <ul>
              <?php 
              $batch = getBatchDiscover($O_page);
              if($batch < 1){$batch = 1;}
              $prevLimit = 1 +(10*($batch-1));
              $nextLimit = 10 * $batch;
              if($nextLimit > $total_page){$nextLimit = $total_page;}
              ?>
              <?php if($batch > 1 && $O_page > 10){?>
              <li><a href="<?=$cur_page.($prevLimit-1)."/".$q_page;?>" class="discover-next">Prev 10</a></li>
              <?php } ?>

              <?php for($pagina = $prevLimit;$pagina <= $nextLimit;$pagina++){?>
              <li><a href="<?=$cur_page;?><?=$pagina;?>/<?=$q_page;?>" class="discover-pagination <?php if($O_page == $pagina){?>active-discover<?php } ?>"><?=$pagina;?></a></li>
              <?php } ?>

              <?php for($pagina = $prevLimit; $pagina <= $nextLimit;$pagina++){?>
              <?php if($pagina == $nextLimit && $pagina < $total_page){if($pagina >= $total_page){ $pagina -=1;}?>
              <li><a href="<?=$cur_page.($pagina+1)."/".$q_page;?>" class="discover-next">Next 10</a></li>
              <?php } ?>
              <?php } ?>
            </ul>
          </div>
          <div class="part-discover-pagination visible-xs">
            <ul>
              <?php if($O_page > 1){?>
              <li><a href="<?=$cur_page.($O_page-1)."/".$q_page;?>" class="discover-next">Prev</a></li>
              <?php } ?>
              <li>
                <select class="form-control discover-select" onchange="location = this.options[this.selectedIndex].getAttribute('value');">
                  <?php for($pagina = 1;$pagina <= $total_data_all;$pagina++){?>
                  <option value="<?=$cur_page;?><?=$pagina;?>/<?=$q_page;?>" <?php if($O_page == $pagina){?>selected="selected"<?php } ?> ><?=$pagina;?></option>
                  <?php } ?>  
                </select>
              </li>
              <?php if($total_page > 1 && ($O_page < $total_page)){?>
              <li><a href="<?=$cur_page.($O_page+1)."/".$q_page;?>" class="discover-next">Next</a></li>
              <?php } ?>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <!-- end center content -->
    <!-- start footer section -->
    <?php include("section-footer.php");?>
    <!-- end footer section -->
  </div><!--  end all div -->
  <!-- START PORTFOLIO CARD-->
  <?php include("part-portfolio_card.php");?>
  <!-- END PORTFOLIO CARD-->
  <script src="<?php echo $global['absolute-url'];?>js/function.js"></script>
</body>
</html>
