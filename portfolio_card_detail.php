<?php 
include("packages/require.php");
include("controller/controller_global.php");
include("controller/controller_portfolio_card.php");

$curpage='portfolio_card';
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Portfolio Card <?=$datas[0]['User_fname'].' '.$datas[0]['User_lname'];?> | Civimi</title>
  <meta name="author" content="CIVIMI">
  <meta name="keywords" content="<?=$datas[0]['Photo_title'];?>">
  <meta name="description" content="<?=$datas[0]['Photo_title'];?>">
  <meta name="robots" content="<?=$seo['robot_yes'];?>">
  <meta itemprop="name" content="Portfolio Card <?=$datas[0]['User_fname'].' '.$datas[0]['User_lname'];?> | Civimi">
  <meta itemprop="description" content="<?=$datas[0]['Photo_title'];?>">
  <meta itemprop="image" content="<?=check_image_url($datas[0]['Photo_imgLink']);?>">
  <!-- SOCIAL MEDIA META -->
  <meta property="og:description" content="">
  <meta property="og:image" content="<?=check_image_url($datas[0]['Photo_imgLink']);?>">
  <meta property="og:site_name" content="civimi">
  <meta property="og:title" content="<?=$datas[0]['Photo_title'];?>">
  <meta property="og:type" content="website">
  <meta property="og:url" content="<?=$path['portfolio-card'].$datas[0]['Photo_ID'].'.html';?>">

  <!-- twitter meta -->
  <meta name="twitter:card" content="summary_large_image">
  <meta name="twitter:description" content="<?=$datas[0]['Photo_title'];?>">
  <meta name="twitter:title" content="Portfolio Card <?=$datas[0]['User_fname'].' '.$datas[0]['User_lname'];?> | Civimi">
  <meta name="twitter:site" content="@civimi">
  <meta name="twitter:creator" content="@civimi">
  <meta name="twitter:image" content="<?=check_image_url($datas[0]['Photo_imgLink']);?>">
  <meta name="twitter:image:src" content="<?=check_image_url($datas[0]['Photo_imgLink']);?>">
  <?php include("packages/head.php");?>
  <script language="javascript">
  var popupWindow = null;
  function centeredPopup(url,winName,w,h,scroll){
    LeftPosition = (screen.width) ? (screen.width-w)/2 : 0;
    TopPosition = (screen.height) ? (screen.height-h)/2 : 0;
    settings ='height='+h+',width='+w+',top='+TopPosition+',left='+LeftPosition+',scrollbars='+scroll+',resizable'
    popupWindow = window.open(url,winName,settings)
  }
  </script>
</head> 
<body>
  <div id="all">
    <!-- start top nav -->
    <?php include("section-top-nav.php");?>
    <!-- end top nav -->
    <div id="portfolio-card-section">

      <div class="container container-max">
        <div class="portfolio-card-content">
          <div class="mp-content">
            <div class="mp-name">
              <img id="card-uimage" src="<?=check_image_url($datas[0]['User_proPhotoThmb']);?>" alt="photo"> <span id="card-uname"><?=$datas[0]['User_fname'].' '.$datas[0]['User_lname'];?></span>
            </div>
            <div id="card-ujob" class="mp-job"><?=$datas[0]['User_bioDesc'];?></div>
            <div id="card-uplace" class="mp-place"><?=$datas[0]['User_city'].', '.$datas[0]['User_country'];?></div>
          </div>
          <div class="mp-image"><img id="mp-image" src="<?=check_image_url($datas[0]['Photo_imgLink']);?>" alt="image"></div>
          <div class="card-detail" style="padding-bottom:20px;">
            <div id="mp-title" class="mp-title" style="display: block;"><?=$datas[0]['Photo_title'];?></div>
            <div class="mp-tag">
              <ul id="tag-photo" class="row"></ul>
            </div>
            <div class="photo-func">
              <ul>
                <li><a href="javascript:;" class="photo-like" onclick="likeCard();"><img id="like-icon" src="http://www.civimi.com/img/heart-reds.png" alt="like" class="liked"><span id="cyou-like"></span><span id="ctotal-like"></span> <span id="clike-text"></span></a></li>
                <li><span><img src="http://www.civimi.com/img/eye.png" alt="views"><span id="ctotal-view"></span> Views</span></li>
                <li class="mp-share">
                  <a href="#" onclick="centeredPopup('https://www.facebook.com/sharer/sharer.php?u=<?php echo $path['portfolio-card'].$datas[0]['Photo_ID'].".html";?>','myWindow','500','300','yes');return false"><img src="http://www.civimi.com/img/share-facebook.png" alt="share"></a>
                  <a href="#" onclick="centeredPopup('https://twitter.com/intent/tweet?url=<?php echo $path['portfolio-card'].$datas[0]['Photo_ID'].".html";?>&text=<?php echo correctDisplay($datas[0]['Photo_title']);?>&via=civimi');return false"><img src="http://www.civimi.com/img/share-twitter.png" alt="share"></a>
                  <a href="#" onclick="centeredPopup('https://plus.google.com/share?url=<?php echo $path['portfolio-card'].$datas[0]['Photo_ID'].".html";?>','myWindow','500','300','yes');return false"><img src="http://www.civimi.com/img/share-google.png" alt="share"></a>
                </li>
              </ul>
            </div>
          </div>
          <input type="hidden" id="card-id" value="">
          <input type="hidden" id="card-userid" value="">
          <input type="hidden" id="card-username" value="">
          <div id="card-load" class="card-load" style="display: none;">
            <div class="card-load-bg">
              <div class="card-load-img">
                <img src="http://www.civimi.com/img/load-input.gif" alt="load">
              </div>
            </div>
          </div>
        </div>
      </div>

    </div>
    <!-- end center content -->
    <!-- start footer section -->
    <?php include("section-footer.php");?>
    <!-- end footer section -->
  </div><!--  end all div -->
  <script src="<?php echo $global['absolute-url'];?>js/function.js"></script>
  <?php $N_ip = getIP(); $userGeoData = getGeoIP($N_ip);?>
  <script>
  openCard(<?=$card_ID;?>);

    function openCard(cardID){
      $("#card-load").show();
      $("#card-id").val(cardID);
      var url = "<?=$api['photo-detail'];?>";
      var data = {
        id : cardID,
      }
      $.ajax({url: url,data : data, success:function(result){
        // var image_card = "<?=$global['img-url'];?>"+result.data[0].Photo_imgLink;
        if(result.data[0].Photo_title != null){
          var title_card = result.data[0].Photo_title;
        } else {
          var title_card = "";
        }
        // if(result.data[0].User_proPhotoThmb != null || result.data[0].User_proPhotoThmb != ""){
        //   var image = "<?=$global['img-url'];?>"+result.data[0].User_proPhotoThmb;
        // } else {
        //   var image = "<?=$global['img-url'];?>img/dummy.jpg";
        // }
        // var full_name = result.data[0].User_fname+" "+result.data[0].User_lname;
        // var job = result.data[0].User_bioDesc;
        // var address = result.data[0].User_city+", "+result.data[0].User_country;
        var user_id = result.data[0].id_user;
        var username = result.data[0].User_username;
        // $("#mp-image").attr("src",image_card);
        // $("#mp-title").text(title_card);
        $("#card-userid").val(user_id);
        $("#card-username").val(username);
        $("#ctotal-view").text(result.data[0].hits_image);
        // $("#card-uimage").attr("src",image);
        // $("#card-uname").text(full_name);
        // $("#card-ujob").text(job);
        // $("#card-uplace").text(address);
        if(title_card != ""){
          $("#mp-title").show();
        } else {
          $("#mp-title").hide();
        }
        hitsCard();
        $("#card-load").fadeOut('slow');
      }});
totalLikeCard();
tagCard();
}
function tagCard(){
  var portfolio_id = $("#card-id").val();
  var porfolio_name = "photo";
  var url = "<?=$api['tag-data'];?>";
  var data = {
    table_id : portfolio_id,
    table_name : porfolio_name
  }
  $.ajax({url: url,data : data, success:function(result){
    $("#tag-photo").html(replace_tag_card(result.data));
  }}); 
}
function replace_tag_card(datas){
  var resultHTML="";
  if(datas != null){
    var obj = datas;    
    for(var i=0;i < obj.length;i++){
      resultHTML += "<li>"+obj[i].Tag_title+"</li>";
    }
  }
  return resultHTML;
}
function totalLikeCard(){
  var portfolio_id = $("#card-id").val();
  var porfolio_type = "image";
  var url = "<?=$api['like-data'];?>";
  var data = {
    type_id : portfolio_id,
    type : porfolio_type
  }
  $.ajax({url: url,data : data, success:function(result){
    var total = result.data[0].total_like;
    checkLikeCard(total)
  }});
}
function checkLikeCard(total_like){
  var portfolio_id = $("#card-id").val();
  var porfolio_type = "image";
  var url = "<?=$api['like-check'];?>";
  var data = {
    type_id : portfolio_id,
    type : porfolio_type,
    view_user : "<?=$_SESSION['userData']['id'];?>",
    auth_code : "<?=$_SESSION['userData']['auth_code'];?>"
  }
  $.ajax({url: url,data : data, success:function(result){
    var likes = result.message;
    var heart = "<?=$global['absolute-url'];?>img/heart.png";
    var heart_reds = "<?=$global['absolute-url'];?>img/heart-reds.png";
    if(likes == "Liked"){

      if(total_like == "1"){
        $("#cyou-like").text("You like this");
        $("#clike-text").text("");
        $("#ctotal-like").hide();
      } else if (total_like >= "1"){
        $("#cyou-like").text("You and ");
        $("#ctotal-like").show();
        $("#ctotal-like").text(eval(total_like-1))
        $("#clike-text").text("other like this")
      }

      $("#like-icon").attr("src",heart_reds);
      $("#like-icon").addClass("liked");

    } else {

      $("#cyou-like").text("");
      $("#ctotal-like").text(total_like);
      $("#ctotal-like").show();
      $("#clike-text").text("Likes");

      $("#like-icon").attr("src",heart);
      $("#like-icon").removeClass("liked");

    }
  }}); 
}
function hitsCard(){
  var portfolio_id = $("#card-id").val();
  var porfolio_page = "portfolio";
  var porfolio_type = "image";
  var porfolio_user_id = $("#card-userid").val();
  var porfolio_user_name = $("#card-username").val();
  var portfolio_ip = "<?=getIP();?>";
  var portfolio_ip_city = "<?=$userGeoData->city;?>";
  var portfolio_ip_country = "<?=$userGeoData->country_name;?>";
  var url = "<?=$api['user-hits'];?>";
  var data = {
    type_id : portfolio_id,
    user_id : porfolio_user_id,
    username : porfolio_user_name,
    page : porfolio_page,
    type : porfolio_type,
    view_user : "<?=$_SESSION['userData']['id'];?>",
    ip : portfolio_ip,
    ip_city : portfolio_ip_city,
    ip_country : portfolio_ip_country
  }
  $.ajax({url: url,data : data, success:function(result){
      // console.log(result);
    }});
}
function likeCard(){
  var portfolio_id = $("#card-id").val();
  var porfolio_page = "portfolio";
  var porfolio_type = "image";
  var porfolio_user_id = $("#card-userid").val();
  var porfolio_user_name = $("#card-username").val();
  var portfolio_ip = "<?=getIP();?>";
  var portfolio_ip_city = "<?=$userGeoData->city;?>";
  var portfolio_ip_country = "<?=$userGeoData->country_name;?>";
  var url = "<?=$api['like-update'];?>";
  var data = {
    type_id : portfolio_id,
    user_id : porfolio_user_id,
    username : porfolio_user_name,
    page : porfolio_page,
    type : porfolio_type,
    view_user : "<?=$_SESSION['userData']['id'];?>",
    auth_code : "<?=$_SESSION['userData']['auth_code'];?>",
    ip : portfolio_ip,
    ip_city : portfolio_ip_city,
    ip_country : portfolio_ip_country
  }
  $.ajax({url: url,data : data, success:function(result){
    var heart = "<?=$global['absolute-url'];?>img/heart.png";
    var heart_reds = "<?=$global['absolute-url'];?>img/heart-reds.png";
    if($("#like-icon").hasClass("liked")){
      $("#like-icon").attr("src",heart);
      $("#like-icon").removeClass("liked");
    } else {
      $("#like-icon").attr("src",heart_reds);
      $("#like-icon").addClass("liked");
    }
    totalLikeCard();
  }});
}
</script>
</body>
</html>
