<?php 
include("packages/require.php");
include("controller/controller_global.php");
include("controller/controller_user_web_detail.php");

$curpage='user_web';
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title><?=$_SESSION['userData']['username']."'s ".$data_web[0]['Web_title']." - ".$global['title-portfolio_web'];?></title>
  <meta name="keywords" content="">
  <meta name="description" content="">
  <meta name="robots" content="<?=$seo['robot_no'];?>">
  <?php include("packages/head.php");?>
  <link rel="stylesheet" href="<?php echo $global['absolute-url'];?>css/dropzone.css"/>
  <script src="<?php echo $global['absolute-url'];?>js/dropzone.js"></script>
</head>
<body>
  <div id="all">
    <!-- start top nav -->
    <?php include("section-top-nav.php");?>
    <!-- end top nav -->
    <div id="user-section" class="container-section">
    
    <div class="container">
      <div id="profile-content">
        <div class="up5 visible-xs"></div>
        <!-- start fixed mobile nav -->
        <?php $header_content = "Web";  include("user_part-mobile-nav.php");?>
        <!-- end fixed mobile nav-->
        <div class="row">
        <div class="up5 visible-xs"></div>
          <!-- profile sidebar -->
          <div class="col-sm-3 col-xs-12">
            <!-- start sidebar -->
            <?php include("user_part-sidebar.php");?>
            <!-- end sidebar -->
          </div><!-- profile sidebar -->
          <div class="col-sm-9 col-xs-12 xs-pad0">
              <div id="personal-portfolio-section" class="job-sbox">
                <div id="education-header" class="user-section-header row border-bottom-eee">
                  <div class="col-xs-12">
                    <div class="profile-title hidden-xs" style="font-size:14px;"><i class="glyphicon glyphicon-globe"></i>&nbsp;&nbsp;
                      <a href="<?=$path['user-web'];?>" class="module-breadcrumb">Web</a> >
                      <a id="folder-name" href="<?=$path['user-web-folder'];?><?=$data_web[0]['WebCat_ID'];?>_<?=encode($data_web[0]['WebCat_name']);?>.html" class="module-breadcrumb"><?=$data_web[0]['WebCat_name'];?></a> > 
                      <span id="web-name" class="module-breadcrumb"><?=$data_web[0]['Web_title'];?></span>
                    </div>
                    <div class="profile-title visible-xs" style="font-size:14px;"><i class="glyphicon glyphicon-globe"></i>&nbsp;&nbsp;
                      <a id="folder-back" href="<?=$path['user-web-folder'];?><?=$data_web[0]['WebCat_ID'];?>_<?=encode($data_web[0]['WebCat_name']);?>.html" class="module-breadcrumb">< Back</a>
                    </div>
                  </div>
                </div>
                <div id="portfolio-profile">
                  <div class="row">
                    <div class="col-sm-4 col-xs-12">
                      <div class="portfolio-label bold">Web Folder *</div>
                    </div>
                    <div class="col-sm-8 col-xs-12">
                      <select id="web-folder" class="form-control no-radius">
                      </select>
                    </div>
                  </div>
                  <div class="row up1">
                    <div class="col-sm-4 col-xs-12">
                      <div class="portfolio-label bold">Web Name *</div>
                    </div>
                    <div class="col-sm-8 col-xs-12">
                      <input id="web-title" type="text" class="form-control no-radius" placeholder="name of the web" value="<?=$data_web[0]['Web_title'];?>">
                    </div>
                  </div>
                  <div class="row up1">
                    <div class="col-sm-4 col-xs-12">
                      <div class="portfolio-label">Description</div>
                    </div>
                    <div class="col-sm-8 col-xs-12">
                      <textarea  id="web-desc" class="form-control no-radius" rows="3" placeholder="tell story about this web"><?=$data_web[0]['Web_desc'];?></textarea>
                    </div>
                  </div>
                  <div class="row up1">
                    <div class="col-sm-4 col-xs-12">
                      <div class="portfolio-label bold">Web Link *</div>
                    </div>
                    <div class="col-sm-8 col-xs-12">
                      <input id="web-link" type="text" class="form-control no-radius" placeholder="i.e. http://www.civimi.com" value="<?=$data_web[0]['Web_link'];?>">
                      <div class="web-notice">*Paste your online web links here, i.e. http://www.civimi.com</div>
                    </div>
                  </div>
                  <div class="up2"></div>
                  <div class="row">
                    <div class="col-xs-12">
                      <div id="alert-web" class="alert alert-success" role="alert" style="display:none;"></div>
                    </div>
                  </div>
                  <hr/>
                  <div class="row up2">
                    <div class="col-xs-12">
                      <input id="web-id" type="hidden" value="<?=$data_web[0]['Web_ID'];?>">
                      <input id="webcat-id" type="hidden" value="<?=$data_web[0]['WebCat_ID'];?>">
                      <div class="btn-group">
                        <button id="save-web" type="button" class="btn btn-success btn-web">Save</button>
                        <a href="<?=$path['user-web-folder'];?><?=$data_web[0]['WebCat_ID'];?>_<?=encode($data_web[0]['WebCat_name']);?>.html" id="cancel-web" type="button" class="btn btn-default btn-web" data-dismiss="modal">Cancel</a>
                        <button id="delete-web" type="button" class="btn btn-danger btn-web">Delete</button>
                      </div>
                      <div id="load-web" class="edit-state hide">
                        <img src="<?=$global['absolute-url'];?>img/loadmores.gif" alt="loading">
                      </div>
                    </div>
                  </div>
                  <div class="pad1"></div>
                </div>
              </div>
          </div>
      </div>
    </div>
            
    </div>
    <!-- end center content -->
  </div>
    <!-- start footer section -->
    <?php include("section-footer.php");?>
    <!-- end footer section -->
  </div><!--  end all div -->
  <script src="<?php echo $global['absolute-url'];?>js/function.js"></script>
  <script type="text/javascript">
  $('#save-web').on('click', function(){
    update_web();
  })
  $('#delete-web').on('click', function(){
    delete_web();
  })
  get_folder();
  function get_data_category(id){
    var cat = $('#web-folder option:selected').text();
    $('#folder-name').text(cat);
  }
  function get_folder(){
    var url = "<?=$api['webcat-data'];?>&user_id=<?php echo $_SESSION['userData']['id'];?>&auth_code=<?php echo $_SESSION['userData']['auth_code'];?>";                            
    $.ajax({url: url,success:function(result){
    $('#web-folder').html(replace_folder(result.data));
    }});
  }
  function replace_folder(datas){
    var webcat_id = $("#webcat-id").val();
    var resultHTML='';
        resultHTML += "<option value=''>Choose folder</option>";
      if(datas != null){
        var obj = datas;    
        for(var i=0;i < obj.length;i++){
          if(webcat_id == obj[i].WebCat_ID){
            var selected = "selected=selected";
          } else {
            var selected = "";
          }
          resultHTML += "<option value='"+obj[i].WebCat_ID+"' "+selected+">"+obj[i].WebCat_name+"</option>";
        }
      }
    return resultHTML;
  }
  // update function web
  function update_web(){

    var url = "<?=$api['web-update'];?>";
    var web_title = $("#web-title").val();
    var web_desc = $("#web-desc").val();
    var web_link = $("#web-link").val();
    var web_id = $("#web-id").val();
    var web_catid = $("#web-folder").val();
    var cat = $('#web-folder option:selected').text();
    var name = cat;
    var regex = /\\/g;
    var replaceName = name.replace(regex, "").replace(/&quot;/g, "");
    var lowercase = replaceName.toLowerCase();
    var encodeName = lowercase.replace(/\s+/g, '-');
    $(".btn-web").addClass("disabled");
    $("#load-web").removeClass("hide");
    var data = {
      user_id : "<?=$_SESSION['userData']['id'];?>",
      auth_code : "<?=$_SESSION['userData']['auth_code'];?>",
      id : web_id,
      webcat_id : web_catid,
      title : web_title,
      desc : web_desc,
      link : web_link
    }
    $.ajax({url: url,data : data, success:function(result){
      console.log(result);
      setTimeout(function() {
          $(".btn-web").removeClass("disabled");
          $("#load-web").addClass("hide");
          $("#web-title").val(web_title);
          $("#web-desc").val(web_desc);
          $("#webcat-id").val(web_catid);
          $("#web-link").val(web_link);
          $("#alert-web").show();
          $("#alert-web").text("Success update web");
          $("#alert-web").fadeOut(5400);
          $('#web-name').text(web_title);   
          var newlinks = "<?=$path['user-web-folder'];?>"+web_catid+"_"+encodeName+".html";
          $('#folder-name').attr("href",newlinks);
          $('#cancel-web').attr("href",newlinks);
          $('#folder-back').attr("href",newlinks);
          get_data_category(web_catid);
          get_folder();
      }, 1500)
    }});
  }//delete function web
  function delete_web(){
    if (confirm("Are you sure to delete this website ?")) {
      var url = "<?=$api['web-delete'];?>";
      var web_id = $("#web-id").val();
      var webcat_id = $("#webcat-id").val();

      var cat = $('#web-folder option:selected').text();
      var name = cat;
      var regex = /\\/g;
      var replaceName = name.replace(regex, "").replace(/&quot;/g, "");
      var lowercase = replaceName.toLowerCase();
      var encodeName = lowercase.replace(/\s+/g, '-');
      $(".btn-web").addClass("disabled");
      $("#load-web").removeClass("hide");
      var data = {
        user_id : "<?=$_SESSION['userData']['id'];?>",
        auth_code : "<?=$_SESSION['userData']['auth_code'];?>",
        id : web_id
      }
      $.ajax({url: url,data : data, success:function(result){
        console.log(result);
        setTimeout(function() {
          window.location = "<?=$path['user-web-folder'];?>"+webcat_id+"_"+encodeName+".html";
          $(".btn-web").removeClass("disabled");
          $("#load-web").addClass("hide");
        }, 1500)
      }});
    }
  }
  function profileNav(){
    $('#profile-arrow').toggleClass('nav-close');
  }
  </script>
</body>
</html>
