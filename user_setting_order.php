<?php 
include("packages/require.php");
include("controller/controller_user_profile.php");
include("controller/controller_user_setting_order.php");

$curpage='user_setting';
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title><?=$global['title-website'];?></title>
  <meta name="keywords" content="">
  <meta name="description" content="">
  <?php include("packages/head.php");?>
  <link rel="stylesheet" href="<?php echo $global['absolute-url'];?>css/dropzone.css"/>
  <script src="<?php echo $global['absolute-url'];?>js/dropzone.js"></script>
</head>
<body>
  <div id="all">
    <!-- start top nav -->
    <?php include("section-top-nav.php");?>
    <!-- end top nav -->
    <!--<div id="dashboard-setting" class="">
      <ul class="dashboard-setting-nav">
        <li><a href="#">Dashboard</a></li>
        <li><a href="#">Profile</a></li>
        <li><a href="#" class="active">Setting</a></li>
      </ul>
    </div>-->
    <div id="user-section">
    
    <div class="container">
      <div id="profile-content">
        <div class="up5 visible-xs"></div>
        <div class="row">
          <div class="col-xs-12 pad0-xs mobile-nav-section">
            <div class="profile-header hidden-xs">Setting Order</div>
            <div class="row visible-xs mobile-nav-pad">
                <div class="col-xs-4 mobile-nav-link text-center border-right-ccc"><a class="active" href="<?=$path['user-edit'];?>"><span class="glyphicon glyphicon-user"></span>&nbsp;<br class="visible-xs" />&nbsp;<?=strtoupper($lang['nav-profil']);?></a></div>
                  <div class="col-xs-4 mobile-nav-link text-center border-right-ccc"><a href="<?=$path['user-portfolio'];?>"><span class="glyphicon glyphicon-folder-open"></span>&nbsp;<br class="visible-xs" />&nbsp;<?=strtoupper($lang['nav-portfolio']);?></a></div>
                  <div class="col-xs-4 mobile-nav-link text-center"><a href="<?=$path['user-setting'];?>"><span class="glyphicon glyphicon-cog"></span>&nbsp;<br class="visible-xs" />&nbsp;<?=strtoupper($lang['nav-setting']);?></a></div>
            </div>
          </div>
        </div>
        <div class="row">
            <div class="up5 visible-xs"></div>
          <!-- profile sidebar -->
          <div class="col-sm-3 col-xs-12 hidden-xs">
            <!-- start sidebar -->
            <?php include("user_part-sidebar.php");?>
            <!-- end sidebar -->
          </div><!-- profile sidebar -->
          <div class="col-sm-9 col-xs-12 pad0-xs">
              <div id="personal-portfolio-section" class="job-sbox" style="padding-bottom:10px;">
                <div id="education-header" class="user-section-header row border-bottom-eee">
                  <div class="col-xs-12">
                    <div class="profile-title"><i class="glyphicon glyphicon-cog"></i>&nbsp;&nbsp;SETTING ORDER</div>
                  </div>
                </div>
                <div id="setting-section">
                <form name="setting" action="<?=$path['user-setting-order-action'];?>" method="post">
                  <div class="privacy-list-head">
                    <div class="row">
                      <div class="col-xs-12">
                        <?php if($message != null){ ?> 
                          <div id="message1" class="alert alert-success" role="alert"><?php echo $message;?></div>
                        <?php } ?>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-xs-8 pad0">
                        <div class="label-privacy-head">Profile Set To Order</div>
                      </div>
                    </div>
                  </div>

                  <div id="privacy_profile_list">

                  <div class="privacy-list">
                    <div class="row">
                      <div class="col-xs-8">
                        <div class="label-privacy">Objective</div>
                      </div>
                      <div class="col-xs-4">
                        <select name="objective" id="obj" class="form-control no-radius">
                          <?php $arr_obj = array("1", "2", "3", "4", "5");
                          for($int=1;$int <= count($arr_obj);$int++){?>
                          <option value="<?=$int;?>" <?php if($O_obj == $int){echo "selected";};?>><?=$arr_obj[$int-1];?></option>
                          <?php } ?>
                        </select>
                        <div id="valid-interest" class="valid-message"></div>
                      </div>
                    </div>
                  </div>

                  <div class="privacy-list">
                    <div class="row">
                      <div class="col-xs-8">
                        <div class="label-privacy">Education</div>
                      </div>
                      <div class="col-xs-4">
                        <select name="education" id="edu" class="form-control no-radius">
                          <?php $arr_edu = array("1", "2", "3", "4", "5");
                          for($int=1;$int <= count($arr_edu);$int++){?>
                          <option value="<?=$int;?>" <?php if($O_edu == $int){echo "selected";};?>><?=$arr_edu[$int-1];?></option>
                          <?php } ?>
                        </select>
                        <div id="valid-interest" class="valid-message"></div>
                      </div>
                    </div>
                  </div>

                  <div class="privacy-list">
                    <div class="row">
                      <div class="col-xs-8">
                        <div class="label-privacy">Experience</div>
                      </div>
                      <div class="col-xs-4">
                        <select name="experience" id="exp" class="form-control no-radius">
                          <?php $arr_exp = array("1", "2", "3", "4", "5");
                          for($int=1;$int <= count($arr_exp);$int++){?>
                          <option value="<?=$int;?>" <?php if($O_exp == $int){echo "selected";};?>><?=$arr_exp[$int-1];?></option>
                          <?php } ?>
                        </select>
                        <div id="valid-interest" class="valid-message"></div>
                      </div>
                    </div>
                  </div>

                  <div class="privacy-list">
                    <div class="row">
                      <div class="col-xs-8">
                        <div class="label-privacy">Skill</div>
                      </div>
                      <div class="col-xs-4">
                        <select name="skill" id="skill" class="form-control no-radius">
                          <?php $arr_skill = array("1", "2", "3", "4", "5");
                          for($int=1;$int <= count($arr_skill);$int++){?>
                          <option value="<?=$int;?>" <?php if($O_skill == $int){echo "selected";};?>><?=$arr_skill[$int-1];?></option>
                          <?php } ?>
                        </select>
                        <div id="valid-interest" class="valid-message"></div>
                      </div>
                    </div>
                  </div>

                  <div class="privacy-list">
                    <div class="row">
                      <div class="col-xs-8">
                        <div class="label-privacy">Work</div>
                      </div>
                      <div class="col-xs-4">
                        <select name="work" id="work" class="form-control no-radius">
                          <?php $arr_work = array("1", "2", "3", "4", "5");
                          for($int=1;$int <= count($arr_work);$int++){?>
                          <option value="<?=$int;?>" <?php if($O_work == $int){echo "selected";};?>><?=$arr_work[$int-1];?></option>
                          <?php } ?>
                        </select>
                        <div id="valid-interest" class="valid-message"></div>
                      </div>
                    </div>
                  </div>

                  <div class="privacy-list">
                    <div class="row">
                      <div class="col-xs-8">
                        <div class="label-privacy">Resume Code</div>
                      </div>
                      <div class="col-xs-4">
                        <input name="resume_code" id="resume_code" type="text" class="form-control no-radius" placeholder="Resume Code for privacy" value="<?=$O_resumeCode;?>"/>
                        <div id="valid-interest" class="valid-message"></div>
                      </div>
                    </div>
                  </div>

                  <div class="privacy-list">
                    <div class="row">
                      <div class="col-xs-8">
                        <div class="label-privacy">Portfolio Page</div>
                      </div>
                      <div class="col-xs-4">
                        <select name="portfolio_page" id="portfolio_page" class="form-control no-radius">
                          <?php $arr_page = array("Photo", "Video", "Doc", "Web");
                          for($int=1;$int <= count($arr_page);$int++){?>
                          <option value="<?=$int;?>" <?php if($O_portfolioPage == $int){echo "selected";};?>><?=$arr_page[$int-1];?></option>
                          <?php } ?>
                        </select>
                        <div id="valid-interest" class="valid-message"></div>
                      </div>
                    </div>
                  </div>

                  <!--<div class="privacy-list">
                    <div class="row">
                      <div class="col-xs-8">
                        <div class="label-privacy">Display Skill</div>
                      </div>
                      <div class="col-xs-4">
                        <div class="onoffswitch">
                            <input type="checkbox" name="skill" class="onoffswitch-checkbox" id="privacy_skill" <?php if($O_profile_skill == "Yes"){echo "checked";}?> value="Yes">
                            <label class="onoffswitch-label" for="privacy_skill">
                                <span class="onoffswitch-inner"></span>
                                <span class="onoffswitch-switch"></span>
                            </label>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="privacy-list">
                    <div class="row">
                      <div class="col-xs-8">
                        <div class="label-privacy">Display Certificate</div>
                      </div>
                      <div class="col-xs-4">
                        <div class="onoffswitch">
                            <input type="checkbox" name="cert" class="onoffswitch-checkbox" id="privacy_certificate" <?php if($O_profile_cert == "Yes"){echo "checked";}?> value="Yes">
                            <label class="onoffswitch-label" for="privacy_certificate">
                                <span class="onoffswitch-inner"></span>
                                <span class="onoffswitch-switch"></span>
                            </label>
                        </div>
                      </div>
                    </div>
                  </div>
                  </div>!--> <!-- end privacy profile list -->

                  <div class="up2"></div>
                  <div class="privacy-list-head">
                    <div class="row">
                      <div class="col-xs-8 pad0">
                        <div class="label-privacy-head">Resume Set To Public</div>
                      </div>
                      <div class="col-xs-4 pad0">
                        <div class="onoffswitch">
                            <input type="checkbox" name="resume" class="onoffswitch-checkbox" id="privacy_resume" <?php if($O_resume == "0"){echo "checked";}?> value="0">
                            <label class="onoffswitch-label" for="privacy_resume">
                                <span class="onoffswitch-inner"></span>
                                <span class="onoffswitch-switch"></span>
                            </label>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="privacy-list-head">
                    <div class="row">
                      <div class="col-xs-8 pad0">
                        <div class="label-privacy-head">Portfolio Set To Public</div>
                      </div>
                      <div class="col-xs-4 pad0">
                        <div class="onoffswitch">
                            <input type="checkbox" name="portfolio" class="onoffswitch-checkbox" id="privacy_portfolio" <?php if($O_portfolio == "0"){echo "checked";}?> value="0">
                            <label class="onoffswitch-label" for="privacy_portfolio">
                                <span class="onoffswitch-inner"></span>
                                <span class="onoffswitch-switch"></span>
                            </label>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="savebutton-privacy">
                    <button class="btn btn-info" type="submit">Save Changes</button>
                  </div>
                </form>
                </div>
              </div>
          </div>
      </div>
    </div>
            
    </div>
    <!-- end center content -->
  </div>
    <!-- start footer section -->
    <?php include("section-footer.php");?>
    <!-- end footer section -->
  </div><!--  end all div -->
  <script src="<?php echo $global['absolute-url'];?>js/function.js"></script>
  <script type="text/javascript">
  $("#message1").fadeOut(8400);

  $(document).ready(function(){
    checkPrivacy();
  })
  function checkPrivacy() {
  var s = document.getElementById('privacy_profile');
    if (s.checked) {
      $("#privacy_profile_list").removeClass('hide');
    } else {
      $("#privacy_profile_list").addClass('hide');
    }
  }
  function profileNav(){
    $('#profile-arrow').toggleClass('nav-close');
  }
  </script>
</body>
</html>
