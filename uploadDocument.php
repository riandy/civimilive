<?php
session_start();
header('content-type: application/json');
header("access-control-allow-origin: *");

if(isset($_GET['action'])){//start gate

	require_once("packages/check_input.php");

	require_once("model/Connection.php");
	$obj_connect = new Connection();
		
	require_once("model/Doc.php");
	$obj_doc = new Doc();

	require_once("model/User.php");
	$obj_user = new User();
	
	if($_GET['action'] == 'insert_doc' && isset($_SESSION['userData']['id']) && isset($_SESSION['userData']['auth_code'])){//START INSERT DOC
		$obj_connect->up();

		$N_user_id = $_SESSION['userData']['id'];
		$N_authcode = $_SESSION['userData']['auth_code'];
		//INSERT
		$N_doccat_id = mysql_real_escape_string($_POST['doccat_id']);		
		$N_name = mysql_real_escape_string($_POST['name']);
		$N_desc = mysql_real_escape_string($_POST['desc']);
		if(isset($_POST['feature'])){
			$N_feature = mysql_real_escape_string($_POST['feature']);
		}else{
			$N_feature = 0;
		}
	
		if($obj_user->check_code($N_authcode, $N_user_id) && $N_doccat_id != '' && $N_name != ''){//check code
			if(!empty($_FILES)){
				$allowed_ext = array('doc', 'docx', 'xls', 'xlsx', 'ppt', 'pptx', 'pdf');
				$file_name	 = time().rand(0,10).cleanSpace($_FILES['FILE']['name']);
				$file_ext	 = strtolower(end(explode('.', $file_name)));
				//$file_type	 = $_FILES['FILE']['type'];
				$file_size	 = $_FILES['FILE']['size'];
				$file_tmp	 = $_FILES['FILE']['tmp_name'];
				
				if(in_array($file_ext, $allowed_ext) === true){
					if($file_size < 10044070){
						$file_loc = 'docs/user/'.$file_name;
						move_uploaded_file($file_tmp, $file_loc);
						$result = $obj_doc->insert_data($file_loc, $N_name, $N_desc, "COVER", $N_doccat_id, $N_user_id, $file_ext, $file_size, $N_feature, "Publish");
						if($result){	
							$R_message = array("status" => "200", "message" => "Success insert document");
						}else{
							$R_message = array("status" => "404", "message" => "Failed insert document");
						}
					}else{
						$R_message = array("status" => "404", "message" => "ERROR: file size max 10 MB!");
					}
				}else{
					$R_message = array("status" => "404", "message" => "ERROR: extension file invalid!");
				}
			}else{
				$R_message = array("status" => "404", "message" => "Upload file is empty");
			}
		}//check code
		else{
			$R_message = array("status" => "401", "message" => "Unauthorized");
		}

		$obj_connect->down();
		echo json_encode($R_message);
	}//END INSERT DOC

	else if($_GET['action'] == 'update_doc' && isset($_SESSION['userData']['id']) && isset($_SESSION['userData']['auth_code'])){//START UPDATE DOC
		$obj_connect->up();

		$N_user_id = $_SESSION['userData']['id'];
		$N_authcode = $_SESSION['userData']['auth_code'];
		//UPDATE
		$N_id = mysql_real_escape_string($_POST['id']);	
		$N_doccat_id = mysql_real_escape_string($_POST['doccat_id']);		
		$N_name = mysql_real_escape_string($_POST['name']);
		$N_desc = mysql_real_escape_string($_POST['desc']);
		if(isset($_POST['feature'])){
			$N_feature = mysql_real_escape_string($_POST['feature']);
		}else{
			$N_feature = 0;
		}
		$N_publish = mysql_real_escape_string($_POST['publish']);
	
		if($obj_user->check_code($N_authcode, $N_user_id) && $N_doccat_id != '' && $N_name != ''){//check code
			if(!empty($_FILES)){
				$allowed_ext = array('doc', 'docx', 'xls', 'xlsx', 'ppt', 'pptx', 'pdf');
				$file_name	 = time().rand(0,10).cleanSpace($_FILES['FILE']['name']);
				$file_ext	 = strtolower(end(explode('.', $file_name)));
				//$file_type	 = $_FILES['FILE']['type'];
				$file_size	 = $_FILES['FILE']['size'];
				$file_tmp	 = $_FILES['FILE']['tmp_name'];
				
				if(in_array($file_ext, $allowed_ext) === true){
					if($file_size < 10044070){
						$file_loc = 'docs/user/'.$file_name; //save document in database
						move_uploaded_file($file_tmp, $file_loc);
						$result = $obj_doc->update_data_doc($N_id, $file_loc, $N_name, $N_desc, "COVER", $N_doccat_id, $file_ext, $file_size, $N_feature, $N_publish);
						if($result){	
							$R_message = array("status" => "200", "message" => "Success update document");
						}else{
							$R_message = array("status" => "404", "message" => "Failed update document");
						}
					}else{
						$R_message = array("status" => "404", "message" => "ERROR: file size max 10 MB!");
					}
				}else{
					$R_message = array("status" => "404", "message" => "ERROR: extension file invalid!");
				}
			}else{
				$result = $obj_doc->update_data($N_id, $N_name, $N_desc, "COVER", $N_doccat_id, $N_feature, $N_publish);
				if($result){	
					$R_message = array("status" => "200", "message" => "Success update document");
				}else{
					$R_message = array("status" => "404", "message" => "Failed update document");
				}
			}
		}//check code
		else{
			$R_message = array("status" => "401", "message" => "Unauthorized");
		}
		$_SESSION['docstat'] = "Success update document";
		$obj_connect->down();
		echo json_encode($R_message);
	}//END UPDATE DOC

	else if($_GET['action'] == 'delete_doc' && isset($_SESSION['userData']['id']) && isset($_SESSION['userData']['auth_code'])){//START DELETE DOC
		$obj_connect->up();

		$N_user_id = $_SESSION['userData']['id'];
		$N_authcode = $_SESSION['userData']['auth_code'];
		//UPDATE
		$N_id = mysql_real_escape_string($_POST['id']);	
	
		if($obj_user->check_code($N_authcode, $N_user_id) && $N_id != ''){//check code
			$result = $obj_doc->delete_data($N_id);
			if($result){	
				$R_message = array("status" => "200", "message" => "Document has been deleted");
			}else{
				$R_message = array("status" => "404", "message" => "Document failed to be deleted");
			}
		}//check code
		else{
			$R_message = array("status" => "401", "message" => "Unauthorized");
		}
		$_SESSION['docstat'] = "Success delete document";
		$obj_connect->down();
		echo json_encode($R_message);
	}//END DELETE DOC

	else{
		echo "error";
	}			
}//end gate
?>