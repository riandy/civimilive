<?php 
if(isset($_GET['action'])){//START GATE
	require_once("../model/Connection.php");
	$obj_con = new Connection();

	require_once("../model/User.php");
	$obj_user = new User();

	require_once("../model/Setting.php");
	$obj_set = new Setting();

	//savve sort by Cliff, 25 Aug 2015
	if($_GET['action'] == 'save_sort' && isset($_REQUEST['user_id']) && isset($_REQUEST['auth_code'])){//START save sort
		$obj_con->up();
		$R_message = array("status" => "404", "message" => "Not Found");
		$N_user_id = mysql_real_escape_string($_REQUEST['user_id']);
		$N_authcode = mysql_real_escape_string($_REQUEST['auth_code']);
		$N_array = $_REQUEST['data'];
		$N_key = array("obj", "edu","exp", "ski", "cert", "lang", "org", "ach");//
				
		$message = ""; $count = 0;
		if($obj_user->check_code($N_authcode, $N_user_id)){//check code
			for($tt = 0;$tt < count($N_array); $tt++){
				$result = $obj_set->save_sort($N_user_id, $N_key[$tt], $N_array[$tt]);
				$message .= "{$N_key[$tt]} : {$N_array[$tt]}, "; 
				$count += $result;
			}
			
			if($count > 1){
				$R_message = array("status" => "200", "message" => $message);				
			}
		}//check code
		else{
			$R_message = array("status" => "401", "message" => "Unauthorized");
		}
		$obj_con->down();	

		header("Content-Type: application/json");
		echo json_encode($R_message);	
	}//END save sort

	else{
		echo "error";
	}

}//END GATE
?>