<?php 
header('content-type: application/json');
header("access-control-allow-origin: *");

if(isset($_GET['action'])){//start gate
	
	require_once("../model/Connection.php");
	$obj_connect = new Connection();
	
	require_once("../model/Skill.php");
	$obj_skill = new Skill();

	require_once("../model/AT_Skill.php");
	$obj_ateskill = new AT_Skill();

	require_once("../model/User.php");
	$obj_user = new User();
	
	$obj_connect->up();//OPEN CONNECTION
	
	if($_GET['action'] == 'autocomplete' && isset($_GET['term'])){
		$obj_connect->up();	
		$R_message = array("status" => "400", "message" => "No Data");
		$term = mysql_real_escape_string($_GET['term']);
		$result = $obj_skill->get_list($term);
		if($result){
			$R_message = array("status" => "200", "message" => "Data Exist", "data" => $result);
		}
		$obj_connect->down();	
		echo json_encode($R_message);
	}

	else if($_GET['action'] == 'get_data' && isset($_REQUEST['user_id']) && isset($_REQUEST['auth_code'])){//START GET DATA
		$obj_connect->up();	
		$R_message = array("status" => "400", "message" => "No Data");

		$N_user_id = mysql_real_escape_string($_REQUEST['user_id']);
		$N_authcode = mysql_real_escape_string($_REQUEST['auth_code']);
		
		if($obj_user->check_code($N_authcode, $N_user_id)){//check code
			$result = $obj_ateskill->get_data($N_user_id);
			//var_dump($result);
			if(is_array($result)){
				$R_message = array("status" => "200", "message" => "Data Exist", "data" => $result);
			}
		}//check code
		else{
			$R_message = array("status" => "401", "message" => "Unauthorized");
		}

		$obj_connect->down();	
		header("Content-Type: application/json");
		echo json_encode($R_message);	
	}//END GET DATA

	else if($_GET['action'] == 'update' && isset($_REQUEST['user_id']) && isset($_REQUEST['auth_code'])){//START insert_education
		$obj_connect->up();	

		$N_user_id = mysql_real_escape_string($_REQUEST['user_id']);
		$N_authcode = mysql_real_escape_string($_REQUEST['auth_code']);
		
		//INSERT INTO T_Skill
		$N_name = mysql_real_escape_string($_REQUEST['name']);
		//INSERT INTO AT_Skill
		$N_id = mysql_real_escape_string($_REQUEST['id']);
		$N_desc = mysql_real_escape_string($_REQUEST['desc']);
		$N_rate = mysql_real_escape_string($_REQUEST['rate']);

		if($obj_user->check_code($N_authcode, $N_user_id) && $N_name != ''){//check code
			$check_exist = $obj_skill->check_skill($N_name);//CHECK IF SKILL EXIST
			//var_dump($check_exist);
			if($check_exist == 0){//IF SKILL NOT EXIST, INSERT
				$result_skill = $obj_skill->insert_data($N_name, "Publish", 0);
				if($result_skill){
					if($N_id != ''){//UPDATE
						$result_update = $obj_ateskill->update_data($N_id, $result_skill, $N_user_id, $N_desc, $N_rate);
						if($result_update){	
							$R_message = array("status" => "200", "message" => "Success insert skill1");
						}else{
							$R_message = array("status" => "404", "message" => "Failed insert skill1");
						}
					}else{//INSERT NEW
						$result_ateskill = $obj_ateskill->insert_data($result_skill, $N_user_id, $N_desc, $N_rate);
						//var_dump($result_ateskill);
						if($result_ateskill == 1){	
							$R_message = array("status" => "200", "message" => "Success insert skill2");
						}else{
							$R_message = array("status" => "404", "message" => "Failed insert skill2");
						}
					}
				}else{
					$R_message = array("status" => "404", "message" => "Failed insert skill3");
				}
			}else{//IF THE SKILL EXIST, GO AHEAD AND INSERT INTO AT_Skill
				$check_primary = $obj_ateskill->check_primary($check_exist, $N_user_id);//CHECK IF PRIMARY EXIST
				//var_dump($check_primary);
				if($N_id != ''){//UPDATE
					$result_update = $obj_ateskill->update_data($N_id, $check_exist, $N_user_id, $N_desc, $N_rate);
					if($result_update){
						$R_message = array("status" => "200", "message" => "Success insert skill4");
					}else{
						$R_message = array("status" => "404", "message" => "Failed insert skill4");
					}
				}else{//INSERT NEW
					if($check_primary == 0){
						$result_insert = $obj_ateskill->insert_data($check_exist, $N_user_id, $N_desc, $N_rate);
						//var_dump($result_insert);
						if($result_insert){
							$R_message = array("status" => "200", "message" => "Success insert skill5");
						}else{
							$R_message = array("status" => "404", "message" => "Failed insert skill5");
						}
					}else{
						$result_update = $obj_ateskill->update_data($check_exist, $N_user_id, $N_desc, $N_rate);
						if($result_update){
							$R_message = array("status" => "200", "message" => "Success insert skill6");
						}else{
							$R_message = array("status" => "404", "message" => "Failed insert skill6");
						}
					}
				}
			}
		}//check code
		else{
			$R_message = array("status" => "401", "message" => "Unauthorized");
		}

		$obj_connect->down();
		header("Content-Type: application/json");
		echo json_encode($R_message);	
	}//END INSERT EDUCATION

	else if($_GET['action'] == 'delete' && isset($_REQUEST['user_id']) && isset($_REQUEST['auth_code'])){//START DELETE
		$obj_connect->up();	
		$N_user_id = mysql_real_escape_string($_REQUEST['user_id']);
		$N_authcode = mysql_real_escape_string($_REQUEST['auth_code']);
		$N_id = mysql_real_escape_string($_REQUEST['id']);

		if($obj_user->check_code($N_authcode, $N_user_id)){//check code
			$result = $obj_ateskill->delete_data($N_id, $N_user_id);
			if($result == 1){
				$data_skill = $obj_ateskill->get_data($N_user_id);
				$R_message = array("status" => "200", "message" => "Skill has been deleted", "data" => $data_skill);
			}else{
				$R_message = array("status" => "404", "message" => "Skill failed to be deleted");	
			}
		}//check code
		else{
			$R_message = array("status" => "401", "message" => "Unauthorized");
		}

		$obj_connect->down();
		echo json_encode($R_message);
	}//END DELETE

	else{
		echo "error";
	}
	
	$obj_connect->down();//CLOSE CONNECTION
}//end gate

?>