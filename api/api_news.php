<?php 
header('content-type: application/json');
header("access-control-allow-origin: *");

if(isset($_GET['action'])){//start gate
	
	require_once("../model/Connection.php");
	$obj_con = new Connection();
	
	require_once("../model/News.php");
	$obj_news = new News();
	
	$obj_con->up();//OPEN CONNECTION
	
	if($_GET['action'] == 'get_data_by_page' && $_REQUEST['page'] > 0){
		$R_message = array("status" => "404", "message" => "No Data");		
		$O_page = mysql_real_escape_string($_REQUEST['page']);

		$result = $obj_news->get_data_by_page($O_page);
		if(is_array($result)){
			$R_message = array("status" => "200", "message" => "Success");
			$R_message['data'] = $result;
		}
		
		echo json_encode($R_message);	
	}

	else{
		echo "error";
	}
	
	$obj_con->down();//CLOSE CONNECTION
}//end gate
?>