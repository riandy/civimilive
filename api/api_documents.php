<?php 
header('content-type: application/json');
header("access-control-allow-origin: *");

if(isset($_GET['action'])){//start gate
	
	require_once("../model/Connection.php");
	$obj_connect = new Connection();
	
	require_once("../model/DocCat.php");
	$obj_dc = new DocCat();

	require_once("../model/Doc.php");
	$obj_doc = new Doc();

	require_once("../model/User.php");
	$obj_user = new User();
	
	$obj_connect->up();//OPEN CONNECTION
	
// START ACTION FOR DOC CAT
	if($_GET['action'] == 'get_doc_cat' && isset($_REQUEST['user_id']) && isset($_REQUEST['auth_code'])){//START GET DATA
		$obj_connect->up();	
		$R_message = array("status" => "400", "message" => "No Data");

		$N_user_id = mysql_real_escape_string($_REQUEST['user_id']);
		$N_authcode = mysql_real_escape_string($_REQUEST['auth_code']);
		// parameter sort by
		$N_sort = mysql_real_escape_string($_REQUEST['sort']);
		
		if($obj_user->check_code($N_authcode, $N_user_id)){//check code
			$result = $obj_dc->get_doc_cat($N_user_id, $N_sort);
			//var_dump($result);
			if(is_array($result)){
				$R_message = array("status" => "200", "message" => "Data Exist", "data" => $result);
			}
		}//check code
		else{
			$R_message = array("status" => "401", "message" => "Unauthorized");
		}

		$obj_connect->down();	
		echo json_encode($R_message);	
	}//END GET DATA

	else if($_GET['action'] == 'insert_doc_cat' && isset($_REQUEST['user_id']) && isset($_REQUEST['auth_code'])){//START insert_doc_cat
		$obj_connect->up();	

		$N_user_id = mysql_real_escape_string($_REQUEST['user_id']);
		$N_authcode = mysql_real_escape_string($_REQUEST['auth_code']);
		//INSERT
		$N_name = mysql_real_escape_string($_REQUEST['name']);
		$N_desc = mysql_real_escape_string($_REQUEST['desc']);

		if($obj_user->check_code($N_authcode, $N_user_id) && $N_name != ''){//check code
			$result = $obj_dc->insert_data($N_user_id, $N_name, $N_desc, "Publish");
			if($result){	
				$R_message = array("status" => "200", "message" => "Success insert document category");
			}else{
				$R_message = array("status" => "404", "message" => "Failed insert document category");
			}
		}//check code
		else{
			$R_message = array("status" => "401", "message" => "Unauthorized");
		}

		$obj_connect->down();
		echo json_encode($R_message);	
	}//END INSERT DOC CAT

	else if($_GET['action'] == 'update_doc_cat' && isset($_REQUEST['user_id']) && isset($_REQUEST['auth_code'])){//START update_doc_cat
		$obj_connect->up();	

		$N_user_id = mysql_real_escape_string($_REQUEST['user_id']);
		$N_authcode = mysql_real_escape_string($_REQUEST['auth_code']);
		//UPDATE
		$N_id = mysql_real_escape_string($_REQUEST['id']);
		$N_name = mysql_real_escape_string($_REQUEST['name']);
		$N_desc = mysql_real_escape_string($_REQUEST['desc']);

		if($obj_user->check_code($N_authcode, $N_user_id) && $N_name != ''){//check code
			$result = $obj_dc->update_data($N_id, $N_name, $N_desc);
			if($result){	
				$R_message = array("status" => "200", "message" => "Success update document category");
			}else{
				$R_message = array("status" => "404", "message" => "Failed update document category");
			}
		}//check code
		else{
			$R_message = array("status" => "401", "message" => "Unauthorized");
		}

		$obj_connect->down();
		echo json_encode($R_message);	
	}//END UPDATE DOC CAT

	else if($_GET['action'] == 'delete_doc_cat' && isset($_REQUEST['user_id']) && isset($_REQUEST['auth_code'])){//START DELETE DOC CAT
		$obj_connect->up();	
		$N_user_id = mysql_real_escape_string($_REQUEST['user_id']);
		$N_authcode = mysql_real_escape_string($_REQUEST['auth_code']);
		$N_id = mysql_real_escape_string($_REQUEST['id']);

		if($obj_user->check_code($N_authcode, $N_user_id)){//check code
			$result = $obj_dc->delete_data($N_id);
			//var_dump($result);
			if($result == 1){
				$R_message = array("status" => "200", "message" => "Document category has been deleted");
			}else{
				$R_message = array("status" => "404", "message" => "Document category failed to be deleted");	
			}
		}//check code
		else{
			$R_message = array("status" => "401", "message" => "Unauthorized");
		}

		$obj_connect->down();
		echo json_encode($R_message);
	}//END DELETE DOC CAT
// END ACTION FOR DOC CAT

// START ACTION FOR DOC
	else if($_GET['action'] == 'get_doc' && isset($_REQUEST['user_id']) && isset($_REQUEST['auth_code'])){//START GET DATA BY DOC CAT
		$obj_connect->up();	
		$R_message = array("status" => "400", "message" => "No Data");

		$N_user_id = mysql_real_escape_string($_REQUEST['user_id']);
		$N_authcode = mysql_real_escape_string($_REQUEST['auth_code']);
		$N_doccat_id = mysql_real_escape_string($_REQUEST['doccat_id']);
		
		if($obj_user->check_code($N_authcode, $N_user_id)){//check code
			$result = $obj_doc->get_data_by_doc_cat($N_doccat_id);
			//var_dump($result);
			if(is_array($result)){
				$R_message = array("status" => "200", "message" => "Data Exist", "data" => $result);
			}
		}//check code
		else{
			$R_message = array("status" => "401", "message" => "Unauthorized");
		}

		$obj_connect->down();	
		echo json_encode($R_message);	
	}//END GET DATA BY DOC CAT

	/*else if($_GET['action'] == 'insert_doc' && isset($_REQUEST['user_id']) && isset($_REQUEST['auth_code'])){//START insert_doc
		$obj_connect->up();	

		$N_user_id = mysql_real_escape_string($_REQUEST['user_id']);
		$N_authcode = mysql_real_escape_string($_REQUEST['auth_code']);
		//INSERT
		$N_doccat_id = mysql_real_escape_string($_REQUEST['doccat_id']);		
		$N_title = mysql_real_escape_string($_REQUEST['title']);
		$N_desc = mysql_real_escape_string($_REQUEST['desc']);
		$N_feature = mysql_real_escape_string($_REQUEST['feature']);
		$N_file = mysql_real_escape_string($_REQUEST['file']);

		if($obj_user->check_code($N_authcode, $N_user_id) && $N_doccat_id != '' && $N_title != ''){//check code
			$result = $obj_doc->insert_data($N_file, $N_title, $N_desc, "COVER", $N_doccat_id, $N_user_id, "file", "123", $N_feature, "Publish");
			if($result){	
				$R_message = array("status" => "200", "message" => "Success insert document");
			}else{
				$R_message = array("status" => "404", "message" => "Failed insert document");
			}
		}//check code
		else{
			$R_message = array("status" => "401", "message" => "Unauthorized");
		}

		$obj_connect->down();
		echo json_encode($R_message);	
	}//END INSERT DOC

	else if($_GET['action'] == 'update_doc' && isset($_REQUEST['user_id']) && isset($_REQUEST['auth_code'])){//START update_doc
		$obj_connect->up();	

		$N_user_id = mysql_real_escape_string($_REQUEST['user_id']);
		$N_authcode = mysql_real_escape_string($_REQUEST['auth_code']);
		//INSERT
		$N_id = mysql_real_escape_string($_REQUEST['id']);
		$N_doccat_id = mysql_real_escape_string($_REQUEST['doccat_id']);		
		$N_file = mysql_real_escape_string($_REQUEST['file']);
		$N_title = mysql_real_escape_string($_REQUEST['title']);
		$N_desc = mysql_real_escape_string($_REQUEST['desc']);
		$N_type = mysql_real_escape_string($_REQUEST['type']);
		$N_size = mysql_real_escape_string($_REQUEST['filesize']);
		$N_feature = mysql_real_escape_string($_REQUEST['feature']);

		if($obj_user->check_code($N_authcode, $N_user_id) && $N_id != '' && $N_title != ''){//check code
			$result = $obj_doc->update_data($N_id, $N_file, $N_title, $N_desc, $N_doccat_id, $N_type, $N_size, $N_feature);
			if($result){	
				$R_message = array("status" => "200", "message" => "Success update document");
			}else{
				$R_message = array("status" => "404", "message" => "Failed update document");
			}
		}//check code
		else{
			$R_message = array("status" => "401", "message" => "Unauthorized");
		}

		$obj_connect->down();
		echo json_encode($R_message);	
	}//END UPDATE DOC

	else if($_GET['action'] == 'delete_doc' && isset($_REQUEST['user_id']) && isset($_REQUEST['auth_code'])){//START DELETE DOC
		$obj_connect->up();	
		$N_user_id = mysql_real_escape_string($_REQUEST['user_id']);
		$N_authcode = mysql_real_escape_string($_REQUEST['auth_code']);
		$N_id = mysql_real_escape_string($_REQUEST['id']);

		if($obj_user->check_code($N_authcode, $N_user_id)){//check code
			$result = $obj_doc->delete_data($N_id);
			if($result == 1){
				$R_message = array("status" => "200", "message" => "Document has been deleted");
			}else{
				$R_message = array("status" => "404", "message" => "Document failed to be deleted");	
			}
		}//check code
		else{
			$R_message = array("status" => "401", "message" => "Unauthorized");
		}

		$obj_connect->down();
		echo json_encode($R_message);
	}//END DELETE DOC*/
// END ACTION FOR DOC

	else{
		echo "error";
	}
	
	$obj_connect->down();//CLOSE CONNECTION
}//end gate

?>