<?php 
header('content-type: application/json');
header("access-control-allow-origin: *");
//require_once(dirname(__FILE__)."/../civimi/packages/SimpleImage.php");

if(isset($_GET['action'])){//start gate
	
	require_once("../model/Connection.php");
	$obj_connect = new Connection();
	
	require_once("../model/Album.php");
	$obj_album = new Album();

	require_once("../model/Photo.php");
	$obj_photo = new Photo();

	require_once("../model/User.php");
	$obj_user = new User();
	
	$obj_connect->up();//OPEN CONNECTION
	
// START ACTION FOR ALBUM
	if($_GET['action'] == 'get_album' && isset($_REQUEST['user_id']) && isset($_REQUEST['auth_code'])){//START GET DATA
		$obj_connect->up();	
		$R_message = array("status" => "400", "message" => "No Data");

		$N_user_id = mysql_real_escape_string($_REQUEST['user_id']);
		$N_authcode = mysql_real_escape_string($_REQUEST['auth_code']);
		// parameter sort by
		$N_sort = mysql_real_escape_string($_REQUEST['sort']);
		
		if($obj_user->check_code($N_authcode, $N_user_id)){//check code
			$result = $obj_album->get_album($N_user_id, $N_sort);
			//var_dump($result);
			if(is_array($result)){
				$R_message = array("status" => "200", "message" => "Data Exist", "data" => $result);
			}
		}//check code
		else{
			$R_message = array("status" => "401", "message" => "Unauthorized");
		}

		$obj_connect->down();	
		echo json_encode($R_message);	
	}//END GET DATA

	else if($_GET['action'] == 'insert_album' && isset($_REQUEST['user_id']) && isset($_REQUEST['auth_code'])){//START insert_album
		$obj_connect->up();	

		$N_user_id = mysql_real_escape_string($_REQUEST['user_id']);
		$N_authcode = mysql_real_escape_string($_REQUEST['auth_code']);
		//INSERT
		$N_name = mysql_real_escape_string($_REQUEST['name']);
		$N_desc = mysql_real_escape_string($_REQUEST['desc']);

		if($obj_user->check_code($N_authcode, $N_user_id) && $N_name != ''){//check code
			$result_album = $obj_album->insert_data($N_user_id, $N_name, $N_desc, "Publish");
			if($result_album){	
				$R_message = array("status" => "200", "message" => "Success insert album");
			}else{
				$R_message = array("status" => "404", "message" => "Failed insert album");
			}
		}//check code
		else{
			$R_message = array("status" => "401", "message" => "Unauthorized");
		}

		$obj_connect->down();
		echo json_encode($R_message);	
	}//END INSERT ALBUM

	else if($_GET['action'] == 'update_album' && isset($_REQUEST['user_id']) && isset($_REQUEST['auth_code'])){//START update_album
		$obj_connect->up();	

		$N_user_id = mysql_real_escape_string($_REQUEST['user_id']);
		$N_authcode = mysql_real_escape_string($_REQUEST['auth_code']);
		//UPDATE
		$N_id = mysql_real_escape_string($_REQUEST['id']);
		$N_name = mysql_real_escape_string($_REQUEST['name']);
		$N_desc = mysql_real_escape_string($_REQUEST['desc']);

		if($obj_user->check_code($N_authcode, $N_user_id) && $N_id != '' && $N_name != ''){//check code
			$result_album = $obj_album->update_data($N_id, $N_name, $N_desc, "Publish");
			if($result_album){	
				$R_message = array("status" => "200", "message" => "Success update album");
			}else{
				$R_message = array("status" => "404", "message" => "Failed update album");
			}
		}//check code
		else{
			$R_message = array("status" => "401", "message" => "Unauthorized");
		}

		$obj_connect->down();
		echo json_encode($R_message);	
	}//END UPDATE ALBUM

	else if($_GET['action'] == 'delete_album' && isset($_REQUEST['user_id']) && isset($_REQUEST['auth_code'])){//START DELETE ALBUM
		$obj_connect->up();	
		$N_user_id = mysql_real_escape_string($_REQUEST['user_id']);
		$N_authcode = mysql_real_escape_string($_REQUEST['auth_code']);
		$N_id = mysql_real_escape_string($_REQUEST['id']);

		if($obj_user->check_code($N_authcode, $N_user_id)){//check code
			$result = $obj_album->delete_data($N_id);
			//var_dump($result);
			if($result == 1){
				$R_message = array("status" => "200", "message" => "Album has been deleted");
			}else{
				$R_message = array("status" => "404", "message" => "Album failed to be deleted");	
			}
		}//check code
		else{
			$R_message = array("status" => "401", "message" => "Unauthorized");
		}

		$obj_connect->down();
		echo json_encode($R_message);
	}//END DELETE ALBUM
// END ACTION FOR ALBUM

// START ACTION FOR PHOTO
	else if($_GET['action'] == 'get_photo' && isset($_REQUEST['user_id']) && isset($_REQUEST['auth_code'])){//START GET DATA BY ALBUM
		$obj_connect->up();	
		$R_message = array("status" => "400", "message" => "No Data");

		$N_user_id = mysql_real_escape_string($_REQUEST['user_id']);
		$N_authcode = mysql_real_escape_string($_REQUEST['auth_code']);
		$N_album_id = mysql_real_escape_string($_REQUEST['album_id']);
		
		if($obj_user->check_code($N_authcode, $N_user_id)){//check code
			$result = $obj_photo->get_data_by_album($N_album_id);
			//var_dump($result);
			if(is_array($result)){
				$R_message = array("status" => "200", "message" => "Data Exist", "data" => $result);
			}
		}//check code
		else{
			$R_message = array("status" => "401", "message" => "Unauthorized");
		}

		$obj_connect->down();	
		// header("Content-Type: application/json");
		echo json_encode($R_message);	
	}//END GET DATA BY ALBUM

	else if($_GET['action'] == 'update_photo' && isset($_REQUEST['user_id']) && isset($_REQUEST['auth_code'])){//START update_photo
		$obj_connect->up();	

		$N_user_id = mysql_real_escape_string($_REQUEST['user_id']);
		$N_authcode = mysql_real_escape_string($_REQUEST['auth_code']);
		//UPDATE
		$N_id = mysql_real_escape_string($_REQUEST['id']);
		$N_title = mysql_real_escape_string($_REQUEST['title']);

		if($obj_user->check_code($N_authcode, $N_user_id) && $N_id != '' && $N_title != ''){//check code
			$result = $obj_photo->update_data($N_id, $N_title, "Publish");
			if($result == 1){
				$R_message = array("status" => "200", "message" => "Success update photo(s)");
			}else{
				$R_message = array("status" => "404", "message" => "Failed update photo(s)");
			}
		}//check code
		else{
			$R_message = array("status" => "401", "message" => "Unauthorized");
		}

		$obj_connect->down();
		// header("Content-Type: application/json");
		echo json_encode($R_message);	
	}//END UPDATE PHOTO

	else if($_GET['action'] == 'set_primary' && isset($_REQUEST['user_id']) && isset($_REQUEST['auth_code'])){//START set_primary
		$obj_connect->up();	

		$N_user_id = mysql_real_escape_string($_REQUEST['user_id']);
		$N_authcode = mysql_real_escape_string($_REQUEST['auth_code']);
		//UPDATE
		$N_id = mysql_real_escape_string($_REQUEST['id']);
		$N_album_id = mysql_real_escape_string($_REQUEST['album_id']);
		$N_primary = mysql_real_escape_string($_REQUEST['primary']);

		if($obj_user->check_code($N_authcode, $N_user_id) && $N_id != '' && $N_album_id != ''){//check code
			$result = $obj_photo->set_primary($N_id, $N_album_id, $N_primary);
			if($result == 1){
				$R_message = array("status" => "404", "message" => "Success set primary photo(s)");
			}else{
				$R_message = array("status" => "404", "message" => "Failed set primary photo(s)");
			}
		}//check code
		else{
			$R_message = array("status" => "401", "message" => "Unauthorized");
		}

		$obj_connect->down();
		header("Content-Type: application/json");
		echo json_encode($R_message);	
	}//END SET PRIMARY

	else if($_GET['action'] == 'delete_photo' && isset($_REQUEST['user_id']) && isset($_REQUEST['auth_code'])){//START DELETE PHOTO
		$obj_connect->up();	
		$N_user_id = mysql_real_escape_string($_REQUEST['user_id']);
		$N_authcode = mysql_real_escape_string($_REQUEST['auth_code']);
		$N_id = mysql_real_escape_string($_REQUEST['id']);

		if($obj_user->check_code($N_authcode, $N_user_id)){//check code
			$result = $obj_photo->delete_data($N_id);
			if($result == 1){
				$R_message = array("status" => "200", "message" => "Photo has been deleted");
			}else{
				$R_message = array("status" => "404", "message" => "Photo failed to be deleted");	
			}
		}//check code
		else{
			$R_message = array("status" => "401", "message" => "Unauthorized");
		}

		$obj_connect->down();
		echo json_encode($R_message);
	}//END DELETE PHOTO
// END ACTION FOR PHOTO

// START ACTION FOR GET DETAIL
	else if($_GET['action'] == 'get_data_detail' && isset($_REQUEST['id'])){//START GET DETAIL
		$obj_connect->up();	
		$R_message = array("status" => "400", "message" => "No Data");

		$N_id = mysql_real_escape_string(check_input($_REQUEST['id']));
		
		$result = $obj_photo->api_get_data_detail($N_id);
		//var_dump($result);
		if(is_array($result)){
			$R_message = array("status" => "200", "message" => "Data Exist", "data" => $result);
		}

		$obj_connect->down();	
		echo json_encode($R_message);	
	}//END GET DATA DETAIL
// END ACTION FOR GET DETAIL

	else{
		echo "error";
	}
	
	$obj_connect->down();//CLOSE CONNECTION
}//end gate

?>