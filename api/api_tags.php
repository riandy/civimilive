<?php 

if(isset($_GET['action'])){//START GATE
	require_once("../model/Connection.php");
	$obj_connect = new Connection();

	require_once("../model/Tagging.php");
	$obj_tagging = new Tagging();

	require_once("../model/Ref_Tag.php");
	$obj_rtag = new Ref_Tag();

	if($_GET['action'] == 'get_data'){//START get TAGS
	$obj_connect->up();	

		$R_message = null;
		$target_id = mysql_real_escape_string($_GET['table_id']);
		$table_name = mysql_real_escape_string($_GET['table_name']);

		$result = $obj_tagging->get_data($table_name, $table_id);
		if($result != null){
			$R_message = $result;
		}
		echo $R_message;
		//header('Content-Type: application/json');
	$obj_connect->down();	
	}//END GET TAGS

	else if($_GET['action'] == 'insert_data' && isset($_POST['table_id']) && isset($_POST['table_name'])){//START insert_tags
		$obj_connect->up();	

		$R_message = array("status" => "0", "message" => "Failed insert tags");
		$target_id = mysql_real_escape_string($_POST['table_id']);
		$table_name = mysql_real_escape_string($_POST['table_name']);
		$data = ($_POST['data']);
		$message = array();

		$result_delete_tag = $obj_tagging->delete_data($target_id, $table_name);
		if($result_delete_tag){
			//delete succeeds
		}

			for($i = 0;$i < count($data);$i++){//START FOR
				$check_rtag_id = $obj_tagging->check_exist($data[$i]);//CHECK IF TAG EXIST, IF SO RETURN THE rtag_id, else returns 0

				if($check_rtag_id == 0){//IF THE TAG IS NOT EXIST
					$result_rtag = $obj_rtag->insert_data(strip_tags($data[$i]));
					if($result_rtag >= 1){
						$message[$i] = "Success to create tag {$data[$i]}.";
						$result_tag = $obj_tagging->insert_data($target_id, $result_rtag, $table_name);
						if($result_tag){
							$message[$i] .= "Success to attach tag {$data[$i]}.";
						}
					}else{
						$message[$i] = "Failed to create tag {$data[$i]}.";
					}
				}else{//IF THE TAG EXIST, GO AHEAD AND INSERT INTO tbl_tagging
					$message[$i] = "Tag {$data[$i]} already exist. ";
					$result_tag = $obj_tagging->insert_data($target_id, $check_rtag_id, $table_name);
					if($result_tag){
						$message[$i] .= "Success to insert {$data[$i]}";
					}else{
						$message[$i] .= "Failed to insert {$data[$i]}";
					}
				}
				$R_message = array("status" => "1", "message" => $message);
			}//END FOR
			
	$obj_connect->down();

	header("Content-Type: application/json");
	echo json_encode($R_message);	
	}//END INSERT_TAGS

	else if($_GET['action'] == 'update_data' && isset($_REQUEST['table_id']) && isset($_REQUEST['table_name'])){//START update_tags
		$obj_connect->up();	

		$R_message = array("status" => "0", "message" => "Failed insert tags");
		$N_target_id = mysql_real_escape_string($_REQUEST['table_id']);
		$N_table_name = mysql_real_escape_string($_REQUEST['table_name']);
		$N_data = $_REQUEST['data'];
		if($N_data != ''){
			$data = explode(",", $N_data);
		}
		$message = array();

		//delete tags
		$result_delete_tag = $obj_tagging->delete_data($N_target_id, $N_table_name);
        if($result_delete_tag){
           	//delete succeeds
        }
        //start input tags
		if($N_target_id){
            if(isset($data)){
                for($i = 0;$i < count($data);$i++){//START FOR
                    $check_rtag_id = $obj_tagging->check_exist($data[$i]);//CHECK IF TAG EXIST, IF SO RETURN THE rtag_id, else returns 0

                    if($check_rtag_id == 0){//IF THE TAG IS NOT EXIST
                        $result_rtag = $obj_rtag->insert_data(strip_tags($data[$i]));
                        if($result_rtag >= 1){
                            $message[$i] = "Success to create tag {$data[$i]}.";
                            $result_tag = $obj_tagging->insert_data($N_table_name, $N_target_id, $result_rtag);
                            if($result_tag){
                                $message[$i] .= "Success to attach tag {$data[$i]}.";
                            }
                        }else{
                            $message[$i] = "Failed to create tag {$data[$i]}.";
                        }
                    }else{//IF THE TAG EXIST, GO AHEAD AND INSERT INTO tbl_tagging
                        $message[$i] = "Tag {$data[$i]} already exist. ";
                        $result_tag = $obj_tagging->insert_data($N_table_name, $N_target_id, $check_rtag_id);
                        if($result_tag){
                            $message[$i] .= "Success to insert {$data[$i]}";
                        }else{
                            $message[$i] .= "Failed to insert {$data[$i]}";
                        }
                    }
                    $R_message = array("status" => "1", "message" => $message);
                }//END FOR
            }
        }
		
		$obj_connect->down();
		header("Content-Type: application/json");
		echo json_encode($R_message);	
	}//END UPDATE_TAGS

	else if($_GET['action'] == 'autocomplete' && isset($_GET['term'])){//start autocomplete
		$obj_connect->up();

		$term = mysql_real_escape_string($_GET['term']);
		$result = $obj_rtag->get_list($term);

		$obj_connect->down();		
		header("Content-Type: application/json");
		echo json_encode($result);
	}//END autocomplete

	else if($_GET['action'] == 'get_data_tags' && isset($_REQUEST['table_name']) && $_REQUEST['table_id']){//START get data tags
		$obj_connect->up();	
		$R_message = array("status" => "400", "message" => "No Data");

		$table_id = mysql_real_escape_string($_GET['table_id']);
		$table_name = mysql_real_escape_string($_GET['table_name']);

		$result = $obj_tagging->get_data_tags($table_name, $table_id);
		//var_dump($result);
		if(is_array($result)){
			$R_message = array("status" => "200", "message" => "Data Exist", "data" => $result);
		}

		header("Content-Type: application/json");
		echo json_encode($R_message);
		$obj_connect->down();	
	}//END get data tags

}//END GATE
?>