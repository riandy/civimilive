<?php 
header('content-type: application/json');
header("access-control-allow-origin: *");
require_once(dirname(__FILE__)."/../packages/check_input.php");

if(isset($_GET['action'])){//start gate
	
	require_once("../model/Connection.php");
	$obj_con = new Connection();
	
	require_once("../model/News.php");
	$obj_news = new News();

	require_once("../model/News_Category.php");
	$obj_nc = new News_Category();

	require_once("../model/Hits_User.php");
	$obj_hu = new Hits_User();
	
	if($_GET['action'] == 'get_data_by_page' && $_REQUEST['page'] > 0){
		$obj_con->up();
		$R_message = array("status" => "404", "message" => "No Data");
		
		$O_page = mysql_real_escape_string($_REQUEST['page']);
		$O_category_id = mysql_real_escape_string($_REQUEST['category_id']);
		$O_sort = mysql_real_escape_string($_REQUEST['sort']);

		$result = $obj_news->get_news_mobile($O_page, $O_category_id, $O_sort);
		if(is_array($result)){
			$R_message = array("status" => "200", "message" => "Success");
			$R_message['data'] = $result;
		}
		
		$obj_con->down();
		echo json_encode($R_message);	
	}

	else if($_GET['action'] == 'get_data_by_detail' && isset($_REQUEST['news_id'])){
		$obj_con->up();
		$R_message = array("status" => "404", "message" => "No Data");
		
		$N_id = mysql_real_escape_string($_REQUEST['news_id']);
		$N_view_userID = 0;
		if(isset($_REQUEST['user_id'])){
			$N_view_userID = mysql_real_escape_string($_REQUEST['user_id']);	
		}

		$result = $obj_news->get_news_detail_mobile($N_id);
		if(is_array($result)){
			//start for save hits user
		    $N_ip = getIP(); //get ip
		    $userGeoData = getGeoIP($N_ip); //get data by ip address
		    $N_city = $userGeoData->city; //ip city
		    $N_country = $userGeoData->country_name; //ip country
		                
		    if($N_ip != "" && $N_city != "" && $N_country != ""){
		        $obj_hu->insert_data("", "", "mobile", "news", $N_id, $N_view_userID, $N_ip, $N_city, $N_country);  
		    }
		    //end for save hits user

			$R_message = array("status" => "200", "message" => "Success");
			$R_message['data'] = $result;
		}
		
		$obj_con->down();
		echo json_encode($R_message);	
	}

	else if($_GET['action'] == 'get_category'){
		$obj_con->up();
		$R_message = array("status" => "404", "message" => "No Data");
		
		$result = $obj_nc->get_index_mobile();
		if(is_array($result)){
			$R_message = array("status" => "200", "message" => "Success");
			$R_message['data'] = $result;
		}
		
		$obj_con->down();
		echo json_encode($R_message);	
	}

	else{
		echo "error";
	}
	
}//end gate

?>