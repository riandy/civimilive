<?php 
header('content-type: application/json');
header("access-control-allow-origin: *");

if(isset($_GET['action'])){//start gate
	
	require_once("../model/Connection.php");
	$obj_connect = new Connection();
	
	require_once("../model/Edu.php");
	$obj_edu = new Edu();

	require_once("../model/AT_Edu.php");
	$obj_atedu = new AT_Edu();

	require_once("../model/User.php");
	$obj_user = new User();
	
	$obj_connect->up();//OPEN CONNECTION
	
	if($_GET['action'] == 'autocomplete' && isset($_GET['term'])){
		$obj_connect->up();	
		$R_message = array("status" => "400", "message" => "No Data");
		$term = mysql_real_escape_string($_GET['term']);
		$result = $obj_edu->get_list($term);
		if($result){
			$R_message = array("status" => "200", "message" => "Data Exist", "data" => $result);
		}
		$obj_connect->down();	
		echo json_encode($R_message);
	}

	else if($_GET['action'] == 'get_data' && isset($_REQUEST['user_id']) && isset($_REQUEST['auth_code'])){//START GET DATA
		$obj_connect->up();	
		$R_message = array("status" => "400", "message" => "No Data");

		$N_user_id = mysql_real_escape_string($_REQUEST['user_id']);
		$N_authcode = mysql_real_escape_string($_REQUEST['auth_code']);
		
		if($obj_user->check_code($N_authcode, $N_user_id)){//check code
			$result = $obj_atedu->get_data($N_user_id);
			//var_dump($result);
			if(is_array($result)){
				$R_message = array("status" => "200", "message" => "Data Exist", "data" => $result);
			}
		}//check code
		else{
			$R_message = array("status" => "401", "message" => "Unauthorized");
		}

		$obj_connect->down();	
		header("Content-Type: application/json");
		echo json_encode($R_message);	
	}//END GET DATA

	//else if($_GET['action'] == 'update' && isset($_REQUEST['name']) && isset($_REQUEST['city']) && isset($_REQUEST['country'])){//START insert_education
	else if($_GET['action'] == 'update' && isset($_REQUEST['user_id']) && isset($_REQUEST['auth_code'])){//START insert_education
		$obj_connect->up();	

		$N_user_id = mysql_real_escape_string($_REQUEST['user_id']);
		$N_authcode = mysql_real_escape_string($_REQUEST['auth_code']);
		
		//INSERT INTO T_Edu
		$N_name = mysql_real_escape_string($_REQUEST['name']);
		$N_city = mysql_real_escape_string($_REQUEST['city']);
		$N_country = mysql_real_escape_string($_REQUEST['country']);
		//INSERT INTO AT_Edu
		$N_id = mysql_real_escape_string($_REQUEST['id']);
		$N_degree = mysql_real_escape_string($_REQUEST['degree']);
		$N_major = mysql_real_escape_string($_REQUEST['major']);
		$N_honors = mysql_real_escape_string($_REQUEST['honors']);
		$N_fromMonth = mysql_real_escape_string($_REQUEST['from_month']);
		$N_fromYear = mysql_real_escape_string($_REQUEST['from_year']);
		$N_toMonth = mysql_real_escape_string($_REQUEST['to_month']);
		$N_toYear = mysql_real_escape_string($_REQUEST['to_year']);
		$N_gpa = mysql_real_escape_string($_REQUEST['gpa']);
		$N_desc = mysql_real_escape_string($_REQUEST['desc']);
		$N_status = mysql_real_escape_string($_REQUEST['status']);

		if($obj_user->check_code($N_authcode, $N_user_id) && $N_name != '' && $N_city != '' && $N_country != ''){//check code
			$check_exist = $obj_edu->check_school($N_name, $N_city, $N_country);//CHECK IF SCHOOL EXIST
			if($check_exist == 0){//IF SCHOOL NOT EXIST, INSERT
				$result_edu = $obj_edu->insert_data($N_name, $N_city, $N_country, "", "", "", "", 0, "Publish");
				if($result_edu){
					if($N_id != ''){//UPDATE
						$result_update = $obj_atedu->update_data($N_id, $result_edu, $N_degree, $N_major, $N_honors, $N_fromMonth, $N_fromYear, $N_toMonth, $N_toYear, $N_gpa, $N_desc, $N_status);
						if($result_update){	
							$R_message = array("status" => "200", "message" => "Success insert education1");
						}else{
							$R_message = array("status" => "404", "message" => "Failed insert education1");
						}
					}else{//INSERT NEW
						$result_atedu = $obj_atedu->insert_data($result_edu, $N_user_id, $N_degree, $N_major, $N_honors, $N_fromMonth, $N_fromYear, $N_toMonth, $N_toYear, $N_gpa, $N_desc, $N_status);
						if($result_atedu){	
							$R_message = array("status" => "200", "message" => "Success insert education2");
						}else{
							$R_message = array("status" => "404", "message" => "Failed insert education2");
						}
					}
				}else{
					$R_message = array("status" => "404", "message" => "Failed insert education3");
				}
			}else{//IF THE SCHOOL EXIST, GO AHEAD AND INSERT INTO AT_Edu
				if($N_id != ''){//UPDATE
					$result_update = $obj_atedu->update_data($N_id, $check_exist, $N_degree, $N_major, $N_honors, $N_fromMonth, $N_fromYear, $N_toMonth, $N_toYear, $N_gpa, $N_desc, $N_status);
					if($result_update){
						$R_message = array("status" => "200", "message" => "Success insert education4");
					}else{
						$R_message = array("status" => "404", "message" => "Failed insert education4");
					}
				}else{//INSERT NEW
					$result_insert = $obj_atedu->insert_data($check_exist, $N_user_id, $N_degree, $N_major, $N_honors, $N_fromMonth, $N_fromYear, $N_toMonth, $N_toYear, $N_gpa, $N_desc, $N_status);
					if($result_insert){
						$R_message = array("status" => "200", "message" => "Success insert education5");
					}else{
						$R_message = array("status" => "404", "message" => "Failed insert education5");
					}
				}	
			}
		}//check code
		else{
			$R_message = array("status" => "401", "message" => "Unauthorized");
		}

		$obj_connect->down();
		header("Content-Type: application/json");
		echo json_encode($R_message);	
	}//END INSERT EDUCATION

	//else if($_GET['action'] == 'delete' && isset($_GET['id'])){//START DELETE
	else if($_GET['action'] == 'delete' && isset($_REQUEST['user_id']) && isset($_REQUEST['auth_code'])){//START DELETE
		$obj_connect->up();	
		$N_user_id = mysql_real_escape_string($_REQUEST['user_id']);
		$N_authcode = mysql_real_escape_string($_REQUEST['auth_code']);
		$N_id = mysql_real_escape_string($_REQUEST['id']);

		if($obj_user->check_code($N_authcode, $N_user_id)){//check code
			$result = $obj_atedu->delete_data($N_id);
			if($result == 1){
				$R_message = array("status" => "200", "message" => "Education has been deleted");
			}else{
				$R_message = array("status" => "404", "message" => "Education failed to be deleted");	
			}
		}//check code
		else{
			$R_message = array("status" => "401", "message" => "Unauthorized");
		}

		$obj_connect->down();
		echo json_encode($R_message);
	}//END DELETE

	else if($_GET['action'] == 'get_data_detail' && isset($_REQUEST['atedu_ID'])){//START GET DATA DETAIL
		$obj_connect->up();	
		$R_message = array("status" => "400", "message" => "No Data");

		$N_id = mysql_real_escape_string($_REQUEST['atedu_ID']);
		
		$result = $obj_atedu->get_data_detail($N_id);
		//var_dump($result);
		if(is_array($result)){
			$R_message = array("status" => "200", "message" => "Data Exist", "data" => $result);
		}

		$obj_connect->down();	
		header("Content-Type: application/json");
		echo json_encode($R_message);	
	}//END GET DATA

	else if($_GET['action'] == 'get_data_all'){//START GET DATA ALL EDU
		$obj_connect->up();	
		$R_message = array("status" => "400", "message" => "No Data");
		$term = mysql_real_escape_string($_GET['q']);
		$result = $obj_edu->get_data_all($term);
		//var_dump($result);
		if(is_array($result)){
			$R_message = array("status" => "200", "message" => "Data Exist", "data" => $result);
		}

		$obj_connect->down();	
		header("Content-Type: application/json");
		echo json_encode($R_message);	
	}//END GET DATA

	else{
		echo "error";
	}
	
	$obj_connect->down();//CLOSE CONNECTION
}//end gate

?>