<?php 
header('content-type: application/json');
header("access-control-allow-origin: *");

if(isset($_GET['action'])){//start gate
	
	require_once("../model/Connection.php");
	$obj_connect = new Connection();
	
	require_once("../model/VideoCat.php");
	$obj_vc = new VideoCat();

	require_once("../model/Video.php");
	$obj_video = new Video();

	require_once("../model/User.php");
	$obj_user = new User();
	
	$obj_connect->up();//OPEN CONNECTION
	
// START ACTION FOR Video Category
	if($_GET['action'] == 'get_video_cat' && isset($_REQUEST['user_id']) && isset($_REQUEST['auth_code'])){//START GET DATA
		$obj_connect->up();	
		$R_message = array("status" => "400", "message" => "No Data");

		$N_user_id = mysql_real_escape_string($_REQUEST['user_id']);
		$N_authcode = mysql_real_escape_string($_REQUEST['auth_code']);
		// parameter sort by
		$N_sort = mysql_real_escape_string($_REQUEST['sort']);
		
		if($obj_user->check_code($N_authcode, $N_user_id)){//check code
			$result = $obj_vc->get_video_cat($N_user_id, $N_sort);
			//var_dump($result);
			if(is_array($result)){
				$R_message = array("status" => "200", "message" => "Data Exist", "data" => $result);
			}
		}//check code
		else{
			$R_message = array("status" => "401", "message" => "Unauthorized");
		}

		$obj_connect->down();	
		echo json_encode($R_message);	
	}//END GET DATA

	else if($_GET['action'] == 'insert_video_cat' && isset($_REQUEST['user_id']) && isset($_REQUEST['auth_code'])){//START insert_video_cat
		$obj_connect->up();	

		$N_user_id = mysql_real_escape_string($_REQUEST['user_id']);
		$N_authcode = mysql_real_escape_string($_REQUEST['auth_code']);
		//INSERT
		$N_name = mysql_real_escape_string($_REQUEST['name']);
		$N_desc = mysql_real_escape_string($_REQUEST['desc']);

		if($obj_user->check_code($N_authcode, $N_user_id) && $N_name != ''){//check code
			$result = $obj_vc->insert_data($N_user_id, $N_name, $N_desc, "Publish");
			if($result){	
				$R_message = array("status" => "200", "message" => "Success insert video category");
			}else{
				$R_message = array("status" => "404", "message" => "Failed insert video category");
			}
		}//check code
		else{
			$R_message = array("status" => "401", "message" => "Unauthorized");
		}

		$obj_connect->down();
		echo json_encode($R_message);	
	}//END INSERT VIDEO CAT

	else if($_GET['action'] == 'update_video_cat' && isset($_REQUEST['user_id']) && isset($_REQUEST['auth_code'])){//START update_video_cat
		$obj_connect->up();	

		$N_user_id = mysql_real_escape_string($_REQUEST['user_id']);
		$N_authcode = mysql_real_escape_string($_REQUEST['auth_code']);
		//UPDATE
		$N_id = mysql_real_escape_string($_REQUEST['id']);
		$N_name = mysql_real_escape_string($_REQUEST['name']);
		$N_desc = mysql_real_escape_string($_REQUEST['desc']);

		if($obj_user->check_code($N_authcode, $N_user_id) && $N_name != ''){//check code
			$result = $obj_vc->update_data($N_id, $N_user_id, $N_name, $N_desc, "Publish");
			if($result){	
				$R_message = array("status" => "200", "message" => "Success update video category");
			}else{
				$R_message = array("status" => "404", "message" => "Failed update video category");
			}
		}//check code
		else{
			$R_message = array("status" => "401", "message" => "Unauthorized");
		}

		$obj_connect->down();
		echo json_encode($R_message);	
	}//END UPDATE CAT

	else if($_GET['action'] == 'delete_video_cat' && isset($_REQUEST['user_id']) && isset($_REQUEST['auth_code'])){//START DELETE VIDEO CAT
		$obj_connect->up();	
		$N_user_id = mysql_real_escape_string($_REQUEST['user_id']);
		$N_authcode = mysql_real_escape_string($_REQUEST['auth_code']);
		$N_id = mysql_real_escape_string($_REQUEST['id']);

		if($obj_user->check_code($N_authcode, $N_user_id)){//check code
			$result = $obj_vc->delete_data($N_id);
			//var_dump($result);
			if($result == 1){
				$R_message = array("status" => "200", "message" => "Video category has been deleted");
			}else{
				$R_message = array("status" => "404", "message" => "Video category failed to be deleted");	
			}
		}//check code
		else{
			$R_message = array("status" => "401", "message" => "Unauthorized");
		}

		$obj_connect->down();
		echo json_encode($R_message);
	}//END DELETE VIDEO CAT
// END ACTION FOR VIDEO CAT

// START ACTION FOR VIDEO
	else if($_GET['action'] == 'get_video' && isset($_REQUEST['user_id']) && isset($_REQUEST['auth_code'])){//START GET DATA BY VIDEO CAT
		$obj_connect->up();	
		$R_message = array("status" => "400", "message" => "No Data");

		$N_user_id = mysql_real_escape_string($_REQUEST['user_id']);
		$N_authcode = mysql_real_escape_string($_REQUEST['auth_code']);
		$N_videocat_id = mysql_real_escape_string($_REQUEST['videocat_id']);
		
		if($obj_user->check_code($N_authcode, $N_user_id)){//check code
			$result = $obj_video->get_data_by_video_cat($N_videocat_id);
			//var_dump($result);
			if(is_array($result)){
				$R_message = array("status" => "200", "message" => "Data Exist", "data" => $result);
			}
		}//check code
		else{
			$R_message = array("status" => "401", "message" => "Unauthorized");
		}

		$obj_connect->down();	
		echo json_encode($R_message);	
	}//END GET DATA BY VIDEO CAT

	else if($_GET['action'] == 'insert_video' && isset($_REQUEST['user_id']) && isset($_REQUEST['auth_code'])){//START insert_video
		$obj_connect->up();	

		$N_user_id = mysql_real_escape_string($_REQUEST['user_id']);
		$N_authcode = mysql_real_escape_string($_REQUEST['auth_code']);
		//INSERT
		$N_videocat_id = mysql_real_escape_string($_REQUEST['videocat_id']);
		$N_link = mysql_real_escape_string($_REQUEST['link']);
		$N_title = mysql_real_escape_string($_REQUEST['title']);
		$N_desc = mysql_real_escape_string($_REQUEST['desc']);

		if($obj_user->check_code($N_authcode, $N_user_id) && $N_link != '' && $N_title != ''){//check code
			$result = $obj_video->insert_data($N_link, $N_title, $N_desc, $N_videocat_id, $N_user_id, "Publish");
			if($result){	
				$R_message = array("status" => "200", "message" => "Success insert video");
			}else{
				$R_message = array("status" => "404", "message" => "Failed insert video");
			}
		}//check code
		else{
			$R_message = array("status" => "401", "message" => "Unauthorized");
		}

		$obj_connect->down();
		echo json_encode($R_message);	
	}//END INSERT VIDEO

	else if($_GET['action'] == 'update_video' && isset($_REQUEST['user_id']) && isset($_REQUEST['auth_code'])){//START update_video
		$obj_connect->up();	

		$N_user_id = mysql_real_escape_string($_REQUEST['user_id']);
		$N_authcode = mysql_real_escape_string($_REQUEST['auth_code']);
		//INSERT
		$N_id = mysql_real_escape_string($_REQUEST['id']);
		$N_videocat_id = mysql_real_escape_string($_REQUEST['videocat_id']);
		$N_link = mysql_real_escape_string($_REQUEST['link']);
		$N_title = mysql_real_escape_string($_REQUEST['title']);
		$N_desc = mysql_real_escape_string($_REQUEST['desc']);

		if($obj_user->check_code($N_authcode, $N_user_id) && $N_id != '' && $N_link != '' && $N_title != ''){//check code
			$result = $obj_video->update_data($N_id, $N_link, $N_title, $N_desc, $N_videocat_id, $N_user_id, "Publish");
			if($result){	
				$R_message = array("status" => "200", "message" => "Success update video");
			}else{
				$R_message = array("status" => "404", "message" => "Failed update video");
			}
		}//check code
		else{
			$R_message = array("status" => "401", "message" => "Unauthorized");
		}

		$obj_connect->down();
		echo json_encode($R_message);	
	}//END UPDATE VIDEO

	else if($_GET['action'] == 'delete_video' && isset($_REQUEST['user_id']) && isset($_REQUEST['auth_code'])){//START DELETE VIDEO
		$obj_connect->up();	
		$N_user_id = mysql_real_escape_string($_REQUEST['user_id']);
		$N_authcode = mysql_real_escape_string($_REQUEST['auth_code']);
		$N_id = mysql_real_escape_string($_REQUEST['id']);

		if($obj_user->check_code($N_authcode, $N_user_id)){//check code
			$result = $obj_video->delete_data($N_id);
			if($result == 1){
				$R_message = array("status" => "200", "message" => "Video has been deleted");
			}else{
				$R_message = array("status" => "404", "message" => "Video failed to be deleted");	
			}
		}//check code
		else{
			$R_message = array("status" => "401", "message" => "Unauthorized");
		}

		$obj_connect->down();
		echo json_encode($R_message);
	}//END DELETE VIDEO
// END ACTION FOR VIDEO

	else{
		echo "error";
	}
	
	$obj_connect->down();//CLOSE CONNECTION
}//end gate

?>