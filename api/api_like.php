<?php 
require_once(dirname(__FILE__)."/../civimi/packages/front_config.php");
require_once(dirname(__FILE__)."/../civimi/packages/check_input.php");

header('content-type: application/json');
header("access-control-allow-origin: *");

if(isset($_GET['action'])){//start gate
	
	require_once("../model/Connection.php");
	$obj_connect = new Connection();
	
	require_once("../model/Like.php");
	$obj_like = new Like();

	require_once("../model/User.php");
	$obj_user = new User();
	
	$obj_connect->up();//OPEN CONNECTION
	
	if($_GET['action'] == 'get_total_like' && isset($_REQUEST['type']) && isset($_REQUEST['type_id']))
	{
		$R_message = array("status" => "400", "message" => "No Data");

		$N_type = mysql_real_escape_string($_REQUEST['type']);
		$N_type_id = mysql_real_escape_string($_REQUEST['type_id']);
		
		$result = $obj_like->get_total_like($N_type, $N_type_id);
		//var_dump($result);
		if(is_array($result)){
			$R_message = array("status" => "200", "message" => "Data Exist", "data" => $result);
		}
			
		echo json_encode($R_message);	
	}

	else if($_GET['action'] == 'update' && isset($_REQUEST['view_user']) && isset($_REQUEST['auth_code']))
	{
		//START FUNCTION SEND MAIL FOR INFROMATION LIKE
	    function send_email($tujuan, $fname, $lname, $bio, $pro_photo, $country, $img, $img_title, $fname_viewer, $country_viewer, $photo_id, $tag){
	    	if($country == "Indonesia"){
				//LIKE CARD NOTIFICATION 
	    		$lang['card-subject'] = "Selamat, seseorang menyukai karya di profile CIVIMI anda!";
				$lang['card-welcome'] = "Halo";
				$lang['card-welcome_text'] = "Seseorang dari";
				$lang['card-welcome_text2'] = "menyukai karya anda!";
				$lang['card-closing'] = "Tunjukkan semangat anda<br/>Temukan pekerjaan dan dapat ditemukan!";
				$lang['card-notification_link'] = "terlalu banyak notifikasi?klik disini";
			}else{
				//LIKE CARD NOTIFICATION 
				$lang['card-subject'] = "Congrats, someone likes a work in your CIVIMI Profile!";
				$lang['card-welcome'] = "Congratulation";
				$lang['card-welcome_text'] = "Someone from";
				$lang['card-welcome_text2'] = "liked your work";
				$lang['card-closing'] = "Show your passion<br/>Discover work and be discovered!";
				$lang['card-notification_link'] = "too many notifications? click here";
			}
	    	if($img_title != ""){$title = $img_title;}else{$title = "No title";}

	        //mail
	        $to = $tujuan; 
	        $subject = $lang['card-subject'];
	        $data_tag = explode(",", $tag);

	        $message = "<html><head></head><body style='margin:0;'>";
			$message .= "<div id='all' style='width:600px;margin:0 auto;'>";
			$message .= "<div id='notif-content'>";
			$message .= "<div id='header' style='text-align: left;padding: 10px;'>";
			$message .= "<a target='_blank' href='http://www.civimi.com'>";
			$message .= "<img src='http://www.civimi.com/beta/civimi/img/logo/logo-desktop.png' style='width:150px;' alt='civimi'>";
			$message .= "</a></div><div id='content'><div style='padding: 15px 50px 30px 50px;'>";
			$message .= "<div style='display: inline-block;width: 70%;vertical-align: top;'>";
			$message .= "<div style='font-weight: bold;color: #5A5A5A;font-size: 16px;'>".$lang['card-welcome']." ".$fname."</div>";
			$message .= "<div style='font-weight: bold;color: #5A5A5A;font-size: 16px;margin: 5px 0 0 0;'>".$lang['card-welcome_text']." ".$country_viewer." ".$lang['card-welcome_text2']."</div>";
			$message .= "</div><div style='display: inline-block;width: 30%;margin-left: -4px;'>";
			$message .= "<div style='text-align: right;'><img src='http://www.civimi.com/beta/civimi/img/notif-heart.png' style='width: 70px;' alt='civimi'></div>";
			$message .= "</div></div>";

			$message .= "<div style='padding: 30px 100px;'>";
			$message .= "<div id='notif-profil' style='border: solid 1px #B5B5B5;padding-bottom: 10px;'>";
			$message .= "<div style='padding: 10px;''>";
			$message .= "<div style='display: table-cell;vertical-align: top;'><img src='".check_image_url($pro_photo)."' style='width: 30px;' alt='civimi'></div>";
			$message .= "<div style='display: table-cell;vertical-align: bottom;padding-left: 5px;font-weight: bold;font-size: 16px;color: #5A5A5F;'>".$fname." ".$lname."</div>";
			$message .= "</div>";
			$message .= "<div style='font-weight: bold;color: #4E5F69;font-size: 14px;padding: 0 10px;'>".$bio."</div>";
			$message .= "<div style='padding: 5px 10px 0 10px;color: #5A5A5F;font-size: 14px;'>".$country."</div>";
			$message .= "<div style='text-align: center;padding-top:10px;'><img src='".check_image_url($img)."' style='width:100%;' alt='civimi'></div>";
			$message .= "<div style='padding: 5px 10px 0 10px;font-size: 14px;color: #5F5A5A;font-weight: bold;'>".$title."</div>";
			$message .= "<div style='padding: 10px 10px 0 10px; display: flex;'>";
            $message .= "<ul style='margin: 0; padding: 0;'>";
            for($i=0; $i<count($data_tag); $i++){
            $message .= "<li style='display:inline-block;padding: 0 10px 10px 0;margin: 0 !important;'>";
          	$message .= "<span style='display: block;padding: 2px 12px;background-color: #0199d9;color: #fff;border-radius: 3px;font-weight: bold;font-size: 12px;font-family: sans-serif!important;letter-spacing: 1px;'>".$data_tag[$i]."</span>";
          	$message .= "</li>";
          	}
          	$message .= "</ul>";
          	$message .= "</div>";
			$message .= "</div>";
			$message .= "</div>";

			$message .= "<div style='text-align: center;padding-bottom: 30px;display: block;'><a href='http://www.civimi.com/beta/portfolio/card/".$photo_id.".html?source=email-like-notif' style='display: inline-block;text-decoration: none !important;outline: none !important;color: #FFFFFF;font-weight: bold;font-size: 16px;padding: 10px 20px;background-color: #19A0D9;'>View in Civimi</a></div>";
			$message .= "</div></div>";
			$message .= "<div style='text-align: center;font-size: 18px;font-weight: bold;color: #666;padding: 10px 0 20px 0;'>".$lang['card-closing']."</div>";
			$message .= "<div id='footer' style='background-color: #666;padding: 10px 0;'>";
			$message .= "<div style='display: block;height: 50px;padding: 0 20px;position: relative;'>";
			$message .= "<div style='font-size: 20px;font-style: italic;color: #FFFFFF;font-weight: bold;display: inline-block;width: 50%;'>'Reinvent Yourself'</div>";
			$message .= "<div style='display: inline-block;width: 50%;position: relative;text-align: right;'>";
			$message .= "<a target='_blank' href='https://www.facebook.com/civimi'><img src='http://www.civimi.com/beta/civimi/img/logo/facebook.png' alt='facebook' style='width: 20px;'></a>";
			$message .= "<a target='_blank' href='https://twitter.com/civimi'><img src='http://www.civimi.com/beta/civimi/img/logo/twitter.png' alt='google' style='width: 20px;padding-left:8px;'></a>";
			$message .= "<a target='_blank' href='https://plus.google.com/+Civimi'><img src='http://www.civimi.com/beta/civimi/img/logo/google.png' alt='twitter' style='width: 20px;padding-left:8px;'></a>";
			$message .= "</div></div>";
			$message .= "<div style='text-align:left;padding: 0 20px;padding-top: 20px;'><a target='_blank' href='' style='font-size:12px;color: #FFFFFF;outline:none !important;text-decoration:underline;'>".$lang['card-notification_link']."</a></div>";
			$message .= "<div style='text-align:center;font-size: 13px;color: #FFFFFF;padding:5px 20px 0 20px;position:relative;'>";
			$message .= "Copyright &copy; 2016 Civimi. All rights reserved.";
			$message .= "</div></div></div></body></html>";

	        // Always set content-type when sending HTML email
	        $headers = "MIME-Version: 1.0" . "\r\n";
	        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

	        // More headers
	        $headers .= "From: <no-reply@civimi.com>" . "\r\n";
	        $headers .= "bcc: support@civimi.com" . "\r\n";
	        $sent = mail($to, $subject, $message, $headers);
	        return $sent; 
	    }
	    //END FUNCTION SEND MAIL FOR INFROMATION LIKE

		$N_view_user = mysql_real_escape_string($_REQUEST['view_user']); //check code
		$N_authcode = mysql_real_escape_string($_REQUEST['auth_code']); //check code

		$N_user_id = mysql_real_escape_string($_REQUEST['user_id']);
		$N_username = mysql_real_escape_string($_REQUEST['username']);
		$N_page = mysql_real_escape_string($_REQUEST['page']);
		$N_type = mysql_real_escape_string($_REQUEST['type']);
		$N_type_id = mysql_real_escape_string($_REQUEST['type_id']);
		$N_tag = mysql_real_escape_string($_REQUEST['tag']);
		$N_ip = mysql_real_escape_string($_REQUEST['ip']);
		$N_ip_city = mysql_real_escape_string($_REQUEST['ip_city']);
		$N_ip_country = mysql_real_escape_string($_REQUEST['ip_country']);

		if($obj_user->check_code($N_authcode, $N_view_user)){//check code
			$check_exist = $obj_like->check_exist($N_type, $N_type_id, $N_view_user);
			if($check_exist == 0){
				$result = $obj_like->insert_data($N_user_id, $N_username, $N_page, $N_type, $N_type_id, $N_view_user, $N_ip, $N_ip_city, $N_ip_country);
				if($result){
					$info_user = $obj_like->get_info_photo_user($result);
					if($N_view_user != $N_user_id){
						$send = send_email($info_user[0]['User_email'], $info_user[0]['User_fname'], $info_user[0]['User_lname'], $info_user[0]['User_bioDesc'], $info_user[0]['User_proPhotoThmb'], $info_user[0]['User_country'], $info_user[0]['Photo_imgLink'], $info_user[0]['Photo_title'], $info_user[0]['User_fnameview'], $info_user[0]['User_countryview'],  $info_user[0]['Photo_ID'], $N_tag);
					}
					//message
					$R_message = array("status" => "200", "message" => "insert like success");
				}else{
					$R_message = array("status" => "400", "message" => "insert like failed");
				}
			}else{
				$result = $obj_like->delete_data($N_type, $N_type_id, $N_view_user);
				if($result == 1){
					$R_message = array("status" => "200", "message" => "Undo like success");
				}else{
					$R_message = array("status" => "400", "message" => "Undo like failed");	
				}
			}
		}//check code
		else{
			$R_message = array("status" => "401", "message" => "Unauthorized");
		}

		echo json_encode($R_message);
	}

	else if($_GET['action'] == 'check_like' && isset($_REQUEST['view_user']) && isset($_REQUEST['auth_code']))
	{
		$N_view_user = mysql_real_escape_string($_REQUEST['view_user']); //check code
		$N_authcode = mysql_real_escape_string($_REQUEST['auth_code']); //check code

		$N_type = mysql_real_escape_string($_REQUEST['type']);
		$N_type_id = mysql_real_escape_string($_REQUEST['type_id']);
		
		if($obj_user->check_code($N_authcode, $N_view_user)){//check code
			$check_exist = $obj_like->check_exist($N_type, $N_type_id, $N_view_user);
			if($check_exist == 0){
				$R_message = array("status" => "404", "message" => "Not like");
			}else{
				$R_message = array("status" => "200", "message" => "Liked");	
			}
		}//check code
		else{
			$R_message = array("status" => "401", "message" => "Unauthorized");
		}

		echo json_encode($R_message);
	}

	else
	{
		echo "error";
	}
	
	$obj_connect->down();//CLOSE CONNECTION
}//end gate

?>