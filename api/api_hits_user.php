<?php 
header('content-type: application/json');
header("access-control-allow-origin: *");

if(isset($_GET['action'])){//start gate
	
	require_once("../model/Connection.php");
	$obj_connect = new Connection();
	
	require_once("../model/Hits_User.php");
	$obj_hu = new Hits_User();
	
	$obj_connect->up();//OPEN CONNECTION
	
	if($_GET['action'] == 'insert' && isset($_REQUEST['username']))
	{
		$N_user_id = mysql_real_escape_string($_REQUEST['user_id']);
		$N_username = mysql_real_escape_string($_REQUEST['username']);
		$N_page = mysql_real_escape_string($_REQUEST['page']);
		$N_type = mysql_real_escape_string($_REQUEST['type']);
		$N_type_id = mysql_real_escape_string($_REQUEST['type_id']);
		$N_view_user = mysql_real_escape_string($_REQUEST['view_user']);
		$N_ip = mysql_real_escape_string($_REQUEST['ip']);
		$N_ip_city = mysql_real_escape_string($_REQUEST['ip_city']);
		$N_ip_country = mysql_real_escape_string($_REQUEST['ip_country']);
		
		$result = $obj_hu->insert_data($N_user_id, $N_username, $N_page, $N_type, $N_type_id, $N_view_user, $N_ip, $N_ip_city, $N_ip_country);
		if($result == 1){
			$R_message = array("status" => "200", "message" => "insert hits user success");
		}else{
			$R_message = array("status" => "400", "message" => "insert hits user failed");
		}
		
		echo json_encode($R_message);
	}
	else
	{
		echo "error";
	}
	
	$obj_connect->down();//CLOSE CONNECTION
}//end gate

?>