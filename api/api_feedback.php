<?php 
header('content-type: application/json');
header("access-control-allow-origin: *");

if(isset($_GET['action'])){//start gate

	if($_GET['action'] == 'post_feedback')
	{
		function send_email($tujuan, $email, $name, $device, $pesan){
		    //mail
		    $to = $tujuan; 
		    $subject = "Feedback from ".$name;

		    $message = "
		    <!DOCTYPE html>
		    <html>
		    <head>
		    <title></title>
		    </head>
		    <body>
			<span style='font-size:12px;'><em>*This is a visitor data</em></span>
			<br/><br/>
			<b>Visitor Name</b> : $name
			<br/><br/>
			<b>Visitor E-mail</b> : $email
			<br/><br/>
			<b>Visitor Device</b> : <br/> 
			$device
			<br/><br/>
			<b>Message</b> : <br/>
			".str_replace('\r\n','<br/>',$pesan)."

			<br/><br/><br/>
			<em>*Do not reply to this e-mail.<br/><br/>Thank you!</em>
		    </body>
		    </html>
		    ";

		    // Always set content-type when sending HTML email
		    $headers = "MIME-Version: 1.0" . "\r\n";
		    $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

		    // More headers
		    $headers .= "From: <donotreply@civimi.com>" . "\r\n";

		    $result = mail($to,$subject,$message,$headers);
		    return $result;	
	    }

	    //param
		$N_name = urldecode($_REQUEST['name']);
		$N_email = urldecode($_REQUEST['email']);
		$N_device = urldecode($_REQUEST['device']);
		$N_message = urldecode($_REQUEST['message']);
		$N_toEmail = "support@civimi.com";
		
		if(isset($N_name) && isset($N_email) && isset($N_message)){
			$result = send_email($N_toEmail, $N_email, $N_name, $N_device, $N_message);
			if($result){
				$R_message = array("status" => "200", "message" => "Post feedback success");	
			}else{
				$R_message = array("status" => "404", "message" => "Post feedback failed");
			}						
		}
		else{
			$R_message = array("status" => "401", "message" => "Unauthorized");
		}

		echo json_encode($R_message);	
	}

	else{
		echo "error";
	}
}//end gate

?>