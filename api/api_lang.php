<?php 
header('content-type: application/json');
header("access-control-allow-origin: *");

if(isset($_GET['action'])){//start gate
	
	require_once("../model/Connection.php");
	$obj_connect = new Connection();
	
	require_once("../model/Lang.php");
	$obj_lang = new Lang();

	require_once("../model/AT_Lang.php");
	$obj_atelang = new AT_Lang();

	require_once("../model/User.php");
	$obj_user = new User();
	
	$obj_connect->up();//OPEN CONNECTION
	
	if($_GET['action'] == 'autocomplete' && isset($_GET['term'])){
		$obj_connect->up();	
		$R_message = array("status" => "400", "message" => "No Data");
		$term = mysql_real_escape_string($_GET['term']);
		$result = $obj_lang->get_list($term);
		if($result){
			$R_message = array("status" => "200", "message" => "Data Exist", "data" => $result);
		}
		$obj_connect->down();	
		echo json_encode($R_message);
	}

	else if($_GET['action'] == 'get_data' && isset($_REQUEST['user_id']) && isset($_REQUEST['auth_code'])){//START GET DATA
		$obj_connect->up();	
		$R_message = array("status" => "400", "message" => "No Data");

		$N_user_id = mysql_real_escape_string($_REQUEST['user_id']);
		$N_authcode = mysql_real_escape_string($_REQUEST['auth_code']);
		
		if($obj_user->check_code($N_authcode, $N_user_id)){//check code
			$result = $obj_atelang->get_data($N_user_id);
			//var_dump($result);
			if(is_array($result)){
				$R_message = array("status" => "200", "message" => "Data Exist", "data" => $result);
			}
		}//check code
		else{
			$R_message = array("status" => "401", "message" => "Unauthorized");
		}

		$obj_connect->down();	
		header("Content-Type: application/json");
		echo json_encode($R_message);	
	}//END GET DATA

	else if($_GET['action'] == 'update' && isset($_REQUEST['user_id']) && isset($_REQUEST['auth_code'])){//START insert_language
		$obj_connect->up();	

		$N_user_id = mysql_real_escape_string($_REQUEST['user_id']);
		$N_authcode = mysql_real_escape_string($_REQUEST['auth_code']);
		
		//INSERT INTO T_Lang
		$N_name = mysql_real_escape_string($_REQUEST['name']);
		//INSERT INTO AT_Lang
		$N_id = mysql_real_escape_string($_REQUEST['id']);
		$N_listen = mysql_real_escape_string($_REQUEST['listen']);
		$N_speak = mysql_real_escape_string($_REQUEST['speak']);
		$N_read = mysql_real_escape_string($_REQUEST['read']);
		$N_write = mysql_real_escape_string($_REQUEST['write']);
		$N_desc = mysql_real_escape_string($_REQUEST['desc']);

		if($obj_user->check_code($N_authcode, $N_user_id) && $N_name != ''){//check code
			$check_exist = $obj_lang->check_lang($N_name);//CHECK IF SKILL EXIST
			//var_dump($check_exist);
			if($check_exist == 0){//IF SKILL NOT EXIST, INSERT
				$result_lang = $obj_lang->insert_data($N_name, "Publish", 0);
				if($result_lang){
					if($N_id != ''){//UPDATE
						$result_update = $obj_atelang->update_data_id($N_id, $result_lang, $N_user_id, $N_listen, $N_speak, $N_read, $N_write, $N_desc);
						//var_dump($result_update);
						if($result_update){	
							$R_message = array("status" => "200", "message" => "Success insert lang1");
						}else{
							$R_message = array("status" => "404", "message" => "Failed insert lang1");
						}
					}else{//INSERT NEW
						$result_atelang = $obj_atelang->insert_data($result_lang, $N_user_id, $N_listen, $N_speak, $N_read, $N_write, $N_desc);
						//var_dump($result_atelang);
						if($result_atelang == 1){	
							$R_message = array("status" => "200", "message" => "Success insert lang2");
						}else{
							$R_message = array("status" => "404", "message" => "Failed insert lang2");
						}
					}
				}else{
					$R_message = array("status" => "404", "message" => "Failed insert lang3");
				}
			}else{//IF THE LANG EXIST, GO AHEAD AND INSERT INTO AT_Lang
				$check_primary = $obj_atelang->check_primary($check_exist, $N_user_id);//CHECK IF PRIMARY EXIST
				//var_dump($check_primary);
				if($N_id != ''){//UPDATE
					$result_update = $obj_atelang->update_data_id($N_id, $check_exist, $N_user_id, $N_listen, $N_speak, $N_read, $N_write, $N_desc);
					//var_dump($result_update);
					if($result_update){
						$R_message = array("status" => "200", "message" => "Success insert lang4");
					}else{
						$R_message = array("status" => "404", "message" => "Failed insert lang4");
					}
				}else{//INSERT NEW
					if($check_primary == 0){
						$result_insert = $obj_atelang->insert_data($check_exist, $N_user_id, $N_listen, $N_speak, $N_read, $N_write, $N_desc);
						//var_dump($result_insert);
						if($result_insert){
							$R_message = array("status" => "200", "message" => "Success insert lang5");
						}else{
							$R_message = array("status" => "404", "message" => "Failed insert lang5");
						}
					}else{
						$result_update = $obj_atelang->update_data($check_exist, $N_user_id, $N_listen, $N_speak, $N_read, $N_write, $N_desc);
						if($result_update){
							$R_message = array("status" => "200", "message" => "Success insert lang6");
						}else{
							$R_message = array("status" => "404", "message" => "Failed insert lang6");
						}
					}
				}
			}
		}//check code
		else{
			$R_message = array("status" => "401", "message" => "Unauthorized");
		}

		$obj_connect->down();
		header("Content-Type: application/json");
		echo json_encode($R_message);	
	}//END UPDATE LANGUAGE

	else if($_GET['action'] == 'delete' && isset($_REQUEST['user_id']) && isset($_REQUEST['auth_code'])){//START DELETE
		$obj_connect->up();	
		$N_user_id = mysql_real_escape_string($_REQUEST['user_id']);
		$N_authcode = mysql_real_escape_string($_REQUEST['auth_code']);
		$N_id = mysql_real_escape_string($_REQUEST['id']);

		if($obj_user->check_code($N_authcode, $N_user_id)){//check code
			$result = $obj_atelang->delete_data($N_id, $N_user_id);
			if($result == 1){
				$R_message = array("status" => "200", "message" => "Lang has been deleted");
			}else{
				$R_message = array("status" => "404", "message" => "Lang failed to be deleted");	
			}
		}//check code
		else{
			$R_message = array("status" => "401", "message" => "Unauthorized");
		}

		$obj_connect->down();
		echo json_encode($R_message);
	}//END DELETE

	else{
		echo "error";
	}
	
	$obj_connect->down();//CLOSE CONNECTION
}//end gate

?>