<?php 
header('content-type: application/json');
header("access-control-allow-origin: *");

if(isset($_GET['action'])){//start gate
	
	require_once("../model/Connection.php");
	$obj_connect = new Connection();
	
	require_once("../model/AT_Work.php");
	$obj_work = new AT_Work();

	require_once("../model/Company.php");
	$obj_company = new Company();

	require_once("../model/Ref_City.php");
	$obj_rcity = new Ref_City();

	require_once("../model/Ref_Country.php");
	$obj_rcountry = new Ref_Country();

	require_once("../model/User.php");
	$obj_user = new User();
	
	$obj_connect->up();//OPEN CONNECTION
	
	if($_GET['action'] == 'autocomplete' && isset($_GET['term'])){
		$obj_connect->up();	
		$R_message = array("status" => "400", "message" => "No Data");
		$term = mysql_real_escape_string($_GET['term']);
		$result = $obj_company->get_list($term);
		if($result){
			$R_message = array("status" => "200", "message" => "Data Exist", "data" => $result);
		}
		$obj_connect->down();	
		echo json_encode($R_message);
	}

	else if($_GET['action'] == 'get_data' && isset($_REQUEST['user_id']) && isset($_REQUEST['auth_code'])){//START GET DATA
		$obj_connect->up();	
		$R_message = array("status" => "400", "message" => "No Data");

		$N_user_id = mysql_real_escape_string($_REQUEST['user_id']);
		$N_authcode = mysql_real_escape_string($_REQUEST['auth_code']);
		
		if($obj_user->check_code($N_authcode, $N_user_id)){//check code
			$result = $obj_work->get_data($N_user_id);
			//var_dump($result);
			if(is_array($result)){
				$R_message = array("status" => "200", "message" => "Data Exist", "data" => $result);
			}
		}//check code
		else{
			$R_message = array("status" => "401", "message" => "Unauthorized");
		}

		$obj_connect->down();	
		header("Content-Type: application/json");
		echo json_encode($R_message);	
	}//END GET DATA

	else if($_GET['action'] == 'update' && isset($_REQUEST['user_id']) && isset($_REQUEST['auth_code'])){//START insert_education
		$obj_connect->up();	

		$N_user_id = mysql_real_escape_string($_REQUEST['user_id']);
		$N_authcode = mysql_real_escape_string($_REQUEST['auth_code']);
		
		//INSERT
		$N_id = mysql_real_escape_string($_REQUEST['id']);
		// INSERT INTO T_COMPANY
		$N_company = mysql_real_escape_string($_REQUEST['company']);
		$N_city = mysql_real_escape_string($_REQUEST['city']);
		$N_country = mysql_real_escape_string($_REQUEST['country']);
		// INSERT INTO AT_Work
		$N_title = mysql_real_escape_string($_REQUEST['title']);
		$N_fromMonth = mysql_real_escape_string($_REQUEST['from_month']);
		$N_fromYear = mysql_real_escape_string($_REQUEST['from_year']);
		$N_toMonth = mysql_real_escape_string($_REQUEST['to_month']);
		$N_toYear = mysql_real_escape_string($_REQUEST['to_year']);
		$N_current = mysql_real_escape_string($_REQUEST['current']);
		$N_desc = mysql_real_escape_string($_REQUEST['desc']);

		//if($obj_user->check_code($N_authcode, $N_user_id)){//check code
		if($obj_user->check_code($N_authcode, $N_user_id) && $N_company != '' && $N_city != '' && $N_country != ''){//check code
			$check_city = $obj_rcity->check_city($N_city); //CHECK IF CITY EXIST
			//var_dump($check_city);
			if($check_city == 0){ //IF CITY NOT EXIST, INSERT
				$result_city = $obj_rcity->insert_data(0, $N_city, 0, "Publish"); //insert city
				$city_id = $result_city;
				//var_dump($result_city);
			}else{
				$city_id = $check_city;
			}

			$check_country = $obj_rcountry->check_country($N_country); //CHECK IF COUNTRY EXIST
			//var_dump($check_country);
			if($check_country == 0){ //IF COUNTRY NOT EXIST, INSERT
				$result_country = $obj_rcountry->insert_data($N_country, null, null, 0, "Publish"); //insert country
				$country_id = $result_country;
				//var_dump($result_country);
			}else{
				$country_id = $check_country;
			}

			$check_exist = $obj_company->check_company($N_company, $N_city, $N_country);//CHECK IF COMPANY EXIST
			//var_dump($check_exist);
			if($check_exist == 0){//IF COMPANY NOT EXIST, INSERT
				$result_company = $obj_company->insert_data(0, $city_id, $country_id, $N_company, null, null, null, null, null, null, null, 0, "Publish");
				if($result_company){
					if($N_id != ''){//UPDATE
						$result_update = $obj_work->update_data($N_id, $result_company, $N_title, $N_fromMonth, $N_fromYear, $N_toMonth, $N_toYear, $N_current, $N_desc);	
						if($result_update){
							$R_message = array("status" => "200", "message" => "Success insert work1");
						}else{
							$R_message = array("status" => "404", "message" => "Failed insert work1");
						}
					}else{//INSERT NEW
						$result_work = $obj_work->insert_data($result_company, $N_user_id, $N_title, $N_fromMonth, $N_fromYear, $N_toMonth, $N_toYear, $N_current, $N_desc);
						if($result_work){
							$R_message = array("status" => "200", "message" => "Success insert work2");
						}else{
							$R_message = array("status" => "404", "message" => "Failed insert work2");
						}
					}
				}else{
					$R_message = array("status" => "404", "message" => "Failed insert work3");
				}
			}else{//IF THE COMPANY EXIST, GO AHEAD AND INSERT INTO AT_Work
				if($N_id != ''){
					$result_update = $obj_work->update_data($N_id, $check_exist, $N_title, $N_fromMonth, $N_fromYear, $N_toMonth, $N_toYear, $N_current, $N_desc);
					if($result_update){
						$R_message = array("status" => "200", "message" => "Success insert work4");
					}else{
						$R_message = array("status" => "404", "message" => "Failed insert work4");
					}
				}else{
					$result_insert = $obj_work->insert_data($check_exist, $N_user_id, $N_title, $N_fromMonth, $N_fromYear, $N_toMonth, $N_toYear, $N_current, $N_desc);
					if($result_insert){
						$R_message = array("status" => "200", "message" => "Success insert work5");
					}else{
						$R_message = array("status" => "404", "message" => "Failed insert work5");
					}
				}
			}
		}//check code
		else{
			$R_message = array("status" => "401", "message" => "Unauthorized");
		}

		$obj_connect->down();
		header("Content-Type: application/json");
		echo json_encode($R_message);	
	}//END INSERT WORK

	else if($_GET['action'] == 'delete' && isset($_REQUEST['user_id']) && isset($_REQUEST['auth_code'])){//START DELETE
		$obj_connect->up();	
		$N_user_id = mysql_real_escape_string($_REQUEST['user_id']);
		$N_authcode = mysql_real_escape_string($_REQUEST['auth_code']);
		$N_id = mysql_real_escape_string($_REQUEST['id']);

		if($obj_user->check_code($N_authcode, $N_user_id)){//check code
			$result = $obj_work->delete_data($N_id);
			if($result == 1){
				$R_message = array("status" => "200", "message" => "Work has been deleted");
			}else{
				$R_message = array("status" => "404", "message" => "Work failed to be deleted");	
			}
		}//check code
		else{
			$R_message = array("status" => "401", "message" => "Unauthorized");
		}

		$obj_connect->down();
		echo json_encode($R_message);
	}//END DELETE

	else{
		echo "error";
	}
	
	$obj_connect->down();//CLOSE CONNECTION
}//end gate

?>