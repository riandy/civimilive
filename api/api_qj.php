<?php 
header('content-type: application/json');
header("access-control-allow-origin: *");

if(isset($_GET['action'])){//start gate
	
	require_once("../model/Connection.php");
	$obj_connect = new Connection();
	
	require_once("../model/Query_job.php");
	$obj_qjob = new Query_job();
	
	$obj_connect->up();//OPEN CONNECTION
	
	if($_GET['action'] == 'get_recent'){
		$R_message = array("status" => "400", "message" => "No Data");
		
		$result = $obj_qjob->get_recent();
		if(is_array($result)){
			$R_message = array("status" => "200", "message" => "Data Exist", "data" => $result);
		}
		
		echo json_encode($R_message);
	}
	
	$obj_connect->down();//CLOSE CONNECTION
}//end gate

?>