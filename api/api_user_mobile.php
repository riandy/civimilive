<?php 
header('content-type: application/json');
header("access-control-allow-origin: *");
require_once(dirname(__FILE__)."/../packages/check_input.php");
if(isset($_GET['action'])){//START GATE
	require_once("../model/Connection.php");
	$obj_con = new Connection();

	require_once("../model/User_Mobile.php");
	$obj_user_mobile = new User_Mobile();

	require_once("../model/User.php");
	$obj_user = new User();

	require_once("../model/Third_party.php");
	$obj_tp = new Third_party();

	require_once("../model/Device.php");
	$obj_device = new Device();

	require_once("../model/Setting.php");
	$obj_set = new Setting();

	require_once("../model/AT_Progress.php");
	$obj_atpro = new AT_Progress();

	function send_email($tujuan, $username, $code){
	    //mail
	    $to = $tujuan; 
	    $subject = "Activation User";

	    $message = "
	    <!DOCTYPE html>
	    <html>
	    <head>
	    <title></title>
	    </head>
	    <body>
		<span style='font-size:12px;'><em>*This is a your register data</em></span>
		<br/><br/>
		<b>Username</b> : $username
		<br/><br/>
		<b>Code</b> : $code (input this code in your civimi mobile application)

		<br/><br/><br/>
		<em>*Do not reply to this e-mail.<br/><br/>Thank you!</em>
	    </body>
	    </html>
	    ";

	    // Always set content-type when sending HTML email
	    $headers = "MIME-Version: 1.0" . "\r\n";
	    $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

	    // More headers
	    $headers .= "From: <donotreply@civimi.com>" . "\r\n";

	    $result = mail($to,$subject,$message,$headers);
	    return $result;	
    }

	//===================================== register user ========================================
	//start register
	if($_GET['action'] == 'register'){
		$obj_con->up();	
		$R_message = array("status" => "404", "message" => "Register failed");

		$N_username = mysql_real_escape_string($_REQUEST['username']);
		$N_email = mysql_real_escape_string($_REQUEST['email']);
		$N_password = mysql_real_escape_string($_REQUEST['password']);
		$N_interest = mysql_real_escape_string($_REQUEST['interest']);
		$N_via = mysql_real_escape_string($_REQUEST['via']);		
		$N_code = generate_code(6);
		//insert device
		$N_os = mysql_real_escape_string($_REQUEST['device_os']);
		$N_api = mysql_real_escape_string($_REQUEST['device_api']);
		$N_device = mysql_real_escape_string($_REQUEST['device_mydevice']);
		$N_model = mysql_real_escape_string($_REQUEST['device_model']);
		$N_product = mysql_real_escape_string($_REQUEST['device_product']);

		$check = $obj_user_mobile->check_primary_email($N_email); // check e-mail
		if(is_array($check)){
			$R_message = array("status" => "422", "message" => "Email already exists");
		}else{
			$result = $obj_user_mobile->insert_data($N_username, $N_email, $N_password, $N_interest, $N_via, $N_code);
			if($result){
				send_email($N_email, $N_username, $N_code);
				$obj_device->insert_data($result, $N_os, $N_api, $N_device, $N_model, $N_product);
				$R_message = array("status" => "200", "message" => "Register Success");
			}
		}

		$obj_con->down();
		echo json_encode($R_message);	
	}//end register

	//===================================== register user third party ========================================
	//start register user third party
	else if($_GET['action'] == 'register_third_party'){
		$obj_con->up();	
		$R_message = array("status" => "404", "message" => "Register third party failed");

		$N_fname = mysql_real_escape_string($_REQUEST['fname']);
		$N_lname = mysql_real_escape_string($_REQUEST['lname']);
		$N_interest = mysql_real_escape_string($_REQUEST['interest']);
		$N_email = mysql_real_escape_string($_REQUEST['email']);
		$N_provider = mysql_real_escape_string($_REQUEST['provider']);
		$N_via = mysql_real_escape_string($_REQUEST['via']);
		$N_photo = mysql_real_escape_string($_REQUEST['photo']);
		$N_photo_thmb = $N_photo;
				
		//insert device
		$N_os = mysql_real_escape_string($_REQUEST['device_os']);
		$N_api = mysql_real_escape_string($_REQUEST['device_api']);
		$N_device = mysql_real_escape_string($_REQUEST['device_mydevice']);
		$N_model = mysql_real_escape_string($_REQUEST['device_model']);
		$N_product = mysql_real_escape_string($_REQUEST['device_product']);

		$check = $obj_user_mobile->check_primary_email($N_email); // check e-mail
		if(is_array($check)){
			$R_message = array("status" => "422", "message" => "Email already exists");
		}else{
			$result = $obj_user_mobile->insert_data_third_party($N_fname, $N_lname, $N_interest, $N_email, $N_provider, $N_via, $N_photo, $N_photo_thmb);
			if($result){
				$obj_device->insert_data($result, $N_os, $N_api, $N_device, $N_model, $N_product);
				$data_user = $obj_tp->user_login($N_email);
				if(is_array($data_user)){
		        	$R_message = array("status" => "200", "message" => "Register third party success", "data" => $data_user);
				}
			}
		}		

		$obj_con->down();
		echo json_encode($R_message);	
	}//end register third party

	//===================================== activation user ========================================
	//start activation user
	else if($_GET['action'] == 'activation_user'){
		$obj_con->up();	
		$R_message = array("status" => "404", "message" => "Activation user failed");

		$N_username = mysql_real_escape_string($_REQUEST['username']);
		$N_code = mysql_real_escape_string($_REQUEST['code']);

		$result = $obj_user->update_act_status($N_username, $N_code);
		if($result == 1){
			$data_user = $obj_user_mobile->get_data_by_username($N_username);
			$R_message = array("status" => "200", "message" => "Activation user success", "data" => $data_user);
		}

		$obj_con->down();
		echo json_encode($R_message);	
	}//end activation user

	//===================================== login user ========================================
	//start login user
	else if($_GET['action'] == 'login'){
		$obj_con->up();	
		$R_message = array("status" => "404", "message" => "Login failed");

		$N_email = mysql_real_escape_string($_REQUEST['email']);
		$N_password = mysql_real_escape_string($_REQUEST['password']);

		if($N_email != null && $N_password != null){
			$N_salt = $obj_user->get_salt($N_email);	
			$password = substr(doHash($N_password, $N_salt), 0, 64);

			$result = $obj_user_mobile->user_login($N_email, $password);
			if(is_array($result)){
				if($result[0]['User_actStatus'] == 1){ //if email confirmation success
					$obj_user->update_action_login($result[0]['User_ID']); //update for last login and num login
					//if auth_code null, update
					if($result[0]['User_auth_code'] == "" || $result[0]['User_auth_code'] == NULL){
						$obj_user->update_auth_code($result[0]['User_ID']);
						$result = $obj_user_mobile->user_login($N_email, $password); //check login again with a new auth code
					}
					$obj_set->insert_data($result[0]['User_ID'], 1, 2, 3, 4, 5, 6, 7); //insert setting sort
	        		$obj_atpro->insert_data($result[0]['User_ID'], 0, 0, 0, 0, 0, 0, 0, 0); //insert progress
	        	}

	        	$R_message = array("status" => "200", "message" => "Login success", "data" => $result);
			}
		}
		
		$obj_con->down();
		echo json_encode($R_message);	
	}//end login user

	//===================================== login user third party ========================================
	//start login user third party
	else if($_GET['action'] == 'login_third_party'){
		$obj_con->up();	
		$R_message = array("status" => "404", "message" => "Login third party failed");

		$N_email = mysql_real_escape_string($_REQUEST['email']);

		$result = $obj_user_mobile->user_login_third_party($N_email);	
		if(is_array($result)){
			$obj_user->update_action_login($result[0]['User_ID']); //update for last login and num login
			//if auth_code null, update
			if($result[0]['User_auth_code'] == "" || $result[0]['User_auth_code'] == NULL){
				$obj_user->update_auth_code($result[0]['User_ID']);
				$result = $obj_user_mobile->user_login_third_party($N_email); //check login again with a new auth code
			}
			$obj_set->insert_data($result[0]['User_ID'], 1, 2, 3, 4, 5, 6, 7); //insert setting sort
	        $obj_atpro->insert_data($result[0]['User_ID'], 0, 0, 0, 0, 0, 0, 0, 0); //insert progress
	        	
	        $R_message = array("status" => "200", "message" => "Login third party success", "data" => $result);
		}
		
		$obj_con->down();
		echo json_encode($R_message);	
	}//end register third party

	//===================================== upload photo profile ========================================
	//start upload photo profile
	else if($_GET['action'] == 'upload_photo_profile' && isset($_REQUEST['user_id']) && isset($_REQUEST['auth_code'])){
		$obj_con->up();
		$N_user_id = mysql_real_escape_string($_REQUEST['user_id']);
		$N_authcode = mysql_real_escape_string($_REQUEST['auth_code']);
		
		if($obj_user->check_code($N_authcode, $N_user_id)){//check code
			if(!empty($_FILES)){
				require_once("../packages/SimpleImage.php"); // class simple image
				$allowed_ext = array('jpg', 'jpeg', 'png', 'gif');
				$file_name	 = cleanSpace($_FILES['image']['name']);
				$file_ext	 = strtolower(end(explode('.', $file_name)));
				$file_size	 = $_FILES['image']['size'];
				$file_tmp	 = $_FILES['image']['tmp_name'];
				$ran = rand();
        		$timestamp = time();
				
				if(in_array($file_ext, $allowed_ext) === true){
					if($file_size < 10044070){
						//save image in server
						$file_loc = "../image/proPhotos/" . $timestamp . $ran . $file_name; 
						$file_locThmb = "../image/thmb-proPhotos/" . $timestamp . $ran . $file_name;

						//save image in database
						$file_loc1 = "image/proPhotos/" . $timestamp . $ran . $file_name; 
						$file_locThmb1 = "image/thmb-proPhotos/" . $timestamp . $ran . $file_name;

						if(move_uploaded_file($file_tmp, $file_loc))
						{
							$image = new SimpleImage();
							$image->load($file_loc);
							$image->resize(200,200);
							$image->save($file_locThmb);
						}

						$result = $obj_user_mobile->update_personal_image($N_user_id, $N_authcode, $file_loc1, $file_locThmb1);
						if($result == 1){
							$R_message = array("status" => "200", "message" => "Upload photo success");	
						}else{
							$R_message = array("status" => "404", "message" => "Upload photo failed");
						}
					}else{
						$R_message = array("status" => "404", "message" => "ERROR: file size max 10 MB!");
					}
				}else{
					$R_message = array("status" => "404", "message" => "ERROR: extension file invalid!");
				}
			}else{
				$R_message = array("status" => "404", "message" => "Upload file is empty");
			}
		}//check code
		else{
			$R_message = array("status" => "401", "message" => "Unauthorized");
		}

		$obj_con->down();
		echo json_encode($R_message);
	}//end upload photo profile

	//===================================== update step personal ========================================
	//start update step personal
	else if($_GET['action'] == 'update_step_personal' && isset($_REQUEST['user_id']) && isset($_REQUEST['auth_code'])){
		$obj_con->up();	
		$N_user_id = mysql_real_escape_string($_REQUEST['user_id']);
		$N_authcode = mysql_real_escape_string($_REQUEST['auth_code']);

		//field
		$N_username = mysql_real_escape_string($_REQUEST['username']); 
		$N_fname = mysql_real_escape_string($_REQUEST['fname']);
		$N_lname = mysql_real_escape_string($_REQUEST['lname']);
		$N_day = mysql_real_escape_string($_REQUEST['day']);
		$N_month = mysql_real_escape_string($_REQUEST['month']);
		$N_year = mysql_real_escape_string($_REQUEST['year']);
		$N_bio = mysql_real_escape_string($_REQUEST['bio']);
		$N_city = mysql_real_escape_string($_REQUEST['city']);
		$N_state = mysql_real_escape_string($_REQUEST['state']);
		$N_country = mysql_real_escape_string($_REQUEST['country']);
		$N_phone = mysql_real_escape_string($_REQUEST['phone']);
		$N_website = mysql_real_escape_string($_REQUEST['website']);
		
		if($obj_user->check_code($N_authcode, $N_user_id)){//check code
			if($N_username != ""){
				$result = $obj_user_mobile->update_step_personal($N_user_id, $N_username, $N_fname, $N_lname, $N_day[0], $N_month, $N_year, $N_bio, $N_city, $N_state, $N_country, $N_phone, $N_website);
				//var_dump($result);
				if($result == 1){
					//save progress
					$users = $obj_user->get_data_detail($N_user_id);
					if(is_array($users)){
						$N_array = array(
				            array('module' => 'image', 'field' => $users[0]['User_proPhotoThmb']),
				            array('module' => 'profession', 'field' => $users[0]['User_interest']),
				            array('module' => 'city', 'field' => $users[0]['User_city']),
				            array('module' => 'state', 'field' => $users[0]['User_state']),
				            array('module' => 'country', 'field' => $users[0]['User_country']),
				            array('module' => 'phone', 'field' => $users[0]['User_phone']),
				            array('module' => 'website', 'field' => $users[0]['User_website'])
				        );

				        $progress_personal = check_progress("personal", $N_array);
				        $obj_atpro->save_progress($N_user_id, "personal", $progress_personal);
					}//end save progress

					$R_message = array("status" => "200", "message" => "Update step personal success");
				}
				else{
					$R_message = array("status" => "404", "message" => "Update data personal failed");
				}
			}
			else{
				$R_message = array("status" => "404", "message" => "Username empty");
			}
		}//check code
		else{
			$R_message = array("status" => "401", "message" => "Unauthorized");
		}

		$obj_con->down();
		echo json_encode($R_message);	
	}//end update step personal


	//===================================== get data profile ========================================
	//start get data profile
	else if($_GET['action'] == 'get_data_profile' && isset($_REQUEST['user_id']) && isset($_REQUEST['auth_code'])){
		$obj_con->up();	
		$R_message = array("status" => "404", "message" => "Data Not Found");
		$N_user_id = mysql_real_escape_string($_REQUEST['user_id']);
		$N_authcode = mysql_real_escape_string($_REQUEST['auth_code']);

		if($obj_user->check_code($N_authcode, $N_user_id)){//check code
			$result = $obj_user_mobile->get_data_profile($N_user_id);
			if(is_array($result)){
				$R_message = array("status" => "200", "message" => "Data Exist", "data" => $result);
			}
		}//check code
		else{
			$R_message = array("status" => "401", "message" => "Unauthorized");
		}

		$obj_con->down();
		echo json_encode($R_message);	
	}//end data profile

	//===================================== update profile ========================================
	//start update profile
	else if($_GET['action'] == 'update_profile' && isset($_REQUEST['user_id']) && isset($_REQUEST['auth_code'])){
		$obj_con->up();	
		$N_user_id = mysql_real_escape_string($_REQUEST['user_id']);
		$N_authcode = mysql_real_escape_string($_REQUEST['auth_code']);

		//field
		$N_fname = mysql_real_escape_string($_REQUEST['fname']);
		$N_lname = mysql_real_escape_string($_REQUEST['lname']);
		$N_day = mysql_real_escape_string($_REQUEST['day']);
		$N_month = mysql_real_escape_string($_REQUEST['month']);
		$N_year = mysql_real_escape_string($_REQUEST['year']);
		$N_dob_show = mysql_real_escape_string($_REQUEST['dob_show']);
		$N_interest = mysql_real_escape_string($_REQUEST['interest']);
		$N_bio = mysql_real_escape_string($_REQUEST['bio']);
		$N_city = mysql_real_escape_string($_REQUEST['city']);
		$N_state = mysql_real_escape_string($_REQUEST['state']);
		$N_country = mysql_real_escape_string($_REQUEST['country']);
		$N_phone = mysql_real_escape_string($_REQUEST['phone']);
		$N_phone_show = mysql_real_escape_string($_REQUEST['phone_show']);
		$N_website = mysql_real_escape_string($_REQUEST['website']);
		$N_obj = mysql_real_escape_string($_REQUEST['obj']);

		if($obj_user->check_code($N_authcode, $N_user_id)){//check code
			if($N_user_id != ""){
				$result = $obj_user_mobile->update_profile($N_user_id, $N_fname, $N_lname, $N_day, $N_month, $N_year, $N_dob_show, $N_interest, $N_bio, $N_city, $N_state, $N_country, $N_phone, $N_phone_show, $N_website, $N_obj);
				//var_dump($result);
				if($result){
					//save progress
					$users = $obj_user->get_data_detail($N_user_id);
					if(is_array($users)){
						$N_array = array(
				            array('module' => 'image', 'field' => $users[0]['User_proPhotoThmb']),
				            array('module' => 'profession', 'field' => $users[0]['User_interest']),
				            array('module' => 'city', 'field' => $users[0]['User_city']),
				            array('module' => 'state', 'field' => $users[0]['User_state']),
				            array('module' => 'country', 'field' => $users[0]['User_country']),
				            array('module' => 'phone', 'field' => $users[0]['User_phone']),
				            array('module' => 'website', 'field' => $users[0]['User_website'])
				        );

				        $progress_personal = check_progress("personal", $N_array);
				        $obj_atpro->save_progress($N_user_id, "personal", $progress_personal);
					}//end save progress

					$R_message = array("status" => "200", "message" => "Update data profile success");
				}
				else{
					$R_message = array("status" => "404", "message" => "Update data profile failed");
				}
			}
			else{
				$R_message = array("status" => "404", "message" => "Id empty");
			}
		}//check code
		else{
			$R_message = array("status" => "401", "message" => "Unauthorized");
		}
		$obj_con->down();

		header("Access-Control-Allow-Origin: *"); 
		header("Content-Type: application/json");
		echo json_encode($R_message);	
	}//end update personal

	else{
		echo "error";
	}

}//END GATE
?>