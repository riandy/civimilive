<?php 
header('content-type: application/json');
header("access-control-allow-origin: *");
if(isset($_GET['action'])){//START GATE
	require_once("../model/Connection.php");
	$obj_con = new Connection();

	require_once("../model/User.php");
	$obj_user = new User();

	//===================================== get data objective ========================================
	//start get data objective
	if($_GET['action'] == 'get_data_objective'){
		$obj_con->up();	
		$R_message = array("status" => "400", "message" => "No Data");

		$result = $obj_user->get_featured_user();
		if(is_array($result)){
			$R_message = array("status" => "200", "message" => "Data Exist", "data" => $result);
		}

		$obj_con->down();
		echo json_encode($R_message);	
	}//end get data objective

	else{
		echo "error";
	}

}//END GATE
?>