<?php 
header('content-type: application/json');
header("access-control-allow-origin: *");

if(isset($_GET['action'])){//start gate
	
	require_once("../model/Connection.php");
	$obj_connect = new Connection();
	
	require_once("../model/Hits.php");
	$obj_hit = new Hits();
	
	$obj_connect->up();//OPEN CONNECTION
	
	if($_GET['action'] == 'insert' && isset($_POST['job_id'])){

		$ip = mysql_real_escape_string($_POST['ip']);
		$ipCity = mysql_real_escape_string($_POST['ip_city']);
		$ipCountry = mysql_real_escape_string($_POST['ip_country']);
		$jobsID = mysql_real_escape_string($_POST['job_id']);
		$companyID = mysql_real_escape_string($_POST['company_id']);
		$industryID = mysql_real_escape_string($_POST['industry_id']);
		$fieldID = mysql_real_escape_string($_POST['field_id']);
		$levelID = mysql_real_escape_string($_POST['level_id']);
		$userID = mysql_real_escape_string($_POST['user_id']);//get USER ID
		$type = "quick";

		$R_message = array("status" => "400", "message" => "View Store Failed");
		
		$result = $obj_hit->insert($ip, $ipCity, $ipCountry, $jobsID, $companyID, $industryID, $fieldID, $levelID, $userID, $type);
		if($result){
			$R_message = array("status" => "200", "message" => "View $jobsID Stored");
		}
		
		echo json_encode($R_message);
	}
	
	$obj_connect->down();//CLOSE CONNECTION
}//end gate

?>