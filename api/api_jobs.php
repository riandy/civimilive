<?php 

if(isset($_GET['action'])){//START GATE
	require_once("../model/Connection.php");
	$obj_con = new Connection();

	require_once("../model/Jobs.php");
	$obj_job = new Jobs();

	require_once("../model/User.php");
	$obj_user = new User();		

	require_once("../model/Tagging.php");
	$obj_tagging = new Tagging();

	//===================================== get data ========================================
	//start get data
	if($_GET['action'] == 'get_data' && isset($_REQUEST['keyword']) || isset($_REQUEST['level_id']) || isset($_REQUEST['field_id']) || isset($_REQUEST['city_id']) || isset($_REQUEST['chk_it']) || isset($_REQUEST['chk_ft']) || isset($_REQUEST['chk_pt']) || isset($_REQUEST['chk_fl'])){
		$obj_con->up();	
		$R_message = array("status" => "404", "message" => "Not Found");
		$N_keyword = mysql_real_escape_string($_REQUEST['keyword']);
		$N_levelID = mysql_real_escape_string($_REQUEST['level_id']);
		$N_fieldID = mysql_real_escape_string($_REQUEST['field_id']);
		$N_cityID = mysql_real_escape_string($_REQUEST['city_id']);
		$N_intern = mysql_real_escape_string($_REQUEST['chk_it']);
		$N_fulltime = mysql_real_escape_string($_REQUEST['chk_ft']);
		$N_parttime = mysql_real_escape_string($_REQUEST['chk_pt']);
		$N_freelance = mysql_real_escape_string($_REQUEST['chk_fl']);
		$result = $obj_job->get_data(0, $N_keyword, $N_levelID, $N_fieldID, $N_cityID, $N_intern, $N_fulltime, $N_parttime, $N_freelance);
		if($result){
			$R_message = array("status" => "200", "message" => "OK");
			$R_message['data'] = $result;
		}
		$obj_con->down();

		header("Content-Type: application/json");
		echo json_encode($R_message);	
	}//end get data

	//===================================== get data detail ========================================
	//start get data detail
	else if($_GET['action'] == 'get_data_detail' && isset($_REQUEST['jobs_id'])){
		$obj_con->up();	
		$R_message = array("status" => "404", "message" => "Not Found");
		$N_jobsID = mysql_real_escape_string($_REQUEST['jobs_id']);
		$result = $obj_job->get_data_detail($N_jobsID);
		if($result){
			$R_message = array("status" => "200", "message" => "OK");
			$R_message['data'] = $result;
		}
		$obj_con->down();

		header("Content-Type: application/json");
		echo json_encode($R_message);	
	}//end get data detail

	//===================================== get related by company ======================================== Billy 26 may, 2015
	//start get related by company
	else if($_GET['action'] == 'get_related_by_company' && isset($_REQUEST['jobs_id']) && isset($_REQUEST['company_id'])){
		$obj_con->up();	
		$R_message = array("status" => "404", "message" => "Not Found");
		$N_jobsID = mysql_real_escape_string($_REQUEST['jobs_id']);
		$N_companyID = mysql_real_escape_string($_REQUEST['company_id']);
		
		$result = $obj_job->get_related_by_company($N_jobsID, $N_companyID);
		if($result){
			$R_message = array("status" => "200", "message" => "OK");
			$R_message['data'] = $result;
		}
		$obj_con->down();

		header("Content-Type: application/json");
		echo json_encode($R_message);	
	}//end get related by company

	//===================================== get related by tag ========================================
	//start get related by tag
	else if($_GET['action'] == 'get_related_by_tag' && isset($_REQUEST['jobs_id'])){
		$obj_con->up();	
		$R_message = array("status" => "404", "message" => "Not Found");
		$N_table = "jobs";
		$N_jobsID = mysql_real_escape_string($_REQUEST['jobs_id']);
		$data_tags = $obj_tagging->get_tags($N_table, $N_jobsID); 

		$tag = "";
		if(is_array($data_tags)){
			if(count($data_tags) > 0){
				for($i = 0; $i < count($data_tags); $i++){
					if($i >= 1){
						$tag .= ",";
					}
					$tag .= $data_tags[$i]['Tag_ID'];
				}
			}
			$result = $obj_job->get_related_by_tag($N_jobsID, $tag);
			//var_dump($result);
		}else{
			$data_related_tags = "";
		}
		
		if($result){
			$R_message = array("status" => "200", "message" => "OK");
			$R_message['data'] = $result;
		}
		$obj_con->down();

		header("Content-Type: application/json");
		echo json_encode($R_message);	
	}//end get related by tag

	//===================================== get data result ========================================
	//start get data result search dari search3.php
	else if($_GET['action'] == 'get_result' && (isset($_REQUEST['q']) || isset($_REQUEST['company']) || isset($_REQUEST['location']) || isset($_REQUEST['expertise']) || isset($_REQUEST['function']) || isset($_REQUEST['industry']) || isset($_REQUEST['experience']) || isset($_REQUEST['sort']))){
		$obj_con->up();	
		$R_message = array("status" => "404", "message" => "Not Found");

		//$O_page = $_REQUEST['page'];
		$O_page = 1;
		if(isset($_REQUEST['page'])){
			$O_page = mysql_real_escape_string($_REQUEST['page']);
		}

		$O_keyword = "";
		if(isset($_REQUEST['q'])){
			$O_keyword = mysql_real_escape_string($_REQUEST['q']);
		}

		$O_company = "";
		if(isset($_REQUEST['company'])){
			$O_company = mysql_real_escape_string($_REQUEST['company']);
		}
			
		$O_location = "";
		if(isset($_REQUEST['location'])){
			$O_location = mysql_real_escape_string($_REQUEST['location']);
		}
			
		$O_expertise = "";
		if(isset($_REQUEST['expertise'])){
			$O_expertise = mysql_real_escape_string($_REQUEST['expertise']);
		}
			
		$O_function = "";
		if(isset($_REQUEST['function'])){
			$O_function = mysql_real_escape_string($_REQUEST['function']);
		}
			
		$O_industry = "";
		if(isset($_REQUEST['industry'])){
			$O_industry = mysql_real_escape_string($_REQUEST['industry']);
		}
			
		$O_exp = "";
		if(isset($_REQUEST['experience'])){
			$O_exp = mysql_real_escape_string($_REQUEST['experience']);
		}

		$sort = 'weight';
	    if(isset($_REQUEST['sort'])){
	        $sort = mysql_real_escape_string($_REQUEST['sort']);
	    }

	    $user_id = '';
	    if(isset($_REQUEST['user_id'])){
	        $user_id = mysql_real_escape_string($_REQUEST['user_id']);
	    }

	    if($O_keyword != '' || $O_company != '' || $O_location != '' || $O_expertise != '' || $O_function != '' || $O_industry != '' || $O_exp != ''){
			//$result = $obj_job->get_result($O_page, $O_keyword, $O_company, $O_location, $O_expertise, $O_function, $O_industry, $O_exp, $sort, $user_id);
			$result = $obj_job->get_result($O_page, $O_keyword, $O_company, $O_location, $O_expertise, $O_function, $O_industry, $O_exp, $sort);
			//var_dump($result);
			$data_remaining = $obj_job->get_remaining($O_keyword, $O_company, $O_location, $O_expertise, $O_function, $O_industry, $O_exp);
			if($data_remaining <= count($result)){
				$remaining = 0;
			}else{
				$remaining = $data_remaining - ($O_page * 5);
				if($remaining <= 0){
					$remaining = 0;
				}
			}
		}
		
		if($result){
			$R_message = array("status" => "200", "message" => "OK", "remaining" => "$remaining");
			$R_message['data'] = $result;
		}
		$obj_con->down();

		header("Content-Type: application/json");
		echo json_encode($R_message);	
	}//end get data result

	//===================================== get recent viewed ========================================
	//start get recent viewed
	else if($_GET['action'] == 'get_recent_viewed' && isset($_REQUEST['user_id']) && isset($_REQUEST['auth_code'])){
		$obj_con->up();	
		$R_message = array("status" => "404", "message" => "No Data");
		
		$N_ip = $_SERVER['REMOTE_ADDR']; //ip address
		$N_user_id = mysql_real_escape_string($_REQUEST['user_id']);
		$N_authcode = mysql_real_escape_string($_REQUEST['auth_code']);
		
		if($N_user_id != ''){ //if login, check user
			if($obj_user->check_code($N_authcode, $N_user_id)){//check code
				$result = $obj_job->get_recent_viewed($N_user_id, null);
				//var_dump($result);
				if(is_array($result)){
					$R_message = array("status" => "200", "message" => "Data Exist", "data" => $result);
				}
			}//check code
			else{
				$R_message = array("status" => "401", "message" => "Unauthorized");
			}
		}
		else{ //not login, check ip
			$result = $obj_job->get_recent_viewed(null, $N_ip);
			//var_dump($result);
			if(is_array($result)){
				$R_message = array("status" => "200", "message" => "Data Exist", "data" => $result);
			}
		}

		$obj_con->down();
		header("Content-Type: application/json");
		echo json_encode($R_message);	
	}//end get data detail

	else{
		echo "error";
	}

}//END GATE
?>