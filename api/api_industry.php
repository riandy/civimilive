<?php 
header('content-type: application/json');
header("access-control-allow-origin: *");

if(isset($_GET['action'])){//start gate
	
	require_once("../model/Connection.php");
	$obj_connect = new Connection();
	
	require_once("../model/Industry.php");
	$obj_industry = new Industry();
	
	$obj_connect->up();//OPEN CONNECTION
	
	if($_GET['action'] == 'autocomplete' && isset($_GET['term'])){
		$term = mysql_real_escape_string($_GET['term']);
		$result = $obj_industry->get_list($term);
		
		header("Content-Type: application/json");
		echo json_encode($result);
	}
	else if($_GET['action'] == 'index'){
		$R_message = array("status" => "400", "message" => "No Data");
		
		$result = $obj_industry->get_index();
		if(is_array($result)){
			$R_message = array("status" => "200", "message" => "Data Exist", "data" => $result);
		}
		
		echo json_encode($R_message);
	}
	
	$obj_connect->down();//CLOSE CONNECTION
}//end gate

?>