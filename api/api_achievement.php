<?php 
header('content-type: application/json');
header("access-control-allow-origin: *");

if(isset($_GET['action'])){//start gate
	
	require_once("../model/Connection.php");
	$obj_connect = new Connection();
	
	require_once("../model/Edu.php");
	$obj_edu = new Edu();

	require_once("../model/Company.php");
	$obj_company = new Company();

	require_once("../model/Organization.php");
	$obj_org = new Organization();

	require_once("../model/AT_Achievement.php");
	$obj_achieve = new AT_Achievement();

	require_once("../model/User.php");
	$obj_user = new User();
	
	$obj_connect->up();//OPEN CONNECTION
	
	if($_GET['action'] == 'autocomplete' && isset($_GET['source']) && isset($_GET['term'])){
		$obj_connect->up();	
		$R_message = array("status" => "400", "message" => "No Data");

		$source = mysql_real_escape_string($_GET['source']);
		$term = mysql_real_escape_string($_GET['term']);

		$result = $obj_achieve->get_list($source, $term);
		//var_dump($result);

		if($result){
			$R_message = array("status" => "200", "message" => "Data Exist", "data" => $result);
		}
		$obj_connect->down();	
		echo json_encode($R_message);
	}

	else if($_GET['action'] == 'get_data' && isset($_REQUEST['user_id']) && isset($_REQUEST['auth_code'])){//START GET DATA
		$obj_connect->up();	
		$R_message = array("status" => "400", "message" => "No Data");

		$N_user_id = mysql_real_escape_string($_REQUEST['user_id']);
		$N_authcode = mysql_real_escape_string($_REQUEST['auth_code']);
		
		if($obj_user->check_code($N_authcode, $N_user_id)){//check code
			$result = $obj_achieve->get_data($N_user_id);
			//var_dump($result);
			if(is_array($result)){
				$R_message = array("status" => "200", "message" => "Data Exist", "data" => $result);
			}
		}//check code
		else{
			$R_message = array("status" => "401", "message" => "Unauthorized");
		}

		$obj_connect->down();	
		header("Content-Type: application/json");
		echo json_encode($R_message);	
	}//END GET DATA

	else if($_GET['action'] == 'update' && isset($_REQUEST['user_id']) && isset($_REQUEST['auth_code'])){//START insert_achievement
		$obj_connect->up();	

		$N_user_id = mysql_real_escape_string($_REQUEST['user_id']);
		$N_authcode = mysql_real_escape_string($_REQUEST['auth_code']);
		
		//INSERT INTO AT_Achievement
		$N_id = mysql_real_escape_string($_REQUEST['id']);
		$N_title = mysql_real_escape_string($_REQUEST['title']);
		$N_source = mysql_real_escape_string($_REQUEST['source']);
		$N_institute = mysql_real_escape_string($_REQUEST['institute']);
		$N_year = mysql_real_escape_string($_REQUEST['year']);
		$N_desc = mysql_real_escape_string($_REQUEST['desc']);
		$N_publish = "Publish";
		
		//action
		if($obj_user->check_code($N_authcode, $N_user_id) && $N_source != "" && $N_institute != ""){//check code
			$check_exist = $obj_achieve->check_source($N_source, $N_institute);//CHECK IF SOURCE EXIST
			if($check_exist == 0){//IF SOURCE NOT EXIST, INSERT
				$result_source = $obj_achieve->insert_source($N_source, $N_institute);
				if($result_source){
					if($N_source != "other"){
						$N_source_id = $result_source;
						$N_institute = "";	
					}else{
						$N_source_id = 0;;
					}
					
					if($N_id != ""){//UPDATE
						$result = $obj_achieve->update_data($N_id, $N_title, $N_source, $N_source_id, $N_institute, $N_year, $N_desc, $N_publish);
						if($result){	
							$R_message = array("status" => "200", "message" => "Success insert achievement1");
						}else{
							$R_message = array("status" => "404", "message" => "Failed insert achievement1");
						}
					}else{//INSERT NEW
						$result = $obj_achieve->insert_data($N_user_id, $N_title, $N_source, $result_source, $N_institute, $N_year, $N_desc, $N_publish);
						if($result){	
							$R_message = array("status" => "200", "message" => "Success insert achievement2");
						}else{
							$R_message = array("status" => "404", "message" => "Failed insert achievement2");
						}
					}
				}else{
					$R_message = array("status" => "404", "message" => "Failed insert achievement3");
				}
			}else{//IF THE CERTIFICATE EXIST, GO AHEAD AND INSERT INTO AT_Certificate
				if($N_source != "other"){
					$N_source_id = $check_exist;
					$N_institute = "";	
				}else{
					$N_source_id = 0;
				}

				if($N_id != ""){//UPDATE
					$result = $obj_achieve->update_data($N_id, $N_title, $N_source, $N_source_id, $N_institute, $N_year, $N_desc, $N_publish);
					if($result){	
						$R_message = array("status" => "200", "message" => "Success insert achievement4");
					}else{
						$R_message = array("status" => "404", "message" => "Failed insert achievement4");
					}
				}else{//INSERT NEW
					$result = $obj_achieve->insert_data($N_user_id, $N_title, $N_source, $N_source_id, $N_institute, $N_year, $N_desc, $N_publish);
					if($result){	
						$R_message = array("status" => "200", "message" => "Success insert achievement5");
					}else{
						$R_message = array("status" => "404", "message" => "Failed insert achievement5");
					}
				}
			}
		}//check code
		else{
			$R_message = array("status" => "401", "message" => "Unauthorized");
		}

		$obj_connect->down();
		header("Content-Type: application/json");
		echo json_encode($R_message);	
	}//END INSERT ACHIEVEMENT

	else if($_GET['action'] == 'delete' && isset($_REQUEST['user_id']) && isset($_REQUEST['auth_code'])){//START DELETE
		$obj_connect->up();	
		$N_user_id = mysql_real_escape_string($_REQUEST['user_id']);
		$N_authcode = mysql_real_escape_string($_REQUEST['auth_code']);
		$N_id = mysql_real_escape_string($_REQUEST['id']);

		if($obj_user->check_code($N_authcode, $N_user_id)){//check code
			$result = $obj_achieve->delete_data($N_id);
			if($result == 1){
				$R_message = array("status" => "200", "message" => "Achievement has been deleted");
			}else{
				$R_message = array("status" => "404", "message" => "Achievement failed to be delete");	
			}
		}//check code
		else{
			$R_message = array("status" => "401", "message" => "Unauthorized");
		}

		$obj_connect->down();
		echo json_encode($R_message);
	}//END DELETE

	else{
		echo "error";
	}
	
	$obj_connect->down();//CLOSE CONNECTION
}//end gate

?>