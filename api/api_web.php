<?php 
header('content-type: application/json');
header("access-control-allow-origin: *");

if(isset($_GET['action'])){//start gate
	
	require_once("../model/Connection.php");
	$obj_connect = new Connection();
	
	require_once("../model/WebCat.php");
	$obj_wc = new WebCat();

	require_once("../model/Web.php");
	$obj_web = new Web();

	require_once("../model/User.php");
	$obj_user = new User();
	
	$obj_connect->up();//OPEN CONNECTION
	
// START ACTION FOR Web Category
	if($_GET['action'] == 'get_web_cat' && isset($_REQUEST['user_id']) && isset($_REQUEST['auth_code'])){//START GET DATA
		$obj_connect->up();	
		$R_message = array("status" => "400", "message" => "No Data");

		$N_user_id = mysql_real_escape_string($_REQUEST['user_id']);
		$N_authcode = mysql_real_escape_string($_REQUEST['auth_code']);
		
		if($obj_user->check_code($N_authcode, $N_user_id)){//check code
			$result = $obj_wc->get_web_cat($N_user_id);
			//var_dump($result);
			if(is_array($result)){
				$R_message = array("status" => "200", "message" => "Data Exist", "data" => $result);
			}
		}//check code
		else{
			$R_message = array("status" => "401", "message" => "Unauthorized");
		}

		$obj_connect->down();	
		echo json_encode($R_message);	
	}//END GET DATA

	else if($_GET['action'] == 'insert_web_cat' && isset($_REQUEST['user_id']) && isset($_REQUEST['auth_code'])){//START insert_web_cat
		$obj_connect->up();	

		$N_user_id = mysql_real_escape_string($_REQUEST['user_id']);
		$N_authcode = mysql_real_escape_string($_REQUEST['auth_code']);
		//INSERT
		$N_name = mysql_real_escape_string($_REQUEST['name']);
		$N_desc = mysql_real_escape_string($_REQUEST['desc']);

		if($obj_user->check_code($N_authcode, $N_user_id) && $N_name != ''){//check code
			$result = $obj_wc->insert_data($N_user_id, $N_name, $N_desc, "Publish");
			if($result){	
				$R_message = array("status" => "200", "message" => "Success insert web category");
			}else{
				$R_message = array("status" => "404", "message" => "Failed insert web category");
			}
		}//check code
		else{
			$R_message = array("status" => "401", "message" => "Unauthorized");
		}

		$obj_connect->down();
		echo json_encode($R_message);	
	}//END INSERT WEB CAT

	else if($_GET['action'] == 'update_web_cat' && isset($_REQUEST['user_id']) && isset($_REQUEST['auth_code'])){//START update_web_cat
		$obj_connect->up();	

		$N_user_id = mysql_real_escape_string($_REQUEST['user_id']);
		$N_authcode = mysql_real_escape_string($_REQUEST['auth_code']);
		//UPDATE
		$N_id = mysql_real_escape_string($_REQUEST['id']);
		$N_name = mysql_real_escape_string($_REQUEST['name']);
		$N_desc = mysql_real_escape_string($_REQUEST['desc']);

		if($obj_user->check_code($N_authcode, $N_user_id) && $N_name != ''){//check code
			$result = $obj_wc->update_data($N_id, $N_name, $N_desc);
			if($result){	
				$R_message = array("status" => "200", "message" => "Success update web category");
			}else{
				$R_message = array("status" => "404", "message" => "Failed update web category");
			}
		}//check code
		else{
			$R_message = array("status" => "401", "message" => "Unauthorized");
		}

		$obj_connect->down();
		echo json_encode($R_message);	
	}//END UPDATE WEB CAT

	else if($_GET['action'] == 'delete_web_cat' && isset($_REQUEST['user_id']) && isset($_REQUEST['auth_code'])){//START DELETE WEB CAT
		$obj_connect->up();	
		$N_user_id = mysql_real_escape_string($_REQUEST['user_id']);
		$N_authcode = mysql_real_escape_string($_REQUEST['auth_code']);
		$N_id = mysql_real_escape_string($_REQUEST['id']);

		if($obj_user->check_code($N_authcode, $N_user_id)){//check code
			$result = $obj_wc->delete_data($N_id);
			//var_dump($result);
			if($result == 1){
				$R_message = array("status" => "200", "message" => "Web category has been deleted");
			}else{
				$R_message = array("status" => "404", "message" => "Web category failed to be deleted");	
			}
		}//check code
		else{
			$R_message = array("status" => "401", "message" => "Unauthorized");
		}

		$obj_connect->down();
		echo json_encode($R_message);
	}//END DELETE WEB CAT
// END ACTION FOR WEB CAT

// START ACTION FOR VIDEO
	else if($_GET['action'] == 'get_web' && isset($_REQUEST['user_id']) && isset($_REQUEST['auth_code'])){//START GET DATA BY WEB CAT
		$obj_connect->up();	
		$R_message = array("status" => "400", "message" => "No Data");

		$N_user_id = mysql_real_escape_string($_REQUEST['user_id']);
		$N_authcode = mysql_real_escape_string($_REQUEST['auth_code']);
		$N_webcat_id = mysql_real_escape_string($_REQUEST['webcat_id']);
		
		if($obj_user->check_code($N_authcode, $N_user_id)){//check code
			$result = $obj_web->get_data_by_web_cat($N_webcat_id);
			//var_dump($result);
			if(is_array($result)){
				$R_message = array("status" => "200", "message" => "Data Exist", "data" => $result);
			}
		}//check code
		else{
			$R_message = array("status" => "401", "message" => "Unauthorized");
		}

		$obj_connect->down();	
		echo json_encode($R_message);	
	}//END GET DATA BY WEB CAT

	else if($_GET['action'] == 'insert_web' && isset($_REQUEST['user_id']) && isset($_REQUEST['auth_code'])){//START insert_web
		$obj_connect->up();	

		$N_user_id = mysql_real_escape_string($_REQUEST['user_id']);
		$N_authcode = mysql_real_escape_string($_REQUEST['auth_code']);
		//INSERT
		$N_webcat_id = mysql_real_escape_string($_REQUEST['webcat_id']);
		$N_link = mysql_real_escape_string($_REQUEST['link']);
		$N_title = mysql_real_escape_string($_REQUEST['title']);
		$N_desc = mysql_real_escape_string($_REQUEST['desc']);

		if($obj_user->check_code($N_authcode, $N_user_id) && $N_link != '' && $N_title != ''){//check code
			$result = $obj_web->insert_data($N_link, $N_title, $N_desc, $N_webcat_id, $N_user_id, "Publish");
			if($result){	
				$R_message = array("status" => "200", "message" => "Success insert web");
			}else{
				$R_message = array("status" => "404", "message" => "Failed insert web");
			}
		}//check code
		else{
			$R_message = array("status" => "401", "message" => "Unauthorized");
		}

		$obj_connect->down();
		echo json_encode($R_message);	
	}//END INSERT WEB

	else if($_GET['action'] == 'update_web' && isset($_REQUEST['user_id']) && isset($_REQUEST['auth_code'])){//START update_web
		$obj_connect->up();	

		$N_user_id = mysql_real_escape_string($_REQUEST['user_id']);
		$N_authcode = mysql_real_escape_string($_REQUEST['auth_code']);
		//INSERT
		$N_id = mysql_real_escape_string($_REQUEST['id']);
		$N_webcat_id = mysql_real_escape_string($_REQUEST['webcat_id']);
		$N_link = mysql_real_escape_string($_REQUEST['link']);
		$N_title = mysql_real_escape_string($_REQUEST['title']);
		$N_desc = mysql_real_escape_string($_REQUEST['desc']);

		if($obj_user->check_code($N_authcode, $N_user_id) && $N_id != '' && $N_link != '' && $N_title != ''){//check code
			$result = $obj_web->update_data($N_id, $N_link, $N_title, $N_desc, $N_webcat_id);
			if($result){	
				$R_message = array("status" => "200", "message" => "Success update web");
			}else{
				$R_message = array("status" => "404", "message" => "Failed update web");
			}
		}//check code
		else{
			$R_message = array("status" => "401", "message" => "Unauthorized");
		}

		$obj_connect->down();
		echo json_encode($R_message);	
	}//END UPDATE WEB

	else if($_GET['action'] == 'delete_web' && isset($_REQUEST['user_id']) && isset($_REQUEST['auth_code'])){//START DELETE WEB
		$obj_connect->up();	
		$N_user_id = mysql_real_escape_string($_REQUEST['user_id']);
		$N_authcode = mysql_real_escape_string($_REQUEST['auth_code']);
		$N_id = mysql_real_escape_string($_REQUEST['id']);

		if($obj_user->check_code($N_authcode, $N_user_id)){//check code
			$result = $obj_web->delete_data($N_id);
			if($result == 1){
				$R_message = array("status" => "200", "message" => "Web has been deleted");
			}else{
				$R_message = array("status" => "404", "message" => "Web failed to be deleted");	
			}
		}//check code
		else{
			$R_message = array("status" => "401", "message" => "Unauthorized");
		}

		$obj_connect->down();
		echo json_encode($R_message);
	}//END DELETE WEB
// END ACTION FOR WEB

	else{
		echo "error";
	}
	
	$obj_connect->down();//CLOSE CONNECTION
}//end gate

?>