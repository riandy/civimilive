<?php 
header('content-type: application/json');
header("access-control-allow-origin: *");

if(isset($_GET['action'])){//start gate
	
	require_once("../model/Connection.php");
	$obj_connect = new Connection();
	
	require_once("../model/Ref_Field.php");
	$obj_rfield = new Ref_Field();
	
	$obj_connect->up();//OPEN CONNECTION
	
	if($_GET['action'] == 'get_popular'){
		$R_message = array("status" => "400", "message" => "No Data");
		$O_page = $_GET['page'];
		$result = $obj_rfield->get_popular($O_page);
		if(is_array($result)){
			$R_message = array("status" => "200", "message" => "Data Exist", "data" => $result);
		}
		
	echo json_encode($R_message);	
	}

	else if($_GET['action'] == 'autocomplete' && isset($_GET['term'])){
		$term = mysql_real_escape_string($_GET['term']);
		$result = $obj_rfield->get_list($term);
		
		header("Content-Type: application/json");
		echo json_encode($result);
	}

	else if($_GET['action'] == 'index'){
		$R_message = array("status" => "400", "message" => "No Data");
		
		$result = $obj_rfield->get_index();
		if(is_array($result)){
			$R_message = array("status" => "200", "message" => "Data Exist", "data" => $result);
		}
		
		echo json_encode($R_message);
	}
	
	$obj_connect->down();//CLOSE CONNECTION
}//end gate

?>