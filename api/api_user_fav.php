<?php 
header('content-type: application/json');
header("access-control-allow-origin: *");

if(isset($_GET['action'])){//START GATE
	require_once("../model/Connection.php");
	$obj_connect = new Connection();

	require_once("../model/AT_User_Fav.php");
	$obj_fav = new AT_User_Fav();


	if($_GET['action'] == 'get_data' && isset($_GET['user_id']) && isset($_GET['table_name'])){//START GET DATA
		$obj_connect->up();	

		$user_id = mysql_real_escape_string($_GET['user_id']);
		$table_name = mysql_real_escape_string($_GET['table_name']);

		$result = $obj_fav->get_data($user_id, $table_name);
		if(is_array($result)){
			$R_message = array("status" => "200", "message" => "fetch data success", "data" => $result);
		}else{
			$R_message = array("status" => "0", "message" => "fetch data failed");
		}
		echo json_encode($R_message);

		$obj_connect->down();	
	}//END GET DATA

	else if($_GET['action'] == 'update' && isset($_REQUEST['user_id']) && isset($_REQUEST['table_name']) && isset($_REQUEST['table_id'])){//START INSERT
	//else if($_GET['action'] == 'insert'){//START INSERT
		$obj_connect->up();	

		$user_id = mysql_real_escape_string($_REQUEST['user_id']);
		$table_name = mysql_real_escape_string($_REQUEST['table_name']);
		$table_id = mysql_real_escape_string($_REQUEST['table_id']);
		//$user_id = 10;
		//$table_name = "jobs";
		//$table_id = 3;
		$flag_exist = $obj_fav->check_exist($user_id, $table_name, $table_id);
		if($flag_exist == 1){//already exist, so delete
			$result = $obj_fav->delete($user_id, $table_name, $table_id);
			if($result == 1){
				$R_message = array("status" => "200", "message" => "User id $user_id unfavourite jobs id $table_id");
			}else{
				$R_message = array("status" => "0", "message" => "User id $user_id failed to unfavourite jobs id $table_id!");
			}
		}else{
			$result = $obj_fav->insert($user_id, $table_name, $table_id);
			if($result == 1){
				$R_message = array("status" => "200", "message" => "User id $user_id favourite jobs id $table_id");
			}else{
				$R_message = array("status" => "0", "message" => "User id $user_id failed to favourite jobs id $table_id!");
			}
		}
		echo json_encode($R_message);

		$obj_connect->down();
	}//END INSERT

	else if($_GET['action'] == 'delete' && isset($_GET['user_id']) && isset($_GET['table_name']) && isset($_GET['table_id'])){//START DELETE
		$obj_connect->up();	

		$user_id = mysql_real_escape_string($_GET['user_id']);
		$table_name = mysql_real_escape_string($_GET['table_name']);
		$table_id = mysql_real_escape_string($_GET['table_id']);

		$result = $obj_fav->delete($user_id, $table_name, $table_id);
		if($result == 1){
			$R_message = array("status" => "200", "message" => "User id $user_id unfavourite jobs id $table_id");
		}else{
			$R_message = array("status" => "0", "message" => "User id $user_id failed to unfavourite jobs id $table_id!");

		}
		echo json_encode($R_message);

		$obj_connect->down();
	}//END DELETE

}//END GATE
?>