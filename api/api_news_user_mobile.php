<?php 
header('content-type: application/json');
header("access-control-allow-origin: *");
require_once(dirname(__FILE__)."/../civimi/packages/check_input.php");

if(isset($_GET['action'])){//start gate
	
	require_once("../model/Connection.php");
	$obj_con = new Connection();
	
	require_once("../model/News_User_Mobile.php");
	$obj_nu = new News_User_Mobile();
	
	if($_GET['action'] == 'register' && $_REQUEST['gcm_id'] != ""){
		$obj_con->up();
		$R_message = array("status" => "404", "message" => "Register failed");
		
		$N_gcm_id = mysql_real_escape_string($_REQUEST['gcm_id']);
		$N_os = mysql_real_escape_string($_REQUEST['device_os']);
		$N_api = mysql_real_escape_string($_REQUEST['device_api']);
		$N_device = mysql_real_escape_string($_REQUEST['device_mydevice']);
		$N_model = mysql_real_escape_string($_REQUEST['device_model']);
		$N_product = mysql_real_escape_string($_REQUEST['device_product']);
		$N_notif = "yes";
		$N_publish = "Publish";

		$check_gcm = $obj_nu->check_gcm_id($N_gcm_id);
		if($check_gcm == 1){
			$result = $obj_nu->insert_data($N_gcm_id, $N_os, $N_api, $N_device, $N_model, $N_product, $N_notif, $N_publish);
			if($result){
				$datas = $obj_nu->get_news_user_detail($result);
				$R_message = array("status" => "200", "message" => "Register Success", "data" => $datas);
			}
		}else{
			$R_message = array("status" => "400", "message" => "GCM ID already exist");
		}
		
		$obj_con->down();
		echo json_encode($R_message);	
	}

	else if($_GET['action'] == 'send_notification'){
		$obj_con->up();

		// Replace with the real server API key from Google APIs
	    $apiKey = "AIzaSyDXIFXir1Wrgup3HaOFwL67RNwBD5Cj2k0";
	    $users = $obj_nu->get_news_user();
		if(is_array($users)){
			// Replace with the real client registration IDs
	    	$registrationIDs = array();
			for($i=0; $i < count($users); $i++){
				$registrationIDs[] = $users[$i]['Nu_gcmID'];
			}
		}

	    // Message to be sent
	    $message = "Lihat Artikel baru di Berita Karir";

	    // Set POST variables
	    $url = 'https://android.googleapis.com/gcm/send';

	    $fields = array(
	        'registration_ids' => $registrationIDs,
	        'data' => array( "message" => $message ),
	    );
	    $headers = array(
	        'Authorization: key=' . $apiKey,
	        'Content-Type: application/json'
	    );

	    // Open connection
	    $ch = curl_init();

	    // Set the URL, number of POST vars, POST data
	    curl_setopt( $ch, CURLOPT_URL, $url);
	    curl_setopt( $ch, CURLOPT_POST, true);
	    curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers);
	    curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true);
	    //curl_setopt( $ch, CURLOPT_POSTFIELDS, json_encode( $fields));

	    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	    // curl_setopt($ch, CURLOPT_POST, true);
	    // curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode( $fields));

	    // Execute post
	    $result = curl_exec($ch);

	    // Close connection
	    curl_close($ch);
	    echo $result;
	    //print_r($result);
	    //var_dump($result);
	    $obj_con->down();
	}

	else{
		echo "error";
	}
	
}//end gate

?>