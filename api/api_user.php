<?php 
require_once(dirname(__FILE__)."/../packages/check_input.php");
if(isset($_GET['action'])){//START GATE
	require_once("../model/Connection.php");
	$obj_con = new Connection();

	require_once("../model/User.php");
	$obj_user = new User();

	require_once("../model/AT_Progress.php");
	$obj_atpro = new AT_Progress();

	//===================================== check username ========================================
	//start check username
	if($_GET['action'] == 'check_username' && isset($_REQUEST['username'])){
		$obj_con->up();	

		$R_message = array("status" => "0", "message" => "Not Available");
		$N_username = mysql_real_escape_string($_REQUEST['username']);

		$result = $obj_user->check_username($N_username);
		if($result == 1){
			$R_message = array("status" => "1", "message" => "Available");
		}
		
		$obj_con->down();
		header("Content-Type: application/json");
		echo json_encode($R_message);	
	}//end check username

	//===================================== get data detail ========================================
	//start get data detail
	else if($_GET['action'] == 'get_data_detail' && isset($_REQUEST['id']) && isset($_REQUEST['auth_code'])){
		$obj_con->up();	
		$R_message = array("status" => "404", "message" => "Data Not Found");
		$N_id = mysql_real_escape_string($_REQUEST['id']);
		$N_authcode = mysql_real_escape_string($_REQUEST['auth_code']);

		if($obj_user->check_code($N_authcode, $N_id)){//check code
			$result = $obj_user->get_data_detail($N_id);
			if($result){
				$R_message = array("status" => "200", "message" => "OK");
				$R_message['data'] = $result;
			}
		}//check code
		else{
			$R_message = array("status" => "401", "message" => "Unauthorized");
		}
		$obj_con->down();

		header("Content-Type: application/json");
		echo json_encode($R_message);	
	}//end get data detail

	//===================================== get personal ========================================
	//start get personal
	else if($_GET['action'] == 'get_personal' && isset($_REQUEST['id']) && isset($_REQUEST['auth_code'])){
		$obj_con->up();	
		$R_message = array("status" => "404", "message" => "Data Not Found");
		$N_id = mysql_real_escape_string($_REQUEST['id']);
		$N_authcode = mysql_real_escape_string($_REQUEST['auth_code']);

		if($obj_user->check_code($N_authcode, $N_id)){//check code
				$result = $obj_user->get_data_detail($N_id);
				if($result){
					$R_message = array("status" => "200", "message" => "OK");
					$result[0]['age'] = calculate_age($result[0]['User_DOBday']."-".$result[0]['User_DOBmonth']."-".$result[0]['User_DOB']);
					$R_message['data'] = $result;
				}
		}//check code
		else{
			$R_message = array("status" => "401", "message" => "Unauthorized");
		}
		$obj_con->down();

		header("Content-Type: application/json");
		echo json_encode($R_message);	
	}//end get personal

	//CRUD

	//===================================== update personal ========================================
	//start update personal
	else if($_GET['action'] == 'update_personal' && isset($_REQUEST['id']) && isset($_REQUEST['auth_code'])){
		$obj_con->up();	
		$R_message = array("status" => "404", "message" => "Data Not Found");
		$N_id = mysql_real_escape_string($_REQUEST['id']);
		$N_authcode = mysql_real_escape_string($_REQUEST['auth_code']);

		//field
		$N_title = mysql_real_escape_string($_REQUEST['title']);
		$N_interest = mysql_real_escape_string($_REQUEST['interest']);
		$N_fname = mysql_real_escape_string($_REQUEST['fname']);
		$N_lname = mysql_real_escape_string($_REQUEST['lname']);
		$N_day = mysql_real_escape_string($_REQUEST['day']);
		$N_month = mysql_real_escape_string($_REQUEST['month']);
		$N_year = mysql_real_escape_string($_REQUEST['year']);
		$N_dob_show = mysql_real_escape_string($_REQUEST['dob_show']);
		$N_address = mysql_real_escape_string($_REQUEST['address']);
		$N_city = mysql_real_escape_string($_REQUEST['city']);
		$N_state = mysql_real_escape_string($_REQUEST['state']);
		$N_country = mysql_real_escape_string($_REQUEST['country']);
		$N_email = mysql_real_escape_string($_REQUEST['email']);
		$N_phone = mysql_real_escape_string($_REQUEST['phone']);
		$N_phone_show = mysql_real_escape_string($_REQUEST['phone_show']);
		$N_website = mysql_real_escape_string($_REQUEST['website']);

		if($obj_user->check_code($N_authcode, $N_id)){//check code
			if($N_interest != '' && $N_fname != '' && $N_lname != '' && $N_day != '' && $N_month != '' && $N_year != '' && $N_email != ''){
				$result = $obj_user->update_personal($N_id, $N_title, $N_interest, $N_fname, $N_lname, $N_day, $N_month, $N_year, $N_address, $N_city, $N_state,$N_country, $N_email, $N_phone, $N_dob_show, $N_phone_show, $N_website);
				//var_dump($result);
				if($result){
					//save progress
					$users = $obj_user->get_data_detail($N_id);
					if(is_array($users)){
						$N_array = array(
				            array('module' => 'image', 'field' => $users[0]['User_proPhotoThmb']),
				            array('module' => 'profession', 'field' => $users[0]['User_interest']),
				            array('module' => 'city', 'field' => $users[0]['User_city']),
				            array('module' => 'state', 'field' => $users[0]['User_state']),
				            array('module' => 'country', 'field' => $users[0]['User_country']),
				            array('module' => 'phone', 'field' => $users[0]['User_phone']),
				            array('module' => 'website', 'field' => $users[0]['User_website'])
				        );

				        $progress_personal = check_progress("personal", $N_array);
				        $obj_atpro->save_progress($N_id, "personal", $progress_personal);
					}//end save progress

					$R_message = array("status" => "200", "message" => "OK");
					$R_message['data'] = $result;
				}
				else{
					$R_message = array("status" => "400", "message" => "Update data personal failed");
				}
			}
			else{
				$R_message = array("status" => "400", "message" => "Field empty");
			}
		}//check code
		else{
			$R_message = array("status" => "401", "message" => "Unauthorized");
		}
		$obj_con->down();

		header("Access-Control-Allow-Origin: *"); 
		header("Content-Type: application/json");
		echo json_encode($R_message);	
	}//end update personal

	//===================================== update summary ========================================
	//start update summary
	else if($_GET['action'] == 'update_summary' && isset($_REQUEST['id']) && isset($_REQUEST['auth_code'])){
		$obj_con->up();	
		$R_message = array("status" => "404", "message" => "Data Not Found");
		$N_id = mysql_real_escape_string($_REQUEST['id']);
		$N_authcode = mysql_real_escape_string($_REQUEST['auth_code']);

		//field
		$N_summary = mysql_real_escape_string($_REQUEST['summary']);

		if($obj_user->check_code($N_authcode, $N_id)){//check code
			$result = $obj_user->update_summary($N_id,"obj",$N_summary);
			if($result){
				$R_message = array("status" => "200", "message" => "OK");
				$R_message['data'] = $result;
			}
		}//check code
		else{
			$R_message = array("status" => "401", "message" => "Unauthorized");
		}
		$obj_con->down();

		header("Content-Type: application/json");
		echo json_encode($R_message);	
	}//end update summary

	//===================================== update background image ========================================
	//start update background image
	else if($_GET['action'] == 'update_background' && isset($_REQUEST['id']) && isset($_REQUEST['auth_code'])){
		$obj_con->up();	
		$R_message = array("status" => "404", "message" => "Data Not Found");
		$N_id = mysql_real_escape_string($_REQUEST['id']);
		$N_authcode = mysql_real_escape_string($_REQUEST['auth_code']);
		//field
		$N_background = mysql_real_escape_string($_REQUEST['background']);

		if($obj_user->check_code($N_authcode, $N_id)){//check code
			$result = $obj_user->update_bg_image($N_id, $N_authcode, $N_background);
			if($result){
				$R_message = array("status" => "200", "message" => "Update background image success");
				$R_message['data'] = $result;
			}else{
				$R_message = array("status" => "400", "message" => "Update background image failed");
			}
		}//check code
		else{
			$R_message = array("status" => "401", "message" => "Unauthorized");
		}
		$obj_con->down();

		header("Content-Type: application/json");
		echo json_encode($R_message);	
	}//end update background image

	else{
		echo "error";
	}

}//END GATE
?>