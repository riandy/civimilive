<?php 
header('content-type: application/json');
header("access-control-allow-origin: *");

if(isset($_GET['action'])){//start gate
	
	require_once("../model/Connection.php");
	$obj_connect = new Connection();
	
	require_once("../model/Company.php");
	$obj_company = new Company();
	
	$obj_connect->up();//OPEN CONNECTION
	
	if($_GET['action'] == 'get_popular'){
		$R_message = array("status" => "400", "message" => "No Data");
		
		$result = $obj_company->get_popular();
		if(is_array($result)){
			$R_message = array("status" => "200", "message" => "Data Exist", "data" => $result);
		}
		
	echo json_encode($R_message);	
	}
	else if($_GET['action'] == 'get_data'){
		$R_message = array("status" => "400", "message" => "No Data");
		
		$result = $obj_company->get_data_by_page();
		if(is_array($result)){
			$R_message = array("status" => "200", "message" => "Data Exist", "data" => $result);
		}
		
	echo json_encode($R_message);	
	}
	
	$obj_connect->down();//CLOSE CONNECTION
}//end gate

?>