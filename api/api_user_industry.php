<?php 
header('content-type: application/json');
header("access-control-allow-origin: *");
if(isset($_GET['action'])){//START GATE
	require_once("../model/Connection.php");
	$obj_con = new Connection();

	require_once("../model/AT_User_Industry.php");
	$obj_atui = new AT_User_Industry();

	require_once("../model/User.php");
	$obj_user = new User();

	//===================================== get data industry ========================================
	//start get data industry
	if($_GET['action'] == 'get_data_industry'){
		$obj_con->up();	
		$R_message = array("status" => "400", "message" => "No Data");

		$result = $obj_atui->get_index_industry();
		if(is_array($result)){
			
			$R_message = array("status" => "200", "message" => "Data Exist", "data" => $result);
		}

		$obj_con->down();
		echo json_encode($R_message);	
	}//end get data industry

	//===================================== insert data ========================================
	//start insert data
	else if($_GET['action'] == 'insert_data' && isset($_REQUEST['user_id']) && isset($_REQUEST['auth_code'])){
		$obj_con->up();	
		$R_message = array("status" => "404", "message" => "Data Not Found");
		$N_user_id = mysql_real_escape_string($_REQUEST['user_id']);
		$N_authcode = mysql_real_escape_string($_REQUEST['auth_code']);
		//field
		$N_industry = mysql_real_escape_string($_REQUEST['industry']);
	
		if($obj_user->check_code($N_authcode, $N_user_id)){//check code
			$obj_atui->delete_data($N_user_id); //remove data in AT_User_Industry
	    	$data = explode(",", $N_industry);
		    if(isset($data)){
		        for($i=0; $i < count($data); $i++){
		        	if($data[$i] != 0){
		        		$result = $obj_atui->insert_data($N_user_id, $data[$i]);
		        	}
		        }
		        
		        $obj_user->update_form_industry($N_user_id);
		        if($result == 1){
					$R_message = array("status" => "200", "message" => "Insert data industry success");
				}else{
					$R_message = array("status" => "400", "message" => "Insert data industry failed");
				}
		    }
		}//check code
		else{
			$R_message = array("status" => "401", "message" => "Unauthorized");
		}

		$obj_con->down();
		echo json_encode($R_message);	
	}//end insert data

	else{
		echo "error";
	}

}//END GATE
?>