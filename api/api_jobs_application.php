<?php 

if(isset($_GET['action'])){//START GATE
	require_once("../model/Connection.php");
	$obj_connect = new Connection();

	require_once("../model/AT_Jobs_Application.php");
	$obj_japp = new AT_Jobs_Application();

	if($_GET['action'] == 'insert' && isset($_REQUEST['jobs_id']) && isset($_REQUEST['user_id'])){//START INSERT
		$obj_connect->up();	

		$jobs_id   = mysql_real_escape_string($_REQUEST['jobs_id']);
		$user_id   = mysql_real_escape_string($_REQUEST['user_id']);
		$message   = mysql_real_escape_string($_REQUEST['message']);
		$read 	   = mysql_real_escape_string($_REQUEST['read']);
		$read_date = mysql_real_escape_string($_REQUEST['read_date']);
		$status    = mysql_real_escape_string($_REQUEST['status']);
		$publish   = mysql_real_escape_string($_REQUEST['publish']);
		
		$result = $obj_japp->insert($jobs_id, $user_id, $message, $read, $read_date, $status, $publish);
		if($result){
			$R_message = array("status" => "200", "message" => "User id $user_id apply job id $jobs_id");
		}else{
			$R_message = array("status" => "404", "message" => "Failed to apply job");
		}
		echo json_encode($R_message);

		$obj_connect->down();
	}//END INSERT

	else if($_GET['action'] == 'delete' && isset($_GET['id'])){//START DELETE
		$obj_connect->up();	

		$id = mysql_real_escape_string($_GET['id']);

		$result = $obj_japp->delete($id);
		if($result == 1){
			$R_message = array("status" => "200", "message" => "Job application id $id has been deleted");
		}else{
			$R_message = array("status" => "404", "message" => "Job application failed to be deleted");	
		}
		echo json_encode($R_message);

		$obj_connect->down();
	}//END DELETE

	else if($_GET['action'] == 'read' && isset($_GET['id'])){//START READ
		$obj_connect->up();	

		$id = mysql_real_escape_string($_GET['id']);

		$result = $obj_japp->read($id);
		if($result == 1){
			$R_message = array("status" => "200", "message" => "Job application id $id read");
		}else{
			$R_message = array("status" => "404", "message" => "Job application failed to be read");
		}
		echo json_encode($R_message);

		$obj_connect->down();
	}//END READ

	else if($_GET['action'] == 'publish' && isset($_GET['id'])){//START PUBLISH
		$obj_connect->up();	

		$id = mysql_real_escape_string($_GET['id']);

		$result = $obj_japp->publish($id);
		if($result == 1){
			$R_message = array("status" => "200", "message" => "Job application id $id in publish");
		}else{
			$R_message = array("status" => "404", "message" => "Job application failed to change in publish");
		}
		echo json_encode($R_message);

		$obj_connect->down();
	}//END PUBLISH

	else if($_GET['action'] == 'unpublish' && isset($_GET['id'])){//START UNPUBLISH
		$obj_connect->up();	

		$id = mysql_real_escape_string($_GET['id']);

		$result = $obj_japp->unpublish($id);
		if($result == 1){
			$R_message = array("status" => "200", "message" => "Job application id $id in unpublish");
		}else{
			$R_message = array("status" => "404", "message" => "Job application failed to change in unpublish");
		}
		echo json_encode($R_message);

		$obj_connect->down();
	}//END UNPUBLISH

	else if($_GET['action'] == 'update' && isset($_REQUEST['id']) && isset($_REQUEST['jobs_id']) && isset($_REQUEST['user_id'])){//START UPDATE
		$obj_connect->up();	

		$id        = mysql_real_escape_string($_REQUEST['id']);
		$jobs_id   = mysql_real_escape_string($_REQUEST['jobs_id']);
		$user_id   = mysql_real_escape_string($_REQUEST['user_id']);
		$message   = mysql_real_escape_string($_REQUEST['message']);
		$status    = mysql_real_escape_string($_REQUEST['status']);
		$publish   = mysql_real_escape_string($_REQUEST['publish']);
		
		$result = $obj_japp->update($id, $jobs_id, $user_id, $message, $status, $publish);
		if($result){
			$R_message = array("status" => "200", "message" => "User id $user_id update apply job id $jobs_id");
		}else{
			$R_message = array("status" => "404", "message" => "Failed to update apply job");
		}
		echo json_encode($R_message);

		$obj_connect->down();
	}//END UPDATE

	else{
		echo "error";
	}
}//END GATE
?>